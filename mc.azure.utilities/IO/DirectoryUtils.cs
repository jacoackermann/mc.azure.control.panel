﻿using System;
using System.IO;

using Colorware.Core.Azure.Config;

namespace mc.azure.utilities.IO {
    public static class DirectoryUtils {
        public static string GetSiteDir(string httpContextRoot) {
            if (ConfigValueReader.IsProductionMode) {
                return Environment.GetEnvironmentVariable("HOME");
            }

            return httpContextRoot;
        }

        //---------------------------------------------------------------------

        public static void CreatePath(string pathStr) {
            if (string.IsNullOrWhiteSpace(pathStr)) return;

            // assume we have a directory path (not a file path) at this point

            DirectoryInfo dirInfo = new DirectoryInfo(pathStr);
            if (dirInfo.FullName == dirInfo.Root.FullName) {
                // don't attempt to create e.g. c:
                return;
            }

            // first create the containing directories
            CreatePath(dirInfo.Parent.FullName);

            // finally create the pathStr directory
            if (!Directory.Exists(pathStr)) {
                Directory.CreateDirectory(pathStr);
            }
        }
    }
}