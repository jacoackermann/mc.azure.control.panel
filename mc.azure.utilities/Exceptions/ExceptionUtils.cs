﻿using System;
using System.Text;

namespace mc.azure.utilities.Exceptions {
    public static class ExceptionUtils {
        public static string GetExceptionMsg(Exception e) {
            if (e == null) return "";

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(e.Message);

            if (e.InnerException != null) {
                sb.AppendLine();
                sb.AppendLine(GetExceptionMsg(e.InnerException));
            }

            return sb.ToString();
        }
    }
}