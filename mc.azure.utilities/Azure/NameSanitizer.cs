﻿using System.Linq;

namespace mc.azure.utilities.Azure {
    public static class NameSanitizer {
        public static string GetSanitizedName(string name) {
            if (string.IsNullOrWhiteSpace(name)) return string.Empty;

            // remove all non-alphanumeric characters
            name = new string(
                name.Where(c => char.IsLetterOrDigit(c) /*|| char.IsWhiteSpace(c) || c == '-'*/).ToArray());

            // all lowercase
            name = name.ToLower();

            return name;
        }
    }
}