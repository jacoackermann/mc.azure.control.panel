﻿using System;
using System.Web;
using System.Web.Mvc;

using Colorware.Core.Helpers;

namespace mc.azure.utilities.Server {
    public static class ServerInformationHelper
    {
        private static string serverVersion;

        public static IHtmlString ServerVersion(this HtmlHelper helper)
        {
            try
            {
                if (serverVersion == null)
                {
                    serverVersion = ApplicationHelper.GetMajorVersion() + " (build " + ApplicationHelper.GetBuildVersion() + ")";
                }
                return new HtmlString(serverVersion);
            }
            catch (Exception e)
            {
                return new HtmlString("Could not get version: " + e.Message);
            }
        }

        private static string serverVersionVerbose;
        public static IHtmlString ServerVersionVerbose(this HtmlHelper helper)
        {
            try
            {
                if (serverVersionVerbose == null)
                {
                    serverVersionVerbose = ApplicationHelper.GetVersionVerbose();
                }
                return new HtmlString(serverVersionVerbose);
            }
            catch (Exception e)
            {
                return new HtmlString("Could not get version: " + e.Message);
            }
        }
    }
}