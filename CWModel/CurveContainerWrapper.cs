﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CWModel {
    public class CurveContainerWrapper {
        private Hashtable data;

        public CurveContainerWrapper() {
            data = new Hashtable();
        }

        public void AddCurveContainer(string description, CurveContainer container) {
            data.Add(description, container);
        }

        public CurveContainer GetCurveContainer(string description) {
            if (data.ContainsKey(description)) return (CurveContainer)(data[description]);
            else throw (new Exception(string.Format("CurveContainer[{0}] not present", description)));
        }

        public List<string> GetCurveContainerDescriptionsList() {
            List<string> result = new List<string>();
            foreach (string key in data.Keys) result.Add(key);
            return result;
        }
    }
}