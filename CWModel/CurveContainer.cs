﻿using System;
using System.Collections;

namespace CWModel {
    public class CurveContainer {
        public int PointCount {
            get { return xpoints.Count; }
        }

        /// <summary>
        /// Minimum and maximum value of the interval where the curve is defined, based on the minimum of the original datapoints
        /// </summary>
        public double XIntervalMinimum { get; set; }

        public double XIntervalMaximum { get; set; }

        public double XCurveMinimum { get; set; }
        //x value of the absolute minimum of the curve in the interval specified above
        public double YCurveMinimum { get; set; } //the actual minimum
        public double YMinPoints { get; set; } //minimum of the set of data points given
        public double YMaxPoints { get; set; } //maximum of the set of data points given
        public double LeftMaximum { get; set; } //maximum of the set of datapoints left side
        public double RightMaximum { get; set; } //maximum of the set of datapoints right side

        private double MinimumYArray = 0;
        private double MaximumYArray = 0;
        private ArrayList xpoints;
        private ArrayList ypoints;
        private alglib.spline1dinterpolant s; //method used for creating the spline
        private alglib.spline1dfitreport i;


        private void CreateCurve(double[] x, double[] y, int Count, double rho) //creates the spline
        {
            XIntervalMinimum = x[0];
            XIntervalMaximum = x[Count - 1];
            FindMinYMaxY(y, Count);
            s = new alglib.spline1dinterpolant();
            i = new alglib.spline1dfitreport();
            int inf;
            alglib.spline1dfitpenalized(x, y, Count, rho, out inf, out s, out i);
            FindMinimumAndLeftAndRightMaximum();
        }

        private void FindMinYMaxY(double[] y, int Count) {
            MinimumYArray = y[0];
            MaximumYArray = y[0];
            if (Count > 1) {
                for (int i = 1; i < Count; i++) {
                    if (y[i] < MinimumYArray) MinimumYArray = y[i];
                    if (y[i] > MaximumYArray) MaximumYArray = y[i];
                }
            }
        }

        private double FindRoot(double val, double XMin, double XMax) {
            //Find roots of a value (val) in the given interval (XMin,XMax) by means of Regula-Falsi
            if (val < YCurveMinimum) return YCurveMinimum;
            if (XMin == XMax) return XMin;
            double fmin = alglib.spline1dcalc(s, XMin) - val;
            double fmax = alglib.spline1dcalc(s, XMax) - val;
            double c = XMin - fmin * (XMin - XMax) / (fmin - fmax);
            double fc = alglib.spline1dcalc(s, c) - val;
            int Counter = 0;

            while (Math.Abs(fc) > 0.0001) {
                if (fmin * fc > 0) XMin = c;
                if (fmax * fc > 0) XMax = c;
                fmin = alglib.spline1dcalc(s, XMin) - val;
                fmax = alglib.spline1dcalc(s, XMax) - val;
                c = XMin - fmin * (XMin - XMax) / (fmin - fmax);
                if (c < XIntervalMinimum || c > XIntervalMaximum)
                    throw (new Exception("egula falsi failed to find a root"));
                fc = alglib.spline1dcalc(s, c) - val;
                if (Counter++ > 1000) throw (new Exception("Regula falsi failed to find a root"));
            }
            return c;
        }

        private void FindMinimumAndLeftAndRightMaximum() {
            // Brute force, nothing fancy. Good enough for now.
            double Xmin = XIntervalMinimum;
            double Xmax = XIntervalMaximum;
            double YLeftMax = GetCurveValue(Xmin);
            double YRightMax = GetCurveValue(Xmax);
            double interval = (Xmax - Xmin) / 10000;
            double CurrentMinX = Xmin;
            double FX = GetCurveValue(CurrentMinX);
            double CurrentMinFX = GetCurveValue(CurrentMinX);
            for (double currentX = Xmin; currentX <= Xmax; currentX += interval) {
                if (FX < CurrentMinFX) {
                    CurrentMinFX = FX;
                    CurrentMinX = currentX;
                }
                FX = GetCurveValue(currentX);
            }
            XCurveMinimum = CurrentMinX;
            YCurveMinimum = CurrentMinFX;

            for (double currentX = Xmin; currentX <= XCurveMinimum; currentX += interval) {
                YLeftMax = Math.Max(YLeftMax, GetCurveValue(currentX));
            }
            LeftMaximum = YLeftMax;
            for (double currentX = XCurveMinimum; currentX <= XIntervalMaximum; currentX += interval) {
                YRightMax = Math.Max(YRightMax, GetCurveValue(currentX));
            }
            RightMaximum = YRightMax;
        }

        public double GetXPoint(int index) {
            return (double)xpoints[index];
        }

        public double GetYPoint(int index) {
            return (double)ypoints[index];
        }

        public CurveContainer() {
            xpoints = new ArrayList();
            ypoints = new ArrayList();
        }

        public void AddPoint(double X, double Y) //Adds an x,y point to the (sorted) list of points.
        {
            // Merge based on sorting order of X
            int ListSize = xpoints.Count;
            int Index = 0;
            if (ListSize == 0) {
                xpoints.Add(X);
                ypoints.Add(Y);
                YMinPoints = Y;
                YMaxPoints = Y;
            }
            else {
                YMinPoints = Math.Min(YMinPoints, Y);
                YMaxPoints = Math.Max(YMaxPoints, Y);
                while (X >= (double)xpoints[Index]) {
                    Index++;
                    if (Index >= ListSize) break;
                }
                xpoints.Insert(Index, X);
                ypoints.Insert(Index, Y);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rho"></param>
        public void CreateModel(double rho)
            //create the spline based on the list of data points. The factor tho determines the 'smoothness'.
            //More is smoother, less means closer to the original datapoints. Usable values (-1,1).
            //For now we use the value 0.
        {
            int Count = xpoints.Count;
            double[] x = new double[Count];
            double[] y = new double[Count];
            for (int i = 0; i < Count; i++) {
                x[i] = (double)xpoints[i];
                y[i] = (double)ypoints[i];
            }
            CreateCurve(x, y, Count, rho);
        }

        public double GetCurveValue(double x) //returns the y value of the curve for a given x
        {
            if (x < XIntervalMinimum)
                return LeftMaximum;
            else if (x > XIntervalMaximum)
                return RightMaximum;
            else
                return alglib.spline1dcalc(s, x);
        }

        public double LeftRoot(double val) // determines the left x value on the curve.
        {
            if (val > GetCurveValue(XIntervalMinimum))
                return XIntervalMinimum;
            else
                return FindRoot(val, XIntervalMinimum, XCurveMinimum);
        }

        public double RightRoot(double val) //determines the right x value on the curve.
        {
            if (val > GetCurveValue(XIntervalMaximum))
                return XIntervalMaximum;
            else
                return FindRoot(val, XCurveMinimum, XIntervalMaximum);
        }
    }
}