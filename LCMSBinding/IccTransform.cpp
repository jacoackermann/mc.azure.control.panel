#include "Stdafx.h"
#include "IccTransform.h"

namespace LCMSBinding {
	IccTransform::IccTransform(IccProfile ^ inputProfile, cmsUInt32Number inputFormat) {
		this->inputProfile = inputProfile;
		this->inputFormat = inputFormat;
		this->outputProfile = gcnew IccProfile(cmsCreateLab2Profile(NULL));
		this->outputFormat = TYPE_Lab_DBL;
	}
	IccTransform::IccTransform(IccProfile ^ inputProfile, cmsUInt32Number inputFormat, IccProfile ^ outputProfile, cmsUInt32Number outputFormat) {
		this->inputProfile = inputProfile;
		this->inputFormat = inputFormat;
		this->outputProfile = outputProfile;
		this->outputFormat = outputFormat;
	}
	IccTransform::~IccTransform() {
		Close();
	}

	IccTransform ^ IccTransform::CreateTransformation(IccProfile ^ inputProfile) {
		IccTransform ^ transform;
		if (inputProfile->IsCMYK()) {
			transform = gcnew IccTransform(inputProfile, TYPE_CMYK_DBL);
		}
		else if (inputProfile->ContainsCMYK()) {
			int numColorants = inputProfile->GetNumberOfColorants();
			transform = gcnew IccTransform(inputProfile, (FLOAT_SH(1) | COLORSPACE_SH(PT_MCH1 + numColorants - 1) | CHANNELS_SH(numColorants) | BYTES_SH(0)));
		}
		else
			return nullptr;
		if (!transform->Create())
			return nullptr;
		return transform;
	}

	void IccTransform::Close() {
		if( transformHandle ) {
			cmsDeleteTransform(transformHandle);
		}
		transformHandle = nullptr;
	}

	bool IccTransform::Create() {
		// Use absolute colorimetric only for now!
		transformHandle = cmsCreateTransform(inputProfile->profileHandle, inputFormat, outputProfile ? outputProfile->profileHandle : 0,
			outputFormat, INTENT_ABSOLUTE_COLORIMETRIC, 0);

		return transformHandle != 0;
	}



	LabTuple ^ IccTransform::CMYKToLab(CMYK ^ cmyk) {
		return CMYKToLab(cmyk->C, cmyk->M, cmyk->Y, cmyk->K);
	}

	LabTuple ^ IccTransform::CMYKToLab(double c, double m, double y, double k) {
		double cmyk[4] = { c, m, y, k };
		double lab[3];
		if (inputProfile->IsCMYK()) {
			cmsDoTransform(transformHandle, cmyk, lab, 1);
		}
		else {
			double * colorantData = new double[inputProfile->GetNumberOfColorants()];
			for (int i = 0; i < inputProfile->GetNumberOfColorants(); ++i)
				colorantData[i] = 0.0;
			for (int i = 0; i < 4; ++i)
				colorantData[inputProfile->getCMYKChannel(i)] = cmyk[i];
			cmsDoTransform(transformHandle, colorantData, lab, 1);
			delete[] colorantData;
		}
		return gcnew LabTuple(lab[0], lab[1], lab[2]);
	}

	LabTuple ^ IccTransform::SpotToLab(int spotIndex, double percentage) {
		double * colorantData = new double[inputProfile->GetNumberOfColorants()];
		double lab[3];
		for (int i = 0; i < inputProfile->GetNumberOfColorants(); ++i)
			colorantData[i] = 0.0;
		colorantData[spotIndex] = percentage;
		cmsDoTransform(transformHandle, colorantData, lab, 1);
		delete[] colorantData;
		return gcnew LabTuple(lab[0], lab[1], lab[2]);
	}
}