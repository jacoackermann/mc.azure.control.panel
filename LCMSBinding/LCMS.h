#pragma once


namespace LCMSBinding {
	public ref class LCMS {
	public:
		/** Should be called before using any functions of the library. */
		static void Initialize();
		/** Returns true if an error occurred at some point during the execution, false otherwise.
		 */
		static bool HasError();
		/** Return the last error that occurred as a string and reset the error state.
		* This will always return the last error. If another error occurs while another error was pending, the previous error
		* will be 'overwritten'. This method guarantees to return a valid string, if no error occurred previously this
		* will just return an empty string.
		*/
		static String ^ GetError();

	internal:
		/** Add a custom error message */
		static void SetError(String ^ error);

	private:
		static bool initialized = false;

		static void initErrorHandler();

		static GCHandle gch;
		static String ^ errorMessage;
		[UnmanagedFunctionPointer(CallingConvention::Cdecl)]
		delegate void lcmsErrorHandlerDelegate(cmsContext contextId, cmsUInt32Number errorCode, const char * message);
		static void lcmsErrorHandler(cmsContext contextId, cmsUInt32Number errorCode, const char * message);
	};
}