#include "stdafx.h"
#include "CGATSParser.h"


namespace LCMSBinding {
	std::vector<std::pair<std::string, std::string>> CGATSParser::getAllProperties(const std::vector<std::string> & tableLines) {
		std::vector<std::pair<std::string, std::string>> result;

		for (std::string line : tableLines) {
			// Skip some white space only
			size_t startLine = line.find_first_not_of("\t ");
			if (startLine == std::string::npos)
				continue;
			// Skip comments
			if (line[startLine] == '#')
				continue;
			// Find property name
			size_t endOfPropertyName = line.find_first_of("\t", startLine);
			if (endOfPropertyName == std::string::npos)
				endOfPropertyName = line.size();
			std::string propertyName = line.substr(startLine, endOfPropertyName - startLine);
			// Stop reading at data start
			if (propertyName == "BEGIN_DATA_FORMAT")
				break;
			// No room for property value?
			if (endOfPropertyName == line.size())
				continue;
			// Get and add property
			std::string property = line.substr(endOfPropertyName + 1, line.size() - (endOfPropertyName + 1));
			result.push_back(std::pair<std::string, std::string>(propertyName, stripQuotes(property)));
		}

		return result;
	}

	std::string CGATSParser::findPropertyString(std::vector<std::pair<std::string, std::string>> properties, std::vector<std::string> propertyNameMatches, std::regex regularExpression) {
		std::vector<std::string> results = findPropertyStrings(properties, propertyNameMatches, regularExpression);
		if (results.empty())
			return "";
		return results[0];
	}

	std::vector<std::string> CGATSParser::findPropertyStrings(std::vector<std::pair<std::string, std::string>> properties, std::vector<std::string> propertyNameMatches, std::regex regularExpression) {
		std::vector<std::string> result;
		for (std::pair<std::string, std::string> property : properties) {
			std::string propertyName = toLower(property.first);
			if (std::find_if(propertyNameMatches.begin(), propertyNameMatches.end(), [&](std::string & nameMatch) {
				return (toLower(nameMatch) == propertyName);
			}) != propertyNameMatches.end() && std::regex_match(property.second, regularExpression))
				result.push_back(property.second);
		}
		if (result.empty())
			return findPropertyStringsInSubProperties(properties, propertyNameMatches, regularExpression);
		return result;
	}

	std::vector<std::string> CGATSParser::findPropertyStringsInSubProperties(std::vector<std::pair<std::string, std::string>> properties, std::vector<std::string> propertyNameMatches, std::regex regularExpression) {
		std::vector<std::string> result;
		for (std::pair<std::string, std::string> property : properties) {
			//check whether the property value contains any of the name matches
			std::string propertyValue = property.second;
			std::string propertyValueLowerCase = toLower(property.second);
			bool containsNameMatch = false;
			for (std::string & nameMatch : propertyNameMatches) {
				if (propertyValueLowerCase.find(toLower(nameMatch)) != std::string::npos) {
					containsNameMatch = true;
					break;
				}
			}
			if (!containsNameMatch)
				continue;

			//from this point, we have a name match, so try to parse the subproperty list, if it seems valid
			//any sequence of <name, value>+ is supported, with delimiting characters whitespace, ',' and '='
			std::vector<std::string> tokens = tokenizeProperty(propertyValue, " \t,=", "\'\'\"\"{}");
			for (int i = tokens.size() - 1; i >= 0; --i) {
				if (tokens[i] == "")
					tokens.erase(tokens.begin() + i);
			}
			for (std::string & token : tokens)
				token = stripQuotes(token);
			for (size_t i = 0; i + 1 < tokens.size(); i += 2) {
				if (std::find_if(propertyNameMatches.begin(), propertyNameMatches.end(), [&](std::string & nameMatch) {
					return (toLower(nameMatch) == toLower(tokens[i]));
				}) != propertyNameMatches.end() && std::regex_match(tokens[i + 1], regularExpression))
					result.push_back(tokens[i + 1]);
			}
		}
		return result;
	}

	std::vector<std::string> CGATSParser::getColumnNames(cmsHANDLE handle) {
		std::vector<std::string> result;
		char ** columnNames;
		int numColumns = cmsIT8EnumDataFormat(handle, &columnNames);
		for (int i = 0; i < numColumns; ++i)
			result.push_back(columnNames[i]);
		return result;
	}

	std::vector<std::string> CGATSParser::readSampleNames(cmsHANDLE handle, int numSamples, const std::vector<std::string> & columnNames) {
		std::vector<std::string> result;
		int columnIndex = getColumnIndexByNames(columnNames, { "SAMPLE_NAME" });
		if (columnIndex >= 0) {
			for (int i = 0; i < numSamples; ++i)
				result.push_back(cmsIT8GetDataRowCol(handle, i, columnIndex));
		}
		return result;
	}

	std::vector<double> CGATSParser::readSampleData(cmsHANDLE handle, int numSamples, const std::vector<std::string> & columnNames, std::regex regularExpression, int numColumns) {
		std::vector<double> result;
		auto beginColumn = std::find_if(columnNames.begin(), columnNames.end(), [&](const std::string & columnName) {
			return std::regex_match(columnName, regularExpression);
		});
		if (beginColumn == columnNames.end())
			return result;
		auto endColumn = std::find_if_not(beginColumn, columnNames.end(), [&](const std::string & columnName) {
			return std::regex_match(columnName, regularExpression);
		});
		if (numColumns < 0 || (numColumns >= 0 && (endColumn - beginColumn) == numColumns)) {
			for (int i = 0; i < numSamples; ++i) {
				for (auto it = beginColumn; it != endColumn; ++it) {
					result.push_back(cmsIT8GetDataRowColDbl(handle, i, it - columnNames.begin()));
				}
			}
		}
		return result;
	}

	std::vector<double> CGATSParser::readSpectralSampleData(cmsHANDLE handle, int numSamples, const std::vector<std::string> & columnNames, int & waveLengthStart, int & waveLengthEnd) {
		std::vector<double> result;

		std::regex columnRegularExpression(".*(nm|NM)_?[0-9]{3}");
		auto spectralStart = std::find_if(columnNames.begin(), columnNames.end(), [&](const std::string & columnName) {
			return std::regex_match(columnName, columnRegularExpression);
		});
		// Require at least 31 spectral values, for 10nm steps in 400 - 700nm
		if (spectralStart == columnNames.end() || (columnNames.end() - spectralStart) < 31)
			return result;

		int firstWaveLength = atoi(spectralStart->substr(spectralStart->size() - 3, 3).c_str());
		// Check all wavelength intervals
		int previousWaveLength = firstWaveLength;
		auto spectralEnd = spectralStart;
		for (auto spectralIt = spectralStart + 1; spectralIt != columnNames.end() && std::regex_match(*spectralIt, columnRegularExpression); ++spectralIt) {
			int nextWaveLength = atoi(spectralIt->substr(spectralIt->size() - 3, 3).c_str());
			if (nextWaveLength - previousWaveLength != 10)
				return result;
			previousWaveLength = nextWaveLength;
			spectralEnd = spectralIt;
		}
		++spectralEnd;
		int lastWaveLength = previousWaveLength;

		for (int i = 0; i < numSamples; ++i) {
			for (auto it = spectralStart; it != spectralEnd; ++it) {
				result.push_back(cmsIT8GetDataRowColDbl(handle, i, it - columnNames.begin()));
			}
		}
		waveLengthStart = firstWaveLength;
		waveLengthEnd = lastWaveLength;
		return result;
	}

	int CGATSParser::getColumnIndexByNames(const std::vector<std::string> & columnNames, std::vector<std::string> columnNameMatches) {
		for (int i = 0; i < (int)columnNames.size(); ++i) {
			for (std::string & nameMatch : columnNameMatches)
				if (columnNames[i] == nameMatch)
					return i;
		}
		return -1;
	}



	std::vector<std::string> CGATSParser::tokenize(const std::string & input, const std::string & delimiters) {
		std::vector<std::string> result;
		int lastsplit = -1;
		for (int i = 0; i < (int)input.size(); ++i) {
			if (delimiters.find(input[i]) != std::string::npos) {
				if (i > lastsplit + 1)
					result.push_back(input.substr(lastsplit + 1, i - (lastsplit + 1)));
				lastsplit = i;
			}
		}
		if ((int)input.size() > lastsplit + 1)
			result.push_back(input.substr(lastsplit + 1, input.size() - lastsplit - 1));
		return result;
	}
	std::vector<std::string> CGATSParser::tokenizeProperty(const std::string & input, const std::string & delimiters, const std::string & ignorePairsOf) {
		std::vector<std::string> result;
		int lastsplit = -1;
		for (int i = 0; i < (int)input.size(); ++i) {
			if (delimiters.find(input[i]) != std::string::npos) {
				if (i > lastsplit + 1)
					result.push_back(input.substr(lastsplit + 1, i - (lastsplit + 1)));
				lastsplit = i;
			}
			size_t pairCharIndex = ignorePairsOf.find(input[i]);
			if (pairCharIndex != std::string::npos && pairCharIndex % 2 == 0)
				i = input.find(ignorePairsOf[pairCharIndex + 1], i + 1);
		}
		if ((int)input.size() > lastsplit + 1)
			result.push_back(input.substr(lastsplit + 1, input.size() - lastsplit - 1));
		return result;
	}
	std::string CGATSParser::join(const std::vector<std::string> & strings, char seperator) {
		size_t resultLength = 0;
		size_t numInput = strings.size();
		for (size_t i = 0; i < numInput; ++i)
			resultLength += strings[i].size() + 1;
		if (numInput > 0)
			--resultLength; //do not add trailing seperator

		std::string result;
		if (numInput == 0)
			return result;
		result.reserve(resultLength);
		for (size_t i = 0; i < numInput; ++i) {
			result += strings[i];
			if (i < numInput - 1)
				result.push_back(seperator);
		}
		return result;
	}
	std::string CGATSParser::toLower(std::string str) {
		for (size_t i = 0; i < str.size(); ++i)
			if (str[i] >= 'A' && str[i] <= 'Z')
				str[i] += 'a' - 'A';
		return str;
	}
	std::string CGATSParser::stripQuotes(const std::string & str) {
		if (str[0] == '\'' || str[0] == '\"')
			return str.substr(1, str.size() - 2);
		return str;
	}
	bool CGATSParser::startsWith(const std::string & str, const std::string & start) {
		if (str.size() < start.size())
			return false;
		for (size_t i = 0; i < start.size(); ++i)
			if (str[i] != start[i])
				return false;
		return true;
	}
}