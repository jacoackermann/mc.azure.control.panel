#pragma once

#include "CGATSTable.h"


namespace LCMSBinding {
	class CGATSParser
	{
	public:
		// Returns a list of all properties as pairs of <name, value>, duplicate names may occur
		static std::vector<std::pair<std::string, std::string>> getAllProperties(const std::vector<std::string> & tableLines);
		static std::string findPropertyString(std::vector<std::pair<std::string, std::string>> properties, std::vector<std::string> propertyNameMatches, std::regex regularExpression);
		// Searches the properties for a name matching any in propertyNameMatches (lower case matching) with a value matching the given regular expression
		static std::vector<std::string> findPropertyStrings(std::vector<std::pair<std::string, std::string>> properties, std::vector<std::string> propertyNameMatches, std::regex regularExpression);
		// Invoked from findPropertyString, continues to search the property values, for a sublist of properties
		static std::vector<std::string> findPropertyStringsInSubProperties(std::vector<std::pair<std::string, std::string>> properties, std::vector<std::string> propertyNameMatches, std::regex regularExpression);
		static std::vector<std::string> getColumnNames(cmsHANDLE handle);
		static std::vector<std::string> readSampleNames(cmsHANDLE handle, int numSamples, const std::vector<std::string> & columnNames);
		static std::vector<double> readSampleData(cmsHANDLE handle, int numSamples, const std::vector<std::string> & columnNames, std::regex regularExpression, int numColumns);
		static std::vector<double> readSpectralSampleData(cmsHANDLE handle, int numSamples, const std::vector<std::string> & columnNames, int & waveLengthStart, int & waveLengthEnd);
		static int getColumnIndexByNames(const std::vector<std::string> & columnNames, std::vector<std::string> columnNameMatches);


		// Some low level custom string manipulation functions
		static std::vector<std::string> tokenize(const std::string & input, const std::string & delimiters);
		static std::vector<std::string> tokenizeProperty(const std::string & input, const std::string & delimiters, const std::string & ignorePairsOf);
		static std::string join(const std::vector<std::string> & strings, char seperator);
		static std::string toLower(std::string str);
		static std::string stripQuotes(const std::string & str);
		static bool startsWith(const std::string & str, const std::string & start);
	};
}