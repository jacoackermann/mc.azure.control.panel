#include "Stdafx.h"
#include "Lcms.h"

namespace LCMSBinding {
	void LCMS::Initialize() {
		if (initialized)
			return;
		initErrorHandler();
		initialized = true;
	}

	bool LCMS::HasError() {
		return errorMessage != nullptr;
	}

	String ^ LCMS::GetError() {
		String ^ err = HasError() ? errorMessage : "";
		errorMessage = nullptr;
		return err;
	}

	void LCMS::SetError(String ^ error) {
		if (errorMessage != nullptr)
			errorMessage = errorMessage + Environment::NewLine + error;
		else
			errorMessage = error;
	}

	void LCMS::initErrorHandler() {
		lcmsErrorHandlerDelegate ^ fp = gcnew lcmsErrorHandlerDelegate(lcmsErrorHandler);
		gch = GCHandle::Alloc(fp);
		IntPtr ptr = Marshal::GetFunctionPointerForDelegate(fp);
		cmsLogErrorHandlerFunction callback = static_cast<cmsLogErrorHandlerFunction>(ptr.ToPointer());
		cmsSetLogErrorHandler(callback);
	}

	void LCMS::lcmsErrorHandler(cmsContext contextId, cmsUInt32Number errorCode, const char * message) {
		String ^ msg = gcnew String(message);
		System::Console::WriteLine("LCMS error: {0}", msg);
		SetError(msg);
	}
}