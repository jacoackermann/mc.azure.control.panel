#pragma once

#include "CGATSFile.h"


namespace LCMSBinding {
	public ref class IccProfile {
	public:
		enum struct ProfileOpenMode {
			Read = 0,
			Write = 1
		};

		void Close();
		bool SaveProfile(String ^ fullpath);

		static IccProfile ^ OpenFromStream(Stream ^ stream);
		static IccProfile ^ CreateLab2Profile(Illuminant illuminant, Observer observer);
		
		//Is this a CMYK profile? Does it have no other spotcolorants?
		bool IsCMYK();

		//Does this profile contain colorants for CMYK? If IsCMYK() this will also return true.
		bool ContainsCMYK();
		//Does this profile contain colorants for Extended OGV?
		bool ContainsOGV();

		array<int> ^ GetCMYKChannels();
		array<int> ^ GetOGVChannels();

		//Overrides the auto detection of CMYK channels
		void SetCMYKChannels(array<int> ^ channelsCMYK);

		//Returns the total number of colorants defined for this profile
		int GetNumberOfColorants();

		//Returns the names of the colorants as stored in the profile
		//These names may be empty, espcecially true if the profile IsCMYK
		array<String ^> ^ GetColorantNames();

		//Returns the Lab values of all colorants
		array<Lab ^> ^ GetColorantsAsLab();

		//Returns the indices of the non-CMYK colorants, useful for getting non-CMYK data from transforms
		//if !ContainsCMYK, this will return all colorants, even if some subset of CMYK is found
		array<int> ^ GetNonCMYKColorantIndices();

		Illuminant ^ GetIlluminant();

		Observer ^ GetObserver();

		bool ContainsCGATSFile();
		CGATSFile ^ GetContainedCGATSFile();

	internal:
		//this constructor should only be used by CreateLab2Profile
		IccProfile(cmsHPROFILE handle);
		//LCMS does not read the XYZ data for the illuminant of the PCS, so we supply it separately
		IccProfile(cmsHPROFILE handle, TriStimulusReference ^ triStimulusReference);

		//this returns the entry in indicesCMYK, valid if ContainsCMYK is true
		//this is useful for TransformCmykToLab in the case the profile is not pure CMYK
		int getCMYKChannel(int cmykIndex);


		cmsHPROFILE profileHandle;

	protected:
		//these fields are initialized in detectColorSpace, to be called from the Profile constructor
		//the color space used for input in the profile
		cmsColorSpaceSignature colorSpace;
		//the number of colorants in the colorSpace
		int numberOfColorants;
		//the XYZ values of the illuminant of the PCS, converted to a standard illuminant observer
		TriStimulusReference ^ triStimulusReference;
		//if a colorant is recognised as CMYK, store the corresponding index here
		//-1 is stored if the colorant is not found
		//this array is not used for a CMYK colorspace
		array<int> ^ indicesCMYK;
		//same as above for OGV, only if numberOfColorants >= 7
		array<int> ^ indicesOGV;
		//the Lab values for each colorant, useful for computing dotgain patches
		//this array is not used for a CMYK colorspace
		array<Lab ^> ^ solidColors;
		//the names of the colorants, taken from the profile
		array<String ^> ^ colorantNames;

		//if found, the contents of a CIED tag with a cgats file
		List<array<unsigned char> ^> ^ cgatsFiles;

		//detect the color space and find CMYK colortants
		bool detectColorSpace();
		//used in detectColorSpace, returns the cielab colors of the solids
		array<Lab ^> ^ getSolidColors();
		//reads the profile colorants tag to get names for colorants, should be called after detectColorSpace
		bool readColorantNamesTag();
		//looks for a cgats file in a CIED tag and stores it in cgatsFile if found
		bool findCGATSFiles();

		//LCMS does not read the XYZ data for the illuminant of the PCS
		//these functions read it manually from the file or the file's memory data
		static XYZTuple ^ readIlluminantXYZFromFile(const char * filename);
		static XYZTuple ^ readIlluminantXYZ(void * profileMemory);
		static double cms15Fixed16toDouble(cmsS15Fixed16Number fix32);

	private:
		IccProfile();
		virtual ~IccProfile();
	};
}