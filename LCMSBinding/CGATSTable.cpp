#include "Stdafx.h"
#include "CGATSTable.h"

using namespace Colorware::Core::Enums;
using namespace Colorware::Core::Extensions;

namespace LCMSBinding
{
	String ^ CGATSTable::GetDescription() {
		return description;
	}
	bool CGATSTable::HasIlluminant() {
		return hasIlluminant;
	}
	Illuminant CGATSTable::GetIlluminant() {
		return illuminant;
	}
	bool CGATSTable::HasObserver() {
		return hasObserver;
	}
	Observer CGATSTable::GetObserver() {
		return observer;
	}
	bool CGATSTable::HasMCondition() {
		return hasMCondition;
	}
	MCondition CGATSTable::GetMCondition() {
		return mcondition;
	}

	CGATSTable::TableValidity CGATSTable::GetValidity() {
		return validity;
	}


	int CGATSTable::GetNumberOfValidColorants() {
		int validCount;
		auto validColumns = getValidColumns(validCount);
		return validCount;
	}

	List<String ^> ^ CGATSTable::GetColorantNames() {
		if (colorantNames == nullptr) {
			return nullptr;
		}
		int validCount;
		auto validColumns = getValidColumns(validCount);
		List<String^> ^ validNames = gcnew List<String^>(validCount);
		for (int i = 0; i < validCount; i++) {
			validNames->Add(nullptr);
		}
		for (int iFrom = 0, iTo = 0; iFrom < numberOfColorants; iFrom++) {
			if (validColumns[iFrom]) {
				validNames[iTo] = colorantNames[iFrom];
				iTo++;
			}
		}
		return validNames;
	}


	int CGATSTable::GetNumberOfSamples() {
		return numberOfSamples;
	}

	List<String ^> ^ CGATSTable::GetSampleNames() {
		return sampleNames;
	}

	List<double> ^ CGATSTable::GetColorantData() {
		return colorantData;
	}

	List<double> ^ CGATSTable::GetLabData() {
		return LabData;
	}

	List<double> ^ CGATSTable::GetXYZData() {
		return XYZData;
	}

	List<double> ^ CGATSTable::GetxyYData() {
		return xyYData;
	}

	List<double> ^ CGATSTable::GetSpectralData() {
		return spectralData;
	}

	int CGATSTable::GetSpectralWaveLengthStart() {
		return spectralWaveLengthStart;
	}

	int CGATSTable::GetSpectralWaveLengthEnd() {
		return spectralWaveLengthEnd;
	}

	void CGATSTable::ComputeColorants(SpectralMeasurementConditions^ measurementConditions) {
		if (validity == TableValidity::ValidSpotOnly) {
			colorantColors = gcnew List<Lab ^>(numberOfSamples);
			for (int i = 0; i < numberOfSamples; ++i)
				colorantColors->Add(SampleAsLab(i, measurementConditions));
			// no automatic CMYK detection
			indicesCMYK = gcnew List<int>(4);
			for (int i = 0; i < 4; ++i)
				indicesCMYK->Add(-1);
			indicesOGV = gcnew List<int>(3);
			for (int i = 0; i < 3; ++i)
				indicesOGV->Add(-1);
			return;
		}
		if (validity != TableValidity::ValidColorants)
			return;

		colorantColors = gcnew List<Lab ^>(numberOfColorants);
		for (int i = 0; i < numberOfColorants; ++i)
			colorantColors->Add(SampleAsLab(colorantSolidSampleIndices[i], measurementConditions));

		// Try to find CMYK channels
		indicesCMYK = gcnew List<int>(4);
		for (int i = 0; i < 4; ++i)
			indicesCMYK->Add(-1);
		indicesOGV = gcnew List<int>(3);
		for (int i = 0; i < 3; ++i)
			indicesOGV->Add(-1);
		if (numberOfColorants >= 4) {
			for (int i = 0; i < numberOfColorants; ++i) {
				if (colorantColors[i] == nullptr) {
					continue;
				}
				ColorantReferenceData::ColorantReference colorantReference = ColorantReferenceData::GetColorantReference(colorantColors[i]->AsLch);

				if (colorantReference == ColorantReferenceData::ColorantReference::Cyan)
					indicesCMYK[0] = i;
				else if (colorantReference == ColorantReferenceData::ColorantReference::Magenta)
					indicesCMYK[1] = i;
				else if (colorantReference == ColorantReferenceData::ColorantReference::Yellow)
					indicesCMYK[2] = i;
				else if (colorantReference == ColorantReferenceData::ColorantReference::Key)
					indicesCMYK[3] = i;
				if (numberOfColorants >= 7) {
					if (colorantReference == ColorantReferenceData::ColorantReference::Orange)
						indicesOGV[0] = i;
					else if (colorantReference == ColorantReferenceData::ColorantReference::Green)
						indicesOGV[1] = i;
					else if (colorantReference == ColorantReferenceData::ColorantReference::Violet)
						indicesOGV[2] = i;
				}
			}
		}
	}

	List<Lab ^> ^ CGATSTable::GetColorantColors() {
		int validCount;
		auto validColumns = getValidColumns(validCount);
		List<Lab ^> ^ validLabs = gcnew List<Lab^>(validCount);
		for (int i = 0; i < validCount; i++) {
			validLabs->Add(nullptr);
		}
		for (int iFrom = 0, iTo = 0; iFrom < numberOfColorants; iFrom++) {
			if (validColumns[iFrom]) {
				validLabs[iTo] = colorantColors[iFrom];
				iTo++;
			}
		}
		return validLabs;
	}

	List<List<double> ^> ^ CGATSTable::GetColorantSpectra() {
		int validCount;
		auto validColumns = getValidColumns(validCount);
		List<List<double> ^> ^ spectra = gcnew List<List<double> ^>();
		for (int i = 0; i < numberOfColorants; ++i) {
			if (validColumns[i]) {
				spectra->Add(GetSampleSpectrum(colorantSolidSampleIndices[i]));
			}
		}
		return spectra;
	}
	
	List<double> ^ CGATSTable::GetSubstrateSpectrum() {
		for (int i = 0; i < numberOfSamples; ++i) {
			bool isSubstrate = true;
			for (int j = 0; j < numberOfColorants; ++j) {
				if (colorantData[numberOfColorants * i + j] > 0.0) {
					isSubstrate = false;
					break;
				}
			}
			if (isSubstrate) {
				return GetSampleSpectrum(i);
				break;
			}
		}

		return nullptr;
	}

	List<int> ^ CGATSTable::GetIndicesCMYK() {
		return indicesCMYK;
	}

	List<int> ^ CGATSTable::GetIndicesOGV() {
		return indicesOGV;
	}

	Lab ^ CGATSTable::SampleAsLab(int sampleIndex, SpectralMeasurementConditions^ measurementConditions) {
		if(sampleIndex < 0) {
			return nullptr;
		}
		if (spectralData != nullptr) {
			int numSpectralSamples = spectralData->Count / numberOfSamples;
			return (gcnew Spectrum(spectralData->GetRange(numSpectralSamples * sampleIndex, numSpectralSamples), spectralWaveLengthStart, spectralWaveLengthEnd))->ToLab(measurementConditions);
		}
		if (LabData != nullptr)
			return gcnew Lab(LabData[sampleIndex * 3 + 0], LabData[sampleIndex * 3 + 1], LabData[sampleIndex * 3 + 2], measurementConditions);
		if (XYZData != nullptr)
			return (gcnew XYZ(XYZData[sampleIndex * 3 + 0], XYZData[sampleIndex * 3 + 1], XYZData[sampleIndex * 3 + 2], measurementConditions))->AsLab;
		if (xyYData != nullptr)
			return xyYToXYZ(XYZData[sampleIndex * 3 + 0], XYZData[sampleIndex * 3 + 1], XYZData[sampleIndex * 3 + 2], measurementConditions)->AsLab;
		return nullptr;
	}

	List<double> ^ CGATSTable::GetSampleColorants(int sampleIndex) {
		auto data = colorantData->GetRange(numberOfColorants * sampleIndex, numberOfColorants);
		int validCount;
		auto validColumns = getValidColumns(validCount);
		List<double> ^ validColorants = gcnew List<double>(validCount);
		for (int i = 0; i < validCount; i++) {
			validColorants->Add(0);
		}
		for (int iFrom = 0, iTo = 0; iFrom < numberOfColorants; iFrom++) {
			if (validColumns[iFrom]) {
				validColorants[iTo] = data[iFrom];
				iTo++;
			}
		}
		return validColorants;
	}

	int CGATSTable::externalToInternalIndex(int externalIndex) {
		int validCount;
		auto validColumns = getValidColumns(validCount);
		int internalIndex = 0;
		int currentFoundValidColumns = 0;
		for (int i = 0; i < numberOfColorants; i++) {
			if (validColumns[i]) {
				if (currentFoundValidColumns == externalIndex) {
					return i;
				}
				currentFoundValidColumns++;
			}
		}
		return externalIndex; // This will probably fail if we get here?
	}

	int CGATSTable::GetNumSpectralSamples() {
		return spectralData->Count / numberOfSamples;
	}

	List<double> ^ CGATSTable::GetSampleSpectrum(int sampleIndex) {
		int numSpectralSamples = GetNumSpectralSamples();
		return spectralData->GetRange(numSpectralSamples * sampleIndex, numSpectralSamples);
	}

	double CGATSTable::GetSampleSpectrumPoint(int sampleIndex, int spectralIndex) {
		int numSpectralSamples = GetNumSpectralSamples();
		return spectralData[numSpectralSamples * sampleIndex + spectralIndex];
	}


	void CGATSTable::findValidityAndSolidColorSamples() {
		if (LabData == nullptr && XYZData == nullptr && xyYData == nullptr && spectralData == nullptr) {
			validity = TableValidity::NoColorData;
			return;
		}
		if (numberOfColorants == 0) {
			validity = TableValidity::ValidSpotOnly;
			return;
		}

		// Find solids and use them to add the colors of each colorant
		colorantSolidSampleIndices = gcnew List<int>(numberOfColorants);
		for (int i = 0; i < numberOfColorants; ++i)
			colorantSolidSampleIndices->Add(-1);
		for (int i = 0; i < numberOfSamples; ++i) {
			int solidIndex;
			if (isSampleSolid(i, solidIndex)) {
				colorantSolidSampleIndices[solidIndex] = i;
			}
		}

		for(int i = 0; i < numberOfColorants; i++) {
			if (colorantSolidSampleIndices[i] != -1) {
				validity = TableValidity::ValidColorants;
				return;
			}
		}

		validity = TableValidity::NoSolids;
		/*
		// Missing one or more solids
		for (int i = 0; i < numberOfColorants; ++i) {
			if (colorantSolidSampleIndices[i] == -1) {
				validity = TableValidity::NoSolids;
				return;
			}
		}

		validity = TableValidity::ValidColorants;
		*/
	}

	bool CGATSTable::isSampleSolid(int sampleIndex, int & solidIndex) {
		int sampleDataIndex = sampleIndex * numberOfColorants;
		bool solidFound = false;
		for (int i = 0; i < numberOfColorants; ++i) {
			if (colorantData[sampleDataIndex + i] == 100.0) {
				if (solidFound)
					return false;
				else {
					solidFound = true;
					solidIndex = i;
				}
			}
			else if (colorantData[sampleDataIndex + i] != 0.0)
				return false;
		}
		return solidFound;
	}

	List<bool> ^ CGATSTable::getValidColumns(int & validCount) {
		List<bool> ^ result = gcnew List<bool>(numberOfColorants);
		validCount = 0;
		for (int i = 0; i < numberOfColorants; i++) {
			result->Add(false);
		}
		for (int i = 0; i < numberOfSamples; ++i) {
			int solidIndex;
			if (isSampleSolid(i, solidIndex)) {
				if (!result[solidIndex]) {
					result[solidIndex] = true;
					validCount++;
				}
			}
		}
		return result;
	}

	XYZ ^ CGATSTable::xyYToXYZ(double x, double y, double Y, SpectralMeasurementConditions^ measurementConditions) {
		if (NumberExtensions::NearlyEquals(y, 0.0)) {
			return gcnew XYZ(0.0, 0.0, 0.0, measurementConditions);
		}
		double outX = x * Y / y;
		double outY = Y;
		double outZ = (1.0 - x - y) * Y / y;
		return gcnew XYZ(outX, outY, outZ, measurementConditions);
	}
}