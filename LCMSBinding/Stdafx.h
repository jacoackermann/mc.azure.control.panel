// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#include <lcms2.h>

#include <vector>
#include <string>
#include <algorithm>
#include <codecvt>
#include <regex>
#include <fstream>

using namespace System;
using namespace System::IO;
using namespace System::Collections::Generic;
using namespace Runtime::InteropServices;
using namespace Colorware::Core::Enums;
using namespace Colorware::Core::Color;
using namespace Colorware::Core::Color::Support;
using namespace Colorware::Core::Globalization;
