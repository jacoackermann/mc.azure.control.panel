#pragma once


namespace LCMSBinding {
	public ref class CGATSTable {
	public:
		enum class TableValidity {
			ValidColorants,
			ValidSpotOnly,
			NoSolids,
			NoColorData
		};

		// Common properties
		String ^ GetDescription();
		bool HasIlluminant();
		Illuminant GetIlluminant();
		bool HasObserver();
		Observer GetObserver();
		bool HasMCondition();
		MCondition GetMCondition();

		TableValidity GetValidity();

		// The number of colorants used in ColorantData, often 4 (CMYK) or 0, but can be anything
		// If ColorantData is not null, will always be non zero
		int GetNumberOfValidColorants();
		// The names of the colorants used in ColorantData
		// This is optional, even if ColorantData is given
		// If not null, ColorantData will be given and the size will be NumberOfColorants
		List<String ^> ^ GetColorantNames();

		// The number of samples in the table
		int GetNumberOfSamples();
		// The names of the samples
		// Size is NumberOfSamples
		List<String ^> ^ GetSampleNames();
		// Colorant data for all samples
		// Size is NumberOfSamples * numberOfInks
		List<double> ^ GetColorantData();
		// L*a*b* data for all samples
		// Size is NumberOfSamples * 3
		List<double> ^ GetLabData();
		// XYZ data for all samples
		// Size is NumberOfSamples * 3
		List<double> ^ GetXYZData();
		// xyY data for all samples
		// Size is NumberOfSamples * 3
		List<double> ^ GetxyYData();
		// Spectral data for all samples, from SpectralWaveLengthStart to SpectralWaveLengthEnd in equal steps
		// Size is NumberOfSamples * #, where # is (SpectralWaveLengthEnd - SpectralWaveLengthStart) / Step + 1
		// with Step being the step size in nm
		List<double> ^ GetSpectralData();
		// The first wavelength for spectral data samples in nm, often ~380-400nm
		// If SpectralData is not null, will always be non 0
		int GetSpectralWaveLengthStart();
		// The last wavelength for spectral data samples in nm, often ~700-730nm
		// If SpectralData is not null, will always be non 0
		int GetSpectralWaveLengthEnd();

		void ComputeColorants(SpectralMeasurementConditions^ measurementConditions);

		// Returns the colors of the solid patches found in ColorantData
		// This function is available after ComputeColorants() is called if the table is Valid
		// If no ColorantData is given, these are the converted colors of each sample
		// These colors are only an indication, use the data below to create patches
		List<Lab ^> ^  GetColorantColors();
		// Returns the spectra of the solid patches found in ColorantData
		List<List<double> ^> ^ GetColorantSpectra();
		// Returns the spectrum of the first substrate patch found
		List<double> ^ GetSubstrateSpectrum();
		// Returns the list of automatically detected CMYK colorant indices, 4+ colorants only
		List<int> ^ GetIndicesCMYK();
		// Returns the list of automatically detected OGV colorant indices, 7+ colorants only
		List<int> ^ GetIndicesOGV();

		Lab ^ SampleAsLab(int sampleIndex, SpectralMeasurementConditions^ measurementConditions);
		List<double> ^ GetSampleColorants(int sampleIndex);
		int GetNumSpectralSamples();
		List<double> ^ GetSampleSpectrum(int sampleIndex);
		double GetSampleSpectrumPoint(int sampleIndex, int spectralIndex);

	internal:
		// All data except for numSamples is optional
		// Optional data is simply null

		String ^ description;
		bool hasIlluminant;
		Illuminant illuminant;
		bool hasObserver;
		Observer observer;
		bool hasMCondition;
		MCondition mcondition;

		TableValidity validity;

		int numberOfColorants;
		List<String ^> ^ colorantNames;

		int numberOfSamples;
		List<String ^> ^ sampleNames;
		List<double> ^ colorantData;
		List<double> ^ LabData;
		List<double> ^ XYZData;
		List<double> ^ xyYData;
		List<double> ^ spectralData;
		int spectralWaveLengthStart;
		int spectralWaveLengthEnd;

		List<int> ^ colorantSolidSampleIndices;
		List<Lab ^> ^ colorantColors;
		List<int> ^ indicesCMYK;
		List<int> ^ indicesOGV;

		void findValidityAndSolidColorSamples();
		bool isSampleSolid(int sampleIndex, int & solidIndex);
		XYZ ^ xyYToXYZ(double x, double y, double Y, SpectralMeasurementConditions^ measurementConditions);
		List<bool> ^ getValidColumns(int & validCount);
		int externalToInternalIndex(int externalIndex);
	};
}