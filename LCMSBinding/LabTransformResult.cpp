#include "Stdafx.h"
#include "LabTransformResult.h"

namespace LCMSBinding {
	LabTransformResult::LabTransformResult(double l, double a, double b) {
		this->l = l;
		this->a = a;
		this->b = b;
	}
}