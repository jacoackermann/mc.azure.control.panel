#pragma once

#include "CGATSParser.h"
#include "CGATSTable.h"
#include "LCMS.h"


namespace LCMSBinding {
	public ref class CGATSFile
	{
	public:
		static CGATSFile ^ CreateFromStream(Stream ^ stream);

		int GetNumberOfTables();
		CGATSTable ^ GetTable(int num);

		// Combines this file with the given file by adding in missing data per table
		// This will only have an effect if the files contain the same number of tables
		// and each table matches the number of samples
		// Any data already defined in this file is not overwritten
		void CombineWith(CGATSFile ^ file);

	internal:
		CGATSFile();

		void create(void * memory, int memorySize);

		CGATSTable ^ loadTable(cmsContext context, const std::vector<std::string> & tableLines, size_t tableStart);
		CGATSTable ^ loadTable(cmsHANDLE handle, const std::vector<std::string> & tableLines);

		void combineTable(CGATSTable ^ table1, CGATSTable ^ table2);

		List<CGATSTable ^> ^ tables;
		String ^ errorMessage;
	};
}