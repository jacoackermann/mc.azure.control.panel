#pragma once

#include "IccProfile.h"


namespace LCMSBinding {
	public ref class IccTransform {
	public:
		LabTuple ^ CMYKToLab(CMYK ^ cmyk);
		LabTuple ^ CMYKToLab(double c, double m, double y, double k);
		LabTuple ^ SpotToLab(int spotIndex, double percentage);

		static IccTransform ^ CreateTransformation(IccProfile ^ inputProfile);

	protected:
		cmsHTRANSFORM transformHandle;
		cmsUInt32Number inputFormat, outputFormat;
		IccProfile ^ inputProfile, ^ outputProfile;

		IccTransform(IccProfile ^ inputProfile, cmsUInt32Number inputFormat);
		IccTransform(IccProfile ^ inputProfile, cmsUInt32Number inputFormat, IccProfile ^ outputProfile, cmsUInt32Number outputFormat);
		~IccTransform();

		bool Create();
		void Close();
	};
}