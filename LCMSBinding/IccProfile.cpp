#include "Stdafx.h"
#include "IccProfile.h"

using namespace Colorware::Core::Color;
using namespace Colorware::Core::Color::Support;

namespace LCMSBinding {
	IccProfile::IccProfile() {
		profileHandle = nullptr;
	}
	IccProfile::IccProfile(cmsHPROFILE handle) {
		profileHandle = handle;
		this->triStimulusReference = TriStimulusReferenceData::D50_2;
	}
	IccProfile::IccProfile(cmsHPROFILE handle, TriStimulusReference ^ triStimulusReference) {
		profileHandle = handle;
		this->triStimulusReference = triStimulusReference;
	}
	IccProfile::~IccProfile() {
		Close();
	}

	void IccProfile::Close() {
		if( profileHandle ) {
			cmsCloseProfile(profileHandle);
		}
		profileHandle = nullptr;
	}

	bool IccProfile::SaveProfile(String ^ fullpath) {
		IntPtr ptr = Marshal::StringToHGlobalAnsi(fullpath);
		const char * zFullpath = static_cast<const char *>(ptr.ToPointer());
		int result = cmsSaveProfileToFile(profileHandle, zFullpath);
		Marshal::FreeHGlobal(ptr);
		return result > 0;
	}

	IccProfile ^ IccProfile::OpenFromStream(Stream ^ stream) {
		int streamLength = (int)stream->Length;
		array<unsigned char> ^ streamData = gcnew array<unsigned char>(streamLength);
		stream->Read(streamData, 0, streamLength);
		IntPtr streamDataPtr = Marshal::AllocHGlobal(streamLength);
		Marshal::Copy(streamData, 0, streamDataPtr, streamLength);
		cmsHPROFILE profileHandle = cmsOpenProfileFromMem(streamDataPtr.ToPointer(), streamLength);
		XYZTuple ^ illuminantXYZ = readIlluminantXYZ(streamDataPtr.ToPointer());
		Marshal::FreeHGlobal(streamDataPtr);

		if (profileHandle) {
			IccProfile ^ profile = gcnew IccProfile(profileHandle, TriStimulusReferenceData::FindMatchingTristimulusReferenceFor(illuminantXYZ));
			if (!profile->detectColorSpace())
				return nullptr;
			if (!profile->readColorantNamesTag())
				return nullptr;
			if (!profile->findCGATSFiles())
				return nullptr;
			return profile;
		}

		return nullptr;
	}

	IccProfile ^ IccProfile::CreateLab2Profile(Illuminant illuminant, Observer observer) {
		TriStimulusReference ^ triStimRef = TriStimulusReferenceData::GetTristimulusReferenceDataFor(illuminant, observer);
		if( ! triStimRef ) {
			return nullptr;
		}
		// Transform to xyY:
		cmsCIExyY xyY;
		cmsCIEXYZ xyz;
		xyz.X = triStimRef->XRef / 100.0;
		xyz.Y = triStimRef->YRef / 100.0;
		xyz.Z = triStimRef->ZRef / 100.0;
		cmsXYZ2xyY(&xyY, &xyz);

		cmsHPROFILE profileHandle = cmsCreateLab2Profile(&xyY);
		if( profileHandle )
			return gcnew IccProfile(profileHandle);
		return nullptr;
	}


	
	bool IccProfile::IsCMYK() {
		return (colorSpace == cmsSigCmykData);
	}

	bool IccProfile::ContainsCMYK() {
		//CMYK contains CMYK
		if (IsCMYK())
			return true;
		//if one colorant fails, there is no CMYK
		for (int i = 0; i < 4; ++i) {
			if (indicesCMYK[i] == -1)
				return false;
		}
		return true;
	}

	bool IccProfile::ContainsOGV() {
		//if one colorant fails, there is no OGV
		for (int i = 0; i < 3; ++i) {
			if (indicesOGV[i] == -1)
				return false;
		}
		return true;
	}

	array<int> ^ IccProfile::GetCMYKChannels() {
		return indicesCMYK;
	}
	array<int> ^ IccProfile::GetOGVChannels() {
		return indicesOGV;
	}

	void IccProfile::SetCMYKChannels(array<int> ^ channelsCMYK) {
		indicesCMYK = channelsCMYK;
	}

	int IccProfile::GetNumberOfColorants() {
		return numberOfColorants;
	}

	array<String ^> ^ IccProfile::GetColorantNames() {
		return colorantNames;
	}

	array<Lab ^> ^ IccProfile::GetColorantsAsLab() {
		return solidColors;
	}

	array<int> ^ IccProfile::GetNonCMYKColorantIndices() {
		List<int> ^ spotIndices = gcnew List<int>(0);
		if (IsCMYK())
			return spotIndices->ToArray();
		else if (!ContainsCMYK()) {
			for (int i = 0; i < numberOfColorants; ++i)
				spotIndices->Add(i);
		}
		for (int i = 0; i < numberOfColorants; ++i) {
			bool isInCMYK = false;
			for (int j = 0; j < 4; ++j) {
				if (indicesCMYK[j] == i) {
					isInCMYK = true;
					break;
				}
			}
			if (!isInCMYK)
				spotIndices->Add(i);
		}
		return spotIndices->ToArray();
	}

	Illuminant ^ IccProfile::GetIlluminant() {
		return triStimulusReference->Illuminant;
	}

	Observer ^ IccProfile::GetObserver() {
		return triStimulusReference->Observer;
	}


	int IccProfile::getCMYKChannel(int cmykIndex) {
		return indicesCMYK[cmykIndex];
	}

	bool IccProfile::ContainsCGATSFile() {
		return cgatsFiles->Count > 0;
	}

	CGATSFile ^ IccProfile::GetContainedCGATSFile() {
		if (!ContainsCGATSFile())
			return nullptr;
		CGATSFile ^ result = nullptr;
		for (int i = 0; i < cgatsFiles->Count; ++i) {
			CGATSFile ^ cgatsFile = gcnew CGATSFile();
			std::vector<char> cgatsFileData;
			cgatsFileData.resize(cgatsFiles[i]->Length);
			Marshal::Copy(cgatsFiles[i], 0, IntPtr((void *)cgatsFileData.data()), cgatsFiles[i]->Length);
			cgatsFile->create(cgatsFileData.data(), cgatsFileData.size());
			if (result == nullptr)
				result = cgatsFile;
			else
				result->CombineWith(cgatsFile);
		}
		return result;
	}


	bool IccProfile::detectColorSpace() {
		colorSpace = cmsGetColorSpace(profileHandle);
		numberOfColorants = cmsChannelsOf(colorSpace);
		indicesCMYK = gcnew array<int>(4);
		for (int i = 0; i < 4; ++i)
			indicesCMYK[i] = -1;
		indicesOGV = gcnew array<int>(3);
		for (int i = 0; i < 3; ++i)
			indicesOGV[i] = -1;
		solidColors = getSolidColors();
		if (solidColors == nullptr)
			return false;

		//try to find CMYK
		if (!IsCMYK() && numberOfColorants >= 4) {
			for (int i = 0; i < solidColors->Length; ++i) {
				ColorantReferenceData::ColorantReference colorantReference = ColorantReferenceData::GetColorantReference(solidColors[i]->AsLch);

				if (colorantReference == ColorantReferenceData::ColorantReference::Cyan)
					indicesCMYK[0] = i;
				else if (colorantReference == ColorantReferenceData::ColorantReference::Magenta)
					indicesCMYK[1] = i;
				else if (colorantReference == ColorantReferenceData::ColorantReference::Yellow)
					indicesCMYK[2] = i;
				else if (colorantReference == ColorantReferenceData::ColorantReference::Key)
					indicesCMYK[3] = i;
				if (numberOfColorants >= 7) {
					if (colorantReference == ColorantReferenceData::ColorantReference::Orange)
						indicesOGV[0] = i;
					if (colorantReference == ColorantReferenceData::ColorantReference::Green)
						indicesOGV[1] = i;
					if (colorantReference == ColorantReferenceData::ColorantReference::Violet)
						indicesOGV[2] = i;
				}
			}
		}

		return true;
	}

	array<Lab ^> ^ IccProfile::getSolidColors() {
		//setup an empty profile to output to Lab
		cmsHPROFILE nullProfile = cmsCreateLab2Profile(nullptr);

		//transform for numberOfColorants to Lab
		cmsHTRANSFORM hTransform;
		if (IsCMYK())
			hTransform = cmsCreateTransform(profileHandle, TYPE_CMYK_DBL,
				nullProfile, TYPE_Lab_DBL, INTENT_ABSOLUTE_COLORIMETRIC, 0);
		else
			hTransform = cmsCreateTransform(profileHandle, (FLOAT_SH(1) | COLORSPACE_SH(PT_MCH1 + numberOfColorants - 1) | CHANNELS_SH(numberOfColorants) | BYTES_SH(0)),
				nullProfile, TYPE_Lab_DBL, INTENT_ABSOLUTE_COLORIMETRIC, 0);

		if (hTransform == nullptr) {
			LCMS::SetError(gcnew String("Unable to define transform"));
			return nullptr;
		}

		//prepare solid tone input for all channels
		std::vector<double> input;
		input.resize(numberOfColorants * numberOfColorants, 0.0);
		for (int i = 0; i < numberOfColorants; ++i)
			input[i * numberOfColorants + i] = 100.0;

		//output in cielab coords for all channels
		std::vector<double> output_cielab;
		output_cielab.resize(3 * numberOfColorants);

		cmsDoTransform(hTransform, input.data(), output_cielab.data(), numberOfColorants);

		//copy the internal result to output
		array<Lab ^> ^ result = gcnew array<Lab ^>(numberOfColorants);
		for (int i = 0; i < numberOfColorants; ++i)
			result[i] = gcnew Lab(output_cielab[3 * i + 0], output_cielab[3 * i + 1], output_cielab[3 * i + 2], gcnew SpectralMeasurementConditions(Illuminant::D50, Observer::TwoDeg, MCondition::M0));

		return result;
	}

	bool IccProfile::readColorantNamesTag() {
		if (!cmsIsTag(profileHandle, cmsSigColorantTableTag))
			return true;

		colorantNames = gcnew array<String ^>(numberOfColorants);
		for (int i = 0; i < numberOfColorants; ++i)
			colorantNames[i] = gcnew String("");

		cmsNAMEDCOLORLIST * colorlist = (cmsNAMEDCOLORLIST *)cmsReadTag(profileHandle, cmsSigColorantTableTag);
		if (colorlist == nullptr)
			return true;

		char name[256];
		char prefix[33];
		char suffix[33];
		cmsUInt16Number pcs[3];
		cmsUInt16Number colorant[16];

		int numColors = cmsNamedColorCount(colorlist);
		for (int i = 0; i < numColors && i < numberOfColorants; ++i) {
			cmsNamedColorInfo(colorlist, i, name, prefix, suffix, pcs, colorant);
			colorantNames[i] = gcnew String(name);
		}

		return true;
	}

	bool IccProfile::findCGATSFiles() {
		// While there is a good chance the profile contains multiple CGATS files in different tags
		// we are only interested in the kind stored in the 'DevD' and 'CIED' tag
		cmsTagSignature Signatures[2] = { (cmsTagSignature)0x44657644, //'DevD'
			(cmsTagSignature)0x43494544 }; //'CIED'

		cgatsFiles = gcnew List<array<unsigned char> ^>();

		for (int i = 0; i < 2; ++i) {
			if (!cmsIsTag(profileHandle, Signatures[i]))
				continue;

			cmsInt32Number tagDataSize = cmsReadRawTag(profileHandle, Signatures[i], nullptr, 0);
			if (tagDataSize <= 9)
				return true;

			std::string tagRawData;
			tagRawData.resize(tagDataSize);
			cmsReadRawTag(profileHandle, Signatures[i], (void *)tagRawData.data(), tagDataSize);
			// Data sanity check: does this tag contain a text block?
			if (tagRawData.substr(0, 4) == "text") {
				// Skip "text" and 4 null characters and omit the trailing null character
				array<unsigned char> ^ cgatsFile = gcnew array<unsigned char>(tagDataSize - 9);
				Marshal::Copy((IntPtr)(void *)(tagRawData.data() + 8), cgatsFile, 0, tagDataSize - 9);
				cgatsFiles->Add(cgatsFile);
			}
		}

		return true;
	}


	XYZTuple ^ IccProfile::readIlluminantXYZFromFile(const char * filename) {
		std::ifstream file(filename, std::ios::binary);
		file.seekg(0, std::ios::end);
		std::vector<char> profileMemory((size_t)file.tellg());
		file.seekg(0, std::ios::beg);
		file.read(profileMemory.data(), profileMemory.size());
		file.close();
		return IccProfile::readIlluminantXYZ(profileMemory.data());
	}
	XYZTuple ^ IccProfile::readIlluminantXYZ(void * profileMemory) {
		cmsICCHeader * header = (cmsICCHeader *)profileMemory;
		return gcnew XYZTuple(IccProfile::cms15Fixed16toDouble(header->illuminant.X) * 100.0,
			IccProfile::cms15Fixed16toDouble(header->illuminant.Y) * 100.0,
			IccProfile::cms15Fixed16toDouble(header->illuminant.Z) * 100.0
		);
	}

	double IccProfile::cms15Fixed16toDouble(cmsS15Fixed16Number fix32) {
		//convert big endian to native endian
		unsigned char * fix32bytes = (unsigned char *)&fix32;
		fix32 = ((int)fix32bytes[3] << 0) | ((int)fix32bytes[2] << 8) | ((int)fix32bytes[1] << 16) | ((int)fix32bytes[0] << 24);

		//the following is a copy from _cms15Fixed16toDouble in LittleCMS
		cmsFloat64Number floater, sign, mid;
		int Whole, FracPart;

		sign = (fix32 < 0 ? -1 : 1);
		fix32 = abs(fix32);

		Whole = (cmsUInt16Number)(fix32 >> 16) & 0xffff;
		FracPart = (cmsUInt16Number)(fix32 & 0xffff);

		mid = (cmsFloat64Number)FracPart / 65536.0;
		floater = (cmsFloat64Number)Whole + mid;

		return sign * floater;
	}
}