#pragma once

using namespace System;

namespace LCMSBinding {
	public ref class LabTransformResult {
	public:
		double l, a, b;
		LabTransformResult(double l, double a, double b);
	};
}
