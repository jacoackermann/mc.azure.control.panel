#pragma once

using namespace System;

namespace LCMSBinding {
	public ref class TestClass {
	public:
		int Add(int a, int b);
	};
}