#include "stdafx.h"
#include "CGATSFile.h"


namespace LCMSBinding {
	CGATSFile ^ CGATSFile::CreateFromStream(Stream ^ stream)
	{
		CGATSFile ^ cgatsFile = gcnew CGATSFile();

		int streamLength = (int)stream->Length;
		array<unsigned char> ^ streamData = gcnew array<unsigned char>(streamLength);
		stream->Read(streamData, 0, streamLength);
		IntPtr streamDataPtr = Marshal::AllocHGlobal(streamLength);
		Marshal::Copy(streamData, 0, streamDataPtr, streamLength);

		cgatsFile->create(streamDataPtr.ToPointer(), streamLength);

		Marshal::FreeHGlobal(streamDataPtr);

		if (cgatsFile->GetNumberOfTables() == 0)
			return nullptr;
		return cgatsFile;
	}


	int CGATSFile::GetNumberOfTables() {
		return tables->Count;
	}
	CGATSTable ^ CGATSFile::GetTable(int num) {
		if (num >= 0 && num < tables->Count)
			return tables[num];
		return nullptr;
	}

	void CGATSFile::CombineWith(CGATSFile ^ file) {
		if (GetNumberOfTables() != file->GetNumberOfTables())
			return;
		for (int i = 0; i < GetNumberOfTables(); ++i)
			if (tables[i]->GetNumberOfSamples() != file->tables[i]->GetNumberOfSamples())
				return;

		for (int i = 0; i < GetNumberOfTables(); ++i)
			combineTable(tables[i], file->tables[i]);
	}


	CGATSFile::CGATSFile() {
	}


	void CGATSFile::create(void * memory, int memorySize) {
		tables = gcnew List<CGATSTable ^>();

		// Context must be created once to store internal data in LCMS
		cmsContext context = cmsCreateContext(nullptr, nullptr);

		// We subdivide the file into lines and separate it into tables manually
		// This allows the omission of often encountered blocks such as BEGIN_DATA_EMISSION / END_DATA_EMISSION
		// LCMS cannot really handle that data and will instead find multiple tables
		std::string fileString((char *)memory, memorySize);
		std::vector<std::string> fileLines;
		fileLines = CGATSParser::tokenize(fileString, "\n\r");
		// Remove any leading and trailing white space
		for (std::string & fileLine : fileLines) {
			if (fileLine.find_first_not_of(" \t\n\r") != std::string::npos)
				fileLine = fileLine.substr(fileLine.find_first_not_of(" \t\n\r"), fileLine.find_last_not_of(" \t\n\r") + 1);
			else
				fileLine = "";
		}
		// Remove empty lines
		fileLines.erase(std::remove(fileLines.begin(), fileLines.end(), ""), fileLines.end());

		// Get the first table start and end
		size_t tableStart = 0;
		size_t tableEnd = std::find(fileLines.begin(), fileLines.end(), "END_DATA") - fileLines.begin() + 1;
		if (tableEnd > fileLines.size())
			LCMS::SetError(L"Invalid or incomplete CGATS file. Cannot find the end of the data.");

		while (tableEnd <= fileLines.size()) {
			// Get the lines and joined text for this table
			std::vector<std::string> tableLines(fileLines.begin() + tableStart, fileLines.begin() + tableEnd);

			CGATSTable ^ table = loadTable(context, tableLines, tableStart);
			if (table)
				tables->Add(table);

			// Continue to next table
			tableStart = tableEnd;
			if (tableStart >= fileLines.size())
				break;
			// Check if the file contains a second unsupported data block
			if (CGATSParser::startsWith(fileLines[tableStart], "BEGIN_DATA")) {
				std::string lookFor = "END_DATA" + fileLines[tableStart].substr(std::string("BEGIN_DATA").size());
				tableStart = std::find(fileLines.begin() + tableStart, fileLines.end(), lookFor) - fileLines.begin() + 1;
			}
			if (tableStart >= fileLines.size())
				break;
			tableEnd = std::find(fileLines.begin() + tableStart, fileLines.end(), "END_DATA") - fileLines.begin() + 1;
		}

		// Cleanup
		cmsDeleteContext(context);
	}



	CGATSTable ^ CGATSFile::loadTable(cmsContext context, const std::vector<std::string> & tableLines, size_t tableStart) {
		// Parse the table data, without the properties, through LCMS
		auto numberOfFieldsLine = tableLines.end();
		auto numberOfSetsLine = tableLines.end();
		for (auto line = tableLines.begin(); line != tableLines.end(); ++line) {
			if (line->substr(0, 16) == "NUMBER_OF_FIELDS") {
				numberOfFieldsLine = line;
				if (numberOfSetsLine != tableLines.end())
					break;
			}
			else if (line->substr(0, 14) == "NUMBER_OF_SETS") {
				numberOfSetsLine = line;
				if (numberOfFieldsLine != tableLines.end())
					break;
			}
		}

		bool valid = true;

		for (auto line = tableLines.begin(); line != tableLines.end(); ++line) {
			if (line->substr(0, 14) == "NUMBER_OF_SETS") {
				numberOfSetsLine = line;
				break;
			}
		}

		auto beginDataFormat = std::find(tableLines.begin(), tableLines.end(), "BEGIN_DATA_FORMAT");

		// check manually whether the number of fields is correct (common mistake)
		auto dataFormatStart = beginDataFormat + 1;
		auto endDataFormat = std::find(dataFormatStart, tableLines.end(), "END_DATA_FORMAT");
		if (beginDataFormat != tableLines.end() && endDataFormat != tableLines.end() && numberOfFieldsLine != tableLines.end() && numberOfSetsLine != tableLines.end()) {
			int numFields = 0;
			for (auto it = dataFormatStart; it != endDataFormat; ++it) {
				numFields += CGATSParser::tokenize(*it, " \t").size();
			}
			int numFieldsSpecified = atoi(CGATSParser::tokenize(*numberOfFieldsLine, " \t").rbegin()->c_str());
			if (numFields != numFieldsSpecified) {
				int lineNumber = (int)tableStart + (numberOfFieldsLine - tableLines.begin()) + 1;
				LCMS::SetError(String::Format(LanguageManager::_(L"RM.ReferenceImport.Cgats.NumberOfFieldsError"), lineNumber));
				valid = false;
			}
		}
		else {
			if (beginDataFormat == tableLines.end())
				LCMS::SetError(LanguageManager::_(L"RM.ReferenceImport.Cgats.MissingBeginDataFormat"));
			else if (endDataFormat == tableLines.end())
				LCMS::SetError(LanguageManager::_(L"RM.ReferenceImport.Cgats.MissingEndDataFormat"));
			if (numberOfFieldsLine == tableLines.end())
				LCMS::SetError(LanguageManager::_(L"RM.ReferenceImport.Cgats.MissingNumberOfFields"));
			if (numberOfSetsLine == tableLines.end())
				LCMS::SetError(LanguageManager::_(L"RM.ReferenceImport.Cgats.MissingNumberOfSets"));
			valid = false;
		}

		if (valid) {
			std::vector<std::string> tableDataLines;
			tableDataLines.push_back(*numberOfFieldsLine);
				if(numberOfSetsLine - beginDataFormat < 0)
				{
					// NUMBER_OF_SETS field is before the BEGIN_DATA_FORMAT_FIELD, which means it
					// would dissapear as the current algorithm appends everything after
					// BEGIN_DATA_FORMAT_FIELD. LCMS needs this data, so we append it here.
					tableDataLines.push_back(*numberOfSetsLine);
				}
			tableDataLines.insert(tableDataLines.end(), beginDataFormat, tableLines.end());
			std::string tableDataText = CGATSParser::join(tableDataLines, '\n');
			cmsHANDLE handle = cmsIT8LoadFromMem(context, (void *)tableDataText.c_str(), (cmsUInt32Number)tableDataText.size());

			// If LCMS correctly parsed the data
			if (handle) {
				// Add table
				CGATSTable ^ table = loadTable(handle, tableLines);

				cmsIT8Free(handle);

				if (table)
					return table;
			}
		}

		return nullptr;
	}

	CGATSTable ^ CGATSFile::loadTable(cmsHANDLE handle, const std::vector<std::string> & tableLines) {
		CGATSTable ^ table = gcnew CGATSTable();

		int numSamples = (int)cmsIT8GetPropertyDbl(handle, "NUMBER_OF_SETS");
		if (numSamples == 0)
			return nullptr;

		std::vector<std::pair<std::string, std::string>> properties = CGATSParser::getAllProperties(tableLines);
		std::string description = CGATSParser::findPropertyString(properties, { "DESCRIPTOR", "FILE_DESCRIPTOR" }, std::regex(".*"));
		std::string illuminant = CGATSParser::findPropertyString(properties, { "ILLUMINATION_NAME", "Illumination", "Illuminant" }, std::regex("([A-C])|((D|d)[0-9]{2})|(F[0-9]{1,2})"));
		std::string observer = CGATSParser::findPropertyString(properties, { "OBSERVER_ANGLE", "Observer", "ObserverAngle" }, std::regex("[0-9]+.*"));
		if (!observer.empty())
			observer.substr(0, observer.find_first_not_of("0123456789"));
		std::vector<std::string> inkNames = CGATSParser::findPropertyStrings(properties, { "InkName" }, std::regex(".*"));
		if (inkNames.empty())
			inkNames = CGATSParser::findPropertyStrings(properties, { "PROCESSCOLOR_ID", "SPOT_ID" }, std::regex(".*"));

		std::vector<std::string> columnNames = CGATSParser::getColumnNames(handle);
		std::vector<std::string> sampleNames = CGATSParser::readSampleNames(handle, numSamples, columnNames);
		std::vector<double> inkData = CGATSParser::readSampleData(handle, numSamples, columnNames, std::regex("(CMYK_.)|(PC[0-9]{1,2}_[0-9]{1,2})|(SPOT_[0-9]{1,2})|([0-9]{1,2}CLR_[0-9]{1,2})"), -1);
		int numberOfInks = inkData.size() / numSamples;
		std::vector<double> labData = CGATSParser::readSampleData(handle, numSamples, columnNames, std::regex("(LAB_L)|(LAB_A)|(LAB_B)"), 3);
		std::vector<double> xyzData = CGATSParser::readSampleData(handle, numSamples, columnNames, std::regex("(XYZ_X)|(XYZ_Y)|(XYZ_Z)"), 3);
		std::vector<double> xyyData = CGATSParser::readSampleData(handle, numSamples, columnNames, std::regex("(XYY_X)|(XYY_Y)|(XYY_CAPY)"), 3);
		int spectralWaveLengthStart = 0;
		int spectralWaveLengthEnd = 0;
		std::vector<double> spectralData = CGATSParser::readSpectralSampleData(handle, numSamples, columnNames, spectralWaveLengthStart, spectralWaveLengthEnd);



		// Convert to managed data
		table->description = gcnew String(description.c_str());
		if (Enum::TryParse(gcnew String(illuminant.c_str()), table->illuminant))
			table->hasIlluminant = true;
		else
			table->hasIlluminant = false;
		table->hasObserver = true;
		if (observer == "2")
			table->observer = Observer::TwoDeg;
		else if (observer == "10")
			table->observer = Observer::TenDeg;
		else
			table->hasObserver = false;
		table->hasMCondition = false;

		if (sampleNames.size() > 0) {
			table->sampleNames = gcnew List<String ^>((int)sampleNames.size());
			for (std::string & sampleName : sampleNames)
				table->sampleNames->Add(gcnew String(sampleName.c_str()));
		}
		if (!inkNames.empty() && inkNames.size() == numberOfInks) {
			table->colorantNames = gcnew List<String ^>((int)inkNames.size());
			for (std::string & inkName : inkNames)
				table->colorantNames->Add(gcnew String(inkName.c_str()));
		}
		if (numberOfInks > 0) {
			table->colorantData = gcnew List<double>((int)inkData.size());
			for (double & data : inkData)
				table->colorantData->Add(data);
			table->numberOfColorants = numberOfInks;
		}
		else
			table->numberOfColorants = 0;

		table->numberOfSamples = numSamples;
		if (labData.size() > 0) {
			table->LabData = gcnew List<double>((int)labData.size());
			for (double & data : labData)
				table->LabData->Add(data);
		}
		if (xyzData.size() > 0) {
			table->XYZData = gcnew List<double>((int)xyzData.size());
			for (double & data : xyzData)
				table->XYZData->Add(data);
		}
		if (xyyData.size() > 0) {
			table->xyYData = gcnew List<double>((int)xyyData.size());
			for (double & data : xyyData)
				table->xyYData->Add(data);
		}
		if (spectralData.size() > 0) {
			table->spectralData = gcnew List<double>((int)spectralData.size());
			for (double & data : spectralData)
				table->spectralData->Add(data);
			table->spectralWaveLengthStart = spectralWaveLengthStart;
			table->spectralWaveLengthEnd = spectralWaveLengthEnd;
		}

		table->findValidityAndSolidColorSamples();

		return table;
	}

	void CGATSFile::combineTable(CGATSTable ^ table1, CGATSTable ^ table2) {
		if (table1->colorantData == nullptr && table2->colorantData != nullptr)
			table1->colorantData = table2->colorantData;
		if (table1->LabData == nullptr && table2->LabData != nullptr)
			table1->LabData = table2->LabData;
		if (table1->XYZData == nullptr && table2->XYZData != nullptr)
			table1->XYZData = table2->XYZData;
		if (table1->xyYData == nullptr && table2->xyYData != nullptr)
			table1->xyYData = table2->xyYData;
		if (table1->spectralData == nullptr && table2->spectralData != nullptr) {
			table1->spectralData = table2->spectralData;
			table1->spectralWaveLengthStart = table2->spectralWaveLengthStart;
			table1->spectralWaveLengthEnd = table2->spectralWaveLengthEnd;
		}

		table1->findValidityAndSolidColorSamples();
	}
}