﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace mc.azure.controlpanel.Models {
    public class DbMigrationModel {
        public List<DatabaseInfo> Databases { get; set; }

        [Required(ErrorMessage = "Select at least one database from the list of available databases.")]
        public int[] SelectedDatabases { get; set; }

        [Required]
        [EmailAddress]
        [DisplayName("Support Email Recipients")]
        public string SupportEmailRecipients { get; set; }

        [Required(ErrorMessage = "You have to tick the Confirm box to submit this request.")]
        [DisplayName("Tick to confirm!")]
        public bool ConfirmSelection { get; set; }

        public DbMigrationModel() { }

        public DbMigrationModel(List<DatabaseInfo> databases) {
            Databases = databases;
        }
    }

    public class DatabaseInfo {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}