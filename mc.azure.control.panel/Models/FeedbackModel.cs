﻿namespace mc.azure.controlpanel.Models {
    public class FeedbackModel {
        public string Heading { get; set; }
        public string Message { get; set; }
    }
}