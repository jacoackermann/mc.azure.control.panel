﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;

using JetBrains.Annotations;

using mc.azure.sdk.wrappers.Interfaces;
using mc.azure.sdk.wrappers.Models;

namespace mc.azure.controlpanel.Controllers {
#if !DEBUG
    [Authorize]
#endif
    public class SqlServerFirewallRulesController : Controller {
        private readonly string ipRuleErrorKey = "IpRuleError";
        private readonly IAzureResourceService azureResourceService;

        //---------------------------------------------------------------------

        public SqlServerFirewallRulesController([NotNull] IAzureResourceService azureResourceService) {
            this.azureResourceService =
                azureResourceService ?? throw new ArgumentNullException(nameof(azureResourceService));
        }

        //---------------------------------------------------------------------

        // GET: ManageIps
        public async Task<ActionResult> Index() {
            var firewallRules = await azureResourceService.GetFirewallRulesAsync();
            var model = new FirewallRuleCollectionModel {
                ExistingFirewallRules = firewallRules,
                CurrentClientIp = Request.UserHostAddress,
                Feedback = (string)TempData[ipRuleErrorKey]
            };
            return View(model);
        }

        //---------------------------------------------------------------------

        [HttpPost]
        public async Task<ActionResult> AddFirewallRule(FirewallRuleCollectionModel model) {
            await azureResourceService.AddFirewallRuleForIpAsync(model);
            TempData[ipRuleErrorKey] = azureResourceService.Feedback;

            // TODO - error handling!

            return RedirectToAction("Index");
        }

        //---------------------------------------------------------------------
    }
}