﻿using System;
using System.Web.Mvc;

using Colorware.Core.Azure.Enums;
using Colorware.Core.Logging;

using JetBrains.Annotations;

using mc.azure.sdk.wrappers.Interfaces;
using mc.azure.sdk.wrappers.Models;

namespace mc.azure.controlpanel.Controllers {
#if !DEBUG
    [Authorize]
#endif
    public class ViewCustomerController : Controller {
        private readonly ILog log;
        private readonly IAzureResourceService azureResourceService;

        //---------------------------------------------------------------------

        public ViewCustomerController([NotNull] IAzureResourceService azureResourceService,
                                      [NotNull] ILog log) {
            this.log = log ?? throw new ArgumentNullException(nameof(log));
            this.azureResourceService =
                azureResourceService ?? throw new ArgumentNullException(nameof(azureResourceService));
        }

        //---------------------------------------------------------------------

        // GET: ViewClient
        public ActionResult Index() {
            var model = new CustomerViewModel {
                CustomerInfos = azureResourceService.GetCustomerInfos(CustomerInfoSelectOption.All)
            };

            return View(model);
        }

        //---------------------------------------------------------------------
    }
}