﻿using System.Web.Mvc;

namespace mc.azure.controlpanel.Controllers {
    public class FeedbackController : Controller {
        // GET: Feedback
        public ActionResult Index(string message) {
            return View((object)message);
        }
    }
}