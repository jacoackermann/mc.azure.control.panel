﻿using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;

using Colorware.Core.Azure.Enums;

using Hangfire;

using JetBrains.Annotations;

using mc.azure.controlpanel.Models;
using mc.azure.sdk.wrappers.Interfaces;
using mc.azure.utilities.IO;

namespace mc.azure.controlpanel.Controllers {
    public class MigrationsController : Controller {
        private readonly IAzureResourceService azureResourceService;

        //---------------------------------------------------------------------

        public MigrationsController([NotNull] IAzureResourceService azureResourceService) {
            this.azureResourceService =
                azureResourceService ?? throw new ArgumentNullException(nameof(azureResourceService));
        }

        //---------------------------------------------------------------------

        // GET: Migrations
        public ActionResult Index() {
            // create model (containing list of DBs)
            var databases = azureResourceService.GetCustomerInfos(CustomerInfoSelectOption.Active)
                                                .Select(
                                                    x => new DatabaseInfo {
                                                        Id = x.CustomerId,
                                                        Name = x.DatabaseName
                                                    });

            var dbMigrationModel = new DbMigrationModel(databases.ToList());

            return View(dbMigrationModel);
        }

        //---------------------------------------------------------------------

        [HttpPost]
        public ActionResult Index(DbMigrationModel model) {
            if (!ModelState.IsValid) {
                // TODO - why is it necessary to reset these model values here?

                model.Databases = azureResourceService.GetCustomerInfos(CustomerInfoSelectOption.Active)
                                                      .Select(
                                                          x => new DatabaseInfo {
                                                              Id = x.CustomerId,
                                                              Name = x.DatabaseName
                                                          }).ToList();
                model.ConfirmSelection = false;
                return View(model);
            }

            StringBuilder feedback = new StringBuilder();
            if (model.SelectedDatabases != null) {
                var selectedCustomers = azureResourceService.GetCustomerInfos(CustomerInfoSelectOption.Active)
                                                            .Select(x => x)
                                                            .Where(x => model.SelectedDatabases.Contains(
                                                                       x.CustomerId))
                                                            .ToList();

                var sitePath = DirectoryUtils.GetSiteDir(System.Web.HttpContext.Current.Server.MapPath("/"));
                BackgroundJob.Enqueue<IAzureResourceService>(
                    x => x.MigrateDatabases(selectedCustomers, sitePath, model.SupportEmailRecipients));

                feedback.AppendLine("Migrations requested for the following databases:")
                        .AppendLine()
                        .AppendLine(string.Join(Environment.NewLine,
                                                selectedCustomers.Select(x => x.DatabaseName).ToArray()))
                        .AppendLine()
                        .AppendLine($"Migrations report will be sent to '{model.SupportEmailRecipients}'");
            }
            else {
                feedback.Append("No databases selected!");
            }

            return RedirectToAction("Index", "Feedback", new {message = feedback.ToString()});
        }

        //---------------------------------------------------------------------
    }
}