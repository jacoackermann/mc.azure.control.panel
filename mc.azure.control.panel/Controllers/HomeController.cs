﻿using System.Web.Mvc;

namespace mc.azure.controlpanel.Controllers {
#if !DEBUG
    [Authorize]
#endif
    public class HomeController : Controller {
        public ActionResult Index() {
            return View();
        }

        //---------------------------------------------------------------------

        public ActionResult About() {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        //---------------------------------------------------------------------
    }
}