﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

using Colorware.Core.Azure.Interfaces;
using Colorware.Core.Azure.Messaging.EventGrid;
using Colorware.Core.Azure.Messaging.EventGrid.Events;

using JetBrains.Annotations;

namespace mc.azure.controlpanel.Controllers {
    public class MaintenanceController : Controller {
        private readonly IEventGridManager eventGridManager;

        public MaintenanceController([NotNull] IEventGridManager eventGridManager) {
            this.eventGridManager = eventGridManager ?? throw new ArgumentNullException(nameof(eventGridManager));
        }

        //---------------------------------------------------------------------

        public ActionResult Index() {
            return View();
        }

        //---------------------------------------------------------------------

        public async Task<ActionResult> ActivateMaintenanceMode() {
            var events = new List<BaseEvent>();
            events.Add(new ServerMaintenanceEvent(true));

            // TODO - get http result and check if event was sent successfully...
            var result = await eventGridManager.Send(events);

            return RedirectToAction("Index", "Feedback", new {message = $"Server Maintenance Mode Activated event sent.  Message:  {result}"});
        }

        //---------------------------------------------------------------------

        public async Task<ActionResult> DeactivateMaintenanceMode() {
            var events = new List<BaseEvent>();
            events.Add(new ServerMaintenanceEvent(false));

            // TODO - get http result and check if event was sent successfully...
            var result = await eventGridManager.Send(events);

            return RedirectToAction("Index", "Feedback", new {message = $"Server Maintenance Mode Deactivated event sent.  Message:  {result}" });
        }

        //---------------------------------------------------------------------
    }
}