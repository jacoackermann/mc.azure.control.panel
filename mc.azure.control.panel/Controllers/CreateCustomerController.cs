﻿using System;
using System.Text;
using System.Web.Mvc;

using Hangfire;

using JetBrains.Annotations;

using mc.azure.sdk.wrappers.Interfaces;
using mc.azure.sdk.wrappers.Models;
using mc.azure.utilities.IO;

namespace mc.azure.controlpanel.Controllers {
#if !DEBUG
    [Authorize]
#endif
    public class CreateCustomerController : Controller {
        private readonly string basicCustomerKey = "basicCustomerInfo";

        private readonly IAzureResourceService azureResourceService;

        //---------------------------------------------------------------------

        public CreateCustomerController([NotNull] IAzureResourceService azureResourceService) {
            this.azureResourceService =
                azureResourceService ?? throw new ArgumentNullException(nameof(azureResourceService));
        }

        //---------------------------------------------------------------------

        [HttpGet]
        public ActionResult Index() {
            return View(getBasicCustomerModel());
        }

        //---------------------------------------------------------------------

        [HttpPost]
        public ActionResult Index(BasicCustomerModel model) {
            if (!ModelState.IsValid) {
                return View(model);
            }

            if (!canCreateCustomerInfrastructure(model, out var actionResult)) {
                return actionResult;
            }

            updateBasicCustomerModel(model);

            return RedirectToAction("Confirm");
        }

        //---------------------------------------------------------------------

        [HttpGet]
        public ActionResult Confirm() {
            var basicCustomerInfo = getBasicCustomerModel();

            if (string.IsNullOrWhiteSpace(basicCustomerInfo.Name)) {
                return RedirectToAction("Index");
            }

            var createCustomerInfo = new CustomerCreateModel(basicCustomerInfo);

            createCustomerInfo.IsNewCustomer = azureResourceService.IsNewCustomer(createCustomerInfo);

            if (!createCustomerInfo.IsNewCustomer) {
                createCustomerInfo.DbBackupAvailable =
                    azureResourceService.IsCustomerDbBackupAvailable(createCustomerInfo);
            }

            return View(createCustomerInfo);
        }

        //---------------------------------------------------------------------

        [HttpPost]
        public ActionResult Confirm(CustomerCreateModel model) {
            if (!ModelState.IsValid) {
                return View(model);
            }

            if (!canCreateCustomerInfrastructure(model, out var actionResult)) {
                return actionResult;
            }

            var sitePath = DirectoryUtils.GetSiteDir(System.Web.HttpContext.Current.Server.MapPath("/"));
            BackgroundJob.Enqueue<IAzureResourceService>(
                x => x.CreateCustomerAsync(model, sitePath, System.Web.HttpContext.Current.Request.UserHostAddress));

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Creating resources for customer '{model.CustomerFullName}'.");

            sb.AppendLine(
                model.SupportEmailRecipients == null
                    ? "No support email address was provided.  You can go to 'View client info' to track the progress of your request."
                    : $"An email will be sent to '{model.SupportEmailRecipients}' to notify you of the outcome.");

            removeBasicCustomerModel();

            return RedirectToAction("Index", "Feedback", new {message = sb.ToString()});
        }

        //---------------------------------------------------------------------

        private bool canCreateCustomerInfrastructure(BasicCustomerModel model, out ActionResult redirect) {
            var customerCreateInfo = new CustomerCreateModel(model);
            return canCreateCustomerInfrastructure(customerCreateInfo, out redirect);
        }

        //---------------------------------------------------------------------

        private bool canCreateCustomerInfrastructure(CustomerCreateModel model, out ActionResult redirect) {
            if (azureResourceService.CanCreateCustomerInfrastructure(model, out var message)) {
                redirect = null;
                return true;
            }

            removeBasicCustomerModel();

            var errorMsg = $"Cannot create infrastructure for customer '{model.CustomerFullName}'."
                           + $"{Environment.NewLine}"
                           + $"Reason:  {message}";

            redirect = RedirectToAction("Index", "Feedback", new {message = errorMsg});
            return false;
        }

        //---------------------------------------------------------------------

        private BasicCustomerModel getBasicCustomerModel() {
            if (Session[basicCustomerKey] == null) {
                Session[basicCustomerKey] = new BasicCustomerModel();
            }

            return (BasicCustomerModel)Session[basicCustomerKey];
        }

        //---------------------------------------------------------------------

        private void updateBasicCustomerModel(BasicCustomerModel model) {
            // TODO - is this safe?
            // is there some condition to test for before updating?
            Session[basicCustomerKey] = model;
        }

        //---------------------------------------------------------------------

        private void removeBasicCustomerModel() {
            Session.Remove(basicCustomerKey);
        }

        //---------------------------------------------------------------------
    }
}