﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;

using Colorware.Core.Azure.Enums;

using Hangfire;

using JetBrains.Annotations;

using mc.azure.sdk.wrappers.Interfaces;
using mc.azure.sdk.wrappers.Models;

//-- it is not yet possible to create a backup (.bak) of an azure sql DB:
//-- https://stackoverflow.com/questions/43447391/how-to-create-bak-file-from-azure-sql-db

// looks like one can create a BACPAC file...
// https://stackoverflow.com/questions/45728248/how-could-i-restore-an-azure-database-using-a-bacpac-during-an-automated-test-bu
// https://stackoverflow.com/questions/45033940/azure-database-export-with-c-sharp
// https://stackoverflow.com/questions/41058003/programmatically-export-azure-sql-as-dacpac-to-blob-storage

namespace mc.azure.controlpanel.Controllers {
#if !DEBUG
    [Authorize]
#endif
    public class DeactivateCustomerController : Controller {
        private readonly IAzureResourceService azureResourceService;

        public DeactivateCustomerController([NotNull] IAzureResourceService azureResourceService) {
            this.azureResourceService =
                azureResourceService ?? throw new ArgumentNullException(nameof(azureResourceService));
        }

        //---------------------------------------------------------------------

        // GET: DeactivateClient
        public ActionResult Index() {
            var model = new CustomerDeactivateModel {
                CustomerInfos = azureResourceService.GetCustomerInfos(CustomerInfoSelectOption.Active)
            };

            List<SelectListItem> customerListItems = new List<SelectListItem>();
            model.CustomerInfos.ForEach(
                x => customerListItems.Add(new SelectListItem {
                    Text = x.CustomerFullName,
                    Value = x.CustomerFullName
                }));

            ViewBag.CustomerInfo = customerListItems;

            return View(model);
        }

        //---------------------------------------------------------------------

        [HttpPost]
        public ActionResult DeactivateCustomer(string customerInfo, string supportEmail) {
            var customerInfos = azureResourceService.GetCustomerInfos(CustomerInfoSelectOption.Active);
            var customerToDeactivate = customerInfos.Find(x => x.CustomerFullName == customerInfo);

            if (customerToDeactivate == null) {
                TempData["ErrorMessage"] = $"Cannot find the requested customer - '{customerInfo}'";
                return RedirectToAction("DeactivateCustomerError");
            }

            BackgroundJob.Enqueue<IAzureResourceService>(
                x => x.DeactivateCustomerAsync(customerToDeactivate, supportEmail));

            RouteValueDictionary dictionary = new RouteValueDictionary {
                {
                    "message",
                    $"Deactivating resources for customer '{customerToDeactivate.CustomerFullName}'.{Environment.NewLine}An email will be sent to '{supportEmail}' to notify you of the outcome."
                }
            };
            return RedirectToAction("Index", "Feedback", dictionary);
        }

        //---------------------------------------------------------------------

        public ActionResult DeactivateCustomerError() {
            var errorMsg = TempData["ErrorMessage"];

            // https://stackoverflow.com/questions/11296634/pass-a-simple-string-from-controller-to-a-view-mvc3
            return View((object)errorMsg);
        }

        //---------------------------------------------------------------------
    }
}