﻿using System.Diagnostics;
using System.Web;

using Hangfire.Annotations;
using Hangfire.Dashboard;

namespace mc.azure.controlpanel.HangFire {
    public class HangFireAuthorizationFilter : IDashboardAuthorizationFilter {
        public bool Authorize([NotNull] DashboardContext context) {
            if (Debugger.IsAttached)
                return true;

            return HttpContext.Current.User.Identity.IsAuthenticated;
        }
    }
}