﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using Autofac;
using Autofac.Integration.Mvc;

using Colorware.Core.Azure.Config;
using Colorware.Core.Azure.Interfaces;
using Colorware.Core.Azure.Messaging.EventGrid;
using Colorware.Core.Azure.Storage.Accounts;
using Colorware.Core.Logging;

using Hangfire;

using mc.azure.email.Interfaces;
using mc.azure.email.MailJet;
using mc.azure.migrations.Interfaces;
using mc.azure.migrations.Services.DataMigrationService;
using mc.azure.sdk.wrappers.Interfaces;
using mc.azure.sdk.wrappers.Services.AzureResources;
using mc.azure.sdk.wrappers.Storage.TableStorage;
using mc.azure.utilities.Azure;

namespace mc.azure.controlpanel {
    public class MvcApplication : System.Web.HttpApplication {
        protected void Application_Start() {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Dependency injection using Autofac
            var builder = new ContainerBuilder();

            // Register your MVC controllers. (MvcApplication is the name of the class in Global.asax.)
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.Register(c => new StorageAccountManager()).As<IStorageAccountManager>();
            builder.Register(c => new MailJetWrapper()).As<IEmailService>();
            builder.Register(c => new DataMigrationService()).As<IDataMigrationService>();
            builder.RegisterType<CustomerInfoManager>().As<ICustomerInfoManager>();
            builder.RegisterType<AzureResourceService>().As<IAzureResourceService>();

            builder.Register(c => new EventGridManager()).As<IEventGridManager>();

            builder.RegisterType<LogManager>().As<ILogManager>();
            builder.Register(c => LogManager.GetLogger("ControlPanel")).As<ILog>();


            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            // configure HangFire...
            GlobalConfiguration.Configuration.UseAutofacActivator(container);
            JobActivator.Current = new AutofacJobActivator(container);

            startAzureStorageEmulator();
        }

        //---------------------------------------------------------------------

        private static void startAzureStorageEmulator() {
            if (ConfigValueReader.IsProductionMode) return;

            // only attempt to start the storage emulator when NOT running in Azure

            // try and start the Azure storage emulator
            StorageEmulatorHelper.StartStorageEmulator();
        }

        //---------------------------------------------------------------------
    }
}