﻿using System;

using Colorware.Core;
using Colorware.Core.Data;
using Colorware.Core.Data.Specification;

using JetBrains.Annotations;

namespace Colorware.Models.ItemCreators {
    public class ServiceModelItemCreator<T> : IItemCreator<T> where T : IBaseModel {
        private readonly ILocalServiceModelIdGenerator localServiceModelIdGenerator;

        public ServiceModelItemCreator([NotNull] ILocalServiceModelIdGenerator localServiceModelIdGenerator) {
            if (localServiceModelIdGenerator == null) throw new ArgumentNullException("localServiceModelIdGenerator");

            this.localServiceModelIdGenerator = localServiceModelIdGenerator;
        }

        public T CreateItem() {
            var item = GlobalContainer.Current.Resolve<T>();

            item.Id = localServiceModelIdGenerator.GenerateId();

            return item;
        }
    }
}