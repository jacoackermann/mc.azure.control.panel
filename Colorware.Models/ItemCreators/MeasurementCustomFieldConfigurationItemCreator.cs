using Colorware.Core.Data;
using Colorware.Core.Data.Models;
using Colorware.Models.ServiceModels;

using JetBrains.Annotations;

namespace Colorware.Models.ItemCreators {
    [UsedImplicitly]
    public class MeasurementCustomFieldConfigurationItemCreator :
        IItemCreator<IMeasurementCustomFieldsConfigurationItem> {
        public IMeasurementCustomFieldsConfigurationItem CreateItem() {
            return new MeasurementCustomFieldsConfigurationItem();
        }
    }
}