using System;
using System.Globalization;
using System.Linq;

using Colorware.Core.Data.Models;

namespace Colorware.Models {
    /// <summary>
    /// Format:
    ///  slot ':' base64(inkZoneLock) ; [repeat for each inkzonelock]
    /// </summary>
    public static class InkZoneLockSerializer {
        public static String Serialize(InkZoneLockSet inkZoneLockSet) {
            var serializeEntries =
                inkZoneLockSet.GetInkZoneLocksBySlot().Select(s => Serialize(s.Key, s.Value)).ToArray();
            var result = String.Join(";", serializeEntries);
            return result;
        }

        public static String Serialize(int key, InkZoneLock inkZoneLock) {
            var result = key.ToString(CultureInfo.InvariantCulture);
            result += ":";

            //if(inkZoneLock.LockedZones.Any(entry => entry < 0 || entry > 255)) {
            //  throw new Exception("Invalid entry, must be between 0 and 255");
            //}
            var inkZoneByteArray = inkZoneLock.LockedZones.ConvertAll(val => (byte)val).ToArray();

            result += Convert.ToBase64String(inkZoneByteArray);
            return result;
        }
    }
}