﻿using Colorware.Core.Interface.StorageWarningChecker;
using Colorware.Core.Security;

using ProtoBuf;

namespace Colorware.Models.Security {
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class StorageWarningCheckerResultDto : IStorageWarningCheckerResultDto {
        private StorageWarningCheckerResultDto(bool resultLimitExceeded, long resultCurrentLimitInBytes,
                                               long resultActualUsedSizeInBytes,
                                               long totalAvailableSizeInBytes) {
            LimitExceeded = resultLimitExceeded;
            CurrentLimitInBytes = resultCurrentLimitInBytes;
            ActualUsedSizeInBytes = resultActualUsedSizeInBytes;
            TotalAvailableSizeInBytes = totalAvailableSizeInBytes;
        }

        // Constructor required for ProtoBuf.
        // ReSharper disable once UnusedMember.Local
        private StorageWarningCheckerResultDto() : this(false, 0, 0, 0) { }

        public long TotalAvailableSizeInBytes { get; set; }
        public long ActualUsedSizeInBytes { get; set; }
        public long CurrentLimitInBytes { get; set; }
        public bool LimitExceeded { get; set; }

        public static StorageWarningCheckerResultDto
            FromStorageWarningCheckerResult(StorageWarningCheckerResult result) {
            return new StorageWarningCheckerResultDto(result.LimitExceeded, result.CurrentLimitInBytes,
                                                      result.ActualUsedSizeInBytes, result.TotalAvailableSizeInBytes);
        }
    }
}