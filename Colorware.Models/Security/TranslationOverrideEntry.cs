﻿using System.Diagnostics;

using Colorware.Core.Security;

using JetBrains.Annotations;

using ProtoBuf;

namespace Colorware.Models.Security {
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [DebuggerDisplay("TranslationOverrideEntry({Key} => {Value})")]
    public class TranslationOverrideEntry : ITranslationOverrideEntry {
        public string Key { get; private set; }
        public string Value { get; private set; }

        public TranslationOverrideEntry() : this("", "") { }

        public TranslationOverrideEntry([NotNull] string key, [NotNull] string value) {
            Key = key;
            Value = value;
        }
    }
}