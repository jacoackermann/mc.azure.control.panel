﻿using System.Diagnostics;

using Colorware.Core.Security;

using JetBrains.Annotations;

using ProtoBuf;

namespace Colorware.Models.Security {
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [DebuggerDisplay("LoginUserProfileEntry({Key} => {Value},  IsSet: {IsSet})")]
    public class LoginUserProfileEntry : ILoginUserProfileEntry {
        [NotNull]
        public string Key { get; private set; }
        [NotNull]
        public string Value { get; private set; }
        public bool IsSet { get; private set; }

        public LoginUserProfileEntry() : this("", "", false) {
        }

        public LoginUserProfileEntry([NotNull] string key, [NotNull] string value, bool isSet) {
            Key = key;
            Value = value;
            IsSet = isSet;
        }
    }
}