﻿using System;
using System.Collections.Generic;

using Colorware.Core.Authentication;
using Colorware.Core.Security;

using ProtoBuf;

// Disable private set warning as private setters are required by ProtoBuf to work properly.
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Colorware.Models.Security {
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class LoginResult : ILoginResult {
        private LoginResult() { }

        // OK constructor
        private LoginResult(IUser loggedInUser,
                            string serverVersion,
                            string serverSystemInfo,
                            List<ILoginUserProfileEntry> userProfileEntries,
                            List<ITranslationOverrideEntry> translationOverrideEntries) {
            LoggedInUser = loggedInUser;
            ServerVersion = serverVersion;
            ServerSystemInfo = serverSystemInfo;
            UserProfileEntries = userProfileEntries;
            TranslationOverrideEntries = translationOverrideEntries;
        }

        // Error constructor
        private LoginResult(string errorMessage,
                            string serverVersion,
                            string serverSystemInfo,
                            List<ILoginUserProfileEntry> userProfileEntries,
                            List<ITranslationOverrideEntry> translationOverrideEntries) {
            LoginErrorMessage = errorMessage;
            ServerVersion = serverVersion;
            ServerSystemInfo = serverSystemInfo;
            UserProfileEntries = userProfileEntries;
            TranslationOverrideEntries = translationOverrideEntries;
        }

        /// <summary>
        /// Create a new instance indicating login failure.
        /// </summary>
        public static LoginResult CreateFailed(string errorMessage, string serverVersion, string serverSystemInfo) {
            return new LoginResult(errorMessage, serverVersion, serverSystemInfo, new List<ILoginUserProfileEntry>(),
                                   new List<ITranslationOverrideEntry>());
        }

        /// <summary>
        /// Create a new instance indicating login success.
        /// </summary>
        public static LoginResult CreateSucceeded(IUser loggedInUser,
                                                  string serverVersion,
                                                  string serverSystemInfo,
                                                  List<ILoginUserProfileEntry> userProfileEntries,
                                                  List<ITranslationOverrideEntry> translationOverrideEntries) {
            return new LoginResult(loggedInUser, serverVersion, serverSystemInfo, userProfileEntries,
                                   translationOverrideEntries);
        }

        public string LoginErrorMessage { get; }

        public IUser LoggedInUser { get; }

        public string ServerVersion { get; } = "";

        public string ServerSystemInfo { get; } = "";

        public DateTimeOffset ServerTime { get; private set; } = DateTimeOffset.Now;

        public void SetServerTimeToNow() {
            ServerTime = DateTimeOffset.Now;
        }

        public IStorageWarningCheckerResultDto HddSpaceResult { get; set; }

        public IStorageWarningCheckerResultDto DatabaseSpaceResult { get; set; }

        public List<ILoginUserProfileEntry> UserProfileEntries { get; } =
            new List<ILoginUserProfileEntry>();

        public List<ITranslationOverrideEntry> TranslationOverrideEntries { get; } =
            new List<ITranslationOverrideEntry>();
    }
}