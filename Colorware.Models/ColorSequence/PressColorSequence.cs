﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media;

using Colorware.Core.Data.Models.ColorSequence;
using Colorware.Core.Extensions;
using Colorware.Core.Functional.Option;
using Colorware.Core.Mvvm;
using Colorware.Models.ServiceModels;

using JetBrains.Annotations;

namespace Colorware.Models.ColorSequence {
    public class PressColorSequence : DefaultNotifyPropertyChanged, IPressColorSequence {
        private const string SerializationSeperator = ";";

        #region Properties
        private ObservableCollection<PressColorSequenceElement> elements;

        public ObservableCollection<PressColorSequenceElement> Elements {
            get {
                if (elements == null)
                    elements = new ObservableCollection<PressColorSequenceElement>();
                return elements;
            }
            set {
                elements = value;
                OnPropertyChanged("Elements");
            }
        }

        private int numberOfColors;

        public int NumberOfColors {
            get { return numberOfColors; }
            set {
                numberOfColors = value;
                padColors();
                OnPropertyChanged("NumberOfColors");
            }
        }
        #endregion

        public static PressColorSequence MockData {
            get {
                var seq = new PressColorSequence();
                seq.Add(0, Colors.Cyan);
                seq.Add(1, Colors.Yellow);
                seq.Add();
                seq.Add(2, Colors.Magenta);
                seq.Add();
                seq.Add(2, Colors.Black);
                seq.Add();
                seq.Add();
                return seq;
            }
        }

        public String SerializeToString() {
            var slots = Elements.Select(e => e.Slot.ToString()).ToArray();
            return String.Join(SerializationSeperator, slots);
        }

        /// <summary>
        /// Note: This doesn't contain any color information, so this might have to be set afterwards!
        /// </summary>
        /// <param name="str"></param>
        public void DeserializeFromString(String str) {
            var slots = DeserializeFromStringFlat(str);
            if (NumberOfColors < slots.Count())
                NumberOfColors = slots.Count();

            for (int i = 0; i < slots.Length; i++)
                Set(i, slots[i], Colors.Black);
        }

        [NotNull, Pure]
        public static int[] DeserializeFromStringFlat(String str) {
            if (String.IsNullOrEmpty(str))
                return new int[] {};

            var parts = str.Split(SerializationSeperator);

            var res = new int[parts.Length];
            for (int i = 0; i < parts.Length; i++) {
                var slot = -1;
                try {
                    slot = int.Parse(parts[i]);
                }
                catch {
                }
                res[i] = slot;
            }
            return res;
        }


        public void Clear() {
            Elements.Clear();
        }

        public void Add(PressColorSequenceElement element) {
            Elements.Insert(0, element);
            if (Elements.Count > NumberOfColors)
                NumberOfColors = Elements.Count;
        }

        public void Add() {
            var el = new PressColorSequenceElement {IsActive = false, Slot = ColorStripPatch.NO_SLOT_VALUE};
            Add(el);
        }

        public void Add(int slot, Color color) {
            var el = new PressColorSequenceElement {IsActive = true, Color = color, Slot = slot};
            Add(el);
        }

        public void Set(int index) {
            if (index >= 0 && index < Elements.Count) {
                var el = Elements[index];
                el.IsActive = false;
                el.Slot = ColorStripPatch.NO_SLOT_VALUE;
            }
        }

        public void Set(int index, int slot, Color color) {
            Set(index, slot, color, "Unknown");
        }

        public void Set(int index, int slot, Color color, String name) {
            if (index >= 0 && index < Elements.Count) {
                var el = Elements[index];
                el.IsActive = true;
                el.Slot = slot;
                el.IsActive = true;
                el.Color = color;
                el.Name = name;
            }
        }

        public bool IsDefined(int index) {
            if (index < 0 || index >= Elements.Count)
                return false;
            return Elements[index].IsActive;
        }


        private void RemoveLast() {
            if (Elements.Count > 0)
                Elements.RemoveAt(Elements.Count - 1);
        }


        /// <summary>
        /// Fills out (or removes) colors at the end of the element list to make sure
        /// the number of items equals NumberOfColors.
        /// </summary>
        private void padColors() {
            if (Elements.Count < NumberOfColors) {
                // Add colors.
                while (Elements.Count < NumberOfColors) {
                    Add();
                }
            }
            else {
                // Remove colors.
                while (Elements.Count > NumberOfColors) {
                    RemoveLast();
                }
            }
        }

        public Option<int> FindIndexForSlot(int slot) {
            for (int i = 0; i < Elements.Count; i++) {
                if (Elements[i].Slot == slot)
                    return i.ToOption();
            }
            return Option<int>.Nothing;
        }

        public Option<Tuple<int, PressColorSequenceElement>> FindElementForSlot(int slot) {
            for (int i = 0; i < Elements.Count; i++) {
                if (Elements[i].Slot == slot)
                    return Tuple.Create(i, Elements[i]).ToOption();
            }
            return Option<Tuple<int, PressColorSequenceElement>>.Nothing;
        }
    }
}