using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

using Colorware.Core.Color;
using Colorware.Core.Config;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Mvvm.AntiMemoryLeaks;

using JetBrains.Annotations;

namespace Colorware.Models.StripCompareInterface {
    /// <summary>
    /// Helper class which wraps a list of StripComparePatches and supplies methods and properties
    /// for getting info out of the list.
    /// </summary>
    public class StripComparePatches : IStripComparePatches, INotifyPropertyChanged {
        private readonly IReadOnlyList<IStripComparePatch> patches;
        private const double MinimumGamutOriginDistance = 20.0;

        /// <summary>
        /// List of patches belonging to this instance.
        /// </summary>
        public IReadOnlyList<IStripComparePatch> Patches => patches;

        public StripComparePatches([NotNull] IReadOnlyList<IStripComparePatch> patches) {
            if (patches == null) throw new ArgumentNullException(nameof(patches));

            this.patches = patches;
        }

        private CollectionViewSource patchesByInkZone;

        public CollectionViewSource PatchesByInkZone {
            get {
                if (patchesByInkZone != null)
                    return patchesByInkZone;
                patchesByInkZone = new CollectionViewSource {Source = Patches};
                patchesByInkZone.GroupDescriptions.Add(new PropertyGroupDescription("InkZone"));
                return patchesByInkZone;
            }
        }

        public bool InTolerance {
            get { return Patches.All(p => p.InTolerance); }
        }

        public bool HasPaperwhite {
            get { return Patches.Any(p => p.IsPaperWhite); }
        }

        public double MaximumDeltaE {
            get { return Patches.Max(p => p.DeltaE); }
        }

        public double AverageDeltaE {
            get {
                if (Patches.Count > 0)
                    return Math.Round(Patches.Average(p => p.DeltaE), 2);

                return 0.0;
            }
        }

        public double PrimariesDeltaE {
            get {
                if (SolidPatches.Any())
                    return Math.Round(SolidPatches.Average(p => p.DeltaE), 2);

                return 0.0;
            }
        }

        public double PaperWhiteDeltaE {
            get {
                var items = patches.Where(p => p.IsPaperWhite);
                if (items.Any())
                    return Math.Round(items.Average(p => p.DeltaE), 2);
                else
                    return 0.0;
            }
        }

        public double PrimariesDeltaH {
            get {
                if (SolidPatches.Any())
                    return Math.Round(SolidPatches.Average(p => p.DeltaH), 2);
                else
                    return 0.0;
            }
        }

        public double GrayBalanceDeltaH {
            get {
                var items = patches.Where(p => p.IsBalanceHighlight || p.IsBalanceMidtone || p.IsBalanceShadow);
                if (items.Any())
                    return Math.Round(items.Average(p => p.DeltaH), 2);
                else
                    return 0.0;
            }
        }

        public double CyanDeltaH {
            get {
                var items = patches.Where(p => p.Name.StartsWith("C") && p.IsSolid);
                if (items.Any())
                    return items.ElementAt(0).DeltaE;
                else
                    return 0.0;
            }
        }

        public double MagentaDeltaH {
            get {
                var items = patches.Where(p => p.Name.StartsWith("M") && p.IsSolid);
                if (items.Any())
                    return items.ElementAt(0).DeltaE;
                else
                    return 0.0;
            }
        }

        public double YellowDeltaH {
            get {
                var items = patches.Where(p => p.Name.StartsWith("Y") && p.IsSolid);
                if (items.Any())
                    return items.ElementAt(0).DeltaE;
                else
                    return 0.0;
            }
        }

        public double BlackDeltaH {
            get {
                var items = patches.Where(p => p.Name.StartsWith("K") && p.IsSolid);
                if (items.Any())
                    return items.ElementAt(0).DeltaE;
                else
                    return 0.0;
            }
        }

        private IReadOnlyList<IStripComparePatch> importantPatches;

        /// <summary>
        /// List of `important' patches.
        /// </summary>
        public IReadOnlyList<IStripComparePatch> ImportantPatches {
            get {
                return importantPatches ??
                       (importantPatches =
                            patches.Where(p => p.IsImportantPatch).OrderBy(p => p.PatchTypeSortNumber)
                                   .ToNonLeakyList());
            }
        }

        private IReadOnlyList<NamedLab> gamutSamples;

        public IReadOnlyList<NamedLab> GamutSamples {
            get {
                return gamutSamples ??
                       (gamutSamples =
                            patches.Where(p => (p.IsSolid || p.IsOverprint) && !pointIsTooCloseToOrigin(p.SampleLab))
                                   .Select(p => new NamedLab(p.SampleLab, p.Name)).ToNonLeakyList());
            }
        }


        private IReadOnlyList<NamedLab> gamutReferences;

        public IReadOnlyList<NamedLab> GamutReferences {
            get {
                return gamutReferences ??
                       (gamutReferences =
                            patches.Where(p => (p.IsSolid || p.IsOverprint) && !pointIsTooCloseToOrigin(p.RefLab))
                                   .Select(p => new NamedLab(p.RefLab, p.Name)).ToNonLeakyList());
            }
        }

        private static bool pointIsTooCloseToOrigin(Lab lab) {
            var distanceToOrigin = Math.Sqrt(lab.a * lab.a + lab.b * lab.b);
            if (distanceToOrigin <= MinimumGamutOriginDistance)
                return true;
            return false;
        }


        private IReadOnlyList<Lab> paperWhiteOffsets;

        public IReadOnlyList<Lab> PaperWhiteOffsets {
            get {
                return paperWhiteOffsets ??
                       (paperWhiteOffsets = patches.Where(p => p.IsPaperWhite)
                                                   .Select(p => p.SampleLab.RelativeOffsetOf(p.RefLab))
                                                   .ToNonLeakyList());
            }
        }


        private IReadOnlyList<Lab> balanceHightlightOffsets;

        public IReadOnlyList<Lab> BalanceHightlightOffsets {
            get {
                return balanceHightlightOffsets ??
                       (balanceHightlightOffsets = patches.Where(p => p.IsBalanceHighlight)
                                                          .Select(p => p.SampleLab.RelativeOffsetOf(p.RefLab))
                                                          .ToNonLeakyList());
            }
        }

        private IReadOnlyList<Lab> balanceMidtoneOffsets;

        public IReadOnlyList<Lab> BalanceMidtoneOffsets {
            get {
                return balanceMidtoneOffsets ??
                       (balanceMidtoneOffsets = patches.Where(p => p.IsBalanceMidtone)
                                                       .Select(p => p.SampleLab.RelativeOffsetOf(p.RefLab))
                                                       .ToNonLeakyList());
            }
        }

        private IReadOnlyList<Lab> balanceShadowOffsets;

        public IReadOnlyList<Lab> BalanceShadowOffsets {
            get {
                return balanceShadowOffsets ??
                       (balanceShadowOffsets = patches.Where(p => p.IsBalanceShadow)
                                                      .Select(p => p.SampleLab.RelativeOffsetOf(p.RefLab))
                                                      .ToNonLeakyList());
            }
        }

        private IReadOnlyList<IStripComparePatch> solidPatches;

        public IReadOnlyList<IStripComparePatch> SolidPatches {
            get { return solidPatches ?? (solidPatches = patches.Where(p => p.IsSolid).ToNonLeakyList()); }
        }

        private IReadOnlyList<IStripCompareDotgainList> showableDotgains;

        public IReadOnlyList<IStripCompareDotgainList> ShowableDotgains {
            get {
                if (showableDotgains != null)
                    return showableDotgains;
                showableDotgains = getShowableDotgains();

                return showableDotgains;
            }
            set { showableDotgains = value; }
        }

        private int[] getDisplayableDotgains() {
            var items = GlobalConfigKeys.GamutDotgainGraphEntries.Value;
            var result = new List<int>();
            try {
                var groups = items.Split(new[] {','});
                foreach (var group in groups) {
                    var parts = group.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries);
                    var partsAsInt = parts.Select(p => int.Parse(p, CultureInfo.InvariantCulture)).ToArray();
                    foreach (var partAsInt in partsAsInt) {
                        if (canUseDisplayableDotgain(partAsInt)) {
                            result.Add(partAsInt);
                            break;
                        }
                    }
                }

                return result.ToArray(); // Just return last one matched, or null if nothing was set.
            }
            catch (Exception) {
                return new[] {20, 25, 40, 50, 60, 75, 80};
            }
        }

        private bool canUseDisplayableDotgain(int possibleTint) {
            foreach (var patch in patches) {
                if (!patch.IsDotgain && !patch.IsDotgainOnly)
                    continue;
                if (patch.Tint == possibleTint)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Build a list containing (for each component) a StripCompareDotgainList
        /// which itself contains the patches for the given dotgain tint.
        /// </summary>
        /// <returns>The list of showable dotgains.</returns>
        private IReadOnlyList<IStripCompareDotgainList> getShowableDotgains() {
            var result = new List<IStripCompareDotgainList>();
            var byPercentage = new Dictionary<int, IStripCompareDotgainList>();
            var displayable = getDisplayableDotgains();

            foreach (var patch in patches) {
                if (!patch.IsDotgain)
                    continue;
                if (!displayable.Contains(patch.Tint))
                    continue;
                if (!byPercentage.ContainsKey(patch.Tint)) {
                    byPercentage[patch.Tint] = new StripCompareDotgainList();
                    byPercentage[patch.Tint].Percentage = patch.Tint;
                }

                byPercentage[patch.Tint].Patches.Add(patch);
            }

            foreach (var keyValues in byPercentage) {
                ((StripCompareDotgainList)keyValues.Value).OrderPatchesFollowingSolids(SolidPatches);
                result.Add(keyValues.Value);
            }

            return result.OrderBy(dg => dg.Percentage).ToNonLeakyList();
        }

        private int[] usedSlots;

        public int[] UsedSlots {
            get {
                if (usedSlots == null)
                    usedSlots = calculateUsedSlots();
                return usedSlots;
            }
        }

        private int[] calculateUsedSlots() {
            var foundSlots = new Dictionary<int, bool>();
            foreach (var patch in Patches) {
                if (patch.HasSlot1)
                    foundSlots[patch.Slot1] = true;
                if (patch.HasSlot2)
                    foundSlots[patch.Slot2] = true;
                if (patch.HasSlot3)
                    foundSlots[patch.Slot3] = true;
            }

            return foundSlots.Keys.ToArray();
        }

        public IStripComparePatch FirstPaperWhitePatch {
            get { return patches.FirstOrDefault(p => p.IsPaperWhite); }
        }

        // Anti memory leak
#pragma warning disable 0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 0067
    }
}