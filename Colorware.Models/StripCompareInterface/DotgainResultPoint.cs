using System;

using Colorware.Core.Mvvm;

namespace Colorware.Models.StripCompareInterface {
    /// <summary>
    /// Contains dotgain results for a single point. The point may be interpolated or not.
    /// </summary>
    // Inherit from INPC to prevent leaks when binding to members
    public class DotgainResultPoint : DefaultNotifyPropertyChanged {
        /// <summary>
        /// Percentage of patch.
        /// </summary>
        public double Tint { get; set; }

        /// <summary>
        /// Dotgain of reference. (Not linear)
        /// </summary>
        public double ReferenceDotgain { get; set; }

        /// <summary>
        /// Dotgain of reference. (Linear)
        /// </summary>
        public double ReferenceDotgainLinear { get; set; }

        /// <summary>
        /// Dotgain of sample. (Not linear)
        /// </summary>
        public double SampleDotgain { get; set; }

        /// <summary>
        /// Dotgain of sample. (Linear)
        /// </summary>
        public double SampleDotgainLinear { get; set; }

        /// <summary>
        /// Dotgain difference between sample and reference.
        /// </summary>
        public double DotgainDiff { get; set; }

        /// <summary>
        /// Gets or sets the base curve value for this tint, this is either linear curve or an existing curve.
        /// </summary>
        /// <value>
        /// The base curve.
        /// </value>
        public double BaseCurve { get; set; }

        /// <summary>
        /// Gets or sets the new curve value for this tint.
        /// </summary>
        /// <value>
        /// The new curve.
        /// </value>
        public double NewCurve { get; set; }

        /// <summary>
        /// Difference between old curve and new curve.
        /// </summary>
        public double CurveDelta { get; set; }


        public double Curve { get; set; }

        /// <summary>
        /// Tint of optimal ctp value.
        /// </summary>
        public double CtpX { get; set; }

        /// <summary>
        /// Dotgain of optimal ctp value.
        /// </summary>
        public double CtpY { get; set; }

        /// <summary>
        /// Ctp compensation between best found dotgain and actual dotgain.
        /// </summary>
        public double CtpCompensation {
            get { return CtpX - Tint; }
        }

        /// <summary>
        /// Value indicating the tolerance at the top of the reference (non-linear) curve. I.e. reference + tolerance.
        /// </summary>
        public double ToleranceTop { get; set; }

        /// <summary>
        /// Value indicating the tolerance at the bottom of the reference (non-linear) curve. I.e. reference - tolerance.
        /// </summary>
        public double ToleranceBottom { get; set; }

        /// <summary>
        /// Is this a real point or an interpolated one?
        /// </summary>
        public bool IsRealPoint { get; set; }

        #region Overrides of Object
        public override string ToString() {
            return String.Format("DotgainResultPoint({0}, RefDot:{1}/{2})", Tint, ReferenceDotgainLinear,
                ReferenceDotgain);
        }
        #endregion
    }
}