using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

using Colorware.Core.Data.Models.StripCompareInterface;

namespace Colorware.Models.StripCompareInterface {
    public class StripCompareDotgainList : IStripCompareDotgainList, INotifyPropertyChanged {
        public int Percentage { get; set; }

        private List<IStripComparePatch> patches;

        public List<IStripComparePatch> Patches {
            get { return patches ?? (patches = new List<IStripComparePatch>()); }
            set { patches = value; }
        }

        /// <summary>
        /// Order the current patchset to represent the ordering of the given solid patches.
        /// </summary>
        /// <param name="solidPatches">The solid patches to use as the basis of the ordering.</param>
        public void OrderPatchesFollowingSolids(IEnumerable<IStripComparePatch> solidPatches) {
            var solidsList = solidPatches.ToList();
            Patches.Sort((patch1, patch2) => {
                var index1 = solidsList.FindIndex(patch => patch.SingleSlot == patch1.SingleSlot);
                var index2 = solidsList.FindIndex(patch => patch.SingleSlot == patch2.SingleSlot);
                return index1.CompareTo(index2);
            });
        }

        // Anti memory leak
#pragma warning disable 0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 0067
    }
}