﻿using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Mvvm;

namespace Colorware.Models.StripCompareInterface {
    /// <summary>
    /// Manager class for handling a collection of DotgainComponentAverages.
    /// </summary>
    public class DotgainAverages : DefaultNotifyPropertyChanged {
        // x step value for interpolation
        private const double defaultXStep = 5.0;
        private double xStep;

        /// <summary>
        /// Calculates and contains dotgain results for all components/slots in the passed-in
        /// compare result.
        /// </summary>
        private readonly IStripCompareResult compareResult;

        #region Properties
        /// <summary>
        /// Return average results hashed by component slot.
        /// </summary>
        private Dictionary<int, DotgainComponentAverage> averagesBySlot;

        public Dictionary<int, DotgainComponentAverage> AveragesBySlot {
            get {
                if (averagesBySlot == null)
                    averagesBySlot = calculateAverages();
                return averagesBySlot;
            }
            private set {
                averagesBySlot = value;
                OnPropertyChanged("Averages");
                OnPropertyChanged("AveragesBySlot");
            }
        }

        /// <summary>
        /// Return averages in no particular order.
        /// </summary>
        private List<DotgainComponentAverage> averages;

        public List<DotgainComponentAverage> Averages {
            get {
                if (averages == null) {
                    averages = new List<DotgainComponentAverage>();
                    var avByComp = AveragesBySlot;
                    foreach (var av in avByComp) {
                        averages.Add(av.Value);
                    }
                }
                return averages;
            }
        }

        /// <summary>
        /// Indicates whether the use of G7 should be taken into account.
        /// </summary>
        public bool IsG7Calculated { get; set; }
        #endregion

        public DotgainAverages(IStripCompareResult compareResult) : this(compareResult, defaultXStep) {
        }

        public DotgainAverages(IStripCompareResult compareResult, double xStep) {
            this.compareResult = compareResult;
            this.xStep = xStep;
        }

        public DotgainAverages Copy() {
            var newAveragesBySlot = new Dictionary<int, DotgainComponentAverage>();
            foreach (var average in AveragesBySlot) {
                newAveragesBySlot[average.Key] = average.Value.Copy();
            }
            return new DotgainAverages(compareResult, xStep) {
                AveragesBySlot = newAveragesBySlot
            };
        }

        public void RefreshPoints() {
            foreach (var dotgainComponentAverage in Averages)
                dotgainComponentAverage.RefreshPoints();
        }

        private Dictionary<int, DotgainComponentAverage> calculateAverages() {
            var result = new Dictionary<int, DotgainComponentAverage>();
            var slots = compareResult.SummaryPatches.UsedSlots;
            foreach (var slot in slots) {
                var localSlot = slot;
                var patches = compareResult.SummaryPatches.Patches
                                           .Where(
                                               p =>
                                               p.IsSingleSlot && p.SingleSlot == localSlot &&
                                               (p.IsDotgain || p.IsDotgainOnly))
                                           .OrderBy(p => p.Tint)
                                           .ToList();
                if (patches.Count > 0) {
                    var solid = compareResult.SummaryPatches.Patches.FirstOrDefault(p =>
                                                                                    p.IsSolid &&
                                                                                    p.SingleSlot == localSlot);
                    var average = new DotgainComponentAverage(patches, xStep);
                    if (solid != null) {
                        average.DisplayColorOverride = solid.AsColor;
                        average.IsSpot = solid.ReferencePatch.IsSpot;
                    }
                    average.ShowTargetData = !IsG7Calculated || average.IsSpot;

                    result[slot] = average;
                }
            }
            return result;
        }

        public void SetLinearCurves() {
            foreach (var dotgainComponentAverage in Averages) {
                dotgainComponentAverage.SetLinearCurve();
            }
        }
    }
}