﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Colorware.Core.Logging;

namespace Colorware.Models.StripCompareInterface.Caching {
    public static class ScoreSerialization {
        private static bool useDirectDecoding = false;

        public static Dictionary<int, int> Deserialize(String scoreString) {
            var dotgainValues = new Dictionary<int, int>();
            if (String.IsNullOrEmpty(scoreString))
                return dotgainValues;
            var result = "";
            if (useDirectDecoding) {
                result = scoreString;
            }
            else {
                try {
                    var encoder = new System.Text.UTF8Encoding();
                    var utf8Decode = encoder.GetDecoder();
                    var todecode_byte = Convert.FromBase64String(scoreString);
                    var charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                    var decoded_char = new char[charCount];
                    utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                    result = new String(decoded_char);
                }
                catch (FormatException) {
                    result = scoreString;
                    useDirectDecoding = true;
                }
            }

            var parts = result.Split('&');
            foreach (var part in parts) {
                byte[] bytes = null;
                try {
                    bytes = Convert.FromBase64String(part);
                }
                catch (FormatException) {
                    try {
                        // If the encoded values only contain a single dotgain value,
                        // the previous decoding might have succeeded and decoding it twice will
                        // not work, so retry with the original string.
                        bytes = Convert.FromBase64String(scoreString);
                    }
                    catch (FormatException e) {
                        LogManager.GetLogger(typeof(DotgainSerialization)).Error(e.Message, e);
                    }
                }
                Debug.Assert(bytes.Length == SizeOfValueBytes + SizeOfValueBytes);
                var patchType = bytes[0];
                var score = bytes[1];
                dotgainValues.Add(patchType, score);
            }
            return dotgainValues;
        }

        private const int SizeOfValueBytes = 1;

        public static String Serialize(Dictionary<int, int> values) {
            var resultStrings = new List<String>();
            foreach (var key in values.Keys) {
                var resultBytes = new byte[SizeOfValueBytes + SizeOfValueBytes];
                var patchType = new[] {(byte)key};
                var score = new[] {(byte)values[key]};
                Debug.Assert(patchType.Length == SizeOfValueBytes);
                Debug.Assert(score.Length == SizeOfValueBytes);

                patchType.CopyTo(resultBytes, 0);
                score.CopyTo(resultBytes, SizeOfValueBytes);

                var resultString = Convert.ToBase64String(resultBytes);
                resultStrings.Add(resultString);
            }
            return String.Join("&", resultStrings.ToArray());
        }
    }
}