﻿using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Enums;
using Colorware.Core.Security;
using Colorware.Models.ServiceModels;
using Colorware.Models.StripCompare;

using JetBrains.Annotations;

namespace Colorware.Models.StripCompareInterface.Caching {
    /// <summary>
    /// Utility class to calculate all the <see cref="CachedCompareResult"/>s of a <see cref="IStripCompareResult" />.
    /// </summary>
    public static class CachedCompareResultCalculator {
        public static IReadOnlyList<ICachedCompareResult> Calculate([NotNull] IStripCompareResult result) {
            if (result == null) throw new ArgumentNullException(nameof(result));

            var cachedCompareResults = new List<ICachedCompareResult>();

            List<IStripComparePatch> averages;

            // Image
            if (result.Job.JobType == (int)JobTypes.Image) {
                averages = new List<IStripComparePatch>();

                if (result.NumberOfInkzones > 0) {
                    var targetsPerPanel = result.RawPatches.Patches.Count / result.NumberOfInkzones;

                    // Save color averages for all colors.
                    for (var targetIdx = 0; targetIdx < targetsPerPanel; targetIdx++) {
                        var targetPatches =
                            result.RawPatches.Patches.Where((p, i) => i % targetsPerPanel == targetIdx).ToList();

                        if (targetPatches.Any()) {
                            var avgPatch = StripComparePatch.CreateSummaryPatch(targetPatches.First().ReferencePatch,
                                                                                targetPatches.First().Sample,
                                                                                result.ToleranceSet,
                                                                                targetPatches);
                            averages.Add(avgPatch);
                        }
                    }
                }
            }
            else {
                averages = new List<IStripComparePatch>(result.SummaryPatches.Patches);
            }

            // averages.Sort((patch1, patch2) => patch1.Tint.CompareTo(patch2.Tint));
            foreach (var average in averages)
                calculateAverage(result, cachedCompareResults, averages, average, false);

            if (result.Job.JobType == (int)JobTypes.Image) {
                // Save per color per panel:
                var singleEntries = new List<ICachedCompareResult>();
                foreach (var patch in result.RawPatches.Patches) {
                    calculateAverage(result, singleEntries, averages, patch, true);
                }
                cachedCompareResults.AddRange(singleEntries);
            }

            return cachedCompareResults;
        }

        private static void calculateAverage(IStripCompareResult result,
                                             List<ICachedCompareResult> cachedCompareResults,
                                             IEnumerable<IStripComparePatch> averages, IStripComparePatch solidPatch,
                                             bool isPerZone) {
            switch (solidPatch.PatchType) {
                case PatchTypes.Solid:
                    calculateResultForSolid(result, cachedCompareResults, averages, solidPatch, isPerZone);
                    break;
                case PatchTypes.Balance:
                case PatchTypes.BalanceHighlight:
                case PatchTypes.BalanceMidtone:
                case PatchTypes.BalanceShadow:
                case PatchTypes.Paperwhite:
                case PatchTypes.Other:
                    calculateForRemaining(result, cachedCompareResults, solidPatch, isPerZone);
                    break;
            }
        }

        private static void calculateForRemaining(IStripCompareResult result,
                                                  ICollection<ICachedCompareResult> cachedCompareResults,
                                                  IStripComparePatch patch,
                                                  bool isPerZone) {
            var cachedCompareResult = createCompareResult(result, patch, isPerZone);
            cachedCompareResults.Add(cachedCompareResult);
        }

        private static void calculateResultForSolid(IStripCompareResult result,
                                                    ICollection<ICachedCompareResult> cachedCompareResults,
                                                    IEnumerable<IStripComparePatch> averages,
                                                    IStripComparePatch solidPatch,
                                                    bool isPerZone) {
            var cachedCompareResult = createCompareResult(result, solidPatch, isPerZone);
            // Grab dotgain patches which correspond to this solid patch.
            var dotgains =
                averages.Where(p => (p.IsDotgain || p.IsDotgainOnly) && p.SingleSlot == solidPatch.SingleSlot);
            var items = dotgains.Select(dotgain =>
                                            new List<double> {
                                                dotgain.Tint,
                                                dotgain.SampleDotgainForCurrentMode,
                                                dotgain.ReferenceDotgainForCurrentMode
                                            }).ToList();
            cachedCompareResult.DotgainValues = items;

            var scores = new Dictionary<int, int>(); //score [PatchType][percentage]
            foreach (var score in result.CompareScore.Categories) {
                if (scores.ContainsKey(score.Identifier)) {
                    var value = scores[score.Identifier];
                    var newValue = (100.0 - score.Score);
                    value += (int)newValue;
                    scores[score.Identifier] = value;
                }
                else {
                    scores.Add(score.Identifier, (int)(100.0 - score.Score));
                }
            }
            cachedCompareResult.ScoreValues = scores;
            cachedCompareResults.Add(cachedCompareResult);
        }

        private static CachedCompareResult createCompareResult(IStripCompareResult result, IStripComparePatch patch,
                                                               bool isPerZone) {
            var cachedCompareResult = new CachedCompareResult {
                MeasurementId = result.Measurement.Id,
                JobId = result.Job.Id,
                MachineId = result.Job.MachineId,
                PaperId = result.Job.PaperId,
                InkSetId = result.Job.InkSetId,
                OkSheetId = result.Job.OkSheetId,
                UserId = LoggedInUserProvider.LoggedInUser.Id,
                Name = patch.Name,
                PatchType = patch.PatchType,
                Slot1 = patch.Slot1,
                Slot2 = patch.Slot2,
                Slot3 = patch.Slot3,
                Percentage = patch.Tint,
                L = patch.SampleLab.L,
                a = patch.SampleLab.a,
                b = patch.SampleLab.b,
                Illuminant = patch.SampleLab.MeasurementConditions.Illuminant,
                Observer = patch.SampleLab.MeasurementConditions.Observer,
                SpectralMCondition = patch.SampleLab.MeasurementConditions.SpectralMCondition,
                DeltaDensity = patch.DiffDensity,
                PrintedDensity = patch.Density,
                DeltaE = patch.DeltaE,
                DeltaH = patch.DeltaH,
                InkZone = isPerZone ? patch.InkZone : CachedCompareResult.AverageInkZoneIdentifier,
            };
            return cachedCompareResult;
        }
    }
}