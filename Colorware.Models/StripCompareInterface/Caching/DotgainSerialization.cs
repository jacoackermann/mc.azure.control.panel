using System;
using System.Collections.Generic;
using System.Diagnostics;

using Colorware.Core.Logging;

namespace Colorware.Models.StripCompareInterface.Caching {
    public class DotgainSerialization {
        public const int DotgainPercentageIndex = 0;
        public const int DotgainSampleIndex = 1;
        public const int DotgainReferenceIndex = 2;


        private const int SizeOfPercentageBytes = 1;
        private const int SizeOfDotgainBytes = 4;

        public static List<List<double>> Deserialize(String dotgainString) {
            var dotgainValues = new List<List<double>>();
            if (String.IsNullOrEmpty(dotgainString))
                return dotgainValues;
            string[] parts = dotgainString.Split('&');
            foreach (string part in parts) {
                byte[] bytes = null;
                try {
                    bytes = Convert.FromBase64String(part);
                }
                catch (FormatException e2) {
                    LogManager.GetLogger(typeof(DotgainSerialization)).Error(e2.Message, e2);
                    continue;
                }
                Debug.Assert(bytes.Length == SizeOfPercentageBytes + SizeOfDotgainBytes + SizeOfDotgainBytes);
                byte percentage = bytes[0];
                float sampleValue = BitConverter.ToSingle(bytes, SizeOfPercentageBytes);
                float refValue = BitConverter.ToSingle(bytes, SizeOfPercentageBytes + SizeOfDotgainBytes);
                var item = new List<double> {percentage, sampleValue, refValue};
                dotgainValues.Add(item);
            }
            return dotgainValues;
            // Old version, supporting double decoding.
            // Shouldn't be needed on new Windows server as this was something that happened using
            // Ruby on some systems.
            /*
            var dotgainValues = new List<List<double>>();
            if (String.IsNullOrEmpty(dotgainString))
                return dotgainValues;
            var result = "";
            if (useDirectDecoding) {
                result = dotgainString;
            }
            else {
                try {
                    var encoder = new System.Text.UTF8Encoding();
                    var utf8Decode = encoder.GetDecoder();
                    var todecode_byte = Convert.FromBase64String(dotgainString);
                    var charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                    var decoded_char = new char[charCount];
                    utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                    result = new String(decoded_char);
                }
                catch (FormatException e) {
                    result = dotgainString;
                    useDirectDecoding = true;
                }
            }

            var parts = result.Split('&');
            foreach (var part in parts) {
                byte[] bytes = null;
                try {
                    // If the encoded values only contain a single dotgain value,
                    // the previous decoding might have succeeded and decoding it twice will
                    // not work, so retry with the original string.
                    bytes = Convert.FromBase64String(dotgainString);
                }
                catch (FormatException e) {
                    try {
                        bytes = Convert.FromBase64String(part);
                    }
                    catch (FormatException e2) {
                        LogManager.GetLogger(typeof(DotgainSerialization)).Error(e2.Message, e2);
                        continue;
                    }
                }
                Debug.Assert(bytes.Length == SizeOfPercentageBytes + SizeOfDotgainBytes + SizeOfDotgainBytes);
                var percentage = bytes[0];
                var sampleValue = BitConverter.ToSingle(bytes, SizeOfPercentageBytes);
                var refValue = BitConverter.ToSingle(bytes, SizeOfPercentageBytes + SizeOfDotgainBytes);
                var item = new List<double> { percentage, sampleValue, refValue };
                dotgainValues.Add(item);
            }
            return dotgainValues;
            */
        }

        public static String Serialize(List<List<double>> values) {
            var resultStrings = new List<String>();
            foreach (var value in values) {
                Debug.Assert(value.Count == 3);
                var resultBytes = new byte[SizeOfPercentageBytes + SizeOfDotgainBytes + SizeOfDotgainBytes];
                var percentageBytes = new[] {(byte)value[0]};
                byte[] sampleDotgainBytes = BitConverter.GetBytes((float)value[1]);
                byte[] referenceDotgainBytes = BitConverter.GetBytes((float)value[2]);
                Debug.Assert(percentageBytes.Length == SizeOfPercentageBytes);
                Debug.Assert(sampleDotgainBytes.Length == SizeOfDotgainBytes);
                Debug.Assert(referenceDotgainBytes.Length == SizeOfDotgainBytes);

                percentageBytes.CopyTo(resultBytes, 0);
                sampleDotgainBytes.CopyTo(resultBytes, SizeOfPercentageBytes);
                referenceDotgainBytes.CopyTo(resultBytes, SizeOfPercentageBytes + SizeOfDotgainBytes);

                string resultString = Convert.ToBase64String(resultBytes);
                resultStrings.Add(resultString);
            }
            return String.Join("&", resultStrings.ToArray());
        }
    }
}