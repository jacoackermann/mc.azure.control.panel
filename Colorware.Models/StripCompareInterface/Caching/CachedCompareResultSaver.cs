using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Data;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Data.Specification;
using Colorware.Models.ServiceModels;

namespace Colorware.Models.StripCompareInterface.Caching {
    public static class CachedCompareResultSaver {
        /// <summary>
        /// Saves the cached results for the passed in compare result.
        /// </summary>
        /// <returns>A list of errors that occurred while saving (can be empty).</returns>
        public static async Task<IEnumerable<ICachedCompareResult>> SaveAsync(IStripCompareResult result) {
            // First remove any remaining items.
            var q = new Query().Eq("measurement_id", result.Measurement.Id);
            await BaseModel.DeleteAllNonCascadingAsync<CachedCompareResult>(q);

            // Save new items.
            var items = CachedCompareResultCalculator.Calculate(result);
            await BaseModel.SaveAllAsync(items);
            return items;
        }
    }
}