﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Media;

using Colorware.Core.Algorithms.PointInterpolation;
using Colorware.Core.Algorithms.PointInterpolation.InterpolationFactories;
using Colorware.Core.Config;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Enums;
using Colorware.Core.Globalization;
using Colorware.Core.Mvvm;
using Colorware.Core.Mvvm.AntiMemoryLeaks;

namespace Colorware.Models.StripCompareInterface {
    /// <summary>
    /// Calculates and contains results for a single component/slot.
    /// </summary>
    public class DotgainComponentAverage : DefaultNotifyPropertyChanged {
        public class DotgainPoint {
            /// <summary>
            /// Represents a single point on a non interpolated curve.
            /// </summary>
            public double Tint, SampleY, ReferenceY;

            public override string ToString() {
                return string.Format("DotgainComponentAverage.DotgainPoint({0}, Sample: {1}, Reference: {2}", Tint,
                    SampleY, ReferenceY);
            }
        }

        private const double DefaultXStep = 5.0;
        private double xStep;

        public double XStep {
            set {
                xStep = value;
                // Reset information.
                pointsOfSelectedMethod = null;
            }
        }

        /// <summary>
        /// The patches to calculate the interpolated points from.
        /// </summary>
        private readonly IReadOnlyList<IStripComparePatch> patches;

        public IReadOnlyList<IStripComparePatch> Patches {
            get { return patches; }
        }

        /// <summary>
        /// The strategy to use to calculate the interpolated points.
        /// </summary>
        private readonly IInterpolationStrategyFactory strategyFactory;

        #region Properties
        private List<DotgainPoint> curve;

        /// <summary>
        /// The raw (uninterpolated) curve containing the dotgain and reference points.
        /// </summary>
        public List<DotgainPoint> Curve {
            get {
                if (curve == null)
                    curve = generateCurveFromPatches();
                return curve;
            }
            set {
                curve = value;
                OnPropertyChanged("Curve");
            }
        }

        private List<Point> baseCurve;

        /// <summary>
        /// The base curve to apply CTP compensation to. This is the none interpolated curve. If no curve is set
        /// this reverts to the linear curve.
        /// </summary>
        public List<Point> BaseCurve {
            get {
                if (baseCurve == null)
                    SetLinearCurve();
                return baseCurve;
            }
            set {
                baseCurve = value;
                OnPropertyChanged("BaseCurve");
            }
        }

        private List<DotgainResultPoint> pointsOfSelectedMethod;

        public List<DotgainResultPoint> PointsOfSelectedMethod {
            get {
                if (pointsOfSelectedMethod == null)
                    RefreshPoints();
                return pointsOfSelectedMethod;
            }
            private set {
                pointsOfSelectedMethod = value;
                OnPropertyChanged("PointsOfSelectedMethod");
            }
        }

        public List<DotgainResultPoint> RealPointsOfSelectedMethod {
            get { return PointsOfSelectedMethod.Where(p => p.IsRealPoint && p.Tint > 0 && p.Tint < 100).ToList(); }
        }

        public List<DotgainResultPoint> InterpolatedPointsOfSelectedMethod {
            get { return PointsOfSelectedMethod.Where(p => ! p.IsRealPoint && p.Tint > 0 && p.Tint < 100).ToList(); }
        }

        public String Name {
            get { return !patches.Any() ? "?" : patches.First().Name; }
        }

        public int Slot {
            get { return !patches.Any() ? -1 : patches.First().SingleSlot; }
        }

        public DotgainMethod DotgainMethod => !Patches.Any() ? DotgainMethod.Density : patches.First().DotgainMethod;

        private double tolerance;

        /// <summary>
        /// Defines the tolerance to use for building the tolerance graphs.
        /// </summary>
        public double Tolerance {
            get { return tolerance; }
            set {
                tolerance = value;
                PointsOfSelectedMethod = null;
                OnPropertyChanged("Tolerance");
            }
        }

        private static readonly Regex processNameRegex = new Regex(@"(k|c|m|y)\d*$",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public String NameStrippedPercentages {
            get {
                var name = Name;
                if (processNameRegex.IsMatch(name)) {
                    var newName = processNameRegex.Replace(name, "$1");
                    return newName;
                }
                return name;
            }
        }

        /// <summary>
        /// Used to override display color. This is useful to set the color of the related solid so
        /// that the resulting interface color will not appear to dim.
        /// </summary>
        public Color? DisplayColorOverride { get; set; }

        public Color AsColor {
            get {
                if (!patches.Any())
                    return Colors.Black;

                if (DisplayColorOverride.HasValue)
                    return DisplayColorOverride.Value;

                return patches.Last().AsColor;
            }
        }

        public Color As10PercentAlphaColor {
            get {
                var color = AsColor;
                return Color.FromArgb((byte)(255 * 0.10), color.R, color.G, color.B);
            }
        }

        public Color As50PercentAlphaColor {
            get {
                var color = AsColor;
                return Color.FromArgb((byte)(255 * 0.50), color.R, color.G, color.B);
            }
        }

        public Color As80PercentAlphaColor {
            get {
                var color = AsColor;
                return Color.FromArgb((byte)(255 * 0.80), color.R, color.G, color.B);
            }
        }

        public bool IsSpot { get; set; }

        public bool ShowTargetData { get; set; }
        #endregion

        #region Constructors
        public DotgainComponentAverage(IEnumerable<IStripComparePatch> patches)
            : this(patches, DefaultXStep) {
        }

        public DotgainComponentAverage(IEnumerable<IStripComparePatch> patches, double xStep)
            : this(patches, selectDefaultStrategy()) {
            this.xStep = xStep;
        }

        private static IInterpolationStrategyFactory selectDefaultStrategy() {
            var useOldDotgainStrategy = Config.DefaultConfig.GetBool("Options.Dotgain.UseOldDotgainStrategy", false);
            if (useOldDotgainStrategy)
                return new HermiteInterpolationStrategyFactory(0, 0, 0.001);
            return new SplineFitInterpolationStrategyFactory();
        }

        public DotgainComponentAverage(IEnumerable<IStripComparePatch> patches,
            IInterpolationStrategyFactory strategyFactory) {
            this.strategyFactory = strategyFactory;
            this.patches = patches.ToNonLeakyList();
        }
        #endregion

        public void SetLinearCurve() {
            var points = new List<Point>();
            // NB: this '+= 10' is arbitrary and has nothing to do with the xStep
            // we just need a list with at least 4 points
            for (var i = 0; i <= 100; i += 10) {
                var point = new Point(i, i);
                points.Add(point);
            }
            SetBaseCurve(points);
        }

        public void RefreshPoints() {
            PointsOfSelectedMethod = calculateDotgainPoints();
        }

        /// <summary>
        /// Sets the base curve, if no curve is entered, the base curve should be the linear curve.
        /// </summary>
        /// <param name="points">The points.</param>
        public void SetBaseCurve(List<Point> points) {
            BaseCurve = points;
            PointsOfSelectedMethod = null;
        }

        public void SetCurve(List<DotgainPoint> newCurvePoints) {
            Curve = newCurvePoints;
            PointsOfSelectedMethod = null;
        }

        private List<DotgainPoint> generateCurveFromPatches() {
            var points = new List<DotgainPoint>();
            foreach (var patch in patches) {
                var dotgainSample = patch.SampleDotGain;
                var dotgainRef = patch.RefDotGain;

                var point = new DotgainPoint {
                    Tint = patch.Tint,
                    SampleY = dotgainSample,
                    ReferenceY = dotgainRef
                };
                points.Add(point);
            }
            return points;
        }

        /// <summary>
        /// Main method to actually calculate the interpolated dotgain points.
        /// </summary>
        /// <returns>The resulting points for the dotgain.</returns>
        private List<DotgainResultPoint> calculateDotgainPoints() {
            // Build list of interpolated points for samples and references.
            var piSample = new PointInterpolator(strategyFactory);
            var piSampleReversed = new PointInterpolator(strategyFactory);
            var piRef = new PointInterpolator(strategyFactory);

            piSample.Add(new Point(0, 0));
            piSampleReversed.Add(new Point(0, 0));
            piRef.Add(new Point(0, 0));
            foreach (var curvePoint in Curve) {
                var samplePoint = new Point(curvePoint.Tint, curvePoint.SampleY + curvePoint.Tint);
                var samplePointReversed = new Point(curvePoint.SampleY + curvePoint.Tint, curvePoint.Tint);
                var refPoint = new Point(curvePoint.Tint, curvePoint.ReferenceY + curvePoint.Tint);
                piSample.Add(samplePoint);
                piSampleReversed.Add(samplePointReversed);
                piRef.Add(refPoint);
            }
            piSample.Add(new Point(100, 100));
            piSampleReversed.Add(new Point(100, 100));
            piRef.Add(new Point(100, 100));

            // Setup interpolation curve for base curve points.
            var piBase = new PointInterpolator(strategyFactory);
            piBase.AddRange(BaseCurve);

            // Interpolate points.
            var samplePoints = piSample.GenerateInterpolatedPointList(xStep);
            var refPoints = piRef.GenerateInterpolatedPointList(xStep);
            var baseCurvePoints = piBase.GenerateInterpolatedPointList(xStep);
            // Check for consistency.
            if (samplePoints.Count != refPoints.Count) {
                MessageBox.Show(LanguageManager._("CW.Models.DCA.DotgainInconsistent"),
                    LanguageManager._("CW.Models.DCA.DotgainInconsistentCap"));
                var minItems = Math.Min(Math.Min(samplePoints.Count, refPoints.Count), baseCurvePoints.Count);
                samplePoints = samplePoints.GetRange(0, minItems);
                refPoints = refPoints.GetRange(0, minItems);
            }

            // Produce final result.
            var resultPoints = new List<DotgainResultPoint>();
            for (int i = 0; i < samplePoints.Count; i++) {
                var sample = samplePoints[i];
                var reference = refPoints[i];
                var @base = piBase.FindPointForX(sample.X);
                // Find best sample point for this reference.
                var ctpCompLinear = piSampleReversed.FindPointForX(reference.Y);
                var ctpCompOnBase = piBase.FindPointForX(ctpCompLinear.Y);
                var dPoint = new DotgainResultPoint {
                    Tint = sample.X,
                    ReferenceDotgain = reference.Y - reference.X,
                    ReferenceDotgainLinear = reference.Y,
                    SampleDotgain = sample.Y - sample.X,
                    SampleDotgainLinear = sample.Y,
                    DotgainDiff = sample.Y - reference.Y,
                    BaseCurve = @base.Y,
                    NewCurve = ctpCompOnBase.Y,
                    Curve = sample.X + (ctpCompOnBase.X - sample.X),
                    CurveDelta = ctpCompOnBase.Y - @base.Y,
                    CtpX = ctpCompOnBase.X,
                    CtpY = ctpCompOnBase.Y,
                    ToleranceTop = reference.Y - reference.X + tolerance,
                    ToleranceBottom = reference.Y - reference.X - tolerance,
                    IsRealPoint = ! sample.IsVirtual
                };
                resultPoints.Add(dPoint);
            }

            return resultPoints;
        }

        #region Overrides of Object
        public override string ToString() {
            return String.Format("DotgainComponentAverage({0} [{1} points])", Name, PointsOfSelectedMethod.Count);
        }
        #endregion

        public DotgainComponentAverage Copy() {
            return new DotgainComponentAverage(patches, strategyFactory) {
                XStep = xStep,
                Tolerance = Tolerance,
                BaseCurve = BaseCurve,
                Curve = Curve
            };
        }
    }
}