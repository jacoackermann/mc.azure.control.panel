﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Media;

using Colorware.Core.Color;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Enums;
using Colorware.Core.Helpers;
using Colorware.Models.ServiceModels;
using Colorware.Models.StripCompare;

namespace Colorware.Models.StripCompareInterface {
    public class StripCompareComponentInkZones : IStripCompareComponentInkZones, INotifyPropertyChanged {
        public String Name {
            get {
                if (!Patches.Any())
                    return "";

                var patch = Patches.FirstOrDefault(s => !string.IsNullOrEmpty(s.Name));
                return patch == null ? "" : patch.Name;
            }
        }

        public int Percentage { get; set; }
        public int Slot { get; private set; }
        public int SequenceNumber { get; set; }

        private List<IStripComparePatch> patches;

        public List<IStripComparePatch> Patches {
            get { return patches ?? (patches = new List<IStripComparePatch>()); }
        }

        private List<IStripComparePatch> solidPatches;

        public List<IStripComparePatch> SolidPatches {
            get { return solidPatches ?? (solidPatches = new List<IStripComparePatch>(patches.Where(p => p.IsSolid))); }
        }

        private bool isOkSheet;

        public StripCompareComponentInkZones(int slot, bool isOkSheet) {
            Slot = slot;
            this.isOkSheet = isOkSheet;
        }

        /// <summary>
        /// Pad the ink zones to make sure they extend the full range as given.
        /// This means adding undefined patches in front of the patches that are present
        /// to bring them into the proper positions (and counting can start from inkzone 1), but
        /// also adding patches at the end to bring the full range up to the range given (which is
        /// usually the range of the current printing/press machine).
        /// </summary>
        /// <param name="endZone">The end inkzone. This is usually defined by the used machine.</param>
        public void PadInkZones(int endZone) {
            patches = InkZonePadder.PadInkZones(Patches, endZone, createMockPatch);
        }

        /// <summary>
        /// Create a mock patch for the given inkzone.
        /// This patch denotes the absence of a patch of the current component in the given inkzone.
        /// </summary>
        /// <param name="current"></param>
        /// <returns></returns>
        private static IStripComparePatch createMockPatch(int current) {
            var p = new StripComparePatch(JobStripPatch.CreateUndefined(),
                                          new Sample(new Lab(0, 0, 0, SpectralMeasurementConditions.Undefined))) {
                InkZone = current,
                PatchType = PatchTypes.Undefined,
                InTolerance = true
            };

            return p;
        }

        #region Calculated properties
        private Color? averageColor;

        public Color AverageColor {
            get {
                if (!averageColor.HasValue) {
                    averageColor = calculateAverageColorOfDefinedPatches(Patches);
                }
                return averageColor.Value;
            }
        }

        private double? averageChromaPlus;

        public double AverageChromaPlus {
            get {
                if (!averageChromaPlus.HasValue) {
                    var patchesToAverage = Patches.Where(p => p.IsDefined && !p.IsLocked);
                    if (!patchesToAverage.Any())
                        averageChromaPlus = 0.0;
                    else
                        averageChromaPlus = patchesToAverage.Average(p => p.ChromaPlus);
                }
                return averageChromaPlus.Value;
            }
        }

        private double? averageDeltaDensity;

        public double AverageDeltaDensity {
            get {
                if (!averageDeltaDensity.HasValue) {
                    var patchesToAverage = Patches.Where(p => p.IsDefined && !p.IsLocked);
                    if (!patchesToAverage.Any())
                        averageDeltaDensity = 0.0;
                    else
                        averageDeltaDensity = patchesToAverage.Average(p => p.DiffDensity);
                }
                return averageDeltaDensity.Value;
            }
        }

        private double? averageSampleDensity;

        public double AverageSampleDensity {
            get {
                if (!averageSampleDensity.HasValue) {
                    var patchesToAverage = Patches.Where(p => p.IsDefined && !p.IsLocked);
                    if (!patchesToAverage.Any())
                        averageSampleDensity = 0.0;
                    else
                        averageSampleDensity = patchesToAverage.Average(p => p.Density);
                }
                return averageSampleDensity.Value;
            }
        }

        private double? averageDeltaE;

        public double AverageDeltaE {
            get {
                if (!averageDeltaE.HasValue) {
                    var patchesToAverage = Patches.Where(p => p.IsDefined && !p.IsLocked);
                    if (!patchesToAverage.Any())
                        averageDeltaE = 0.0;
                    else
                        averageDeltaE = patchesToAverage.Average(p => p.DeltaE);
                }
                return averageDeltaE.Value;
            }
        }

        private double? averageDeltaDotgain;

        public double AverageDeltaDotgain {
            get {
                if (!averageDeltaDotgain.HasValue) {
                    var patchesToAverage = Patches.Where(p => p.IsDefined && !p.IsLocked);
                    if (!patchesToAverage.Any())
                        averageDeltaDotgain = 0.0;
                    else
                        averageDeltaDotgain = patchesToAverage.Average(p => p.DotgainDiff);
                }

                return averageDeltaDotgain.Value;
            }
        }

        private double? solidReferenceDensity;

        public double SolidReferenceDensity {
            get {
                if (!solidReferenceDensity.HasValue) {
                    var fullColor = Patches.FirstOrDefault(p => p.IsSolid && p.SingleSlot == Slot);
                    if (fullColor == null) {
                        return 0.0;
                    }
                    solidReferenceDensity = fullColor.RefDensity.GetFilteredValue();
                }
                return solidReferenceDensity.Value;
            }
        }

        public String SolidReferenceDensityName {
            get {
                if (isOkSheet)
                    return "OK";
                var dens = Math.Round(SolidReferenceDensity, 2);
                return dens.ToString() + "D";
            }
        }
        #endregion

        private static Color calculateAverageColorOfDefinedPatches(
            ICollection<IStripComparePatch> comparePatches) {
            var definedPatches = comparePatches.Where(p => p.IsDefined && !p.IsLocked).ToList();

            if (!definedPatches.Any())
                // No defined patches found. Just return black for now.
                return Colors.Black;

            var averageColor = definedPatches.Select(p => p.SampleLab).Sum() / definedPatches.Count();
            return averageColor.AsColor;
        }

        // Anti memory leak
#pragma warning disable 0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 0067
    }
}