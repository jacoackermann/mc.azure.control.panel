using System;

using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring;
using Colorware.Core.Enums;
using Colorware.Models.StripCompareInterface.Scoring.EPQScoring;
using Colorware.Models.StripCompareInterface.Scoring.Image;

namespace Colorware.Models.StripCompareInterface.Scoring {
    public class StripCompareScoreFactory : IStripCompareScoreFactory {
        private readonly JobTypes jobType;

        public StripCompareScoreFactory(JobTypes jobType) {
            this.jobType = jobType;
        }

        #region Implementation of IStripCompareScoreFactory
        public IStripCompareScore Create(IStripCompareResult result) {
            switch (jobType) {
                case JobTypes.Offset:
                case JobTypes.Printability:
                    return new StripCompareScore(result.CompareSummary);
                case JobTypes.Flexo:
                    return new EpqStripCompareScore(result);
                case JobTypes.Image:
                    return new ImageStripCompareScore(result.CompareSummary);
                default:
                    throw new InvalidOperationException(
                        "Can't create IStripCompareScore for job type specified in factory constructor!");
            }
        }
        #endregion
    }
}