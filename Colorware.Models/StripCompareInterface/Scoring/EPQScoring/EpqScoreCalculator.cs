using System;
using System.Diagnostics;
using System.Windows.Media;

using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring;
using Colorware.Core.Enums;

namespace Colorware.Models.StripCompareInterface.Scoring.EPQScoring {
    public static class EpqScoreCalculator {
        public const double MinVisuaScore = 1.0;
        public const double MaxVisualScore = 5.0;

        /// Score percentages for the individual parts.
        /// These are used to construct the final score.
        /// (Should add up to a hundered)
        public const double ScoreRegistrationPart = 0.25;

        public const double ScoreDeltaE100Part = 0.20;
        public const double ScoreDeltaE50Part = 0.05;
        public const double ScoreTvi50Part = 0.25;
        public const double ScoreVisualPart = 0.25;

        public static EpqStripCompareScoreEntry CalculateDeltaEScoreForSolid(IStripComparePatch patch) {
            Debug.Assert(patch.IsSolid);
            var deltaE = patch.DeltaE;
            double score;
            if (deltaE <= 0.75)
                score = 5.0;
            else if (deltaE <= 1.5)
                score = 4.5;
            else if (deltaE <= 2.0)
                score = 4.0;
            else if (deltaE <= 2.5)
                score = 3.0;
            else if (deltaE <= 3.25)
                score = 2.0;
            else score = 1.0;

            return new EpqStripCompareScoreEntry(patch, score, CalculateSolidDeltaEResultForScore(score));
        }

        public static EpqScoreResult CalculateSolidDeltaEResultForScore(double score) {
            if (score <= 1.0) return EpqScoreResult.Fail;
            if (score <= 2.0) return EpqScoreResult.Poor;
            if (score <= 3.0) return EpqScoreResult.Satisfactory;
            if (score <= 4.0) return EpqScoreResult.Good;
            return EpqScoreResult.Excellent;
        }

        public static EpqStripCompareScoreEntry CalculateDeltaEScoreForDotgain(IStripComparePatch patch) {
            if (patch.IsDotgainOnly)
                return null;
            Debug.Assert(patch.IsDotgain);
            var deltaE = patch.DeltaE;
            double score;
            if (deltaE <= 0.75)
                score = 5.0;
            else if (deltaE <= 1.5)
                score = 4.5;
            else if (deltaE <= 2.0)
                score = 4.0;
            else if (deltaE <= 2.50)
                score = 3.5;
            else if (deltaE <= 3.50)
                score = 3.0;
            else if (deltaE <= 4.0)
                score = 2.0;
            else if (deltaE <= 4.25)
                score = 1.5;
            else
                score = 1.0;
            return new EpqStripCompareScoreEntry(patch, score, CalculateDotgainDeltaEResultForScore(score));
        }

        public static EpqScoreResult CalculateDotgainDeltaEResultForScore(double score) {
            if (score <= 1.0) return EpqScoreResult.Fail;
            if (score <= 1.5) return EpqScoreResult.Fail;
            if (score <= 2.0) return EpqScoreResult.Poor;
            if (score <= 3.0) return EpqScoreResult.Satisfactory;
            if (score <= 3.5) return EpqScoreResult.Good;
            if (score <= 4.0) return EpqScoreResult.Good;
            if (score <= 4.5) return EpqScoreResult.Excellent;
            return EpqScoreResult.Excellent;
        }

        public static EpqStripCompareScoreEntry CalculateTviScoreForDotgain(IStripComparePatch patch, bool isWideFlexo) {
            if (isWideFlexo)
                return calculateTviScoreForDotgainWideFlexo(patch);
            return calculateTviScoreForDotgainNoWideFlexo(patch);
        }

        private static EpqStripCompareScoreEntry calculateTviScoreForDotgainNoWideFlexo(IStripComparePatch patch) {
            Debug.Assert(patch.IsDotgain || patch.IsDotgainOnly);
            var deltaDotgain = Math.Abs(patch.DotgainDiff);
            double score;
            if (deltaDotgain < 4.0)
                score = 5.0;
            else if (deltaDotgain < 5.0)
                score = 4.0;
            else if (deltaDotgain < 6.0)
                score = 3.0;
            else if (deltaDotgain < 7.0)
                score = 2.0;
            else
                score = 1.0;
            return new EpqStripCompareScoreEntry(patch, score, CalculateDotgainTviResultForScoreNoWideFlexo(score));
        }

        public static EpqScoreResult CalculateDotgainTviResultForScoreNoWideFlexo(double score) {
            if (score <= 1.0) return EpqScoreResult.Fail;
            if (score <= 2.0) return EpqScoreResult.Poor;
            if (score <= 3.0) return EpqScoreResult.Satisfactory;
            if (score <= 4.0) return EpqScoreResult.Good;
            return EpqScoreResult.Excellent;
        }

        private static EpqStripCompareScoreEntry calculateTviScoreForDotgainWideFlexo(IStripComparePatch patch) {
            Debug.Assert(patch.IsDotgain || patch.IsDotgainOnly);
            var deltaDotgain = Math.Abs(patch.DotgainDiff);
            double score;
            if (deltaDotgain < 4.0)
                score = 5.0;
            else if (deltaDotgain < 5.0)
                score = 4.0;
            else if (deltaDotgain < 6.0)
                score = 3.5;
            else if (deltaDotgain < 7.0)
                score = 3.25;
            else if (deltaDotgain < 8.0)
                score = 3.0;
            else if (deltaDotgain < 9.0)
                score = 2.0;
            else
                score = 1.0;
            return new EpqStripCompareScoreEntry(patch, score, CalculateDotgainTviResultForScoreWideFlexo(score));
        }

        public static EpqScoreResult CalculateDotgainTviResultForScoreWideFlexo(double score) {
            if (score <= 1.0) return EpqScoreResult.Fail;
            if (score <= 2.0) return EpqScoreResult.Poor;
            if (score <= 3.0) return EpqScoreResult.Satisfactory;
            if (score <= 3.25) return EpqScoreResult.Satisfactory;
            if (score <= 3.5) return EpqScoreResult.Satisfactory;
            if (score <= 4.0) return EpqScoreResult.Good;
            return EpqScoreResult.Excellent;
        }

        public static EpqStripVisualScoreEntry CalculateVisualScore(double score) {
            return new EpqStripVisualScoreEntry(score, CalculateVisualResultForScore(score));
        }

        public static EpqScoreResult CalculateVisualResultForScore(double score) {
            if (score < 2.0) return EpqScoreResult.Fail;
            if (score < 3.0) return EpqScoreResult.Poor;
            if (score < 3.75) return EpqScoreResult.Satisfactory;
            if (score < 4.5) return EpqScoreResult.Good;
            return EpqScoreResult.Excellent;
        }

        public static EpqStripRegistrationScoreEntry CalculateRegistrationScore(IStripComparePatch solid, double score) {
            if (score < 2.0)
                return new EpqStripRegistrationScoreEntry(solid, score, EpqScoreResult.Fail);
            if (score < 3.0)
                return new EpqStripRegistrationScoreEntry(solid, score, EpqScoreResult.Poor);
            if (score < 3.75)
                return new EpqStripRegistrationScoreEntry(solid, score, EpqScoreResult.Satisfactory);
            if (score < 4.5)
                return new EpqStripRegistrationScoreEntry(solid, score, EpqScoreResult.Good);
            return new EpqStripRegistrationScoreEntry(solid, score, EpqScoreResult.Excellent);
        }

        public static EpqScoreResult CalculateRegistrationResultForScore(double score) {
            if (score < 2.0) return EpqScoreResult.Fail;
            if (score < 3.0) return EpqScoreResult.Poor;
            if (score < 3.75) return EpqScoreResult.Satisfactory;
            if (score < 4.5) return EpqScoreResult.Good;
            return EpqScoreResult.Excellent;
        }

        public static EpqScoreResult CalculateFinalResult(double finalScore) {
            return CalculateVisualScore(finalScore).RawResult; // Currently the same as the visualscore.
        }

        public static StripCompareScoreResult EpqScoreToStripCompareScore(EpqScoreResult rawResult) {
            switch (rawResult) {
                case EpqScoreResult.Unknown:
                    return StripCompareScoreResult.Unknown;
                case EpqScoreResult.Excellent:
                    return StripCompareScoreResult.Pass;
                case EpqScoreResult.Good:
                    return StripCompareScoreResult.Pass;
                case EpqScoreResult.Satisfactory:
                    return StripCompareScoreResult.Warning;
                case EpqScoreResult.Poor:
                    return StripCompareScoreResult.Fail;
                case EpqScoreResult.Fail:
                    return StripCompareScoreResult.Fail;
                default:
                    throw new ArgumentOutOfRangeException("rawResult");
            }
        }

        public static Brush ScoreResultToBrush(EpqScoreResult result, bool useTransparentGreen) {
            return new SolidColorBrush(ScoreResultToColor(result, useTransparentGreen));
        }

        public static Brush ScoreResultToBrush(EpqScoreResult result) {
            return new SolidColorBrush(ScoreResultToColor(result));
        }

        public static Color ScoreResultToColor(EpqScoreResult result) {
            return ScoreResultToColor(result, false);
        }

        public static Color ScoreResultToColor(EpqScoreResult result, bool useTransparentGreen) {
            switch (result) {
                case EpqScoreResult.Excellent:
                    return useTransparentGreen ? Colors.Transparent : Colors.DarkGreen;
                case EpqScoreResult.Good:
                    return useTransparentGreen ? Colors.Transparent : Colors.LightGreen;
                case EpqScoreResult.Satisfactory:
                    return Color.FromRgb(239, 219, 0);
                case EpqScoreResult.Poor:
                    return Colors.Orange;
                case EpqScoreResult.Fail:
                    return Color.FromRgb(219, 0, 0);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}