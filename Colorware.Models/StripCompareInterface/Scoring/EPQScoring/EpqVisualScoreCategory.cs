using System.Collections.Generic;

using Colorware.Core.Data.Models.StripCompareInterface.Scoring;
using Colorware.Core.Enums;

namespace Colorware.Models.StripCompareInterface.Scoring.EPQScoring {
    public class EpqVisualScoreCategory : IStripCompareScoreCategory {
        #region Implementation of IStripCompareScoreCategory
        private readonly string name;

        public string Name {
            get { return name; }
        }

        private double score;

        public double Score {
            get { return score; }
            set { score = value; }
        }

        public int Identifier {
            get { return -1; }
        }

        public bool IsOk {
            get { return Result == StripCompareScoreResult.Pass; }
        }

        public bool IsWarning {
            get { return Result == StripCompareScoreResult.Warning; }
        }

        public bool IsFail {
            get { return Result == StripCompareScoreResult.Fail; }
        }

        public StripCompareScoreResult Result {
            get { return EpqScoreCalculator.EpqScoreToStripCompareScore(RawResult); }
        }

        private EpqScoreResult rawResult;

        public EpqScoreResult RawResult {
            get { return rawResult; }
            set { rawResult = value; }
        }

        public IEnumerable<IStripCompareScoreEntry> Entries {
            get { return new List<IStripCompareScoreEntry>(); }
        }

        public void CalculateScore() {
            throw new System.NotImplementedException();
        }
        #endregion

        public EpqVisualScoreCategory(string name) {
            this.name = name;
        }
    }
}