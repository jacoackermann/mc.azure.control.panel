using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using Colorware.Core.Config;
using Colorware.Core.Data;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring.EPQScoring;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;
using Colorware.Core.Security;
using Colorware.Models.ServiceModels;

using JetBrains.Annotations;

namespace Colorware.Models.StripCompareInterface.Scoring.EPQScoring {
    // [Obsolete("Not needed in new Cds system.")] (TODO: check if this is true and strip out any dependencies and refactor)

    public class EpqStripCompareScore : INotifyPropertyChanged, IEpqStripCompareScore {
        public Task Initialize { get; private set; }

        private const string PrinterNameKey = "Epq.Defaults.PrinterName";
        private const string PrinterLocationKey = "Epq.Defaults.PrinterLocation";
        private const string BrandKey = "Epq.Defaults.Brand";

        private readonly IStripCompareResult result;

        #region Implementation of IStripCompareScore
        private double? finalScore;

        public double FinalScore {
            get {
                if (!finalScore.HasValue)
                    CalculateFinalScore();
                Debug.Assert(finalScore.HasValue);
                return finalScore.HasValue ? finalScore.Value : 0.0;
            }
            private set { finalScore = value; }
        }

        public bool HasFinalScore {
            get {
                return
                    HasVisual &&
                    HasRegistration;
            }
        }

        private StripCompareScoreResult? finalResult;

        public StripCompareScoreResult FinalResult {
            get {
                if (!finalResult.HasValue)
                    CalculateFinalScore();
                Debug.Assert(finalResult.HasValue);
                return finalResult.HasValue ? finalResult.Value : StripCompareScoreResult.Unknown;
            }
            private set { finalResult = value; }
        }

        private EpqScoreResult? finalRawResult;

        public EpqScoreResult FinalRawResult {
            get {
                if (! finalRawResult.HasValue)
                    CalculateFinalScore();
                Debug.Assert(finalRawResult.HasValue);
                return finalRawResult.HasValue ? finalRawResult.Value : EpqScoreResult.Unknown;
            }
            set { finalRawResult = value; }
        }

        private readonly EpqCompareScoreCategory categoryDeltaE100;

        public IStripCompareScoreCategory CategoryDeltaE100 {
            get { return categoryDeltaE100; }
        }

        private readonly EpqCompareScoreCategory categoryDeltaE50;

        public IStripCompareScoreCategory CategoryDeltaE50 {
            get { return categoryDeltaE50; }
        }

        private readonly EpqCompareScoreCategory categoryTvi;

        public IStripCompareScoreCategory CategoryTvi {
            get { return categoryTvi; }
        }

        private readonly EpqVisualScoreCategory categoryVisual;

        public IStripCompareScoreCategory CategoryVisual {
            get { return categoryVisual; }
        }

        private readonly EpqRegistrationScoreCategory categoryRegistration;

        public IStripCompareScoreCategory CategoryRegistration {
            get { return categoryRegistration; }
        }

        private readonly EpqCompareScoreCategory other;

        public IStripCompareScoreCategory Other {
            get { return other; }
        }

        public IEnumerable<IStripCompareScoreCategory> Categories {
            get {
                return new[] {
                    CategoryDeltaE100,
                    CategoryDeltaE50,
                    CategoryTvi,
                    CategoryVisual,
                    CategoryRegistration
                };
            }
        }

        public IEnumerable<IStripCompareScoreCategory> EpqCompareScoreCategories {
            get {
                return new[] {
                    CategoryDeltaE100,
                    CategoryDeltaE50,
                    CategoryTvi,
                    // CategoryVisual,
                    // CategoryRegistration,
                    Other
                };
            }
        }

        private readonly List<EpqStripCompareScoreEntry> patchResults;

        public IEnumerable<IStripCompareScoreEntry> PatchResults {
            get { return patchResults; }
        }
        #endregion

        #region Properties
        private IEpqScore currentAdditionalScoreData;

        private bool hasVisual;
        public bool HasVisual {
            get { return hasVisual; }
            private set {
                hasVisual = value;
                InvokePropertyChanged("HasVisual");
            }
        }

        private bool hasRegistration;
        public bool HasRegistration {
            get { return hasRegistration; }
            private set {
                hasRegistration = value;
                InvokePropertyChanged("HasRegistration");
            }
        }
        #endregion

        public EpqStripCompareScore(IStripCompareResult result) : this(result, null) {
        }

        private EpqStripCompareScore([NotNull] IStripCompareResult result, IEpqScore clone) {
            if (result == null) throw new ArgumentNullException("result");
            currentAdditionalScoreData = clone;
            this.result = result;
            patchResults = new List<EpqStripCompareScoreEntry>();

            categoryDeltaE100 = new EpqCompareScoreCategory("DeltaE 100", PatchTypes.Solid);
            categoryDeltaE50 = new EpqCompareScoreCategory("DeltaE 50", PatchTypes.Dotgain);
            categoryTvi = new EpqCompareScoreCategory("Tvi", PatchTypes.Dotgain);
            categoryVisual = new EpqVisualScoreCategory("Visual");
            categoryRegistration = new EpqRegistrationScoreCategory("Registration");
            other = new EpqCompareScoreCategory("Other", PatchTypes.Undefined);

            Initialize = GetAdditionalScoreData();
            initScores();
            CalculateFinalScore();
        }

        private void initScores() {
            initScores(result.SummaryPatches.Patches);
        }

        /// <summary>
        /// Calculate the score entries for each patch.
        /// </summary>
        /// <param name="patchList"></param>
        private void initScores(IEnumerable<IStripComparePatch> patchList) {
            foreach (var patch in patchList) {
                addScoreForPatch(patch);
            }
        }

        /// <summary>
        /// Calculate and add the score entries for a given patch.
        /// </summary>
        /// <param name="patch"></param>
        private void addScoreForPatch(IStripComparePatch patch) {
            switch (patch.PatchType) {
                case PatchTypes.Solid:
                    var scoreEntry = EpqScoreCalculator.CalculateDeltaEScoreForSolid(patch);
                    patchResults.Add(scoreEntry);
                    categoryDeltaE100.AddEntry(scoreEntry);
                    break;
                case PatchTypes.Dotgain:
                case PatchTypes.DotgainOnly:
                    if (patch.Tint != 50)
                        break;
                    var deltaEScoreEntry = EpqScoreCalculator.CalculateDeltaEScoreForDotgain(patch);
                    var tviScoreEntry = EpqScoreCalculator.CalculateTviScoreForDotgain(patch, result.Job.IsWideFlexo);
                    if (deltaEScoreEntry != null) {
                        patchResults.Add(deltaEScoreEntry);
                        categoryDeltaE50.AddEntry(deltaEScoreEntry);
                    }
                    patchResults.Add(tviScoreEntry);
                    categoryTvi.AddEntry(tviScoreEntry);
                    break;
                default:
                    var otherScoreEntry = new EpqStripCompareScoreEntry(patch, 0.0, EpqScoreResult.Excellent);
                    patchResults.Add(otherScoreEntry);
                    other.AddEntry(otherScoreEntry);
                    break;
            }
        }

        /// <summary>
        /// Get the associated additional data for the current score. Will create an empty entry if it does not
        /// exist yet.
        /// </summary>
        /// <returns>An EpqScore instance related to the current score.</returns>
        public async Task<IEpqScore> GetAdditionalScoreData() {
            if (currentAdditionalScoreData != null)
                return currentAdditionalScoreData;
            var query = new Query().Eq("job_id", result.Job.Id).Eq("measurement_id", result.Measurement.Id);
            // Try to fetch.
            currentAdditionalScoreData = (await BaseModel.FindAllAsync<IEpqScore>(query)).FirstOrDefault() ??
                                         createDefaultAdditionalScoreData();
            if (currentAdditionalScoreData == null) {
                HasVisual = false;
                HasRegistration = false;
            }
            else {
                HasVisual = currentAdditionalScoreData.HasVisualScore;
                HasRegistration = currentAdditionalScoreData.HasRegistrations;
            }
            return currentAdditionalScoreData;
        }

        private IEpqScore createDefaultAdditionalScoreData() {
            var item = new EpqScore {
                JobId = result.Job.Id,
                MeasurementId = result.Measurement.Id,
                Production = result.Measurement.CreatedAt,
                Received = result.Measurement.CreatedAt,
                Recorded = DateTime.Now,
                VisualScore = 5.0,
                PrinterName = getDefaultPrinterName(),
                PrinterLocation = getDefaultPrinterLocation(),
                Brand = getDefaultBrand()
            };
            return item;
        }

        public async void SaveDefaults() {
            var epqScore = await GetAdditionalScoreData();
            if (epqScore.NewInstance)
                return;
            var config = Config.DefaultConfig;
            config.Set(makeJobKey(PrinterNameKey), epqScore.PrinterName);
            config.Set(makeJobKey(PrinterLocationKey), epqScore.PrinterLocation);
            config.Set(makeJobKey(BrandKey), epqScore.Brand);
        }

        private string makeJobKey(string baseKey) {
            return string.Format("{0}.{1}", baseKey, result.Job.Id);
        }

        private string getDefaultPrinterName() {
            var user = LoggedInUserProvider.LoggedInUser;
            var config = Config.DefaultConfig;
            var printerName = config.Get(makeJobKey(PrinterNameKey), user.CompanyName);
            return printerName;
        }

        private string getDefaultPrinterLocation() {
            var user = LoggedInUserProvider.LoggedInUser;
            var config = Config.DefaultConfig;
            var printerLocation = config.Get(makeJobKey(PrinterLocationKey), user.CompanyCity);
            return printerLocation;
        }

        private string getDefaultBrand() {
            var config = Config.DefaultConfig;
            var brand = config.Get(makeJobKey(BrandKey), "");
            return brand;
        }

        public void CalculateFinalScore() {
            foreach (var category in EpqCompareScoreCategories) {
                category.CalculateScore();
            }

            var scorePartWithoutMissingScore = 1.0;
            if (!HasVisual)
                scorePartWithoutMissingScore -= EpqScoreCalculator.ScoreVisualPart;
            if (!HasRegistration)
                scorePartWithoutMissingScore -= EpqScoreCalculator.ScoreRegistrationPart;
            var scoreMultiplierDueToMissingScores = 1.0 / scorePartWithoutMissingScore;

            var deltaE100Score = calculateDeltaE100Score();
            var deltaE50Score = calculateDeltaE50Score();
            var tvi50Score = calculateTvi50Score();
            var registrationScore = calculateRegistrationScore();
            var visualScore = calculateVisualScore();
            var newFinalScore =
                deltaE100Score * EpqScoreCalculator.ScoreDeltaE100Part * scoreMultiplierDueToMissingScores +
                deltaE50Score * EpqScoreCalculator.ScoreDeltaE50Part * scoreMultiplierDueToMissingScores +
                tvi50Score * EpqScoreCalculator.ScoreTvi50Part * scoreMultiplierDueToMissingScores;
            if (HasVisual)
                newFinalScore += visualScore * EpqScoreCalculator.ScoreVisualPart * scoreMultiplierDueToMissingScores;
            if (HasRegistration)
                newFinalScore += registrationScore * EpqScoreCalculator.ScoreRegistrationPart *
                                 scoreMultiplierDueToMissingScores;
            FinalScore = newFinalScore;

            calculateFinalResult();
            InvokePropertyChanged(string.Empty);
        }

        private void calculateFinalResult() {
            FinalRawResult = EpqScoreCalculator.CalculateFinalResult(FinalScore);
            FinalResult = EpqScoreCalculator.EpqScoreToStripCompareScore(FinalRawResult);
        }

        private async void updateRegistrationScore() {
            categoryRegistration.Clear();
            var epqScore = await GetAdditionalScoreData();
            if (epqScore == null)
                return;
            var registrations = epqScore.RegistrationsAsDict;
            foreach (var registration in registrations) {
                var patch = findSolidWithSlot(registration.Key);
                var entry = EpqScoreCalculator.CalculateRegistrationScore(patch, registration.Value);
                categoryRegistration.AddEntry(entry);
            }
        }

        private double calculateRegistrationScore() {
            updateRegistrationScore();
            return CategoryRegistration.Score;
        }

        private async void updateVisualScore() {
            categoryVisual.Score = 0.0;
            var epqScore = await GetAdditionalScoreData();
            if (epqScore == null)
                return;
            categoryVisual.Score = epqScore.VisualScore;
            categoryVisual.RawResult = EpqScoreCalculator.CalculateVisualScore(CategoryVisual.Score).RawResult;
        }

        private double calculateVisualScore() {
            updateVisualScore();
            return CategoryVisual.Score;
        }

        private double calculateDeltaE100Score() {
            return CategoryDeltaE100.Score;
        }

        private double calculateDeltaE50Score() {
            return CategoryDeltaE50.Score;
        }

        private double calculateTvi50Score() {
            return CategoryTvi.Score;
        }

        private IStripComparePatch findSolidWithSlot(int slot) {
            return result.SummaryPatches.Patches.FirstOrDefault(patch => patch.IsSolid && patch.SingleSlot == slot);
        }

        #region Implementation of INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        public void InvokePropertyChanged(string property) {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(property));
        }
        #endregion

        /// <summary>
        /// Creates a new instance, copying over all data to this instance. The additional data will also be cloned.
        /// </summary>
        /// <returns></returns>
        public async Task<IEpqStripCompareScore> Clone() {
            var instance = new EpqStripCompareScore(result, (await GetAdditionalScoreData()).Clone<IEpqScore>());
            return instance;
        }
    }
}