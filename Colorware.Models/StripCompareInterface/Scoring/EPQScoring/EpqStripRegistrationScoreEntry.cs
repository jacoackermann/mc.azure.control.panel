using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring;
using Colorware.Core.Enums;

namespace Colorware.Models.StripCompareInterface.Scoring.EPQScoring {
    public class EpqStripRegistrationScoreEntry {
        public enum RegistrationScoreResult {
            Fail,
            Poor,
            Satisfactory,
            Good,
            Excellent
        }

        public IStripComparePatch Solid { get; set; }
        public double Score { get; set; }
        public EpqScoreResult RawResult { get; set; }

        public StripCompareScoreResult Result {
            get { return EpqScoreCalculator.EpqScoreToStripCompareScore(RawResult); }
        }

        public EpqStripRegistrationScoreEntry(IStripComparePatch solid, double score, EpqScoreResult scoreResult) {
            Solid = solid;
            Score = score;
            RawResult = scoreResult;
        }
    }
}