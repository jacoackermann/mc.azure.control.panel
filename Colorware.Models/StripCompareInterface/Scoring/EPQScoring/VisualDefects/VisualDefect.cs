using Colorware.Core.Globalization;

namespace Colorware.Models.StripCompareInterface.Scoring.EPQScoring.VisualDefects {
    /// <summary>
    /// Represents a single visual defect. May want to move this to the database at some time as a model.
    /// </summary>
    public class VisualDefect {
        public string Tag { get; }

        public string TranslationKey { get; }

        public string Name => LanguageManager._(TranslationKey);

        public VisualDefectCategory Category { get; set; }

        public VisualDefect(string tag, string translationKey) {
            Tag = tag;
            TranslationKey = translationKey;
        }
    }
}