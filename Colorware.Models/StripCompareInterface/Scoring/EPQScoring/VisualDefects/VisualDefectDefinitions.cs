﻿using System.Collections.Generic;

namespace Colorware.Models.StripCompareInterface.Scoring.EPQScoring.VisualDefects {
    public static class VisualDefectDefinitions {
        private static List<VisualDefectCategory> categories;

        public static List<VisualDefectCategory> Categories {
            get {
                if (categories == null)
                    buildCategories();
                return categories;
            }
        }

        private static void buildCategories() {
            categories = new List<VisualDefectCategory>();
            buildArtworkCategory();
            buildColorMatchingCategory();
            buildInkPerformanceCategory();
            buildPrintClarityCategory();
            buildSubstrateCategory();
        }

        public static VisualDefect FindVisualDefectByTag(string tag) {
            foreach (var category in Categories) {
                var defect = category.FindVisualDefectByTag(tag);
                if (defect != null)
                    return defect;
            }
            return null;
        }

        private static void buildInkPerformanceCategory() {
            var category = new VisualDefectCategory("InkPerformance", "FM.VisualDefects.Categories.InkPerformance");
            categories.Add(category);

            category.Add(new VisualDefect("Adhesion", "FM.VisualDefects.Categories.Adhesion"));
            category.Add(new VisualDefect("Bleed", "FM.VisualDefects.Categories.Bleed"));
            category.Add(new VisualDefect("Blocking", "FM.VisualDefects.Categories.Blocking"));
            category.Add(new VisualDefect("Brittleness", "FM.VisualDefects.Categories.Brittleness"));
            category.Add(new VisualDefect("CellPluggingOrDryingIn", "FM.VisualDefects.Categories.CellPluggingOrDryingIn"));
            category.Add(new VisualDefect("UVCuring", "FM.VisualDefects.Categories.UVCuring"));
            category.Add(new VisualDefect("InkDryingFast", "FM.VisualDefects.Categories.InkDryingFast"));
            category.Add(new VisualDefect("InkDryingSlow", "FM.VisualDefects.Categories.InkDryingSlow"));
            category.Add(new VisualDefect("InkPiling", "FM.VisualDefects.Categories.InkPiling"));
            category.Add(new VisualDefect("InkTrapping", "FM.VisualDefects.Categories.InkTrapping"));
            category.Add(new VisualDefect("Odor", "FM.VisualDefects.Categories.Odor"));
            category.Add(new VisualDefect("PickingOrLinting", "FM.VisualDefects.Categories.PickingOrLinting"));
            category.Add(new VisualDefect("PoorInkTransfer", "FM.VisualDefects.Categories.PoorInkTransfer"));
            category.Add(new VisualDefect("PoorCureOrInadequateDrying",
                "FM.VisualDefects.Categories.PoorCureOrInadequateDrying"));
            category.Add(new VisualDefect("PinHolingOrFisheyes", "FM.VisualDefects.Categories.PinHolingOrFisheyes"));
            category.Add(new VisualDefect("PoorRubOrScuffResistance",
                "FM.VisualDefects.Categories.PoorRubOrScuffResistance"));
            category.Add(new VisualDefect("ScreeningInk", "FM.VisualDefects.Categories.ScreeningInk"));
            category.Add(new VisualDefect("Slinging", "FM.VisualDefects.Categories.Slinging"));
            category.Add(new VisualDefect("Smearing", "FM.VisualDefects.Categories.Smearing"));
            category.Add(new VisualDefect("Snowflaking", "FM.VisualDefects.Categories.Snowflaking"));
            category.Add(new VisualDefect("Streaks", "FM.VisualDefects.Categories.Streaks"));
            category.Add(new VisualDefect("Striations", "FM.VisualDefects.Categories.Striations"));
            category.Add(new VisualDefect("StrikeThrough", "FM.VisualDefects.Categories.StrikeThrough"));
            category.Add(new VisualDefect("WeakPatchyPrintInk", "FM.VisualDefects.Categories.WeakPatchyPrintInk"));
        }

        private static void buildColorMatchingCategory() {
            var category = new VisualDefectCategory("ColorMatching", "FM.VisualDefects.Categories.ColorMatching");
            categories.Add(category);

            category.Add(new VisualDefect("ColorBalance", "FM.VisualDefects.Categories.ColorBalance"));
            category.Add(new VisualDefect("ColorVariationDeltaE", "FM.VisualDefects.Categories.ColorVariationDeltaE"));
            category.Add(new VisualDefect("Density", "FM.VisualDefects.Categories.Density"));
            category.Add(new VisualDefect("DotGain", "FM.VisualDefects.Categories.DotGain"));
            category.Add(new VisualDefect("OverallVisual", "FM.VisualDefects.Categories.OverallVisual"));
        }

        private static void buildSubstrateCategory() {
            var category = new VisualDefectCategory("Substrate", "FM.VisualDefects.Categories.Substrate");
            categories.Add(category);

            category.Add(new VisualDefect("Creasing", "FM.VisualDefects.Categories.Creasing"));
            category.Add(new VisualDefect("DefectInSubstrate", "FM.VisualDefects.Categories.DefectInSubstrate"));
            category.Add(new VisualDefect("HickeysOrSpotsSub", "FM.VisualDefects.Categories.HickeysOrSpotsSub"));
            category.Add(new VisualDefect("FeatheringWhiskering", "FM.VisualDefects.Categories.FeatheringWhiskering"));
            category.Add(new VisualDefect("InkBuildUp", "FM.VisualDefects.Categories.InkBuildUp"));
            category.Add(new VisualDefect("Wrinkling", "FM.VisualDefects.Categories.Wrinkling"));
        }

        private static void buildArtworkCategory() {
            var category = new VisualDefectCategory("Artwork", "FM.VisualDefects.Categories.Artwork");
            categories.Add(category);

            category.Add(new VisualDefect("BarcodeLegibilityArt", "FM.VisualDefects.Categories.BarcodeLegibilityArt"));
            category.Add(new VisualDefect("DefectivePrintTool", "FM.VisualDefects.Categories.DefectivePrintTool"));
            category.Add(new VisualDefect("MoireOrAngleConflict", "FM.VisualDefects.Categories.MoireOrAngleConflict"));
            category.Add(new VisualDefect("UPCIssuesArt", "FM.VisualDefects.Categories.UPCIssuesArt"));
            category.Add(new VisualDefect("WrongInk", "FM.VisualDefects.Categories.WrongInk"));
            category.Add(new VisualDefect("WrongIPMSNumber", "FM.VisualDefects.Categories.WrongIPMSNumber"));
        }

        private static void buildPrintClarityCategory() {
            var category = new VisualDefectCategory("PrintClarity", "FM.VisualDefects.Categories.PrintClarity");
            categories.Add(category);

            category.Add(new VisualDefect("AniloxLines", "FM.VisualDefects.Categories.AniloxLines"));
            category.Add(new VisualDefect("Backtrapping", "FM.VisualDefects.Categories.Backtrapping"));
            category.Add(new VisualDefect("BarcodeLegibilityPrint", "FM.VisualDefects.Categories.BarcodeLegibilityPrint"));
            category.Add(new VisualDefect("ChatterOrGearingOrBarring",
                "FM.VisualDefects.Categories.ChatterOrGearingOrBarring"));
            category.Add(new VisualDefect("DirtyPrint", "FM.VisualDefects.Categories.DirtyPrint"));
            category.Add(new VisualDefect("DotBridging", "FM.VisualDefects.Categories.DotBridging"));
            category.Add(new VisualDefect("FillingIn", "FM.VisualDefects.Categories.FillingIn"));
            category.Add(new VisualDefect("Fluting", "FM.VisualDefects.Categories.Fluting"));
            category.Add(new VisualDefect("Ghosting", "FM.VisualDefects.Categories.Ghosting"));
            category.Add(new VisualDefect("Halo", "FM.VisualDefects.Categories.Halo"));
            category.Add(new VisualDefect("HickeysOrSpots", "FM.VisualDefects.Categories.HickeysOrSpots"));
            category.Add(new VisualDefect("HighSpot", "FM.VisualDefects.Categories.HighSpot"));
            category.Add(new VisualDefect("Misting", "FM.VisualDefects.Categories.Misting"));
            category.Add(new VisualDefect("Moire", "FM.VisualDefects.Categories.Moire"));
            category.Add(new VisualDefect("MoireOrAngleConflictAnilox",
                "FM.VisualDefects.Categories.MoireOrAngleConflictAnilox"));
            category.Add(new VisualDefect("MottledPrint", "FM.VisualDefects.Categories.MottledPrint"));
            category.Add(new VisualDefect("Offset", "FM.VisualDefects.Categories.Offset"));
            category.Add(new VisualDefect("OverImpression", "FM.VisualDefects.Categories.OverImpression"));
            category.Add(new VisualDefect("PlateBounce", "FM.VisualDefects.Categories.PlateBounce"));
            category.Add(new VisualDefect("PlateLift", "FM.VisualDefects.Categories.PlateLift"));
            category.Add(new VisualDefect("PlateWear", "FM.VisualDefects.Categories.PlateWear"));
            category.Add(new VisualDefect("PrintOrClarityIssues", "FM.VisualDefects.Categories.PrintOrClarityIssues"));
            category.Add(new VisualDefect("RegistrationOrMisregistration",
                "FM.VisualDefects.Categories.RegistrationOrMisregistration"));
            category.Add(new VisualDefect("ScreeningPrint", "FM.VisualDefects.Categories.ScreeningPrint"));
            category.Add(new VisualDefect("ScummingOrTinting", "FM.VisualDefects.Categories.ScummingOrTinting"));
            category.Add(new VisualDefect("ScummingMinor", "FM.VisualDefects.Categories.ScummingMinor"));
            category.Add(new VisualDefect("ScummingMajor", "FM.VisualDefects.Categories.ScummingMajor"));
            category.Add(new VisualDefect("SkipOut", "FM.VisualDefects.Categories.SkipOut"));
            category.Add(new VisualDefect("Slur", "FM.VisualDefects.Categories.Slur"));
            category.Add(new VisualDefect("Tracking", "FM.VisualDefects.Categories.Tracking"));
            category.Add(new VisualDefect("UnwantedPrint", "FM.VisualDefects.Categories.UnwantedPrint"));
            category.Add(new VisualDefect("UPCIssuesPrint", "FM.VisualDefects.Categories.UPCIssuesPrint"));
            category.Add(new VisualDefect("WeakPatchyPrintClarity", "FM.VisualDefects.Categories.WeakPatchyPrintClarity"));
        }
    }
}