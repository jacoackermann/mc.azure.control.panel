using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Globalization;

namespace Colorware.Models.StripCompareInterface.Scoring.EPQScoring.VisualDefects {
    public class VisualDefectCategory {
        private readonly string tag;
        private readonly string translationKey;
        private readonly List<VisualDefect> visualDefects;

        public string Tag {
            get { return tag; }
        }

        public string TranslationKey {
            get { return translationKey; }
        }

        public string Name {
            get { return LanguageManager._(TranslationKey); }
        }

        public List<VisualDefect> VisualDefects {
            get { return visualDefects; }
        }

        public VisualDefectCategory(string tag, string translationKey, IEnumerable<VisualDefect> visualDefects) {
            this.tag = tag;
            this.translationKey = translationKey;
            this.visualDefects = new List<VisualDefect>(visualDefects);
        }

        public VisualDefectCategory(string tag, string translationKey) {
            this.tag = tag;
            this.translationKey = translationKey;
            this.visualDefects = new List<VisualDefect>();
        }

        public void Add(VisualDefect defect) {
            defect.Category = this;
            VisualDefects.Add(defect);
        }

        /// <summary>
        /// Find visual defect by the given tag. If this category does not contain the defect with this tag, null is
        /// returned.
        /// Note, could probably be more efficient.
        /// </summary>
        /// <param name="tagToFind">Tag to search for.</param>
        /// <returns>The defect instance with the given tag, null if it could not be found.</returns>
        public VisualDefect FindVisualDefectByTag(string tagToFind) {
            return VisualDefects.FirstOrDefault(vd => vd.Tag == tagToFind);
        }
    }
}