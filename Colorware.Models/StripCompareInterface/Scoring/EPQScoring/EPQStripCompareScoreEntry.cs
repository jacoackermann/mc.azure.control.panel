using System.Collections.Generic;

using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring.Base;
using Colorware.Core.Enums;

namespace Colorware.Models.StripCompareInterface.Scoring.EPQScoring {
    public class EpqStripCompareScoreEntry : IStripCompareScoreEntry {
        #region Implementation of IStripCompareScorePatch
        private readonly IStripComparePatch patch;

        public IStripComparePatch Patch {
            get { return patch; }
        }

        private readonly double score;

        public double Score {
            get { return score; }
        }

        public StripCompareScoreResult ScoreResult {
            get { return EpqScoreCalculator.EpqScoreToStripCompareScore(RawResult); }
        }

        public string ResultGrade {
            get { return RawResult.ToString(); }
        }

        private readonly EpqScoreResult rawResult;

        public EpqScoreResult RawResult {
            get { return rawResult; }
        }

        private readonly List<StripCompareScoreDefect> defects;

        public IEnumerable<IStripCompareScoreDefect> Defects {
            get { return defects; }
        }
        #endregion

        public EpqStripCompareScoreEntry(IStripComparePatch patch, double score, EpqScoreResult rawResult) {
            defects = new List<StripCompareScoreDefect>();
            this.patch = patch;
            this.score = score;
            this.rawResult = rawResult;
        }
    }
}