using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Data.Models.StripCompareInterface.Scoring;
using Colorware.Core.Enums;

namespace Colorware.Models.StripCompareInterface.Scoring.EPQScoring {
    public class EpqRegistrationScoreCategory : IStripCompareScoreCategory {
        #region Implementation of IStripCompareScoreCategory
        private readonly string name;

        public string Name {
            get { return name; }
        }

        public double Score {
            get {
                var numItems = (double)entries.Count;
                if (numItems <= 0)
                    return 0.0;
                var scoreSum = entries.Sum(item => item.Score);
                return scoreSum / numItems;
            }
        }

        public int Identifier {
            get { return -1; }
        }

        public StripCompareScoreResult Result {
            get { return EpqScoreCalculator.EpqScoreToStripCompareScore(RawResult); }
        }

        public EpqScoreResult RawResult {
            get {
                var worstResult = getWorstResult();
                return worstResult;
            }
        }

        public bool IsOk {
            get { return Result == StripCompareScoreResult.Pass; }
        }

        public bool IsWarning {
            get { return Result == StripCompareScoreResult.Warning; }
        }

        public bool IsFail {
            get { return Result == StripCompareScoreResult.Fail; }
        }

        private List<EpqStripRegistrationScoreEntry> entries;

        public IEnumerable<IStripCompareScoreEntry> Entries {
            get {
                var result = new List<IStripCompareScoreEntry>();
                foreach (var entry in entries) {
                    var newEntry = new EpqStripCompareScoreEntry(null, entry.Score, entry.RawResult);
                    result.Add(newEntry);
                }
                return result;
            }
        }

        public void CalculateScore() {
            throw new System.NotImplementedException();
        }
        #endregion

        public EpqRegistrationScoreCategory(string name) {
            this.name = name;
            entries = new List<EpqStripRegistrationScoreEntry>();
        }

        public void Clear() {
            entries.Clear();
        }

        public void AddEntry(EpqStripRegistrationScoreEntry entry) {
            entries.Add(entry);
        }

        private EpqScoreResult getWorstResult() {
            var worstFound = EpqScoreResult.Excellent;
            foreach (var patch in entries) {
                if (patch.RawResult < worstFound)
                    worstFound = patch.RawResult;
            }
            return worstFound;
        }
    }
}