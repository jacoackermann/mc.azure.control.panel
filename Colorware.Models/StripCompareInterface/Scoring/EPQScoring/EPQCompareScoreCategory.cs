using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Colorware.Core.Data.Models.StripCompareInterface.Scoring;
using Colorware.Core.Enums;

namespace Colorware.Models.StripCompareInterface.Scoring.EPQScoring {
    public class EpqCompareScoreCategory : IStripCompareScoreCategory {
        #region Implementation of IStripCompareScoreCategory
        private readonly string name;

        public string Name {
            get { return name; }
        }

        private double? score;

        public double Score {
            get {
                if (!score.HasValue)
                    CalculateScore();
                Debug.Assert(score.HasValue);
                return score.HasValue ? score.Value : 0.0;
            }
        }

        private readonly PatchTypes patchType;

        public int Identifier {
            get { return (int)patchType; }
        }

        public StripCompareScoreResult Result {
            get { return EpqScoreCalculator.EpqScoreToStripCompareScore(RawResult); }
        }

        public EpqScoreResult RawResult {
            get {
                var worstResult = getWorstResult();
                return worstResult;
            }
        }

        public bool IsOk {
            get { return Result == StripCompareScoreResult.Pass; }
        }

        public bool IsWarning {
            get { return Result == StripCompareScoreResult.Warning; }
        }

        public bool IsFail {
            get { return Result == StripCompareScoreResult.Fail; }
        }

        private readonly List<EpqStripCompareScoreEntry> entries;

        public IEnumerable<IStripCompareScoreEntry> Entries {
            get { return entries; }
        }

        public IEnumerable<EpqStripCompareScoreEntry> RawEntries {
            get { return entries; }
        }
        #endregion

        public EpqCompareScoreCategory(string name, PatchTypes patchType) {
            this.name = name;
            this.patchType = patchType;
            entries = new List<EpqStripCompareScoreEntry>();
        }

        public void AddEntry(EpqStripCompareScoreEntry entry) {
            entries.Add(entry);
        }

        private EpqScoreResult getWorstResult() {
            var worstFound = EpqScoreResult.Excellent;
            foreach (var patch in entries) {
                if (patch.RawResult < worstFound)
                    worstFound = patch.RawResult;
            }
            return worstFound;
        }

        public void Clear() {
            entries.Clear();
            CalculateScore();
        }

        public void CalculateScore() {
            var numItems = (double)entries.Count;
            if (numItems <= 0) {
                score = 0.0;
                return;
            }
            var scoreSum = entries.Sum(entry => entry.Score);
            score = scoreSum / numItems;
        }
    }
}