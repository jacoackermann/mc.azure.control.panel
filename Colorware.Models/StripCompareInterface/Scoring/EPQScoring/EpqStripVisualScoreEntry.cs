using Colorware.Core.Data.Models.StripCompareInterface.Scoring;
using Colorware.Core.Enums;

namespace Colorware.Models.StripCompareInterface.Scoring.EPQScoring {
    public class EpqStripVisualScoreEntry {
        public double Score { get; set; }
        public EpqScoreResult RawResult { get; set; }

        public StripCompareScoreResult Result {
            get { return EpqScoreCalculator.EpqScoreToStripCompareScore(RawResult); }
        }

        public enum VisualScoreResult {
            Fail,
            Poor,
            Satisfactory,
            Good,
            Excellent
        }

        public EpqStripVisualScoreEntry(double score, EpqScoreResult visualScoreResult) {
            Score = score;
            RawResult = visualScoreResult;
        }
    }
}