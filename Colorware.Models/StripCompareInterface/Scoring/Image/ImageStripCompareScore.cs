using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Data.Models.StripCompareInterface.Scoring;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring;

using JetBrains.Annotations;

namespace Colorware.Models.StripCompareInterface.Scoring.Image {
    public class ImageStripCompareScore : IStripCompareScore {
        #region Properties
        // Not used in Image
        public IEnumerable<IStripCompareScoreCategory> Categories {
            get { return new List<IStripCompareScoreCategory>(); }
        }

        // Not used in Image
        public IEnumerable<IStripCompareScoreEntry> PatchResults {
            get { return new List<IStripCompareScoreEntry>(); }
        }

        public double FinalScore { get; private set; }
        #endregion

        public ImageStripCompareScore([NotNull] StripCompareSummary summary) {
            if (summary == null)
                throw new ArgumentNullException("summary");

            FinalScore = ((double)summary.CompareResult.RawPatches.Patches.Count(p => p.InTolerance) /
                          summary.CompareResult.RawPatches.Patches.Count) * 100;
        }
    }
}