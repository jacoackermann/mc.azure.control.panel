using System.Collections.Generic;

namespace Colorware.Models.Epq {
    public static class VisualDefectListDeserializer {
        public static IEnumerable<string> Deserialize(string input) {
            if (input == null)
                return new List<string>();
            var parts = input.Split(new[] {VisualDefectListSerializer.Separator});
            return parts;
        }
    }
}