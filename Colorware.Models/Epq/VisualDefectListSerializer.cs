using System.Collections.Generic;
using System.Linq;

namespace Colorware.Models.Epq {
    public static class VisualDefectListSerializer {
        public const char Separator = ';';

        public static string Serialize(IEnumerable<string> items) {
            var result = string.Join(Separator.ToString(), items.ToArray());
            return result;
        }
    }
}