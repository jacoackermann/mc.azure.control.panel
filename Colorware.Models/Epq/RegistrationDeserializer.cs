using System;
using System.Collections.Generic;

using Colorware.Core.Logging;

namespace Colorware.Models.Epq {
    public static class RegistrationDeserializer {
        public static Dictionary<int, int> Deserialize(string input) {
            var result = new Dictionary<int, int>();
            if (input == null)
                return result;
            var parts = input.Split(new[] {RegistrationSerializer.PartSeparator});
            var itemSplitter = new[] {RegistrationSerializer.ItemSeparator};
            foreach (var part in parts) {
                var sections = part.Split(itemSplitter);
                if (sections.Length != 2)
                    continue;
                try {
                    var slot = int.Parse(sections[0]);
                    var score = int.Parse(sections[1]);
                    result[slot] = score;
                }
                catch (Exception e) {
                    LogManager.GetLogger(typeof(RegistrationSerializer)).Error(e.Message, e);
                }
            }
            return result;
        }
    }
}