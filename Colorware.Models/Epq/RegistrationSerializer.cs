using System.Collections.Generic;
using System.Globalization;

namespace Colorware.Models.Epq {
    public static class RegistrationSerializer {
        public const char PartSeparator = ':';
        public const char ItemSeparator = ',';

        /// <summary>
        /// Serializes a dictionary of registration items toa  string.
        /// </summary>
        /// <param name="items">A dictionary of items with [slotNumber, score] items.</param>
        /// <returns>The serialized string.</returns>
        public static string Serialize(Dictionary<int, int> items) {
            var parts = new List<string>();
            foreach (var item in items) {
                parts.Add(item.Key.ToString(CultureInfo.InvariantCulture) + ItemSeparator +
                          item.Value.ToString(CultureInfo.InvariantCulture));
            }
            var result = string.Join(PartSeparator.ToString(), parts.ToArray());
            return result;
        }
    }
}