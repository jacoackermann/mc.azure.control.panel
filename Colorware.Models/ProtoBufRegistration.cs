﻿using System;
using System.Linq;

using Castle.DynamicProxy.Internal;

using Colorware.Core.Authentication;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Helpers;
using Colorware.Core.Security;
using Colorware.Core.WebApi.ProtoBufSurrogates;
using Colorware.Models.Security;
using Colorware.Models.ServiceModels;

using ProtoBuf.Meta;

namespace Colorware.Models {
    public static class ProtoBufRegistration {
        public static bool HasRegisteredModels { get; private set; }

        public static void RegisterModels() {
            var iBaseModelMetaType = RuntimeTypeModel.Default.Add(typeof(IBaseModel), false);

            var modelTypes =
                typeof(Job).Assembly.GetExportedTypes()
                           .Where(t => typeof(IBaseModel).IsAssignableFrom(t))
                           .OrderBy(t => t.Name);

            var classId = 1;
            foreach (var modelType in modelTypes) {
                // Register model type
                var modelMetaType = RuntimeTypeModel.Default.Add(modelType, false);

                var modelInterfaceType = modelType.GetAllInterfaces()
                                                  .FirstOrDefault(
                                                      i => i.GetAllInterfaces().Contains(typeof(IBaseModel)));
                var modelInterfaceMetaType = RuntimeTypeModel.Default.Add(modelInterfaceType, false);

                if (modelInterfaceType != null && modelInterfaceType != typeof(IBaseModel)) {
                    // Register IModel as child of IBaseModel
                    iBaseModelMetaType.AddSubType(classId, modelInterfaceType);
                    // Register Model as child of IModel
                    modelInterfaceMetaType.AddSubType(classId, modelType);
                }
                else {
                    // Register Model as child of IBaseModel
                    iBaseModelMetaType.AddSubType(classId, modelType);
                }

                // Register the NewInstance property
                modelMetaType.AddField(1, nameof(IBaseModel.NewInstance));

                // Register all properties with ServiceField attribute
                var serviceProperties =
                    modelType.GetProperties().Where(p => p.HasAttribute<ServiceField>()).OrderBy(p => p.Name);

                var memberId = 2;
                foreach (var serviceProperty in serviceProperties) {
                    modelMetaType.AddField(memberId, serviceProperty.Name);
                    memberId++;
                }

                // Register OriginGuid for IReference and IReferencePatch types
                if (typeof(IReference).IsAssignableFrom(modelType) ||
                    typeof(IReferencePatch).IsAssignableFrom(modelType)) {
                    modelMetaType.AddField(memberId, nameof(IReference.OriginTypeGuid));
                }

                classId++;
            }

            // Register User as concrete type for IUser
            RuntimeTypeModel.Default.Add(typeof(User), true);
            var userInterfaceType = RuntimeTypeModel.Default.Add(typeof(IUser), true);
            userInterfaceType.AddSubType(classId++, typeof(User));

            // Register user profile entries
            RuntimeTypeModel.Default.Add(typeof(LoginUserProfileEntry), true);
            var loginUserProfileEntryInterfaceType = RuntimeTypeModel.Default.Add(typeof(ILoginUserProfileEntry), true);
            loginUserProfileEntryInterfaceType.AddSubType(classId++, typeof(LoginUserProfileEntry));

            // Register translation override entries
            RuntimeTypeModel.Default.Add(typeof(TranslationOverrideEntry), true);
            var translationOverrideEntryInterfaceType =
                RuntimeTypeModel.Default.Add(typeof(ITranslationOverrideEntry), true);
            translationOverrideEntryInterfaceType.AddSubType(classId++, typeof(TranslationOverrideEntry));

            // Register StorageWarningCheckerResultDto
            RuntimeTypeModel.Default.Add(typeof(StorageWarningCheckerResultDto), true);
            var storageWarningCheckerResultDtoType =
                RuntimeTypeModel.Default.Add(typeof(IStorageWarningCheckerResultDto), true);
            storageWarningCheckerResultDtoType.AddSubType(classId, typeof(StorageWarningCheckerResultDto));

            // Register surrogates
            // (see: https://stackoverflow.com/questions/7046506/can-i-serialize-arbitrary-types-with-protobuf-net/7046868)
            RuntimeTypeModel.Default.Add(typeof(DateTimeOffset), false).SetSurrogate(typeof(DateTimeOffsetSurrogate));

            // Done, compile for speed
            RuntimeTypeModel.Default.CompileInPlace();

            HasRegisteredModels = true;
        }
    }
}