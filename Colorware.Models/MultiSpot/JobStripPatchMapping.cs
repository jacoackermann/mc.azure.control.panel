using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;
using System.Linq;

using Colorware.Core.Color;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.Compare;
using Colorware.Core.Functional.Option;
using Colorware.Core.Mvvm;
using Colorware.Models.MultiSpot.Averaging;

using JetBrains.Annotations;

namespace Colorware.Models.MultiSpot {
    /// <summary>
    /// Container for keeping track of samples that are associated with
    /// a JobStripPatch.
    /// </summary>
    public class JobStripPatchMapping : DefaultNotifyPropertyChanged {
        #region Properties
        [NotNull]
        public IJobStripPatch JobStripPatch { get; }

        public event EventHandler SampleAdded;

        public virtual void InvokeSampleAdded() {
            SampleAdded?.Invoke(this, EventArgs.Empty);
        }

        [NotNull]
        public IToleranceSet ToleranceSet { get; }
        
        // Was insanely hard/impossible to do from the code behind where this is subscribed to
        public void RemoveAllSampleAddedHandlers() {
            SampleAdded = null;
        }


        public IMeasurementConditionsConfig MeasurementConditionsConfig { get; }

        private IImmutableList<ISample> samples = ImmutableList<ISample>.Empty;

        [ItemNotNull, NotNull]
        public IImmutableList<ISample> Samples {
            get { return samples; }
            private set {
                // Will need to recalculate averages.
                averageResult = null;

                samples = value;

                OnPropertyChanged(nameof(Samples));
            }
        }

        public bool HasPaperwhiteSample => PaperwhiteSample != null;

        private ISample paperwhiteSample;

        public ISample PaperwhiteSample {
            get { return paperwhiteSample; }
            set {
                paperwhiteSample = value;
                if (paperwhiteSample != null) {
                    foreach (var result in CompareResults) {
                        result.InitChromaTrackPredictedDensities(paperwhiteSample.AsSpectrum,
                                                                 MeasurementConditionsConfig);
                    }
                    ResetAverageResult();
                }
                OnPropertyChanged("PaperwhiteSample");
                OnPropertyChanged("HasPaperwhiteSample");
            }
        }

        public IEnumerable<NamedLab> ILabConvertibleSamples {
            get {
                var result = new List<NamedLab>();
                var i = 1;
                foreach (var sample in Samples) {
                    result.Add(new NamedLab(sample.AsLab, i.ToString(CultureInfo.InvariantCulture)));
                    i++;
                }
                return result;
            }
        }

        private IImmutableList<ColorComparer<IJobStripPatch>> compareResults =
            ImmutableList<ColorComparer<IJobStripPatch>>.Empty;

        [ItemNotNull, NotNull]
        public IImmutableList<ColorComparer<IJobStripPatch>> CompareResults {
            get { return compareResults; }
            private set {
                compareResults = value;
                OnPropertyChanged(nameof(CompareResults));
            }
        }

        private AverageCustomColorResult averageResult;

        public AverageCustomColorResult AverageResult => averageResult ?? (averageResult = calculateAverageResult());

        public void ResetAverageResult() {
            averageResult = null;
            OnPropertyChanged("AverageResult");
        }

        public int NumberOfSamples => CompareResults.Count;

        public int NumberOfOkSamples {
            get { return CompareResults.Count(cr => cr.ToleranceReport.InTolerance); }
        }

        public int NumberOfFailedSamples {
            get { return CompareResults.Count(cr => !cr.ToleranceReport.InTolerance); }
        }

        public double PercentageOk {
            get {
                if (NumberOfSamples == 0)
                    return 0;
                return (100.0 / NumberOfSamples) * NumberOfOkSamples;
            }
        }
        #endregion

        public JobStripPatchMapping([NotNull] IJobStripPatch jobStripPatch, IToleranceSet toleranceSet,
                                    IMeasurementConditionsConfig measurementConditionsConfig) {
            if (jobStripPatch == null) throw new ArgumentNullException(nameof(jobStripPatch));

            ToleranceSet = toleranceSet;
            MeasurementConditionsConfig = measurementConditionsConfig;
            JobStripPatch = jobStripPatch;
        }


        public void AddSample([NotNull] ISample sample) {
            if (sample == null) throw new ArgumentNullException(nameof(sample));

            Samples = Samples.Add(sample);
            var result = new ColorComparer<IJobStripPatch>(sample, JobStripPatch, ToleranceSet, JobStripPatch.DeltaEToleranceOverride.ToOption(_ => JobStripPatch.UseDeltaEToleranceOverride));

            if (PaperwhiteSample != null && PaperwhiteSample.HasSpectrum)
                result.InitChromaTrackPredictedDensities(PaperwhiteSample.AsSpectrum, MeasurementConditionsConfig);

            CompareResults = CompareResults.Add(result);
            OnPropertyChanged(string.Empty);
            InvokeSampleAdded();
        }

        public void ClearSamples() {
            Samples = ImmutableList<ISample>.Empty;
            CompareResults = ImmutableList<ColorComparer<IJobStripPatch>>.Empty;
            OnPropertyChanged(string.Empty);
        }

        public void ClearSample(ISample sample) {
            Samples = Samples.Remove(sample);

            foreach (var result in CompareResults) {
                if (result.Sample == sample) {
                    CompareResults = CompareResults.Remove(result);
                    break;
                }
            }

            OnPropertyChanged(string.Empty);
        }

        private AverageCustomColorResult calculateAverageResult() {
            var newAverageResult = new AverageCustomColorResult(CompareResults);
            newAverageResult.Calculate();
            return newAverageResult;
        }
    }
}