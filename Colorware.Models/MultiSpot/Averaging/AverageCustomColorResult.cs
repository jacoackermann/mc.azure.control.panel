﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;

using Colorware.Core.Color;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.Compare;
using Colorware.Core.Mvvm;

namespace Colorware.Models.MultiSpot.Averaging {
    public class AverageCustomColorResult : DefaultNotifyPropertyChanged {
        /// <summary>
        /// Samples to calculate the result from.
        /// </summary>
        public IEnumerable<ColorComparer<IJobStripPatch>> SampleResults { get; private set; }

        private double deltaE;

        public double DeltaE {
            get { return deltaE; }
            set {
                deltaE = value;
                OnPropertyChanged("DeltaE");
            }
        }

        private double deltaH;

        public double DeltaH {
            get { return deltaH; }
            set {
                deltaH = value;
                OnPropertyChanged("DeltaH");
            }
        }

        private double deltaL;

        public double DeltaL {
            get { return deltaL; }
            set {
                deltaL = value;
                OnPropertyChanged("DeltaL");
            }
        }

        private double chromaPlus;

        public double ChromaPlus {
            get { return chromaPlus; }
            set {
                chromaPlus = value;
                OnPropertyChanged("ChromaPlus");
            }
        }

        private double deltaDensity;

        public double DeltaDensity {
            get { return deltaDensity; }
            set {
                deltaDensity = value;
                OnPropertyChanged("DeltaDensity");
            }
        }

        private double predictedDeltaE;

        public double PredictedDeltaE {
            get { return predictedDeltaE; }
            set {
                predictedDeltaE = value;
                OnPropertyChanged("PredictedDeltaE");
            }
        }

        private double predictedDeltaDensity;

        /// <summary>
        /// Based on ChromaTrack.
        /// </summary>
        public double PredictedDeltaDensity {
            get { return predictedDeltaDensity; }
            set {
                predictedDeltaDensity = value;
                OnPropertyChanged("PredictedDeltaDensity");
                OnPropertyChanged("PredictedRequiredDeltaDensityCorrection");
            }
        }

        private double bestPredictedDensity;
        /// <summary>
        /// Best predicted density based on ChromaTrack.
        /// </summary>
        public double BestPredictedDensity {
            get { return bestPredictedDensity; }
            set { bestPredictedDensity = value; OnPropertyChanged("BestPredictedDensity"); }
        }

        /// <summary>
        /// Same as <see cref="PredictedDeltaDensity"/>, but inverted to indicate the required correction. This can be used in value graphs so the graph
        /// will point in the right direction.
        /// </summary>
        public double PredictedRequiredDeltaDensityCorrection {
            get { return -PredictedDeltaDensity; }
        }

        private double averageDensity;

        public double AverageDensity {
            get { return averageDensity; }
            set {
                averageDensity = value;
                OnPropertyChanged("AverageDensity");
            }
        }

        private bool inTolerance;

        public bool InTolerance {
            get { return inTolerance; }
            set {
                inTolerance = value;
                OnPropertyChanged("InTolerance");
            }
        }

        private Lab lab;

        public Lab Lab {
            get { return lab; }
            set {
                lab = value;
                OnPropertyChanged("Lab");
            }
        }

        private Lab predictedBestLab;
        /// <summary>
        /// ChromaTrack calculated best lab.
        /// </summary>

        public Lab PredictedBestLab {
            get { return predictedBestLab; }
            private set {
                predictedBestLab = value;
                OnPropertyChanged("PredictedBestLab");
            }
        }

        public System.Windows.Media.Color AsColor {
            get {
                if (Lab == null)
                    return Colors.Black;
                return Lab.AsColor;
            }
        }

        private bool hasSamples;

        public bool HasSamples {
            get { return hasSamples; }
            set {
                hasSamples = value;
                OnPropertyChanged("HasSamples");
            }
        }

        private bool showInTolerance;
        /// <summary>
        /// Indicates if indicator should be shown for inside-of-tolerance situations.
        /// </summary>
        public bool ShowInTolerance {
            get { return showInTolerance; }
            set {
                showInTolerance = value;
                OnPropertyChanged("ShowInTolerance");
            }
        }

        private bool showOutsideOfTolerance;

        /// <summary>
        /// Indicates if indicator should be shown for outside-of-tolerance situations.
        /// </summary>
        public bool ShowOutsideOfTolerance {
            get { return showOutsideOfTolerance; }
            set {
                showOutsideOfTolerance = value;
                OnPropertyChanged("ShowOutsideOfTolerance");
            }
        }


        public AverageCustomColorResult(IEnumerable<ColorComparer<IJobStripPatch>> results) {
            SampleResults = results;
        }

        /// <summary>
        /// (Re)calculate the averages.
        /// </summary>
        public void Calculate() {
            // Check to see if any samples are present. If no samples
            // are present we init the data with some defaults.
            if (!SampleResults.Any()) {
                DeltaE = 0;
                DeltaH = 0;
                ChromaPlus = 0;
                DeltaDensity = 0;
                Lab = null;
                InTolerance = true;
                HasSamples = false;
                ShowInTolerance = false;
                ShowOutsideOfTolerance = false;
                return;
            }
            DeltaE = SampleResults.Average(s => s.DeltaE);
            DeltaH = SampleResults.Average(s => s.DeltaH);
            DeltaL = SampleResults.Average(s => s.DeltaLComponent);
            ChromaPlus = SampleResults.Average(s => s.ChromaPlus);
            DeltaDensity = SampleResults.Average(s => s.DeltaDensity);
            AverageDensity = SampleResults.Average(s => s.Sample.Density);

            var labs = SampleResults.Select(s => s.Sample.AsLab).ToArray();
            var l = labs.Average(asLab => asLab.L);
            var a = labs.Average(asLab => asLab.a);
            var b = labs.Average(asLab => asLab.b);
            Lab = new Lab(l, a, b, labs.First().MeasurementConditions);

            var chromaTrackSamples = SampleResults.Where(s => s.HasChromaTrack);
            if (chromaTrackSamples.Any()) {
                PredictedDeltaE = chromaTrackSamples.Average(s => s.BestChromaTrackPredictedDeltaE);
                PredictedDeltaDensity = chromaTrackSamples.Average(s => s.BestChromaTrackDensityDifference);
                var avgBestL = chromaTrackSamples.Average(s => s.Sample.L);
                var avgBestA = chromaTrackSamples.Average(s => s.Sample.a);
                var avgBestB = chromaTrackSamples.Average(s => s.Sample.b);
                PredictedBestLab = new Lab(avgBestL, avgBestA, avgBestB, Lab.MeasurementConditions);
                // TODO: Is this correct here?:
                BestPredictedDensity =
                    chromaTrackSamples.Average(s => s.Sample.Density + s.BestChromaTrackDensityDifference);
            }
            else {
                PredictedDeltaE = 0.0;
                PredictedDeltaDensity = 0.0;
                PredictedBestLab = new Lab(0, 0, 0, Lab.MeasurementConditions);
                BestPredictedDensity = 0.0;
            }

            InTolerance = SampleResults.All(s => s.ToleranceReport.InTolerance);
            ShowInTolerance = InTolerance;
            ShowOutsideOfTolerance = !InTolerance;
            HasSamples = true;
        }
    }
}