using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.Compare;
using Colorware.Core.Data.Specification;
using Colorware.Core.Extensions;
using Colorware.Core.Functional.Result;
using Colorware.Core.Helpers;
using Colorware.Core.Logging;
using Colorware.Core.Mvvm;

using JetBrains.Annotations;

namespace Colorware.Models.MultiSpot {
    /// <summary>
    /// Basically a measurement which has a number of samples associated with it.
    /// Each sample is assigned to a job strip patch in the current job when it is measured.
    /// </summary>
    public class MultiSpotSequence : DefaultNotifyPropertyChanged {
        #region Properties
        /// <summary>
        /// The Job this sequence belongs to.
        /// </summary>
        [NotNull]
        public IJob Job { get; }

        [NotNull]
        public IClient Client { get; }

        private ISample paperwhiteSample;

        public ISample PaperwhiteSample {
            get { return paperwhiteSample; }
            private set {
                paperwhiteSample = value;
                OnPropertyChanged("PaperwhiteSample");
            }
        }

        /// <summary>
        /// The measurement this which represents this sequence.
        /// </summary>
        [NotNull]
        public IMeasurement Measurement { get; }

        /// <summary>
        /// The job strip patches which belong to this job.
        /// </summary>
        [NotNull]
        public IReadOnlyList<IJobStripPatch> JobStripPatches { get; }

        /// <summary>
        /// The tolerance set to use for comparing job strip patches to samples.
        /// </summary>
        [NotNull]
        public IToleranceSet ToleranceSet { get; }

        [NotNull]
        public IMeasurementConditionsConfig MeasurementConditionsConfig { get; }

        /// <summary>
        /// Mapping of job strip patches with attached samples.
        /// </summary>
        public Dictionary<long, JobStripPatchMapping> MappedSamples { get; private set; }

        /// <summary>
        ///  Generates a list of all mappings in no particular order.
        /// </summary>
        public IEnumerable<JobStripPatchMapping> AllMappings => new List<JobStripPatchMapping>(MappedSamples.Values);

        public IEnumerable<JobStripPatchMapping> MappingsWithSamples {
            get { return new List<JobStripPatchMapping>(MappedSamples.Values.Where(m => m.Samples.Any())); }
        }

        /// <summary>
        /// Generate a list of all compare results in each mapping. Can be used for raw data list.
        /// </summary>
        public IEnumerable<ColorComparer<IJobStripPatch>> AllCompareResults {
            get {
                var result = new List<ColorComparer<IJobStripPatch>>();
                foreach (var mapping in AllMappings)
                    result.AddRange(mapping.CompareResults);

                return result;
            }
        }

        private int sequenceNumber;

        public int SequenceNumber {
            get { return sequenceNumber; }
            set {
                sequenceNumber = value;
                OnPropertyChanged("SequenceNumber");
            }
        }
        #endregion

        private MultiSpotSequence([NotNull] IJob job,
                                  [NotNull] IMeasurement measurement,
                                  [NotNull] IReadOnlyList<IJobStripPatch> jobStripPatches,
                                  [NotNull] IToleranceSet toleranceSet,
                                  [NotNull] IMeasurementConditionsConfig measurementConditionsConfig,
                                  ISample paperwhiteSample,
                                  [NotNull] IClient client) {
            if (job == null) throw new ArgumentNullException(nameof(job));
            if (measurement == null) throw new ArgumentNullException(nameof(measurement));
            if (jobStripPatches == null) throw new ArgumentNullException(nameof(jobStripPatches));
            if (toleranceSet == null) throw new ArgumentNullException(nameof(toleranceSet));
            if (measurementConditionsConfig == null) throw new ArgumentNullException(nameof(measurementConditionsConfig));
            if (client == null) throw new ArgumentNullException(nameof(client));

            Job = job;
            Client = client;
            Measurement = measurement;
            JobStripPatches = jobStripPatches;
            ToleranceSet = toleranceSet;
            MeasurementConditionsConfig = measurementConditionsConfig;

            if (MeasurementConditionsConfig == null) {
                LogManager.GetLogger(GetType()).Warn("No measurement condition given for job: " + job.Name);
                MeasurementConditionsConfig = ServiceModels.MeasurementConditionsConfig.CreateDefault("- DEFAULT -");
            }

            PaperwhiteSample = paperwhiteSample;
            initializeJobStripPatches();
        }

        [ItemNotNull]
        public static async Task<Result<MultiSpotSequence>> CreateAsync(
                                                                [NotNull] IJob job,
                                                                [NotNull] IMeasurement measurement,
                                                                [NotNull] IReadOnlyList<IJobStripPatch> jobStripPatches,
                                                                [NotNull] IToleranceSet toleranceSet,
                                                                [NotNull] IMeasurementConditionsConfig measurementConditionsConfig,
                                                                ISample paperwhiteSample,
                                                                [NotNull] IClient client) {
            try {
                var ccs = new MultiSpotSequence(job, measurement, jobStripPatches, toleranceSet, measurementConditionsConfig,
                                                paperwhiteSample, client);

                await ccs.loadDataAsync();

                return Result.Ok(ccs);
            }
            catch (Exception ex) {
                return Result.Fail<MultiSpotSequence>(ex);
            }
        }

        /// <summary>
        /// Create dictionary hashed by reference id to make it easier to find the related patches and to keep
        /// track of assigned samples.
        /// </summary>
        private void initializeJobStripPatches() {
            MappedSamples = new Dictionary<long, JobStripPatchMapping>();
            foreach (var jobStripPatch in JobStripPatches) {
                MappedSamples[jobStripPatch.Id] = new JobStripPatchMapping(jobStripPatch, ToleranceSet,
                                                                           MeasurementConditionsConfig);
            }
            updatePaperwhiteSamples();
        }

        private async Task loadDataAsync() {
            await Job.Measurements.GetAsync();
            determineSequenceNumber();
        }

        /// <summary>
        /// Calculates the sequence number of the current measurement.
        /// </summary>
        /// <remarks>Assumes that the measurements for the current job are already loaded!</remarks>
        private void determineSequenceNumber() {
            Debug.Assert(Job.Measurements.IsLoaded);

            var orderedMeasurements = Job.Measurements.OrderByDescending(m => m.CreatedAt);
            var currentNumber = orderedMeasurements.IndexOf(m => m.Id == Measurement.Id);

            SequenceNumber = Job.Measurements.Count() - currentNumber;
        }

        public List<ISample> CollectAllSamples() {
            var samples = new List<ISample>();
            foreach (var mappedSample in MappedSamples)
                samples.AddRange(mappedSample.Value.Samples);

            return samples;
        }

        #region Adding/removing a sample
        /// <summary>
        /// Add multiple samples calling AddSample on each of the samples in the list.
        /// </summary>
        public void AddSamples([NotNull] IEnumerable<ISample> samples,
                               [CanBeNull] ColorMatcher.MultiMatchCallback<IJobStripPatch> multiMatchCallback) {
            if (samples == null) throw new ArgumentNullException(nameof(samples));

            foreach (var sample in samples)
                AddSample(sample, multiMatchCallback);
        }

        /// <summary>
        /// Add a single sample to an appropriate job strip patch.
        /// If the sample was not tagged to belong to this patch (e.g. if it was a newly created instance
        /// or if the Tag property does not match any present job strip patches), it will have its Tag
        /// property updated.
        /// </summary>
        /// <param name="sample"></param>
        /// <param name="multiMatchCallback"></param>
        public bool AddSample([NotNull] ISample sample,
                              [CanBeNull] ColorMatcher.MultiMatchCallback<IJobStripPatch> multiMatchCallback) {
            if (sample == null) throw new ArgumentNullException(nameof(sample));

            bool isOK;
            if (sample.NewInstance || sample.Tag <= 0 || !MappedSamples.ContainsKey(sample.Tag)) {
                isOK = addNewSample(sample, multiMatchCallback);
            }
            else {
                addExistingSample(sample);
                isOK = true;
            }
            notifyVariantDataChanged();
            return isOK;
        }

        private bool addNewSample(ISample sample, ColorMatcher.MultiMatchCallback<IJobStripPatch> multiMatchCallback) {
            var closestJsp = ColorMatcher.MatchSample(sample, JobStripPatches, ToleranceSet, multiMatchCallback);
            if (closestJsp != null)
                addNewSample(closestJsp, sample);

            return closestJsp != null;
        }

        private void addNewSample(IJobStripPatch jsp, ISample sample) {
            sample.Tag = jsp.Id;
            MappedSamples[jsp.Id].AddSample(sample);
        }

        private void addExistingSample(ISample sample) {
            MappedSamples[sample.Tag].AddSample(sample);
        }

        public void ClearSamples() {
            foreach (var mappedSample in MappedSamples)
                mappedSample.Value.ClearSamples();

            notifyVariantDataChanged();
        }

        public void ClearSample(ISample sample) {
            foreach (var mappedSample in MappedSamples)
                mappedSample.Value.ClearSample(sample);

            notifyVariantDataChanged();
        }
        #endregion

        #region Paperwhite helpers
        public async Task SavePaperwhiteAsync([NotNull] ISample newSample) {
            if (newSample == null) throw new ArgumentNullException(nameof(newSample));

            await newSample.SaveAsync();
            Job.PaperWhiteSample.Item = newSample;
            await Job.SaveAsync();
            PaperwhiteSample = newSample;
            updatePaperwhiteSamples();
        }

        private void updatePaperwhiteSamples() {
            foreach (var jobStripPatchMapping in AllMappings)
                jobStripPatchMapping.PaperwhiteSample = PaperwhiteSample;
        }
        #endregion

        private void notifyVariantDataChanged() {
            OnPropertyChanged("AllCompareResults");
        }

        public Task PreloadSequenceDataAsync() {
            return Job.Measurements.GetAsync();
        }
    }
}