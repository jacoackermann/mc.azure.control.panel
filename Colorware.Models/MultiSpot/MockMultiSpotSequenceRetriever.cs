﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Color;
using Colorware.Core.Data.Models;
using Colorware.Core.Enums;
using Colorware.Core.Functional.Result;
using Colorware.Models.ServiceModels;

namespace Colorware.Models.MultiSpot {
    public static class MockMultiSpotSequenceRetriever {
        private static MultiSpotSequence globalMock;

        public static MultiSpotSequence GlobalMock {
            get {
                if (globalMock == null)
                    globalMock = LoadMockDataAsync().Result;

                return globalMock;
            }
        }

        public static async Task<MultiSpotSequence> LoadMockDataAsync() {
            var measurement = createMockMeasurement();
            var job = CreateMockJob(measurement);
            var jobStripPatches = createMockJobStripPatches();
            var toleranceSet = createMockToleranceSet();
            var client = Client.CreateMockClient();
            toleranceSet.DeltaEMethod = DeltaEMethod.Cie2000;
            var mc = MeasurementConditionsConfig.CreateDefault("- MOCK CONDITION -");
            var result =
                await MultiSpotSequence.CreateAsync(job, measurement, jobStripPatches, toleranceSet, mc, null, client)
                                       .ConfigureAwait(false);
            result.Do(sequence => sequence.AddSamples(createMockSamples(jobStripPatches), null));
            return result.OrThrow();
        }

        private static IEnumerable<ISample> createMockSamples(List<IJobStripPatch> patches) {
            var result = new List<ISample>();
            var rand = new Random();
            foreach (var patch in patches) {
                for (int i = 0; i < 4; i++) {
                    var lab = new Lab(randomize(rand, patch.L), randomize(rand, patch.a), randomize(rand, patch.b),
                                      new SpectralMeasurementConditions(Illuminant.D50, Observer.TwoDeg,
                                                                        MCondition.M0));
                    var cmyk = new CMYK(randomize(rand, 0.4), 0, 0, 0);
                    var sample = new Sample(
                        new Spectrum("0.1,0.1,0.2,0.5,0.8,0.7,0.71,0.73,0.5,0.4,0.3,0.2,0.1,0.1", 300, 700, ","),
                        lab, cmyk);
                    result.Add(sample);
                }
            }

            return result;
        }

        private static double randomize(Random r, double d) {
            return d + r.NextDouble() * 8.0 - 4.0;
        }

        public static Job CreateMockJob(IMeasurement measurement) {
            var job = Job.CreateMockJob();
            job.Reference.MockItem = Reference.CreateMockReference();
            job.ClientReference.MockItem = Reference.CreateMockReference();
            var measurements = new[] {
                Measurement.CreateMockMeasurement(),
                measurement,
                Measurement.CreateMockMeasurement(),
            };
            job.Measurements.MockItems = new List<IMeasurement>(measurements);
            return job;
        }

        private static Measurement createMockMeasurement() {
            return Measurement.CreateMockMeasurement();
        }

        private static List<IJobStripPatch> createMockJobStripPatches() {
            var result = new List<IJobStripPatch>();
            result.Add(new JobStripPatch {
                           Id = 1,
                           Name = "Patch 1",
                           L = 70,
                           a = 50,
                           b = 50,
                           DefaultDensity = 0.4,
                           PatchType = PatchTypes.Solid
                       });
            result.Add(new JobStripPatch {
                           Id = 2,
                           Name = "Patch 2",
                           L = 30,
                           a = 20,
                           b = 10,
                           DefaultDensity = 0.4,
                           PatchType = PatchTypes.Solid
                       });
            result.Add(new JobStripPatch {
                           Id = 3,
                           Name = "Patch 3",
                           L = 40,
                           a = -40,
                           b = -30,
                           DefaultDensity = 0.4,
                           PatchType = PatchTypes.Dotgain
                       });
            result.Add(new JobStripPatch {
                           Id = 4,
                           Name = "Patch 4",
                           L = 80,
                           a = 30,
                           b = 80,
                           DefaultDensity = 0.4,
                           PatchType = PatchTypes.Solid
                       });
            result.Add(new JobStripPatch {
                           Id = 5,
                           Name = "Patch 5",
                           L = 30,
                           a = 60,
                           b = -40,
                           DefaultDensity = 0.4,
                           PatchType = PatchTypes.Other
                       });
            result.Add(new JobStripPatch {
                           Id = 6,
                           Name = "Patch 6",
                           L = 40,
                           a = 5,
                           b = 4,
                           DefaultDensity = 0.4,
                           PatchType = PatchTypes.Balance
                       });
            return result;
        }

        private static ToleranceSet createMockToleranceSet() {
            return ToleranceSet.CreateMockToleranceSet();
        }
    }
}