using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Castle.Core.Logging;

using Colorware.Core.Data;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.PantoneLIVE;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;
using Colorware.Core.ErrorReporting;
using Colorware.Core.Functional.Result;
using Colorware.Core.PantoneLive;
using Colorware.Core.StatusReporting;
using Colorware.Models.ServiceModels;

using JetBrains.Annotations;

namespace Colorware.Models.MultiSpot {

    public class MultiSpotSequenceRetriever {
        private readonly IGlobalStatusReporter globalStatusReporter;
        private readonly IErrorReporter errorReporter;
        private readonly IPantoneLiveRightsChecker pantoneLiveRightsChecker;
        private readonly ILogger log;

        public MultiSpotSequenceRetriever([NotNull] IGlobalStatusReporter globalStatusReporter,
                                          [NotNull] IErrorReporter errorReporter,
                                          [NotNull] IPantoneLiveRightsChecker pantoneLiveRightsChecker,
                                          [NotNull] ILogger logger) {
            if (globalStatusReporter == null) throw new ArgumentNullException(nameof(globalStatusReporter));
            if (errorReporter == null) throw new ArgumentNullException(nameof(errorReporter));
            if (pantoneLiveRightsChecker == null) throw new ArgumentNullException(nameof(pantoneLiveRightsChecker));
            if (logger == null) throw new ArgumentNullException(nameof(logger));

            this.globalStatusReporter = globalStatusReporter;
            this.errorReporter = errorReporter;
            this.pantoneLiveRightsChecker = pantoneLiveRightsChecker;
            log = logger;
        }

        [ItemNotNull]
        public async Task<Result<MultiSpotSequence>> LoadFromMeasurementAsync([NotNull] IMeasurement m) {
            if (m == null) throw new ArgumentNullException(nameof(m));

            // Find job.
            var job = await m.Job.GetAsync();

            if (job == null)
                return
                    Result.Fail<MultiSpotSequence>(
                        $"Tried to load a MultiSpotSequence for measurement with id '{m.Id}', but the associated job with id '{m.JobId}' could not be loaded!");

            // Find job strip patches.
            var jspQuery = new Query().Eq("job_strip_id", job.JobStripId);
            var jobStripPatchesTask = BaseModel.FindAllAsync<IJobStripPatch>(jspQuery);

            // Find measurement condition
            var mcTask = findMeasurementConditionAsync(job);

            // Find tolerance set
            var toleranceTask = job.ToleranceSet.GetAsync();

            // Find samples.
            var sampleQuery = new Query().Eq("measurement_id", m.Id);
            var samplesTask = BaseModel.FindAllAsync<ISample>(sampleQuery);

            // Find paperwhite sample.
            var pwTask = job.PaperWhiteSample.GetAsync();

            // Find client.
            var clientTask = job.Client.GetAsync();

            // PantoneLIVE unauthorized patches filter
            var busyReport = globalStatusReporter.AddBusyReport(new BusyReport("Checking PantoneLIVE access rights..."));

            var filteredJobStripPatches = new List<IJobStripPatch>();
            Exception errorException = null;
            foreach (var jsp in await jobStripPatchesTask) {
                try {
                    if (!jsp.IsPantoneLivePatch ||
                        await
                        pantoneLiveRightsChecker.MayAccessDependentStandard(
                            new PantoneLiveStandardId(jsp.PantoneLivePaletteGuid,
                                                      jsp.PantoneLiveDependentStandardGuid)))
                        filteredJobStripPatches.Add(jsp);
                }
                catch (Exception e) {
                    errorException = e;
                }
            }

            globalStatusReporter.RemoveBusyReport(busyReport);

            if (errorException != null)
                errorReporter.ReportError(
                    "An error occurred while checking PantoneLIVE access rights!\n\nColors that could not be checked will be hidden in the results.",
                    "Rights checking failed", errorException);

            if (filteredJobStripPatches.Count < jobStripPatchesTask.Result.Count)
                errorReporter.ReportError(@"This job contains PantoneLIVE colors you are not authorized to use. These colors will be hidden and measurements will not be possible.

Please contact your PantoneLIVE representative if you think this is incorrect.",
                                          "PantoneLIVE error");

            var tolerance = await toleranceTask;
            if (tolerance == null) {
                return
                    Result.Fail<MultiSpotSequence>(
                        $"Could not load the tolerance set associated with the job, it may have been deleted.");
            }
            var client = await clientTask;
            if (client == null) {
                return Result.Fail<MultiSpotSequence>(
                    $"Could not load the client associated with the job, it may have been deleted.");
            }
            var mc = await mcTask;
            if (mc == null) {
                return Result.Fail<MultiSpotSequence>(
                    $"Could not load the measurement condition associated with the job, it may have been deleted.");
            }
            
            // For now (version 16.1 and 16.2) we will check if the job strip patch is linked to a reference patch
            // of a type 'spot color'. If that is the case then the used tolerance group will always be set to
            // 'Spotcolor' overriding the existing value for the tolerance group in the job strip patch.
            // This is done, because spot color type job strip patches should actually be set to tolerance group 'Spotcolor'.
            // In version 16.3 there will be performed a database migration to correct this in the existing job strip patches,
            // making this check obsolete.
            await checkAndModifyForSpotcolors(filteredJobStripPatches);
            
            var result = await
                             MultiSpotSequence.CreateAsync(job, m, filteredJobStripPatches, tolerance,
                                                           mc,
                                                           await pwTask, client);
            await result.Do(async sequence => sequence.AddSamples(await samplesTask, null));

            return result;
        }

        private async Task checkAndModifyForSpotcolors(List<IJobStripPatch> filteredJobStripPatches) {
            foreach (var jsp in filteredJobStripPatches) {
                await jsp.ReferencePatch.GetAsync();
                if (jsp.ReferencePatch.Item != null && jsp.ReferencePatch.Item.IsSpot) {
                    jsp.ToleranceGroup = ToleranceGroups.Spotcolor;
                }
            }
        }

        private async Task<IMeasurementConditionsConfig> findMeasurementConditionAsync(IJob job) {
            var measurementCondition = await job.MeasurementCondition.GetAsync();

            if (measurementCondition == null) {
                log.Warn("No measurement condition loaded for job: " + job.Name);
                return MeasurementConditionsConfig.CreateDefault("- DEFAULT -");
            }

            return measurementCondition;
        }
    }
}