using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Config;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.ColorSequence;
using Colorware.Core.Data.Models.ColorStripMapper;
using Colorware.Core.Extensions;
using Colorware.Models.ColorSequence;

namespace Colorware.Models.ColorStripMapper {
    public class ColorStripSequenceConfig : IColorStripSequenceConfig {
        private readonly long colorStripId;
        private const String BaseConfigKey = "Defaults.Job.ColorSequence.";


        public ColorStripSequenceConfig(IColorStrip colorStrip)
            : this(colorStrip.Id) {
        }

        public ColorStripSequenceConfig(long colorStripId) {
            this.colorStripId = colorStripId;
        }

        public void Save(IPressColorSequence sequence) {
            var serialized = sequence.SerializeToString();
            Config.DefaultConfig.Set(BaseConfigKey + colorStripId, serialized);
        }

        public void Load(IEnumerable<IColorStripMapping> mappings, IPressColorSequence colorSequence) {
            var rawSequencesStr = Config.DefaultConfig.Get(BaseConfigKey + colorStripId, null);
            Load(mappings, colorSequence, rawSequencesStr);
        }

        public void Load(IEnumerable<IColorStripMapping> mappings, IPressColorSequence colorSequence,
            String rawSequencesStr) {
            // Reset all colors to start with.
            for (var i = colorSequence.NumberOfColors - 1; i >= 0; i--) {
                colorSequence.Set(i);
            }
            var unassignedMappings = new List<IColorStripMapping>();
            int[] rawSequences = null;
            if (rawSequencesStr != null)
                rawSequences = PressColorSequence.DeserializeFromStringFlat(rawSequencesStr);

            for (var i = 0; i < mappings.Count(); i++) {
                var index = colorSequence.NumberOfColors - i - 1;
                var m = mappings.ElementAt(i);
                if (rawSequences != null) {
                    index = getSavedIndex(m.Slot, rawSequences, index);
                }
                // Check if the index will actually fit.
                if (index < 0 || index >= colorSequence.NumberOfColors || colorSequence.IsDefined(index)) {
                    unassignedMappings.Add(m);
                    continue;
                }

                if (m.IsUndefined)
                    colorSequence.Set(index);
                else
                    colorSequence.Set(index, m.Slot, m.ReferencePatch.ReferencePatch.AsColor,
                        m.ReferencePatch.ReferencePatch.Name);
            }

            foreach (var unassignedMapping in unassignedMappings) {
                if (unassignedMapping.IsUndefined)
                    continue;
                for (var i = colorSequence.NumberOfColors - 1; i >= 0; i--) {
                    if (!colorSequence.IsDefined(i)) {
                        colorSequence.Set(i, unassignedMapping.Slot,
                            unassignedMapping.ReferencePatch.ReferencePatch.AsColor,
                            unassignedMapping.ReferencePatch.ReferencePatch.Name);
                        break;
                    }
                }
            }
        }

        // For Image
        public void LoadForImageModule(IEnumerable<IJobStripPatch> patches, IPressColorSequence colorSequence,
            String rawSequencesStr) {
            // TODO: if there is no corresponding solid for e.g. a dotgain in the select patches list there will be
            // no ink on the press for that color: maybe warn user or find the solid
            var solidPatches =
                patches.Where(p => p.IsSolid && p.InkZone == 0 && !p.PantoneLiveObjectGuid.HasValue)
                       .DistinctBy(p => p.ReferencePatchId);

            // Add PantoneLIVE patches
            solidPatches =
                solidPatches.Concat(
                    patches.Where(p => p.IsSolid && p.InkZone == 0 && p.PantoneLiveObjectGuid.HasValue)
                           .DistinctBy(p => p.PantoneLiveObjectGuid)).ToList();

            // Reset all colors to start with.
            for (var i = colorSequence.NumberOfColors - 1; i >= 0; i--)
                colorSequence.Set(i);

            var unassignedPatches = new List<IJobStripPatch>();
            int[] rawSequences = null;
            if (rawSequencesStr != null)
                rawSequences = PressColorSequence.DeserializeFromStringFlat(rawSequencesStr);

            for (var i = 0; i < solidPatches.Count(); i++) {
                var index = colorSequence.NumberOfColors - i - 1;
                var p = solidPatches.ElementAt(i);
                if (rawSequences != null)
                    // TODO-image: this might be unneccessary, it can result in sequences with empty ink towers
                    // in the middle
                    index = getSavedIndex(p.Slot1, rawSequences, index);

                // Check if the index will actually fit.
                if (index < 0 || index >= colorSequence.NumberOfColors || colorSequence.IsDefined(index)) {
                    unassignedPatches.Add(p);
                    continue;
                }

                if (p.IsUndefined)
                    colorSequence.Set(index);
                else
                    colorSequence.Set(index, p.Slot1, p.AsColor, p.Name);
            }

            foreach (var unassignedPatch in unassignedPatches) {
                if (unassignedPatch.IsUndefined)
                    continue;

                for (var i = colorSequence.NumberOfColors - 1; i >= 0; i--) {
                    if (!colorSequence.IsDefined(i)) {
                        colorSequence.Set(i, unassignedPatch.Slot1, unassignedPatch.AsColor, unassignedPatch.Name);
                        break;
                    }
                }
            }
        }

        private static int getSavedIndex(int slot, int[] rawSequences, int originalIndex) {
            for (var sequenceIndex = 0; sequenceIndex < rawSequences.Length; sequenceIndex++) {
                var slotNumber = rawSequences[sequenceIndex];
                if (slot == slotNumber) {
                    return sequenceIndex;
                }
            }
            return originalIndex;
        }
    }
}