﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;

using Colorware.Core.Color;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.ColorStripMapper;
using Colorware.Core.Mvvm;
using Colorware.Models.ServiceModels;

using JetBrains.Annotations;

namespace Colorware.Models.ColorStripMapper {
    [Obsolete("Use ColorStripMapper in Colorware.Process for new stuff")]
    public class ColorStripMapperOld : DefaultNotifyPropertyChanged, IColorStripMapperOld {
        private SpectralMeasurementConditions measurementConditions;

        public SpectralMeasurementConditions MeasurementConditions {
            get => measurementConditions;
            set {
                measurementConditions = value;
                UpdateMapping();
                OnPropertyChanged(nameof(MeasurementConditions));
            }
        }

        private IEnumerable<IReferencePatch> referencePatches;

        public IEnumerable<IReferencePatch> ReferencePatches {
            get => referencePatches;
            set {
                referencePatches = value;
                UpdateMapping();
                OnPropertyChanged();
            }
        }

        private IReadOnlyList<IColorStripPatch> colorStripPatches;

        public IReadOnlyList<IColorStripPatch> ColorStripPatches {
            get => colorStripPatches;
            set {
                colorStripPatches = value;
                UpdateMapping();
                if (colorStripPatches != null) {
                    StartPatchIndex = 0;
                    EndPatchIndex = colorStripPatches.Count - 1;
                }

                OnPropertyChanged();
            }
        }

        private int startPatchIndex;

        public int StartPatchIndex {
            get => startPatchIndex;
            set {
                if (startPatchIndex == value)
                    return;

                startPatchIndex = value;
                OnPropertyChanged();
                updateSlicedPatches(ResultingPatches);
            }
        }

        private int endPatchIndex;

        public int EndPatchIndex {
            get => endPatchIndex;
            set {
                if (endPatchIndex == value)
                    return;

                if (value > 1) {
                    endPatchIndex = value;
                    OnPropertyChanged();
                    updateSlicedPatches(ResultingPatches);
                }
            }
        }

        private IEnumerable<IColorStripMapping> mappings;

        public IEnumerable<IColorStripMapping> Mappings {
            get => mappings ?? new List<ColorStripMapping>();
            set {
                mappings = value;
                UpdateMapping();
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Resulting patches of full strip (ignoring start and end inkzone.
        /// </summary>
        public List<IJobStripPatch> ResultingPatches {
            get => resultingPatches;
            set {
                resultingPatches = value;
                OnPropertyChanged();
            }
        }

        private List<IJobStripPatch> resultingPatches;

        /// <remarks>Make sure every list in this dictionary is actually loaded with GetAsync()!</remarks>
        [NotNull]
        public Dictionary<int, IDefaultDotgainList> DotgainCurvesPerSlot { get; set; }

        public ColorStripMapperOld() {
            DotgainCurvesPerSlot = new Dictionary<int, IDefaultDotgainList>();

            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
                initMockData();
        }

        public void UpdateMapping() {
            if (ColorStripPatches == null)
                return;
            //Debug.WriteLine("UpdateMapping()");
            if (referencePatches == null) {
                ResultingPatches = ColorStripPatches.Select(
                    csPatch =>
                    JobStripPatch.CreateFromReferencePatch(
                        null, csPatch, MeasurementConditions, false))
                                                    .ToList();
            }
            else {
                var result = new List<IJobStripPatch>();
                result.AddRange(ColorStripPatches.Select(mapColorStripPatch));

                updateSlicedPatches(result);

                ResultingPatches = result;
            }
        }

        [NotNull]
        private IJobStripPatch mapColorStripPatch([NotNull] IColorStripPatch csPatch) {
            if (csPatch.IsUndefined)
                return JobStripPatch.CreateUndefined(csPatch);

            // Try to find reference patch to create JSP from
            var refPatch = findReferenceFor(csPatch);

            IJobStripPatch jsp;
            if (refPatch == null && canBeMappedToDotgainOnlyPatch(csPatch)) {
                var baseReference = findReferencePatchForDotgainMapping(csPatch);
                if (baseReference != null) {
                    var dotgainCurve = DotgainCurvesPerSlot.ContainsKey(csPatch.Slot1)
                                           ? DotgainCurvesPerSlot[csPatch.Slot1]
                                           : null;

                    var dotgainEntries = dotgainCurve?.DefaultDotgainEntries;

                    // Build `fake' patch.
                    jsp =
                        JobStripPatch.CreateDotgainOnlyPatch(dotgainEntries, baseReference, csPatch,
                                                             MeasurementConditions);
                }
                else {
                    // Create undefined, but with color patch data.
                    jsp = JobStripPatch.CreateFromReferencePatch(null, csPatch, MeasurementConditions, false);
                }
            }
            else {
                var checkMeasurementConditions = refPatch != null && !refPatch.IsSpot;
                // Create patch from actual found reference (this is the best case)
                jsp = JobStripPatch.CreateFromReferencePatch(refPatch, csPatch, MeasurementConditions, false,
                                                             checkMeasurementConditions);
            }

            return jsp;
        }

        [CanBeNull]
        private IReferencePatch findReferencePatchForDotgainMapping([NotNull] IColorStripPatch csPatch) {
            return
                Mappings.Where(m => m.Slot == csPatch.Slot1 && m.IsClientColor && m.ReferencePatch != null)
                        .Select(m => m.ReferencePatch.ReferencePatch)
                        .FirstOrDefault();
        }

        private bool canBeMappedToDotgainOnlyPatch([NotNull] IColorStripPatch patch) {
            // Must have one patch.
            if (!patch.HasSlot1 || patch.HasSlot2)
                return false;

            // Must at least have a mapping.
            var mapping = findMapping(patch.Slot1);
            if (mapping == null)
                return false;

            // Can only map client colors in this way.
            if (!mapping.IsClientColor)
                return false;

            // Can only map <100% patches.
            if (patch.Percentage >= 100)
                return false;

            // Must have dotgain curve to use
            if (!DotgainCurvesPerSlot.ContainsKey(patch.Slot1))
                return false;

            return true;
        }

        private void updateSlicedPatches([CanBeNull] IReadOnlyList<IJobStripPatch> patches) {
            if (patches == null || !patches.Any())
                return;

            var startIdx = StartPatchIndex;
            var endIdx = EndPatchIndex;
            if (startIdx < 0)
                startIdx = 0;

            if (endIdx > patches.Count - 1)
                endIdx = patches.Count - 1;

            if (startIdx > endIdx) {
                var tmp = startIdx;
                startIdx = endIdx;
                endIdx = tmp;
            }

            startPatchIndex = startIdx;
            OnPropertyChanged("StartPatchIndex");
            endPatchIndex = endIdx;
            OnPropertyChanged("EndPatchIndex");
        }

        [CanBeNull]
        private IReferencePatch findReferenceFor([NotNull] IColorStripPatch csPatch) {
            var mapping1 = findMapping(csPatch.Slot1);
            var mapping2 = findMapping(csPatch.Slot2);
            var mapping3 = findMapping(csPatch.Slot3);
            var numDefined = 0;
            numDefined += mapping1 == null || mapping1.IsUndefined ? 0 : 1;
            numDefined += mapping2 == null || mapping2.IsUndefined ? 0 : 1;
            numDefined += mapping3 == null || mapping3.IsUndefined ? 0 : 1;

            // Overprint 2 slots
            if (numDefined == 2) {
                var first = mapping1;
                var second = mapping2;

                if (first == null || first.IsUndefined)
                    first = mapping3;
                if (second == null || second.IsUndefined)
                    second = mapping3;
                if (first == null || second == null || first.IsUndefined || second.IsUndefined)
                    return null;

                return findReferenceWithOverprintMapping(first, second);
            }

            // Overprint 3 slots
            else if (numDefined == 3) {
                if (mapping1 == null || mapping2 == null || mapping3 == null)
                    return null;
                if (mapping1.IsUndefined || mapping2.IsUndefined || mapping3.IsUndefined)
                    return null;

                return findReferenceWithOverprintMapping(mapping1, mapping2, mapping3);
            }
            // 0 or 1 defined patches
            else {
                if (mapping1 != null) {
                    if (mapping1.IsUndefined)
                        return null;

                    // Overprint can't be mapped to a single color if some mappings are undefined.
                    if (mapping2 != null || mapping3 != null)
                        return null;

                    // Solid or dotgain
                    return findReferenceWithMapping(mapping1, csPatch);
                }

                // Other or custom type.
                return findReferenceWithoutMapping(csPatch);
            }
        }

        [CanBeNull]
        private IColorStripMapping findMapping(int slotId) {
            if (slotId == ColorStripPatch.NO_SLOT_VALUE)
                return null;

            return Mappings.FirstOrDefault(m => m.Slot == slotId);
        }

        /// <summary>
        /// Find a reference for a patch with one or more slot entries.
        /// </summary>
        /// <param name="mapping">Mapping to use.</param>
        /// <param name="csPatch">Related color strip patch.</param>
        /// <returns>The found reference patch, or null if none could be found.</returns>
        [CanBeNull]
        private IReferencePatch findReferenceWithMapping([NotNull] IColorStripMapping mapping,
                                                         [NotNull] IColorStripPatch csPatch) {
            if (csPatch.Percentage == 100)
                return mapping.ReferencePatch.ReferencePatch;

            if (mapping.IsUndefined)
                return null;

            return
                ReferencePatches
                    .FirstOrDefault(
                        refPatch =>
                        refPatch.Parent1Id == mapping.ReferencePatch.ReferencePatch.Id &&
                        refPatch.Percentage == csPatch.Percentage);
        }

        /// <summary>
        /// Find reference patch for overprint mapping (2 slots defined).
        /// </summary>
        /// <param name="mapping1">The first mapping.</param>
        /// <param name="mapping2">The second mapping.</param>
        /// <returns>The found reference patch or null.</returns>
        [CanBeNull]
        private IReferencePatch findReferenceWithOverprintMapping([NotNull] IColorStripMapping mapping1,
                                                                  [NotNull] IColorStripMapping mapping2) {
            foreach (var refPatch in ReferencePatches) {
                if (refPatch.Parent1Id <= 0 || refPatch.Parent2Id <= 0)
                    continue; // Either an invalid patch, or a Pantone Live patch which can't be mapped right now.
                if ((refPatch.Parent1Id == mapping1.ReferencePatch.ReferencePatch.Id &&
                     refPatch.Parent2Id == mapping2.ReferencePatch.ReferencePatch.Id)
                    ||
                    (refPatch.Parent2Id == mapping1.ReferencePatch.ReferencePatch.Id &&
                     refPatch.Parent1Id == mapping2.ReferencePatch.ReferencePatch.Id))
                    return refPatch;
            }
            return null;
        }

        [CanBeNull]
        private IReferencePatch findReferenceWithOverprintMapping([NotNull] IColorStripMapping mapping1,
                                                                  [NotNull] IColorStripMapping mapping2,
                                                                  [NotNull] IColorStripMapping mapping3) {
            foreach (var refPatch in ReferencePatches) {
                if (refPatch.Parent1Id <= 0 || refPatch.Parent2Id <= 0 || refPatch.Parent3Id <= 0)
                    continue; // Either an invalid patch, or a Pantone Live patch which can't be mapped right now.
                if ((refPatch.Parent1Id == mapping1.ReferencePatch.ReferencePatch.Id &&
                     refPatch.Parent2Id == mapping2.ReferencePatch.ReferencePatch.Id &&
                     refPatch.Parent3Id == mapping3.ReferencePatch.ReferencePatch.Id)
                    ||
                    (refPatch.Parent1Id == mapping3.ReferencePatch.ReferencePatch.Id &&
                     refPatch.Parent2Id == mapping1.ReferencePatch.ReferencePatch.Id &&
                     refPatch.Parent3Id == mapping2.ReferencePatch.ReferencePatch.Id)
                    ||
                    (refPatch.Parent1Id == mapping2.ReferencePatch.ReferencePatch.Id &&
                     refPatch.Parent2Id == mapping3.ReferencePatch.ReferencePatch.Id &&
                     refPatch.Parent3Id == mapping1.ReferencePatch.ReferencePatch.Id)
                    ||
                    (refPatch.Parent1Id == mapping1.ReferencePatch.ReferencePatch.Id &&
                     refPatch.Parent2Id == mapping3.ReferencePatch.ReferencePatch.Id &&
                     refPatch.Parent3Id == mapping2.ReferencePatch.ReferencePatch.Id)
                    ||
                    (refPatch.Parent1Id == mapping2.ReferencePatch.ReferencePatch.Id &&
                     refPatch.Parent2Id == mapping1.ReferencePatch.ReferencePatch.Id &&
                     refPatch.Parent3Id == mapping3.ReferencePatch.ReferencePatch.Id)
                    ||
                    (refPatch.Parent1Id == mapping3.ReferencePatch.ReferencePatch.Id &&
                     refPatch.Parent2Id == mapping2.ReferencePatch.ReferencePatch.Id &&
                     refPatch.Parent3Id == mapping1.ReferencePatch.ReferencePatch.Id)
                    )
                    return refPatch;
            }
            return null;
        }

        [CanBeNull]
        private IReferencePatch findReferenceWithoutMapping([NotNull] IColorStripPatch csPatch) {
            // Try regular references next if no client patch was found.
            // If patch_type == <any>, the name should also match (e.g. the case in a mediawedge) and anything will do.
            return ReferencePatches.FirstOrDefault(r => r.PatchType == csPatch.PatchType && r.Name == csPatch.Name);
        }

        #region Mock data
        private void initMockData() {
            var res = new List<IJobStripPatch>();
            var r = new Random();

            for (var i = 0; i < 20; i++) {
                var jsp = new JobStripPatch {
                    Name = "JSP " + i,
                    L = r.NextDouble() * 70,
                    a = r.NextDouble() * 120 - 60,
                    b = r.NextDouble() * 120 - 60,
                    InkZone = i / 3
                };
                res.Add(jsp);
            }

            ResultingPatches = res;
        }
        #endregion
    }
}