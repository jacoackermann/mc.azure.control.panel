﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Colorware.Core.Color;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.ColorStripMapper;
using Colorware.Core.Enums;
using Colorware.Core.Mvvm;
using Colorware.Models.ServiceModels;

using JetBrains.Annotations;

namespace Colorware.Models.ColorStripMapper {
    [DebuggerDisplay("ColorStripMapping({" + nameof(Slot) + "})")]
    public class ColorStripMapping : DefaultNotifyPropertyChanged, IColorStripMapping {
        public ColorStripMapping([NotNull] SpectralMeasurementConditions measurementConditions) {
            MeasurementConditions =
                measurementConditions ?? throw new ArgumentNullException(nameof(measurementConditions));
        }

        public static readonly MappableReferencePatch NullPatch =
            new MappableReferencePatch(
                new ReferencePatch(
                    new SpectralMeasurementConditions(
                        // Arbitrary
                        Illuminant.D50,
                        Observer.TwoDeg,
                        MCondition.M0)) {
                            Name = "-- Undefined --",
                            PatchType = PatchTypes.Undefined,
                        }, false);


        private SpectralMeasurementConditions measurementConditions;

        public SpectralMeasurementConditions MeasurementConditions {
            get => measurementConditions;
            set {
                measurementConditions = value;
                OnPropertyChanged(nameof(MeasurementConditions));
            }
        }

        private int slot;

        /// <summary>
        /// Slot id for this mapping.
        /// </summary>
        public int Slot {
            get => slot;
            set {
                slot = value;
                OnPropertyChanged("ReferencePatch");
            }
        }

        public bool IsUndefined => ReferencePatch == null || ReferencePatch.ReferencePatch == null;

        public bool IsReference => ReferencePatch != null && !ReferencePatch.IsClientPatch;

        public bool IsClientColor => ReferencePatch != null && ReferencePatch.IsClientPatch;

        /// <summary>
        /// Refers to 100% patch of chosen reference.
        /// </summary>
        public MappableReferencePatch ReferencePatch {
            get => referencePatch;
            set {
                if (value == null)
                    return;

                referencePatch = value == NullPatch ? null : value;

                OnPropertyChanged();
                OnPropertyChanged("IsUndefined");
                OnPropertyChanged("IsReference");
                OnPropertyChanged("IsClientColor");
            }
        }

        private MappableReferencePatch referencePatch;

        private IReadOnlyList<IReferencePatch> selectableReferencePatches;

        /// <summary>
        /// Only used in views to make editing easier.
        /// </summary>
        public IReadOnlyList<IReferencePatch> SelectableReferencePatches {
            get => selectableReferencePatches;
            set {
                selectableReferencePatches = value;
                OnPropertyChanged();
                updateSelectablePatches();
            }
        }

        private IReadOnlyList<IReferencePatch> selectableClientPatches;

        /// <summary>
        /// Only used in views to make editing easier.
        /// </summary>
        public IReadOnlyList<IReferencePatch> SelectableClientPatches {
            get => selectableClientPatches;
            set {
                selectableClientPatches = value;
                OnPropertyChanged();
                updateSelectablePatches();
            }
        }

        private IReadOnlyList<MappableReferencePatch> selectablePatches;

        public IReadOnlyList<MappableReferencePatch> SelectablePatches {
            get => selectablePatches;
            set {
                selectablePatches = value;
                OnPropertyChanged();
            }
        }


        public void SetSelectableReferencePatches(IEnumerable<IReferencePatch> patches, bool isImageMode = false) {
            if (isImageMode)
                patches = patches.Where(p => p.MeasurementConditions.SpectrallyCompatibleWith(MeasurementConditions));
            else
                patches =
                    patches.Where(
                        p =>
                        p.IsSolid &&
                        (p.IsSpot || p.MeasurementConditions.SpectrallyCompatibleWith(MeasurementConditions)));

            SelectableReferencePatches = patches.ToList();
        }

        public void SetSelectableClientPatches(IEnumerable<IReferencePatch> patches) {
            if (patches == null) {
                SelectableClientPatches = null;
                return;
            }

            var filteredPatches =
                patches.Where(p => p.MeasurementConditions.SpectrallyCompatibleWith(MeasurementConditions));

            SelectableClientPatches = filteredPatches.ToList();
        }

        /// <summary>
        /// Merge client and reference patches.
        /// </summary>
        private void updateSelectablePatches() {
            var patches = new List<MappableReferencePatch> {NullPatch};

            if (SelectableReferencePatches != null)
                patches.AddRange(SelectableReferencePatches.Select(p => new MappableReferencePatch(p, false)));

            if (SelectableClientPatches != null)
                patches.AddRange(SelectableClientPatches.Select(p => new MappableReferencePatch(p, true)));

            SelectablePatches = patches;
        }
    }
}