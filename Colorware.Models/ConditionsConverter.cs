﻿using System;

using Colorware.Core.Color;
using Colorware.Core.Data.Models;
using Colorware.Core.Enums;
using Colorware.Core.Functional.Option;
using Colorware.Core.Functional.Result;
using Colorware.Models.ServiceModels;

using JetBrains.Annotations;

namespace Colorware.Models {
    public class ConditionsConverter {
        /// <summary>
        /// Returns Nothing if no conversion was needed
        /// </summary>
        public static Option<Result<ISample>> TryConvertSampleIfNeeded(
            [NotNull] IMeasurementConditionsConfig sampleMcc,
            [NotNull] IMeasurementConditionsConfig toMcc,
            [NotNull] ISample sample,
            Option<Spectrum> paperwhiteSpectrum) {
            if (sample == null) throw new ArgumentNullException(nameof(sample));

            return tryConvertIfNeeded(sampleMcc,
                                      toMcc,
                                      sample.HasSpectrum ? sample.AsSpectrum.ToOption() : Option<Spectrum>.Nothing,
                                      sample.Densities.ToOption(),
                                      paperwhiteSpectrum);
        }

        /// <summary>
        /// Returns Nothing if no conversion was needed
        /// </summary>
        public static Option<Result<IReferencePatch>> TryConvertReferencePatchIfNeeded(
            [NotNull] IMeasurementConditionsConfig refPatchMcc,
            [NotNull] IMeasurementConditionsConfig toMcc,
            [NotNull] IReferencePatch referencePatch,
            Option<Spectrum> paperwhiteSpectrum) {
            if (referencePatch == null) throw new ArgumentNullException(nameof(referencePatch));

            // Convert
            var converted =
                tryConvertIfNeeded(refPatchMcc,
                                   toMcc,
                                   referencePatch.HasSpectrum
                                       ? referencePatch.AsSpectrum.ToOption()
                                       : Option<Spectrum>.Nothing,
                                   referencePatch.HasSpectrum
                                       // The core conversion function requires the densities as a CMYK tuple, but we
                                       // only have a single double of which we don't know to which channel it belongs.
                                       // This is figured out from the spectrum here.
                                       ? CMYK.BuildFromSingleValueByGuessingComponent(referencePatch.AsSpectrum,
                                                                                      referencePatch.DefaultDensity)
                                             .ToOption()
                                       // If we don't have the spectrum, we just pass a dummy so the converter won't complain in the
                                       // specific case that conversion is needed because densities are missing (with a dummy it will
                                       // just do no conversion). It will still complain if the whitebase or density standard are 
                                       // different and no spectrum is available to calculate the densities from.
                                       : new CMYK(0, 0, 0, 0).ToOption(),
                                   paperwhiteSpectrum);

            // Put results into a clone of the passed in ReferencePatch
            return converted
                .Select(
                    result =>
                        result
                            .Bind(sample => {
                                // Need to clone because we want to keep some fields, e.g. name, patch type, % etc.
                                var newRefPatch = referencePatch.CloneNew<IReferencePatch>();
                                newRefPatch.DefaultDensity = sample.Density;
                                newRefPatch.SetLabAndMeasurementConditions(sample.AsLab);
                                return newRefPatch;
                            })
                );
        }

        /// <summary>
        /// Returns Nothing if no conversion was needed
        /// </summary>
        // TODO: unit tests
        private static Option<Result<ISample>> tryConvertIfNeeded(
            [NotNull] IMeasurementConditionsConfig fromMcc,
            [NotNull] IMeasurementConditionsConfig toMcc,
            Option<Spectrum> spectrum,
            Option<CMYK> densities,
            Option<Spectrum> paperwhiteSpectrum) {
            if (fromMcc == null) throw new ArgumentNullException(nameof(fromMcc));
            if (toMcc == null) throw new ArgumentNullException(nameof(toMcc));

            var fromMc = fromMcc.GetMeasurementConditions();
            var toMc = toMcc.GetMeasurementConditions();

            // Do we need to convert?
            var illObsMatch = fromMc.IllObsMatch(toMc);
            var mCondsCompatible = fromMc.SpectralMCondition == toMc.SpectralMCondition;
            var densStdsMatch = fromMcc.DensityStandard == toMcc.DensityStandard;
            var whiteBasesMatch = fromMcc.DensityWhiteBase == toMcc.DensityWhiteBase;

            // NB: we also have to generate densities if the caller didn't supply any
            var needDensConversion = !densStdsMatch || !whiteBasesMatch || !densities.HasValue;

            // Short-circuit when no conversion is needed
            if (illObsMatch && mCondsCompatible && !needDensConversion)
                return Option<Result<ISample>>.Nothing;

            // Can we convert?

            // Do we even have a spectrum?
            if (!spectrum.HasValue) {
                return failConversion(
                    "Color(s) can't be converted to the selected conditions because no spectrum is available.");
            }

            // Can't do anything if the spectral M-condition is different (can never convert a spectrum to another m-condition)
            if (!mCondsCompatible) {
                return failConversion(
                    "Color(s) can't be converted to the selected condition because the spectral M-conditions differ.");
            }

            // Do we have a spectrum in the density M-condition? (only if it matches the spectral M-condition, as we only keep the
            // curve belonging to the spectral M-condition)
            if (!densStdsMatch && toMc.DensityMCondition != toMc.SpectralMCondition) {
                return failConversion(
                    "Color(s) can't be converted to the selected conditions because the density M-condition differs from the spectral M-condition.");
            }

            // We need a PW spectrum if the new whitebase is Relative and densities need to be recalculated
            if (toMcc.DensityWhiteBase == WhiteBase.Substrate &&
                needDensConversion &&
                !paperwhiteSpectrum.HasValue) {
                return failConversion(
                    "Color(s) can't be converted to the selected conditions because the whitebase is relative and no paperwhite spectrum is available.");
            }

            // Convert densities, if necessary
            var newDensities = needDensConversion
                                   ? (toMcc.DensityWhiteBase == WhiteBase.Substrate
                                          ? spectrum.Value.ToDensities(toMcc.DensityStandard, paperwhiteSpectrum.Value)
                                          : spectrum.Value.ToDensities(toMcc.DensityStandard))
                                   : densities.Value;

            return
                OptionConversion.ToOption(
                    Result.Ok(
                        (ISample)new Sample(spectrum.Value, newDensities, toMc)));
        }

        private static Option<Result<ISample>> failConversion(string err) {
            return OptionConversion.ToOption(Result.Fail<ISample>(err));
        }
    }
}