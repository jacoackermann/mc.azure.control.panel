using System;
using System.Collections.Generic;
using System.Diagnostics;

using Colorware.Core.Color;
using Colorware.Core.Config;
using Colorware.Core.Data.Models;
using Colorware.Core.Enums;
using Colorware.Core.Logging;

namespace Colorware.Models {
    public class JobStripSliceDetector {
        private readonly IReadOnlyList<IJobStripPatch> jobStripPatches;

        public ColorbarAlignment AlignmentMode { get; set; }
        public int StartIndex { get; private set; }
        public int EndIndex { get; private set; }

        // PV-2018: The current default behavior is to take undefined patches into account when
        // determining an optimal colorbar match. This can yield wrong results in some situations.
        // When this config key has been set. We skip these patches completely.
        //
        // TODO: Investigate if this should be the default behavior.
        private const string DoNotMatchUndefinedJspKey = "Options.ProcessControl.ColorbarRecognition.SkipUndefined";
        private readonly bool skipUndefinedJsp;

        public JobStripSliceDetector(IReadOnlyList<IJobStripPatch> jobStripPatches)
            : this(jobStripPatches, ColorbarAlignment.Center) {
        }

        public JobStripSliceDetector(IReadOnlyList<IJobStripPatch> jobStripPatches, ColorbarAlignment alignmentMode) {
            this.jobStripPatches = jobStripPatches;
            AlignmentMode = alignmentMode;
            skipUndefinedJsp = Config.DefaultConfig.GetBool(DoNotMatchUndefinedJspKey, false);
        }

        public void Detect(List<ISample> samples) {
            var paperWhite = new Lab(90, 0, 0, SpectralMeasurementConditions.Undefined);
            if (jobStripPatches != null) {
                foreach (var jsp in jobStripPatches) {
                    if (jsp.IsPaperWhite) {
                        paperWhite = jsp.AsLab;
                    }
                }
            }
            switch (AlignmentMode) {
                case ColorbarAlignment.Auto:
                case ColorbarAlignment.Center:
                    detectFromCenter(samples, paperWhite);
                    break;
                case ColorbarAlignment.Left:
                    detectFromLeft(samples, paperWhite);
                    break;
                case ColorbarAlignment.Right:
                    detectFromRight(samples, paperWhite);
                    break;
                case ColorbarAlignment.ForceLeft:
                    detectForceFromLeft(samples);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void detectForceFromLeft(List<ISample> samples) {
            if (jobStripPatches == null)
                return;
            var sampleCount = samples.Count;
            var jobStripPatchCount = jobStripPatches.Count;
            StartIndex = 0;
            EndIndex = Math.Min(sampleCount, jobStripPatchCount) - 1;
        }

        private void detectFromLeft(List<ISample> samples, Lab paperWhiteLab) {
            if (jobStripPatches == null)
                return;
            var stripCount = jobStripPatches.Count;
            var sampleCount = samples.Count;

            if (stripCount <= 0 || sampleCount <= 0)
                return;
            var first = true;
            var bestIndex = 0;
            var bestDeltaE = -10.0;
            for (var i = 0; i < stripCount - sampleCount; i ++) {
                var deltaE = totalDeltaE(samples, i, bestDeltaE, paperWhiteLab);
                if (first || deltaE < bestDeltaE) {
                    first = false;
                    bestDeltaE = deltaE;
                    bestIndex = i;
                }
            }
            StartIndex = bestIndex;
            EndIndex = bestIndex + sampleCount - 1;
            Debug.Assert(StartIndex <= EndIndex);
        }

        private void detectFromRight(List<ISample> samples, Lab paperWhiteLab) {
            if (jobStripPatches == null)
                return;
            var stripCount = jobStripPatches.Count;
            var sampleCount = samples.Count;

            if (stripCount <= 0 || sampleCount <= 0)
                return;
            var first = true;
            var bestIndex = 0;
            var bestDeltaE = -10.0;
            for (var i = stripCount - sampleCount; i >= 0; i--) {
                var deltaE = totalDeltaE(samples, i, bestDeltaE, paperWhiteLab);
                if (first || deltaE < bestDeltaE) {
                    first = false;
                    bestDeltaE = deltaE;
                    bestIndex = i;
                }
            }
            StartIndex = bestIndex;
            EndIndex = bestIndex + sampleCount - 1;
            Debug.Assert(StartIndex <= EndIndex);
        }

        private void detectFromCenter(List<ISample> samples, Lab paperWhiteLab) {
            if (jobStripPatches == null)
                return;
            var stripCount = jobStripPatches.Count;
            var sampleCount = samples.Count;

            if (stripCount <= 0 || sampleCount <= 0)
                return;

            var stripIsOdd = (stripCount & 1) > 0;
            var stripIsEven = !stripIsOdd;
            var samplesIsOdd = (sampleCount & 1) > 0;
            var samplesIsEven = !samplesIsOdd;
            int left1, left2;
            if (stripIsEven && samplesIsEven) {
                // * * * * * * * *
                //   . . . . . .
                left1 = left2 = (stripCount / 2) - (sampleCount / 2);
            }
            else if (stripIsEven && samplesIsOdd) {
                // * * * * * * * *
                //   . . . . .
                //     . . . . .
                left1 = (stripCount / 2) - (sampleCount / 2);
                left2 = left1 + 1;
            }
            else if (stripIsOdd && samplesIsOdd) {
                // * * * * *
                //   . . .
                left1 = left2 = (stripCount / 2) /* + 1 */- (sampleCount / 2);
            }
            else {
                // * * * * *
                //   . .
                //     . .
                left1 = (stripCount / 2) - sampleCount / 2;
                left2 = left1 + 1;
            }

            var done = false;
            var first = true;
            var bestIndex = 0;
            var bestDeltaE = -10.0;
            while (!done) {
                if (left1 >= 0) {
                    var deltaE = totalDeltaE(samples, left1, bestDeltaE, paperWhiteLab);
                    if (first || deltaE < bestDeltaE) {
                        first = false;
                        bestDeltaE = deltaE;
                        bestIndex = left1;
                    }
                }
                if (left2 <= stripCount - sampleCount) {
                    var deltaE = totalDeltaE(samples, left2, bestDeltaE, paperWhiteLab);
                    if (first || deltaE < bestDeltaE) {
                        first = false;
                        bestDeltaE = deltaE;
                        bestIndex = left2;
                    }
                }
                left1--;
                left2++;
                done = left1 < 0 && left2 > stripCount - sampleCount;
            }
            StartIndex = bestIndex;
            EndIndex = bestIndex + sampleCount - 1;
            Debug.Assert(StartIndex <= EndIndex);
        }

        private double totalDeltaE(List<ISample> samples, int startPatchIdx, double bestDeltaESoFar, Lab paperWhiteLab) {
            var totalDeltaE = 0.0;
            var totalChecked = 0;
            var numUndefined = 0;
            for (var i = 0; i < samples.Count; i++) {
                var colorBarIdx = startPatchIdx + i; // Index into the job strip patches.
                if (colorBarIdx < 0 || colorBarIdx >= jobStripPatches.Count) {
                    LogManager.GetLogger(GetType()).Warn("colorBarIdx was outside of job strip patch array");
                    break;
                }
                var jsp = jobStripPatches[colorBarIdx];
                if (jsp.IsDotgainOnly) {
                    continue; // Don't have any data for this, so just continue.
                }
                if (jsp.IsUndefined) {
                    numUndefined++;
                    if (skipUndefinedJsp) {
                        // See PV-2018: If `skipUndefinedJsp` has been set, we do not take the
                        // current patch into account when evaluating the result. This can provide
                        // better matches in some cases
                        //
                        // (TODO: Investigate if this should be the default behavior).
                        continue;
                    }
                }
                totalChecked++;
                var sample = samples[i];
                var sampleLab = sample.AsLab;
                var refLab = jsp.AsLab;
                if (jsp.IsUndefined) {
                    // See PV-2018: This is the part where things go wrong as L, a and b can be 0
                    // here. The result is then compared to paperwhite, yielding a large delta-E
                    // value further down.
                    refLab = new Lab(jsp.L, jsp.a, jsp.b, sample.MeasurementConditions);
                    sampleLab =
                        !refLab.MeasurementConditions.SpectrallyCompatibleWith(paperWhiteLab.MeasurementConditions)
                            ? new Lab(paperWhiteLab.L, paperWhiteLab.a, paperWhiteLab.b, refLab.MeasurementConditions)
                            : paperWhiteLab;
                }
                var deltaE = sampleLab.DeltaE76(refLab);
                totalDeltaE += deltaE;
                if (bestDeltaESoFar >= 0 && bestDeltaESoFar < totalDeltaE)
                    return totalDeltaE;
                // Optimization for early quit. Best must be given greater than 0 and best must be better than current.
            }
            if (totalChecked <= 0 || totalChecked == numUndefined) // Should at least have one valid patch.
                return double.MaxValue;
            return totalDeltaE;
        }
    }
}