using Colorware.Core.Data.Models;

namespace Colorware.Models {
    /// <summary>
    /// Helper class to keep track of errors in a measurement. Can be used
    /// to check if the current measurement has any significant changes in error state compared to
    /// the last measurement.
    /// </summary>
    public interface IMeasurementErrorCache {
        IJob Job { get; set; }
        bool RequiresCheck { get; }
        void Invalidate();
        void StartUpdate(int sampleCount);

        /// <summary>
        /// Marks the patch at the specified index as invalid.
        /// </summary>
        /// <param name="index">The index of the patch to mark.</param>
        void Mark(int index);

        void EndUpdate();
    }
}