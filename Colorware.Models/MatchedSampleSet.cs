using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Colorware.Core.Config;
using Colorware.Core.Data.Models;
using Colorware.Core.Enums;
using Colorware.Core.Mvvm;
using Colorware.Core.Mvvm.AntiMemoryLeaks;

namespace Colorware.Models {
    /// <summary>
    /// A collection of <see cref="MatchedSample" /> and supporting methods.
    /// </summary>
    public class MatchedSampleSet : DefaultNotifyPropertyChanged {
        #region Properties
        private IReadOnlyList<MatchedSample> matchedSamples;

        public IReadOnlyList<MatchedSample> MatchedSamples {
            get { return matchedSamples; }
            set {
                matchedSamples = value;
                OnPropertyChanged("MatchedSamples");
                OnPropertyChanged("IsAnyLikelyWrong");
            }
        }

        private IReadOnlyList<IJobStripPatch> jobStripPatches;

        public IReadOnlyList<IJobStripPatch> JobStripPatches {
            get { return jobStripPatches; }
            set {
                jobStripPatches = value;
                OnPropertyChanged("JobStripPatches");
                updateMatchedSamples();
            }
        }

        private List<ISample> measuredSamples;

        public List<ISample> MeasuredSamples {
            get { return measuredSamples; }
            set {
                measuredSamples = value;
                OnPropertyChanged("MeasuredSamples");
                OnPropertyChanged("HasSamples");
                updateMatchedSamples();
            }
        }

        private int startIndex;

        public int StartIndex {
            get { return startIndex; }
            set {
                startIndex = value;
                OnPropertyChanged("StartIndex");
            }
        }

        private int endIndex;

        public int EndIndex {
            get { return endIndex; }
            set {
                endIndex = value;
                OnPropertyChanged("EndIndex");
            }
        }

        public bool HasSamples {
            get { return MeasuredSamples != null && MeasuredSamples.Count > 0; }
        }

        public bool HasJobStripPatches {
            get { return JobStripPatches != null && JobStripPatches.Count > 0; }
        }

        public bool SampleCountEqualsJobStripPatchCount {
            get { return HasSamples && HasJobStripPatches && MeasuredSamples.Count == JobStripPatches.Count; }
        }

        public bool IsAnyLikelyWrong {
            get {
                if (MatchedSamples == null)
                    return false;
                return MatchedSamples.Any(ms => ms.IsLikelyWrong);
            }
        }

        public bool CanFinish {
            get { return HasSamples; }
        }

        private ColorbarAlignment alignmentMode;

        public ColorbarAlignment AlignmentMode {
            get { return alignmentMode; }
            set {
                alignmentMode = value;
                OnPropertyChanged("AlignmentMode");
            }
        }

        public bool IncludesPaperwhite {
            get {
                if (!HasSamples)
                    return false;
                return MatchedSamples.Any(ms => ms.HasSample && ms.JobStripPatch.IsPaperWhite);
            }
        }
        #endregion

        #region Constructor
        public MatchedSampleSet() : this(null, null) {
        }

        public MatchedSampleSet(IReadOnlyList<IJobStripPatch> jobStripPatches, List<ISample> samples) {
            JobStripPatches = jobStripPatches;
            MeasuredSamples = samples;
            loadDefaultAlignment();
        }
        #endregion

        private void loadDefaultAlignment() {
            AlignmentMode = GlobalConfigKeys.ColorbarAlignment.Value;
        }

        public void AddSamples(IEnumerable<ISample> samples) {
            if (MeasuredSamples == null)
                MeasuredSamples = new List<ISample>(samples);
            else
                MeasuredSamples.AddRange(samples);
            if (MeasuredSamples.Count > JobStripPatches.Count)
                MeasuredSamples = MeasuredSamples.Take(JobStripPatches.Count).ToList();
            updateMatchedSamples();
        }


        public void ResetAllPatches() {
            MeasuredSamples = new List<ISample>();
        }

        public void ResetLastPatch() {
            if (MeasuredSamples == null)
                return;

            MeasuredSamples = new List<ISample>(MeasuredSamples.Take(MeasuredSamples.Count - 1));
        }

        /// <summary>
        /// Resets the last row.
        /// </summary>
        /// <param name="totalRows">Total number of rows in the color strip.</param>
        /// <remarks>
        /// This function assumes patches are measured in the same order as the patches are defined in strip.
        /// </remarks>
        public void ResetLastRow(int totalRows) {
            if (MeasuredSamples == null)
                return;
            if (totalRows < 1)
                totalRows = 1;

            var patchesPerRow = JobStripPatches.Count / totalRows;
            var offsetLastMeasuredPatch = StartIndex + MeasuredSamples.Count;

            if (offsetLastMeasuredPatch % patchesPerRow == 0) {
                MeasuredSamples = MeasuredSamples.Take(MeasuredSamples.Count - patchesPerRow).ToList();
            }
            else {
                MeasuredSamples =
                    MeasuredSamples.Take(MeasuredSamples.Count - offsetLastMeasuredPatch % patchesPerRow).ToList();
            }
        }

        public void Realign() {
            updateMatchedSamples();
        }

        private void updateMatchedSamples() {
            var newMatchedSamples = new List<MatchedSample>();
            if (JobStripPatches == null) {
                MatchedSamples = newMatchedSamples.ToNonLeakyList();
                return;
            }
            if (MeasuredSamples == null || MeasuredSamples.Count <= 0) {
                // Add all patches with empty samples.
                foreach (var jsp in JobStripPatches) {
                    var matchedSample = new MatchedSample(jsp, null);
                    newMatchedSamples.Add(matchedSample);
                }
            }
            else {
                // Try to detect position and slice it.
                var detector = new JobStripSliceDetector(jobStripPatches, AlignmentMode);
                detector.Detect(MeasuredSamples);
                StartIndex = detector.StartIndex;
                EndIndex = detector.EndIndex;
                Debug.Assert(StartIndex <= EndIndex);
                var sampleIndex = 0;
                for (var idx = 0; idx < jobStripPatches.Count; idx ++) {
                    var jsp = JobStripPatches[idx];
                    if (idx >= StartIndex && idx <= EndIndex) {
                        var sample = MeasuredSamples[sampleIndex];
                        var matchedSample = new MatchedSample(jsp, sample);
                        newMatchedSamples.Add(matchedSample);
                        sampleIndex++;
                    }
                    else {
                        var matchedSample = new MatchedSample(jsp, null);
                        newMatchedSamples.Add(matchedSample);
                    }
                }
                // Code below only selects matched parts. Code above shows whole definition with matches
                // at appropriate position.
                /*
                for(var jspIndex = detector.StartIndex; jspIndex <= detector.EndIndex; jspIndex ++, sampleIndex++ ) {
                    var jsp = JobStripPatches[jspIndex];
                    var sample = MeasuredSamples[sampleIndex];
                    var matchedSample = new MatchedSample(jsp, sample);
                    newMatchedSamples.Add(matchedSample);
                }
                */
            }
            MatchedSamples = newMatchedSamples.ToNonLeakyList();
        }

        public void UpdateErrorCache(IMeasurementErrorCache measurementErrorCache) {
            if (MatchedSamples == null)
                return;
            measurementErrorCache.StartUpdate(MatchedSamples.Count);
            for (var i = 0; i < MatchedSamples.Count; i++) {
                if (MatchedSamples[i].IsLikelyWrong)
                    measurementErrorCache.Mark(i);
            }
            measurementErrorCache.EndUpdate();
        }
    }
}