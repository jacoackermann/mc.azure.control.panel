using Colorware.Core.Data.Models;

namespace Colorware.Models {
    public class MeasurementErrorCache : IMeasurementErrorCache {
        #region Properties
        private IJob job;

        public IJob Job {
            get { return job; }
            set {
                checkForInvalidation(job, value);
                job = value;
            }
        }

        private int numOldErrors;
        private bool[] oldErrors;
        private int numNewErrors;
        private bool[] newErrors;

        public bool RequiresCheck { get; private set; }
        #endregion

        private void checkForInvalidation(IJob oldJob, IJob newJob) {
            if (oldJob == null || newJob == null || oldJob.Id != newJob.Id)
                Invalidate();
        }

        public void Invalidate() {
            numOldErrors = 0;
            numNewErrors = 0;
            oldErrors = null;
            newErrors = null;
        }

        public void StartUpdate(int sampleCount) {
            numOldErrors = numNewErrors;
            oldErrors = newErrors;
            numNewErrors = 0;
            newErrors = new bool[sampleCount];
        }

        public void Mark(int index) {
            newErrors[index] = true;
            numNewErrors++;
        }

        public void EndUpdate() {
            RequiresCheck = false;
            if (numNewErrors <= 0) {
                RequiresCheck = false;
                return;
            }
            if (oldErrors == null || oldErrors.Length != newErrors.Length) {
                RequiresCheck = numNewErrors > 0;
                return;
            }
            if (numNewErrors != numOldErrors) {
                RequiresCheck = true;
                return;
            }
            for (int i = 0; i < newErrors.Length; i ++) {
                if (newErrors[i] != oldErrors[i]) {
                    RequiresCheck = true;
                    return;
                }
            }
        }
    }
}