using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

using Colorware.Core.Color;
using Colorware.Core.Color.Support.Exceptions;
using Colorware.Core.Config;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.ChromaTrack;
using Colorware.Core.Data.Models.Compare;
using Colorware.Core.Data.Models.PantoneLIVE;
using Colorware.Core.Data.Models.StripCompare;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Enums;
using Colorware.Core.Functional.Option;
using Colorware.Core.Globalization;
using Colorware.Core.Models.Image;
using Colorware.Models.ServiceModels;

using JetBrains.Annotations;

namespace Colorware.Models.StripCompare {
    /// <summary>
    /// Defines a comparison between a job strip patch and a sample.
    /// </summary>
    public class StripComparePatch : IStripComparePatch, INotifyPropertyChanged {
        /// <summary>
        /// What is the maximal delta e to the paperwhite patch for use to consider this as a
        /// paperwhite patch as well?
        /// </summary>
        private const double MaxDeltaEToConsiderAsPaperWhite = 2.0;

        // TODO: Should be added to Tolerance set?
        private const string DudThresholdValueKey = "Defaults.ProcessControl.DudThresholdValue";

        private readonly double dudThresholdValue = Config.DefaultConfig.GetDouble(DudThresholdValueKey, 0.5);

        #region Properties
        public int Index { get; set; }

        /// <summary>
        /// If available, this will contain the paperwhite compare patch.
        /// </summary>
        public IStripComparePatch PaperWhite { get; set; }

        /// <inheritdoc />
        public ISample SampleForSolidKParentOfInkZone { get; set; }

        /// <inheritdoc />
        public ISample SampleForClosestCmy { get; set; }

        /// <summary>
        /// If available, this will contain the full color compare patch of the same family
        /// </summary>
        public IStripComparePatch Solid { get; set; }

        /// <summary>
        /// Return the IStripReferencePatch associated with this compare patch.
        /// </summary>
        public IStripReferencePatch ReferencePatch { get; }

        /// <summary>
        /// If we're a summary patch, this is a random patch from the collection of patches
        /// from which the summary was created. (Design error)
        /// </summary>
        // TODO: if we're a summary patch this should probably be removed or be the average
        // of all patches from which we were created. Better yet: extract the summary patch concept
        // from this class into its own class.
        public ISample Sample { get; }

        /// <summary>
        /// Signifies if this patch is created as a summary or not.
        /// </summary>
        public bool IsSummary { get; set; }

        private int numberOfPatches;

        public int NumberOfPatches {
            get {
                if (!IsSummary)
                    return 1;

                return numberOfPatches;
            }
            private set { numberOfPatches = value; }
        }

        private int numberOfPatchesInTolerance;

        public int NumberOfPatchesInTolerance {
            get {
                if (!IsSummary)
                    return InTolerance ? 1 : 0;

                return numberOfPatchesInTolerance;
            }
            private set { numberOfPatchesInTolerance = value; }
        }

        private int numberOfPatchesInToleranceRequired;

        public int NumberOfPatchesInToleranceRequired {
            get {
                if (!IsSummary)
                    return 1;

                return numberOfPatchesInToleranceRequired;
            }
            private set { numberOfPatchesInToleranceRequired = value; }
        }

        private bool isPrinted;

        /// <summary>
        /// Denotes if this patch is actually printed on the sheet.
        /// </summary>
        public bool IsPrinted {
            get { return isPrinted; }
            set { isPrinted = value; }
        }

        public bool IsDud { get; private set; }

        public bool IsDensityOutlier => IsSolid && IsDefined && Math.Abs(DiffDensity) >= dudThresholdValue;

        public bool IsLocked { get; set; }

        public bool IsSpot { get; set; }

        private ColorComparer<IStripReferencePatch> comparer;

        /// <summary>
        /// Cached comparer value. Only available when this is NOT a summary patch.
        /// </summary>
        public ColorComparer<IStripReferencePatch> Comparer => comparer;

        #region Results
        public bool InTolerance { get; set; }

        public bool HasTolerance { get; set; }

        public int Tint { get; set; }

        public ToleranceGroups ToleranceGroup { get; set; }

        public Lab SampleLab { get; set; }

        public Lab RefLab { get; set; }

        private Lch sampleLch;

        public Lch SampleLch {
            get { return sampleLch ?? (sampleLch = SampleLab.AsLch); }
            set { sampleLch = value; }
        }

        private Lch refLch;

        public Lch RefLch {
            get { return refLch ?? (refLch = RefLab.AsLch); }
            set { refLch = value; }
        }

        private Lch referenceLch;

        public Lch ReferenceLch {
            get { return referenceLch ?? (referenceLch = RefLab.AsLch); }
            set { referenceLch = value; }
        }

        public String SampleSpectrum { get; set; }
        public int SampleSpectrumStart { get; set; }
        public int SampleSpectrumEnd { get; set; }

        public bool HasSampleSpectrum => !String.IsNullOrEmpty(SampleSpectrum);

        public Spectrum AsSampleSpectrum => new Spectrum(SampleSpectrum, SampleSpectrumStart, SampleSpectrumEnd, ",");

        public String ReferenceSpectrum { get; set; }
        public int ReferenceSpectrumStart { get; set; }
        public int ReferenceSpectrumEnd { get; set; }

        public bool HasReferenceSpectrum => !string.IsNullOrEmpty(ReferenceSpectrum);

        public Spectrum AsReferenceSpectrum
            => new Spectrum(ReferenceSpectrum, ReferenceSpectrumStart, ReferenceSpectrumEnd, ",");

        public double Cyan { get; set; }
        public double Magenta { get; set; }
        public double Yellow { get; set; }
        public double Key { get; set; }

        private CMYK sampleDensity;

        public CMYK SampleDensity {
            get { return sampleDensity ?? (sampleDensity = new CMYK(0, 0, 0, 0)); }
            set { sampleDensity = value; }
        }

        public CMYK Densities => Sample.Densities;

        public double Density => SampleDensity.GetFilteredValue();

        private CMYK refDensity;

        // TODO
        /// <summary>
        /// This sometimes has all 4 channels, sometimes only 1! (design failure)
        /// </summary>
        public CMYK RefDensity {
            get { return refDensity ?? (refDensity = new CMYK(0, 0, 0, 0)); }
            set { refDensity = value; }
        }

        public bool HasTrapping { get; set; }
        public double Trapping { get; set; }
        public double DeltaE { get; set; }
        public double DeltaH { get; set; }
        public double ChromaPlus { get; set; }

        public CMYK GrayBalanceSample { get; set; }
        public CMYK GrayBalanceReference { get; set; }
        public CMYK GrayBalanceDifference { get; set; }
        public CMYK GrayBalanceDifferenceG7 { get; set; }

        public bool HasDotgain { get; set; }
        public DotgainMethod DotgainMethod { get; set; }
        public DeltaEMethod DeltaEMethod { get; private set; }
        public double SampleDotGain { get; set; }
        public double RefDotGain { get; set; }

        public bool HasPredictedDensityResult { get; private set; }
        public CMYK PredictedDeltaCMYKDensityForBestMatch { get; private set; }

        public double PredictedDeltaDensityForBestMatch {
            get {
                if (!HasPredictedDensityResult)
                    return 0.0;
                var filter = SampleDensity.GetFilteredComponent();
                return PredictedDeltaCMYKDensityForBestMatch.GetFilteredValue(filter);
            }
        }

        public double PredictedDeltaEForBestMatch { get; private set; }

        public double PredictedBestDensity {
            get {
                if (!HasPredictedDensityResult)
                    return 0.0;
                var filter = SampleDensity.GetFilteredComponent();
                var diff = PredictedDeltaCMYKDensityForBestMatch.GetFilteredValue(filter);
                return SampleDensity.GetFilteredValue(filter) + diff;
            }
        }

        private Lab predictedOptimalLab;

        public Lab PredictedOptimalLab {
            get {
                if (!HasPredictedDensityResult)
                    return new Lab(0, 0, 0, SpectralMeasurementConditions.Undefined);

                return predictedOptimalLab;
            }
        }

        public double SampleDotgainForCurrentMode => SampleDotGain;

        public double ReferenceDotgainForCurrentMode => RefDotGain;

        public bool HasScumming { get; set; }
        public double Scumming { get; set; }
        public Lab ScummingSample { get; set; }

        public bool HasG7WeightedDiffL { get; private set; }
        public bool HasG7WeightedDiffCh { get; private set; }
        public double G7WeightedDiffL { get; private set; }
        public double G7WeightedDiffCh { get; private set; }

        public bool HasDeltaEFromTolerance { get; set; }
        public double DeltaEFromTolerance { get; set; }

        public bool HasDotgainFromTolerance { get; set; }
        public double DotgainFromTolerance { get; set; }

        /// <summary>
        /// When set, signifies that a tolerance override is in effect for this patch and the value
        /// denotes the tolerance to use.
        /// </summary>
        public Option<double> DeltaEToleranceOverride { get; set; }
        #endregion Results

        #region Helpers
        public System.Windows.Media.Color AsColor => SampleLab.AsColor;

        public string PatchTypeGenericName {
            get {
                switch (PatchType) {
                    case PatchTypes.Solid:
                        return LanguageManager._("CW.Data.ComparePatch.Solid");

                    case PatchTypes.Overprint:
                        return LanguageManager._("CW.Data.ComparePatch.Overprint");

                    case PatchTypes.Paperwhite:
                        return LanguageManager._("CW.Data.ComparePatch.Paper");

                    case PatchTypes.Balance:
                    case PatchTypes.BalanceHighlight:
                    case PatchTypes.BalanceMidtone:
                    case PatchTypes.BalanceShadow:
                        return LanguageManager._("CW.Data.ComparePatch.Balance");

                    default:
                        return PatchType.ToString();
                }
            }
        }

        /// <summary>
        /// Return true if this is an `important' patch (Service announcement: "Important" to be
        /// defined later...)
        /// </summary>
        public bool IsImportantPatch => PatchType == PatchTypes.Solid ||
                                        PatchType == PatchTypes.Overprint ||
                                        PatchType == PatchTypes.Paperwhite ||
                                        PatchType == PatchTypes.Balance ||
                                        PatchType == PatchTypes.BalanceHighlight ||
                                        PatchType == PatchTypes.BalanceMidtone ||
                                        PatchType == PatchTypes.BalanceShadow;

        public int PatchTypeSortNumber {
            get {
                switch (PatchType) {
                    case PatchTypes.Solid:
                        return -10;

                    case PatchTypes.Overprint:
                        return -8;

                    case PatchTypes.Paperwhite:
                        return -6;

                    case PatchTypes.Balance:
                    case PatchTypes.BalanceHighlight:
                    case PatchTypes.BalanceMidtone:
                    case PatchTypes.BalanceShadow:
                        // We want them to be sorted from light to dark, but we also want them to be
                        // positioned at the end.
                        return (300 - (int)SampleLab.L);

                    default:
                        return (int)PatchType;
                }
            }
        }

        #region Deltas
        public double DiffL => SampleLab.L - RefLab.L;

        public double DiffA => SampleLab.a - RefLab.a;

        public double DiffB => SampleLab.b - RefLab.b;

        public double DiffC => SampleLch.c - ReferenceLch.c;

        public double DiffH => SampleLch.h - ReferenceLch.h;

        public double DiffDensity {
            get {
                var comp = SampleDensity.GetFilteredComponent();
                return SampleDensity.GetFilteredValue(comp) - RefDensity.GetFilteredValue(comp);
            }
        }

        public CMYK DiffDensityFull => SampleDensity - RefDensity;

        public double DotgainDiff => SampleDotGain - RefDotGain;
        #endregion Deltas

        #region Lightness, Chroma and Hue
        public bool IsLighter => SampleLch.L - ReferenceLch.L > 0;

        public bool IsDarker => SampleLch.L - ReferenceLch.L < 0;

        public bool IsDuller => SampleLch.c - ReferenceLch.c < 0;

        public bool IsStronger => SampleLch.c - ReferenceLch.c > 0;

        public bool IsRedder => SampleLch.h - ReferenceLch.h < 0;

        public bool IsYellower => SampleLch.h - ReferenceLch.h > 0;
        #endregion Lightness, Chroma and Hue

        public bool IsSolid => PatchType == PatchTypes.Solid;

        public bool IsOverprint => PatchType == PatchTypes.Overprint;

        public bool IsPaperWhite => PatchType == PatchTypes.Paperwhite;

        public bool IsBalanceHighlight => PatchType == PatchTypes.BalanceHighlight;

        public bool IsBalanceMidtone => PatchType == PatchTypes.BalanceMidtone;

        public bool IsBalanceShadow => PatchType == PatchTypes.BalanceShadow;

        public bool IsDotgain => PatchType == PatchTypes.Dotgain || PatchType == PatchTypes.DotgainOnly;

        public bool IsPantoneLivePatch => !IsUndefined && ReferencePatch.IsPantoneLivePatch;

        public PantoneLiveStandardId PantoneLiveStandardId => IsUndefined
                                                                  ? new PantoneLiveStandardId(Guid.Empty, Guid.Empty)
                                                                  : new PantoneLiveStandardId(
                                                                      ReferencePatch.PantoneLivePaletteGuid,
                                                                      ReferencePatch.PantoneLiveDependentStandardGuid);

        public bool ChromaPlusIsPositive => ChromaPlus >= 0;

        public bool ChromaPlusIsNegative => ChromaPlus < 0;

        public bool ChromaTrackBestMatchIsNotInTolerance => HasPredictedDensityResult && HasDeltaEFromTolerance &&
                                                            PredictedDeltaEForBestMatch > DeltaEFromTolerance;

        public bool ChromaTrackBestMatchIsPositive {
            get {
                var result = false;
                if (HasPredictedDensityResult && (PatchType == PatchTypes.Solid || PatchType == PatchTypes.Dotgain)) {
                    if (HasDeltaEFromTolerance) {
                        result = PredictedDeltaEForBestMatch <= DeltaEFromTolerance &&
                                 PredictedDeltaDensityForBestMatch >= 0;
                    }
                    else if (PatchType == PatchTypes.Dotgain && HasDotgainFromTolerance) {
                        result = Math.Abs(DotgainDiff) > DotgainFromTolerance &&
                                 DotgainDiff < 0;
                    }
                }
                return result;
            }
        }

        public bool ChromaTrackBestMatchIsNegative {
            get {
                var result = false;
                if (HasPredictedDensityResult && (PatchType == PatchTypes.Solid || PatchType == PatchTypes.Dotgain)) {
                    if (HasDeltaEFromTolerance) {
                        result = PredictedDeltaEForBestMatch <= DeltaEFromTolerance &&
                                 PredictedDeltaDensityForBestMatch < 0;
                    }
                    else if (PatchType == PatchTypes.Dotgain && HasDotgainFromTolerance) {
                        result = Math.Abs(DotgainDiff) > DotgainFromTolerance &&
                                 DotgainDiff >= 0;
                    }
                }
                return result;
            }
        }

        /// <summary>
        /// If true, represents a patch which is only valid for its dotgain data. This is basically
        /// meant for spot colors with a percentage of &lt; 100% which can only be defined by using a
        /// standard dotgain curve (DefaultDotgainList/DefaultDotgainEntry).
        /// </summary>
        public bool IsDotgainOnly => PatchType == PatchTypes.DotgainOnly;

        public bool IsDefined => PatchType != PatchTypes.Undefined;

        public bool IsSingleSlot {
            get { return new[] {Slot1, Slot2, Slot3}.Count(s => s != ColorStripPatch.NO_SLOT_VALUE) == 1; }
        }

        public int SingleSlot {
            get {
                if (Slot1 != ColorStripPatch.NO_SLOT_VALUE)
                    return Slot1;
                if (Slot2 != ColorStripPatch.NO_SLOT_VALUE)
                    return Slot2;
                if (Slot3 != ColorStripPatch.NO_SLOT_VALUE)
                    return Slot3;
                throw new Exception("Invalid slot definition");
            }
        }

        public string GuessFilter() {
            var comp = SampleDensity.GetFilteredComponent();
            switch (comp) {
                case CmykComponents.C:
                    return "Cyan";
                case CmykComponents.M:
                    return "Magenta";
                case CmykComponents.Y:
                    return "Yellow";

                default:
                    return "Black";
            }
        }

        // TODO: this currently depends on the patch name...
        public CmykComponents GuessComponent(out bool isComponent) {
            isComponent = true;
            var upperName = Name.ToUpperInvariant();
            if (upperName.StartsWith("C") && SampleDensity.GetHighestComponent() == CmykComponents.C)
                return CmykComponents.C;
            if (upperName.StartsWith("M") && SampleDensity.GetHighestComponent() == CmykComponents.M)
                return CmykComponents.M;
            if (upperName.StartsWith("Y") && SampleDensity.GetHighestComponent() == CmykComponents.Y)
                return CmykComponents.Y;
            if (upperName.StartsWith("K"))
                return CmykComponents.K;
            isComponent = false;
            return CmykComponents.C;
        }

        public IStripComparePatch Copy() {
            var copy = new StripComparePatch(ReferencePatch, Sample) {
                Slot1 = Slot1,
                Slot2 = Slot2,
                Slot3 = Slot3,
                TargetX = TargetX,
                TargetY = TargetY,
                MeasureTarget = MeasureTarget,
                IsPrinted = IsPrinted,
                IsSpot = IsSpot,
                Name = Name,
                Tint = Tint,
                PatchType = PatchType,
                ToleranceGroup = ToleranceGroup,
                InTolerance = InTolerance,
                InkZone = InkZone,
                SampleLab = SampleLab,
                RefLab = RefLab,
                SampleLch = SampleLch,
                RefLch = RefLch,
                SampleDensity = SampleDensity,
                RefDensity = RefDensity,
                DeltaE = DeltaE,
                DeltaH = DeltaH,
                ChromaPlus = ChromaPlus,
                GrayBalanceSample = GrayBalanceSample,
                GrayBalanceReference = GrayBalanceReference,
                GrayBalanceDifference = GrayBalanceDifference,
                GrayBalanceDifferenceG7 = GrayBalanceDifferenceG7,
                SampleDotGain = SampleDotGain,
                RefDotGain = RefDotGain,
                IsSummary = IsSummary,
                IsDud = IsDud
            };
            return copy;
        }

        public void SetSample(ISample sampleToSet) {
            SampleLab = Sample.AsLab;
            SampleLch = SampleLab.AsLch;
            SampleDensity = Sample.Densities;
            SampleDotGain = 0;
            UpdateCompareResults();
        }

        public void UpdateCompareResults() {
            throw new NotImplementedException();
        }
        #endregion Helpers
        #endregion Properties

        #region Associated properties
        public int Slot1 { get; set; }
        public int Slot2 { get; set; }
        public int Slot3 { get; set; }

        public bool HasSlot1 => Slot1 != ColorStripPatch.NO_SLOT_VALUE;

        public bool HasSlot2 => Slot2 != ColorStripPatch.NO_SLOT_VALUE;

        public bool HasSlot3 => Slot3 != ColorStripPatch.NO_SLOT_VALUE;

        // For Image
        public int TargetX { get; set; }

        public int TargetY { get; set; }
        public IMeasureTarget MeasureTarget { get; set; }

        /// <summary>
        /// Return the name of the patch.
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Denotes the patch type of this compare patch.
        /// </summary>
        public PatchTypes PatchType { get; set; }

        /// <summary>
        /// Helper function: Is this a dotgain or dotgain only patch?
        /// </summary>
        public bool IsDotgainLike => PatchType == PatchTypes.Dotgain || PatchType == PatchTypes.DotgainOnly;

        /// <summary>
        /// The ink zone of the current patch.
        /// </summary>
        public int InkZone { get; set; }

        /// <summary>
        /// Id of the related reference patch.
        /// </summary>
        public long ReferencePatchId { get; set; }

        /// <summary>
        /// Is the current patch undefined?
        /// </summary>
        public bool IsUndefined => PatchType == PatchTypes.Undefined;

        /// <summary>
        /// Is the current patch defined?
        /// </summary>
        #endregion Associated properties

        /// <summary>
        /// Construct a single result patch. This must be used when NOT specifying a summary patch.
        /// </summary>
        public StripComparePatch([NotNull] IStripReferencePatch referencePatch, [NotNull] ISample sample) {
            ReferencePatch = referencePatch ?? throw new ArgumentNullException(nameof(referencePatch));
            Sample = sample ?? throw new ArgumentNullException(nameof(sample));
        }

        [NotNull]
        public static IStripComparePatch CreateSummaryPatch([NotNull] IStripReferencePatch referencePatch,
                                                            [NotNull] ISample sample,
                                                            [NotNull] IToleranceSet toleranceSet,
                                                            [ItemNotNull, NotNull] IReadOnlyList<IStripComparePatch> patches) {
            if (referencePatch == null) throw new ArgumentNullException(nameof(referencePatch));
            if (sample == null) throw new ArgumentNullException(nameof(sample));
            if (toleranceSet == null) throw new ArgumentNullException(nameof(toleranceSet));
            if (patches == null) throw new ArgumentNullException(nameof(patches));
            if (patches.Count == 0)
                throw new ArgumentException("Value cannot be an empty collection.", nameof(patches));

            var p = new StripComparePatch(referencePatch, sample) {IsSummary = true};

            p.initSummary(toleranceSet, patches);

            return p;
        }

        /// <summary>
        /// Construct an invalid patch for a given inkzone
        /// </summary>
        private StripComparePatch(int inkZone) {
            ReferencePatch = null;
            Sample = null;
            IsSummary = false;
            InkZone = inkZone;
            PatchType = PatchTypes.Undefined;
            InTolerance = true;
        }

        public static StripComparePatch Invalid(int inkZone) {
            return new StripComparePatch(inkZone);
        }

        /// <summary>
        /// Update the IsPrinted property.
        /// </summary>
        public void UpdateIsPrinted() {
            isPrinted = true;
            if (PaperWhite == null)
                return;
            var deltaE = PaperWhite.Sample.AsLab.DeltaE76(Sample.AsLab);
            if (deltaE <= MaxDeltaEToConsiderAsPaperWhite)
                isPrinted = false;
        }

        /// <summary>
        /// Checks if this patch has any need of knowing about the passed in solid patch.
        /// </summary>
        /// <param name="solid">The solid to compare with.</param>
        /// <returns>true if the Solid property of this patch can be set to the passed in solid.</returns>
        public bool CanAcceptSolid(IStripComparePatch solid) {
            if (!IsDotgainLike)
                return false;
            return solid.Slot1 == Slot1;
        }

        /// <summary>
        /// Create a sample from this result.
        /// </summary>
        /// <returns>A new sample</returns>
        public ISample AsSample() {
            return HasSampleSpectrum
                       ? new Sample(AsSampleSpectrum, SampleLab, SampleDensity)
                       : new Sample(SampleLab, SampleDensity);
        }

        /// <summary>
        /// Create a JobStripPatch from this result.
        /// </summary>
        /// <returns>A new job strip patch.</returns>
        public IJobStripPatch AsJobStripPatch() {
            return new JobStripPatch {
                L = RefLab.L,
                a = RefLab.a,
                b = RefLab.b,
                DefaultDensity = RefDensity.GetFilteredValue(),
                DefaultDotgain = RefDotGain,
                InkZone = InkZone,
                PatchType = PatchType,
                Name = Name,
                ToleranceGroup = ToleranceGroup,
                Percentage = Tint,
                MCondition = ReferencePatch.MCondition,
                Illuminant = ReferencePatch.Illuminant,
                Observer = ReferencePatch.Observer,
                TargetX = TargetX,
                TargetY = TargetY,
                ReferencePatchId = ReferencePatchId,
                Slot1 = Slot1,
                Slot2 = Slot2,
                Slot3 = Slot3
            };
        }

        /// <summary>
        /// Create a new reference from this result.
        /// </summary>
        /// <returns>A new reference patch.</returns>
        public IReferencePatch AsReferencePatch() {
            return new ReferencePatch(
                new SpectralMeasurementConditions(
                    ReferencePatch.Illuminant,
                    ReferencePatch.Observer,
                    ReferencePatch.MCondition)) {
                L = RefLab.L,
                a = RefLab.a,
                b = RefLab.b,
                DefaultDensity = RefDensity.GetFilteredValue(),
                DefaultDotgain = RefDotGain,
                Name = Name,
                PatchType = PatchType,
                ToleranceGroup = ToleranceGroup,
                Percentage = Tint,
                Spectrum = ReferenceSpectrum,
                SpectrumStart = ReferenceSpectrumStart,
                SpectrumEnd = ReferenceSpectrumEnd,
                Cyan = ReferencePatch.Cyan,
                Magenta = ReferencePatch.Magenta,
                Yellow = ReferencePatch.Yellow,
                Key = ReferencePatch.Key
            };
        }

        public Lab AsLab => ReferencePatch.AsLab;

        /// <summary>
        /// Initialize compare results for the given sample and reference data.
        /// </summary>
        public void InitResults([NotNull] IToleranceSet toleranceSet,
                                IMeasurementConditionsConfig measurementConditionsConfig, int patchIndex) {
            if (toleranceSet == null) throw new ArgumentNullException(nameof(toleranceSet));
            if (IsSummary)
                throw new InvalidOperationException("Should only call this method on none summary patches");

            Name = ReferencePatch.Name;
            Index = patchIndex;
            Tint = ReferencePatch.Percentage;
            InkZone = ReferencePatch.InkZone;
            PatchType = ReferencePatch.PatchType;
            Slot1 = ReferencePatch.Slot1;
            Slot2 = ReferencePatch.Slot2;
            Slot3 = ReferencePatch.Slot3;
            IsSpot = ReferencePatch.IsSpot;
            ToleranceGroup = ReferencePatch.ToleranceGroup;
            DotgainMethod = ReferencePatch.IsSpot
                                ? measurementConditionsConfig.DotgainMethodSpot
                                : measurementConditionsConfig.DotgainMethodProcess;
            comparer = new ColorComparer<IStripReferencePatch>(Sample,
                                                               ReferencePatch,
                                                               toleranceSet,
                                                               ScummingSample,
                                                               DeltaEToleranceOverride);
            HasDotgain = false;

            if (PaperWhite != null && Solid != null &&
                (PatchType == PatchTypes.Dotgain || PatchType == PatchTypes.DotgainOnly)) {
                // Calculate dotgain if possible.
                initDotgains(PaperWhite, Solid);
            }

            initG7(comparer);

            HasPredictedDensityResult = false;

            if (PatchType != PatchTypes.Undefined) {
                Debug.Assert(comparer.ToleranceReport != null,
                             "Did not expect `comparer.ToleranceReport` to be null here.");
                InTolerance = comparer.ToleranceReport.InTolerance;
                HasTolerance = comparer.ToleranceReport.HasTolerance;
                HasDeltaEFromTolerance = comparer.ToleranceReport.IsDeltaEToleranceCalculated;
                DeltaEFromTolerance = comparer.ToleranceReport.UsedDeltaETolerance;
                HasDotgainFromTolerance = comparer.ToleranceReport.IsDotgainToleranceCalculated;
                DotgainFromTolerance = comparer.ToleranceReport.UsedDotgainTolerance;
            }
            else {
                InTolerance = true;
                HasTolerance = false;
            }

            SampleLab = Sample.AsLab;
            RefLab = ReferencePatch.AsLab;
            SampleDensity = Sample.Densities;
            RefDensity = CMYK.BuildFromSingleValue(SampleDensity, ReferencePatch.Density);

            SampleSpectrum = Sample.Spectrum;
            SampleSpectrumStart = Sample.SpectrumStart;
            SampleSpectrumEnd = Sample.SpectrumEnd;

            ReferenceSpectrum = ReferencePatch.Spectrum;
            ReferenceSpectrumStart = ReferencePatch.SpectrumStart;
            ReferenceSpectrumEnd = ReferencePatch.SpectrumEnd;

            Cyan = ReferencePatch.Cyan;
            Magenta = ReferencePatch.Magenta;
            Yellow = ReferencePatch.Yellow;
            Key = ReferencePatch.Key;

            if (PatchType == PatchTypes.Undefined || PatchType == PatchTypes.DotgainOnly) {
                DeltaE = 0;
                DeltaH = 0;
                ChromaPlus = 0;

                HasG7WeightedDiffL = false;
                HasG7WeightedDiffCh = false;
                G7WeightedDiffL = 0;
                G7WeightedDiffCh = 0;

                Scumming = 0;
                HasScumming = false;
            }
            else {
                DeltaE = comparer.DeltaE;
                DeltaH = comparer.DeltaH;
                ChromaPlus = comparer.ChromaPlus;

                HasG7WeightedDiffL = comparer.HasG7WeightedDiffL;
                HasG7WeightedDiffCh = comparer.HasG7WeightedDiffCh;
                G7WeightedDiffL = comparer.G7WeightedDiffL;
                G7WeightedDiffCh = comparer.G7WeightedDiffCh;
                
                Scumming = comparer.Scumming;
                HasScumming = comparer.HasScumming;
            }

            IsDud = Sample.IsDud;
            HasTrapping = false;
            Trapping = 0.0;
        }

        private void initDotgains([NotNull] IStripComparePatch paperWhite, [NotNull] IStripComparePatch solid) {
            HasDotgain = true;

            var asOkSheetPatch = ReferencePatch as OKSheetPatch;

            // Ok sheet as reference
            if (asOkSheetPatch != null) {
                var okSheetSolid = solid.ReferencePatch as OKSheetPatch;

                if (okSheetSolid == null) {
                    throw new InvalidOperationException(
                        "Solid.ReferencePatch must be of type OKSheetPatch! (this is a bug)");
                }

                var okSheetPw = paperWhite.ReferencePatch as OKSheetPatch;

                // NB: if this is null, it's of type JSP, happens when the PW is taken from the job (OK sheet measurement has no PW)
                if (okSheetPw != null) {
                    comparer.InitDotgains(okSheetPw, okSheetSolid, asOkSheetPatch, paperWhite.Sample, solid.Sample,
                                          Sample, ReferencePatch.Percentage, Sample.CmykComponent,
                                          DotgainMethod);
                }
            }

            // Regular JSPs as reference, or: OK sheet as reference and the PW is a JSP
            if (!comparer.HasDotgain) {
                comparer.InitDotgains(paperWhite.ReferencePatch, solid.ReferencePatch, ReferencePatch,
                                      paperWhite.Sample, solid.Sample, Sample, ReferencePatch.Percentage,
                                      Sample.CmykComponent, ReferencePatch.DefaultDotgain,
                                      DotgainMethod,
                                      IsDotgainOnly);
            }

            SampleDotGain = comparer.SampleDotgain;
            RefDotGain = comparer.ReferenceDotgain;
        }

        private void initG7(ColorComparer<IStripReferencePatch> colorComparer) {
            switch (PatchType) {
                case PatchTypes.Dotgain when ReferencePatch.IsKey():
                    if (SampleForSolidKParentOfInkZone != null && PaperWhite != null) {
                        colorComparer.InitG7ForKeyDotgain(ReferencePatch.Key, Sample, PaperWhite.Sample, SampleForSolidKParentOfInkZone);
                    }
                    break;
                case PatchTypes.Overprint when ReferencePatch.IsThreeColorComponent():
                    if (PaperWhite != null) {
                        colorComparer.InitG7ForOverprint(Sample, PaperWhite.Sample);
                    }
                    break;
                case PatchTypes.BalanceHighlight:
                case PatchTypes.BalanceMidtone:
                case PatchTypes.BalanceShadow:
                    if (SampleForClosestCmy != null && PaperWhite != null) {
                        colorComparer.InitG7ForBalance(ReferencePatch.Cyan, Sample, PaperWhite.Sample, SampleForClosestCmy);
                    }
                    break;
            }
        }

        public void InitChromaTrack(IToleranceSet toleranceSet,
                                    IMeasurementConditionsConfig measurementConditionsConfig) {
            if (PaperWhite != null && new[] {PatchTypes.Solid, PatchTypes.Dotgain}.Contains(PatchType) &&
                HasSampleSpectrum && PaperWhite.Sample != null && PaperWhite.Sample.HasSpectrum &&
                measurementConditionsConfig != null) {
                // Init ink prediction.
                try {
                    var optimizer = new ChromaTrackOptimizer(toleranceSet);
                    var result = optimizer.Calculate(AsSampleSpectrum, PaperWhite.Sample.AsSpectrum, RefLab);
                    PredictedDeltaCMYKDensityForBestMatch =
                        result.CalculateBestDensityDifferenceCMYK(measurementConditionsConfig);
                    PredictedDeltaEForBestMatch = result.CalculateBestDeltaE();
                    predictedOptimalLab = result.OptimalLab;
                    HasPredictedDensityResult = true;
                }
                catch (ChromaTrackCalculationException) {
                    HasPredictedDensityResult = false;
                }
            }
        }

        /// <summary>
        /// Initialize this summary patch by calculating a summary of the given StripComparePatches.
        /// </summary>
        private void initSummary([NotNull] IToleranceSet toleranceSet,
                                [ItemNotNull, NotNull] IReadOnlyList<IStripComparePatch> patches) {
            if (toleranceSet == null) throw new ArgumentNullException(nameof(toleranceSet));
            if (patches == null) throw new ArgumentNullException(nameof(patches));
            if (patches.Count == 0)
                throw new ArgumentException("Value cannot be an empty collection.", nameof(patches));

            if (!IsSummary)
                throw new InvalidOperationException("Should only call this method on summary patches");

            Name = ReferencePatch.Name;
            Tint = ReferencePatch.Percentage;
            InkZone = ReferencePatch.InkZone;
            PatchType = ReferencePatch.PatchType;
            ToleranceGroup = ReferencePatch.ToleranceGroup;
            Slot1 = ReferencePatch.Slot1;
            Slot2 = ReferencePatch.Slot2;
            Slot3 = ReferencePatch.Slot3;

            var firstPatch = patches.First();

            // We assume that these values will be the same for each patch in the set:
            var mc = firstPatch.RefLab.MeasurementConditions;
            DeltaEToleranceOverride = firstPatch.DeltaEToleranceOverride;

            SampleLab = new Lab(0, 0, 0, mc);
            RefLab = new Lab(0, 0, 0, mc);
            SampleDensity = new CMYK(0, 0, 0, 0);
            RefDensity = new CMYK(0, 0, 0, 0);

            // Just leave the summary patch empty when all patches are duds.
            // (We DOactually want a summary patch because other parts depend on a summary
            // patch being available for every ink, regardless of them being all duds or not)
            if (patches.All(p => p.IsDud)) {
                IsDud = true;
                return;
            }

            IsSpot = firstPatch.IsSpot;

            // Exclude duds from summary patch calculations
            patches = patches.Where(p => !p.IsDud).ToImmutableList();

            DeltaE = 0;
            DeltaH = 0;
            ChromaPlus = 0;

            Cyan = ReferencePatch.Cyan;
            Magenta = ReferencePatch.Magenta;
            Yellow = ReferencePatch.Yellow;
            Key = ReferencePatch.Key;

            // Keeps track of status of the reference density. If this is from the database, it is
            // likely that it has been set from a single value. In this case, the CMYK will have the
            // shape of CMYK(0, 1.3, 0, 0), where the entered value will depend on the component
            // filter of the sample patch. In this case, we can't just average all the reference
            // densities, as the component filter may vary between different samples, which may
            // result in adding references like CMYK(0, 1.3, 0, 0) + CMYK(0, 1.3, 0, 0) + CMYK(0, 0,
            // 1.3, 0). Which in turn will result in a final density that is too low. If we detect
            // this situation, we handle the averaging of the reference density in a different manner
            // (see below).
            bool referenceIsLikelySingleValue = false;


            HasDotgain = false;
            SampleDotGain = 0;
            RefDotGain = 0;

            HasTrapping = false;
            Trapping = 0.0;
            var trappingCount = patches.Count(p => p.HasTrapping);

            HasG7WeightedDiffL = false;
            HasG7WeightedDiffCh = false;
            G7WeightedDiffL = 0;
            G7WeightedDiffCh = 0;

            var g7DiffLCount = 0;
            var g7DiffChCount = 0;
            
            HasScumming = false;
            Scumming = 0.0;
            var scummingCount = patches.Count(p => p.HasScumming);

            HasTolerance = true;

            NumberOfPatchesInTolerance = patches.Count(p => p.InTolerance);

            double[] spectralValues = null;
            double startWavelength = 400, endWavelength = 700;
            var spectralCount = patches.Count(p => p.HasSampleSpectrum);
            int numberOfChromaTrackPatches = 0;

            // NB: Duds are already excluded at this point
            foreach (var patch in patches) {
                referenceIsLikelySingleValue |= isReferenceLikelySingleValue(patch.SampleDensity, patch.RefDensity);

                DotgainMethod = patch.DotgainMethod; // Will just take the last one.
                SampleLab += patch.SampleLab;
                RefLab += patch.RefLab;
                SampleDensity += patch.SampleDensity;
                RefDensity += patch.RefDensity;

                DeltaE += patch.DeltaE;
                DeltaH += patch.DeltaH;
                ChromaPlus += patch.ChromaPlus;

                Index = patch.Index;
                ReferencePatchId = patch.ReferencePatchId;

                if (patch.HasDotgain) {
                    HasDotgain = true;
                    SampleDotGain += patch.SampleDotGain;
                    RefDotGain += patch.RefDotGain;
                }

                if (patch.HasTrapping) {
                    HasTrapping = true;
                    Trapping += patch.Trapping;
                }

                if (patch.HasG7WeightedDiffL) {
                    g7DiffLCount++;
                    HasG7WeightedDiffL = true;
                    G7WeightedDiffL += patch.G7WeightedDiffL;
                }

                if (patch.HasG7WeightedDiffCh) {
                    g7DiffChCount++;
                    HasG7WeightedDiffCh = true;
                    G7WeightedDiffCh += patch.G7WeightedDiffCh;
                }

                if (patch.HasScumming) {
                    HasScumming = true;
                    Scumming += patch.Scumming;
                    ScummingSample = patch.ScummingSample;
                }

                if (patch.HasSampleSpectrum) {
                    var sampleSpectrum = patch.AsSampleSpectrum;
                    if (spectralValues == null) {
                        spectralValues = new double[sampleSpectrum.ReflectanceValues.Count];
                        startWavelength = sampleSpectrum.StartWavelength;
                        endWavelength = sampleSpectrum.EndWaveLength;
                    }

                    var spectralValuesForCurrentSample = sampleSpectrum.ReflectanceValues;
                    for (int i = 0; i < spectralValues.Length; i++)
                        spectralValues[i] += spectralValuesForCurrentSample.ElementAt(i);
                }
                if (patch.HasPredictedDensityResult) {
                    if (numberOfChromaTrackPatches == 0) {
                        // First patch.
                        PredictedDeltaCMYKDensityForBestMatch = patch.PredictedDeltaCMYKDensityForBestMatch;
                        predictedOptimalLab = patch.PredictedOptimalLab;
                    }
                    else {
                        PredictedDeltaCMYKDensityForBestMatch += patch.PredictedDeltaCMYKDensityForBestMatch;
                        predictedOptimalLab += patch.PredictedOptimalLab;
                    }
                    PredictedDeltaEForBestMatch += patch.PredictedDeltaEForBestMatch;
                    numberOfChromaTrackPatches++;
                }

                HasTolerance = HasTolerance && patch.HasTolerance;
            }

            if (numberOfChromaTrackPatches > 0) {
                HasPredictedDensityResult = true;
                PredictedDeltaCMYKDensityForBestMatch = PredictedDeltaCMYKDensityForBestMatch /
                                                        numberOfChromaTrackPatches;
                PredictedDeltaEForBestMatch /= numberOfChromaTrackPatches;
                predictedOptimalLab /= numberOfChromaTrackPatches;
            }
            else {
                HasPredictedDensityResult = false;
            }

            var totalCount = patches.Count;
            
            SampleLab /= totalCount;
            RefLab /= totalCount;
            SampleDensity /= totalCount;

            if (referenceIsLikelySingleValue) {
                var component = SampleDensity.GetFilteredComponent();
                var avg = RefDensity.Sum() / totalCount;
                RefDensity = new CMYK(avg, component);
            }
            else {
                RefDensity /= totalCount;
            }

            DeltaE /= totalCount;
            DeltaH /= totalCount;
            ChromaPlus /= totalCount;

            if (HasDotgain) {
                var dotgainCount = patches.Count(p => p.HasDotgain);
                SampleDotGain /= dotgainCount;
                RefDotGain /= dotgainCount;
            }

            if (HasTrapping)
                Trapping /= trappingCount;

            if (HasG7WeightedDiffL) {
                G7WeightedDiffL /= g7DiffLCount;
            }

            if (HasG7WeightedDiffCh) {
                G7WeightedDiffCh /= g7DiffChCount;
            }

            if (HasScumming)
                Scumming /= scummingCount;

            if (spectralValues != null) {
                for (int i = 0; i < spectralValues.Length; i++)
                    spectralValues[i] /= spectralCount;

                SampleSpectrum = string.Join(",", spectralValues.Select(v => v.ToString(CultureInfo.InvariantCulture)));
                SampleSpectrumStart = (int)startWavelength;
                SampleSpectrumEnd = (int)endWavelength;
            }

            calculateNumberOfPatchesRequiredForInTolerance(toleranceSet, patches);
            InTolerance = NumberOfPatchesInTolerance >= NumberOfPatchesInToleranceRequired;
        }

        /// <summary>
        /// Try to determine if a reference density is build out of a single value. Also see PV-1836.
        /// </summary>
        /// <param name="patchSampleDensity">The full density of the sample.</param>
        /// <param name="patchRefDensity">
        /// The full density of the reference. This may actually consist of a single value, in which
        /// case one component will be set and the rest will be 0.
        /// </param>
        /// <returns>
        /// True if the reference density is likely build from a single value, false otherwise.
        /// </returns>
        private bool isReferenceLikelySingleValue(CMYK patchSampleDensity, CMYK patchRefDensity) {
            var component = patchSampleDensity.GetFilteredComponent();
            switch (component) {
                case CmykComponents.C:
                    return patchRefDensity.C > 0 &&
                           patchRefDensity.M <= 0 &&
                           patchRefDensity.Y <= 0 &&
                           patchRefDensity.K <= 0;

                case CmykComponents.M:
                    return patchRefDensity.C <= 0 &&
                           patchRefDensity.M > 0 &&
                           patchRefDensity.Y <= 0 &&
                           patchRefDensity.K <= 0;

                case CmykComponents.Y:
                    return patchRefDensity.C <= 0 &&
                           patchRefDensity.M <= 0 &&
                           patchRefDensity.Y > 0 &&
                           patchRefDensity.K <= 0;

                case CmykComponents.K:
                    return patchRefDensity.C <= 0 &&
                           patchRefDensity.M <= 0 &&
                           patchRefDensity.Y <= 0 &&
                           patchRefDensity.K > 0;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void calculateNumberOfPatchesRequiredForInTolerance([NotNull] IToleranceSet toleranceSet,
                                                                    [NotNull] IEnumerable<IStripComparePatch> patches) {
            NumberOfPatches = patches.Count();
            var scorePercentage = toleranceSet.GetScorePercentageForType(PatchType,
                                                                         ToleranceGroup == ToleranceGroups.Spotcolor);
            NumberOfPatchesInToleranceRequired = (int)Math.Ceiling(NumberOfPatches * scorePercentage);
        }

        #region Overrides of Object
        public override string ToString() {
            return $"StripComparePatch({Name} {Tint}@{InkZone} ({PatchType}) {Slot1},{Slot2},{Slot3})";
        }
        #endregion Overrides of Object

        // Anti memory leak
#pragma warning disable 0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 0067
    }
}