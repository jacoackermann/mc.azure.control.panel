using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using Castle.Core.Logging;

using Colorware.Core;
using Colorware.Core.Color;
using Colorware.Core.Config;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.G7Analysis;
using Colorware.Core.Data.Models.StripCompare;
using Colorware.Core.Data.Models.StripCompare.PatchSorters;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;
using Colorware.Core.Extensions;
using Colorware.Core.Functional.Option;
using Colorware.Models.GrayFinder;
using Colorware.Models.ServiceModels;

using JetBrains.Annotations;

namespace Colorware.Models.StripCompare {
    /// <summary>
    /// Class which allows the checking of a job strip with a measurement.
    /// </summary>
    public class StripCompareCalculator : IStripCompareCalculator {
        private readonly IJob job;
        private readonly IMeasurement measurement;
        private readonly IMachine machine;
        private readonly IReferencePatch whiteReference;
        private readonly List<IJobStripPatch> jobStripPatches;
        private readonly IToleranceSet toleranceSet;
        private readonly IMeasurementConditionsConfig measurementConditionsConfig;
        private readonly IEnumerable<IReferencePatch> referencePatches;
        private ISample scummingSample;
        private readonly ILogger log;

        private IStripComparePatchSorter stripComparePatchSorter;

        private readonly ReferencePatchGrayFinders referencePatchGrayFinders;

        public IStripComparePatchSorter StripComparePatchSorter {
            get {
                return stripComparePatchSorter ?? (stripComparePatchSorter = createDefaultStripComparePatchSorter());
            }
            set {
                if (value == null)
                    throw new ArgumentNullException(nameof(value), "Sorter cannot be null");
                stripComparePatchSorter = value;
            }
        }

        /// <summary>
        /// TODO: Replace with DI container injected version.
        /// </summary>
        /// <returns><see cref="IStripComparePatchSorter"/> instance</returns>
        private IStripComparePatchSorter createDefaultStripComparePatchSorter() {
            var increasingSorter = new IncreasingStripComparePatchSorter();
            var topSideSorter = new ConfigBasedComparePatchSorter(increasingSorter, GlobalConfigKeys.TopSideSortingKey,
                                                                  Config.DefaultConfig);
            var bottomSideSorter = new ConfigBasedComparePatchSorter(increasingSorter,
                                                                     GlobalConfigKeys.BottomSideSortingKey,
                                                                     Config.DefaultConfig);
            var naSorter = new ConfigBasedComparePatchSorter(increasingSorter, GlobalConfigKeys.NaSideSortingKey,
                                                             Config.DefaultConfig);
            return new SheetSideDependentStripComparePatchSorter(topSideSorter, bottomSideSorter, naSorter);
        }

        /// <summary>
        /// Set a flag to signify that Beer's Law calculations should be skipped. The final calculated results will not have this information
        /// available if this has been set to <c>true</c>.
        /// </summary>
        public bool DisableChromaTrack { get; set; }

        private StripCompareCalculator([NotNull] ReferencePatchGrayFinders referencePatchGrayFinders,
                                       [NotNull] IToleranceSet toleranceSet,
                                       IMeasurementConditionsConfig measurementConditionsConfig, IJob job,
                                       IMeasurement measurement, IMachine machine,
                                       IEnumerable<IReferencePatch> referencePatches, IReferencePatch whiteReference,
                                       IEnumerable<IJobStripPatch> jobStripPatches) {
            DisableChromaTrack = false;
            this.referencePatchGrayFinders = referencePatchGrayFinders;
            this.job = job;
            this.measurement = measurement;
            this.machine = machine;
            this.toleranceSet = toleranceSet;
            this.measurementConditionsConfig = measurementConditionsConfig;
            // Required to calculate reference dotgain if no paperwhite is present in the color strip.
            this.whiteReference = whiteReference;
            this.referencePatches = referencePatches;
            this.jobStripPatches = new List<IJobStripPatch>(jobStripPatches);

            log = GlobalContainer.Current.Resolve<ILogger>();
        }

        public static async Task<StripCompareCalculator> CreateAsync([NotNull] ReferencePatchGrayFinders
                                                                         referencePatchGrayFinders,
                                                                     IToleranceSet toleranceSet,
                                                                     IMeasurementConditionsConfig
                                                                         measurementConditionsConfig,
                                                                     IJob job, IMeasurement measurement,
                                                                     IMachine machine,
                                                                     IEnumerable<IReferencePatch> referencePatches,
                                                                     IReferencePatch whiteReference,
                                                                     IEnumerable<IJobStripPatch> jobStripPatches) {
            return new StripCompareCalculator(referencePatchGrayFinders, toleranceSet, measurementConditionsConfig, job,
                                              measurement, machine, referencePatches, whiteReference, jobStripPatches) {
                scummingSample = await measurement.ScummingSample.GetAsync()
            };
        }

        public async Task<IStripCompareResult> Calculate(IReadOnlyList<ISample> samples,
                                                         IStripCompareScoreFactory stripCompareScoreFactory) {
            return await Calculate(samples, null, stripCompareScoreFactory);
        }

        /// <summary>
        /// Calculate compare data and return the results.
        /// </summary>
        /// <param name="samples">The samples to use, should always be set and have the same count as the jobStripPatches.</param>
        /// <param name="okSheetSamples">If non null, will be used as the ok sheet samples to use. Should have the same size as the jobStripPatches.</param>
        /// <param name="stripCompareScoreFactory">The score factory to use for scoring the result.</param>
        /// <returns>The result of the compare.</returns>
        public async Task<IStripCompareResult> Calculate([NotNull] IReadOnlyList<ISample> samples,
                                                         [CanBeNull] IReadOnlyList<ISample> okSheetSamples,
                                                         [NotNull] IStripCompareScoreFactory stripCompareScoreFactory) {
            if (samples == null) throw new ArgumentNullException(nameof(samples));
            if (stripCompareScoreFactory == null) throw new ArgumentNullException(nameof(stripCompareScoreFactory));

            log.Info("Updating compare results");

            // For efficiency, we require certain elements to be pre loaded.
            if (!job.PaperWhiteSample.IsLoaded)
                throw new InvalidOperationException("PaperWhiteSample of job has to be loaded!");

            if (samples.Count != jobStripPatches.Count) {
                var msg =
                    $"No result: Sample count does not match job strip patch count (Samples: {samples.Count}, JSP: {jobStripPatches.Count})";
                log.Warn(msg);
                throw new InvalidOperationException(
                    "Cannot calculate result as the sample count does not match the job strip patch count");
            }

            if (okSheetSamples != null && okSheetSamples.Count != samples.Count)
                throw new InvalidOperationException(
                    "okSheetSamples do not have the same amount of patches as the samples");

            // Image: filter unmeasured targets
            var filteredJobStripPatches = jobStripPatches.Where((jsp, i) => samples.ElementAt(i).Tag != -1).ToList();
            var filteredSamples = samples.Where(s => s.Tag != -1).ToList();
            var filteredOkSheetSamples = okSheetSamples?.Where((s, i) => samples.ElementAt(i).Tag != -1).ToList();

            SortedDictionary<int, IReadOnlyList<IStripComparePatch>> inkZones;

            // Image needs the unfiltered ones to include disabled targets in results images later on
            IReadOnlyList<IStripComparePatch> unfilteredRawPatches = null;
            if (job.JobType == (int)JobTypes.Image) {
                unfilteredRawPatches = generateRawPatches(jobStripPatches, samples, okSheetSamples,
                                                          out inkZones);
            }

            var filteredRawPatches = generateRawPatches(filteredJobStripPatches, filteredSamples,
                                                        filteredOkSheetSamples,
                                                        out inkZones);

            // NB: undefined and dud patches are not considered
            var summaryPatches = calculateSummaryPatches(toleranceSet, filteredRawPatches);

            var result = new StripCompareResult(job, measurement, toleranceSet, measurementConditionsConfig, machine,
                                                filteredOkSheetSamples, filteredRawPatches, unfilteredRawPatches,
                                                summaryPatches, stripCompareScoreFactory);

            result.G7AnalysisData = await collectG7AnalysisData(result);

            await calculateGrayBalancesForInkzones(result, inkZones);

            return result;
        }

        private IReadOnlyList<IStripComparePatch> generateRawPatches(
            [NotNull] IReadOnlyList<IJobStripPatch> filteredJobStripPatches,
            [NotNull] IReadOnlyList<ISample> filteredSamples,
            [CanBeNull] IReadOnlyList<ISample> filteredOkSheetSamples,
            out SortedDictionary<int, IReadOnlyList<IStripComparePatch>> inkZones) {
            updateJobStripPatchesForG7(filteredJobStripPatches, filteredSamples);
            var patches = createInitialComparePatches(filteredJobStripPatches, filteredSamples, filteredOkSheetSamples);
            // Image: assign indices in a special way knowing there are gaps in between them
            if (filteredJobStripPatches.Count != jobStripPatches.Count)
                patches.ForEach((p, i) => p.Index = jobStripPatches.IndexOf(filteredJobStripPatches[i]));

            initPaperWhites(patches, whiteReference);
            var lockSet = measurement.InkZoneLockSet ?? new InkZoneLockSet();
            initInkZoneLocks(lockSet, patches);
            initScumming(toleranceSet, patches, scummingSample);
            initCmyParents(patches);
            inkZones = initInkZones(patches);
            calculateResults(patches, filteredJobStripPatches.Count != jobStripPatches.Count);
            calculateTrappings(job.SequenceArray, patches);
            // Sort patches.
            ((SheetSideDependentStripComparePatchSorter)StripComparePatchSorter).SheetSide = job.SheetSide;

            return StripComparePatchSorter.Sort(patches).ToList();
        }

        /// <summary>
        /// When G7 is enabled, we want to use dynamic references. Here we consider appropriate
        /// patches and update them with the G7 results.
        /// </summary>
        /// <param name="jsPatches">
        /// The pathes of the jobstrip. Appropriate patches will be updated with a new reference that
        /// is based on the G7 calculations.
        /// </param>
        /// <param name="samples">The sample data to use in the G7 calculations.</param>
        private void updateJobStripPatchesForG7(IReadOnlyList<IJobStripPatch> jsPatches,
                                                IReadOnlyList<ISample> samples) {
            if (!toleranceSet.G7Calculated) {
                // Only update patches when G7 is being used.
                return;
            }

            Debug.Assert(jsPatches.Count == samples.Count, "jobStripPatches.Count == samples.Count");
            for (var i = 0; i < jsPatches.Count; i++) {
                var jsPatch = jsPatches[i];
                switch (jsPatch.PatchType) {
                    case PatchTypes.Dotgain:
                        // Check if the patch is a black tint and update the target if it is.
                        if (jsPatch.Cyan.NearlyEquals(0) &&
                            jsPatch.Magenta.NearlyEquals(0) &&
                            jsPatch.Yellow.NearlyEquals(0) &&
                            jsPatch.Key > 0 && jsPatch.Key < 100) {
                            updateJobStripPatchForG7BlackTint(jsPatch, jsPatches, samples);
                        }

                        break;
                    case PatchTypes.Overprint:
                        if (jsPatch.Cyan > 0 &&
                            jsPatch.Magenta > 0 &&
                            jsPatch.Yellow > 0) {
                            updateJobStripPatchForG7BalanceTint(i, jsPatch, jsPatches, samples);
                        }

                        break;
                    case PatchTypes.BalanceHighlight:
                    case PatchTypes.BalanceMidtone:
                    case PatchTypes.BalanceShadow:
                        updateJobStripPatchForG7BalanceTint(i, jsPatch, jsPatches, samples);
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Update jobstrippatch with new G7-derived references.
        /// </summary>
        private void updateJobStripPatchForG7BlackTint(IJobStripPatch jsPatch, IReadOnlyList<IJobStripPatch> jsPatches,
                                                       IReadOnlyList<ISample> samples) {
            var pwIndex = jsPatches.IndexOf(x => x.PatchType == PatchTypes.Paperwhite);
            var keyIndex = jsPatches.IndexOf(x => x.IsSolidKey() && x.InkZone == jsPatch.InkZone);
            if (pwIndex < 0 || keyIndex < 0) {
                return; // Nothing to do.
            }

            var pwSample = samples[pwIndex];
            var keySample = samples[keyIndex];
            var targetLab = G7Calculations.GetTargetLabBlack(jsPatch.Key, pwSample, keySample);
            jsPatch.OverrideLabTargetForG7(targetLab);
        }

        /// <summary>
        /// Update jobstrippatch with new G7-derived references.
        /// </summary>
        private void updateJobStripPatchForG7BalanceTint(int indexOfPatch, IJobStripPatch jsPatch,
                                                         IReadOnlyList<IJobStripPatch> jsPatches,
                                                         IReadOnlyList<ISample> samples) {
            var pwIndex = jsPatches.IndexOf(x => x.PatchType == PatchTypes.Paperwhite);
            if (!jsPatches.Any(x => x.IsCmyOverprint())) {
                return;
            }

            // Find the closest cmy overprint patch to the current patch (maybe not the most
            // efficient way to do this).
            var cmyIndex =
                jsPatches.Select((i, patch) => (i, patch))
                         .Where(x => x.Item1.IsCmyOverprint())
                         .OrderBy(x => Math.Abs(x.Item2 - indexOfPatch))
                         .Select(x => x.Item2)
                         .First(); // First here because we checked for the presence of an overprint patch above.

            if (pwIndex < 0 || cmyIndex < 0) {
                return; // Nothing to do.
            }

            var pwSample = samples[pwIndex];
            var cmySample = samples[cmyIndex];
            var targetLab = G7Calculations.GetTargetLabCmy(jsPatch.Cyan, pwSample, cmySample);
            jsPatch.OverrideLabTargetForG7(targetLab);
        }

        /// <summary>
        /// Create the basic compare result data for all patches.
        /// This basically creates all the StripComparePatches with their
        /// proper initial data set.
        /// </summary>
        private IReadOnlyList<IStripComparePatch> createInitialComparePatches(
            [NotNull] IReadOnlyList<IJobStripPatch> jobStripPatches,
            [NotNull] IReadOnlyList<ISample> samples,
            [CanBeNull] IReadOnlyList<ISample> okSheetSamples) {
            log.Info("Creating initial patch results");
            var numberOfSamples = samples.Count;
            var resultingPatches = new List<IStripComparePatch>();
            for (var i = 0; i < numberOfSamples; i++) {
                var comparePatch = createPatch(i, jobStripPatches, samples, okSheetSamples);
                // Setup some initial information to make some calculations down the road easier.
                // This info will also be calculated by the patches themselves, but that won't
                // be available yet when we need it below.
                comparePatch.Name = comparePatch.ReferencePatch.Name;
                comparePatch.PatchType = comparePatch.ReferencePatch.PatchType;
                comparePatch.InkZone = comparePatch.ReferencePatch.InkZone;
                comparePatch.Slot1 = comparePatch.ReferencePatch.Slot1;
                comparePatch.Slot2 = comparePatch.ReferencePatch.Slot2;
                comparePatch.Slot3 = comparePatch.ReferencePatch.Slot3;
                resultingPatches.Add(comparePatch);
            }

            return resultingPatches;
        }

        /// <summary>
        /// Find an appropriate paperwhite and set that for every patch in the compare set.
        /// </summary>
        private void initPaperWhites(IEnumerable<IStripComparePatch> patches, IReferencePatch paperWhiteReference) {
            var paperWhite = findOrSynthesizePaperWhite(patches, paperWhiteReference);
            foreach (var patch in patches) {
                patch.PaperWhite = paperWhite;
                // Use paperwhite to tell if the patch should be printed.
                patch.UpdateIsPrinted();
            }
        }

        private static void initInkZoneLocks(InkZoneLockSet set, IEnumerable<IStripComparePatch> patches) {
            foreach (var patch in patches) {
                patch.IsLocked = set.IsLocked(patch);
            }
        }

        /// <summary>
        /// Find cmy patches in the result and initialize each patch with its closest CMY patch
        /// (closest by inkzone).
        /// </summary>
        private static void initCmyParents(IReadOnlyList<IStripComparePatch> patches) {
            var cmyPatches =
                // Note that we can't trust the data in the actual patches yet to match this up as
                // this is not set until later, so we reference the reference patch here:
                patches.Where(p => p.ReferencePatch.PatchType == PatchTypes.Overprint &&
                                   p.ReferencePatch.IsThreeColorComponent())
                       .ToArray();
            foreach (var patch in patches) {
                var closestCmy =
                    cmyPatches.Any()
                        ? cmyPatches.MinBy(cmy => Math.Abs(cmy.InkZone - patch.InkZone))
                        : null;
                patch.SampleForClosestCmy = closestCmy?.Sample;
            }
        }

        private static SortedDictionary<int, IReadOnlyList<IStripComparePatch>> initInkZones(
            IReadOnlyList<IStripComparePatch> patches) {
            var inkZoneGroups = groupResultsIntoInkZones(patches);
            updateInkZones(inkZoneGroups);
            return inkZoneGroups;
        }

        /// <summary>
        /// Group the compare patches into their appropriate ink zones.
        /// </summary>
        private static SortedDictionary<int, IReadOnlyList<IStripComparePatch>> groupResultsIntoInkZones(
            IReadOnlyList<IStripComparePatch> patches) {
            var patchesByInkzone = new SortedDictionary<int, IReadOnlyList<IStripComparePatch>>();

            patches.GroupBy(p => p.InkZone).ForEach(group => patchesByInkzone[group.Key] = group.ToList());

            return patchesByInkzone;
        }

        /// <summary>
        /// Update inkzone information for all inkzones.
        /// </summary>
        private static void updateInkZones(SortedDictionary<int, IReadOnlyList<IStripComparePatch>> patchesByInkzone) {
            // For each inkzone.
            foreach (var keyValue in patchesByInkzone) {
                var fullColors = findSolids(keyValue.Value).ToArray();
                var maybeSolidK = fullColors.FirstOrDefault(p => p.ReferencePatch.IsKey() && p.ReferencePatch.PatchType == PatchTypes.Solid);
                foreach (var fullColorPatch in fullColors) {
                    foreach (var patchInInkzone in keyValue.Value) {
                        if (patchInInkzone.CanAcceptSolid(fullColorPatch)) {
                            patchInInkzone.Solid = fullColorPatch;
                        }

                        patchInInkzone.SampleForSolidKParentOfInkZone = maybeSolidK?.Sample;
                    }
                }
            }
        }

        private static IEnumerable<IStripComparePatch> findSolids(IEnumerable<IStripComparePatch> patches) {
            return patches.Where(p => p.IsSolid);
        }

        /// <summary>
        /// Create a new StripComparePatch for the combination of job strip patch, sample and
        /// Ok sheet sample(if available) for the given patch index.
        /// </summary>
        /// <param name="patchIndex">The index into all the relevant arrays.</param>
        /// <param name="jobStripPatches">The job strip patches.</param>
        /// <param name="samples">The samples.</param>
        /// <param name="okSheetSamples">The ok sheet samples.</param>
        /// <param name="toleranceSetG7Calculated"></param>
        /// <returns>A new StripComparePatch</returns>
        private static StripComparePatch createPatch(int patchIndex,
                                                     [NotNull] IReadOnlyList<IJobStripPatch> jobStripPatches,
                                                     [NotNull] IReadOnlyList<ISample> samples,
                                                     [CanBeNull] IReadOnlyList<ISample> okSheetSamples) {
            var jobStripPatch = jobStripPatches[patchIndex];
            var sample = samples.ElementAt(patchIndex);
            StripComparePatch comparePatch;
            if (okSheetSamples == null) {
                comparePatch = new StripComparePatch(jobStripPatch, sample);
            }
            else {
                var okSheetSample = okSheetSamples.ElementAt(patchIndex);
                var okSheetPatch = new OKSheetPatch(jobStripPatch, okSheetSample);
                comparePatch = new StripComparePatch(okSheetPatch, sample);
            }

            comparePatch.ReferencePatchId = jobStripPatch.ReferencePatchId;
            comparePatch.TargetX = jobStripPatch.TargetX;
            comparePatch.TargetY = jobStripPatch.TargetY;
            comparePatch.DeltaEToleranceOverride =
                jobStripPatch.DeltaEToleranceOverride.ToOption(_ => jobStripPatch.UseDeltaEToleranceOverride);

            return comparePatch;
        }

        /// <summary>
        /// Find a paperwhite patch in the compare set. If no paperwhite can be found,
        /// we try to see if the job has a paperwhite sample set. If this is also not present,
        /// we just return null.
        /// </summary>
        [CanBeNull]
        private IStripComparePatch findOrSynthesizePaperWhite([NotNull] IEnumerable<IStripComparePatch> patches,
                                                              [CanBeNull] IReferencePatch paperWhiteReference) {
            var paper = patches.FirstOrDefault(p => p.IsPaperWhite && !p.IsDud);
            if (paper != null)
                return paper;

            // None found, check job.
            if (job.HasSeparatePaperWhite) {
                Debug.Assert(job.PaperWhiteSample.IsLoaded);
                if (paperWhiteReference == null)
                    return null;

                // Generate a common reference that represents the paperwhite patch.
                var synthesizedJobStripPatch =
                    JobStripPatch.CreateFromReferencePatch(paperWhiteReference, null,
                                                           paperWhiteReference.MeasurementConditions, false);
                IStripComparePatch pw;
                // Check if we are currently calculating a result based on an OK sheet. If this is
                // the case, we wrap the generated paperwhite reference in an OKSheetPatch. This
                // should make sure that dotgains will be calculated correctly in the following situation:
                //
                // * An OK sheet is being used.
                // * There is no paperwhite patch in the colorbar being used.
                //
                // Also see PV-2271.
                //
                // Note: Probably could use a better check here.
                if (patches.FirstOrDefault()?.ReferencePatch is OKSheetPatch) {
                    pw = new StripComparePatch(
                        new OKSheetPatch(synthesizedJobStripPatch, job.PaperWhiteSample.Item),
                        job.PaperWhiteSample.Item
                    );
                }
                else {
                    pw = new StripComparePatch(synthesizedJobStripPatch, job.PaperWhiteSample.Item);
                }

                pw.InitResults(toleranceSet, measurementConditionsConfig, -1);
                return pw;
            }

            return null;
        }

        /// <summary>
        /// Go past patches and actually calculate the results for the currently given data.
        /// </summary>
        /// <param name="patches">The patches to calculate the results for</param>
        /// <param name="indicesAlreadyAssigned">Whether a patch's Index property is already set or not.
        /// (Patches for Image are already assigned before we get here)</param>
        private void calculateResults(IEnumerable<IStripComparePatch> patches, bool indicesAlreadyAssigned) {
            var index = 0;
            foreach (var patch in patches) {
                patch.InitResults(toleranceSet, measurementConditionsConfig,
                                  indicesAlreadyAssigned ? patch.Index : index);
                if (!DisableChromaTrack)
                    patch.InitChromaTrack(toleranceSet, measurementConditionsConfig);
                index++;
            }
        }

        private void calculateTrappings(int[] sequenceArray, IEnumerable<IStripComparePatch> patches) {
            var overprints = patches.Where(patch => patch.IsOverprint);
            foreach (var overprint in overprints) {
                calculateTrapping(sequenceArray, patches, overprint);
            }
        }

        private void calculateTrapping(int[] sequenceArray, IEnumerable<IStripComparePatch> patches,
                                       IStripComparePatch overprintPatch) {
            var slot1 = overprintPatch.Slot1;
            var slot2 = overprintPatch.Slot2;
            var solid1 = patches.FirstOrDefault(patch => patch.IsSolid && patch.SingleSlot == slot1);
            var solid2 = patches.FirstOrDefault(patch => patch.IsSolid && patch.SingleSlot == slot2);
            if (solid1 == null || solid2 == null)
                return;
            // Note that sequenceArray is in reverse order for checking the order of the inks.
            // So if we encounter slot2 first, this is actually the last ink. If we encounter
            // slot1 first then this is the last ink and we flip the solids around.
            foreach (var slot in sequenceArray) {
                if (slot == slot2)
                    // Order is OK.
                    break;
                if (slot == slot1) {
                    // Wrong order.
                    var temp = solid1;
                    solid1 = solid2;
                    solid2 = temp;
                    break;
                }
            }

            overprintPatch.Trapping = Trapping.Preucil(overprintPatch, solid1, solid2);
            overprintPatch.HasTrapping = true;
        }

        private static void initScumming(IToleranceSet toleranceSet, IEnumerable<IStripComparePatch> patches,
                                         ISample scummingSample) {
            foreach (var patch in patches) {
                if (patch.IsPaperWhite) {
                    initScumming(toleranceSet, patch, scummingSample);
                }
                else {
                    patch.HasScumming = false;
                }
            }
        }

        private static void initScumming(IToleranceSet toleranceSet, IStripComparePatch patch, ISample scummingSample) {
            Debug.Assert(patch.IsPaperWhite);
            if (!toleranceSet.ScummingCalculated || scummingSample == null) {
                patch.HasScumming = false;
                return;
            }

            patch.ScummingSample = scummingSample.AsLab;
        }

        private async Task<G7AnalysisDataCollection> collectG7AnalysisData(IStripCompareResult result) {
            await calculateGrayBalancesForPatches(result.SummaryPatches.Patches, result);

            return G7AnalysisDataCollection.CreateAnalysisDataCollection(result.SummaryPatches);
        }

        /// <summary>
        /// Calculates the gray balance results and update the patches accordingly.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="inkzones">The inkzones.</param>
        private async Task calculateGrayBalancesForInkzones(IStripCompareResult result,
                                                            SortedDictionary<int, IReadOnlyList<IStripComparePatch>>
                                                                inkzones) {
            foreach (var inkzone in inkzones)
                await calculateGrayBalancesForPatches(inkzone.Value, result);
        }

        private async Task calculateGrayBalancesForPatches(IEnumerable<IStripComparePatch> patches,
                                                           IStripCompareResult result) {
            if (referencePatches == null)
                return;
            // Find gray balance patches
            var grayPatches =
                patches.Where(p => p.IsBalanceShadow || p.IsBalanceMidtone || p.IsBalanceHighlight);
            foreach (var patch in grayPatches) {
                await CalculateGrayBalanceForPatch(referencePatchGrayFinders, patch, result);
                await CalculateGrayBalanceG7ForPatch(referencePatchGrayFinders, patch, result);
            }
        }

        public static async Task CalculateGrayBalanceForPatch([NotNull] ReferencePatchGrayFinders refPatchGrayFinders,
                                                              [NotNull] IStripComparePatch grayPatch,
                                                              [NotNull] IStripCompareResult result) {
            if (refPatchGrayFinders == null) throw new ArgumentNullException(nameof(refPatchGrayFinders));
            if (grayPatch == null) throw new ArgumentNullException(nameof(grayPatch));
            if (result == null) throw new ArgumentNullException(nameof(result));

            Lab sample = grayPatch.SampleLab;
            Lab reference = grayPatch.RefLab;
            if (result.Job.HasOkSheet && result.OKSheetSamples != null) {
                // Grab the sample which corresponds to our gray patch.
                if (grayPatch.Index < 0 || grayPatch.Index >= result.OKSheetSamples.Count())
                    throw new Exception("Could not grab proper balance sample from ok sheet samples.");
                var okSheetSample = result.OKSheetSamples.ElementAt(grayPatch.Index);
                // Subtract the difference between the OK sheet and the reference
                // This transforms the deviation from OK Sheet relative to Reference relative
                // Reference relative is needed for the GrayFinder to work properly
                sample -= (okSheetSample.AsLab - grayPatch.RefLab);
            }

            var diff = await refPatchGrayFinders.Lookup(grayPatch.ReferencePatchId, sample, reference);
            if (diff == null) {
                var machineType = await result.Machine.MachineType.GetAsync();
                if (machineType != null)
                    diff = DefaultGrayFinders.Lookup(machineType.TypeName,
                                                     new CMYK(grayPatch.Cyan, grayPatch.Magenta, grayPatch.Yellow, 0.0),
                                                     sample, reference);
            }

            grayPatch.GrayBalanceDifference = diff;
        }

        public static async Task CalculateGrayBalanceG7ForPatch([NotNull] ReferencePatchGrayFinders refPatchGrayFinders,
                                                                [NotNull] IStripComparePatch grayPatch,
                                                                [NotNull] IStripCompareResult result) {
            if (refPatchGrayFinders == null) throw new ArgumentNullException(nameof(refPatchGrayFinders));
            if (grayPatch == null) throw new ArgumentNullException(nameof(grayPatch));
            if (result == null) throw new ArgumentNullException(nameof(result));

            // Skip if this patch is not a G7 balance patch
            if (!((grayPatch.IsBalanceHighlight || grayPatch.IsBalanceMidtone || grayPatch.IsBalanceShadow ||
                   grayPatch.PatchType == PatchTypes.Balance || grayPatch.PatchType == PatchTypes.Other) &&
                  G7Calculations.IsPatchGrayBalance(grayPatch)))
                return;
            Lab sample = grayPatch.SampleLab;
            Lab reference = grayPatch.RefLab;
            var substratePatch = result.SummaryPatches.FirstPaperWhitePatch;
            var overprintPatch = result.SummaryPatches.Patches.FirstOrDefault(
                patch => (patch.PatchType == PatchTypes.Overprint && patch.Cyan == 100.0 &&
                          patch.Magenta == 100.0 && patch.Yellow == 100.0));
            // Skip if there is no substrate or overprint patch, as is required by G7
            if (substratePatch == null || overprintPatch == null)
                return;
            var diff = await refPatchGrayFinders.LookupG7(grayPatch.ReferencePatchId, sample, reference,
                                                          substratePatch.RefLab, substratePatch.SampleLab,
                                                          overprintPatch.SampleLab);
            if (diff == null) {
                var machineType = await result.Machine.MachineType.GetAsync();
                if (machineType != null)
                    diff = DefaultGrayFinders.LookupG7(machineType.TypeName,
                                                       new CMYK(grayPatch.Cyan, grayPatch.Magenta, grayPatch.Yellow,
                                                                0.0),
                                                       sample, reference, substratePatch.RefLab,
                                                       substratePatch.SampleLab, overprintPatch.SampleLab);
            }

            grayPatch.GrayBalanceDifferenceG7 = diff;
        }

        private static List<IStripComparePatch> calculateSummaryPatches(IToleranceSet toleranceSet,
                                                                        IEnumerable<IStripComparePatch> patches) {
            var groupedPatches = groupPatches(patches.Where(p => p.IsDefined && !p.IsLocked));
            var summaryPatches = new List<IStripComparePatch>();
            foreach (var patchGroupKeyValue in groupedPatches) {
                var firstPatch = patchGroupKeyValue.Value.First(); // Use first patch as general reference.
                var summaryPatch = StripComparePatch.CreateSummaryPatch(firstPatch.ReferencePatch,
                                                                        firstPatch.Sample,
                                                                        toleranceSet,
                                                                        patchGroupKeyValue.Value);
                summaryPatches.Add(summaryPatch);
            }

            return summaryPatches;
        }

        private static Dictionary<String, IImmutableList<IStripComparePatch>> groupPatches(
            IEnumerable<IStripComparePatch> patches) {
            var result = new Dictionary<string, IImmutableList<IStripComparePatch>>();
            foreach (var patch in patches) {
                var key = dividePatches(patch);
                if (!result.ContainsKey(key))
                    result[key] = ImmutableList<IStripComparePatch>.Empty;

                result[key] = result[key].Add(patch);
            }

            return result;
        }

        /// <summary>
        /// Kind of a misnomer maybe, but takes a patch and produces a key which can be used to
        /// uniquely identify the family the patch belongs to, this keeps into account the patch type and
        /// the slot values for process colors.
        /// </summary>
        /// <param name="patch">The patch to generate the identifier for.</param>
        /// <returns>An identifier to identify the group the patch belongs to.</returns>
        private static String dividePatches(IStripComparePatch patch) {
            switch (patch.PatchType) {
                case PatchTypes.Undefined:
                    return "undefined";
                case PatchTypes.Solid:
                    var solidSlot = patch.SingleSlot;
                    return "solid" + solidSlot;
                case PatchTypes.Dotgain:
                    var dotgainSlot = patch.SingleSlot;
                    return "dotgain" + dotgainSlot + ":" + patch.Tint;
                case PatchTypes.Slur:
                    throw new NotImplementedException();
                case PatchTypes.Balance:
                    return "balance";
                case PatchTypes.Paperwhite:
                    return "paperwhite";
                case PatchTypes.BalanceHighlight:
                    return "balancehighlight";
                case PatchTypes.BalanceMidtone:
                    return "balancemidtone";
                case PatchTypes.BalanceShadow:
                    return "balanceshadow";
                case PatchTypes.Overprint:
                    var slot1 = patch.Slot1;
                    var slot2 = patch.Slot2;
                    var slot3 = patch.Slot3;
                    return "overprint" + slot1 + ":" + slot2 + ":" + slot3;
                case PatchTypes.Other:
                    return "other_" + patch.Name;
                case PatchTypes.DotgainOnly:
                    var dotgainOnlySlot = patch.SingleSlot;
                    return "dotgain" + dotgainOnlySlot + ":" + patch.Tint;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}