﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using Castle.Core.Logging;

using Colorware.Core;
using Colorware.Core.Config;
using Colorware.Core.Data;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.PantoneLIVE;
using Colorware.Core.Data.Models.StripCompare;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;
using Colorware.Core.Helpers;
using Colorware.Models.GrayFinder;
using Colorware.Models.ServiceModels;

using JetBrains.Annotations;

namespace Colorware.Models.StripCompare {
    /// <summary>
    /// Helper class for handling the loading of <see cref="StripCompareResult" /> instances.
    /// This class caches some values and when loading a new result, will check if these values need to be reloaded.
    /// </summary>
    public class StripCompareLoader : IStripCompareLoader {
        private StripCompareLoaderOptions currentOptions = 0;

        public void SetOptions(StripCompareLoaderOptions newOptions) {
            currentOptions = newOptions;
        }

        private readonly ILogger log;

        private IStripCompareScoreFactory lastUsedStripCompareScoreFactory;

        private readonly IPantoneLiveFilteredStripCompareResultFactory pantoneLiveFilteredStripCompareResultFactory;
        private readonly ReferencePatchGrayFinders referencePatchGrayFinders;

        private int okSheetJobStripStartIndex;
        private int okSheetJobStripEndIndex;

        public IJob Job { get; private set; }
        public IMachine Machine { get; private set; }
        public IMeasurement Measurement { get; private set; }
        public IColorStrip ColorStrip { get; private set; }
        public IToleranceSet ToleranceSet { get; private set; }
        public IReferencePatch WhiteReference { get; private set; }
        public IReadOnlyList<IReferencePatch> ReferencePatches { get; private set; }
        public IReadOnlyList<IJobStripPatch> JobStripPatches { get; private set; }
        public IReadOnlyList<ISample> OKSheetSamples { get; private set; }

        public IReadOnlyList<IJobStripPatch> JobStripPatchesSliced {
            get {
                var startIndex = Measurement.JobStripStartIndex;
                var endIndex = Measurement.JobStripEndIndex;

                log.DebugFormat(
                    "JobStripPatchesSliced.get(): startIndex '{0}', endIndex '{1}', HasOkSheet '{2}', JobStripPatches.Count '{3}', okSheetJobStripStartIndex '{4}', okSheetJobStripEndIndex '{5}'",
                    startIndex, endIndex, HasOkSheet, JobStripPatches.Count, okSheetJobStripStartIndex,
                    okSheetJobStripEndIndex);

                if (startIndex == 0 && endIndex == 0) {
                    if (Job.MeasurementTypeId == MeasurementTypes.DigitalProof) {
                        return JobStripPatches; // Proofing always uses the complete color strip.
                    }

                    // Process control can use any slice of the color strip.
                    // Only first patch.
                    var first = new List<IJobStripPatch> {JobStripPatches.First()};
                    return first;
                }

                if (startIndex <= 0 && endIndex <= 0) {
                    // Marks to use the whole definition.
                    return JobStripPatches;
                }

                var patches = new List<IJobStripPatch>();
                var idx = 0;
                if (HasOkSheet) {
                    // If OK sheet is present, the job strip patches will have to be `unioned'
                    // with the samples of the OK sheet and the indexes of the current measurement.
                    startIndex = Math.Max(startIndex, okSheetJobStripStartIndex);
                    endIndex = Math.Min(endIndex, okSheetJobStripEndIndex);
                    if (startIndex > endIndex) {
                        return new List<IJobStripPatch>();
                        // Flip around.
                        //var temp = startIndex;
                        //startIndex = endIndex;
                        //endIndex = temp;
                    }
                }

                foreach (var patch in JobStripPatches) {
                    if (idx > endIndex)
                        break;
                    if (idx >= startIndex) {
                        patches.Add(patch);
                    }

                    idx++;
                }

                return patches;
            }
        }

        /// <summary>
        /// Combine the ok sheet samples and the measured samples to determine where they overlap.
        /// This overlap is then returned as the final slice.
        /// </summary>
        public IReadOnlyList<ISample> SlicedOkSheetSamples {
            get {
                if (OKSheetSamples == null)
                    return null;
                var measurementStartIndex = Measurement.JobStripStartIndex;
                var measurementEndIndex = Measurement.JobStripEndIndex;

                if (measurementStartIndex <= 0 && measurementEndIndex <= 0)
                    return OKSheetSamples; // Marks whole samples.

                var numberToDrop = measurementStartIndex - okSheetJobStripStartIndex;
                var numberToTake = Math.Min(okSheetJobStripEndIndex, measurementEndIndex) -
                                   Math.Max(okSheetJobStripStartIndex, measurementStartIndex) + 1;
                var collection = OKSheetSamples;
                if (numberToDrop <= 0 && numberToTake <= 0)
                    return new List<ISample>();
                if (numberToDrop > 0)
                    collection = collection.Skip(numberToDrop).ToList();
                if (numberToTake > 0)
                    collection = collection.Take(numberToTake).ToList();
                return collection;
            }
        }

        /// <summary>
        /// Determine what parts of the samples should be returned taking into account the ok sheet
        /// samples. These will be the samples that are shared with the OK sheet.
        /// </summary>
        /// <param name="samples"></param>
        /// <returns></returns>
        private IReadOnlyList<ISample> getSlicedSamples(IReadOnlyList<ISample> samples) {
            log.DebugFormat(
                "StripCompareLoader.getSlicedSamples(): Measurement.JobStripStartIndex '{0}', Measurement.JobStripEndIndex '{1}', HasOkSheet '{2}', okSheetJobStripEndIndex '{3}', okSheetJobStripStartIndex '{4}'",
                Measurement.JobStripStartIndex, Measurement.JobStripEndIndex, HasOkSheet, okSheetJobStripEndIndex,
                okSheetJobStripStartIndex);

            if (!HasOkSheet)
                return samples;
            var measurementStartIndex = Measurement.JobStripStartIndex;
            var measurementEndIndex = Measurement.JobStripEndIndex;

            if (measurementStartIndex <= 0 && measurementEndIndex <= 0)
                return samples; // Marks whole sample set.

            var numberToDrop = okSheetJobStripStartIndex - measurementStartIndex;
            var numberToTake = Math.Min(measurementEndIndex, okSheetJobStripEndIndex) -
                               Math.Max(measurementStartIndex, okSheetJobStripStartIndex) + 1;
            var collection = samples;
            if (numberToDrop <= 0 && numberToTake <= 0)
                return new List<ISample>();
            if (numberToDrop > 0)
                collection = collection.Skip(numberToDrop).ToList();
            if (numberToTake > 0)
                collection = collection.Take(numberToTake).ToList();

            return collection;
        }

        public bool HasOkSheet => Job.HasOkSheet && Measurement.Id > Job.OkSheetId;

        #region Events
        public event StripCompareLoaderErrorHandler Error;

        private void invokeError(StripCompareLoaderErrorArgs args) {
            Error?.Invoke(this, args);
        }
        #endregion

        public IProgressLoader Progress { get; private set; }

        public StripCompareLoader() {
            pantoneLiveFilteredStripCompareResultFactory =
                GlobalContainer.Current.Resolve<IPantoneLiveFilteredStripCompareResultFactory>();
            log = GlobalContainer.Current.Resolve<ILogger>();
            referencePatchGrayFinders = GlobalContainer.Current.Resolve<ReferencePatchGrayFinders>();
        }

        public void UpdateJobStripPatches(IReadOnlyList<IJobStripPatch> newJobStripPatches) {
            // Update cache.
            JobStripPatches = newJobStripPatches;
        }

        public async Task<IStripCompareCalculator> LoadAsync([NotNull] IJob job, IMeasurement measurement) {
            if (job == null) throw new ArgumentNullException(nameof(job));

            if (job.JobStripId <= 0) {
                invokeError(new StripCompareLoaderErrorArgs(
                                "Job has no associated job strip, something probably went wrong while saving the job."));
                return null;
            }

            var toleranceSetId = job.ToleranceSetId;
            if (toleranceSetId <= 0) {
                invokeError(new StripCompareLoaderErrorArgs(
                                "Job has no associated tolerance set, something probably went wrong while saving the job."));
                return null;
            }

            log.Debug("Starting actual data loading");
            await loadDataAsync(job, measurement);
            if (ToleranceSet == null) {
                invokeError(
                    new StripCompareLoaderErrorArgs("Could not load tolerance set with id: " + toleranceSetId));
                return null;
            }

            log.Debug("Data loaded, calculating results");
            var mc = await Job.MeasurementCondition.GetAsync();
            var calculator =
                await StripCompareCalculator.CreateAsync(referencePatchGrayFinders, ToleranceSet, mc, job, Measurement,
                                                         Machine, ReferencePatches,
                                                         WhiteReference, JobStripPatchesSliced);

            if (currentOptions.HasFlag(StripCompareLoaderOptions.NoChromaTrack) ||
                !GlobalConfigKeys.ChromaTrackTrajectoryEnabled)
                calculator.DisableChromaTrack = true;

            return calculator;
        }

        /// <summary>
        /// Recalculate current result. Note, that all results MUST be loaded already.
        /// </summary>
        public async Task<IStripCompareResult> RecalculateAsync() {
            var calculator =
                await StripCompareCalculator.CreateAsync(referencePatchGrayFinders, ToleranceSet,
                                                         Job.MeasurementCondition.Item, Job, Measurement,
                                                         Machine, ReferencePatches, WhiteReference,
                                                         JobStripPatchesSliced);
            var incomingSamples = await Measurement.Samples.GetAsync();
            ToleranceSet.InProduction = Measurement.IsProductionMode;

            // Check location of OK sheet to determine if we should use the OKSheet samples for this measurement.
            IReadOnlyList<ISample> okSheetSamplesToUse = null;
            if (HasOkSheet)
                okSheetSamplesToUse = SlicedOkSheetSamples; // OKSheetSamples;

            try {
                var slicedIncomingSamples = getSlicedSamples(incomingSamples);
                var result = await calculator.Calculate(slicedIncomingSamples, okSheetSamplesToUse,
                                                        lastUsedStripCompareScoreFactory);

                // Filter unauthorized PantoneLIVE colors 
                return await pantoneLiveFilteredStripCompareResultFactory.Create(result).Initialization;
            }
            catch (Exception e) {
                invokeError(new StripCompareLoaderErrorArgs(e.Message));
                return null;
            }
        }

        /// <summary>
        /// Recalculate current result with new set of job strip patches.
        /// </summary>
        /// <param name="newJobStripPatches"></param>
        public Task<IStripCompareResult> RecalculateAsync(IReadOnlyList<IJobStripPatch> newJobStripPatches) {
            // Update cache.
            JobStripPatches = newJobStripPatches;
            return RecalculateAsync();
        }

        public Task<IStripCompareResult> CalculateAsync([NotNull] IJob job, [NotNull] IMeasurement measurement) {
            if (job == null) throw new ArgumentNullException(nameof(job));
            if (measurement == null) throw new ArgumentNullException(nameof(measurement));

            return CalculateAsync(job, measurement, job.GetCompareScoreFactory());
        }

        public Task<IStripCompareResult> CalculateAsync([NotNull] IJob job, [NotNull] IMeasurement measurement,
                                                        IStripCompareScoreFactory stripCompareScoreFactory) {
            if (job == null) throw new ArgumentNullException(nameof(job));
            if (measurement == null) throw new ArgumentNullException(nameof(measurement));

            return CalculateAsync(job, measurement, null, stripCompareScoreFactory);
        }

        public async Task<IStripCompareResult> CalculateAsync([NotNull] IJob job, [NotNull] IMeasurement measurement,
                                                              IReadOnlyList<ISample> samples,
                                                              IStripCompareScoreFactory stripCompareScoreFactory) {
            if (job == null) throw new ArgumentNullException(nameof(job));
            if (measurement == null) throw new ArgumentNullException(nameof(measurement));

            log.Debug($"Calculate: Loading data. job: {job.Id}, measurement: {measurement.Id}, name: {job.Name}");

            Task<IReadOnlyList<ISample>> samplesTask;
            if (samples == null) {
                if (measurement.IsDeleted) {
                    // The original query may not have returned the samples if they have been
                    // deleted. In this case we try to get the deleted samples. Note that this should
                    // be OK as long as we have no way of adding additional samples to an existing
                    // measurement (as is currently the case). If this ever does become possible,
                    // this call may result in too many samples being returned.
                    //
                    // Note: This change corresponds to PV-2240.
                    var query = new Query().Eq("measurement_id", measurement.Id).IncludeDeleted();
                    samplesTask = BaseModel.FindAllAsync<ISample>(query);
                }
                else {
                    samplesTask = measurement.Samples.GetAsync();
                }
            }
            else {
                samplesTask = Task.FromResult(samples);
            }

            lastUsedStripCompareScoreFactory = stripCompareScoreFactory;

            var stripCompareCalculatorTask = LoadAsync(job, measurement);

            await Task.WhenAll(samplesTask, stripCompareCalculatorTask);

            if (stripCompareCalculatorTask.Result == null)
                return null;

            log.Debug("Data loaded");

            var result = await calculateWithSamples(stripCompareCalculatorTask.Result, measurement, samplesTask.Result);

            if (result == null)
                return null;

            // Filter unauthorized PantoneLIVE colors 
            return await pantoneLiveFilteredStripCompareResultFactory.Create(result).Initialization;
        }

        [ItemCanBeNull]
        private async Task<IStripCompareResult> calculateWithSamples(IStripCompareCalculator stripCompareCalculator,
                                                                     IMeasurement measurement,
                                                                     IReadOnlyList<ISample> samples) {
            ToleranceSet.InProduction = measurement.IsProductionMode;

            // Check location of OK sheet to determine if we should use the OKSheet samples for this measurement.
            IReadOnlyList<ISample> okSheetSamplesToUse = null;
            if (HasOkSheet)
                okSheetSamplesToUse = SlicedOkSheetSamples; //  OKSheetSamples;

            try {
                Debug.Assert(lastUsedStripCompareScoreFactory != null);
                var slicedSamples = getSlicedSamples(samples);
                var result = await stripCompareCalculator.Calculate(slicedSamples, okSheetSamplesToUse,
                                                                    lastUsedStripCompareScoreFactory);
                return result;
            }
            catch (Exception e) {
                invokeError(new StripCompareLoaderErrorArgs(e.Message));
                return null;
            }
        }

        private async Task loadDataAsync([NotNull] IJob job, IMeasurement measurement) {
            if (job == null) throw new ArgumentNullException(nameof(job));

            log.Debug("Loading main data");
            // The job parameter `job' is the new (incoming) job,
            // which will be compared to the existing job property named `Job'.
            Progress = new ProgressLoader();
            // Progress.Reset();
            Progress.Begin(); // Guard.

            // Load OK sheet samples
            Task okSheetSamplesTask = Task.CompletedTask;
            if (job.HasOkSheet) {
                if (Job == null || job.OkSheetId != Job.OkSheetId || OKSheetSamples == null) {
                    // Load ok sheet samples.
                    okSheetSamplesTask = loadOkSheetPatchesAsync(job);
                }
            }
            else {
                OKSheetSamples = null;
            }

            // Load paperwhite
            var paperWhiteTask = job.PaperWhiteSample.GetAsync();

            // Load measurement conditions
            var measurementConditionTask = loadMeasurementConditionAsync(job);

            // Load reference info
            var refPatchesTask = loadReferencePatchesAsync(job);

            // Load color strip
            var colorStripTask = loadColorStripAsync(job);

            // Load job strip patches
            var jspTask = loadJobStripPatchesAsync(job);

            // Load tolerance set.
            var toleranceSetTask = loadToleranceSetAsync(job);

            // Load machine
            var machineTask = loadMachineAsync(job);

            // Load opacity results
            var opacityResultsTask = measurement.OpacityResults.GetAsync(true);

            await
                Task.WhenAll(paperWhiteTask, measurementConditionTask, refPatchesTask, colorStripTask,
                             toleranceSetTask, machineTask, okSheetSamplesTask, jspTask, opacityResultsTask);

            Job = job;
            Measurement = measurement;

            Progress.End(); // Guard end.
        }

        private async Task loadColorStripAsync(IJob job) {
            var colorStrip = await job.ColorStrip.GetAsync();

            if (colorStrip == null) {
                log.Warn("Job without colorstrip condition found, using default. Job.Id = " + job.Id);
                ColorStrip = new ColorStrip {Name = "Job has no associated color bar"};
            }
            else {
                ColorStrip = colorStrip;
            }
        }

        private async Task loadMeasurementConditionAsync(IJob job) {
            if (job.MeasurementConditionId <= 0) {
                log.Warn("Job without measurement condition found, using default. Job.Id = " + job.Id);
                job.MeasurementCondition.MockItem =
                    MeasurementConditionsConfig.CreateDefault("Job has no measurement conditions!");
            }
            else {
                var measurementCondition = await job.MeasurementCondition.GetAsync();

                // May happen with old jobs.
                if (measurementCondition == null)
                    job.MeasurementCondition.MockItem = MeasurementConditionsConfig.CreateDefault("Default condition");
            }
        }

        private async Task loadToleranceSetAsync(IJob job) {
            ToleranceSet = await job.ToleranceSet.GetAsync();
        }

        private async Task loadMachineAsync(IJob job) {
            var machine = await job.Machine.GetAsync();

            // Machine may have been deleted.
            if (machine == null) {
                log.Warn($"Job without machine condition found, using default. Job.Id = {job.Id}");
                var mockMachine = new Machine {
                    Name = "Job has no machine associated",
                    NumberOfColors = 4,
                    NumberOfInkzones = 34,
                    NewInstance = false
                };
                job.Machine.MockItem = mockMachine;
                machine = mockMachine;
            }

            Machine = machine;
        }

        private async Task loadOkSheetPatchesAsync(IJob job) {
            var m = (await BaseModel.FindAllAsync<IMeasurement>(new Query().Eq("id", job.OkSheetId))).FirstOrDefault();

            // Ok sheet missing?
            // TODO: this is just a hack for when the OK sheet is somehow missing, and should have
            // already been dealt with when it was deleted
            if (m == null) {
                // Get job measurements.
                var jobMeasurements = await job.Measurements.GetAsync();
                var okSheetMeasurement = jobMeasurements.FirstOrDefault(m2 => m2.OkSheet);
                if (okSheetMeasurement == null) {
                    // No Measurement marked as OK sheet found, reset to nothing.
                    job.OkSheetId = 0;
                }
                else {
                    // Measurement with OK sheet found, reset to this measurement.
                    job.OkSheetId = okSheetMeasurement.Id;
                }

                // Save job, recalculate cached results and reload.
                await job.SaveAsync();

                await job.RecalculateAllCachedResultsAsync(null);
                if (okSheetMeasurement != null) {
                    // Retry reloading with newly set OK sheet.
                    await loadOkSheetPatchesAsync(job);
                }

                // No OK sheet. Load without OK sheet patches.
                return;
            }

            okSheetJobStripStartIndex = m.JobStripStartIndex;
            okSheetJobStripEndIndex = m.JobStripEndIndex;

            OKSheetSamples = await m.Samples.GetAsync();
        }

        private async Task loadJobStripPatchesAsync(IJob job) {
            var jobStrip = await job.JobStrip.GetAsync();
            JobStripPatches = await jobStrip.JobStripPatches.GetAsync(true);
        }

        private async Task loadReferencePatchesAsync(IJob job) {
            // Load the current set of reference patches from the connected job reference if there is any.
            var reference = await job.Reference.GetAsync();
            if (reference == null) {
                ReferencePatches = new List<IReferencePatch>();
            }
            else {
                ReferencePatches = await reference.ReferencePatches.GetAsync();
            }

            // Try to get the paper reference from the substrate first.
            var paper = await job.Paper.GetAsync();
            if (paper == null) {
                // Try to get a backup paper from the current job reference.
                WhiteReference = ReferencePatches.FirstOrDefault(p => p.IsPaperwhite);
            }
            else {
                WhiteReference = await paper.PaperwhiteReferencePatch.GetAsync();
            }
        }

        public void Clear() {
            Job = null;
            Machine = null;
            ReferencePatches = null;
            JobStripPatches = null;
            OKSheetSamples = null;
        }
    }
}