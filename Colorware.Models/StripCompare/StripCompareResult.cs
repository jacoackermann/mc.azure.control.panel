using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.G7Analysis;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring;
using Colorware.Models.StripCompareInterface;

using JetBrains.Annotations;

namespace Colorware.Models.StripCompare {
    /// <summary>
    /// Represents the results of comparing a job strip patch to a measurement.
    /// Use a <see cref="StripCompareCalculator"/> to produce this result.
    /// </summary>
    public class StripCompareResult : IStripCompareResult {
        #region Properties
        public IJob Job { get; }
        public IMeasurement Measurement { get; }
        public IToleranceSet ToleranceSet { get; }
        public IMeasurementConditionsConfig MeasurementConditionsConfig { get; }
        public IMachine Machine { get; }
        public IEnumerable<ISample> OKSheetSamples { get; }

        /// <summary>
        /// Complete list of raw patches.
        /// </summary>
        public IStripComparePatches RawPatches { get; }

        /// <summary>
        /// Gets the unfiltered raw patches (currently only used for Image to access disabled targets for
        /// display on the result screen Image).
        /// </summary>
        public IStripComparePatches UnfilteredRawPatches { get; }

        /// <summary>
        /// Complete list of summary patches.
        /// </summary>
        public IStripComparePatches SummaryPatches { get; }
        #endregion

        #region Query information
        public int NumberOfInkzones { get; }

        public int EndInkZone { get; }

        public List<IStripCompareComponentInkZones> ComponentInkzones { get; }

        public bool HasInkzones => NumberOfInkzones > 1;

        /// <summary>
        /// Return true if the strip contains a paperwhite or if the job has an associated paperwhite measurement patch.
        /// </summary>
        public bool HasDotgains => Job.HasSeparatePaperWhite || RawPatches.HasPaperwhite;

        /// <summary>
        /// Provides a summary for each patch type.
        /// </summary>
        public StripCompareSummary CompareSummary { get; }

        /// <summary>
        /// Not unlike CompareSummary, but provides scoring features to calculate an overall score
        /// for this compare.
        /// </summary>
        public IStripCompareScore CompareScore { get; }

        /// <summary>
        /// Contains data for drawing G7 analysis curves.
        /// </summary>
        public G7AnalysisDataCollection G7AnalysisData { get; set; }
        #endregion

        #region Calculations
        private int calculateNumberOfInkzones() {
            int found = 0;
            int current = -1;
            foreach (var patch in RawPatches.Patches) {
                if (patch.InkZone != current) {
                    found++;
                    current = patch.InkZone;
                }
            }
            return found;
        }


        private int calculateEndInkZone() {
            if (RawPatches == null)
                throw new NullReferenceException("RawPatches is null");
            if (RawPatches.Patches == null)
                throw new NullReferenceException("RawPatches.Patches is null");
            if (Machine == null)
                throw new NullReferenceException("Machine is null, cannot determine number of inkzones");

            var maxZone = RawPatches.Patches.Any() ? RawPatches.Patches.Max(p => p.InkZone) : 0;
            return Math.Max(maxZone, Machine.NumberOfInkzones);
        }

        private List<IStripCompareComponentInkZones> calculateComponentInkZones() {
            var compsBySlot = new Dictionary<int, StripCompareComponentInkZones>();
            foreach (var patch in RawPatches.Patches) {
                if (!patch.IsSolid)
                    continue;
                if (!patch.IsSingleSlot)
                    continue;
                var slot = patch.SingleSlot;
                if (!compsBySlot.ContainsKey(slot))
                    compsBySlot[slot] = new StripCompareComponentInkZones(slot, Job.ShouldUseOkSheet(Measurement))
                    {Percentage = 100};
                var inkzones = compsBySlot[slot];
                inkzones.Patches.Add(patch);
            }
            var result = new List<IStripCompareComponentInkZones>();
            foreach (var inkZone in compsBySlot) {
                var item = inkZone.Value;
                item.PadInkZones(EndInkZone); // Always start at first inkzone for now.
                result.Add(item);
            }
            sortResultOnColorSequence(result);
            initSequenceNumbers(result);
            return result;
        }

        private void initSequenceNumbers(List<IStripCompareComponentInkZones> zones) {
            var sequence = Job.SequenceArray;
            foreach (var zone in zones) {
                // Cast to access setter.
                ((StripCompareComponentInkZones)zone).SequenceNumber = sequence.Length -
                                                                       Array.IndexOf(sequence, zone.Slot);
            }
        }

        private void sortResultOnColorSequence(List<IStripCompareComponentInkZones> zones) {
            var sequence = Job.SequenceArray;
            zones.Sort(
                (item1, item2) =>
                Array.IndexOf(sequence, item2.Slot).CompareTo(Array.IndexOf(sequence, item1.Slot)));
        }

        public List<IStripCompareComponentInkZones> CalculateDotgainInkZones(int slot) {
            var compsByPercentage = new Dictionary<int, IStripCompareComponentInkZones>();
            foreach (var patch in RawPatches.Patches) {
                if (!patch.IsDotgain)
                    continue;
                if (!patch.IsSingleSlot)
                    continue;
                if (patch.SingleSlot != slot)
                    continue;
                if (!compsByPercentage.ContainsKey(patch.Tint))
                    compsByPercentage[patch.Tint] = new StripCompareComponentInkZones(slot,
                                                                                      Job.ShouldUseOkSheet(Measurement)) {
                                                                                          Percentage = patch.Tint
                                                                                      };
                compsByPercentage[patch.Tint].Patches.Add(patch);
            }
            var result = new List<IStripCompareComponentInkZones>();
            foreach (var inkZone in compsByPercentage) {
                var item = inkZone.Value;
                item.PadInkZones(EndInkZone);
                result.Add(item);
            }
            return result;
        }

        public IStripCompareComponentInkZones CalculateInkZonesForFilteredPatches(
            Func<IStripComparePatch, bool> patchFilter) {
            var patches = RawPatches.Patches.Where(patchFilter);
            var result = new StripCompareComponentInkZones(-1, Job.ShouldUseOkSheet(Measurement));
            result.Patches.AddRange(patches);
            result.PadInkZones(EndInkZone);
            return result;
        }
        #endregion

        public StripCompareResult([NotNull] IJob job,
                                  [NotNull] IMeasurement measurement,
                                  [NotNull] IToleranceSet toleranceSet,
                                  [NotNull] IMeasurementConditionsConfig measurementConditionsConfig,
                                  [NotNull] IMachine machine,
                                  [CanBeNull] IEnumerable<ISample> okSheetSamples,
                                  [NotNull] IReadOnlyList<IStripComparePatch> rawPatches,
                                  [CanBeNull] IReadOnlyList<IStripComparePatch> unfilteredRawPatches,
                                  [NotNull] List<IStripComparePatch> summaryPatches,
                                  [NotNull] IStripCompareScoreFactory stripCompareScoreFactory) {
            if (job == null) throw new ArgumentNullException(nameof(job));
            if (measurement == null) throw new ArgumentNullException(nameof(measurement));
            if (toleranceSet == null) throw new ArgumentNullException(nameof(toleranceSet));
            if (measurementConditionsConfig == null) throw new ArgumentNullException(nameof(measurementConditionsConfig));
            if (machine == null) throw new ArgumentNullException(nameof(machine));
            if (rawPatches == null) throw new ArgumentNullException(nameof(rawPatches));
            if (stripCompareScoreFactory == null) throw new ArgumentNullException(nameof(stripCompareScoreFactory));
            if (summaryPatches == null) throw new ArgumentNullException(nameof(summaryPatches));

            Job = job;
            Measurement = measurement;
            ToleranceSet = toleranceSet;
            MeasurementConditionsConfig = measurementConditionsConfig;
            Machine = machine;
            OKSheetSamples = okSheetSamples;
            RawPatches = new StripComparePatches(rawPatches);
            EndInkZone = calculateEndInkZone();
            NumberOfInkzones = calculateNumberOfInkzones();
            ComponentInkzones = calculateComponentInkZones();
            SummaryPatches = new StripComparePatches(summaryPatches);

            // Is null for non-Image jobs
            if (unfilteredRawPatches != null)
                UnfilteredRawPatches = new StripComparePatches(unfilteredRawPatches);

            CompareSummary = new StripCompareSummary(this);
            CompareScore = stripCompareScoreFactory.Create(this);
        }
    }
}