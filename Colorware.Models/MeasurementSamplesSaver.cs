﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Helpers;

using JetBrains.Annotations;

namespace Colorware.Models {
    [UsedImplicitly]
    public class MeasurementSamplesSaver : IMeasurementSamplesSaver {
        #region Properties
        private IProgressLoader progress;

        public IProgressLoader Progress {
            get { return progress ?? (progress = new ProgressLoader()); }
            set { progress = value; }
        }
        #endregion

        public async Task<IMeasurement> SaveAsync(IMeasurement measurement, IEnumerable<ISample> samples) {
            if (!measurement.NewInstance)
                throw new ArgumentException("Passed measurement is already saved");

            await measurement.SaveAsync();

            await saveSamplesAsync(measurement, samples);

            Progress.End();
            return measurement;
        }

        private async Task saveSamplesAsync(IMeasurement result, IEnumerable<ISample> samples) {
            Debug.Assert(result.Id > 0 && !result.NewInstance);
            var sampleList = samples.ToList();

            foreach (var sample in sampleList)
                sample.MeasurementId = result.Id;

            await BaseModel.SaveAllAsync(sampleList);
        }
    }
}