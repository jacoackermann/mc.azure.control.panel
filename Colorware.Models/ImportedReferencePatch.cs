﻿using Colorware.Core.Data.Models;

using JetBrains.Annotations;

namespace Colorware.Models {
    public class ImportedReferencePatch {
        [NotNull]
        public IReferencePatch ReferencePatch { get; }

        [CanBeNull]
        public IGrayFinderPatch GrayFinderPatch { get; }

        public ImportedReferencePatch([NotNull] IReferencePatch referencePatch,
                                      [CanBeNull] IGrayFinderPatch grayFinderPatch) {
            ReferencePatch = referencePatch;
            GrayFinderPatch = grayFinderPatch;
        }

        public ImportedReferencePatch([NotNull] IReferencePatch referencePatch) {
            ReferencePatch = referencePatch;
        }
    }
}