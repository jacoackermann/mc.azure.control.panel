﻿using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Data.Models;
using Colorware.Core.Mvvm;
using Colorware.Models.ColorStripMapper;

using JetBrains.Annotations;

namespace Colorware.Models {
    public class DefaultDotgainListSelection : DefaultNotifyPropertyChanged {
        [NotNull]
        public ColorStripMapping Mapping { get; private set; }

        [NotNull]
        public IReadOnlyList<IDefaultDotgainList> DefaultDotgainLists { get; private set; }

        [CanBeNull]
        private IDefaultDotgainList selectedDefaultDotgainList;

        [CanBeNull]
        public IDefaultDotgainList SelectedDefaultDotgainList {
            get { return selectedDefaultDotgainList; }
            set {
                selectedDefaultDotgainList = value;
                OnPropertyChanged("SelectedDefaultDotgainList");
            }
        }

        public IDefaultDotgainList SetSelectedDefaultDotgainList(long defaultDotgainListId) {
            return SelectedDefaultDotgainList = DefaultDotgainLists.FirstOrDefault(l => l.Id == defaultDotgainListId);
        }

        public DefaultDotgainListSelection([NotNull] ColorStripMapping mapping,
            [NotNull] IReadOnlyList<IDefaultDotgainList> defaultDotgainLists, long selectedDefaultDotgainListId) {
            if (mapping == null) throw new ArgumentNullException("mapping");
            if (defaultDotgainLists == null) throw new ArgumentNullException("defaultDotgainLists");

            Mapping = mapping;
            DefaultDotgainLists = defaultDotgainLists;
            SelectedDefaultDotgainList = DefaultDotgainLists.FirstOrDefault(l => l.Id == selectedDefaultDotgainListId);
        }
    }
}