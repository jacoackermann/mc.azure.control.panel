﻿using System;

namespace Colorware.Core {
    public static class GlobalConfigKeys {
        /// <summary>
        /// Enable/disable trajectory display.
        /// </summary>
        public const string ChromaTrackTrajectoryEnabledConfigKey = "Defaults.ChromaTrack.Enabled";

        public static bool ChromaTrackTrajectoryEnabled {
            get { return Config.Config.DefaultConfig.GetBool(ChromaTrackTrajectoryEnabledConfigKey, true); }
            set { Config.Config.DefaultConfig.SetBool(ChromaTrackTrajectoryEnabledConfigKey, value); }
        }

        /// <summary>
        /// Number of subdivisions to calculate for the trajectory.
        /// </summary>
        public const string ChromaTrackTrajectorySubdivisionsConfigKey = "Defaults.ChromaTrack.SubDivisions";

        public static int ChromaTrackTrajectorySubdivisions {
            get { return Config.Config.DefaultConfig.GetInt(ChromaTrackTrajectorySubdivisionsConfigKey, 3); }
            set { Config.Config.DefaultConfig.SetInt(ChromaTrackTrajectorySubdivisionsConfigKey, value); }
        }

        /// <summary>
        /// KFactor to extend to.
        /// </summary>
        public const string ChromaTrackTrajectoryExtendsConfigKey = "Defaults.ChromaTrack.Extends";

        public static double ChromaTrackTrajectoryExtends {
            get { return Config.Config.DefaultConfig.GetDouble(ChromaTrackTrajectoryExtendsConfigKey, 0.15); }
            set { Config.Config.DefaultConfig.SetDouble(ChromaTrackTrajectoryExtendsConfigKey, value); }
        }

        public const string ServerHostConfigKey = "Options.Server.Host";

        public static string ServerHost {
            get { return Config.Config.DefaultConfig.Get(ServerHostConfigKey, "http://localhost:3000"); }
            set { Config.Config.DefaultConfig.Set(ServerHostConfigKey, value); }
        }

        public const String DefaultAlignmentKey = "Defaults.ProcessControl.ColorbarAlignment";
        public const string TopSideSortingKey = "Options.InkZoneSorting.Top";
        public const string BottomSideSortingKey = "Options.InkZoneSorting.Bottom";
        public const string NaSideSortingKey = "Options.InkZoneSorting.Na";
    }
}