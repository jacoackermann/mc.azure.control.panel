﻿using System.Collections.ObjectModel;

using Colorware.Core.Data.Models.StripCompareInterface;

namespace Colorware.Models {
    public class FlexoLane {
        public string Name { get; set; }

        public ObservableCollection<IStripComparePatch> FocusPatches { get; set; }

        // Dotgain patches nearest 50%
        public ObservableCollection<IStripComparePatch> DotgainPatches { get; set; }
    }
}