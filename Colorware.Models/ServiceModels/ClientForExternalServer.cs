﻿using Colorware.Core.Data.Models;

namespace Colorware.Models.ServiceModels {
    public class ClientForExternalServer : IClientForExternalServer {
        public long ClientServerId { get; set; }
        public bool Active { get; set; }
        public long ClientId { get; set; }
        public string ClientName { get; set; }
        public long ServerId { get; set; }
        public string ServerName { get; set; }
        public long Servers { get; set; }
        public PqmSendMethod SendMethod { get; set; }
    }
}