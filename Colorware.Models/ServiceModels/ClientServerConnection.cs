﻿using Colorware.Core.Data.Models;

namespace Colorware.Models.ServiceModels {
    public class ClientServerConnection : IClientServerConnection {
        public long ServerId { get; set; }
        public long ClientId { get; set; }
        public long SendMethod { get; set; }
    }
}