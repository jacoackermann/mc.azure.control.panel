﻿using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [ServiceModel(ListingName = "user-group", ServiceName = "user_groups")]
    public class UserGroup : BaseModel, IUserGroup {
        #region Serialized fields
        [ServiceField("name")]
        public String Name { get; set; }

        [ServiceField("description")]
        public String Description { get; set; }

        [ServiceField("admin")]
        public Boolean Admin { get; set; }
        #endregion

        public UserGroup() {
            Name = "NA";
            Admin = false;
        }
    }
}