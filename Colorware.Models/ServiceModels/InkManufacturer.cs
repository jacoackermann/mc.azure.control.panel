﻿using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [ServiceModel(ListingName = "ink-manufacturer", ServiceName = "ink_manufacturers")]
    public class InkManufacturer : BaseModel, IInkManufacturer {
        #region Serialized fields
        [ServiceField("name")]
        public string Name { get; set; }
        #endregion
    }
}