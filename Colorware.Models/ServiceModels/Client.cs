﻿using System;
using System.Diagnostics;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("clients_access")]
    [ServiceModel(ListingName = "client", ServiceName = "clients")]
    [DebuggerDisplay("Client({Id}, {Name}})")]
    public class Client : BaseModel, IClient {
        [ServiceField("external_guid")]
        public Guid ExternalGuid { get; set; } = Guid.NewGuid();

        [ServiceField("name")]
        public String Name { get; set; }

        [ServiceField("notes")]
        public String Notes { get; set; }

        [ServiceField("custom_field_name_1")]
        public String CustomFieldName1 { get; set; }

        [ServiceField("custom_field_name_2")]
        public String CustomFieldName2 { get; set; }

        [ServiceField("custom_field_name_3")]
        public String CustomFieldName3 { get; set; }

        [ServiceField("custom_field_name_4")]
        public String CustomFieldName4 { get; set; }

        [ServiceField("custom_field_name_5")]
        public String CustomFieldName5 { get; set; }

        [ServiceField("custom_field_name_6")]
        public String CustomFieldName6 { get; set; }

        public bool HasCustomFieldName1 {
            get { return !String.IsNullOrEmpty(CustomFieldName1); }
        }

        public bool HasCustomFieldName2 {
            get { return !String.IsNullOrEmpty(CustomFieldName2); }
        }

        public bool HasCustomFieldName3 {
            get { return !String.IsNullOrEmpty(CustomFieldName3); }
        }

        public bool HasCustomFieldName4 {
            get { return !String.IsNullOrEmpty(CustomFieldName4); }
        }

        public bool HasCustomFieldName5 {
            get { return !String.IsNullOrEmpty(CustomFieldName5); }
        }

        public bool HasCustomFieldName6 {
            get { return !String.IsNullOrEmpty(CustomFieldName6); }
        }

        public bool HasCustomFields {
            get {
                return
                    HasCustomFieldName1 ||
                    HasCustomFieldName2 ||
                    HasCustomFieldName3 ||
                    HasCustomFieldName4 ||
                    HasCustomFieldName5 ||
                    HasCustomFieldName6;
            }
        }

        #region Relations
        private IHasManyCollection<IJob> jobs;

        public IHasManyCollection<IJob> Jobs {
            get { return jobs ?? (jobs = new HasManyCollection<IJob>("client_id", this)); }
        }
        #endregion

        public static IClient CreateDefaultClient() {
            var c = new Client {Name = "-"};
            return c;
        }

        public static Client CreateMockClient() {
            return new Client {
                Name = "Client name",
                Notes = "Notes go here"
            };
        }
    }
}