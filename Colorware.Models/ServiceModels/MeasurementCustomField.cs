﻿using Colorware.Core.Data.Models;
using Colorware.Core.Mvvm;

namespace Colorware.Models.ServiceModels {
    public abstract class MeasurementCustomField : DefaultNotifyPropertyChanged, IMeasurementCustomField {
        public int FieldIndex { get; private set; }
        public string Name { get; set; }

        public abstract string Value { get; set; }

        public MeasurementCustomFieldType TypeOfField { get; set; }
        public bool IsMultiline { get; set; }
        public bool IsDirty { get; private set; }

        public MeasurementCustomField(int fieldIndex, string name, string value, MeasurementCustomFieldType typeOfField) {
            TypeOfField = typeOfField;
            FieldIndex = fieldIndex;
            Name = name;
            Value = value;
        }

        public abstract void Clear();
    }
}