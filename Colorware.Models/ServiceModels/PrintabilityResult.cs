﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Colorware.Core.Data;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.Printability;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Data.Specification;
using Colorware.Models.StripCompare;

namespace Colorware.Models.ServiceModels {
    [ServiceModel(ServiceName = "printability_results", ListingName = "printability-result", LogDelete = false)]
    public class PrintabilityResult : BaseModel, IPrintabilityResult {
        #region Service fields
        [ServiceField("name")]
        public string Name { get; set; }

        [ServiceField("reference_patch_id")]
        public long ReferencePatchId { get; set; }

        [ServiceField("ink_set_id")]
        public long InkSetId { get; set; }

        [ServiceField("job_id")]
        public long JobId { get; set; }

        [ServiceField("measurement_id")]
        public long MeasurementId { get; set; }

        [ServiceField("job_name")]
        public string JobName { get; set; }

        [ServiceField("job_number")]
        public string JobNumber { get; set; }

        [ServiceField("measurement_date")]
        public DateTime MeasurementDate { get; set; }

        [ServiceField("chosen_density")]
        public double ChosenDensity { get; set; }

        [ServiceField("delta_e")]
        public double DeltaE { get; set; }

        [ServiceField("is_wet")]
        public bool IsWet { get; set; }

        [ServiceField("created_at")]
        public DateTime CreatedAt { get; set; }

        [ServiceField("updated_at")]
        public DateTime UpdatedAt { get; set; }
        #endregion

        #region Relations
        private IBelongsTo<IReferencePatch> referencePatch;

        public IBelongsTo<IReferencePatch> ReferencePatch {
            get {
                if (referencePatch == null)
                    referencePatch = new BelongsTo<IReferencePatch>("ReferencePatchId", this);
                return referencePatch;
            }
        }

        private IBelongsTo<IInkSet> inkSet;

        public IBelongsTo<IInkSet> InkSet {
            get {
                if (inkSet == null)
                    inkSet = new BelongsTo<IInkSet>("InkSetId", this);
                return inkSet;
            }
        }

        private IBelongsTo<IJob> job;

        public IBelongsTo<IJob> Job {
            get {
                if (job == null)
                    job = new BelongsTo<IJob>("JobId", this);
                return job;
            }
        }

        private IBelongsTo<IMeasurement> measurement;

        public IBelongsTo<IMeasurement> Measurement {
            get {
                if (measurement == null)
                    measurement = new BelongsTo<IMeasurement>("MeasurementId", this);
                return measurement;
            }
        }
        #endregion

        /// <summary>
        /// Get the printability result related to the given compareresult and given solid.
        /// </summary>
        /// <param name="compareResult">The compare result to look for. The job and measurement are retrieved from this.</param>
        /// <param name="solid">The solid to look for, the reference patch id is retrieved from this.</param>
        /// <param name="action">The action to call when the result returns.</param>
        public static void Get(IStripCompareResult compareResult, IStripComparePatch solid,
            Action<PrintabilityResult> action) {
            var query = new Query()
                .Eq("reference_patch_id", solid.ReferencePatchId)
                .Eq("job_id", compareResult.Job.Id)
                .Eq("measurement_id", compareResult.Measurement.Id);

            Task.Run(async () => {
                var result = await FindAllAsync<PrintabilityResult>(query);
                var item = result.FirstOrDefault();
                action(item);
            });
        }

        /// <summary>
        /// Load all printability curves belonging to this printability result.
        /// </summary>
        public async Task<PrintabilityResultCalculator> LoadPrintabilityResultsCalculatorAsync() {
            var jobTask = FindIncludingDeletedAsync<Job>(JobId);
            var measurementTask = FindIncludingDeletedAsync<Measurement>(MeasurementId);

            await Task.WhenAll(jobTask, measurementTask);

            if (jobTask.Result == null || measurementTask.Result == null)
                return null;

            var loader = new StripCompareLoader();
            var result = await loader.CalculateAsync(jobTask.Result, measurementTask.Result);
            return new PrintabilityResultCalculator(result);
        }

        /// <summary>
        /// Load curve belonging to this result.
        /// </summary>
        public async Task<PrintabilityCurve> LoadCurveAsync() {
            var printabilityResult = await LoadPrintabilityResultsCalculatorAsync();

            return printabilityResult == null ? null : printabilityResult.GetCurveForReferenceId(ReferencePatchId);
        }


        /// <summary>
        /// Create a new 'empty' printability result which can be used to represent 'null' items in selection lists.
        /// </summary>
        /// <param name="name">The name to use.</param>
        /// <returns>A new <see cref="PrintabilityResult"/></returns>
        public static PrintabilityResult CreateEmpty(string name) {
            var empty = new PrintabilityResult {Name = name, JobName = name};
            return empty;
        }
    }
}