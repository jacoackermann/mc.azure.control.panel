﻿using System;
using System.IO;

using Colorware.Core.Color;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

using JetBrains.Annotations;

using LCMSBinding;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("grayfinder_patches_access")]
    [ServiceModel(ServiceName = "grayfinder_patches", TableName = "grayfinder_patches", ListingName = "gray-finder-patch",
        DisplayName = "CW.Data.GrayFinderPatch")]
    public class GrayFinderPatch : BaseModel, IGrayFinderPatch {
        #region Fields
        [ServiceField("data")]
        public byte[] Data { get; set; }
        #endregion

        #region Relations
        [ServiceField("reference_patch_id")]
        public long ReferencePatchId { get; set; }
        #endregion


        /// <summary>
        /// Default constructor, not to be used manually
        /// </summary>
        public GrayFinderPatch() {}

        /// <summary>
        /// Constructs a GrayFinderPatch using an ICC transform and reference patch to store in the database.
        /// </summary>
        /// <param name="transform">The ICC transform to use for construction</param>
        /// <param name="patch">The reference patch for which to store the GrayFinder, containing the GrayFinder's CMY tone</param>
        public GrayFinderPatch([NotNull] IccTransform transform, [NotNull] IReferencePatch patch) {
            if (transform == null) throw new ArgumentNullException(nameof(transform));
            if (patch == null) throw new ArgumentNullException(nameof(patch));

            var newGrayFinder = new GrayFinder.GrayFinder(transform, new CMYK(patch.Cyan, patch.Magenta, patch.Yellow, 0));
            var dataStream = new MemoryStream();
            newGrayFinder.Serialize(dataStream);
            Data = dataStream.ToArray();
            ReferencePatchId = patch.Id;
        }


        private GrayFinder.GrayFinder grayFinder;

        /// <summary>
        /// Performs a lookup of a given CIE L*a*b* value, returning the CMY difference.
        /// If not yet called, this will internally allocate a GrayFinder, which can be deallocated with Clear().
        /// </summary>
        /// <param name="color">The absolute L*a*b* for which to perform the lookup</param>
        /// <returns>The CMY difference from the reference used to construct the GrayFinder</returns>
        public CMYK Lookup(Lab color, Lab reference) {
            if (grayFinder == null) {
                var dataStream = new MemoryStream(Data, false);
                grayFinder = new GrayFinder.GrayFinder(dataStream);
            }
            return grayFinder?.Lookup(color, reference);
        }

        /// <summary>
        /// Frees up memory by deallocating the internally stored GrayFinder.
        /// </summary>
        public void Clear() {
            grayFinder = null;
        }
    }
}
