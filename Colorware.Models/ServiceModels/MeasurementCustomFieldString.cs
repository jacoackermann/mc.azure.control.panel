﻿using Colorware.Core.Data.Models;

namespace Colorware.Models.ServiceModels {
    public class MeasurementCustomFieldString : MeasurementCustomField {
        private string value;

        public MeasurementCustomFieldString(int fieldIndex, string name, string value,
            MeasurementCustomFieldType typeOfField)
            : base(fieldIndex, name, value, typeOfField) {
        }

        public override string Value {
            get { return value; }
            set {
                this.value = value;
                OnPropertyChanged("Value");
            }
        }

        public override void Clear() {
            value = string.Empty;
            OnPropertyChanged("Value");
        }
    }
}