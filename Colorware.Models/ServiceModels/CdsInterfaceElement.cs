﻿using System.Diagnostics;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

namespace Colorware.Models.ServiceModels {
    [ServiceModel(ServiceName = "cds_interface_elements", ListingName = "cds_interface_element",
        DisplayName = "CW.Data.CdsInterfaceElement")]
    [DebuggerDisplay("CdsInterfaceElement(Id={Id}, FamilyId={FamilyId}, Name={Name}, Tint={Tint}, PatchType={PatchType})")]
    public class CdsInterfaceElement : BaseModel, ICdsInterfaceElement {
        #region Service fields
        
        

        [ServiceField("job_id")]
        public long JobId { get; set; }

        [ServiceField("family_id")]
        public int FamilyId { get; set; }

        [ServiceField("name")]
        public string Name { get; set; }

        [ServiceField("tint")]
        public int Tint { get; set; }

        [ServiceField("description")]
        public string Description { get; set; }

        [ServiceField("patch_type")]
        public PatchTypes PatchType { get; set; }

        [ServiceField("attribute_type")]
        public CdsAttributeTypes AttributeType { get; set; }
        #endregion

        #region relations
        private IBelongsTo<IJob> job;

        public IBelongsTo<IJob> Job {
            get { return job ?? (job = new BelongsTo<IJob>("JobId", this)); }
        }
        #endregion

        public bool IsDotgain {
            get { return PatchType == PatchTypes.Dotgain; }
        }

        public bool IsDotgainOnly {
            get { return PatchType == PatchTypes.DotgainOnly; }
        }

        public bool IsRegistration {
            get { return AttributeType == CdsAttributeTypes.Registration; }
        }

        public bool IsVisual {
            get { return AttributeType == CdsAttributeTypes.Visual; }
        }

        /// <summary>
        /// Can currently only display regular values in the result overview. Visual and registration cannot be displayed.
        /// This property can be used to check or filter for elements that can be displayed.
        /// </summary>
        public bool HasDisplayableAttributeType {
            get { return AttributeType != CdsAttributeTypes.Visual && AttributeType != CdsAttributeTypes.Registration; }
        }

        public override string ToString() {
            return string.Format("CdsInterfaceElement(Id={0}, {1}, {2}, {3}, {4})", Id, Name, PatchType, AttributeType,
                Tint);
        }
    }
}