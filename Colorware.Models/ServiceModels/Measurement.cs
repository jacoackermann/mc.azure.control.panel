﻿using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("measurements_access", CanUpdateOwned = true, CanCreate = true, CanDeleteOwned = true)]
    [ServiceModel(ListingName = "measurement", ServiceName = "measurements")]
    public class Measurement : BaseModel, IMeasurement {

        #region Fields
        [ServiceField("external_guid")]
        public Guid ExternalGuid { get; set; } = Guid.NewGuid();

        [ServiceField("name")]
        public String Name { get; set; }

        /// <summary>
        /// Currently used as a remarks field for each measurement.
        /// </summary>
        [ServiceField("content")]
        public String Content { get; set; }

        public bool HasRemark => !string.IsNullOrEmpty(Content);

        [ServiceField("wet")]
        public bool IsWet { get; set; }

        [ServiceField("ok_sheet")]
        public bool OkSheet { get; set; }

        [ServiceField("is_production_mode")]
        public bool IsProductionMode { get; set; }

        [ServiceField("flow_change")]
        public int FlowChange { get; set; }

        /// <summary>
        /// Returns a status text based on the current FlowChange.
        /// FlowChange &lt; 0  ==&gt; Make ready
        /// FlowChange &gt; 0  ==&gt; Production
        /// FlowChange == 0  ==&gt; Ok Sheet
        /// Obviously, this should not be used for in-code status checks!
        /// </summary>
        public String StatusText {
            get {
                if (FlowChange < 0)
                    return "Make ready";
                if (FlowChange > 0)
                    return "Production";
                return "Ok Sheet";
            }
        }

        [ServiceField("created_at")]
        public DateTime CreatedAt { get; set; }

        [ServiceField("measurement_type_id")]
        public MeasurementTypes MeasurementType { get; set; }

        [ServiceField("job_strip_start_index")]
        public int JobStripStartIndex { get; set; }

        [ServiceField("job_strip_end_index")]
        public int JobStripEndIndex { get; set; }

        /// <summary>
        /// The identifier of the roll.
        /// </summary>
        [ServiceField("roll_id", CanBeNull = true, DefaultValue = "")]
        public String RollId { get; set; }

        /// <summary>
        /// Roll number.
        /// </summary>
        [ServiceField("flexo_roll", CanBeNull = true, DefaultValue = "")]
        public string FlexoRoll { get; set; }

        /// <summary>
        /// Panel identifier.
        /// </summary>
        [ServiceField("flexo_panel", CanBeNull = true, DefaultValue = "")]
        public String FlexoPanel { get; set; }

        [ServiceField("is_flexo")]
        public bool IsFlexo { get; set; }

        /// <summary>
        /// Has an audit been saved locally?
        /// </summary>
        [ServiceField("is_audited")]
        public bool IsAudited { get; set; }

        /// <summary>
        /// Has an audit been submited to CDS?
        /// </summary>
        [ServiceField("is_transmitted")]
        public bool IsTransmitted { get; set; }

        [ServiceField("has_cached_score")]
        public bool HasCachedScore { get; set; }

        [ServiceField("cached_score")]
        public double CachedScore { get; set; }

        [ServiceField("ink_zone_locks")]
        public String SerializedInkZoneLocks { get; set; }

        private InkZoneLockSet inkZoneLockSet;

        public InkZoneLockSet InkZoneLockSet {
            get {
                if (String.IsNullOrEmpty(SerializedInkZoneLocks))
                    return null;
                return inkZoneLockSet ?? (inkZoneLockSet = InkZoneLockDeserializer.Deserialize(SerializedInkZoneLocks));
            }
            set {
                if (value == null) {
                    SerializedInkZoneLocks = String.Empty;
                    return;
                }
                SerializedInkZoneLocks = InkZoneLockSerializer.Serialize(value);
                inkZoneLockSet = value;
            }
        }

        [ServiceField("has_custom_data")]
        public bool HasCustomData { get; set; }

        [ServiceField("custom_string_field_1")]
        public string CustomStringField1 { get; set; }

        [ServiceField("custom_string_field_2")]
        public string CustomStringField2 { get; set; }

        [ServiceField("custom_string_field_3")]
        public string CustomStringField3 { get; set; }

        [ServiceField("custom_string_field_4")]
        public string CustomStringField4 { get; set; }

        [ServiceField("custom_string_field_5")]
        public string CustomStringField5 { get; set; }

        [ServiceField("custom_float_field_1")]
        public double CustomFloatField1 { get; set; }

        [ServiceField("custom_float_field_2")]
        public double CustomFloatField2 { get; set; }

        [ServiceField("custom_float_field_3")]
        public double CustomFloatField3 { get; set; }

        [ServiceField("custom_float_field_4")]
        public double CustomFloatField4 { get; set; }

        [ServiceField("custom_float_field_5")]
        public double CustomFloatField5 { get; set; }

        [ServiceField(("last_pqr_status"))]
        public LastPqrStatus LastPqrStatus { get; set; }
        #endregion

        #region Relations
        [ServiceField("reference_id")]
        public long ReferenceId { get; set; }

        private IBelongsTo<IReference> reference;

        [Related("reference_id", OnDeleteAction.Nothing)]
        public IBelongsTo<IReference> Reference {
            get { return reference ?? (reference = new BelongsTo<IReference>("ReferenceId", this)); }
            set { reference = value; }
        }

        [ServiceField("job_id")]
        public long JobId { get; set; }

        private IBelongsTo<IJob> job;

        [Related("job_id")]
        public IBelongsTo<IJob> Job {
            get { return job ?? (job = new BelongsTo<IJob>("JobId", this)); }
            set { job = value; }
        }

        [ServiceField("job_strip_id")]
        public long JobStripId { get; set; }

        private IBelongsTo<IJobStrip> jobStrip;

        public IBelongsTo<IJobStrip> JobStrip {
            get { return jobStrip ?? (jobStrip = new BelongsTo<IJobStrip>("JobStripId", this)); }
            set { jobStrip = value; }
        }

        private HasManyCollection<IEpqScore> epqScores;

        public IHasManyCollection<IEpqScore> EpqScores
            => epqScores ?? (epqScores = new HasManyCollection<IEpqScore>("measurement_id", this));

        private IHasManyCollection<ICachedCompareResult> cachedCompareResults;

        [Related("measurement_id", OnDeleteAction.DeleteRelated)]
        public IHasManyCollection<ICachedCompareResult> CachedCompareResults {
            get {
                return cachedCompareResults ??
                       (cachedCompareResults = new HasManyCollection<ICachedCompareResult>("measurement_id", this));
            }
            set { cachedCompareResults = value; }
        }

        private HasManyCollection<ISample> samples;

        [Related("measurement_id", OnDeleteAction.DeleteRelated)]
        public IHasManyCollection<ISample> Samples
            => samples ?? (samples = new HasManyCollection<ISample>("measurement_id", this));

        [ServiceField("scumming_sample_id")]
        public long ScummingSampleId { get; set; }

        private BelongsTo<ISample> scummingSample;

        [Related("scumming_sample_id", OnDeleteAction.DeleteRelated)]
        public IBelongsTo<ISample> ScummingSample
            => scummingSample ?? (scummingSample = new BelongsTo<ISample>("ScummingSampleId", this));

        /// <summary>
        /// User that created the measurement.
        /// </summary>
        [ServiceField("user_id")]
        public long UserId { get; set; }

        private BelongsTo<IDatabaseUser> user;

        [Related("user_id")]
        public IBelongsTo<IDatabaseUser> User => user ?? (user = new BelongsTo<IDatabaseUser>("UserId", this));

        private IHasManyCollection<IOpacityResult> opacityResults;

        [Related("measurement_id", OnDeleteAction.DeleteRelated)]
        public IHasManyCollection<IOpacityResult> OpacityResults {
            get {
                return opacityResults ??
                       (opacityResults = new HasManyCollection<IOpacityResult>("measurement_id", this));
            }
            set { opacityResults = value; }
        }
        #endregion

        public String OneLineFormatContent {
            get {
                if (!string.IsNullOrEmpty(Content))
                    return Content.Trim().Replace("\r\n", " ").Replace("\n", " ").Replace("\t", " ");
                else
                    return "";
            }
        }

        public Measurement() {
            CustomFloatField1 = 0;
            CustomFloatField2 = 0;
            CustomFloatField3 = 0;
            CustomFloatField4 = 0;
            CustomFloatField5 = 0;
            CustomStringField1 = string.Empty;
            CustomStringField2 = string.Empty;
            CustomStringField3 = string.Empty;
            CustomStringField4 = string.Empty;
            CustomStringField5 = string.Empty;
        }

        public static Measurement CreateForJob(IJob job) {
            return CreateForJob(job, MeasurementTypes.Production);
        }

        public static Measurement CreateForJob(IJob job, MeasurementTypes measurementType) {
            var m = new Measurement {
                JobId = job.Id,
                FlowChange = -1,
                CreatedAt = DateTime.Now,
                JobStripId = job.JobStripId,
                Name = job.Name,
                IsProductionMode = job.IsProductionMode,
                MeasurementType = measurementType
            };
            return m;
        }

        public static Measurement CreateMockMeasurement() {
            return new Measurement {
                CreatedAt = DateTime.Now,
                FlowChange = -1,
                IsWet = true,
                Name = "Mock measurement",
                Content = "Remarks go here",
                MeasurementType = MeasurementTypes.Production
            };
        }

        public override string ToString() {
            return $"Measurement({Id}, {CreatedAt}, {Content})";
        }
    }
}