﻿using System;
using System.Diagnostics;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("job_ink_infos_access")]
    [ServiceModel(ServiceName = "job_ink_infos", ListingName = "job_ink_info")]
    [DebuggerDisplay("JobInkInfo(Id={Id}, JobId={JobId}, Slot={Slot}, Name={Name})")]
    public class JobInkInfo : BaseModel, IJobInkInfo {

        #region Fields
        [ServiceField("slot")]
        public int Slot { get; set; }

        [ServiceField("name")]
        public String Name { get; set; }

        [ServiceField("note")]
        public String Note { get; set; }
        #endregion

        #region Relations
        [ServiceField("job_id")]
        public long JobId { get; set; }

        private IBelongsTo<IJob> job;

        [Related("job_id")]
        public IBelongsTo<IJob> Job {
            get { return job ?? (job = new BelongsTo<IJob>("JobId", this)); }
            set { job = value; }
        }
        #endregion
    }
}