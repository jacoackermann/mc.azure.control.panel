using System.Diagnostics;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("default_dotgain_entries_access")]
    [DebuggerDisplay("DefaultDotgainEntry(Id={Id}, DefaultDotgainListId={DefaultDotgainListId}, Percentage={Percentage}, Dotgain={Dotgain})")]
    [ServiceModel(ListingName = "default-dotgain-entry", ServiceName = "default_dotgain_entries")]
    public class DefaultDotgainEntry : BaseModel, IDefaultDotgainEntry {
        #region Fields
        
        

        [ServiceField("default_dotgain_list_id")]
        public long DefaultDotgainListId { get; set; }

        [ServiceField("percentage")]
        public int Percentage { get; set; }

        [ServiceField("dotgain")]
        public double Dotgain { get; set; }
        #endregion

        #region Relations
        private BelongsTo<IDefaultDotgainList> dotgainList;

        public IBelongsTo<IDefaultDotgainList> DotgainList {
            get {
                return dotgainList ?? (dotgainList = new BelongsTo<IDefaultDotgainList>("DefaultDotgainListId", this));
            }
        }
        #endregion
    }
}