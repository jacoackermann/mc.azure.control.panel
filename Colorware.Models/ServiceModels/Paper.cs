﻿using System;
using System.Diagnostics;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

using ReactiveUI;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("papers_access")]
    [ServiceModel(ListingName = "paper", ServiceName = "papers")]
    [DebuggerDisplay("Paper({Id}, {Name})")]
    public class Paper : BaseModel, IPaper {
        [ServiceField("external_guid")]
        public Guid ExternalGuid { get; set; } = Guid.NewGuid();

        private string name;

        [ServiceField("name")]
        public string Name {
            get { return name; }
            set { this.RaiseAndSetIfChanged(ref name, value); }
        }

        [ServiceField("weight")]
        public int Weight { get; set; }

        #region Relations
        [ServiceField("company_id")]
        public long CompanyId { get; set; }

        private IBelongsTo<ICompany> company;

        [Related("company_id")]
        public IBelongsTo<ICompany> Company {
            get { return company ?? (company = new BelongsTo<ICompany>("CompanyId", this)); }
        }

        [ServiceField("paper_type_id")]
        public long PaperTypeId { get; set; }

        private IBelongsTo<IPaperType> paperType;

        [Related("paper_type_id")]
        public IBelongsTo<IPaperType> PaperType {
            get { return paperType ?? (paperType = new BelongsTo<IPaperType>("PaperTypeId", this)); }
        }

        [ServiceField("paper_manufacturer_id")]
        public long PaperManufacturerId { get; set; }

        private BelongsTo<IPaperManufacturer> paperManufacturer;

        [Related("paper_manufacturer_id")]
        public IBelongsTo<IPaperManufacturer> PaperManufacturer {
            get {
                return paperManufacturer ??
                       (paperManufacturer = new BelongsTo<IPaperManufacturer>("PaperManufacturerId", this));
            }
        }

        [ServiceField("paperwhite_reference_patch_id")]
        public long PaperwhiteReferencePatchId { get; set; }

        private IBelongsTo<IReferencePatch> paperwhiteReferencePatch;

        [Related("paperwhite_reference_patch_id", OnDeleteAction.Nothing)]
        public IBelongsTo<IReferencePatch> PaperwhiteReferencePatch {
            get {
                return paperwhiteReferencePatch ??
                       (paperwhiteReferencePatch = new BelongsTo<IReferencePatch>("PaperwhiteReferencePatchId", this));
            }
        }
        #endregion
    }
}