﻿using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("database_users_access")]
    [ServiceModel(ListingName = "database-user", ServiceName = "database_users")]
    public class DatabaseUser : BaseModel, IDatabaseUser {
        #region Serialized fields
        [ServiceField("external_guid")]
        public Guid ExternalGuid { get; set; } = Guid.NewGuid();

        [ServiceField("user_group_id")]
        public long UserGroupId { get; set; }

        [ServiceField("company_id")]
        public long CompanyId { get; set; }

        [ServiceField("name")]
        public string Name { get; set; }

        [ServiceField("username")]
        public string Username { get; set; }

        [ServiceField("password")]
        public string Password { get; set; }

        [ServiceField("forced_license_product")]
        public Guid ForcedLicenseProduct { get; set; }
        #endregion

        #region Relations
        private IBelongsTo<IUserGroup> userGroup;

        [Related("user_group_id")]
        public IBelongsTo<IUserGroup> UserGroup {
            get { return userGroup ?? (userGroup = new BelongsTo<IUserGroup>("UserGroupId", this)); }
        }

        private BelongsTo<ICompany> company;

        [Related("company_id")]
        public IBelongsTo<ICompany> Company {
            get { return company ?? (company = new BelongsTo<ICompany>("CompanyId", this)); }
        }
        #endregion

        #region Derived Properties
        /// <summary>
        /// Name to use for display purposes. Only the Name field for now.
        /// </summary>
        public string DisplayName {
            get { return Name ?? "-"; }
        }
        #endregion
    }
}