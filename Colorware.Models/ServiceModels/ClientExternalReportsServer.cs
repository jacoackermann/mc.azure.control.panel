﻿using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("client_external_reports_servers_access")]
    [ServiceModel(ListingName = "client_external_reports_server", ServiceName = "client_external_reports_servers")]
    public class ClientExternalReportsServer : BaseModel, IClientExternalReportsServer {
        [ServiceField("client_id")]
        public long ClientId { get; set; }

        [Related("client_id")]
        public IBelongsTo<IClient> Client { get; private set; }

        [ServiceField("external_reports_server_id")]
        public long ExternalReportsServerId { get; set; }

        [Related("external_reports_server_id")]
        public IBelongsTo<IExternalReportsServer> ExternalReportsServer { get; private set; }

        [ServiceField("active")]
        public bool Active { get; set; }

        [ServiceField("send_method")]
        public PqmSendMethod SendMethod { get; set; }
    }
}