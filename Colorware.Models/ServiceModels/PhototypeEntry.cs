﻿using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    /// TODO: Reijer (2016-09-19): it seems to me some of these fields aren't meant to be filled in and stored
    /// in the DB, they're set later in code from e.g. the IStripCompareResult (e.g. PieceOfArtName)
    [ServiceModel(ServiceName = "phototype_entries", ListingName = "phototype-entry")]
    public class PhotoTypeEntry : BaseModel, IPhotoTypeEntry {
        [ServiceField("comments")]
        public string Comments { get; set; }

        [ServiceField("completed_date")]
        public DateTime CompletedDate { get; set; }

        [ServiceField("date_received")]
        public DateTime DateReceived { get; set; }

        [ServiceField("date_scheduled")]
        public DateTime DateScheduled { get; set; }

        [ServiceField("defect_class_1")]
        public int? DefectClass1 { get; set; }

        [ServiceField("defect_class_2")]
        public int? DefectClass2 { get; set; }

        [ServiceField("defect_class_3")]
        public int? DefectClass3 { get; set; }

        [ServiceField("defect_class_4")]
        public int? DefectClass4 { get; set; }

        [ServiceField("horizontal_registration")]
        public double HorizontalRegistration { get; set; }

        [ServiceField("vertical_registration")]
        public double VerticalRegistration { get; set; }

        [ServiceField("is_production")]
        public bool IsProduction { get; set; }

        [ServiceField("job_id")]
        public long JobId { get; set; }

        [ServiceField("measurement_id")]
        public long MeasurementId { get; set; }

        [ServiceField("piece_of_art_name")]
        public string PieceOfArtName { get; set; }

        [ServiceField("pressrun_completed")]
        public bool PressrunCompleted { get; set; }

        [ServiceField("pressrun_date")]
        public DateTime PressrunDate { get; set; }

        [ServiceField("press_sheet_identifier")]
        public string PressSheetIdentifier { get; set; }

        [ServiceField("printed_date")]
        public DateTime PrintedDate { get; set; }

        [ServiceField("printer_roll_number")]
        public string PrinterRollNumber { get; set; }

        [ServiceField("print_lot_number")]
        public string PrintLotNumber { get; set; }

        [ServiceField("print_plant_location")]
        public string PrintPlantLocation { get; set; }

        [ServiceField("upc")]
        public PhotoTypeUpc Upc { get; set; }

        [ServiceField("visual_match")]
        public PhotoTypeVisualMatch VisualMatch { get; set; }

        public PhotoTypeEntry() {
            PieceOfArtName = string.Empty;
            PressSheetIdentifier = string.Empty;
            PrintLotNumber = string.Empty;
            PrintPlantLocation = string.Empty;

            DateReceived = DateTime.Now;
            DateScheduled = DateTime.Now;
            CompletedDate = DateTime.Now;
            PressrunDate = DateTime.Now;
            PrintedDate = DateTime.Now;
        }
    }
}