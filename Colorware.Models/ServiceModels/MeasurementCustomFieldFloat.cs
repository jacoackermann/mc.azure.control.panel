﻿using System.Globalization;

using Colorware.Core.Data.Models;
using Colorware.Core.Helpers.Formatting;

namespace Colorware.Models.ServiceModels {
    public class MeasurementCustomFieldFloat : MeasurementCustomField {
        public MeasurementCustomFieldFloat(int fieldIndex, string name, double value,
            MeasurementCustomFieldType typeOfField)
            : base(fieldIndex, name, "", typeOfField) {
            this.value = value;
        }

        private IValueFormatter<double> numberFormatter;

        public IValueFormatter<double> NumberFormatter {
            get { return numberFormatter ?? (numberFormatter = new DoubleFormatter("0.####")); }
            set { numberFormatter = value; }
        }

        private double value = 0;

        public override string Value {
            get { return NumberFormatter.Format(value); }
            set {
                double parsedValue;
                if (!double.TryParse(value, NumberStyles.Number, CultureInfo.InvariantCulture, out parsedValue))
                    return;
                this.value = parsedValue;
                OnPropertyChanged("Value");
            }
        }

        public override void Clear() {
            value = default(double);
            OnPropertyChanged("Value");
        }

        public double RawValue {
            get { return value; }
        }
    }
}