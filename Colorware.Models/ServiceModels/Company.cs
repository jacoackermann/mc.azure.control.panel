﻿using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("companies_access")]
    [ServiceModel(ListingName = "company", ServiceName = "companies")]
    public class Company : BaseModel, ICompany {
        #region Serialized fields
        [ServiceField("external_guid")]
        public Guid ExternalGuid { get; set; } = Guid.NewGuid();

        [ServiceField("name")]
        public String Name { get; set; }

        [ServiceField("address1")]
        public String Address1 { get; set; }

        [ServiceField("address2")]
        public String Address2 { get; set; }

        [ServiceField("city1")]
        public String City1 { get; set; }

        [ServiceField("city2")]
        public String City2 { get; set; }

        [ServiceField("contact_person")]
        public String ContactPerson { get; set; }

        [ServiceField("email")]
        public String Email { get; set; }

        [ServiceField("mobile")]
        public String Mobile { get; set; }

        [ServiceField("telephone")]
        public String Telephone { get; set; }

        [ServiceField("zipcode1")]
        public String ZipCode1 { get; set; }

        [ServiceField("zipcode2")]
        public String ZipCode2 { get; set; }

        [ServiceField("country")]
        public string Country { get; set; }

        [ServiceField("is_remote_company")]
        public bool IsRemoteCompany { get; set; }
        #endregion

        public Company() {
            Name = "NA";
            IsRemoteCompany = true;
        }
    }
}