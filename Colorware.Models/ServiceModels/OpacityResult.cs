using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("opacity_results_access", CanDeleteOwned = true, CanCreate = true)]
    [ServiceModel(ListingName = "opacity-result", ServiceName = "opacity_results", LogDelete = false)]
    public class OpacityResult : BaseModel, IOpacityResult {
        #region Fields
        [ServiceField("created_at")]
        public DateTime CreatedAt { get; set; }

        [ServiceField("opacity")]
        public double Opacity { get; set; }
        #endregion

        #region Relations
        [ServiceField("measurement_id")]
        public long MeasurementId { get; set; }

        private IBelongsTo<IMeasurement> measurement;

        public IBelongsTo<IMeasurement> Measurement {
            get {
                if (measurement == null)
                    measurement = new BelongsTo<IMeasurement>("MeasurementId", this);
                return measurement;
            }
            set { measurement = value; }
        }
        #endregion

        public OpacityResult() {
            CreatedAt = DateTime.Now;
        }
    }
}