﻿using System;
using System.Diagnostics;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("color_strip_patches_access")]
    [ServiceModel(ServiceName = "color_strip_patches", ListingName = "color-strip-patch", LogDelete = false,
        DefaultSortOrder = "sort_order", SortOrderDirection = ServiceModel.SortOrders.Ascending)]
    [DebuggerDisplay("ColorStripPatch(Id={Id}, Name={Name}, Percentage={Percentage}, PatchType={PatchType}, Slots={Slot1};{Slot2};{Slot3})")]
    public class ColorStripPatch : BaseModel, IColorStripPatch {
        public const int NO_SLOT_VALUE = -1;

        [ServiceField("name")]
        public String Name { get; set; }

        [ServiceField("percentage")]
        public int Percentage { get; set; }

        [ServiceField("ink_zone")]
        public int InkZone { get; set; }

        [ServiceField("slot1", CanBeNull = true, DefaultValue = "-1")] // DefaultValue = NO_SLOT_VALUE
        public int Slot1 { get; set; }

        [ServiceField("slot2", CanBeNull = true, DefaultValue = "-1")] // DefaultValue = NO_SLOT_VALUE
        public int Slot2 { get; set; }

        [ServiceField("slot3", CanBeNull = true, DefaultValue = "-1")] // DefaultValue = NO_SLOT_VALUE
        public int Slot3 { get; set; }

        [ServiceField("tolerance_group")]
        public ToleranceGroups ToleranceGroup { get; set; }

        [ServiceField("patch_type")]
        public PatchTypes PatchType { get; set; }

        [ServiceField("sort_order")]
        public int SortOrder { get; set; }

        [ServiceField("color_strip_id")]
        public long ColorStripId { get; set; }

        public ColorStripPatch() {
            Slot1 = -1;
            Slot2 = -1;
            Slot3 = -1;
            Percentage = 100;
            ToleranceGroup = ToleranceGroups.Undefined;
            PatchType = PatchTypes.Undefined;
            InkZone = 0;
            SortOrder = 0;
            ColorStripId = 0;
            Id = 0;
            Name = "NA";
        }

        #region Helpers
        public bool HasSlot1 {
            get { return Slot1 != NO_SLOT_VALUE; }
        }

        public bool HasSlot2 {
            get { return Slot2 != NO_SLOT_VALUE; }
        }

        public bool HasSlot3 {
            get { return Slot3 != NO_SLOT_VALUE; }
        }

        public bool IsUndefined {
            get { return PatchType == PatchTypes.Undefined; }
        }
        #endregion

        #region Overrides of Object
        public override String ToString() {
            return String.Format("ColorStripPatch({0} ({1}), {2})", Name, Percentage, PatchType);
        }
        #endregion
    }
}