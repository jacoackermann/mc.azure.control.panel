﻿using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("job_strips_access", CanUpdateOwned = true)]
    [ServiceModel(ServiceName = "job_strips", ListingName = "job-strip")]
    public class JobStrip : BaseModel, IJobStrip {
        [ServiceField("external_guid")]
        public Guid ExternalGuid { get; set; } = Guid.NewGuid();

        [ServiceField("is_customized")]
        public bool IsCustomized { get; set; }

        [ServiceField("is_reset")]
        public bool IsReset { get; set; }

        #region Relations
        private IHasManyCollection<IJobStripPatch> jobStripPatches;

        [Related("job_strip_id", OnDeleteAction.DeleteRelated)]
        public IHasManyCollection<IJobStripPatch> JobStripPatches => jobStripPatches ?? (jobStripPatches = new HasManyCollection<IJobStripPatch>("job_strip_id", this));
        #endregion

        public static bool HasPaperWhite(IEnumerable<IJobStripPatch> patches) {
            return patches.Any(p => p.IsPaperWhite);
        }
    }
}