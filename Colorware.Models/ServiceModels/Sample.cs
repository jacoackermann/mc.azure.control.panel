using System;
using System.Globalization;

using Colorware.Core.Color;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

using JetBrains.Annotations;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("samples_access", CanDeleteOwned = true, CanUpdateOwned = true, CanCreate = true)]
    [ServiceModel(ListingName = "sample", ServiceName = "samples", LogDelete = false, DefaultSortOrder = "id",
         SortOrderDirection = ServiceModel.SortOrders.Ascending)]
    public class Sample : BaseModel, ISample {
        [ServiceField("l")]
        public double L { get; set; }

        [ServiceField("a")]
        // ReSharper disable InconsistentNaming
        public double a { get; set; }

        // ReSharper restore InconsistentNaming

        [ServiceField("b")]
        // ReSharper disable InconsistentNaming
        public double b { get; set; }

        // ReSharper restore InconsistentNaming

        [ServiceField("illuminant")]
        public Illuminant Illuminant { get; set; }

        [ServiceField("observer")]
        public Observer Observer { get; set; }

        [ServiceField("spectral_m_condition")]
        public MCondition MCondition { get; set; } = MCondition.M0;

        public SpectralMeasurementConditions MeasurementConditions
            => new SpectralMeasurementConditions(Illuminant, Observer, MCondition);

        [ServiceField("density_c")]
        public double DensityC { get; set; }

        [ServiceField("density_m")]
        public double DensityM { get; set; }

        [ServiceField("density_y")]
        public double DensityY { get; set; }

        [ServiceField("density_k")]
        public double DensityK { get; set; }


        [ServiceField("density_s")]
        public double DensityS { get; set; }

        [ServiceField("s_wavelength")]
        public double SWavelength { get; set; }

        [ServiceField("spectrum")]
        public string Spectrum { get; private set; }

        public bool HasSpectrum => !string.IsNullOrEmpty(Spectrum);

        [CanBeNull]
        private Spectrum asSpectrum;

        public Spectrum AsSpectrum {
            get {
                if (asSpectrum != null)
                    return asSpectrum;

                if (string.IsNullOrEmpty(Spectrum))
                    return null;

                return asSpectrum = new Spectrum(Spectrum, SpectrumStart, SpectrumEnd, ",");
            }
        }

        [ServiceField("spectrum_start")]
        public int SpectrumStart { get; private set; }

        [ServiceField("spectrum_end")]
        public int SpectrumEnd { get; private set; }

        /// <summary>
        /// Custom integer field which holds information depending on context.
        /// </summary>
        [ServiceField("tag")]
        public long Tag { get; set; }

        #region Relations
        [ServiceField("measurement_id")]
        public long MeasurementId { get; set; }

        private IBelongsTo<IMeasurement> measurement;

        public IBelongsTo<IMeasurement> Measurement {
            get { return measurement ?? (measurement = new BelongsTo<IMeasurement>("MeasurementId", this)); }
            set { measurement = value; }
        }
        #endregion

        #region Properties
        public CMYK Densities {
            get { return new CMYK(DensityC, DensityM, DensityY, DensityK); }
            set {
                DensityC = value.C;
                DensityM = value.M;
                DensityY = value.Y;
                DensityK = value.K;
            }
        }

        public double Density => Densities.GetFilteredValue();

        public CmykComponents CmykComponent => Densities.GetFilteredComponent();

        [ServiceField("is_dud")]
        public bool IsDud { get; set; }

        public System.Windows.Media.Color AsColor => AsLab.AsColor;

        public double AsDensity => Densities.GetFilteredValue();

        public Lab AsLab => new Lab(L, a, b, MeasurementConditions);

        public Lch AsLch => AsLab.AsLch;

        public string AsString {
            get {
                var lab = AsLab;
                return string.Format(CultureInfo.InvariantCulture, "{0:F}, {1:F}, {2:F}", lab.L, lab.a, lab.b);
            }
        }
        #endregion

        private Sample([CanBeNull] Spectrum spectrum, [CanBeNull] Lab lab, [CanBeNull] CMYK densities,
                       [NotNull] SpectralMeasurementConditions measurementConditions) {
            // Type system failure
            if (spectrum == null && lab == null)
                throw new Exception("Must specify spectrum and/or Lab to create a Sample!");

            Illuminant = measurementConditions.Illuminant;
            Observer = measurementConditions.Observer;
            MCondition = measurementConditions.SpectralMCondition;

            if (lab != null) {
                L = lab.L;
                a = lab.a;
                b = lab.b;
            }

            if (densities != null)
                Densities = densities;

            setSpectrumFields(spectrum);
        }

        private void setSpectrumFields([CanBeNull] Spectrum spectrum) {
            if (spectrum == null) {
                Spectrum = "";
            }
            else {
                Spectrum = spectrum.GetStringValue();
                SpectrumStart = (int)spectrum.StartWavelength;
                SpectrumEnd = (int)spectrum.EndWaveLength;
            }
        }

        // TODO: this constructor is debatable, it makes no theoretical sense to supply both
        // a spectrum and an Lab (as we can calculate the Lab from the spectrum to assure
        // they represent the same color). However, an importer still uses it and it can
        // also be used for performance reasons, but a specialized ViewModel might be better
        // suited in that case.
        public Sample([NotNull] Spectrum spectrum, [NotNull] Lab lab, [NotNull] CMYK densities)
            : this(spectrum, lab, densities, lab.MeasurementConditions) {
            if (spectrum == null) throw new ArgumentNullException(nameof(spectrum));
            if (lab == null) throw new ArgumentNullException(nameof(lab));
            if (densities == null) throw new ArgumentNullException(nameof(densities));
        }

        public Sample([NotNull] Spectrum spectrum, [NotNull] CMYK cmyk,
                      [NotNull] SpectralMeasurementConditions measurementConditions)
            : this(spectrum, spectrum.ToXYZ(measurementConditions).AsLab, cmyk, measurementConditions) {
            if (spectrum == null) throw new ArgumentNullException(nameof(spectrum));
            if (cmyk == null) throw new ArgumentNullException(nameof(cmyk));
            if (measurementConditions == null) throw new ArgumentNullException(nameof(measurementConditions));
        }

        public Sample([NotNull] Spectrum spectrum, [NotNull] SpectralMeasurementConditions measurementConditions)
            : this(spectrum, spectrum.ToXYZ(measurementConditions).AsLab, null, measurementConditions) {
            if (spectrum == null) throw new ArgumentNullException(nameof(spectrum));
            if (measurementConditions == null) throw new ArgumentNullException(nameof(measurementConditions));
        }

        /// <summary>
        /// NB: Use a constructor overload that requires a Spectrum, if you have it.
        /// </summary>
        public Sample([NotNull] Lab lab, [NotNull] CMYK cmyk) : this(null, lab, cmyk, lab.MeasurementConditions) {
            if (lab == null) throw new ArgumentNullException(nameof(lab));
            if (cmyk == null) throw new ArgumentNullException(nameof(cmyk));
        }

        /// <summary>
        /// NB: Use a constructor overload that requires a Spectrum, if you have it.
        /// </summary>
        public Sample([NotNull] Lab lab) : this(null, lab, null, lab.MeasurementConditions) {
            if (lab == null) throw new ArgumentNullException(nameof(lab));
        }

        private Sample() {}

        #region Overrides of Object
        public override string ToString() {
            return $"Sample({AsLab}, {Densities})";
        }
        #endregion

        public static ISample CreateImageDummySample() {
            return new Sample {Tag = -1};
        }
    }
}