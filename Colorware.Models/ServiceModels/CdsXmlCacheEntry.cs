﻿using System.Threading.Tasks;

using Colorware.Core.Data;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

namespace Colorware.Models.ServiceModels {
    [ServiceModel(ServiceName = "cds_xml_cache_entries", ListingName = "cds-xml-cache-entries", LogDelete = false)]
    public class CdsXmlCacheEntry : BaseModel {
        #region Service fields
        
        

        [ServiceField("job_id")]
        public long JobId { get; set; }

        [ServiceField("measurement_id")]
        public long MeasurementId { get; set; }

        [ServiceField("content")]
        public string Content { get; set; }

        [ServiceField("transaction_type")]
        public CdsTransactionType TransactionType { get; set; }
        #endregion

        public static async Task DeleteForAsync(IStripCompareResult compareResult) {
            var query = new Query()
                .Eq("job_id", compareResult.Job.Id)
                .Eq("measurement_id", compareResult.Measurement.Id);
            var items = await FindAllAsync<CdsXmlCacheEntry>(query);
            await DeleteAllAsync(items);
        }
    }
}