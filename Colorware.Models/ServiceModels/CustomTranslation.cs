﻿using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    /// <inheritdoc cref="ICustomTranslation"/>
    [ServiceModel(ServiceName = "custom_translations", ListingName = "custom-translation")]
    public class CustomTranslation : BaseModel, ICustomTranslation {
        /// <inheritdoc />
        [ServiceField("translation_key")]
        public string TranslationKey { get; set; }

        /// <inheritdoc />
        [ServiceField("translation_value")]
        public string TranslationValue { get; set; }
    }
}