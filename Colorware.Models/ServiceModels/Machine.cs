﻿using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("machines_access")]
    [ServiceModel(ListingName = "machine", ServiceName = "machines")]
    public class Machine : BaseModel, IMachine {
        #region Serialized fields
        [ServiceField("external_guid")]
        public Guid ExternalGuid { get; set; } = Guid.NewGuid();
        [ServiceField("name")]
        public String Name { get; set; }

        [ServiceField("number_of_colors")]
        public int NumberOfColors { get; set; }

        [ServiceField("number_of_inkzones")]
        public int NumberOfInkzones { get; set; }

        [ServiceField("zone_width")]
        public double ZoneWidth { get; set; }
        #endregion

        #region Relations
        [ServiceField("machine_type_id")]
        public long MachineTypeId { get; set; }

        private IBelongsTo<IMachineType> machineType;

        [Related("machine_type_id")]
        public IBelongsTo<IMachineType> MachineType
            => machineType ?? (machineType = new BelongsTo<IMachineType>("MachineTypeId", this));

        [ServiceField("company_id")]
        public long CompanyId { get; set; }

        private BelongsTo<ICompany> company;

        [Related("company_id")]
        public IBelongsTo<ICompany> Company => company ?? (company = new BelongsTo<ICompany>("CompanyId", this));

        [ServiceField("default_tolerance_id")]
        public long DefaultToleranceId { get; set; }

        private BelongsTo<IToleranceSet> toleranceSet;

        [Related("default_tolerance_id")]
        public IBelongsTo<IToleranceSet> ToleranceSet
            => toleranceSet ?? (toleranceSet = new BelongsTo<IToleranceSet>("DefaultToleranceId", this));

        public bool HasDefaultToleranceSet => DefaultToleranceId > 0;
        #endregion

        public Machine() {
            Name = "NA";
            NumberOfColors = 1;
            NumberOfInkzones = 1;
            ZoneWidth = 0.0;
        }

        public static IMachine CreateMockMachine() {
            return new Machine {
                Name = "Mock machine",
                NumberOfColors = 4,
                NumberOfInkzones = 32,
                ZoneWidth = 29.8,
            };
        }
    }
}