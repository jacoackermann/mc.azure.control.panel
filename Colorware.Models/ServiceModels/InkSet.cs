﻿using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("ink_sets_access")]
    [ServiceModel(ServiceName = "ink_sets", ListingName = "ink-set")]
    public class InkSet : BaseModel, IInkSet {
        [ServiceField("external_guid")]
        public Guid ExternalGuid { get; set; } = Guid.NewGuid();

        [ServiceField("name")]
        public String Name { get; set; }

        #region Relations
        [Related("ink_set_id", OnDeleteAction.DeleteRelated)]
        public IHasManyCollection<IInk> Inks { get; private set; }
        #endregion

        public InkSet() {
            Inks = new HasManyCollection<IInk>("ink_set_id", this);
        }
    }
}