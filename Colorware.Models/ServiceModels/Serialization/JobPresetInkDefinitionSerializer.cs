﻿using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

namespace Colorware.Models.ServiceModels.Serialization {
    public class JobPresetInkDefinitionSerializer {
        public static string Serialize(IEnumerable<JobPresetInkDefinition> definitions) {
            return JsonConvert.SerializeObject(definitions.ToList());
        }

        public static IEnumerable<JobPresetInkDefinition> Deserialize(string input) {
            return JsonConvert.DeserializeObject<List<JobPresetInkDefinition>>(input);
        } 
    }
}