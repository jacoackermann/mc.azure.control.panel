﻿using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("inks_access")]
    [ServiceModel(ServiceName = "inks", ListingName = "ink")]
    public class Ink : BaseModel, IInk {
        [ServiceField("external_guid")]
        public Guid ExternalGuid { get; set; } = Guid.NewGuid();

        [ServiceField("name")]
        public String Name { get; set; }

        #region Relations
        [ServiceField("ink_set_id")]
        public long InkSetId { get; set; }

        [Related("ink_set_id")]
        public IBelongsTo<IInkSet> InkSet { get; private set; }

        [ServiceField("ink_type_id")]
        public long InkTypeId { get; set; }

        [Related("ink_type_id")]
        public IBelongsTo<IInkType> InkType { get; private set; }

        [ServiceField("ink_manufacturer_id")]
        public long InkManufacturerId { get; set; }

        [Related("ink_manufacturer_id")]
        public IBelongsTo<IInkManufacturer> InkManufacturer { get; private set; }
        #endregion

        public Ink() {
            InkSet = new BelongsTo<IInkSet>("InkSetId", this);
            InkType = new BelongsTo<IInkType>("InkTypeId", this);
            InkManufacturer = new BelongsTo<IInkManufacturer>("InkManufacturerId", this);
        }
    }
}