﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Media;

using Colorware.Core.Color;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;
using Colorware.Core.Functional.Option;

using JetBrains.Annotations;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("job_strip_patches_access", CanUpdateOwned = true)]
    [ServiceModel(ServiceName = "job_strip_patches", ListingName = "job-strip-patch", LogDelete = false,
        DefaultSortOrder = "id", SortOrderDirection = ServiceModel.SortOrders.Ascending)]
    public class JobStripPatch : BaseModel, IJobStripPatch {
        #region Fields
        [ServiceField("external_guid")]
        public Guid ExternalGuid { get; set; } = Guid.NewGuid();

        [ServiceField("name")]
        public String Name { get; set; }

        [ServiceField("percentage")]
        public int Percentage { get; set; }

        [ServiceField("patch_type")]
        public PatchTypes PatchType { get; set; }

        [ServiceField("tolerance_group")]
        public ToleranceGroups ToleranceGroup { get; set; }

        // In Image mode this designates the panel number 
        [ServiceField("ink_zone")]
        public int InkZone { get; set; }

        [ServiceField("l")]
        public double L { get; set; }

        [ServiceField("a")]
        public double a { get; set; }

        [ServiceField("b")]
        public double b { get; set; }

        [ServiceField("illuminant")]
        public Illuminant Illuminant { get; set; } = Illuminant.Unknown;

        [ServiceField("observer")]
        public Observer Observer { get; set; } = Observer.Unknown;

        [ServiceField("spectral_m_condition")]
        public MCondition MCondition { get; set; } = MCondition.M0;

        public SpectralMeasurementConditions MeasurementConditions
            => new SpectralMeasurementConditions(Illuminant, Observer, MCondition);

        [ServiceField("cyan")]
        public double Cyan { get; set; }

        [ServiceField("magenta")]
        public double Magenta { get; set; }

        [ServiceField("yellow")]
        public double Yellow { get; set; }

        [ServiceField("key")]
        public double Key { get; set; }

        [ServiceField("spectrum")]
        public String Spectrum { get; set; }

        [ServiceField("spectrum_start")]
        public int SpectrumStart { get; set; }

        [ServiceField("spectrum_end")]
        public int SpectrumEnd { get; set; }

        [ServiceField("default_density")]
        public double DefaultDensity { get; set; }

        [ServiceField("default_dotgain")]
        public double DefaultDotgain { get; set; }

        [ServiceField("is_customized")]
        public bool IsCustomized { get; set; }

        [ServiceField("slot1", CanBeNull = true, DefaultValue = -1)] // DefaultValue = ColorStripPatch.NO_SLOT_VALUE
        public int Slot1 { get; set; } = ColorStripPatch.NO_SLOT_VALUE;

        [ServiceField("slot2", CanBeNull = true, DefaultValue = -1)] // DefaultValue = ColorStripPatch.NO_SLOT_VALUE
        public int Slot2 { get; set; } = ColorStripPatch.NO_SLOT_VALUE;

        [ServiceField("slot3", CanBeNull = true, DefaultValue = -1)] // DefaultValue = ColorStripPatch.NO_SLOT_VALUE
        public int Slot3 { get; set; } = ColorStripPatch.NO_SLOT_VALUE;

        // For image: measure target location, in image pixels (top left = 0,0)
        [ServiceField("target_x")]
        public int TargetX { get; set; }

        [ServiceField("target_y")]
        public int TargetY { get; set; }

        [ServiceField("use_tolerance_delta_e_override")]
        public bool UseDeltaEToleranceOverride { get; set; }

        [ServiceField("tolerance_delta_e_override")]
        public double DeltaEToleranceOverride { get; set; }

        [ServiceField("is_spot")]
        public bool IsSpot { get; set; }
        #endregion

        public bool HasSlot1 => Slot1 >= 0;

        public bool HasSlot2 => Slot2 >= 0;

        public bool HasSlot3 => Slot3 >= 0;

        public double Density => DefaultDensity;

        #region Relations
        [ServiceField("job_strip_id")]
        public long JobStripId { get; set; }

        [ServiceField("reference_patch_id")]
        public long ReferencePatchId { get; set; }

        public IBelongsTo<IReferencePatch> ReferencePatch { get; set; }

        [ServiceField("pantone_live_dependent_standard_guid")]
        public Guid PantoneLiveDependentStandardGuid { get; set; }

        [ServiceField("pantone_live_palette_guid")]
        public Guid PantoneLivePaletteGuid { get; set; }
        #endregion

        #region Helpers
        [Obsolete("This gives a false sense of security (ReferencePatch can be missing in DB)")]
        public bool HasReferencePatch => ReferencePatchId > 0;

        public bool IsPantoneLivePatch => PantoneLiveDependentStandardGuid != Guid.Empty;

        [NotNull]
        public String SlotPreview {
            get {
                if (PatchType == PatchTypes.Solid || PatchType == PatchTypes.Dotgain ||
                    PatchType == PatchTypes.DotgainOnly)
                    return Slot1.ToString(CultureInfo.InvariantCulture);
                if (PatchType == PatchTypes.Overprint)
                    return Slot1 + "+" + Slot2;
                if (PatchType == PatchTypes.Undefined)
                    return Slot1.ToString(CultureInfo.InvariantCulture);
                return "";
            }
        }

        public bool IsUndefined => PatchType == PatchTypes.Undefined;

        public bool IsDefined => !IsUndefined;

        public bool IsDotgainOnly => PatchType == PatchTypes.DotgainOnly;

        public bool IsAnyBalance => PatchType == PatchTypes.Balance ||
                                    PatchType == PatchTypes.BalanceHighlight ||
                                    PatchType == PatchTypes.BalanceMidtone ||
                                    PatchType == PatchTypes.BalanceShadow;

        public long ColorStripPatchId { get; set; }

        public bool HasSpectrum => !String.IsNullOrEmpty(Spectrum);

        [NotNull]
        public Spectrum AsSpectrum {
            get { return new Spectrum(Spectrum, SpectrumStart, SpectrumEnd, ","); }
            set { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Set or get lab via XYZ values of model.
        /// </summary>
        /// <remarks>TODO Use lab in datamodel itself.</remarks>
        [NotNull]
        public Lab AsLab {
            get {
                if (IsUndefined) {
                    // Need some ill/obs to be able to get a color representation
                    return new Lab(0, 0, 0,
                                   new SpectralMeasurementConditions(Illuminant.D50, Observer.TwoDeg,
                                                                     MCondition.M0));
                }

                return new Lab(L, a, b, MeasurementConditions);
            }
            set {
                // value.EnsureConditionsAreCompatible(Illuminant, Observer);
                Illuminant = value.MeasurementConditions.Illuminant;
                Observer = value.MeasurementConditions.Observer;
                MCondition = value.MeasurementConditions.SpectralMCondition;
                L = value.L;
                a = value.a;
                b = value.b;
            }
        }

        [NotNull]
        public Lch AsLch => AsLab.AsLch;

        public Color AsColor => IsDefined ? AsLab.AsColor : Colors.Transparent;

        public bool IsSolid => PatchType == PatchTypes.Solid;

        public bool IsDotgain => PatchType == PatchTypes.Dotgain;

        public bool IsDotgainLike => PatchType == PatchTypes.Dotgain || PatchType == PatchTypes.DotgainOnly;

        public bool IsPaperWhite => PatchType == PatchTypes.Paperwhite;

        public bool IsOverprint => PatchType == PatchTypes.Overprint;

        public int FirstValidSlot {
            get {
                if (Slot1 != ColorStripPatch.NO_SLOT_VALUE)
                    return Slot1;
                if (Slot2 != ColorStripPatch.NO_SLOT_VALUE)
                    return Slot2;
                if (Slot3 != ColorStripPatch.NO_SLOT_VALUE)
                    return Slot3;
                return ColorStripPatch.NO_SLOT_VALUE;
            }
        }

        // For PantoneLIVE implementation in Image
        public Option<Guid> PantoneLiveObjectGuid { get; set; }

        // For disabling patches in the color bar during job setup.
        public bool IsDisabled { get; set; }
        #endregion

        public JobStripPatch() {
            ReferencePatch = new BelongsTo<IReferencePatch>("ReferencePatchId", this);
        }

        /// <inheritdoc />
        public void OverrideLabTargetForG7(Lab targetLab) {
            Spectrum = null;
            L = targetLab.L;
            a = targetLab.a;
            b = targetLab.b;
        }

        /// <param name="useReferenceForSlots">Set to true for usage in Image or SVF importer</param>
        /// <param name="checkMeasurementConditions">Specifies if the reference patch (when provided) should be checked for compatible measurement conditions. Default value is 'true'.</param>
        [NotNull]
        public static IJobStripPatch CreateFromReferencePatch([CanBeNull] IReferencePatch refPatch,
                                                              [CanBeNull] IColorStripPatch csPatch,
                                                              [NotNull] SpectralMeasurementConditions
                                                                  measurementConditions,
                                                              bool useReferenceForSlots,
                                                              bool checkMeasurementConditions = true) {
            if (measurementConditions == null) throw new ArgumentNullException(nameof(measurementConditions));

            if (refPatch == null
                || measurementConditions.Illuminant == Illuminant.Unknown
                || measurementConditions.Observer == Observer.Unknown
                ||
                checkMeasurementConditions &&
                !refPatch.MeasurementConditions.SpectrallyCompatibleWith(measurementConditions)) {
                var undef = CreateUndefined();
                if (csPatch != null) {
                    undef.ColorStripPatchId = csPatch.Id;
                    undef.Percentage = csPatch.Percentage;
                    undef.Name = csPatch.Name;
                    undef.InkZone = csPatch.InkZone;
                    undef.ToleranceGroup = csPatch.ToleranceGroup;
                    undef.Slot1 = csPatch.Slot1;
                    undef.Slot2 = csPatch.Slot2;
                    undef.Slot3 = csPatch.Slot3;
                }
                return undef;
            }
            var jsp = new JobStripPatch {
                ReferencePatchId = refPatch.Id,
                IsSpot = refPatch.IsSpot,
                Name = refPatch.Name,
                Spectrum =
                    refPatch.IsPantoneLivePatch ? refPatch.AsSpectrum.SerializeAsEncryptedSpectrum() : refPatch.Spectrum,
                SpectrumStart = refPatch.SpectrumStart,
                SpectrumEnd = refPatch.SpectrumEnd,
                Illuminant = measurementConditions.Illuminant,
                Observer = measurementConditions.Observer,
                MCondition = measurementConditions.SpectralMCondition,
                AsLab = refPatch.HasSpectrum
                            ? refPatch.AsSpectrum.ToLab(measurementConditions)
                            : refPatch.AsLab,
                PatchType = refPatch.PatchType,
                DefaultDensity = refPatch.DefaultDensity,
                DefaultDotgain = refPatch.DefaultDotgain,
                Cyan = refPatch.Cyan,
                Magenta = refPatch.Magenta,
                Yellow = refPatch.Yellow,
                Key = refPatch.Key,
                PantoneLiveObjectGuid = refPatch.PantoneLiveObjectGuid,
                PantoneLiveDependentStandardGuid =
                    refPatch.PantoneLiveStandardId.SelectOrElseDefault(id => id.DependentStandardGuid),
                PantoneLivePaletteGuid = refPatch.PantoneLiveStandardId.SelectOrElseDefault(id => id.PaletteGuid),
                UseDeltaEToleranceOverride = refPatch.UseDeltaEToleranceOverride,
                DeltaEToleranceOverride = refPatch.DeltaEToleranceOverride
            };

            if (csPatch == null) {
                jsp.ColorStripPatchId = -1;
                jsp.InkZone = 0;
                jsp.Percentage = refPatch.Percentage;
                jsp.ToleranceGroup = refPatch.IsSolid && refPatch.IsSpot
                                         ? ToleranceGroups.Spotcolor
                                         : ToleranceSet.GetDefaultForPatchType(refPatch.PatchType);

                // Image and SVF importer only (need to fill slots from the ref. patch because we can't extract it from the
                // mapping field like normal)
                if (useReferenceForSlots) {
                    if (refPatch.IsSolid) {
                        if (refPatch.IsPantoneLivePatch)
                            // Negative numbers aren't in use yet, so use them for PantoneLIVE patches that
                            // don't have a refpatch id to use
                            jsp.Slot1 = -Math.Abs(refPatch.PantoneLiveObjectGuid.GetHashCode());
                        else
                            jsp.Slot1 = (int)refPatch.Id;
                    }
                    else {
                        jsp.Slot1 = (int)refPatch.Parent1Id == 0
                                        ? ColorStripPatch.NO_SLOT_VALUE
                                        : (int)refPatch.Parent1Id;
                        jsp.Slot2 = (int)refPatch.Parent2Id == 0
                                        ? ColorStripPatch.NO_SLOT_VALUE
                                        : (int)refPatch.Parent2Id;
                        jsp.Slot3 = (int)refPatch.Parent3Id == 0
                                        ? ColorStripPatch.NO_SLOT_VALUE
                                        : (int)refPatch.Parent3Id;
                    }
                }
            }
            else {
                jsp.ColorStripPatchId = csPatch.Id;
                jsp.InkZone = csPatch.InkZone;
                jsp.Percentage = csPatch.Percentage;
                // Change made in resonse to issue PV-752
                // Special check for solid process- and solid reference colors.
                // As these can be switched around freely we want to keep their tolerance groups
                // linked to their inherit category. So if a spot is mapped in a process color
                // location, we still want to use the SpotColor tolerance group. The same goes
                // for spot colors. Here we check if this situations is occuring and if it is not,
                // we just assign the tolerance group from the definition of the patch in the colorbar.
                // NOTE: that at the time of writing 2015-04-01 the IsSpot field may not be set correctly
                // for all spot color instances in the database. As such a hack was added to the ColorStripMapper
                // to set this property when the list of reference patches is set.
                if (csPatch.PatchType == PatchTypes.Solid &&
                    (csPatch.ToleranceGroup == ToleranceGroups.Primaries ||
                     csPatch.ToleranceGroup == ToleranceGroups.Spotcolor)) {
                    jsp.ToleranceGroup =
                        refPatch.IsSpot ? ToleranceGroups.Spotcolor : ToleranceGroups.Primaries;
                }
                else {
                    jsp.ToleranceGroup = csPatch.ToleranceGroup;
                }
                jsp.Slot1 = csPatch.Slot1;
                jsp.Slot2 = csPatch.Slot2;
                jsp.Slot3 = csPatch.Slot3;
            }

            return jsp;
        }

        [NotNull]
        public static IJobStripPatch CreateDotgainOnlyPatch(
            [CanBeNull] IEnumerable<IDefaultDotgainEntry> defaultDotgainEntries, [CanBeNull] IReferencePatch solidPatch,
            [CanBeNull] IColorStripPatch csPatch, [NotNull] SpectralMeasurementConditions measurementConditions) {
            if (measurementConditions == null) throw new ArgumentNullException(nameof(measurementConditions));

            // Use reference as base.
            var patch = CreateFromReferencePatch(solidPatch, csPatch, measurementConditions, false);
            patch.PatchType = PatchTypes.DotgainOnly;

            // Get target dotgain from slot's selected default dotgain curve, if available
            if (defaultDotgainEntries != null)
                patch.DefaultDotgain = defaultDotgainEntries.FindDotgain(patch.Percentage).OrElse(0);

            if (solidPatch != null) {
                // Scale color by percentage of reference patch.
                /*
                var scaledLab = refPatch.AsLab.ScaleOnChroma(patch.Percentage/100.0);
                scaledLab.L *= 1.0 + patch.Percentage / 100.0;
                if (scaledLab.L > 100) scaledLab.L = 100;
                */
                var scaledLab = solidPatch.AsLab.ScaleTowardsWhite(patch.Percentage / 100.0);
                patch.L = scaledLab.L;
                patch.a = scaledLab.a;
                patch.b = scaledLab.b;
            }

            return patch;
        }

        [NotNull]
        public static JobStripPatch CreateUndefined() {
            return CreateUndefined(null);
        }

        [NotNull]
        public static IJobStripPatch CreateUndefined(int inkZone) {
            var jsp = CreateUndefined(null);
            jsp.InkZone = inkZone;
            return jsp;
        }

        [NotNull]
        public static JobStripPatch CreateUndefined([CanBeNull] IColorStripPatch csPatch) {
            var jsp = new JobStripPatch {
                IsSpot = false,
                PatchType = PatchTypes.Undefined,
                Illuminant = Illuminant.Unknown,
                Observer = Observer.Unknown,
                Name = "Undefined"
            };
            if (csPatch != null) {
                jsp.InkZone = csPatch.InkZone;
                jsp.ColorStripPatchId = csPatch.Id;
                jsp.Name += " (" + csPatch.Name + "/" + csPatch.PatchType + ")";
                jsp.Slot1 = csPatch.Slot1;
                jsp.Slot2 = csPatch.Slot2;
                jsp.Slot3 = csPatch.Slot3;
            }
            return jsp;
        }

        #region Overrides of Object
        public override string ToString() {
            return $"JobStripPatch({Name}, {PatchType}, {AsLab})";
        }
        #endregion
    }
}