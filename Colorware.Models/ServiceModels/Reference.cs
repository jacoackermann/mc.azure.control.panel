﻿using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

using JetBrains.Annotations;

using ReactiveUI;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("color_references_access")]
    [ServiceModel(ServiceName = "references", TableName = "color_references", ListingName = "reference",
        DisplayName = "CW.Data.Reference")]
    public class Reference : BaseModel, IReference {
        public Guid OriginTypeGuid { get; set; }

        #region Serializable fields
        [ServiceField("external_guid")]
        public Guid ExternalGuid { get; set; } = Guid.NewGuid();
        
        [ServiceField("default_dotgain_list_id")]
        public long DefaultDotgainListId { get; set; }

        [ServiceField("is_spot_lib")]
        public bool IsSpotLib { get; set; }

        private string name;

        [ServiceField("name")]
        public string Name {
            get { return name; }
            set { this.RaiseAndSetIfChanged(ref name, value); }
        }

        [ServiceField("company_id")]
        public long CompanyId { get; set; }
        #endregion

        #region Related collections
        protected IHasManyCollection<IReferencePatch> referencePatches;

        [Related("reference_id", OnDeleteAction.DeleteRelated)]
        public virtual IHasManyCollection<IReferencePatch> ReferencePatches {
            get {
                return referencePatches ??
                       (referencePatches = new HasManyCollection<IReferencePatch>("reference_id", this));
            }
        }

        private IBelongsTo<IDefaultDotgainList> defaultDotgainList;

        [Related("default_dotgain_list_id")]
        public IBelongsTo<IDefaultDotgainList> DefaultDotgainList {
            get {
                return defaultDotgainList ??
                       (defaultDotgainList = new BelongsTo<IDefaultDotgainList>("DefaultDotgainListId", this));
            }
        }

        private IBelongsTo<ICompany> company;

        [Related("company_id")]
        public IBelongsTo<ICompany> Company {
            get { return company ?? (company = new BelongsTo<ICompany>("CompanyId", this)); }
        }

        [ServiceField("created_at")]
        public DateTime CreatedAt { get; set; }

        [ServiceField("updated_at")]
        public DateTime UpdatedAt { get; set; }

        [ServiceField("paper_type_id")]
        public long PaperTypeId { get; set; }

        private IBelongsTo<IPaperType> paperType;

        [Related("paper_type_id")]
        public IBelongsTo<IPaperType> PaperType {
            get { return paperType ?? (paperType = new BelongsTo<IPaperType>("PaperTypeId", this)); }
            set { paperType = value; }
        }

        [ServiceField("icc_file_name")]
        public Guid IccFileName { get; set; }

        private bool locked;

        [ServiceField("locked")]
        public bool Locked {
            get { return locked; }
            set { this.RaiseAndSetIfChanged(ref locked, value); }
        }

        public virtual bool IsParentOf([NotNull] IReferencePatch referencePatch) {
            // NB: Only a PantoneLiveReference can be a parent of a PL patch
            // (handled by IsParentOf() in PantoneLiveReference)
            return !referencePatch.IsPantoneLivePatch && Id == referencePatch.ReferenceId;
        }
        #endregion

        public static IReference CreateMockReference() {
            return new Reference {
                Id = 0,
                IsSpotLib = true,
                Name = "Mock reference"
            };
        }

        public override string ToString() {
            return String.Format("Reference({0})", Name);
        }
    }
}