using System;
using System.Diagnostics;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("default_dotgain_lists_access")]
    [DebuggerDisplay("defaultDotgainList(Id={Id}, Name={Name})")]
    [ServiceModel(ListingName = "default-dotgain-list", ServiceName = "default_dotgain_lists")]
    public class DefaultDotgainList : BaseModel, IDefaultDotgainList {
        #region Fields
        [ServiceField("name")]
        public String Name { get; set; }
        #endregion

        #region Relations
        private IHasManyCollection<IDefaultDotgainEntry> defaultDotgainEntries;

        [Related("default_dotgain_list_id", OnDeleteAction.DeleteRelated)]
        public IHasManyCollection<IDefaultDotgainEntry> DefaultDotgainEntries {
            get {
                return defaultDotgainEntries ??
                       (defaultDotgainEntries =
                           new HasManyCollection<IDefaultDotgainEntry>("default_dotgain_list_id", this));
            }
        }
        #endregion
    }
}