﻿using System;
using System.Diagnostics;
using System.Windows.Media;

using Colorware.Core.Color;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.PantoneLIVE;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;
using Colorware.Core.Functional.Option;

using JetBrains.Annotations;

using ReactiveUI;

// ReSharper disable ExplicitCallerInfoArgument

namespace Colorware.Models.ServiceModels {
    [RemoteClient("reference_patches_access")]
    [ServiceModel(ListingName = "reference-patch", ServiceName = "reference_patches", DefaultSortOrder = "id",
        SortOrderDirection = ServiceModel.SortOrders.Ascending)]
    [DebuggerDisplay(
        "ReferencePatch({Id}, Name={Name}, Percentage={Percentage}, PatchType={PatchType}, {Illuminant}/{Observer})")]
    public class ReferencePatch : BaseModel, IReferencePatch, IEquatable<ReferencePatch> {
        public Guid OriginTypeGuid { get; set; }

        #region Fields
        [ServiceField("external_guid")]
        public Guid ExternalGuid { get; set; } = Guid.NewGuid();

        private string name;

        [ServiceField("name")]
        public string Name {
            get { return name; }
            set { this.RaiseAndSetIfChanged(ref name, value); }
        }

        private int percentage;

        [ServiceField("percentage")]
        public int Percentage {
            get { return percentage; }
            set { this.RaiseAndSetIfChanged(ref percentage, value); }
        }

        private double l;

        [ServiceField("l")]
        public double L {
            get { return l; }
            set {
                this.RaiseAndSetIfChanged(ref l, value);
                updateAsLab();
            }
        }

        private double _a;

        [ServiceField("a")]
        public double a {
            get { return _a; }
            set {
                this.RaiseAndSetIfChanged(ref _a, value);
                updateAsLab();
            }
        }

        private double _b;

        [ServiceField("b")]
        public double b {
            get { return _b; }
            set {
                this.RaiseAndSetIfChanged(ref _b, value);
                updateAsLab();
            }
        }

        [ServiceField("x")]
        public double x { get; set; }

        [ServiceField("y")]
        public double y { get; set; }

        [ServiceField("z")]
        public double z { get; set; }

        private Illuminant illuminant = Illuminant.Unknown;

        [ServiceField("illuminant")]
        public Illuminant Illuminant {
            get { return illuminant; }
            set {
                this.RaiseAndSetIfChanged(ref illuminant, value);
                updateAsLab();
            }
        }

        private Observer observer = Observer.Unknown;

        [ServiceField("observer")]
        public Observer Observer {
            get { return observer; }
            set {
                this.RaiseAndSetIfChanged(ref observer, value);
                updateAsLab();
            }
        }

        private MCondition mCondition = MCondition.M0;

        [ServiceField("spectral_m_condition")]
        public MCondition MCondition {
            get { return mCondition; }
            set {
                this.RaiseAndSetIfChanged(ref mCondition, value); 
                updateAsLab();
            }
        }

        public SpectralMeasurementConditions MeasurementConditions
            => new SpectralMeasurementConditions(Illuminant, Observer, MCondition);

        [ServiceField("is_spot")]
        public bool IsSpot {
            get { return isSpot; }
            set { this.RaiseAndSetIfChanged(ref isSpot, value); }
        }

        private double cyan;

        /// <summary>
        /// Don't use for calculations!
        /// </summary>
        [ServiceField("cyan")]
        public double Cyan {
            get { return cyan; }
            set { this.RaiseAndSetIfChanged(ref cyan, value); }
        }

        private double magenta;

        /// <summary>
        /// Don't use for calculations!
        /// </summary>
        [ServiceField("magenta")]
        public double Magenta {
            get { return magenta; }
            set { this.RaiseAndSetIfChanged(ref magenta, value); }
        }

        private double yellow;

        /// <summary>
        /// Don't use for calculations!
        /// </summary>
        [ServiceField("yellow")]
        public double Yellow {
            get { return yellow; }
            set { this.RaiseAndSetIfChanged(ref yellow, value); }
        }

        private double key;

        /// <summary>
        /// Don't use for calculations!
        /// </summary>
        [ServiceField("key")]
        public double Key {
            get { return key; }
            set { this.RaiseAndSetIfChanged(ref key, value); }
        }

        private double defaultDensity;

        [ServiceField("default_density")]
        public double DefaultDensity {
            get { return defaultDensity; }
            set { this.RaiseAndSetIfChanged(ref defaultDensity, value); }
        }

        private double defaultDotgain;

        // This is used only when calculating densitometric dotgain (when a job's DotgainMethodProcess is
        // 'Density'). Spectral and SCTV dotgain is always calculated live, in that case this fields acts just
        // as an indication/reference for the user.
        [ServiceField("default_dotgain", CanBeNull = true, DefaultValue = 0.0)]
        public double DefaultDotgain {
            get { return defaultDotgain; }
            set { this.RaiseAndSetIfChanged(ref defaultDotgain, value); }
        }

        [ServiceField("spectral_density")]
        public double SpectralDensity { get; set; }

        private string spectrum;

        [ServiceField("spectrum")]
        public string Spectrum {
            get { return spectrum; }
            set {
                this.RaiseAndSetIfChanged(ref spectrum, value);
                this.RaisePropertyChanged(nameof(HasSpectrum));
            }
        }

        [ServiceField("spectrum_start")]
        public int SpectrumStart { get; set; }

        [ServiceField("spectrum_end")]
        public int SpectrumEnd { get; set; }

        [ServiceField("wavelength_of_spectral_density")]
        public double WavelengthOfSpectralDensity { get; set; }

        private PatchTypes patchType;

        [ServiceField("patch_type")]
        public PatchTypes PatchType {
            get { return patchType; }
            set { this.RaiseAndSetIfChanged(ref patchType, value); }
        }

        private long parent1Id;

        [ServiceField("parent1_id")]
        public long Parent1Id {
            get { return parent1Id; }
            set { this.RaiseAndSetIfChanged(ref parent1Id, value); }
        }

        private long parent2Id;

        [ServiceField("parent2_id")]
        public long Parent2Id {
            get { return parent2Id; }
            set { this.RaiseAndSetIfChanged(ref parent2Id, value); }
        }

        private long parent3Id;

        [ServiceField("parent3_id")]
        public long Parent3Id {
            get { return parent3Id; }
            set { this.RaiseAndSetIfChanged(ref parent3Id, value); }
        }

        [ServiceField("reference_id")]
        public long ReferenceId { get; set; }

        [ServiceField("gray_c")]
        public double GrayC { get; set; }

        [ServiceField("gray_m")]
        public double GrayM { get; set; }

        [ServiceField("gray_y")]
        public double GrayY { get; set; }

        [ServiceField("use_tolerance_delta_e_override")]
        public bool UseDeltaEToleranceOverride { get; set; }

        [ServiceField("tolerance_delta_e_override")]
        public double DeltaEToleranceOverride { get; set; }

        public Option<Guid> PantoneLiveObjectGuid { get; set; }

        public Option<PantoneLiveStandardId> PantoneLiveStandardId { get; set; }

        public bool IsPantoneLivePatch => PantoneLiveObjectGuid.HasValue;
        #endregion Fields

        #region Relations
        private IBelongsTo<IReference> reference;

        [Related("ReferenceId")]
        public IBelongsTo<IReference> Reference
            => reference ?? (reference = new BelongsTo<IReference>("ReferenceId", this));

        private string extraInfo1;

        [ServiceField("extra_info_1")]
        public string ExtraInfo1 {
            get { return extraInfo1; }
            set { this.RaiseAndSetIfChanged(ref extraInfo1, value); }
        }

        private string extraInfo2;

        [ServiceField("extra_info_2")]
        public string ExtraInfo2 {
            get { return extraInfo2; }
            set { this.RaiseAndSetIfChanged(ref extraInfo2, value); }
        }
        #endregion Relations

        public Lab AsLab { get; private set; }

        public Lch AsLch { get; private set; }

        public Color AsColor { get; private set; }

        public bool IsSolid => PatchType == PatchTypes.Solid;

        public bool IsDotgain => PatchType == PatchTypes.Dotgain || PatchType == PatchTypes.DotgainOnly;

        public bool IsPaperwhite => PatchType == PatchTypes.Paperwhite;

        public bool IsDefined => PatchType != PatchTypes.Undefined;

        public bool IsUndefined => PatchType == PatchTypes.Undefined;

        public bool IsOverprint => PatchType == PatchTypes.Overprint;

        public bool IsAnyBalance => PatchType == PatchTypes.Balance ||
                                    PatchType == PatchTypes.BalanceHighlight ||
                                    PatchType == PatchTypes.BalanceMidtone ||
                                    PatchType == PatchTypes.BalanceShadow;

        public double Density => DefaultDensity;

        private bool ignoreAsLabUpdates;

        private ReferencePatch() {
        }
        /// <summary>
        /// NB: more arguments are needed to create a meaningful patch, but forgetting 
        /// the MeasurementConditions is a common mistake so I made it mandatory
        /// (making other stuff mandatory requires too much refactoring)
        /// </summary>
        public ReferencePatch([NotNull] SpectralMeasurementConditions mc) : this() {
            if (mc == null) throw new ArgumentNullException(nameof(mc));
            if (mc.Illuminant == Illuminant.Unknown)
                throw new ArgumentException(
                    "ReferencePatch must be constructed with a valid Illuminant! (Unknown not allowed)", nameof(mc));
            if (mc.Observer == Observer.Unknown)
                throw new ArgumentException(
                    "ReferencePatch must be constructed with a valid Observer! (Unknown not allowed)", nameof(mc));

            SetLabAndMeasurementConditions(new Lab(0, 0, 0, mc));
        }


        /// <summary>
        /// Defer updates when calling <see cref="updateAsLab()"/> to limit the amount of
        /// calculations when passed a true value. When passed false, updates are calculated as normal.
        /// </summary>
        /// <param name="value"></param>
        private void setIgnoreAsLabUpdates(bool value) {
            ignoreAsLabUpdates = value;
        }

        /// <summary>
        /// Helper method to calculate derived data from L, a, b, illuminant and observer. This
        /// method updates the private <see cref="AsLab"/> value first, then it updates both <see
        /// cref="AsLch"/> and <see cref="AsColor"/>. When <see cref="setIgnoreAsLabUpdates(bool)"/>
        /// has been called with false, updates are defered. This is to limit the amount of
        /// calculations being done when multiple properties are set in a batch."/>.
        /// </summary>
        private void updateAsLab() {
            if (ignoreAsLabUpdates)
                return;

            AsLab = new Lab(L, a, b, MeasurementConditions);

            if (Illuminant != Illuminant.Unknown && Observer != Observer.Unknown) {
                AsLch = AsLab.AsLch;
                AsColor = AsLab.AsColor;
            }
            else {
                AsLch = new Lch(0, 0, 0, SpectralMeasurementConditions.Undefined);
                AsColor = Colors.Black;
            }

            this.RaisePropertyChanged(nameof(AsLab));
            this.RaisePropertyChanged(nameof(AsLch));
            this.RaisePropertyChanged(nameof(AsColor));
        }

        #region Implementation of IToleranceGroup
        private ToleranceGroups? toleranceGroup;
        private bool isSpot;

        public ToleranceGroups ToleranceGroup {
            get {
                return toleranceGroup ?? ToleranceSet.GetDefaultForPatchType(PatchType);
            }
            set { toleranceGroup = value; }
        }
        #endregion Implementation of IToleranceGroup

        #region Overrides of Object
        public override string ToString() {
            return $"ReferencePatch({Name}, {PatchType}, {Percentage}%, {AsLab})";
        }
        #endregion Overrides of Object

        public void UpdateFromSample(ISample sample) {
            if (sample == null) throw new ArgumentNullException(nameof(sample));

            SetLabAndMeasurementConditions(sample.AsLab);

            DefaultDensity = sample.Density;
            Spectrum = sample.Spectrum;
            SpectrumStart = sample.SpectrumStart;
            SpectrumEnd = sample.SpectrumEnd;
        }

        public void SetLabAndMeasurementConditions(Lab lab) {
            if (lab == null) throw new ArgumentNullException(nameof(lab));

            if (HasSpectrum) {
                // This is not allowed because it would change the way the spectrum was measured (which is impossible)
                if (MCondition != lab.MeasurementConditions.SpectralMCondition)
                    throw new Exception(
                        $"Can't set Lab with specified spectral M-condition because {nameof(ReferencePatch)} already has a spectrum with different spectral M-condition!"
                        + $"\n\nCurrent measurement conditions:\n{MeasurementConditions}"
                        + $"\n\nSpecified Lab measurement conditions:\n{lab.MeasurementConditions}");
            }

            // Defer calculating related data until all values have been set.
            setIgnoreAsLabUpdates(true);
            L = lab.L;
            a = lab.a;
            b = lab.b;
            Illuminant = lab.MeasurementConditions.Illuminant;
            Observer = lab.MeasurementConditions.Observer;
            MCondition = lab.MeasurementConditions.SpectralMCondition;
            setIgnoreAsLabUpdates(false);
            updateAsLab();
        }

        public static ReferencePatch CreateMockReferencePatch(Random random) {
            return new ReferencePatch {
                Name = "Reference patch " + random.Next(),
                PatchType = PatchTypes.Solid,
                L = random.NextDouble() * 100,
                a = random.NextDouble() * 200 - 100,
                b = random.NextDouble() * 200 - 100,
                Illuminant = Illuminant.D50,
                Observer = Observer.TwoDeg,
                DefaultDensity = random.NextDouble() * 2,
                DefaultDotgain = random.Next(),
                Percentage = random.Next(100),
            };
        }

        public bool HasSpectrum => !string.IsNullOrWhiteSpace(Spectrum);

        public Spectrum AsSpectrum {
            [NotNull] get { return new Spectrum(Spectrum, SpectrumStart, SpectrumEnd, ","); }
            [CanBeNull]
            set {
                if (value == null) {
                    Spectrum = null;
                }
                else {
                    SpectrumStart = (int)value.StartWavelength;
                    SpectrumEnd = (int)value.EndWaveLength;
                    Spectrum = value.GetStringValue();
                }
            }
        }
        [Pure]
        public override T Clone<T>() {
            var clone = (IReferencePatch)base.Clone<T>();

            clone.PantoneLiveObjectGuid = PantoneLiveObjectGuid;
            clone.PantoneLiveStandardId = PantoneLiveStandardId;

            return (T)clone;
        }

        public bool Equals(ReferencePatch other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return base.Equals(other) && string.Equals(PantoneLiveObjectGuid, other.PantoneLiveObjectGuid);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((ReferencePatch)obj);
        }

        public override int GetHashCode() {
            unchecked {
                return (base.GetHashCode() * 397) ^ PantoneLiveObjectGuid.SelectOrElse(g => g.GetHashCode(), () => 0);
            }
        }
    }
}