﻿using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

using ReactiveUI;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("color_strips_access")]
    [ServiceModel(ServiceName = "color_strips", ListingName = "color-strip")]
    public class ColorStrip : BaseModel, IColorStrip {
        private string name;

        [ServiceField("name")]
        public string Name {
            get { return name; }
            set { this.RaiseAndSetIfChanged(ref name, value); }
        }

        [ServiceField("description")]
        public String Description { get; set; }

        [ServiceField("patchwidth")]
        public double PatchWidth { get; set; }

        [ServiceField("patchheight")]
        public double PatchHeight { get; set; }

        /// <inheritdoc />
        [ServiceField("rowcount")]
        public int RowCount { get; set; } = 1;

        [ServiceField("default_mapping", CanBeNull = true)]
        public String DefaultMapping { get; set; }

        [ServiceField("patches_per_zone", CanBeNull = true)]
        public int PatchesPerZone { get; set; }

        [ServiceField("number_of_zones", CanBeNull = true)]
        public int NumberOfZones { get; set; }

        [ServiceField("repeats")]
        public bool Repeats { get; set; }

        #region Relations
        [ServiceField("company_id")]
        public long CompanyId { get; set; }

        private IBelongsTo<ICompany> company;

        [Related("company_id")]
        public IBelongsTo<ICompany> Company {
            get { return company ?? (company = new BelongsTo<ICompany>("CompanyId", this)); }
        }

        [Related("color_strip_id", OnDeleteAction.DeleteRelated)]
        public IHasManyCollection<IColorStripPatch> ColorStripPatches { get; private set; }
        #endregion

        #region Related properties
        #endregion

        public ColorStrip() {
            ColorStripPatches = new HasManyCollection<IColorStripPatch>("color_strip_id", this);
        }

        public static int[] CalculateSlots(IEnumerable<IColorStripPatch> colorStripPatches) {
            var found = new Dictionary<int, bool>();
            foreach (var patch in colorStripPatches) {
                if (patch.Slot1 >= 0)
                    found[patch.Slot1] = true;
                if (patch.Slot2 >= 0)
                    found[patch.Slot2] = true;
                if (patch.Slot3 >= 0)
                    found[patch.Slot3] = true;
            }
            return found.Keys.ToArray();
        }

        public static int[] GetOrderedSlotLayout(IEnumerable<IColorStripPatch> colorStripPatches) {
            return colorStripPatches.Where(cs => cs.PatchType == PatchTypes.Solid)
                                    .Select(cs => cs.HasSlot1
                                                      ? cs.Slot1
                                                      : (cs.HasSlot2
                                                             ? cs.Slot2
                                                             : cs.Slot3))
                                    .Distinct()
                                    .ToArray();
        }

        #region Overrides of Object
        public override string ToString() {
            return String.Format("ColorStrip(({0}), {1}, {2})", Id, Name, Description);
        }
        #endregion
    }
}