﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using Colorware.Core.Data;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [ServiceModel(ListingName = "configuration-item", ServiceName = "configuration_items")]
    public class ConfigurationItem : BaseModel {
        #region Serialized fields
        
        

        [ServiceField("key")]
        public string Key { get; set; }

        [ServiceField("value")]
        public string Value { get; set; }
        #endregion

        #region Retrieval helpers
        /// <summary>
        /// Find the item for a specific key.
        /// </summary>
        /// <param name="key">The key to retrieve</param>
        /// <param name="defaultValue">The default value to return if no config item could be found for the given key.</param>
        /// <returns>The found item or null if it was not present.</returns>
        public static async Task<string> GetForKeyAsync(string key, string defaultValue) {
            var query = new Query().Eq("[key]", key);
            var items = await FindAllAsync<ConfigurationItem>(query).ConfigureAwait(false);
            return items.Any() ? items.First().Value : defaultValue;
        }

        public static Task<string> GetForKeyAsync(string key) {
            return GetForKeyAsync(key, null);
        }

        public static async Task<bool> GetForKeyAsBoolAsync(string key, bool defaultValue) {
            var value = await GetForKeyAsync(key, defaultValue ? "1" : "0").ConfigureAwait(false);
            return value == "1";
        }

        public static async Task<int> GetForKeyAsIntAsync(string key, int defaultValue) {
            var value =
                await GetForKeyAsync(key, defaultValue.ToString(CultureInfo.InvariantCulture)).ConfigureAwait(false);
            try {
                return Int32.Parse(value);
            }
            catch (FormatException) {
                return defaultValue;
            }
        }

        public static async Task<long> GetForKeyAsLongAsync(string key, long defaultValue) {
            var value =
                await GetForKeyAsync(key, defaultValue.ToString(CultureInfo.InvariantCulture)).ConfigureAwait(false);
            try {
                return long.Parse(value);
            }
            catch (FormatException) {
                return defaultValue;
            }
        }

        public static async Task<double> GetForKeyAsDoubleAsync(string key, double defaultValue) {
            var value =
                await GetForKeyAsync(key, defaultValue.ToString(CultureInfo.InvariantCulture)).ConfigureAwait(false);
            try {
                return Double.Parse(value, CultureInfo.InvariantCulture);
            }
            catch (FormatException) {
                return defaultValue;
            }
        }

        /// <summary>
        /// Find items for multiple keys at once. This should be more efficient for retrieving multiple keys
        /// as there is only one round-trip to the server.
        /// </summary>
        /// <param name="keys">The keys to find.</param>
        /// <returns>The found items. Note that items may be missing from the result if they are not present on the server.</returns>
        public static Task<IReadOnlyList<ConfigurationItem>> GetForKeysAsync(params string[] keys) {
            var query = keys.Aggregate(new Query(Query.QueryType.Or), (q, key) => q.Eq("[key]", key));

            return FindAllAsync<ConfigurationItem>(query);
        }

        /// <summary>
        /// As <see cref="GetForKeysAsync"/>, but returns the found items in a dictionary.
        /// </summary>
        /// <param name="keys">The keys to find.</param>
        /// <returns>A dictionary containing the found items. The items are hashed by their keys.</returns>
        public static async Task<Dictionary<string, ConfigurationItem>> GetForKeysAsDictAsync(params string[] keys) {
            var items = await GetForKeysAsync(keys).ConfigureAwait(false);
            return items.ToDictionary(item => item.Key);
        }
        #endregion

        public override string ToString() {
            return string.Format("ConfigurationItem({0}, key:\"{1}\", value:\"{2}\"", Id, Key, Value);
        }
    }
}