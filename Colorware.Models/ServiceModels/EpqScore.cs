using System;
using System.Collections.Generic;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;
using Colorware.Models.Epq;

namespace Colorware.Models.ServiceModels {
    [ServiceModel(ListingName = "epq-score", ServiceName = "epq_scores")]
    public class EpqScore : BaseModel, IEpqScore {
        #region Fields
        [ServiceField("job_id")]
        public long JobId { get; set; }

        [ServiceField("measurement_id")]
        public long MeasurementId { get; set; }

        [ServiceField("production")]
        public DateTime Production { get; set; }

        [ServiceField("received")]
        public DateTime Received { get; set; }

        [ServiceField("recorded")]
        public DateTime Recorded { get; set; }

        [ServiceField("visual_score")]
        public double VisualScore { get; set; }

        [ServiceField("has_visual_score")]
        public bool HasVisualScore { get; set; }

        [ServiceField("visual_defects")]
        public String VisualDefects { get; set; }

        [ServiceField("printer_name")]
        public string PrinterName { get; set; }

        [ServiceField("printer_location")]
        public string PrinterLocation { get; set; }

        [ServiceField("brand")]
        public string Brand { get; set; }

        /// <summary>
        /// Returns/sets VisualDefects as a list of tags.
        /// </summary>
        public IEnumerable<string> VisualDefectsAsList {
            get { return VisualDefectListDeserializer.Deserialize(VisualDefects); }
            set { VisualDefects = VisualDefectListSerializer.Serialize(value); }
        }

        [ServiceField("registrations")]
        public String Registrations { get; set; }

        public Dictionary<int, int> RegistrationsAsDict {
            get { return RegistrationDeserializer.Deserialize(Registrations); }
            set { Registrations = RegistrationSerializer.Serialize(value); }
        }

        [ServiceField("has_registrations")]
        public bool HasRegistrations { get; set; }

        [ServiceField("opacity_type")]
        public OpacityTypes OpacityType { get; set; }

        [ServiceField("opacity_low")]
        public double OpacityLow { get; set; }

        [ServiceField("opacity_high")]
        public double OpacityHigh { get; set; }

        [ServiceField("opacity_average")]
        public double OpacityAverage { get; set; }

        [ServiceField("was_exported")]
        public bool WasExported { get; set; }

        [ServiceField("last_export_date")]
        public DateTime LastExportDate { get; set; }

        [ServiceField("was_transmitted")]
        public bool WasTransmitted { get; set; }

        [ServiceField("last_transmit_date")]
        public DateTime LastTransmitDate { get; set; }

        [ServiceField("created_at")]
        public DateTime CreatedAt { get; set; }

        [ServiceField("updated_at")]
        public DateTime UpdatedAt { get; set; }
        #endregion

        private static readonly DateTime emptyDateTime = new DateTime(1970,1,1);
        public EpqScore() {
            LastExportDate = emptyDateTime;
            LastTransmitDate = emptyDateTime;
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
        }
    }
}