﻿using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Enums;

namespace Colorware.Models.ServiceModels {
    public class FailedProcessedReportsMeasurement : IFailedProcessedReportsMeasurement, ICreatedStamped, IUpdatedStamped {
        public long Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public long ExternalReportsServerId { get; set; }
        public long MeasurementId { get; set; }
        public ProcessingStatus Status { get; set; }
        public string Message { get; set; }
        public long JobId { get; set; }
        public string JobName { get; set; }
        public string JobNumber { get; set; }
    }
}