using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

namespace Colorware.Models.ServiceModels {
    [ServiceModel(ListingName = "processed-reports-models", ServiceName = "processed_reports_models")]
    public class ProcessedReportsModels : BaseModel, IProcessedReportsModels {
        [ServiceField("external_reports_server_id")]
        public long ExternalReportsServerId { get; set; }

        [ServiceField("model_id")]
        public long ModelId { get; set; }

        [ServiceField("aggregate")]
        public string Aggregate { get; set; }

        [ServiceField("status")]
        public ProcessingStatus Status { get; set; }

        [ServiceField("message")]
        public string Message { get; set; }

        [ServiceField("created_at")]
        public DateTime CreatedAt { get; set; }

        [ServiceField("updated_at")]
        public DateTime UpdatedAt { get; set; }

    }
}