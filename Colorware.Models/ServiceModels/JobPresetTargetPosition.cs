﻿using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [ServiceModel(ServiceName = "job_preset_target_positions", ListingName = "job-preset-target-position",
        DisplayName = "CW.Data.JobPresetTargetPosition")]
    public class JobPresetTargetPosition : BaseModel, IJobPresetTargetPosition {
        #region Fields
        
        

        [ServiceField("x")]
        public int X { get; set; }

        [ServiceField("y")]
        public int Y { get; set; }

        [ServiceField("job_preset_id")]
        public long JobPresetId { get; set; }

        [ServiceField("ink_definitions_sequence_index")]
        public int InkDefinitionsSequenceIndex { get; set; }

        [ServiceField("is_substrate")]
        public bool IsSubstrate { get; set; }

        public JobPresetTargetPosition() {
            Id = 0;
            X = -1;
            Y = -1;
            InkDefinitionsSequenceIndex = -1;
            IsSubstrate = false;
        }

        public JobPresetTargetPosition(int x, int y) : this(x, y, -1, false) {
        }

        public JobPresetTargetPosition(int x, int y, int inkDefinitionsSequenceIndex, bool isSubstrate) {
            X = x;
            Y = y;
            InkDefinitionsSequenceIndex = inkDefinitionsSequenceIndex;
            IsSubstrate = isSubstrate;
        }
        #endregion
    }
}