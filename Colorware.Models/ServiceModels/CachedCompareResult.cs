﻿using System;
using System.Collections.Generic;

using Colorware.Core.Color;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;
using Colorware.Models.StripCompareInterface.Caching;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("cached_compare_results_access", CanCreate = true, CanDeleteOwned = true)]
    [ServiceModel(ListingName = "cached-compare-result", ServiceName = "cached_compare_results", LogDelete = false)]
    public class CachedCompareResult : BaseModel, ICachedCompareResult {
        public const int DotgainPercentageIndex = DotgainSerialization.DotgainPercentageIndex;
        public const int DotgainSampleIndex = DotgainSerialization.DotgainSampleIndex;
        public const int DotgainReferenceIndex = DotgainSerialization.DotgainReferenceIndex;

        public const int AverageInkZoneIdentifier = -1;

        [ServiceField("name")]
        public String Name { get; set; }

        [ServiceField("created_at")]
        public DateTime CreatedAt { get; set; }

        [ServiceField("a")]
        public double a { get; set; }

        [ServiceField("b")]
        public double b { get; set; }

        [ServiceField("l")]
        public double L { get; set; }

        [ServiceField("illuminant")]
        public Illuminant Illuminant { get; set; }

        [ServiceField("observer")]
        public Observer Observer { get; set; }

        [ServiceField("spectral_m_condition")]
        public MCondition SpectralMCondition { get; set; } = MCondition.M0;

        public SpectralMeasurementConditions MeasurementConditions
            => new SpectralMeasurementConditions(Illuminant, Observer, SpectralMCondition);

        [ServiceField("delta_density")]
        public double DeltaDensity { get; set; }

        [ServiceField("printed_density")]
        public double PrintedDensity { get; set; }

        [ServiceField("delta_e")]
        public double DeltaE { get; set; }

        [ServiceField("delta_h")]
        public double DeltaH { get; set; }

        [ServiceField("dotgains")]
        public String Dotgains { get; set; }

        [ServiceField("ink_set_id")]
        public long InkSetId { get; set; }

        [ServiceField("job_id")]
        public long JobId { get; set; }

        [ServiceField("machine_id")]
        public long MachineId { get; set; }

        [ServiceField("measurement_id")]
        public long MeasurementId { get; set; }

        [ServiceField("ok_sheet_id")]
        public long OkSheetId { get; set; }

        [ServiceField("paper_id")]
        public long PaperId { get; set; }

        [ServiceField("user_id")]
        public long UserId { get; set; }

        [ServiceField("patch_type")]
        public PatchTypes PatchType { get; set; }

        [ServiceField("percentage")]
        public int Percentage { get; set; }

        [ServiceField("score")]
        public String Scores { get; set; }

        [ServiceField("slot1")]
        public int Slot1 { get; set; }

        [ServiceField("slot2")]
        public int Slot2 { get; set; }

        [ServiceField("slot3")]
        public int Slot3 { get; set; }

        [ServiceField("updated_at")]
        public DateTime UpdatedAt { get; set; }

        [ServiceField("ink_zone")]
        public int InkZone { get; set; }

        public int SingleSlot {
            get {
                if (Slot1 != ColorStripPatch.NO_SLOT_VALUE)
                    return Slot1;
                if (Slot2 != ColorStripPatch.NO_SLOT_VALUE)
                    return Slot2;
                if (Slot3 != ColorStripPatch.NO_SLOT_VALUE)
                    return Slot3;
                throw new Exception("Invalid slot definition");
            }
        }

        public System.Windows.Media.Color AsColor => new Lab(L, a, b, MeasurementConditions).AsColor;

        public bool IsSolid => PatchType == PatchTypes.Solid;

        public double DeltaETwoDecimals => Math.Round(DeltaE, 2);

        private List<List<double>> dotgainValues;

        /// <summary>
        /// Returns a list where each entry has the format:
        /// {percentage, sampleValue, refValue};
        /// </summary>
        public List<List<double>> DotgainValues {
            get {
                if (dotgainValues == null) {
                    try {
                        dotgainValues = DotgainSerialization.Deserialize(Dotgains);
                    }
                    catch (Exception e) {
                        throw new Exception("Error in base64Decode" + e.Message);
                    }
                }
                return dotgainValues;
            }
            set { Dotgains = DotgainSerialization.Serialize(value); }
        }

        private Dictionary<int, int> scoreValues;

        public Dictionary<int, int> ScoreValues {
            get {
                if (scoreValues == null) {
                    try {
                        scoreValues = ScoreSerialization.Deserialize(Scores);
                    }
                    catch (Exception e) {
                        throw new Exception("Error in base64Decode" + e.Message);
                    }
                }
                return scoreValues;
            }
            set { Scores = ScoreSerialization.Serialize(value); }
        }

        public CachedCompareResult() {
            InkZone = AverageInkZoneIdentifier;
        }

        #region Overrides of Object
        public override string ToString() {
            return $"CachedCompareResult({Name}[{Percentage}] {PatchType})";
        }
        #endregion
    }
}