﻿using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

namespace Colorware.Models.ServiceModels {
    [ServiceModel(ListingName = "process_control_statistic", ServiceName = "process_control_statistics")]
    public class ProcessControlStatisticsEntry : BaseModel, IProcessControlStatisticsEntry {
        private string userName;
        private string productionStatus;

        [ServiceField("user_name")]
        public string UserName {
            get { return userName; }
            set {
                if (value.Length > 255)
                    throw new ArgumentException();

                userName = value;
            }
        }

        [ServiceField("job_id")]
        public long JobId { get; set; }

        [ServiceField("job_name")]
        public string JobName { get; set; }

        [ServiceField("job_number")]
        public string JobNumber { get; set; }

        [ServiceField("job_description")]
        public string JobDescription { get; set; }

        [ServiceField("sheet_number")]
        public int SheetNumber { get; set; }

        [ServiceField("production_status")]
        public string ProductionStatus {
            get { return productionStatus; }
            set {
                if (value.Length > 50)
                    throw new ArgumentException();

                productionStatus = value;
            }
        }

        [ServiceField("created_at")]
        public DateTime CreatedAt { get; set; }

        [ServiceField("client_id")]
        public long ClientId { get; set; }

        [ServiceField("client_name")]
        public string ClientName { get; set; }

        [ServiceField("client_colorbook_name")]
        public string ClientColorbookName { get; set; }

        [ServiceField("colorbar_name")]
        public string ColorbarName { get; set; }

        [ServiceField("measurement_id")]
        public long MeasurementId { get; set; }

        [ServiceField("measurement_comment")]
        public string MeasurementComment { get; set; }

        [ServiceField("machine_id")]
        public long MachineId { get; set; }

        [ServiceField("machine_name")]
        public string MachineName { get; set; }

        [ServiceField("paper_type_name")]
        public string PaperTypeName { get; set; }

        [ServiceField("sort_order")]
        public int SortOrder { get; set; }

        [ServiceField("ink_zone")]
        public int InkZone { get; set; }

        [ServiceField("patch_type")]
        public string PatchType { get; set; }

        [ServiceField("sample_name")]
        public string SampleName { get; set; }

        [ServiceField("sample_percentage")]
        public int SamplePercentage { get; set; }

        [ServiceField("sample_l")]
        public double SampleL { get; set; }

        [ServiceField("sample_a")]
        public double SampleA { get; set; }

        [ServiceField("sample_b")]
        public double SampleB { get; set; }

        [ServiceField("reference_l")]
        public double ReferenceL { get; set; }

        [ServiceField("reference_a")]
        public double ReferenceA { get; set; }

        [ServiceField("reference_b")]
        public double ReferenceB { get; set; }

        [ServiceField("sample_density")]
        public double SampleDensity { get; set; }

        [ServiceField("reference_density")]
        public double ReferenceDensity { get; set; }

        [ServiceField("sample_dotgain")]
        public double SampleDotgain { get; set; }

        [ServiceField("reference_dotgain")]
        public double ReferenceDotgain { get; set; }

        [ServiceField("delta_e")]
        public double DeltaE { get; set; }

        [ServiceField("delta_h")]
        public double DeltaH { get; set; }

        [ServiceField("delta_density")]
        public double DeltaDensity { get; set; }

        [ServiceField("sample_spectrum")]
        public string SampleSpectrum { get; set; }

        [ServiceField("sample_spectrum_start")]
        public int SampleSpectrumStart { get; set; }

        [ServiceField("sample_spectrum_end")]
        public int SampleSpectrumEnd { get; set; }

        [ServiceField("tolerance_set_name")]
        public string ToleranceSetName { get; set; }

        [ServiceField("tolerance_density")]
        public double ToleranceDensity { get; set; }

        [ServiceField("tolerance_dotgain")]
        public double ToleranceDotgain { get; set; }

        [ServiceField("tolerance_delta_e")]
        public double ToleranceDeltaE { get; set; }

        [ServiceField("conditions_name")]
        public string ConditionsName { get; set; }

        [ServiceField("conditions_status")]
        public string ConditionsStatus { get; set; }

        [ServiceField("conditions_density_m_condition")]
        public MCondition ConditionsDensityMCondition { get; set; } = MCondition.M0;

        [ServiceField("conditions_spectral_m_condition")]
        public MCondition ConditionsSpectralMCondition { get; set; } = MCondition.M0;

        [ServiceField("conditions_illuminant")]
        public string ConditionsIlluminant { get; set; } = "Undefined";

        [ServiceField("conditions_observer_angle")]
        public string ConditionsObserverAngle { get; set; } = "Undefined";

        [ServiceField("is_flexo")]
        public bool IsFlexo { get; set; }

        [ServiceField("flexo_roll")]
        public string FlexoRoll { get; set; }

        [ServiceField("flexo_panel")]
        public string FlexoPanel { get; set; }

        [ServiceField("sample_guid")]
        public string SampleGuid { get; set; }

        [ServiceField("measurement_guid")]
        public string MeasurementGuid { get; set; }

        [ServiceField("has_scumming")]
        public bool HasScumming { get; set; }

        [ServiceField("scumming_delta_e")]
        public double ScummingDeltaE { get; set; }

        [ServiceField("scumming_raw_board_l")]
        public double ScummingRawBoardL { get; set; }

        [ServiceField("scumming_raw_board_a")]
        public double ScummingRawBoardA { get; set; }

        [ServiceField("scumming_raw_board_b")]
        public double ScummingRawBoardB { get; set; }

        [ServiceField("roll_id")]
        public string RollId { get; set; }

        [ServiceField("custom_field_name_1")]
        public string CustomFieldName1 { get; set; }

        [ServiceField("custom_field_data_1")]
        public string CustomFieldData1 { get; set; }

        [ServiceField("custom_field_name_2")]
        public string CustomFieldName2 { get; set; }

        [ServiceField("custom_field_data_2")]
        public string CustomFieldData2 { get; set; }

        [ServiceField("custom_field_name_3")]
        public string CustomFieldName3 { get; set; }

        [ServiceField("custom_field_data_3")]
        public string CustomFieldData3 { get; set; }

        [ServiceField("custom_field_name_4")]
        public string CustomFieldName4 { get; set; }

        [ServiceField("custom_field_data_4")]
        public string CustomFieldData4 { get; set; }

        [ServiceField("custom_field_name_5")]
        public string CustomFieldName5 { get; set; }

        [ServiceField("custom_field_data_5")]
        public string CustomFieldData5 { get; set; }

        [ServiceField("custom_field_name_6")]
        public string CustomFieldName6 { get; set; }

        [ServiceField("custom_field_data_6")]
        public string CustomFieldData6 { get; set; }
    }
}