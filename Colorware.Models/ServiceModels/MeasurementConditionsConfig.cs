using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

using JetBrains.Annotations;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("measurement_conditions_access")]
    [ServiceModel(ListingName = "measurement-condition", ServiceName = "measurement_conditions")]
    public class MeasurementConditionsConfig : BaseModel, IMeasurementConditionsConfig, IExternalGuid {
        [ServiceField("external_guid")]
        public Guid ExternalGuid { get; set; } = System.Guid.NewGuid();

        [ServiceField("is_standard")]
        public bool IsStandard { get; set; }

        [ServiceField("name")]
        public string Name { get; set; }

        [ServiceField("density_standard")]
        public DensityMethod DensityStandard { get; set; } = DensityMethod.Undefined;

        [ServiceField("dotgain_method_process")]
        public DotgainMethod DotgainMethodProcess { get; set; }

        [ServiceField("dotgain_method_spot")]
        public DotgainMethod DotgainMethodSpot { get; set; }

        [ServiceField("white_base")]
        public WhiteBase DensityWhiteBase { get; set; }

        [ServiceField("illuminant")]
        public Illuminant Illuminant { get; set; }

        [ServiceField("observer_angle")]
        public Observer Observer { get; set; }

        [ServiceField("density_m_condition")]
        public MCondition DensityMCondition { get; set; } = MCondition.M0;

        [ServiceField("spectral_m_condition")]
        public MCondition SpectralMCondition { get; set; } = MCondition.M0;

        public string ObserverName => Observer.ToHumanString();

        public static IMeasurementConditionsConfig CreateMockCondition() {
            return CreateDefault("Mock measurement condition");
        }

        public MeasurementConditionsConfig() {
            DotgainMethodProcess = DotgainMethod.Density;
            DotgainMethodSpot = DotgainMethod.SpotColorToneValue;
        }

        public static MeasurementConditionsConfig CreateDefault([NotNull] string name) {
            if (name == null) throw new ArgumentNullException(nameof(name));

            return new MeasurementConditionsConfig {
                // M1 is a better standard, but M0 is more compatible
                DensityMCondition = MCondition.M0,
                SpectralMCondition = MCondition.M0,
                DensityStandard = DensityMethod.StatusT,
                DotgainMethodProcess = DotgainMethod.Spectral,
                DotgainMethodSpot = DotgainMethod.SpotColorToneValue,
                Illuminant = Illuminant.D50,
                Observer = Observer.TwoDeg,
                DensityWhiteBase = WhiteBase.Absolute,
                IsStandard = false,
                Name = name
            };
        }

        /// <summary>
        /// Provides a string representing the measurement conditions of this instance.
        /// Useful for logging.
        /// </summary>
        /// <returns>A string with measurement conditions.</returns>
        public string ToLogString() {
            return
                $"MeasurementConditionsConfig({Name}, {DensityStandard}, {DotgainMethodProcess}/{DotgainMethodSpot}, {SpectralMCondition} (S), {DensityMCondition} (D), {Illuminant}, {Observer}, {DensityWhiteBase}, {IsStandard})";
        }

        #region Overrides of Object
        public override string ToString() {
            return $"MeasurementConditionsConfig({Name})";
        }
        #endregion
    }
}