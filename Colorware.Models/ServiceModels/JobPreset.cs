﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Media.Imaging;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;
using Colorware.Core.Functional.Option;
using Colorware.Models.ServiceModels.Serialization;

namespace Colorware.Models.ServiceModels {
    [DebuggerDisplay("JobPreset({Id}; {Name})")]
    [ServiceModel(ListingName = "job-preset", ServiceName = "job_presets")]
    public class JobPreset : BaseModel, IJobPreset {
        #region Fields
        [ServiceField("name")]
        public string Name { get; set; }

        [ServiceField("job_type")]
        public JobTypes JobType { get; set; }

        [ServiceField("description")]
        public string Description { get; set; }

        [ServiceField("created_at")]
        public DateTime CreatedAt { get; set; }

        [ServiceField("updated_at")]
        public DateTime UpdatedAt { get; set; }

        [ServiceField("user_group_id")]
        public long UserGroupId { get; set; }

        [ServiceField("machine_id")]
        public long MachineId { get; set; }

        /// <summary>
        /// Does this preset belong to a specific machine, or can it be used for all machines.
        /// </summary>
        [ServiceField("is_shared")]
        public bool IsShared { get; set; }

        private IBelongsTo<IMachine> machine;

        [Related("machine_id")]
        public IBelongsTo<IMachine> Machine => machine ?? (machine = new BelongsTo<IMachine>("MachineId", this));

        [ServiceField("paper_type_id")]
        public long PaperTypeId { get; set; }

        private IBelongsTo<IPaperType> paperType;

        [Related("paper_type_id")]
        public IBelongsTo<IPaperType> PaperType
            => paperType ?? (paperType = new BelongsTo<IPaperType>("PaperTypeId", this));

        [ServiceField("tolerance_set_id")]
        public long ToleranceSetId { get; set; }

        private IBelongsTo<IToleranceSet> toleranceSet;

        [Related("tolerance_set_id")]
        public IBelongsTo<IToleranceSet> ToleranceSet
            => toleranceSet ?? (toleranceSet = new BelongsTo<IToleranceSet>("ToleranceSetId", this));

        [ServiceField("measurement_condition_id")]
        public long MeasurementConditionId { get; set; }

        private IBelongsTo<IMeasurementConditionsConfig> measurementCondition;

        [Related("measurement_condition_id")]
        public IBelongsTo<IMeasurementConditionsConfig> MeasurementCondition =>
            measurementCondition ??
            (measurementCondition = new BelongsTo<IMeasurementConditionsConfig>("MeasurementConditionId", this));

        [ServiceField("reference_id")]
        public long ReferenceId { get; set; }

        private IBelongsTo<IReference> reference;

        [Related("reference_id")]
        public IBelongsTo<IReference> Reference
            => reference ?? (reference = new BelongsTo<IReference>("ReferenceId", this));

        [ServiceField("client_reference_id")]
        public long ClientReferenceId { get; set; }

        private IBelongsTo<IReference> clientReference;

        [Related("client_reference_id")]
        public IBelongsTo<IReference> ClientReference
            => clientReference ?? (clientReference = new BelongsTo<IReference>("ClientReferenceId", this));

        [ServiceField("paper_id")]
        public long PaperId { get; set; }

        private IBelongsTo<IPaper> paper;

        [Related("paper_id")]
        public IBelongsTo<IPaper> Paper => paper ?? (paper = new BelongsTo<IPaper>("PaperId", this));

        [ServiceField("ink_set_id")]
        public long InkSetId { get; set; }

        private IBelongsTo<IInkSet> inkSet;

        [Related("ink_set_id")]
        public IBelongsTo<IInkSet> InkSet => inkSet ?? (inkSet = new BelongsTo<IInkSet>("InkSetId", this));

        [ServiceField("color_strip_id")]
        public long ColorStripId { get; set; }

        private IBelongsTo<IColorStrip> colorStrip;

        [Related("color_strip_id")]
        public IBelongsTo<IColorStrip> ColorStrip
            => colorStrip ?? (colorStrip = new BelongsTo<IColorStrip>("ColorStripId", this));

        /// <summary>
        /// Serialized color mapping, including mapping for client colors.
        /// </summary>
        [ServiceField("color_mapping")]
        public string ColorMapping { get; set; }

        /// <summary>
        /// Serialized color sequence.
        /// </summary>
        [ServiceField("color_sequence")]
        public string ColorSequence { get; set; }

        [ServiceField("ink_definitions_serialized")]
        public string InkDefinitionsSerialized { get; set; }

        /// <summary>
        /// Serialized densities.
        /// </summary>
        [ServiceField("default_densities")]
        public string DefaultDensities { get; set; }

        /// <summary>
        /// Dotgain list per spot color in serialized form.
        /// </summary>
        [ServiceField("default_dotgain_lists")]
        public string DefaultDotgainLists { get; set; }

        /// <summary>
        /// Cached result for easy display.
        /// </summary>
        [ServiceField("job_colors")]
        public string JobColors { get; set; }

        // N.B.: Iff ImageName != "", this JobPreset is seen as an Image type JobPreset
        [ServiceField("image_name")]
        public Guid ImageName { get; set; }

        [ServiceField("number_of_panels")]
        public int NumberOfPanels { get; set; }

        // For Image: a measure target's X,Y coordinates
        private IHasManyCollection<IJobPresetTargetPosition> jobPresetTargetPositions;

        [Related("job_preset_id", OnDeleteAction.DeleteRelated)]
        public IHasManyCollection<IJobPresetTargetPosition> JobPresetTargetPositions {
            get {
                return jobPresetTargetPositions ??
                       (jobPresetTargetPositions =
                        new HasManyCollection<IJobPresetTargetPosition>("job_preset_id", this));
            }
            set { jobPresetTargetPositions = value; }
        }

        // PantoneLIVE
        [ServiceField("pantone_live_client_reference_guid")]
        public Guid PantoneLiveClientReferenceGuid { get; set; }

        // For Process Control and Flexibles: color strip index of a disabled patch
        private IHasManyCollection<IJobPresetDisabledPatch> jobPresetDisabledPatches;

        [Related("job_preset_id", OnDeleteAction.DeleteRelated)]
        public IHasManyCollection<IJobPresetDisabledPatch> JobPresetDisabledPatches {
            get {
                return jobPresetDisabledPatches ??
                       (jobPresetDisabledPatches =
                        new HasManyCollection<IJobPresetDisabledPatch>("job_preset_id", this));
            }
            set { jobPresetDisabledPatches = value; }
        }
        #endregion

        public BitmapImage ThumbnailTiny { get; set; }

        public IEnumerable<IJobPresetInkDefinition> GetInkDefinitions() {
            try {
                var result = JobPresetInkDefinitionSerializer.Deserialize(InkDefinitionsSerialized);
                return (IEnumerable<IJobPresetInkDefinition>)result ?? new List<IJobPresetInkDefinition>();
            }
            catch {
                return new List<IJobPresetInkDefinition>();
            }
        }

        public void SetInkDefinitions(IEnumerable<IJobPresetInkDefinition> newDefinitions) {
            var definitions = newDefinitions
                .Select(nd => new JobPresetInkDefinition(nd.SlotIndex,
                                                         nd.SequenceIndex,
                                                         nd.ReferencePatchId,
                                                         nd.PantoneLiveObjectGuid,
                                                         nd.PantoneLiveDependentStandardGuid,
                                                         nd.PantoneLivePaletteGuid,
                                                         nd.DefaultDotgainListId.ToOption(),
                                                         nd.SolidDensity,
                                                         nd.DeltaEToleranceOverride.ToOption(_ => nd.UseDeltaEToleranceOverride),
                                                         nd.Name,
                                                         nd.Note,
                                                         nd.IsSpot));
            InkDefinitionsSerialized = JobPresetInkDefinitionSerializer.Serialize(definitions);
        }
    }
}