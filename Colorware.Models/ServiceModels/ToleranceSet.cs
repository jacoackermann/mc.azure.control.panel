﻿using System;
using System.Diagnostics;
using System.Globalization;

using Colorware.Core.Algorithms.DeltaEShapeApproximation.DeltaEStrategies;
using Colorware.Core.Color;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("tolerance_sets_access")]
    [ServiceModel(ServiceName = "tolerance_sets", ListingName = "tolerance-set")]
    [DebuggerDisplay("ToleranceSet({Id}, {Name})")]
    public class ToleranceSet : BaseModel, IToleranceSet {
        #region Servicefield properties
        [ServiceField("external_guid")]
        public Guid ExternalGuid { get; set; } = Guid.NewGuid();

        [ServiceField("name")]
        public String Name { get; set; }

        [ServiceField("delta_e_method")]
        public DeltaEMethod DeltaEMethod { get; set; }

        public String DeltaEMethodName {
            get {
                switch (DeltaEMethod) {
                    case DeltaEMethod.Cie76:
                        return "CIE76";
                    case DeltaEMethod.Cie94:
                        return "CIE94";
                    case DeltaEMethod.Cie2000:
                        return "Cie2000";
                    case DeltaEMethod.CMC:
                        var l = Math.Round(DeltaCMCL, 1);
                        var c = Math.Round(DeltaCMCC, 1);
                        return String.Format(CultureInfo.InvariantCulture, "CMC ({0}:{1})", l, c);
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public String DeltaEMethodNameShort {
            get {
                switch (DeltaEMethod) {
                    case DeltaEMethod.Cie76:
                        return "76";
                    case DeltaEMethod.Cie94:
                        return "94";
                    case DeltaEMethod.Cie2000:
                        return "00";
                    case DeltaEMethod.CMC:
                        var l = Math.Round(DeltaCMCL, 1);
                        var c = Math.Round(DeltaCMCC, 1);
                        return String.Format(CultureInfo.InvariantCulture, "CMC{0}:{1}", l, c);
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        [ServiceField("delta_cmc_l")]
        public double DeltaCMCL { get; set; }

        [ServiceField("delta_cmc_c")]
        public double DeltaCMCC { get; set; }

        [ServiceField("average_calculated")]
        public bool AverageCalculated { get; set; }

        [ServiceField("average_tolerance", CanBeNull = true, DefaultValue = 0.0)]
        public double AverageTolerance { get; set; }

        [ServiceField("delta_H_calculated")]
        public bool DeltaHCalculated { get; set; }

        [ServiceField("delta_H_tolerence", CanBeNull = true, DefaultValue = 0.0)]
        public double DeltaHTolerance { get; set; }

        [ServiceField("delta_h_primaries_calculated")]
        public bool DeltaHPrimariesCalculated { get; set; }

        [ServiceField("delta_h_primaries_tolerance", CanBeNull = true, DefaultValue = 0.0)]
        public double DeltaHPrimariesTolerance { get; set; }

        [ServiceField("density_calculated")]
        public bool DensityCalculated { get; set; }

        [ServiceField("density_tolerance", CanBeNull = true, DefaultValue = 0.0)]
        public double DensityTolerance { get; set; }

        [ServiceField("density_variation", CanBeNull = true, DefaultValue = 0.0)]
        public double DensityVariation { get; set; }

        [ServiceField("primaries_calculated")]
        public bool PrimariesCalculated { get; set; }

        [ServiceField("primaries_tolerance", CanBeNull = true, DefaultValue = 0.0)]
        public double PrimariesTolerance { get; set; }

        [ServiceField("primaries_variation", CanBeNull = true, DefaultValue = 0.0)]
        public double PrimariesVariation { get; set; }

        [ServiceField("secondaries_calculated")]
        public bool SecondariesCalculated { get; set; }

        [ServiceField("secondaries_tolerance", CanBeNull = true, DefaultValue = 0.0)]
        public double SecondariesTolerance { get; set; }

        [ServiceField("secondaries_variation", CanBeNull = true, DefaultValue = 0.0)]
        public double SecondariesVariation { get; set; }

        [ServiceField("spotcolor_calculated")]
        public bool SpotcolorCalculated { get; set; }

        [ServiceField("spotcolor_tolerance", CanBeNull = true, DefaultValue = 0.0)]
        public double SpotcolorTolerance { get; set; }

        [ServiceField("spotcolor_variation", CanBeNull = true, DefaultValue = 0.0)]
        public double SpotcolorVariation { get; set; }

        [ServiceField("dotgain_calculated")]
        public bool DotgainCalculated { get; set; }

        [ServiceField("dotgain_spread", CanBeNull = true, DefaultValue = 0.0)]
        public double DotgainSpread { get; set; }

        [ServiceField("dotgain_tolerance", CanBeNull = true, DefaultValue = 0.0)]
        public double DotgainTolerance { get; set; }

        [ServiceField("dotgain_variation", CanBeNull = true, DefaultValue = 0.0)]
        public double DotgainVariation { get; set; }

        [ServiceField("dotgain_delta_e_calculated")]
        public bool DotgainDeltaECalculated { get; set; }

        [ServiceField("dotgain_delta_e_tolerance")]
        public double DotgainDeltaETolerance { get; set; }

        [ServiceField("dotgain_delta_e_variation")]
        public double DotgainDeltaEVariation { get; set; }


        [ServiceField("graybalance_calculated")]
        public bool GraybalanceCalculated { get; set; }

        [ServiceField("graybalance_tolerance", CanBeNull = true, DefaultValue = 0.0)]
        public double GraybalanceTolerance { get; set; }

        [ServiceField("graybalance_variation", CanBeNull = true, DefaultValue = 0.0)]
        public double GraybalanceVariation { get; set; }

        [ServiceField("max_calculated")]
        public bool MaxCalculated { get; set; }

        [ServiceField("max_tolerance", CanBeNull = true, DefaultValue = 0.0)]
        public double MaxTolerance { get; set; }

        [ServiceField("paperwhite_calculated")]
        public bool PaperwhiteCalculated { get; set; }

        [ServiceField("paperwhite_tolerance", CanBeNull = true, DefaultValue = 0.0)]
        public double PaperwhiteTolerance { get; set; }

        [ServiceField("scumming_calculated")]
        public bool ScummingCalculated { get; set; }

        [ServiceField("scumming_tolerance")]
        public double ScummingTolerance { get; set; }


        [ServiceField("g7_calculated")]
        public bool G7Calculated { get; set; }

        [ServiceField("g7_weighted_delta_l_average")]
        public double G7WeightedDeltaLAverage { get; set; }

        [ServiceField("g7_weighted_delta_l_peak")]
        public double G7WeightedDeltaLPeak { get; set; }

        [ServiceField("g7_weighted_delta_ch_average")]
        public double G7WeightedDeltaChAverage { get; set; }

        [ServiceField("g7_weighted_delta_ch_peak")]
        public double G7WeightedDeltaChPeak { get; set; }


        [ServiceField("score_for_primaries")]
        public int ScoreForPrimaries { get; set; }

        [ServiceField("score_for_spots")]
        public int ScoreForSpots { get; set; }

        [ServiceField("score_for_dotgains")]
        public int ScoreForDotgains { get; set; }

        [ServiceField("score_for_secondaries")]
        public int ScoreForSecondaries { get; set; }

        [ServiceField("score_for_paperwhite")]
        public int ScoreForPaperwhite { get; set; }

        [ServiceField("score_for_graybalance")]
        public int ScoreForGraybalance { get; set; }

        [ServiceField("score_for_g7")]
        public int ScoreForG7 { get; set; }

        [ServiceField("score_for_other")]
        public int ScoreForOther { get; set; }

        [ServiceField("score_for_max", DefaultValue = 10.0)]
        public int ScoreForMax { get; set; }

        [ServiceField("score_for_average", DefaultValue = 10.0)]
        public int ScoreForAverage { get; set; }


        [ServiceField("score_percentage_for_primaries", DefaultValue = 1.0)]
        public double ScorePercentageForPrimaries { get; set; }

        [ServiceField("score_percentage_for_spots", DefaultValue = 1.0)]
        public double ScorePercentageForSpots { get; set; }

        [ServiceField("score_percentage_for_dotgains", DefaultValue = 1.0)]
        public double ScorePercentageForDotgains { get; set; }

        [ServiceField("score_percentage_for_secondaries", DefaultValue = 1.0)]
        public double ScorePercentageForSecondaries { get; set; }

        [ServiceField("score_percentage_for_paperwhite", DefaultValue = 1.0)]
        public double ScorePercentageForPaperWhite { get; set; }

        [ServiceField("score_percentage_for_graybalance", DefaultValue = 1.0)]
        public double ScorePercentageForGraybalance { get; set; }

        [ServiceField("score_percentage_for_g7", DefaultValue = 1.0)]
        public double ScorePercentageForG7 { get; set; }

        [ServiceField("score_percentage_for_other", DefaultValue = 1.0)]
        public double ScorePercentageForOther { get; set; }

        [ServiceField("score_percentage_for_max", DefaultValue = 1.0)]
        public double ScorePercentageForMax { get; set; }

        [ServiceField("score_percentage_for_average", DefaultValue = 1.0)]
        public double ScorePercentageForAverage { get; set; }

        [ServiceField("opacity_measured", DefaultValue = false)]
        public bool OpacityMeasured { get; set; }

        [ServiceField("opacity_target", DefaultValue = 50.0)]
        public double OpacityTarget { get; set; }

        [ServiceField("score_for_opacity", DefaultValue = 20.0)]
        public double ScoreForOpacity { get; set; }
        #endregion

        #region Relations
        [ServiceField("default_measurement_condition_id")]
        public long DefaultMeasurementConditionId { get; set; }

        private IBelongsTo<IMeasurementConditionsConfig> defaultMeasurementCondition;

        [Related("default_measurement_condition_id")]
        public IBelongsTo<IMeasurementConditionsConfig> DefaultMeasurementCondition {
            get {
                if (defaultMeasurementCondition == null)
                    defaultMeasurementCondition = new BelongsTo<IMeasurementConditionsConfig>(
                        "DefaultMeasurementConditionId",
                        this);
                return defaultMeasurementCondition;
            }
        }

        public bool HasDefaultMeasurementCondition {
            get { return DefaultMeasurementConditionId > 0; }
        }
        #endregion

        #region Properties
        /// <summary>
        /// If InProduction is true, this indicates that we need to use the variation and not
        /// the regular tolerance if it is available.
        /// </summary>
        public bool InProduction { get; set; }
        #endregion

        public ToleranceSet() {
            DeltaCMCL = Lab.CieCmcDefaultForL;
            DeltaCMCC = Lab.CieCmcDefaultForC;
            G7WeightedDeltaLAverage = G7WeightedDeltaChAverage = 1.5;
            G7WeightedDeltaLPeak = G7WeightedDeltaChPeak = 3.0;
            ScoreForPrimaries =
                ScoreForSpots =
                    ScoreForDotgains =
                        ScoreForSecondaries =
                            ScoreForPaperwhite =
                                ScoreForGraybalance =
                                    ScoreForOther =
                                        5;
            ScoreForG7 = 20;
            ScoreForMax =
                ScoreForAverage =
                    10;
            ScorePercentageForPrimaries =
                ScorePercentageForSpots =
                    ScorePercentageForDotgains =
                        ScorePercentageForSecondaries =
                            ScorePercentageForPaperWhite =
                                ScorePercentageForGraybalance =
                                    ScorePercentageForG7 =
                                        ScorePercentageForOther =
                                            ScorePercentageForMax =
                                                ScorePercentageForAverage =
                                                    1.0;
            OpacityTarget = 50;
            ScoreForOpacity = 20;
        }

        public static ToleranceGroups GetDefaultForPatchType(PatchTypes patchType) {
            switch (patchType) {
                case PatchTypes.Undefined:
                    return ToleranceGroups.Undefined;
                case PatchTypes.Solid:
                    return ToleranceGroups.Primaries;
                case PatchTypes.Dotgain:
                case PatchTypes.DotgainOnly:
                    return ToleranceGroups.Dotgain;
                case PatchTypes.Slur:
                    return ToleranceGroups.Undefined;
                case PatchTypes.Balance:
                    return ToleranceGroups.GrayBalance;
                case PatchTypes.Paperwhite:
                    return ToleranceGroups.PaperWhite;
                case PatchTypes.BalanceHighlight:
                    return ToleranceGroups.GrayBalance;
                case PatchTypes.BalanceMidtone:
                    return ToleranceGroups.GrayBalance;
                case PatchTypes.BalanceShadow:
                    return ToleranceGroups.GrayBalance;
                case PatchTypes.Overprint:
                    return ToleranceGroups.Secondaries;
                case PatchTypes.Other:
                    return ToleranceGroups.Max;
                default:
                    throw new ArgumentOutOfRangeException("patchType");
            }
        }

        public bool IsDeltaEToleranceCalculated(ToleranceGroups toleranceGroup) {
            switch (toleranceGroup) {
                case ToleranceGroups.Undefined:
                    return false;
                case ToleranceGroups.Primaries:
                    return PrimariesCalculated;
                case ToleranceGroups.Secondaries:
                    return SecondariesCalculated;
                case ToleranceGroups.Spotcolor:
                    return SpotcolorCalculated;
                case ToleranceGroups.Density:
                    return DensityCalculated;
                case ToleranceGroups.GrayBalance:
                    return GraybalanceCalculated;
                case ToleranceGroups.PaperWhite:
                    return PaperwhiteCalculated;
                case ToleranceGroups.Dotgain:
                    return DotgainDeltaECalculated;
                case ToleranceGroups.Max:
                    return MaxCalculated;
                case ToleranceGroups.Average:
                    return AverageCalculated;
                case ToleranceGroups.Spread:
                    return false;
                default:
                    throw new ArgumentOutOfRangeException("toleranceGroup");
            }
        }

        public bool IsDeltaHToleranceCalculated(ToleranceGroups toleranceGroup) {
            switch (toleranceGroup) {
                case ToleranceGroups.Primaries:
                    return DeltaHPrimariesCalculated;
                case ToleranceGroups.GrayBalance:
                    return DeltaHCalculated;
                default:
                    return false;
            }
        }

        public bool IsDensityCalculated(ToleranceGroups toleranceGroup) {
            switch (toleranceGroup) {
                case ToleranceGroups.Primaries:
                    return DensityCalculated;
                default:
                    return false;
            }
        }

        public bool IsDotgainToleranceCalculated(ToleranceGroups toleranceGroup) {
            switch (toleranceGroup) {
                case ToleranceGroups.Dotgain:
                    return DotgainCalculated;
                default:
                    return false;
            }
        }

        public bool IsMaxToleranceCalculated(ToleranceGroups toleranceGroup) {
            // Don't split on groups as all groups should at least adhere to this.
            return MaxCalculated;
        }

        public bool IsScummingCalculated(ToleranceGroups toleranceGroup) {
            return ScummingCalculated && toleranceGroup == ToleranceGroups.PaperWhite;
        }

        public bool IsG7WeightedDeltaLToleranceCalculated(ToleranceGroups toleranceGroup, bool isKey, bool isThreeColorOverprint) {
            if (!G7Calculated) {
                return false;
            }
            switch (toleranceGroup) {
                case ToleranceGroups.Dotgain when isKey:
                    return true;
                case ToleranceGroups.GrayBalance:
                    return true;
                default:
                    return false;
            }
        }

        public bool IsG7WeightedDeltaChToleranceCalculated(ToleranceGroups toleranceGroup, bool isKey, bool isThreeColorOverprint) {
            if (!G7Calculated) {
                return false;
            }
            switch (toleranceGroup) {
                case ToleranceGroups.Secondaries when isThreeColorOverprint:
                    return true;
                case ToleranceGroups.GrayBalance:
                    return true;
                default:
                    return false;
            }
        }

        public double DeltaEToleranceFor(ToleranceGroups toleranceGroup) {
            switch (toleranceGroup) {
                case ToleranceGroups.Undefined:
                    return 0.0;
                case ToleranceGroups.Primaries:
                    if (InProduction)
                        return PrimariesVariation;
                    return PrimariesTolerance;
                case ToleranceGroups.Dotgain:
                    if (InProduction)
                        return DotgainDeltaEVariation;
                    return DotgainDeltaETolerance;
                case ToleranceGroups.GrayBalance:
                    if (InProduction)
                        return GraybalanceVariation;
                    return GraybalanceTolerance;
                case ToleranceGroups.PaperWhite:
                    return PaperwhiteTolerance;
                case ToleranceGroups.Secondaries:
                    if (InProduction)
                        return SecondariesVariation;
                    return SecondariesTolerance;
                case ToleranceGroups.Max:
                    return MaxTolerance;
                case ToleranceGroups.Spotcolor:
                    if (InProduction)
                        return SpotcolorVariation;
                    return SpotcolorTolerance;
                case ToleranceGroups.Density:
                    return 0.0;
                case ToleranceGroups.Average:
                    return AverageTolerance;
                default:
                    throw new ArgumentOutOfRangeException("toleranceGroup");
            }
        }

        public double DeltaHToleranceFor(ToleranceGroups toleranceGroup) {
            switch (toleranceGroup) {
                case ToleranceGroups.Primaries:
                    return DeltaHPrimariesTolerance;
                case ToleranceGroups.GrayBalance:
                    return DeltaHTolerance;
                default:
                    throw new Exception($"{toleranceGroup} never uses delta H tolerances");
            }
        }

        public double DensityToleranceFor(ToleranceGroups toleranceGroup) {
            switch (toleranceGroup) {
                case ToleranceGroups.Primaries:
                    if (InProduction)
                        return DensityVariation;
                    return DensityTolerance;
                default:
                    throw new Exception($"{toleranceGroup} never uses density tolerances");
            }
        }

        public double DotgainToleranceFor(ToleranceGroups toleranceGroup) {
            switch (toleranceGroup) {
                case ToleranceGroups.Dotgain:
                    if (InProduction)
                        return DotgainVariation;
                    return DotgainTolerance;
                default:
                    throw new Exception($"{toleranceGroup} never uses dotgain tolerances");
            }
        }

        public double MaxToleranceFor(ToleranceGroups toleranceGroup) {
            return MaxTolerance;
        }

        public double ScummingToleranceFor(ToleranceGroups toleranceGroup) {
            if (toleranceGroup == ToleranceGroups.PaperWhite)
                return ScummingTolerance;
            throw new Exception($"{toleranceGroup} never uses scumming tolerances");
        }

        public double G7WeightedDeltaLPeakToleranceFor(ToleranceGroups toleranceGroup, bool isKey, bool isThreeColorOverprint) {
            switch (toleranceGroup) {
                case ToleranceGroups.Dotgain when isKey:
                    return G7WeightedDeltaLPeak;
                case ToleranceGroups.GrayBalance:
                    return G7WeightedDeltaLPeak;
                default:
                    throw new Exception(
                        $"{toleranceGroup}/{isKey}/{isThreeColorOverprint} does not use G7 peak wDl tolerance");
            }
        }
        public double G7WeightedDeltaChPeakToleranceFor(ToleranceGroups toleranceGroup, bool isKey, bool isThreeColorOverprint) {
            switch (toleranceGroup) {
                case ToleranceGroups.Secondaries when isThreeColorOverprint:
                    return G7WeightedDeltaChPeak;
                case ToleranceGroups.GrayBalance:
                    return G7WeightedDeltaChPeak;
                default:
                    throw new Exception(
                        $"{toleranceGroup}/{isKey}/{isThreeColorOverprint} does not use G7 peak wDCh tolerance");
            }
        }

        public double G7WeightedDeltaLAverageToleranceFor(ToleranceGroups toleranceGroup, bool isKey, bool isThreeColorOverprint) {
            switch (toleranceGroup) {
                case ToleranceGroups.Dotgain when isKey:
                    return G7WeightedDeltaLAverage;
                case ToleranceGroups.GrayBalance:
                    return G7WeightedDeltaLAverage;
                default:
                    throw new Exception(
                        $"{toleranceGroup}/{isKey}/{isThreeColorOverprint} does not use G7 peak wDl tolerance");
            }
        }
        public double G7WeightedDeltaChAverageToleranceFor(ToleranceGroups toleranceGroup, bool isKey, bool isThreeColorOverprint) {
            switch (toleranceGroup) {
                case ToleranceGroups.Secondaries when isThreeColorOverprint:
                    return G7WeightedDeltaChAverage;
                case ToleranceGroups.GrayBalance:
                    return G7WeightedDeltaChAverage;
                default:
                    throw new Exception(
                        $"{toleranceGroup}/{isKey}/{isThreeColorOverprint} does not use G7 peak wDCh tolerance");
            }
        }

        public static ToleranceSet CreateMockToleranceSet() {
            return new ToleranceSet {
                Name = "Mock tolerance set",
                DeltaHCalculated = true,
                DeltaHTolerance = 3.0,
                PrimariesCalculated = true,
                PrimariesTolerance = 4.0,
                PrimariesVariation = 3.0,
                SecondariesCalculated = true,
                SecondariesTolerance = 5.0,
                SecondariesVariation = 3.0,
                DeltaHPrimariesCalculated = true,
                DeltaHPrimariesTolerance = 4.0,
                DensityCalculated = true,
                DensityTolerance = 0.6,
                DensityVariation = 0.3,
                DotgainCalculated = true,
                DotgainTolerance = 3.0,
                DotgainVariation = 2.0,
                DotgainDeltaECalculated = true,
                DotgainDeltaETolerance = 5.0,
                DotgainDeltaEVariation = 3.0,
                GraybalanceCalculated = true,
                GraybalanceTolerance = 4.0,
                GraybalanceVariation = 3.0,
                G7Calculated = true,
                G7WeightedDeltaLAverage = 2.5,
                G7WeightedDeltaLPeak = 5.0,
                G7WeightedDeltaChAverage = 3.0,
                G7WeightedDeltaChPeak = 7.0,
                MaxCalculated = true,
                MaxTolerance = 7.0,
                PaperwhiteCalculated = true,
                PaperwhiteTolerance = 2.0,
                ScoreForPrimaries = 5,
                ScoreForDotgains = 5,
                ScoreForSecondaries = 5,
                ScoreForPaperwhite = 5,
                ScoreForGraybalance = 5,
                ScoreForG7 = 20,
                ScoreForOther = 5
            };
        }

        public double GetScorePercentageForType(PatchTypes patchType, bool isSpot) {
            switch (patchType) {
                case PatchTypes.Solid:
                    if (isSpot)
                        return ScorePercentageForSpots;
                    return ScorePercentageForPrimaries;
                case PatchTypes.DotgainOnly:
                case PatchTypes.Dotgain:
                    return ScorePercentageForDotgains;
                case PatchTypes.Paperwhite:
                    return ScorePercentageForPaperWhite;
                case PatchTypes.Balance:
                case PatchTypes.BalanceHighlight:
                case PatchTypes.BalanceMidtone:
                case PatchTypes.BalanceShadow:
                    return ScorePercentageForGraybalance;
                case PatchTypes.Overprint:
                    return ScorePercentageForSecondaries;
                case PatchTypes.Other:
                    return ScorePercentageForOther;
                case PatchTypes.Undefined:
                case PatchTypes.Slur:
                    return 1.0;
                default:
                    throw new ArgumentOutOfRangeException("patchType");
            }
        }

        public double GetScoreForType(PatchTypes patchType, bool isSpot) {
            switch (patchType) {
                case PatchTypes.Solid:
                    if (isSpot)
                        return ScoreForSpots;
                    return ScoreForPrimaries;
                case PatchTypes.Dotgain:
                case PatchTypes.DotgainOnly:
                    return ScoreForDotgains;
                case PatchTypes.Balance:
                case PatchTypes.BalanceHighlight:
                case PatchTypes.BalanceMidtone:
                case PatchTypes.BalanceShadow:
                    return ScoreForGraybalance;
                case PatchTypes.Paperwhite:
                    return ScoreForPaperwhite;
                case PatchTypes.Overprint:
                    return ScoreForSecondaries;
                case PatchTypes.Other:
                    return ScoreForOther;
                case PatchTypes.Slur:
                case PatchTypes.Undefined:
                    return 0;
                default:
                    throw new ArgumentOutOfRangeException("patchType", patchType, null);
            }
        }

        public double DeltaEUsingCurrentMethod(Lab refLab, Lab sampleLab) {
            switch (DeltaEMethod) {
                case DeltaEMethod.Cie76:
                    return refLab.DeltaE76(sampleLab);
                case DeltaEMethod.Cie94:
                    return refLab.DeltaE94(sampleLab);
                case DeltaEMethod.CMC:
                    return refLab.DeltaECMC(sampleLab, DeltaCMCL, DeltaCMCC);
                case DeltaEMethod.Cie2000:
                    return refLab.DeltaE2000(sampleLab);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public IDeltaEStrategy GetDeltaEStrategy() {
            switch (DeltaEMethod) {
                case DeltaEMethod.Cie76:
                    return new Cie76DeltaEStrategy();
                case DeltaEMethod.CMC:
                    return new CieCMCDeltaEStrategy(DeltaCMCL, DeltaCMCC);
                case DeltaEMethod.Cie94:
                    return new Cie94DeltaEStrategy();
                case DeltaEMethod.Cie2000:
                    return new Cie2000DeltaEStrategy();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}