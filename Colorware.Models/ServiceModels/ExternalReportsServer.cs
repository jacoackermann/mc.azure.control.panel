using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.PrintQualityReport;

namespace Colorware.Models.ServiceModels {

    [ServiceModel(ListingName = "external_reports_server", ServiceName = "external_reports_servers")]
    public class ExternalReportsServer : BaseModel, IExternalReportsServer {

        [ServiceField("type_guid")]
        public Guid TypeGuid { get; set; }

        [ServiceField("name")]
        public string Name { get; set; }

        [ServiceField("url")]
        public string Url { get; set; }

        [ServiceField("verification_token")]
        public string VerificationToken { get; set; }

        [ServiceField("username")]
        public string Username { get; set; }

        [ServiceField("password")]
        public string Password { get; set; }

        [ServiceField("extra_field_1")]
        public string ExtraField1 { get; set; }

        [ServiceField("extra_field_2")]
        public string ExtraField2 { get; set; }

        [ServiceField("extra_field_3")]
        public string ExtraField3 { get; set; }

        [ServiceField("extra_field_4")]
        public string ExtraField4 { get; set; }

        [ServiceField("send_enabled")]
        public bool SendEnabled { get; set; }

        [ServiceField("valid_connection")]
        public bool ValidConnection { get; set; }

        [ServiceField("send_all_data_unfiltered")]
        public bool SendAllDataUnfiltered { get; set; }

        public PqrProviderGuid ProviderGuid => PqrProviderGuid.FromGuid(TypeGuid);

        public ExternalReportsServer() {
            // Default for SendEnabled.
            SendEnabled = false;
        }
    }
}