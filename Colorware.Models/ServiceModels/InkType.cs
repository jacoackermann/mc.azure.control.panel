﻿using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [ServiceModel(ServiceName = "ink_types", ListingName = "ink-type")]
    public class InkType : BaseModel, IInkType {
        
        

        [ServiceField("name")]
        public String Name { get; set; }
    }
}