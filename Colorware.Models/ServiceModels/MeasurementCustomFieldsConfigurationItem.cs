﻿using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    /// <summary>
    /// Specifies a single configuration for a custom measurement field.
    /// Currently the configuration has no limit on number of fields, while the fields themselves
    /// are statically defined on the measurement object.
    /// </summary>
    [RemoteClient("measurement_custom_fields_configuration_items_access")]
    [ServiceModel(ServiceName = "measurement_custom_fields_configuration_items",
        ListingName = "measurement-custom-fields-configuration-item", DisplayName = "CW.Data.Job")]
    public class MeasurementCustomFieldsConfigurationItem : BaseModel, IMeasurementCustomFieldsConfigurationItem {
        #region Serialized fields

        [ServiceField("is_enabled")]
        public bool IsEnabled { get; set; }

        [ServiceField("is_multiline")]
        public bool IsMultiline { get; set; }

        [ServiceField("name")]
        public string Name { get; set; }

        [ServiceField("type_of_field")]
        public MeasurementCustomFieldType TypeOfField { get; set; }

        [ServiceField("field_index")]
        public int FieldIndex { get; set; }

        [ServiceField("sort_order")]
        public int SortOrder { get; set; }
        #endregion
    }
}