using System.Diagnostics;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [ServiceModel(ServiceName = "user_profile_entries", ListingName = "user_profile_entry")]
    [DebuggerDisplay("UserProfileEntry({Id}, {IsCustom}, \"{Key}\", \"{Value}\")")]
    public class UserProfileEntry : BaseModel, IUserProfileEntry {
        [ServiceField("key")]
        public string Key { get; set; }

        [ServiceField("value")]
        public string Value { get; set; }

        [ServiceField("is_custom")]
        public bool IsCustom { get; set; }

        [ServiceField("user_group_id")]
        public long UserGroupId { get; set; }

        [ServiceField("profile_category")]
        public string Category { get; set; }

        [Related("user_group_id")]
        public IBelongsTo<IUserGroup> UserGroup { get; private set; }

        public UserProfileEntry() {
            UserGroup = new BelongsTo<IUserGroup>("UserGroupId", this);
        }
    }
}
