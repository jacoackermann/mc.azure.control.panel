﻿using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("machine_types_access")]
    [ServiceModel(ServiceName = "machine_types", ListingName = "machine-type")]
    public class MachineType : BaseModel, IMachineType {
        [ServiceField("external_guid")]
        public Guid ExternalGuid { get; set; } = Guid.NewGuid();

        [ServiceField("type_name")]
        public String TypeName { get; set; }

        [ServiceField("follow_from_ok_sheet")]
        public bool FollowFromOkSheet { get; set; }
    }
}