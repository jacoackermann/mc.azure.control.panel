using System;
using System.Diagnostics;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("paper_types_access")]
    [ServiceModel(ServiceName = "paper_types", ListingName = "paper-type")]
    [DebuggerDisplay("PaperType({Id}, {Name})")]
    public class PaperType : BaseModel, IPaperType {
        #region Field properties
        [ServiceField("external_guid")]
        public Guid ExternalGuid { get; set; } = Guid.NewGuid();

        [ServiceField("name")]
        public String Name { get; set; }

        [ServiceField("description")]
        public String Description { get; set; }

        [ServiceField("number")]
        public int Number { get; set; }
        #endregion

        #region Relations
        private IHasManyCollection<IPaper> papers;

        public IHasManyCollection<IPaper> Papers {
            get { return papers ?? (papers = new HasManyCollection<IPaper>("paper_type_id", this)); }
        }

        private IHasManyCollection<IReference> references;

        [Related("paper_type_id")]
        public IHasManyCollection<IReference> References {
            get { return references ?? (references = new HasManyCollection<IReference>("paper_type_id", this)); }
            set { references = value; }
        }
        #endregion
    }
}