﻿using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [ServiceModel(ServiceName = "job_preset_disabled_patches", ListingName = "job-preset-disabled-patch")]
    public class JobPresetDisabledPatch : BaseModel, IJobPresetDisabledPatch {

        #region Fields
        [ServiceField("job_preset_id")]
        public long JobPresetId { get; set; }

        [ServiceField("job_strip_patch_index")]
        public int JobStripPatchIndex { get; set; }

        public JobPresetDisabledPatch() {
            Id = 0;
            JobStripPatchIndex = -1;
        }

        public JobPresetDisabledPatch(int index) {
            JobStripPatchIndex = index;
        }
        #endregion
    }
}