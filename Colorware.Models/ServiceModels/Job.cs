﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using Colorware.Core;
using Colorware.Core.Config;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;
using Colorware.Core.Logging;
using Colorware.Core.StatusReporting;
using Colorware.Models.ColorSequence;
using Colorware.Models.StripCompare;
using Colorware.Models.StripCompareInterface.Caching;
using Colorware.Models.StripCompareInterface.Scoring;

using JetBrains.Annotations;

using ReactiveUI;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("jobs_access", CanUpdateOwned = true)]
    [ServiceModel(ServiceName = "jobs", ListingName = "job", DisplayName = "CW.Data.Job")]
    public class Job : BaseModel, IJob {
        private const string UseSavedJobSettingsOnJobCreationKey = "Options.UseSavedJobSettingsOnJobCreation.Enabled";

        #region Fields
        [ServiceField("external_guid")]
        public Guid ExternalGuid { get; set; } = System.Guid.NewGuid();

        [ServiceField("other_side_id", CanBeNull = true, DefaultValue = -1)]
        public long OtherSideId { get; set; }

        [ServiceField("name")]
        public String Name { get; set; }

        [ServiceField("number")]
        public String Number { get; set; }

        [ServiceField("description")]
        public String Description { get; set; }

        /// <summary>
        /// Unique identifier for job. Currently used in CDS communication.
        /// </summary>
        [ServiceField("guid")]
        public string Guid { get; set; }

        /// <summary>
        /// SCHAWK: EPQ/CDS field, stores brand for CDS communication.
        /// </summary>
        [ServiceField("brand")]
        public String Brand { get; set; }

        /// <summary>
        /// SCHAWK: EPQ/CDS field, stores printer name for CDS communication.
        /// </summary>
        [ServiceField("printer_name")]
        public String PrinterName { get; set; }

        /// <summary>
        /// SCHAWK: EPQ/CDS field, stores printer location for CDS communication.
        /// </summary>
        [ServiceField("printer_location")]
        public String PrinterLocation { get; set; }

        /// <summary>
        /// SCHAWK: EPQ/CDS field, stores scoring set id for sending in transactions
        /// </summary>
        [ServiceField("cds_scoring_set_id")]
        public long CdsScoringSetId { get; set; }

        /// <summary>
        /// SCHAWK: EPQ/CDS field, stores scoring set name for sending in transactions
        /// </summary>
        [ServiceField("cds_scoring_set_name")]
        public string CdsScoringSetName { get; set; }

        private double cachedScore;

        /// <summary>
        /// This is calculated by a stored procedure whenever a measurement is
        /// added/removed/updated.
        /// </summary>
        [ServiceField("cached_score", CanBeNull = true, DefaultValue = -1.0)]
        public double CachedScore {
            get => cachedScore;
            set {
                this.RaiseAndSetIfChanged(ref cachedScore, value);
                this.RaisePropertyChanged(nameof(CachedScoreString));
            }
        }

        [ServiceField("sheet_number")]
        public int SheetNumber { get; set; }

        [ServiceField("sheet_side")]
        public SheetSides SheetSide { get; set; }

        #region Special setters for Sheet side
        public bool IsNa {
            get { return (int)SheetSide == (int)SheetSides.NA; }
            set {
                if (value)
                    SheetSide = SheetSides.NA;
                //else
                //SheetSide = SheetSides.Top;
            }
        }

        public bool IsTop {
            get { return (int)SheetSide == (int)SheetSides.Top; }
            set {
                if (value)
                    SheetSide = SheetSides.Top;
                //else
                //SheetSide = SheetSides.NA;
            }
        }

        public bool IsBottom {
            get { return (int)SheetSide == (int)SheetSides.Bottom; }
            set {
                if (value)
                    SheetSide = SheetSides.Bottom;
                //else
                //SheetSide = SheetSides.NA;
            }
        }

        public String SheetSideName {
            get {
                if (IsTop)
                    return "Top";
                if (IsBottom)
                    return "Bottom";
                return "N/A";
            }
        }

        public bool HasSheetSides => !IsNa;

        public bool HasOtherSideId => OtherSideId > 0;
        #endregion

        [ServiceField("last_date")]
        public DateTime LastDate { get; set; }

        // Note that this denotes the presses color sequence.
        private string sequence;

        [ServiceField("sequence")]
        public String Sequence {
            get { return sequence; }
            set {
                sequence = value;
                sequenceArray = null;
            }
        }

        [ServiceField("measurement_condition_id")]
        public long MeasurementConditionId { get; set; }

        /// <summary>
        /// Semi custom field to indicate what kind of job this is.
        /// Values below 1024 are currently reserved for use by Colorware.
        /// </summary>
        [ServiceField("job_type")]
        public int JobType { get; set; }

        /// <summary>
        /// Denotes whether this job is a flexo 'wide flexo film' job which uses different
        /// scoring preferences.
        /// </summary>
        [ServiceField("is_wide_flexo")]
        public bool IsWideFlexo { get; set; }

        [ServiceField("job_strip_current_start_index")]
        public int JobStripCurrentStartIndex { get; set; }

        [ServiceField("job_strip_current_end_index")]
        public int JobStripCurrentEndIndex { get; set; }

        [ServiceField("ink_zone_locks")]
        public String SerializedInkZoneLocks { get; set; }

        [ServiceField("image_name")]
        public Guid ImageName { get; set; }

        [ServiceField("piece_of_art")]
        public String PieceOfArt { get; set; }

        [ServiceField("use_automatic_density_adjustment")]
        public bool UseAutomaticDensityAdjustment { get; set; }

        private InkZoneLockSet inkZoneLockSet;

        public InkZoneLockSet InkZoneLockSet {
            get {
                if (String.IsNullOrEmpty(SerializedInkZoneLocks))
                    return null;

                return inkZoneLockSet ?? (inkZoneLockSet = InkZoneLockDeserializer.Deserialize(SerializedInkZoneLocks));
            }
            set {
                if (value == null) {
                    SerializedInkZoneLocks = String.Empty;
                    return;
                }

                SerializedInkZoneLocks = InkZoneLockSerializer.Serialize(value);
                inkZoneLockSet = value;
            }
        }
        #endregion

        #region Relations
        [ServiceField("tolerance_set_id")]
        public long ToleranceSetId { get; set; }

        [Related("tolerance_set_id")]
        public IBelongsTo<IToleranceSet> ToleranceSet { get; private set; }

        [ServiceField("color_strip_id")]
        public long ColorStripId { get; set; }

        [Related("color_strip_id")]
        public IBelongsTo<IColorStrip> ColorStrip { get; private set; }

        [Obsolete("This gives a false sense of security (ColorStrip can be missing in DB)")]
        public bool HasColorStrip => ColorStripId > 0;

        [ServiceField("reference_id")]
        public long ReferenceId { get; set; }

        [Related("reference_id")]
        public IBelongsTo<IReference> Reference { get; private set; }

        [ServiceField("user_group_id")]
        public long UserGroupId { get; set; }

        [Related("user_group_id")]
        public IBelongsTo<IUserGroup> UserGroup { get; private set; }

        [ServiceField("client_reference_id")]
        public long ClientReferenceId { get; set; }

        [Related("client_reference_id")]
        public IBelongsTo<IReference> ClientReference { get; private set; }

        [ServiceField("job_strip_id")]
        public long JobStripId { get; set; }

        [Related("job_strip_id", OnDeleteAction.DeleteRelated)]
        public IBelongsTo<IJobStrip> JobStrip { get; private set; }

        [ServiceField("ok_sheet_id")]
        public long OkSheetId { get; set; }

        [Obsolete("This gives a false sense of security (OkSheet can be missing in DB)")]
        public bool HasOkSheet => OkSheetId > 0;

        [Related("ok_sheet_id")]
        public IBelongsTo<IMeasurement> OkSheet { get; private set; }

        [ServiceField("is_production_mode")]
        public bool IsProductionMode { get; set; }

        [ServiceField("machine_id")]
        public long MachineId { get; set; }

        [Related("machine_id")]
        public IBelongsTo<IMachine> Machine { get; private set; }

        [ServiceField("paper_id")]
        public long PaperId { get; set; }

        [Related("paper_id")]
        public IBelongsTo<IPaper> Paper { get; private set; }

        [ServiceField("ink_set_id")]
        public long InkSetId { get; set; }

        [Related("ink_set_id")]
        public IBelongsTo<IInkSet> InkSet { get; private set; }

        [ServiceField("measurement_type_id")]
        public MeasurementTypes MeasurementTypeId { get; set; }

        [ServiceField("paper_white_sample_id")]
        public long PaperWhiteSampleId { get; set; }

        [ServiceField("client_id")]
        public long ClientId { get; set; }

        /// <summary>
        /// User that created the job.
        /// </summary>
        [ServiceField("user_id")]
        public long UserId { get; set; }

        /// <summary>
        /// Company that the jobs belongs to. Remote users will only be able to see the jobs
        /// for their own company.
        /// 
        /// If this is 0 there was no company available at the time of creation/migration.
        /// </summary>
        [ServiceField("print_location_company_id")]
        public long PrintLocationCompanyId { get; set; }

        private IBelongsTo<ICompany> printLocationCompany;

        [Related("print_location_company_id")]
        public IBelongsTo<ICompany> PrintLocationCompany =>
            printLocationCompany ??
            (printLocationCompany = new BelongsTo<ICompany>(nameof(PrintLocationCompanyId), this));

        /// <summary>
        /// Company that created the job. Remote users will only be able to see the jobs
        /// for their own company.
        /// 
        /// If this is 0 there was no company available at the time of creation/migration.
        /// </summary>
        [ServiceField("creator_company_id")]
        public long CreatorCompanyId { get; set; }

        private IBelongsTo<ICompany> creatorCompany;

        [Related("creator_company_id")]
        public IBelongsTo<ICompany> CreatorCompany =>
            creatorCompany ??
            (creatorCompany = new BelongsTo<ICompany>(nameof(CreatorCompanyId), this));

        [ServiceField("custom_field_data_1")]
        public String CustomFieldData1 { get; set; }

        [ServiceField("custom_field_data_2")]
        public String CustomFieldData2 { get; set; }

        [ServiceField("custom_field_data_3")]
        public String CustomFieldData3 { get; set; }

        [ServiceField("custom_field_data_4")]
        public String CustomFieldData4 { get; set; }

        [ServiceField("custom_field_data_5")]
        public String CustomFieldData5 { get; set; }

        [ServiceField("custom_field_data_6")]
        public String CustomFieldData6 { get; set; }

        private IBelongsTo<ISample> paperWhiteSample;

        [Related("paper_white_sample_id", OnDeleteAction.DeleteRelated)]
        public IBelongsTo<ISample> PaperWhiteSample {
            get { return paperWhiteSample ?? (paperWhiteSample = new BelongsTo<ISample>("PaperWhiteSampleId", this)); }
            set { paperWhiteSample = value; }
        }

        private IHasManyCollection<IMeasurement> measurements;

        [Related("job_id", OnDeleteAction.DeleteRelated)]
        public IHasManyCollection<IMeasurement> Measurements {
            get { return measurements ?? (measurements = new HasManyCollection<IMeasurement>("job_id", this)); }
            set { measurements = value; }
        }

        private IBelongsTo<IClient> client;

        [Related("client_id")]
        public IBelongsTo<IClient> Client => client ?? (client = new BelongsTo<IClient>("ClientId", this));

        private IBelongsTo<IJob> otherSide;

        [Related("other_side_id")]
        public IBelongsTo<IJob> OtherSide {
            get { return otherSide ?? (otherSide = new BelongsTo<IJob>("OtherSideId", this)); }
            set { otherSide = value; }
        }

        private IBelongsTo<IDatabaseUser> user;

        /// <summary>
        /// User that created the job.
        /// </summary>
        [Related("user_id")]
        public IBelongsTo<IDatabaseUser> User => user ?? (user = new BelongsTo<IDatabaseUser>("UserId", this));

        private IHasManyCollection<ICdsInterfaceElement> cdsInterfaceElements;

        [Related(ParentKeyName = "job_id", OnDelete = OnDeleteAction.DeleteRelated)]
        public IHasManyCollection<ICdsInterfaceElement> CdsInterfaceElements =>
            cdsInterfaceElements ??
            (cdsInterfaceElements = new HasManyCollection<ICdsInterfaceElement>("job_id", this));

        private IBelongsTo<IMeasurementConditionsConfig> measurementCondition;

        [Related(ParentKeyName = "measurement_condition_id", OnDelete = OnDeleteAction.RunAction,
            Action = "onDeleteMeasurementCondition")]
        public IBelongsTo<IMeasurementConditionsConfig> MeasurementCondition {
            get {
                return measurementCondition ??
                       (measurementCondition =
                            new BelongsTo<IMeasurementConditionsConfig>("MeasurementConditionId", this));
            }
            set { measurementCondition = value; }
        }

        /// <summary>
        /// Only delete MeasurementCondition if it isn't a standard measurement condition
        /// </summary>
        [System.Reflection.ObfuscationAttribute(Feature = "all", Exclude = true), UsedImplicitly]
        private static async Task onDeleteMeasurementCondition(Type type, long id) {
            // TODO: DEADLOCK?
            var mc = await BaseModel.FindIncludingDeletedAsync<IMeasurementConditionsConfig>(id);
            if (mc != null && !mc.IsStandard)
                await mc.DeleteAsync();
        }

        // ReSharper restore UnusedMember.Local

        public String CachedScoreString {
            get {
                if (CachedScore < 0)
                    return "--";
                else {
                    if (JobType == (int)JobTypes.Flexo)
                        return CachedScore.ToString("0.00", CultureInfo.InvariantCulture);
                    else
                        return Math.Round(CachedScore, 0) + "%";
                }
            }
        }

        private IHasManyCollection<IJobInkInfo> jobinkinfos;

        [Related("job_id", OnDeleteAction.DeleteRelated)]
        public IHasManyCollection<IJobInkInfo> JobInkInfos {
            get { return jobinkinfos ?? (jobinkinfos = new HasManyCollection<IJobInkInfo>("job_id", this)); }
            set { jobinkinfos = value; }
        }
        #endregion

        #region Helpers
        public bool HasSeparatePaperWhite => PaperWhiteSampleId > 0;

        private int[] sequenceArray;

        public int[] SequenceArray {
            [NotNull]
            get {
                if (String.IsNullOrEmpty(Sequence))
                    return new int[0];

                return sequenceArray ?? (sequenceArray = PressColorSequence.DeserializeFromStringFlat(Sequence));
            }
        }

        public bool ShouldUseOkSheet(IMeasurement measurement) {
            Debug.Assert(measurement != null);
            return ShouldUseOkSheet(measurement.Id);
        }

        public bool ShouldUseOkSheet(long measurementId) {
            // Kind of dangerous optimization. Use ok sheet if ok sheet present and
            // if the measurement was created after the measurement which represents the ok sheet.
            // This is currently checked by looking at the id's as the later measurements should have
            // a higher id.
            return HasOkSheet && measurementId >= OkSheetId;
        }

        public bool AllowMeasurements { get; set; }

        // Convenience properties used in job list

        /// <summary>
        /// Create a new GUID string.
        /// </summary>
        /// <returns></returns>
        private static string generateGuid() {
            var guid = System.Guid.NewGuid().ToString("N");
            return guid;
        }

        /// <summary>
        /// Helper function to initialize the <see cref="Guid" /> field if it is not yet set.
        /// If it is set, nothing changes. This method does not save out the job to the database, so just calling this method
        /// does not cause the value to persist.
        /// </summary>
        /// <returns><c>true</c>, if the field was initialized. <c>false</c>, if the field already had a value.</returns>
        public bool InitGuidFieldIfUnset() {
            if (string.IsNullOrEmpty(Guid)) {
                Guid = generateGuid();
                return true;
            }

            return false;
        }
        #endregion

        public Job() {
            // TODO: Create these on demand like PaperWhiteSample.
            ColorStrip = new BelongsTo<IColorStrip>("ColorStripId", this);
            Reference = new BelongsTo<IReference>("ReferenceId", this);
            ClientReference = new BelongsTo<IReference>("ClientReferenceId", this);
            ToleranceSet = new BelongsTo<IToleranceSet>("ToleranceSetId", this);
            Machine = new BelongsTo<IMachine>("MachineId", this);
            Paper = new BelongsTo<IPaper>("PaperId", this);
            InkSet = new BelongsTo<IInkSet>("InkSetId", this);
            JobStrip = new BelongsTo<IJobStrip>("JobStripId", this);
            UserGroup = new BelongsTo<IUserGroup>("UserGroupId", this);
            OkSheet = new BelongsTo<IMeasurement>("OkSheetId", this);

            AllowMeasurements = true;
            LastDate = DateTime.Now;
        }

        /// <summary>
        /// Create a new Job instance with default settings from the configuration.
        /// </summary>
        /// <returns>A new (unsaved) job instance.</returns>
        public static Job CreateDefault() {
            var config = Config.DefaultConfig;
            if (!config.GetBool(UseSavedJobSettingsOnJobCreationKey, true)) {
                return new Job {
                    Guid = generateGuid(),
                    MachineId = config.GetInt("Defaults.Job.MachineId", 1),
                    LastDate = DateTime.Now
                };
            }

            var job = new Job {
                Name = config.Get("Defaults.Job.Name", "-"),
                Number = config.Get("Defaults.Job.Number", "090"),
                Description = config.Get("Defaults.Job.Description", "-"),
                Brand = config.Get("Defaults.Job.Brand", ""),
                PrinterLocation = config.Get("Defaults.Job.PrinterLocation", ""),
                PrinterName = config.Get("Defaults.Job.PrinterName", ""),
                Guid = generateGuid(),
                IsProductionMode = false,
                IsWideFlexo = config.GetBool("Defaults.Job.IsWideFlexo", false),
                ColorStripId = config.GetInt("Defaults.Job.ColorStripId", 2),
                InkSetId = config.GetInt("Defaults.Job.InkSetId", 1),
                MachineId = config.GetInt("Defaults.Job.MachineId", 1),
                MeasurementConditionId = config.GetInt("Defaults.Job.MeasurementConditionId", 0),
                ReferenceId = config.GetInt("Defaults.Job.ReferenceId", 4),
                ClientReferenceId = config.GetInt("Defaults.Job.ClientReferenceId", 0),
                ToleranceSetId = config.GetInt("Defaults.Job.ToleranceSetId", 0),
                PaperId = config.GetInt("Defaults.Job.PaperId", 1),
                LastDate = DateTime.Now
            };
            try {
                job.SheetSide = (SheetSides)config.GetInt("Defaults.Job.SheetSide", (int)SheetSides.NA);
            }
            catch {
                job.SheetSide = SheetSides.NA;
            }

            return job;
        }

        private const string BaseCustomDataKey = "Defaults.Job.Custom.";

        public void InitDefaultCustomData(IClient forClient) {
            var config = Config.DefaultConfig;
            var baseClientKey = BaseCustomDataKey + forClient.Id + ".";
            if (forClient.HasCustomFieldName1)
                CustomFieldData1 = config.Get(baseClientKey + "Field1", "");
            if (forClient.HasCustomFieldName2)
                CustomFieldData2 = config.Get(baseClientKey + "Field2", "");
            if (forClient.HasCustomFieldName3)
                CustomFieldData3 = config.Get(baseClientKey + "Field3", "");
            if (forClient.HasCustomFieldName4)
                CustomFieldData4 = config.Get(baseClientKey + "Field4", "");
            if (forClient.HasCustomFieldName5)
                CustomFieldData5 = config.Get(baseClientKey + "Field5", "");
            if (forClient.HasCustomFieldName6)
                CustomFieldData6 = config.Get(baseClientKey + "Field6", "");
        }

        public void SaveDefaultCustomData(IClient forClient) {
            var config = Config.DefaultConfig;
            var baseClientKey = BaseCustomDataKey + forClient.Id + ".";
            if (forClient.HasCustomFieldName1)
                config.Set(baseClientKey + "Field1", CustomFieldData1);
            if (forClient.HasCustomFieldName2)
                config.Set(baseClientKey + "Field2", CustomFieldData2);
            if (forClient.HasCustomFieldName3)
                config.Set(baseClientKey + "Field3", CustomFieldData3);
            if (forClient.HasCustomFieldName4)
                config.Set(baseClientKey + "Field4", CustomFieldData4);
            if (forClient.HasCustomFieldName5)
                config.Set(baseClientKey + "Field5", CustomFieldData5);
            if (forClient.HasCustomFieldName6)
                config.Set(baseClientKey + "Field6", CustomFieldData6);
        }

        /// <summary>
        /// Saves the values of the passed in jobs as defaults to the configuration file.
        /// </summary>
        public void SaveDefaults() {
            var config = Config.DefaultConfig;
            config.Set("Defaults.Job.Name", Name);
            config.Set("Defaults.Job.Number", Number);
            config.Set("Defaults.Job.Description", Description);
            config.Set("Defaults.Job.Brand", Brand);
            config.Set("Defaults.Job.PrinterLocation", PrinterLocation);
            config.Set("Defaults.Job.PrinterName", PrinterName);
            config.SetBool("Defaults.Job.IsWideFlexo", IsWideFlexo);
            config.SetLong("Defaults.Job.ColorStripId", ColorStripId);
            config.SetLong("Defaults.Job.InkSetId", InkSetId);
            config.SetLong("Defaults.Job.MachineId", MachineId);
            config.SetLong("Defaults.Job.ToleranceSetId", ToleranceSetId);
            config.SetLong("Defaults.Job.MeasurementConditionId", MeasurementConditionId);
            config.SetLong("Defaults.Job.ReferenceId", ReferenceId);
            config.SetLong("Defaults.Job.ClientReferenceId", ClientReferenceId);
            config.SetLong("Defaults.Job.PaperId", PaperId);
            config.SetLong("Defaults.Job.SheetSide", (int)SheetSide);
        }

        public IStripCompareScoreFactory GetCompareScoreFactory() {
            return new StripCompareScoreFactory((JobTypes)JobType);
        }

        /// <summary>
        /// Create a new (unsaved) measurement.
        /// Special attention has to be given to the flow change. If the job is set to
        /// production mode, the new measurement will have an accompanying flow change set.
        /// The current flow change of the job is also updated, but the job will
        /// not be saved automatically! NOTE: Do not forget to save the job if the measurement is saved!
        /// </summary>
        /// <returns>A new measurement.</returns>
        public IMeasurement CreateMeasurement() {
            if (NewInstance)
                throw new InvalidOperationException("Can only create measurements for existing (saved) jobs");
            var measurement = new Measurement {
                CreatedAt = DateTime.Now,
                MeasurementType = MeasurementTypes.Production,
                IsWet = true,
                JobId = Id,
                JobStripId = JobStripId,
                Name = Name,
                ReferenceId = ReferenceId,
                IsProductionMode = IsProductionMode,
                OkSheet = false,
                InkZoneLockSet = InkZoneLockSet
            };
            return measurement;
        }

        public static Job CreateMockJob() {
            return new Job {
                Name = "Mock job",
                Description = "Mock job",
                SheetSide = (int)SheetSides.NA,
                SheetNumber = 0,
                Number = "010213123"
            };
        }

        public async Task RecalculateAllCachedResultsAsync(IProgressReport progressReport) {
            var incomingMeasurements = await Measurements.GetAsync();

            var first = incomingMeasurements.OrderBy(m => m.CreatedAt).FirstOrDefault();

            await RecalculateCachedResultsAsync(first, progressReport);
        }

        private List<IMeasurement> resetFlowChangeSequences(IEnumerable<IMeasurement> measurementsToUpdate) {
            var sorted = measurementsToUpdate.OrderBy(m => m.CreatedAt).ToList();
            var currentFlow = -1;
            foreach (var measurement in sorted) {
                if (measurement.IsProductionMode) {
                    currentFlow++;
                    measurement.FlowChange = currentFlow;
                }
                else {
                    measurement.FlowChange = -1;
                }
            }

            return sorted;
        }


        private async Task recalculateCachedResultsForMeasurementAsync(StripCompareLoader loader,
                                                                       IMeasurement measurementToUpdate) {
            var result = await loader.CalculateAsync(this, measurementToUpdate, GetCompareScoreFactory());
            if (result == null)
                return; // When the result could not be calculated, maybe this should be handled in a different manner.
            measurementToUpdate.HasCachedScore = true;
            measurementToUpdate.CachedScore = result.CompareScore.FinalScore;
            await CachedCompareResultSaver.SaveAsync(result);
        }

        public async Task ToggleOkSheetAsync(IMeasurement newMeasurement, IProgressReport progressReport) {
            if (newMeasurement == null) throw new ArgumentNullException(nameof(newMeasurement));

            IMeasurement oldMeasurement;
            if (HasOkSheet) {
                // Already has OK sheet, so disable it first.
                oldMeasurement = await FindIncludingDeletedAsync<Measurement>(OkSheetId);

                OkSheetId = -1;
                await this.SaveAsync();

                if (oldMeasurement != null) {
                    oldMeasurement.OkSheet = false;
                    await oldMeasurement.SaveAsync();
                }
            }
            else {
                // Set new OK sheet.
                OkSheetId = newMeasurement.Id;
                newMeasurement.OkSheet = true;
                await newMeasurement.SaveAsync();
                oldMeasurement = newMeasurement;

                if (UserProfileDefinitions.Process.GoToProductionAfterSettingOkSheet.Value)
                    IsProductionMode = true;

                await this.SaveAsync();

                // Check if this is the last measurement. If it is, we transfer it to production mode.
                var lastMeasurement =
                    (await Measurements.GetAsync()).OrderByDescending(m => m.CreatedAt).FirstOrDefault();
                if (lastMeasurement != null && lastMeasurement.Id == newMeasurement.Id) {
                    newMeasurement.IsProductionMode = true;
                    await newMeasurement.SaveAsync();
                }
            }

            await RecalculateCachedResultsAsync(oldMeasurement, progressReport);
        }

        public async Task RecalculateCachedResultsAsync(IMeasurement fromMeasurement, IProgressReport progressReport) {
            LogManager.GetLogger(GetType()).Info("Updating results for measurement: " +
                                                 (fromMeasurement?.Id.ToString(CultureInfo.InvariantCulture) ??
                                                  "null"));

            await Measurements.GetAsync(true);

            var toUpdate = resetFlowChangeSequences(Measurements);

            if (fromMeasurement != null) {
                // Only update newer measurements.
                toUpdate =
                    Measurements.Where(m => m.CreatedAt >= fromMeasurement.CreatedAt || m.Id >= fromMeasurement.Id)
                                .ToList();
            }

            var loader = new StripCompareLoader();
            loader.SetOptions(StripCompareLoaderOptions.NoChromaTrack);

            if (progressReport != null)
                progressReport.Max = toUpdate.Count;

            for (int i = 0; i < toUpdate.Count; i++) {
                await recalculateCachedResultsForMeasurementAsync(loader, toUpdate[i]);

                if (progressReport != null)
                    progressReport.Progress = i + 1;
            }

            await SaveAllAsync(Measurements.ToList());
        }

        public async Task CalculateAverageJobScoreAsync(Action<bool, int> callback) {
            if (callback == null) throw new ArgumentNullException(nameof(callback));

            var measurements = await Measurements.GetAsync();
            var productionMeasurements =
                measurements.Where(m => m.IsProductionMode && m.HasCachedScore && !m.OkSheet).ToList();

            if (!productionMeasurements.Any()) {
                callback(false, 0);
            }
            else {
                var average = productionMeasurements.Average(m => m.CachedScore);
                callback(true, (int)Math.Round(average, 0));
            }
        }

        public async Task ReloadCachedScore() {
            var job = await FindIncludingDeletedAsync<IJob>(Id);
            CachedScore = job?.CachedScore ?? 0;
        }
    }
}