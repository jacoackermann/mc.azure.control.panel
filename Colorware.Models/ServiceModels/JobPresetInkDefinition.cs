﻿using System;
using System.Diagnostics;

using Colorware.Core.Data.Models;
using Colorware.Core.Functional.Option;

namespace Colorware.Models.ServiceModels {
    [DebuggerDisplay(
        "JobPresetInkDefinition(SlotIndex={SlotIndex}, SequenceIndex={SequenceIndex}, ReferencePatchId={ReferencePatchId}, Name={Name}"
    )]
    public class JobPresetInkDefinition : IJobPresetInkDefinition {
        [Obsolete("Use for serialization/deserialization purposes only")]
        public JobPresetInkDefinition() { }

        public JobPresetInkDefinition(int slotIndex,
                                      int sequenceIndex,
                                      long referencePatchId,
                                      string pantoneLiveObjectGuid,
                                      string pantoneLiveDependentStandardGuid,
                                      string pantoneLivePaletteGuid,
                                      Option<long> defaultDotgainListId,
                                      double solidDensity,
                                      Option<double> deltaEToleranceOverride,
                                      string name,
                                      string note,
                                      bool isSpot) {
            SlotIndex = slotIndex;
            SequenceIndex = sequenceIndex;
            ReferencePatchId = referencePatchId;
            PantoneLiveObjectGuid = pantoneLiveObjectGuid;
            PantoneLiveDependentStandardGuid = pantoneLiveDependentStandardGuid;
            PantoneLivePaletteGuid = pantoneLivePaletteGuid;
            DefaultDotgainListId = defaultDotgainListId.OrElseDefault();
            SolidDensity = solidDensity;
            UseDeltaEToleranceOverride = deltaEToleranceOverride.HasValue;
            DeltaEToleranceOverride = deltaEToleranceOverride.OrElse(0);
            Name = name;
            Note = note;
            IsSpot = isSpot;
        }

        public long DefaultDotgainListId { get; set; }
        public double DeltaEToleranceOverride { get; set; }
        public bool IsSpot { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public string PantoneLiveDependentStandardGuid { get; set; }
        public string PantoneLiveObjectGuid { get; set; }
        public string PantoneLivePaletteGuid { get; set; }
        public long ReferencePatchId { get; set; }
        public int SequenceIndex { get; set; }
        public int SlotIndex { get; set; }
        public double SolidDensity { get; set; }
        public bool UseDeltaEToleranceOverride { get; set; }
    }
}