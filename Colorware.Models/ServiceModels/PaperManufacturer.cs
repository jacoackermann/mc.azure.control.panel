﻿using System;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;

namespace Colorware.Models.ServiceModels {
    [ServiceModel(ListingName = "paper-manufacturer", ServiceName = "paper_manufacturers")]
    public class PaperManufacturer : BaseModel, IPaperManufacturer {
        #region Serialized fields
        [ServiceField("name")]
        public String Name { get; set; }
        #endregion

        public PaperManufacturer() {
            Name = "NA";
        }
    }
}