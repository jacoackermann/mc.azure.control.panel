﻿using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

using ReactiveUI;

namespace Colorware.Models.ServiceModels {
    [RemoteClient("color_mappings_access")]
    [ServiceModel(ListingName = "color-mapping", ServiceName = "color_mappings")]
    public class ColorMapping : BaseModel, IColorMapping {
        /// <summary>
        /// This is used to store mappings for multiple modules/categories/families etc. in the same table.
        /// E.g: 1 could be for the SVF importer, 2 could be for importer X, 3 could be for module Y
        /// 
        /// Family '0' is reserved and means 'applies to all families'. Useful for simple colors like
        /// Black --> K, Cyan --> C etc.
        /// </summary>
        [ServiceField("family")]
        public ColorMappingFamily Family { get; set; }

        /// <summary>
        /// Specifies the priority of a mapping within a family. Lower is more important.
        /// 
        /// The exact meaning is implementation specific.
        /// </summary>
        [ServiceField("priority")]
        public int Priority {
            get { return priority; }
            set { this.RaiseAndSetIfChanged(ref priority, value); }
        }

        private int priority;
        
        /// <summary>
        /// Name of the color in the foreign/external system/source/file etc. This can be a wildcard,
        /// regular expression, partial name etc. depending on the implementation.
        /// </summary>
        [ServiceField("foreign_name")]
        public string ForeignName { get; set; }

        /// <summary>
        /// Name of the color in the local (Pressview/MC) system/source/file etc. This can be a wildcard,
        /// regular expression, partial name etc. depending on the implementation.
        /// </summary>
        [ServiceField("local_name")]
        public string LocalName { get; set; }
    }
}