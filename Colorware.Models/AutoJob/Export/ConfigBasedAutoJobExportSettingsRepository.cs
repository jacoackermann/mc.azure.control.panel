﻿using System;

using Colorware.Core.Config;

using JetBrains.Annotations;

namespace Colorware.Models.AutoJob.Export {
    [UsedImplicitly]
    public class ConfigBasedAutoJobExportSettingsRepository : IAutoJobExportSettingsRepository {
        private class ConfigKeys {
            internal class AutoExport {
                internal const string JobPath = "AutoExport.JobPath";
                internal const string IsEnabled = "AutoExport.ExportJobs";
            }
        }

        private readonly IConfig config;

        public ConfigBasedAutoJobExportSettingsRepository([NotNull] IConfig config) {
            if (config == null) throw new ArgumentNullException("config");

            this.config = config;
        }

        public string GetJobExportPath() {
            return config.Get(ConfigKeys.AutoExport.JobPath, GlobalConfig.GetDataPath("jobinformation.xml"));
        }

        public void SetJobExportPath(string path) {
            config.Set(ConfigKeys.AutoExport.JobPath, path);
        }

        public bool GetIsAutoJobExportEnabled() {
            return config.GetBool(ConfigKeys.AutoExport.IsEnabled, false);
        }

        public void SetIsAutoJobExportEnabled(bool enabled) {
            config.SetBool(ConfigKeys.AutoExport.IsEnabled, enabled);
        }
    }
}