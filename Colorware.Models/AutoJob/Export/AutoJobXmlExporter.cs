﻿using System.IO;
using System.Xml.Serialization;

using JetBrains.Annotations;

namespace Colorware.Models.AutoJob.Export {
    [UsedImplicitly]
    public class AutoJobXmlExporter {
        public void Export(JobInformationType autoJob, Stream outStream) {
            var serializer = new XmlSerializer(typeof(JobInformationType));
            serializer.Serialize(outStream, autoJob);
        }

        public void Export(JobInformationType autoJob, string path) {
            using (var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write))
                Export(autoJob, fileStream);
        }
    }
}