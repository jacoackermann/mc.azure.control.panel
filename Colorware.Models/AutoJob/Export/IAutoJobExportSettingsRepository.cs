﻿namespace Colorware.Models.AutoJob.Export {
    public interface IAutoJobExportSettingsRepository {
        string GetJobExportPath();
        void SetJobExportPath(string path);

        bool GetIsAutoJobExportEnabled();
        void SetIsAutoJobExportEnabled(bool enabled);
    }
}