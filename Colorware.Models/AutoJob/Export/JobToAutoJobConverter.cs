﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Castle.Core.Logging;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;
using Colorware.Models.ColorSequence;

using JetBrains.Annotations;

namespace Colorware.Models.AutoJob.Export {
    [UsedImplicitly]
    public class JobToAutoJobConverter {
        private readonly ILogger logger;

        public JobToAutoJobConverter([NotNull] ILogger logger) {
            if (logger == null) throw new ArgumentNullException("logger");

            this.logger = logger;
        }

        public async Task<JobInformationType> ConvertJob([NotNull] IJob job) {
            if (job == null) throw new ArgumentNullException("job");

            await Task.WhenAll(
                job.Client.GetAsync(),
                job.ColorStrip.GetAsync(),
                job.InkSet.GetAsync(),
                job.Machine.GetAsync(),
                job.MeasurementCondition.GetAsync(),
                job.Reference.GetAsync(),
                job.ClientReference.GetAsync(),
                job.Paper.GetAsync(),
                job.ToleranceSet.GetAsync()
                );
            if(job.Paper.Item != null)
                await job.Paper.Item.PaperType.GetAsync();

            return new JobInformationType {
                ClientName = job.Client.Item?.Name ?? string.Empty,
                ColorReferences = await sequenceToColorReferences(job),
                ColorbarName = job.ColorStrip.Item?.Name ?? string.Empty,
                InkSetName = job.InkSet.Item?.Name ?? string.Empty,
                JobDescription = job.Description,
                JobName = job.Name,
                JobNumber = job.Number,
                MachineName = job.Machine.Item?.Name ?? string.Empty,
                MeasurementConditionsName =
                    job.MeasurementCondition.Item?.Name ?? string.Empty,
                ProcessReferenceName = job.Reference.Item?.Name ?? string.Empty,
                ScanSide = job.SheetSide.ToPressviewSheetSide(),
                ScanSideSpecified = true,
                SignatureNumber = job.SheetNumber,
                SignatureNumberSpecified = true,
                SpotColorBookName = job.ClientReference.Item?.Name ?? string.Empty,
                SubstrateName = job.Paper.Item?.Name ?? string.Empty,
                PrintingConditionName = job.Paper.Item?.PaperType?.Item?.Name ?? string.Empty,
                ToleranceSetName = job.ToleranceSet.Item?.Name ?? string.Empty,
                Version = "1.0"
            };
        }

        private async Task<ColorReferencesType> sequenceToColorReferences(IJob job) {
            var result = new ColorReferencesType();

            // Generate ColorReferences from serialized run sequence
            var seq = PressColorSequence.DeserializeFromStringFlat(job.Sequence);
            result.ColorReference =
                seq.Where(slot => slot >= 0)
                   .Select(
                       (slot, index) => new ColorReferenceType {ColorbarSlot = slot, RunSequence = seq.Count() - index})
                   .OrderBy(colorRef => colorRef.ColorbarSlot)
                   .ToArray();

            var jobStrip = await job.JobStrip.GetAsync();
            if (jobStrip == null) {
                logger.ErrorFormat("Error while exporting job with id '{0}': related job strip (id '{1}') is missing. ColorReference elements will not be exported.",
                    job.Id, job.JobStripId);
                return result;
            }

            var patches = await jobStrip.JobStripPatches.GetAsync();

            // Assign color names and patch type
            foreach (var colorRef in result.ColorReference) {
                var jsp = patches.FirstOrDefault(p => p.Slot1 == colorRef.ColorbarSlot);
                if (jsp != null) {
                    colorRef.Value = jsp.Name;

                    var refPatch = await jsp.ReferencePatch.GetAsync();
                    if (refPatch == null)
                        continue;

                    var reference = await refPatch.Reference.GetAsync();
                    if (reference == null)
                        continue;

                    colorRef.ReferenceType = reference.IsSpotLib ? ReferenceTypeType.Spot : ReferenceTypeType.Process;
                }
            }

            return result;
        }
    }

    public static class ScanSideTypeExtensions {
        public static ScanSideType ToPressviewSheetSide(this SheetSides autoJobScanSide) {
            switch (autoJobScanSide) {
                case SheetSides.NA:
                    return ScanSideType.Unknown;
                case SheetSides.Bottom:
                    return ScanSideType.Bottom;
                case SheetSides.Top:
                    return ScanSideType.Top;
                default:
                    throw new ArgumentOutOfRangeException("autoJobScanSide");
            }
        }
    }
}