﻿using System.IO;
using System.Xml.Serialization;

namespace Colorware.Models.AutoJob.Import {
    public static class AutoJobXmlImporter {
        public static JobInformationType Parse(string xmlPath) {
            using (var stream = new FileStream(xmlPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                return Parse(stream);
        }

        public static JobInformationType Parse(Stream xmlStream) {
            var serializer = new XmlSerializer(typeof(JobInformationType));
            return serializer.Deserialize(xmlStream) as JobInformationType;
        }
    }
}