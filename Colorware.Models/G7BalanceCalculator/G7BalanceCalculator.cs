using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using Colorware.Core.Color;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;
using Colorware.Core.Helpers;
using Colorware.Core.Mvvm;
using Colorware.Models.ServiceModels;
using Colorware.Models.StripCompare;

namespace Colorware.Models.G7BalanceCalculator {
    public class G7BalanceCalculator : DefaultNotifyPropertyChanged {
        private int blackFamilySlot;

        #region Properties
        public bool IsOkSheet {
            get {
                if (CompareResult == null)
                    return false;
                return CompareResult.Job.HasOkSheet && CompareResult.OKSheetSamples != null;
            }
        }

        private IStripCompareResult compareResult;

        public IStripCompareResult CompareResult {
            get { return compareResult; }
            set {
                compareResult = value;
                OnPropertyChanged("CompareResult");
                Task.Run(() => recalculateAsync());
            }
        }

        private ISample paperWhite;

        public ISample PaperWhite {
            get { return paperWhite; }
            set {
                paperWhite = value;
                OnPropertyChanged("PaperWhite");
            }
        }

        private List<IStripComparePatch> patchesFor50PercentBlack;

        public List<IStripComparePatch> PatchesFor50PercentBlack {
            get { return patchesFor50PercentBlack; }
            set {
                patchesFor50PercentBlack = value;
                OnPropertyChanged("PatchesFor50PercentBlack");
            }
        }

        private IStripComparePatch summaryPatchFor50PercentBlack;

        public IStripComparePatch SummaryPatchFor50PercentBlack {
            get { return summaryPatchFor50PercentBlack; }
            set {
                summaryPatchFor50PercentBlack = value;
                OnPropertyChanged("SummaryPatchFor50PercentBlack");
            }
        }

        public string ReferenceDensityFor50PercentBlack {
            get {
                if (SummaryPatchFor50PercentBlack == null)
                    return "-";
                return SummaryPatchFor50PercentBlack.RefDensity.GetFilteredValue().ToString("F",
                                                                                            CultureInfo.InvariantCulture);
            }
        }

        private IStripComparePatch midToneSummaryPatch;

        public IStripComparePatch MidToneSummaryPatch {
            get { return midToneSummaryPatch; }
            set {
                midToneSummaryPatch = value;
                OnPropertyChanged("MidToneSummaryPatch");
            }
        }

        private List<IStripComparePatch> midTones;

        public List<IStripComparePatch> MidTones {
            get { return midTones; }
            set {
                midTones = value;
                OnPropertyChanged("MidTones");
            }
        }

        public string MidToneReferenceDensity {
            get {
                if (MidToneSummaryPatch == null)
                    return "-";
                return MidToneSummaryPatch.RefDensity.GetFilteredValue().ToString("F", CultureInfo.InvariantCulture);
            }
        }

        private bool hasMidTones;

        public bool HasMidTones {
            get { return hasMidTones; }
            set {
                hasMidTones = value;
                OnPropertyChanged("HasMidTones");
            }
        }

        private List<IStripComparePatch> shadows;

        public List<IStripComparePatch> Shadows {
            get { return shadows; }
            set {
                shadows = value;
                OnPropertyChanged("Shadows");
            }
        }

        private bool hasShadows;

        public bool HasShadows {
            get { return hasShadows; }
            set {
                hasShadows = value;
                OnPropertyChanged("HasShadows");
            }
        }

        private List<IStripComparePatch> highlights;

        public List<IStripComparePatch> Highlights {
            get { return highlights; }
            set {
                highlights = value;
                OnPropertyChanged("Highlights");
            }
        }

        private bool hasHighlights;

        public bool HasHighlights {
            get { return hasHighlights; }
            set {
                hasHighlights = value;
                OnPropertyChanged("HasHighlights");
            }
        }
        #endregion

        public G7BalanceCalculator() {}

        public G7BalanceCalculator(IStripCompareResult compareResult) {
            CompareResult = compareResult;
        }

        private async Task recalculateAsync() {
            if (CompareResult == null)
                return;

            blackFamilySlot = getBlackFamilySlot();
            await calculatePaperWhiteAsync();
            calculate50PercentInkZones();
            calculateHighlights();
            calculateMidTones();
            calculateShadows();
        }

        private int getBlackFamilySlot() {
            var patches = CompareResult.SummaryPatches.Patches.Where(p => p.IsSolid && isLikelyBlack(p));
            if (!patches.Any())
                return -1;

            IStripComparePatch maxDensPatch = null;
            foreach (var patch in patches) {
                var blackDens = patch.SampleDensity.K;
                if (maxDensPatch == null || blackDens > maxDensPatch.SampleDensity.K)
                    maxDensPatch = patch;
            }
            if (maxDensPatch == null)
                return -1;
            return maxDensPatch.SingleSlot;
        }

        private async Task calculatePaperWhiteAsync() {
            var paperWhiteResult = CompareResult.SummaryPatches.Patches.FirstOrDefault(p => p.IsPaperWhite);
            if (paperWhiteResult != null)
                PaperWhite = paperWhiteResult.AsSample();
            else
                PaperWhite = await CompareResult.Job.PaperWhiteSample.GetAsync().ConfigureAwait(false);
        }

        private static bool isLikelyBlack(IStripComparePatch patch) {
            var component = patch.SampleDensity.GetFilteredComponent();
            return component == CmykComponents.K;
        }

        private StripComparePatch createUndefinedStripComparePatch(int inkZone) {
            return new StripComparePatch(JobStripPatch.CreateUndefined(),
                                         new Sample(new Lab(0, 0, 0,
                                                            CompareResult.MeasurementConditionsConfig
                                                                         .GetMeasurementConditions()))) {
                                                                             InkZone = inkZone,
                                                                             PatchType = PatchTypes.Undefined,
                                                                             InTolerance = true
                                                                         };
        }

        private void calculate50PercentInkZones() {
            if (paperWhite == null)
                return;
            // ToList here is required to stop the patches from being copied each time they are referenced.
            var patches =
                CompareResult.RawPatches.Patches.Where(
                    p => p.IsDotgain && p.Tint == 50 && p.SingleSlot == blackFamilySlot)
                             .Select(p => p.Copy())
                             .ToList();
            if (!IsOkSheet) {
                foreach (var patch in patches) {
                    // patch.SampleDensity -= paperWhite.SampleDensity;
                    patch.RefDensity += PaperWhite.Densities;
                }
            }
            PatchesFor50PercentBlack = InkZonePadder.PadInkZones(patches, CompareResult.Machine.NumberOfInkzones,
                                                                 createUndefinedStripComparePatch);
            var summaryPatchFor50PercentBlackCopy =
                CompareResult.SummaryPatches.Patches.FirstOrDefault(
                    p => p.IsDotgain && p.Tint == 50 && p.SingleSlot == blackFamilySlot);
            if (summaryPatchFor50PercentBlackCopy != null) {
                summaryPatchFor50PercentBlackCopy = summaryPatchFor50PercentBlackCopy.Copy();
                if (!IsOkSheet) {
                    summaryPatchFor50PercentBlackCopy.RefDensity += PaperWhite.Densities;
                }
            }
            SummaryPatchFor50PercentBlack = summaryPatchFor50PercentBlackCopy;
        }

        private void calculateMidTones() {
            if (paperWhite == null)
                return;
            // ToList here is required to stop the patches from being copied each time they are referenced.
            var patches = CompareResult.RawPatches.Patches.Where(p => p.IsBalanceMidtone).Select(p => p.Copy()).ToList();
            //foreach (var patch in patches) {
            //patch.RefDensity += PaperWhite.SampleDensity;
            //}
            MidTones = InkZonePadder.PadInkZones(patches, CompareResult.Machine.NumberOfInkzones,
                                                 createUndefinedStripComparePatch);
            var midToneSummaryPatchCopy = CompareResult.SummaryPatches.Patches.FirstOrDefault(p => p.IsBalanceMidtone);
            if (midToneSummaryPatchCopy == null)
                return;

            midToneSummaryPatchCopy.RefDensity += PaperWhite.Densities;
            MidToneSummaryPatch = midToneSummaryPatchCopy.Copy();
            HasMidTones = patches.Count > 0;
        }

        private void calculateHighlights() {
            // ToList here is required to stop the patches from being copied each time they are referenced.
            var patches =
                CompareResult.RawPatches.Patches.Where(p => p.IsBalanceHighlight).Select(p => p.Copy()).ToList();
            //foreach (var patch in patches) {
            //patch.RefDensity += PaperWhite.SampleDensity;
            //}
            Highlights = InkZonePadder.PadInkZones(patches, CompareResult.Machine.NumberOfInkzones,
                                                   createUndefinedStripComparePatch);
            HasHighlights = patches.Count > 0;
        }

        private void calculateShadows() {
            // ToList here is required to stop the patches from being copied each time they are referenced.
            var patches = CompareResult.RawPatches.Patches.Where(p => p.IsBalanceShadow).Select(p => p.Copy()).ToList();
            //foreach (var patch in patches) {
            //patch.RefDensity += PaperWhite.SampleDensity;
            //}
            Shadows = InkZonePadder.PadInkZones(patches, CompareResult.Machine.NumberOfInkzones,
                                                createUndefinedStripComparePatch);
            HasShadows = patches.Count > 0;
        }
    }
}