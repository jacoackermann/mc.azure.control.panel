﻿using System;
using System.Collections.Generic;

using Colorware.Core.Extensions;

namespace Colorware.Models {
    public static class DefaultDotgainListSelectionHelper {
        // Slot => DefaultDotgainList id
        public static Dictionary<int, long> DeserializeDotgainListSelections(string serializedSelections) {
            var dotgainListSlotsAndIds = serializedSelections.Split("|", StringSplitOptions.RemoveEmptyEntries);

            var dotgainListSelections = new Dictionary<int, long>();

            foreach (var dotgainListSlotsAndId in dotgainListSlotsAndIds) {
                var slot = int.Parse(dotgainListSlotsAndId.Split(";", StringSplitOptions.RemoveEmptyEntries)[0]);
                var id = long.Parse(dotgainListSlotsAndId.Split(";", StringSplitOptions.RemoveEmptyEntries)[1]);
                dotgainListSelections[slot] = id;
            }

            return dotgainListSelections;
        }
    }
}