using System;
using System.Globalization;
using System.Linq;

using Colorware.Core.Data.Models;

namespace Colorware.Models {
    public static class InkZoneLockDeserializer {
        public static InkZoneLockSet Deserialize(string serialized) {
            var set = new InkZoneLockSet();
            var zoneParts = serialized.Split(new[] {';'});
            foreach (var zonePart in zoneParts) {
                var inkZoneLock = deserializeInkZoneLock(zonePart);
                set.AddInkZoneLock(inkZoneLock);
            }
            return set;
        }

        private static InkZoneLock deserializeInkZoneLock(string serialized) {
            var parts = serialized.Split(new[] {':'});
            if (parts.Length != 2) {
                throw new Exception("Invalid input: " + serialized);
            }
            var slot = int.Parse(parts[0], CultureInfo.InvariantCulture);

            var inkZoneByteArray = Convert.FromBase64String(parts[1]).Cast<sbyte>();
            var inkZoneList = inkZoneByteArray.Select(val => (int)val);
            return new InkZoneLock(slot, inkZoneList);
        }
    }
}