﻿using System.Collections.Generic;

using Castle.Windsor;

namespace Colorware.Core {
    /// <summary>
    /// Makes an IWindsorContainer available throughout the application (Ambient Context) to use
    /// when performing partial refactorings. (it allows us to obtain an implementation for an interface
    /// anywhere we want, so that we don't have to keep refactoring up the constructor chain)
    /// 
    /// NB: this facilitates the service-locator anti-pattern and should ONLY be used for partial refactorings
    /// </summary>
    public static class GlobalContainer {
        private static readonly Stack<IWindsorContainer> containerStack = new Stack<IWindsorContainer>();

        public static IWindsorContainer Current {
            get { return containerStack.Peek(); }
        }

        public static void PushContainer(IWindsorContainer container) {
            containerStack.Push(container);
        }

        public static IWindsorContainer PopContainer() {
            return containerStack.Pop();
        }
    }
}