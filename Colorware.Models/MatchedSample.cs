using Colorware.Core.Data.Models;
using Colorware.Core.Mvvm;

namespace Colorware.Models {
    /// <summary>
    /// Represents a combined reference with sample. This is used to show the sample against the reference
    /// so they can be compared.
    /// </summary>
    public class MatchedSample : DefaultNotifyPropertyChanged {
        #region Properties
        private IJobStripPatch jobStripPatch;

        public IJobStripPatch JobStripPatch {
            get { return jobStripPatch; }
            set {
                jobStripPatch = value;
                OnPropertyChanged("JobStripPatch");
            }
        }

        private ISample sample;

        public ISample Sample {
            get { return sample; }
            set {
                sample = value;
                OnPropertyChanged("Sample");
                OnPropertyChanged("HasSample");
            }
        }

        public bool IsDefined {
            get { return JobStripPatch != null && JobStripPatch.IsDefined; }
        }

        public bool HasSample {
            get { return Sample != null; }
        }

        public bool HasDefinedSample {
            get { return HasSample && IsDefined; }
        }

        public bool HasUndefinedSample {
            get { return HasSample && !IsDefined; }
        }

        public bool IsLikelyWrong {
            get {
                if (Sample == null)
                    return false;
                if (JobStripPatch == null)
                    return false;
                if (JobStripPatch.IsUndefined)
                    return false;
                if (JobStripPatch.IsDotgainOnly)
                    return false;
                if (JobStripPatch.IsAnyBalance)
                    return false;
                var deltaE = Sample.AsLab.DeltaE76(jobStripPatch.AsLab);
                return deltaE > 50.0; // Magic number for now... Todo, make proper.
            }
        }
        #endregion

        public MatchedSample(IJobStripPatch jobStripPatch, ISample measuredSample) {
            JobStripPatch = jobStripPatch;
            Sample = measuredSample;
        }

        public System.Windows.Media.Color AsColor {
            get { return Sample.AsColor; }
        }

        public bool IsUndefined {
            get { return JobStripPatch.IsUndefined; }
        }

        public int InkZone {
            get { return JobStripPatch.InkZone; }
        }
    }
}