﻿using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Color;
using Colorware.Core.Data.Models;
using Colorware.Core.Extensions;
using Colorware.Models.ServiceModels;

using JetBrains.Annotations;

using ReactiveUI;

namespace Colorware.Models {
    /// <summary>
    /// Averages the spectral values and CMYK densities of a number of samples. Samples are
    /// required to have spectral data.
    /// 
    /// Class is thread-safe.
    /// </summary>
    [UsedImplicitly]
    public class SpectralSampleAverager : ReactiveObject {
        private readonly List<ISample> allSamples = new List<ISample>();
        private readonly object stateLock = new object();

        private int numMeasurements;

        public int NumMeasurements {
            get { return numMeasurements; }
            private set { this.RaiseAndSetIfChanged(ref numMeasurements, value); }
        }

        private ISample averageSample;

        [CanBeNull]
        public ISample AverageSample {
            get { return averageSample; }
            private set { this.RaiseAndSetIfChanged(ref averageSample, value); }
        }

        [NotNull]
        public ISample AddSampleAndGetAverage([NotNull] ISample sample) {
            if (sample == null) throw new ArgumentNullException(nameof(sample));

            lock (stateLock) {
                ensureSpectraConsistent(sample, AverageSample);
                ensureMConditionsMatch(sample, AverageSample);

                allSamples.Add(sample);
                NumMeasurements = allSamples.Count;

                AverageSample = AverageSample == null ? sample : calculateAverageSample();

                return AverageSample;
            }
        }

        public void Reset() {
            lock (stateLock) {
                allSamples.Clear();
                AverageSample = null;
                NumMeasurements = 0;
            }
        }

        [NotNull]
        private Sample calculateAverageSample() {
            var avgSpecs = new List<double>();
            // Iterate over all wavelengths
            for (int i = 0; i < AverageSample.AsSpectrum.SpectrumSamples.Count; i++) {
                // Take average of all measured spectral values for this wavelength
                double avgSpec = allSamples.Sum(s => s.AsSpectrum.SpectrumSamples[i].Value) / allSamples.Count;
                avgSpecs.Add(avgSpec);
            }

            var avgSpectrum = new Spectrum(avgSpecs, AverageSample.AsSpectrum.StartWavelength,
                                           AverageSample.AsSpectrum.EndWaveLength);

            // Filter out the sample that was originally a Reference Patch for which we never
            // have 4 densities
            var samplesToAvg = allSamples.Where(s => !s.Densities.GetValueOfHighestComponent().NearlyEquals(0)).ToList();
            // Average densities
            if (samplesToAvg.Any()) {
                var avgDensities = samplesToAvg.Sum(s => s.Densities) / allSamples.Count;
                return new Sample(avgSpectrum, avgDensities, AverageSample.MeasurementConditions);
            }

            return new Sample(avgSpectrum, AverageSample.MeasurementConditions);
        }

        // ReSharper disable once UnusedParameter.Local
        private void ensureSpectraConsistent([NotNull] ISample sample, [CanBeNull] ISample refSample) {
            if (sample.AsSpectrum == null)
                throw new InvalidOperationException(
                    "The measurement sample to be averaged has no spectral data, sample can't be used.\r\nThis probably means there is a problem with the device or device drivers. Please restart your computer and re-connect the device. Contact your system administrator if the problem persists.");

            if (refSample == null)
                return;

            if (refSample.SpectrumStart != sample.SpectrumStart ||
                refSample.SpectrumEnd != sample.SpectrumEnd ||
                refSample.AsSpectrum.SpectrumSamples.Count != sample.AsSpectrum.SpectrumSamples.Count) {
                throw new InvalidOperationException(
                    "The measurement sample to be averaged contains spectral wavelengths that are different from the wavelengths used in previous measurements, this makes averaging impossible. Please use the same device and spectral settings when measuring the average of a color.");
            }
        }

        // ReSharper disable once UnusedParameter.Local
        private void ensureMConditionsMatch([NotNull] ISample sample, [CanBeNull] ISample refSample) {
            if (refSample == null)
                return;

            if (refSample.MCondition != sample.MCondition)
                throw new ArgumentException("The measured sample's M-condition differs from the M-condition used in previous measurements, averaging the spectra would make no sense.");
        }
    }
}