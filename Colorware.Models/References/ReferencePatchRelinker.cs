﻿using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Data.Models;
using Colorware.Core.Extensions;

using JetBrains.Annotations;

namespace Colorware.Models.References {
    [UsedImplicitly]
    public class ReferencePatchRelinker {
        /// <summary>
        /// Relinks the passed in database patches according to their relations specified in local patches.
        /// 
        /// Patches need to be in the same order for this to work!
        /// 
        /// NB: They won't actually be saved to the DB by this function.
        /// </summary>
        /// <param name="patchesWithProperRelations">The local patches with proper relationships.</param>
        /// <param name="patchesToRelink">Patches with IDs, but with no relationships.</param>
        public void RelinkPatches([NotNull] IReadOnlyList<IReferencePatch> patchesWithProperRelations,
            [NotNull] IReadOnlyList<IReferencePatch> patchesToRelink) {
            for (int i = 0; i < patchesToRelink.Count(); i++) {
                var examplePatch = patchesWithProperRelations[i];

                if (examplePatch == null)
                    continue;

                var dbParent1Idx = examplePatch.Parent1Id != 0
                    ? patchesWithProperRelations.IndexOf(p => p != null && p.Id == examplePatch.Parent1Id)
                    : -1;

                var dbParent2Idx = examplePatch.Parent2Id != 0
                    ? patchesWithProperRelations.IndexOf(p => p != null && p.Id == examplePatch.Parent2Id)
                    : -1;

                var dbParent3Idx = examplePatch.Parent3Id != 0
                    ? patchesWithProperRelations.IndexOf(p => p != null && p.Id == examplePatch.Parent3Id)
                    : -1;

                var patchToRelink = patchesToRelink[i];
                patchToRelink.Parent1Id = dbParent1Idx >= 0 ? patchesToRelink[dbParent1Idx].Id : 0;
                patchToRelink.Parent2Id = dbParent2Idx >= 0 ? patchesToRelink[dbParent2Idx].Id : 0;
                patchToRelink.Parent3Id = dbParent3Idx >= 0 ? patchesToRelink[dbParent3Idx].Id : 0;
            }
        }
    }
}