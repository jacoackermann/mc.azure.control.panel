using System;

using Colorware.Core.Color;
using Colorware.Core.Data;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.References;
using Colorware.Core.Enums;
using Colorware.Models.ServiceModels;

using JetBrains.Annotations;

namespace Colorware.Models.References {
    [UsedImplicitly]
    public class ReferencePatchCreator : IReferencePatchCreator {
        private readonly ILocalServiceModelIdGenerator localServiceModelIdGenerator;

        public ReferencePatchCreator([NotNull] ILocalServiceModelIdGenerator localServiceModelIdGenerator) {
            if (localServiceModelIdGenerator == null)
                throw new ArgumentNullException(nameof(localServiceModelIdGenerator));

            this.localServiceModelIdGenerator = localServiceModelIdGenerator;
        }

        public IReferencePatch CreateReferencePatch(SpectralMeasurementConditions mc) {
            if (mc == null) throw new ArgumentNullException(nameof(mc));

            var refPatch = new ReferencePatch(mc) {
                Id = localServiceModelIdGenerator.GenerateId(),
                Percentage = 100,
                PatchType = PatchTypes.Solid
            };

            return refPatch;
        }
    }
}