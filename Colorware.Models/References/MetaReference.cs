﻿using System;
using System.Diagnostics;

using Colorware.Core.Data.Models;

using JetBrains.Annotations;

using ProtoBuf;

using ReactiveUI;
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local, for deserialization

namespace Colorware.Models.References {
    [DebuggerDisplay("MetaReference({Id}, {Name})")]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class MetaReference : ReactiveObject, IEquatable<MetaReference> {
        private MetaReference() {
        }

        public MetaReference([NotNull] IReference reference, IPaperType printingCondition, int numberOfPatches,
            int numberOfColors, bool isPantoneLiveReference, bool locked) {
            if (reference == null) throw new ArgumentNullException(nameof(reference));

            OriginalReference = reference;
            PrintingCondition = printingCondition;
            Id = reference.Id;
            Name = reference.Name ?? "";
            NumberOfColors = numberOfColors;
            IsPantoneLiveReference = isPantoneLiveReference;
            NumberOfPatches = numberOfPatches;
            Locked = locked;
            CreatedAt = reference.CreatedAt;
            LastModified = reference.UpdatedAt;
        }

        [NotNull]
        public IReference OriginalReference { get; private set; }

        public long Id { get; private set; }

        private string name;

        public string Name {
            get { return name; }
            set { this.RaiseAndSetIfChanged(ref name, value); }
        }

        [CanBeNull]
        public IPaperType PrintingCondition { get; private set; }

        public int NumberOfColors { get; private set; }

        public bool IsPantoneLiveReference { get; private set; }

        public int NumberOfPatches { get; private set; }

        public bool Locked { get; private set; }

        public DateTime CreatedAt { get; private set; }

        public DateTime LastModified { get; private set; }

        // Friendly display for when we don't know the count (e.g. for PL references)
        [ProtoIgnore]
        public string NumberOfColorsFriendly => NumberOfColors >= 0 ? NumberOfColors.ToString() : "";

        [ProtoIgnore]
        public string NumberOfPatchesFriendly => NumberOfPatches >= 0 ? NumberOfPatches.ToString() : "";

        public bool Equals(MetaReference other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return OriginalReference.Equals(other.OriginalReference);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((MetaReference)obj);
        }

        public override int GetHashCode() {
            return OriginalReference.GetHashCode();
        }
    }
}