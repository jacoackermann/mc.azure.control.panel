﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

using Colorware.Core.Color;

using JetBrains.Annotations;

namespace Colorware.Models.GrayFinder {
    class DefaultGrayFinders {
        const string DefaultsDirectory = "GfData";
        const string DefaultsFileName = "Defaults.gf";

        /// <summary>
        /// Performs a lookup in the default GrayFinder for the given machine type and patch name.
        /// The lookup searches for of a given CIE L*a*b* value, returning the CMY difference.
        /// If the combination of machine type and patch name could not be found or the defaults could
        /// not be loaded at all, null is returned.
        /// </summary>
        /// <param name="machineType">The machine type</param>
        /// <param name="patchName">The patch name in the StaticPatchList</param>
        /// <param name="sample">The absolute L*a*b* for which to perform the lookup</param>
        /// <returns>The CMY difference from the reference used to construct the GrayFinder</returns>
        [CanBeNull]
        public static CMYK Lookup(string machineType, string patchName, Lab sample, Lab reference) {
            GrayFinder grayFinder = getGrayFinder(machineType, patchName);
            return grayFinder?.Lookup(sample, reference);
        }

        /// <summary>
        /// Performs a lookup in the default GrayFinder for the given machine type and CMY tone.
        /// The lookup searches for of a given CIE L*a*b* value, returning the CMY difference.
        /// If the combination of machine type and CMY tone could not be found or the defaults could
        /// not be loaded at all, null is returned.
        /// </summary>
        /// <param name="machineType">The machine type</param>
        /// <param name="tone">The CMY tone of the patch</param>
        /// <param name="sample">The absolute L*a*b* for which to perform the lookup</param>
        /// <returns>The CMY difference from the reference used to construct the GrayFinder</returns>
        [CanBeNull]
        public static CMYK Lookup(string machineType, CMYK tone, Lab sample, Lab reference) {
            GrayFinder grayFinder = getGrayFinder(machineType, tone);
            return grayFinder?.Lookup(sample, reference);
        }

        /// <summary>
        /// Performs a lookup in the default GrayFinder for the given machine type and patch name.
        /// The lookup searches for of a given CIE L*a*b* value, returning the CMY difference.
        /// If the combination of machine type and CMY tone could not be found or the defaults could
        /// not be loaded at all, null is returned.
        /// This Lookup also performs a correction for G7, where the shift in a*b* is subtracted from the sample,
        /// based on the difference between the measured and reference substrate.
        /// </summary>
        /// <param name="machineType">The machine type</param>
        /// <param name="patchName">The patch name in the StaticPatchList</param>
        /// <param name="sample">The absolute L*a*b* for which to perform the lookup</param>
        /// <param name="substrateReference">The L*a*b* of the reference substrate</param>
        /// <param name="substrateSample">The L*a*b* of the measured substrate</param>
        /// <param name="overprintSample">The L*a*b* of the measured CMY overprint</param>
        /// <returns>The CMY difference from the reference used to construct the GrayFinder</returns>
        [CanBeNull]
        public static CMYK LookupG7(string machineType, string patchName, Lab sample, Lab reference, Lab substrateReference, Lab substrateSample, Lab overprintSample) {
            double shiftAmount = (substrateSample.L - sample.L) / (substrateSample.L - overprintSample.L);
            Lab shift = (substrateSample - substrateReference) * shiftAmount;
            Lab shiftedSample = sample - new Lab(0.0, shift.a, shift.b, sample.MeasurementConditions);

            return Lookup(machineType, patchName, shiftedSample, reference);
        }

        /// <summary>
        /// Performs a lookup in the default GrayFinder for the given machine type and CMY tone.
        /// The lookup searches for of a given CIE L*a*b* value, returning the CMY difference.
        /// If the combination of machine type and CMY tone could not be found or the defaults could
        /// not be loaded at all, null is returned.
        /// This Lookup also performs a correction for G7, where the shift in a*b* is subtracted from the sample,
        /// based on the difference between the measured and reference substrate.
        /// </summary>
        /// <param name="machineType">The machine type</param>
        /// <param name="tone">The CMY tone of the patch</param>
        /// <param name="sample">The absolute L*a*b* for which to perform the lookup</param>
        /// <param name="substrateReference">The L*a*b* of the reference substrate</param>
        /// <param name="substrateSample">The L*a*b* of the measured substrate</param>
        /// <param name="overprintSample">The L*a*b* of the measured CMY overprint</param>
        /// <returns>The CMY difference from the reference used to construct the GrayFinder</returns>
        [CanBeNull]
        public static CMYK LookupG7(string machineType, CMYK tone, Lab sample, Lab reference, Lab substrateReference, Lab substrateSample, Lab overprintSample) {
            double shiftAmount = (substrateSample.L - sample.L) / (substrateSample.L - overprintSample.L);
            Lab shift = (substrateSample - substrateReference) * shiftAmount;
            Lab shiftedSample = sample - new Lab(0.0, shift.a, shift.b, sample.MeasurementConditions);

            return Lookup(machineType, tone, shiftedSample, reference);
        }


        [CanBeNull]
        private static GrayFinder getGrayFinder(string machineType, string patchName) {
            if (defaults == null)
                loadFromFile();
            if (defaults == null)
                return null;

            int entryIndex = entries.FindIndex(e => machineType.ToLower().Contains(e.MachineType.ToLower()) &&
                                                    e.PatchName == patchName);
            if (entryIndex < 0)
                return null;
            defaults.Position = entries[entryIndex].Position;
            return new GrayFinder(defaults);
        }

        [CanBeNull]
        private static GrayFinder getGrayFinder(string machineType, CMYK tone) {
            if (defaults == null)
                loadFromFile();
            if (defaults == null)
                return null;

            int entryIndex = entries.FindIndex(e => machineType.ToLower().Contains(e.MachineType.ToLower()) &&
                                                    e.C == (byte)tone.C && e.M == (byte)tone.M && e.Y == (byte)tone.Y);
            if (entryIndex < 0)
                return null;
            defaults.Position = entries[entryIndex].Position;
            return new GrayFinder(defaults);
        }


        struct Entry {
            public string MachineType;
            public string PatchName;
            public byte C, M, Y;
            public long Position;
        }

        private static MemoryStream defaults;
        private static List<Entry> entries;

        private static void decode(byte[] bytes) {
            byte[] maskString = System.Text.Encoding.ASCII.GetBytes("GrayFinder ");
            int offset = 0;
            int maxOffset = (maskString.Length - 1) * 8;
            for (int i = 0; i < bytes.Length; ++i) {
                offset += 11;
                if (offset > maxOffset)
                    offset -= maxOffset;
                byte mask = (byte)((maskString[offset / 8] << (offset % 8)) |
                            (maskString[(offset + 7) / 8] >> (8 - offset % 8)));
                bytes[i] ^= mask;
            }
        }

        private static string getDefaultsFilePath() {
            var applicationDir = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) ??
                                 Directory.GetCurrentDirectory();
            return Path.Combine(applicationDir, DefaultsDirectory, DefaultsFileName);
        }

        private static void loadFromFile() {
            try {
                byte[] bytes = File.ReadAllBytes(getDefaultsFilePath());
                decode(bytes);
                defaults = new MemoryStream(bytes);
                constructLookups();
            }
            catch (Exception) {}
        }

        private static void constructLookups() {
            entries = new List<Entry>();

            var reader = new BinaryReader(defaults, Encoding.UTF8, true);

            int numBalancePatches = reader.ReadInt32();
            var newEntry = new Entry();
            while (reader.BaseStream.Position != reader.BaseStream.Length) {
                newEntry.MachineType = reader.ReadString();
                for (int i = 0; i < numBalancePatches; ++i) {
                    newEntry.PatchName = reader.ReadString();
                    newEntry.C = reader.ReadByte();
                    newEntry.M = reader.ReadByte();
                    newEntry.Y = reader.ReadByte();
                    long grayFinderSize = reader.ReadInt64();
                    newEntry.Position = reader.BaseStream.Position;
                    entries.Add(newEntry);
                    reader.BaseStream.Position += grayFinderSize;
                }
            }
        }
    }
}
