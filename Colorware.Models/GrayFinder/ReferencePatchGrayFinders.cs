﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Colorware.Core.Color;
using Colorware.Core.Data;
using Colorware.Core.Data.Models;

using JetBrains.Annotations;

namespace Colorware.Models.GrayFinder {
    public class ReferencePatchGrayFinders {
        private readonly IServiceModelRepository<IGrayFinderPatch> grayFinderRepository;

        public ReferencePatchGrayFinders([NotNull] IServiceModelRepository<IGrayFinderPatch> grayFinderRepository) {
            if (grayFinderRepository == null) throw new ArgumentNullException(nameof(grayFinderRepository));

            this.grayFinderRepository = grayFinderRepository;
        }

        /// <summary>
        /// Performs a lookup of a given CIE L*a*b* value, returning the CMY difference.
        /// If the reference patch does not have a GrayFinder, null is returned.
        /// </summary>
        /// <param name="referencePatchId">The Id of the reference patch to compare to</param>
        /// <param name="sample">The absolute L*a*b* for which to perform the lookup</param>
        /// <returns>The CMY difference from the reference used to construct the GrayFinder</returns>
        [CanBeNull]
        public async Task<CMYK> Lookup(long referencePatchId, Lab sample, Lab reference) {
            GrayFinder grayFinder = await getGrayFinder(referencePatchId);
            return grayFinder?.Lookup(sample, reference);
        }

        /// <summary>
        /// Performs a lookup of a given CIE L*a*b* value, returning the CMY difference.
        /// If the reference patch does not have a GrayFinder, null is returned.
        /// This Lookup also performs a correction for G7, where the shift in a*b* is subtracted from the sample,
        /// based on the difference between the measured and reference substrate.
        /// </summary>
        /// <param name="referencePatchId">The Id of the reference patch to compare to</param>
        /// <param name="sample">The absolute L*a*b* for which to perform the lookup</param>
        /// <param name="substrateReference">The L*a*b* of the reference substrate</param>
        /// <param name="substrateSample">The L*a*b* of the measured substrate</param>
        /// <param name="overprintSample">The L*a*b* of the measured CMY overprint</param>
        /// <returns>The CMY difference from the reference used to construct the GrayFinder</returns>
        [CanBeNull]
        public async Task<CMYK> LookupG7(long referencePatchId, Lab sample, Lab reference, Lab substrateReference, Lab substrateSample, Lab overprintSample) {
            double shiftAmount = (substrateSample.L - sample.L) / (substrateSample.L - overprintSample.L);
            Lab shift = (substrateSample - substrateReference) * shiftAmount;
            Lab shiftedSample = sample - new Lab(0.0, shift.a, shift.b, sample.MeasurementConditions);

            return await Lookup(referencePatchId, shiftedSample, reference);
        }

        private async Task<GrayFinder> getGrayFinder(long referencePatchId) {
            var grayFinderPatch = (await grayFinderRepository.FindAllAsync(
                new Query().Eq("reference_patch_id", referencePatchId))).FirstOrDefault();
            if (grayFinderPatch == null)
                return null;
            var dataStream = new MemoryStream(grayFinderPatch.Data, false);
            return new GrayFinder(dataStream);
        }
    }
}
