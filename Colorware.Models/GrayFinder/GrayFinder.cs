﻿using System;
using System.IO;
using System.Text;

using Colorware.Core.Color;
using Colorware.Core.Helpers.Math;

using JetBrains.Annotations;

using LCMSBinding;


namespace Colorware.Models.GrayFinder {
    /// <summary>
    /// A GrayFinder is a series of CMY samples around a given tone, with associated CIE L*a*b* differences for each.
    /// The CMY samples are in a block shape with 2 steps of deviation in each direction.
    /// This results in a total of 5x5x5 data points, the center being the given tone.
    /// 
    /// This class contains the implementation of the GrayFinder. For the database entry, look at GrayFinderPatch.
    /// 
    /// An instance of this class can be created using an ICC profile and a given CMY tone. The steps in CMY will be
    /// determined automatically to fit most usecases. It will store at most -10/+10 for a single tonevalue. It can
    /// also be constructed using a binary representation, to be created using the AsBytes() method.
    /// 
    /// The GrayFinder can be used to do an inversed search by giving a deviation in CIE L*a*b*. It will return the
    /// difference in CMY that would lead to this L*a*b* value, according to the reference with which the GrayFinder
    /// is constructed.
    /// </summary>
    internal class GrayFinder {
        /// <summary>
        /// A single point in the GrayFinder, storing CIE L*a*b* and CMY deviation
        /// </summary>
        class GrayFinderPoint {
            public readonly double L, a, b, C, M, Y;

            public GrayFinderPoint(double L, double a, double b, double C, double M, double Y) {
                this.L = L;
                this.a = a;
                this.b = b;
                this.C = C;
                this.M = M;
                this.Y = Y;
            }

            public static GrayFinderPoint operator +(GrayFinderPoint p1, GrayFinderPoint p2) {
                return new GrayFinderPoint(p1.L + p2.L, p1.a + p2.a, p1.b + p2.b,
                                           p1.C + p2.C, p1.M + p2.M, p1.Y + p2.Y);
            }

            public static GrayFinderPoint operator -(GrayFinderPoint p1, GrayFinderPoint p2) {
                return new GrayFinderPoint(p1.L - p2.L, p1.a - p2.a, p1.b - p2.b,
                                           p1.C - p2.C, p1.M - p2.M, p1.Y - p2.Y);
            }

            public static GrayFinderPoint operator *(GrayFinderPoint p, double s) {
                return new GrayFinderPoint(p.L * s, p.a * s, p.b * s, p.C * s, p.M * s, p.Y * s);
            }

            public static GrayFinderPoint operator /(GrayFinderPoint p, double s) {
                return new GrayFinderPoint(p.L / s, p.a / s, p.b / s, p.C / s, p.M / s, p.Y / s);
            }
        }

        // The 5x5x5 array of points
        readonly GrayFinderPoint[,,] points;

        // Preallocated temporary storage of intermediate results
        // These prevent allocation during processing
        private readonly GrayFinderPoint[,,] segPointsUVW;
        private readonly GrayFinderPoint[,] segPointsUV;
        private readonly GrayFinderPoint[] segPointsU;

        // These are the settings used to construct Grayfinders
        // They are fixed constants that should not be changed
        const int Size = 5;
        const int Steps = 2;
        const double Threshold = 0.0001;
        const double SearchStep = 0.9;
        const int MaxSearchSteps = 20;


        /// <summary>
        /// Constructs a GrayFinder using the transform from an ICC profile and a CMYK tonevalue.
        /// The tone will typically be a G7 gray value.
        /// </summary>
        /// <param name="transform">The transform of the ICC profile</param>
        /// <param name="tone">The CMY tone around which to form the GrayFinder</param>
        public GrayFinder([NotNull] IccTransform transform, [NotNull] CMYK tone) {
            if (transform == null) throw new ArgumentNullException(nameof(transform));
            if (tone == null) throw new ArgumentNullException(nameof(tone));

            var reference = transform.CMYKToLab(tone);
            points = new GrayFinderPoint[Size, Size, Size];
            segPointsUVW = new GrayFinderPoint[4, 4, 4];
            segPointsUV = new GrayFinderPoint[4, 4];
            segPointsU = new GrayFinderPoint[4];

            for (int ui = -Steps; ui <= Steps; ++ui) {
                for (int vi = -Steps; vi <= Steps; ++vi) {
                    for (int wi = -Steps; wi <= Steps; ++wi) {
                        double cStep = ui * toneValueStep(tone.C) / Steps;
                        double mStep = vi * toneValueStep(tone.M) / Steps;
                        double yStep = wi * toneValueStep(tone.Y) / Steps;

                        var labDiff = transform.CMYKToLab(tone.C + cStep, tone.M + mStep, tone.Y + yStep, 0) - reference;
                        var point = new GrayFinderPoint(labDiff.L, labDiff.a, labDiff.b, cStep, mStep, yStep);
                        points[ui + Steps, vi + Steps, wi + Steps] = point;
                    }
                }
            }
        }

        /// <summary>
        /// Constructs a GrayFinder using a previously generated stream containing a binary form.
        /// This is useful for loading GrayFinders from a file or database.
        /// </summary>
        /// <param name="stream">The stream to read the binary form from</param>
        public GrayFinder(Stream stream) {
            points = new GrayFinderPoint[Size, Size, Size];
            segPointsUVW = new GrayFinderPoint[4, 4, 4];
            segPointsUV = new GrayFinderPoint[4, 4];
            segPointsU = new GrayFinderPoint[4];

            var reader = new BinaryReader(stream, Encoding.Default, true);
            for (int ui = 0; ui < Size; ++ui) {
                for (int vi = 0; vi < Size; ++vi) {
                    for (int wi = 0; wi < Size; ++wi) {
                        float L = readFloat(reader);
                        float a = readFloat(reader);
                        float b = readFloat(reader);
                        float C = readFloat(reader);
                        float M = readFloat(reader);
                        float Y = readFloat(reader);
                        points[ui, vi, wi] = new GrayFinderPoint(L, a, b, C, M, Y);
                    }
                }
            }
        }

        /// <summary>
        /// Serializes this GrayFinder to a binary form. Useful for storing grayFinders in a file or database.
        /// </summary>
        /// <param name="stream">The stream to output the binary form into</param>
        public void Serialize(Stream stream) {
            var writer = new BinaryWriter(stream, Encoding.Default, true);
            for (int ui = 0; ui < Size; ++ui) {
                for (int vi = 0; vi < Size; ++vi) {
                    for (int wi = 0; wi < Size; ++wi) {
                        GrayFinderPoint p = points[ui, vi, wi];
                        writeFloat(writer, (float)p.L);
                        writeFloat(writer, (float)p.a);
                        writeFloat(writer, (float)p.b);
                        writeFloat(writer, (float)p.C);
                        writeFloat(writer, (float)p.M);
                        writeFloat(writer, (float)p.Y);
                    }
                }
            }
        }

        /// <summary>
        /// Performs a lookup of a given CIE L*a*b* value, returning the CMY difference.
        /// </summary>
        /// <param name="color">The absolute L*a*b* for which to perform the lookup</param>
        /// <param name="reference">The reference L*a*b* to compare the deviation against</param>
        /// <returns>The CMY difference from the reference used to construct the GrayFinder</returns>
        public CMYK Lookup(Lab color, Lab reference) {
            Lab target = color - reference;
            // Find a (u,v,w) that results in an evaluation with matching (L,a,b)
            // Initial search starts at the center of the given points
            double u = (Size - 1) / 2.0;
            double v = (Size - 1) / 2.0;
            double w = (Size - 1) / 2.0;
            GrayFinderPoint value = evaluate(u, v, w);
            bool outOfBounds = false;
            // Limit steps by a maximum, keep searching until we find (L,a,b)
            for (int i = 0; i < MaxSearchSteps; ++i) {
                // d is the difference in (L,a,b) where we want to go
                Vector3 d = new Vector3(target.L - value.L, target.a - value.a, target.b - value.b);
                // Check if we are at (L,a,b) already
                double error = d.Length();
                if (error < Threshold)
                    break;
                // Find the local partial derivatives for u, v and w
                GrayFinderPoint dUPoint = diffU(u, v, w);
                GrayFinderPoint dVPoint = diffV(u, v, w);
                GrayFinderPoint dWPoint = diffW(u, v, w);
                Vector3 dU = new Vector3(dUPoint.L, dUPoint.a, dUPoint.b);
                Vector3 dV = new Vector3(dVPoint.L, dVPoint.a, dVPoint.b);
                Vector3 dW = new Vector3(dWPoint.L, dWPoint.a, dWPoint.b);
                // Get the vector decomposition of the difference in terms of
                // the partial derivatives to get the next step in u, v and w directions
                double stepU = Vector3.Wedge(d, dV, dW) / Vector3.Wedge(dU, dV, dW);
                double stepV = Vector3.Wedge(dU, d, dW) / Vector3.Wedge(dU, dV, dW);
                double stepW = Vector3.Wedge(dU, dV, d) / Vector3.Wedge(dU, dV, dW);
                if (outOfBounds) {
                    // If the (u,v,w) tried to step outside the point data before
                    // and the derivative points outside the data again,
                    // the best we can do is estimate the solution from here
                    if ((u == 0.0 && stepU < 0.0) || (u == Size - 1 && stepU > 0.0) ||
                        (v == 0.0 && stepV < 0.0) || (v == Size - 1 && stepV > 0.0) ||
                        (w == 0.0 && stepW < 0.0) || (w == Size - 1 && stepW > 0.0)) {
                        value = value + dUPoint * stepU + dVPoint * stepV + dWPoint * stepW;
                        break;
                    }
                }
                // Update (u,v,w) to get closer to target (L,a,b)
                // This is a simple Euler step, good enough
                // Searchstep should be < 1 to improve convergence
                u += stepU * SearchStep;
                v += stepV * SearchStep;
                w += stepW * SearchStep;
                if (u < 0.0 || u > Size - 1 || v < 0.0 || v > Size - 1 || w < 0.0 || w > Size - 1) {
                    // If the (u,v,w) should fall outside the point data, make a note of it
                    // and set (u,v,w) to inside valid data
                    // This is to prevent erroneous extrapolation
                    outOfBounds = true;
                    u = Math.Max(0.0, Math.Min(Size - 1, u));
                    v = Math.Max(0.0, Math.Min(Size - 1, v));
                    w = Math.Max(0.0, Math.Min(Size - 1, w));
                }
                else {
                    outOfBounds = false;
                }
                value = evaluate(u, v, w);
            }
            return new CMYK(value.C, value.M, value.Y, 0);
        }

        
        // Read a float in network byte order (big endian)
        private float readFloat(BinaryReader reader) {
            byte[] bytes = reader.ReadBytes(4);
            // floats are 4 bytes long, so we can simply reverse
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            return BitConverter.ToSingle(bytes, 0);
        }
        // Write a float in network byte order (big endian)
        private void writeFloat(BinaryWriter writer, float f) {
            byte[] bytes = BitConverter.GetBytes(f);
            // floats are 4 bytes long, so we can simply reverse
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            writer.Write(bytes);
        }


        private GrayFinderPoint getCenterPoint() {
            return points[Steps, Steps, Steps];
        }

        private double sqr(double x) {
            return x * x;
        }
        private double toneValueStep(double centerToneValue) {
            // A simple parabolic curve that peaks at (50, 10) and is 0 at (0, 0) and (100, 0)
            return 10 - 10 * sqr((centerToneValue - 50) / 50);
        }


        // With t in [0, 1] for [p1, p2]
        private double catmullRom(double p0, double p1, double p2, double p3, double t) {
            double t2 = t * t;
            double t3 = t2 * t;
            double m1 = p2 - p0;
            double m2 = p3 - p1;
            return (2.0 * t3 - 3.0 * t2 + 1.0) * p1 + (t3 - 2.0 * t2 + t) * m1 + (-2.0 * t3 + 3.0 * t2) * p2 +
                   (t3 - t2) * m2;
        }

        private GrayFinderPoint catmullRom(GrayFinderPoint p0, GrayFinderPoint p1,
                                           GrayFinderPoint p2, GrayFinderPoint p3, double t) {
            double L = catmullRom(p0.L, p1.L, p2.L, p3.L, t);
            double a = catmullRom(p0.a, p1.a, p2.a, p3.a, t);
            double b = catmullRom(p0.b, p1.b, p2.b, p3.b, t);
            double C = catmullRom(p0.C, p1.C, p2.C, p3.C, t);
            double M = catmullRom(p0.M, p1.M, p2.M, p3.M, t);
            double Y = catmullRom(p0.Y, p1.Y, p2.Y, p3.Y, t);
            return new GrayFinderPoint(L, a, b, C, M, Y);
        }

        // With t in [0, 1] for [p1, p2]
        private double catmullRomDiff(double p0, double p1, double p2, double p3, double t) {
            double t2 = t * t;
            double m1 = p2 - p0;
            double m2 = p3 - p1;
            return (6.0 * t2 - 6.0 * t) * p1 + (3.0 * t2 - 4.0 * t + 1.0) * m1 + (-6.0 * t2 + 6.0 * t) * p2 +
                   (3.0 * t2 - 2.0 * t) * m2;
        }

        private GrayFinderPoint catmullRomDiff(GrayFinderPoint p0, GrayFinderPoint p1,
                                               GrayFinderPoint p2, GrayFinderPoint p3, double t) {
            double L = catmullRomDiff(p0.L, p1.L, p2.L, p3.L, t);
            double a = catmullRomDiff(p0.a, p1.a, p2.a, p3.a, t);
            double b = catmullRomDiff(p0.b, p1.b, p2.b, p3.b, t);
            double C = catmullRomDiff(p0.C, p1.C, p2.C, p3.C, t);
            double M = catmullRomDiff(p0.M, p1.M, p2.M, p3.M, t);
            double Y = catmullRomDiff(p0.Y, p1.Y, p2.Y, p3.Y, t);
            return new GrayFinderPoint(L, a, b, C, M, Y);
        }

        private int getSegmentU(double u) {
            return Math.Max(0, Math.Min(Size - 2, (int)Math.Floor(u)));
        }

        private int getSegmentV(double v) {
            return Math.Max(0, Math.Min(Size - 2, (int)Math.Floor(v)));
        }

        private int getSegmentW(double w) {
            return Math.Max(0, Math.Min(Size - 2, (int)Math.Floor(w)));
        }

        private void getPointsForSegment(int segU, int segV, int segW) {
            for (int ui = 0; ui < 4; ++ui) {
                int u = Math.Max(0, Math.Min(Size - 1, ui - 1 + segU));
                for (int vi = 0; vi < 4; ++vi) {
                    int v = Math.Max(0, Math.Min(Size - 1, vi - 1 + segV));
                    for (int wi = 0; wi < 4; ++wi) {
                        int w = Math.Max(0, Math.Min(Size - 1, wi - 1 + segW));
                        segPointsUVW[ui, vi, wi] = points[u, v, w];
                    }
                }
            }
        }

        // Perform an interpolation of the data points at given (u, v, w) coordinate
        // Return the interpolated point
        private GrayFinderPoint evaluate(double u, double v, double w) {
            int segU = getSegmentU(u);
            int segV = getSegmentV(v);
            int segW = getSegmentW(w);
            getPointsForSegment(segU, segV, segW);
            for (int ui = 0; ui < 4; ++ui) {
                for (int vi = 0; vi < 4; ++vi) {
                    segPointsUV[ui, vi] = catmullRom(segPointsUVW[ui, vi, 0], segPointsUVW[ui, vi, 1],
                                                     segPointsUVW[ui, vi, 2], segPointsUVW[ui, vi, 3], w - segW);
                }
            }
            for (int ui = 0; ui < 4; ++ui) {
                segPointsU[ui] = catmullRom(segPointsUV[ui, 0], segPointsUV[ui, 1],
                                            segPointsUV[ui, 2], segPointsUV[ui, 3], v - segV);
            }
            return catmullRom(segPointsU[0], segPointsU[1],
                              segPointsU[2], segPointsU[3], u - segU);
        }

        // Perform an interpolation of the data points at given (u, v, w) coordinate
        // Return the partial derivative in the u direction
        private GrayFinderPoint diffU(double u, double v, double w) {
            int segU = getSegmentU(u);
            int segV = getSegmentV(v);
            int segW = getSegmentW(w);
            getPointsForSegment(segU, segV, segW);
            for (int ui = 0; ui < 4; ++ui) {
                for (int vi = 0; vi < 4; ++vi) {
                    segPointsUV[ui, vi] = catmullRom(segPointsUVW[ui, vi, 0], segPointsUVW[ui, vi, 1],
                                                     segPointsUVW[ui, vi, 2], segPointsUVW[ui, vi, 3], w - segW);
                }
            }
            for (int ui = 0; ui < 4; ++ui) {
                segPointsU[ui] = catmullRom(segPointsUV[ui, 0], segPointsUV[ui, 1],
                                            segPointsUV[ui, 2], segPointsUV[ui, 3], v - segV);
            }
            return catmullRomDiff(segPointsU[0], segPointsU[1],
                                  segPointsU[2], segPointsU[3], u - segU);
        }

        // Perform an interpolation of the data points at given (u, v, w) coordinate
        // Return the partial derivative in the v direction
        private GrayFinderPoint diffV(double u, double v, double w) {
            int segU = getSegmentU(u);
            int segV = getSegmentV(v);
            int segW = getSegmentW(w);
            getPointsForSegment(segU, segV, segW);
            for (int ui = 0; ui < 4; ++ui) {
                for (int vi = 0; vi < 4; ++vi) {
                    segPointsUV[ui, vi] = catmullRom(segPointsUVW[ui, vi, 0], segPointsUVW[ui, vi, 1],
                                                     segPointsUVW[ui, vi, 2], segPointsUVW[ui, vi, 3], w - segW);
                }
            }
            for (int ui = 0; ui < 4; ++ui) {
                segPointsU[ui] = catmullRomDiff(segPointsUV[ui, 0], segPointsUV[ui, 1],
                                                segPointsUV[ui, 2], segPointsUV[ui, 3], v - segV);
            }
            return catmullRom(segPointsU[0], segPointsU[1],
                              segPointsU[2], segPointsU[3], u - segU);
        }

        // Perform an interpolation of the data points at given (u, v, w) coordinate
        // Return the partial derivative in the w direction
        private GrayFinderPoint diffW(double u, double v, double w) {
            int segU = getSegmentU(u);
            int segV = getSegmentV(v);
            int segW = getSegmentW(w);
            getPointsForSegment(segU, segV, segW);
            for (int ui = 0; ui < 4; ++ui) {
                for (int vi = 0; vi < 4; ++vi) {
                    segPointsUV[ui, vi] = catmullRomDiff(segPointsUVW[ui, vi, 0], segPointsUVW[ui, vi, 1],
                                                         segPointsUVW[ui, vi, 2], segPointsUVW[ui, vi, 3], w - segW);
                }
            }
            for (int ui = 0; ui < 4; ++ui) {
                segPointsU[ui] = catmullRom(segPointsUV[ui, 0], segPointsUV[ui, 1],
                                            segPointsUV[ui, 2], segPointsUV[ui, 3], v - segV);
            }
            return catmullRom(segPointsU[0], segPointsU[1],
                              segPointsU[2], segPointsU[3], u - segU);
        }
    }
}