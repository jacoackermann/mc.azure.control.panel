﻿using System;
using System.Net.Mail;
using System.Text;

using Colorware.Core.Azure.Config;

using mc.azure.email.Enums;
using mc.azure.email.Interfaces;
using mc.azure.email.Messages;

namespace mc.azure.email.MailJet {
    public class MailJetWrapper : IEmailService {
        public void Send(MessageType messageType, string customerName, string customerLocation,
                         string recipients, string supportRecipients,
                         string customerUrl, string supportMessage) {
            if (!ConfigValueReader.IsProductionMode) {
                return;
            }

            var msgData =
                MessageFactory.CreateMessage(messageType, customerName, customerLocation, customerUrl, supportMessage);

            try {
                sendCustomerMessage(recipients, msgData);
                sendSupportMessage(supportRecipients, msgData);
            }
            catch {
                // TODO
            }
        }

        //---------------------------------------------------------------------

        private void sendCustomerMessage(string recipients, IMessage msgData) {
            if (!msgData.SendClientMessage || string.IsNullOrWhiteSpace(recipients)) return;

            MailMessage customerMsg = new MailMessage {
                // this address must be verified on mailjet!
                // currently only my colorware email address is verified
                From = new MailAddress("jaco@colorware.eu", "Colorware"),
                Subject = msgData.Subject,
                Body = msgData.Body
            };

            sendMessage(recipients, customerMsg);
        }

        //---------------------------------------------------------------------

        private void sendSupportMessage(string supportRecipients, IMessage msgData) {
            if (string.IsNullOrWhiteSpace(supportRecipients)) return;

            StringBuilder supportMsgBuilder = new StringBuilder();
            supportMsgBuilder.AppendLine(msgData.SupportBody);
            if (msgData.SendClientMessage) {
                supportMsgBuilder.AppendLine().AppendLine()
                                 .AppendLine("The following message was sent to the customer:")
                                 .AppendLine().AppendLine()
                                 .AppendLine(msgData.Body);
            }


            MailMessage supportMsg = new MailMessage {
                // this address must be verified on mailjet!
                // currently only my colorware email address is verified
                // perhaps something like 'support@colorware.eu'
                From = new MailAddress("jaco@colorware.eu", "Colorware"),
                Subject = msgData.SupportSubject,
                Body = supportMsgBuilder.ToString()
            };

            sendMessage(supportRecipients, supportMsg);
        }

        //---------------------------------------------------------------------

        private void sendMessage(string recipients, MailMessage customerMsg) {
            var recipientList = recipients.Split(new[] {' ', ',', ';'}, StringSplitOptions.RemoveEmptyEntries);
            foreach (var recipient in recipientList) {
                if (!IsValidEmail(recipient)) {
                    continue;
                }

                var addressTokens = recipient.Split('@');
                if (addressTokens.Length > 0) {
                    var name = addressTokens[0];
                    customerMsg.To.Add(new MailAddress(recipient, name));
                }
            }

            sendMessage(customerMsg);
        }

        //---------------------------------------------------------------------

        private void sendMessage(MailMessage msg) {
            if (msg.To.Count == 0) return;

            SmtpClient client = new SmtpClient(ConfigValueReader.MailjetHost, ConfigValueReader.MailjetPort) {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials =
                    new System.Net.NetworkCredential(ConfigValueReader.MailjetApiKey,
                                                     ConfigValueReader.MailjetSecretKey)
            };

            client.Send(msg);
        }

        //---------------------------------------------------------------------

        public bool IsValidEmail(string emailaddress) {
            try {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException) {
                return false;
            }
        }

        //---------------------------------------------------------------------
    }
}