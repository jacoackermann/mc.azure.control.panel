﻿namespace mc.azure.email.Enums {
    public enum MessageType {
        CreateCustomerWelcome,
        CreateCustomerSuccess,
        CreateCustomerFailure,

        DeactivateCustomerSuccess,
        DeactivateCustomerFailure,

        DbMigration
    }
}