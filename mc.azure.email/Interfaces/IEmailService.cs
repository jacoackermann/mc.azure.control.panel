﻿using mc.azure.email.Enums;

namespace mc.azure.email.Interfaces {
    public interface IEmailService {
        void Send(MessageType messageType, string customerName, string customerLocation,
                  string recipients, string supportRecipients,
                  string customerUrl = "", string supportMessage = "");
    }
}