﻿using System.ComponentModel;

using mc.azure.email.Enums;
using mc.azure.email.Messages.CreateCustomer;

namespace mc.azure.email.Messages {
    public static class MessageFactory {
        public static IMessage CreateMessage(MessageType messageType, string customerName,
                                             string customerLocation, string customerUrl,
                                             string supportMessage) {
            switch (messageType) {
                case MessageType.CreateCustomerWelcome:
                    return new WelcomeMessage(customerName, customerLocation, customerUrl, supportMessage);

                case MessageType.CreateCustomerSuccess:
                    return new SuccessMessage(customerName, customerLocation, customerUrl, supportMessage);

                case MessageType.CreateCustomerFailure:
                    return new FailureMessage(customerName, customerLocation, customerUrl, supportMessage);

                case MessageType.DeactivateCustomerSuccess:
                    return new DeactivateCustomer.SuccessMessage(customerName, customerLocation, customerUrl,
                                                                 supportMessage);

                case MessageType.DeactivateCustomerFailure:
                    return new DeactivateCustomer.FailureMessage(customerName, customerLocation, customerUrl,
                                                                 supportMessage);

                case MessageType.DbMigration:
                    return new DbMigration.DbMigrationMessage(customerName, customerLocation, customerUrl,
                                                              supportMessage);

                default:
                    throw new InvalidEnumArgumentException(nameof(messageType), (int)messageType, typeof(MessageType));
            }
        }
    }
}