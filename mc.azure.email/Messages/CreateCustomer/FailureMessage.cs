﻿using System;
using System.Text;

namespace mc.azure.email.Messages.CreateCustomer {
    public class FailureMessage : BaseMessage {
        public override string Subject => throw new NotImplementedException();
        public override string Body => throw new NotImplementedException();

        public override string SupportSubject => $"Could not create infrastructure for customer '{customerFullName}'";

        public override string SupportBody {
            get {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine($"Could not create infrastructure for customer '{customerFullName}'");

                if (!string.IsNullOrWhiteSpace(supportMessage)) {
                    sb.AppendLine().AppendLine()
                      .AppendLine("Additional information:")
                      .AppendLine()
                      .AppendLine(supportMessage);
                }

                return sb.ToString();
            }
        }

        public FailureMessage(string customerName, string customerLocation,
                              string customerUrl, string supportMessage)
            : base(customerName, customerLocation, customerUrl, supportMessage) { }
    }
}