﻿using System;

namespace mc.azure.email.Messages.CreateCustomer {
    public class WelcomeMessage : BaseMessage {
        public override bool SendClientMessage => false;

        public override string Subject => throw new NotImplementedException();
        public override string Body => throw new NotImplementedException();

        public override string SupportSubject =>
            $"Processing request to create infrastructrure for '{customerFullName}'";

        public override string SupportBody => $"Creating infrastructure for customer '{customerFullName}'"
                                              + $"{Environment.NewLine}"
                                              + "Another mail will be sent once the request was processed.";

        public WelcomeMessage(string customerName, string customerLocation,
                              string customerUrl, string supportMessage)
            : base(customerName, customerLocation, customerUrl, supportMessage) { }
    }
}