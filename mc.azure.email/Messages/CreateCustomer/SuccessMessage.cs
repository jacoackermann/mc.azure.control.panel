﻿using System;
using System.Text;

namespace mc.azure.email.Messages.CreateCustomer {
    public class SuccessMessage : BaseMessage {
        public override bool SendClientMessage => true;

        public override string Subject => "Your website is ready for first time use!";

        public override string Body => "Please use the following link to access your website:"
                                       + $"{Environment.NewLine}"
                                       + $"{CustomerUrl}";

        public override string SupportSubject =>
            $"Successfully created infrastructure for customer '{customerFullName}'";

        public override string SupportBody {
            get {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine($"Created infrastructure for customer '{customerFullName}'");

                if (!string.IsNullOrWhiteSpace(supportMessage)) {
                    sb.AppendLine().AppendLine()
                      .AppendLine("Additional information:")
                      .AppendLine()
                      .AppendLine(supportMessage);
                }

                return sb.ToString();
            }
        }

        public SuccessMessage(string customerName, string customerLocation,
                              string customerUrl, string supportMessage)
            : base(customerName, customerLocation, customerUrl, supportMessage) { }
    }
}