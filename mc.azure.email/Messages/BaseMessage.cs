﻿namespace mc.azure.email.Messages {
    public abstract class BaseMessage : IMessage {
        protected readonly string supportMessage;

        public virtual bool SendClientMessage => false;

        public abstract string Subject { get; }
        public abstract string Body { get; }

        public abstract string SupportSubject { get; }
        public abstract string SupportBody { get; }

        public string CustomerName { get; }
        public string CustomerLocation { get; }
        public string CustomerUrl { get; }

        protected string customerFullName {
            get {
                if (!string.IsNullOrWhiteSpace(CustomerLocation)) {
                    return $"{CustomerName} ({CustomerLocation})";
                }

                return CustomerName;
            }
        }

        public BaseMessage(string customerName, string customerLocation,
                           string customerUrl, string supportMessage) {
            CustomerName = customerName;
            CustomerLocation = customerLocation;
            CustomerUrl = customerUrl;
            this.supportMessage = supportMessage;
        }
    }
}