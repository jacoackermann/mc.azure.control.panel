﻿namespace mc.azure.email.Messages {
    public interface IMessage {
        bool SendClientMessage { get; }

        string Subject { get; }
        string Body { get; }

        string SupportSubject { get; }
        string SupportBody { get; }
    }
}