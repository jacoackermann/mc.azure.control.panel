﻿using System;
using System.Text;

namespace mc.azure.email.Messages.DeactivateCustomer {
    public class SuccessMessage : BaseMessage {
        public override bool SendClientMessage => false;

        public override string Subject => throw new NotImplementedException();
        public override string Body => throw new NotImplementedException();

        public override string SupportSubject =>
            $"Successfully deactivated infrastructure for customer '{customerFullName}'";

        public override string SupportBody {
            get {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine($"Deactivated customer infrastructure for customer '{customerFullName}'");

                if (!string.IsNullOrWhiteSpace(supportMessage)) {
                    sb.AppendLine().AppendLine()
                      .AppendLine("Additional information:")
                      .AppendLine()
                      .AppendLine(supportMessage);
                }

                return sb.ToString();
            }
        }

        public SuccessMessage(string customerName, string customerLocation,
                              string customerUrl, string supportMessage)
            : base(customerName, customerLocation, customerUrl, supportMessage) { }
    }
}