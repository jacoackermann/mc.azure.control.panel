/*!
 * APIs for Konica Minolta's Spectrodensitometer
 * Copyright (C) KONICA MINOLTA. 2009-2013 All rights reserved.
 * @brief FD-7/FD-5 Application Programming Interface Fucntions
 * @file FxAPI.h
 * @author KONICA MINOLTA, INC
 * @date Created on 2009/12/03
 */

#ifndef FX_API_H_
#define FX_API_H_


#ifdef _WIN32
//! FX API ( for Windows APIs)
#define FXAPI	__stdcall
#else
//! FX API ( for common APIs)
#define FXAPI	//_cdecl
#endif


#include "FxTypeDef.h"
#include "FxConditions.h"
#include "ColorConditions.h"
#include "Version.h"
#include "Report.h"

#if defined(__cplusplus)
extern "C"{
#endif


#if defined(_WIN32) || defined(__APPLE__)
#pragma pack(push,4) 
#endif




/*!
 * the interface handle 
 * @brief interface handle
 */
typedef struct tDEVINTERFACE
{
	void* hDev;	//!< interface to device
}
DEVINTERFACE;

/*!
 * the pointer to device interface
 * @brief the pointer to device interface
 */
typedef DEVINTERFACE* PDEVINTERFACE;

/*!
 * interface handle to spectrodensitomter 
 * @brief handle to spectrodesintometer
 */
typedef void* FD_DEVICE_HANDLE;

/*!
 * the properties about measurement condition
 * @brief measurement property
 */
typedef enum tDIPROPERTIES
{
	DI_PR_MEASMODE = 0,			//!< measurement mode 
	DI_PR_MEASCOND,				//!< measurement condiotion, MEASCONDITIONS   
	DI_PR_OBSERVER,				//!< observer angle, which is refferred by the color matching functions.
	DI_PR_OBS_ILLUMINANT,		//!< observer illuminant used for computing CIE XYZ. ref. DI_ILLUMINANTS
	DI_PR_MEAS_SOURCE,			//!< measurement source used for modulation of UV level. ref.  DI_ILLUMINANTS
	DI_PR_COLORSPACE,			//!< color space 	ref. DI_COLORSPACES
	DI_PR_DEN_STATUS,			//!< density status 	ref. DI_DENSITYSTATUS
	DI_PR_INDEX_TYPE,			//!< type of the index for paper character maesurement 	ref. COLOR_INDEX_TYPES
	DI_PR_REMOTEMODE,			//!< remote mode to chage remote mode or logger mode
	DI_PR_CALIBRATEMODE,		//!< calibrate mode to  change UserRefMode or WhiteRefMode
	DI_PR_CALIBSTATUS,			//!< calibration status
	DI_PR_NO,					//!< the number of the measurement properties
}
DIPROPERTIES;

/*!
 *  Events
 */
typedef enum tDI_EVENT
{
	DI_EVENT_RECIEVED_REPORT,		//!< event when report was recieved.
	DI_EVENT_PUSHED_MEAS_TRIGGER,		//!< event when the measurement was triggered. 
}
DI_EVENT;

/*!
 *  Error Code
 */
typedef enum tDI_ERROR_TYPES
{
	DI_NO_ERROR=0,				//!< normal

	DI_ERROR,					//!< common error
	DI_CRITICAL,				//!< critical error, must repair.
	DI_WARNING,					//!< warning 
	DI_ERR_USB_SPEED,			//!< USB version is not USB2.0
	DI_ERR_BATTERY,				//!< battery voltage is out of available range
	DI_ERR_INPUT,				//!< parameter value was not available.
	DI_ERR_ARGUMENT,			//!< the argument of the function was wrong. 
	DI_ERR_COMMUNICATE,			//!< the spectrodensitometer failed to communicate with.
	DI_ERR_DEVICE_CONNECTION,	//!< connection mistakes.
	DI_ERR_DEVICE_MEMORY,		//!< the memories inside the spectrodesitometer were wrong.
	DI_ERR_DEVICE_RTC,			//!< the RTC device inside the spectrodesitometer was wrong.
	DI_ERR_CAL_DARK,			//!< the instument calibration failed in the dark level detection.  
	DI_ERR_CAL_UV_WLSHIFT,		//!< the instument calibration failed to correct the wavelength shift. 
	DI_ERR_CAL_WHITE,			//!< the instument calibration failed in the white level process.
	DI_ERR_SENSOR_TEMP,			//!< the temperture of the sensor inside the instrument was abnormal. 
	DI_ERR_LED_APPLYED_VOLTAGE,	//!< it was abnormal voltage to apply the actual measurement source LED inside the instrument.
	DI_ERR_OVER_EXPOSURE,		//!< it was too bright to measure the sample.
	DI_ERR_NUMERIC,				//!< it occured the error to compute.
	DI_ERR_NOT_CALIBRATED,		//!< it failed to measure because the instrument had not been calibrated yet.
	DI_ERR_PATCH_RECOGNITION,	//!< it failed to measure because the each pacthes failed to recognize indivisually.
	DI_ERR_MEASURING,			//!< the query failed to perform while the another one was running.
	DI_ERR_DATA_NOT_EXIST,		//!< the data failed to provide because of no measured data.
	DI_ERR_DEVICE_NOT_EXIST,	//!< the query failed to perform because the instrument which was selected was not found.
	DI_ERR_SYSTEM_VER,			//!< it failed to perform because that the combination of the version of the hardware and the software was not right. 

	DI_ERR_DEVICE_AD,			//!< the A/D device inside the spectrodesitometer was wrong.
	DI_ERR_MEAS_INTERRUPTED,	//!< spot measurement or calibration process was interrupted.
	DI_ERR_BATTERY_CELL,		//!< reserved. 
	DI_ERR_BATTERY_TEMP,		//!< reserved. 
	DI_ERR_CAL_RECOMMENDED,		//!< reserved. 
	DI_ERR_TARGETDATA_NOT_EXIST,//!< the data failed to provide because of no target data.

	DI_ERR_OBS_ILLUMINANT,		//!< it was wrong observer illuminant to caluclate the whiteness index or the brightness.
	DI_ERR_CALCULATE,			//!< The error occurred or the calculation have failed
	DI_ERR_COLORSET_NOT_EXIST,	//!< it is not registered a color set in the specified index number.
	DI_ERR_NOT_CALIBRATED_POLARIZATION,	//!< It was not performed a zero calibration for polarization filter.
	DI_ERR_BACKINGDATA_NOT_EXIST,	//!< It was not exist a backing data in the instrument
	DI_ERR_POLARIZATION_STATE,	//!< the state of polarization filter is different from measurement condition.
	DI_ERR_CONFLICTED_COLORSET,	//!< The  

	DI_NOT = 10000,
	DI_NOT_AVAILABLE,			//!< the query failed temporarily. 
	DI_NOT_SUPPORTED,			//!< the query failed because it was not supported in the present version.
	DI_NOT_APPLICABLE,			//!< the query failed because 

}
DI_ERROR_TYPES;


typedef int32_di DI_CAL_TYPES;		//!< definition of calibration type 
#define	DI_CAL_WLSHIFT  0x01		//!< the wavelength calibration
#define DI_CAL_ZERO_RAD	0x02		//!< the zero calibration for radiometric measurement
#define DI_CAL_WHITE	0x04		//!< the white calibration 
#define DI_CAL_ZERO_REF	0x10		//!< the zero calibration for reflectance measurement


#if defined(_WIN32) || defined(__APPLE__)
#pragma pack(pop) 
#endif

//! callback function to notify all kinds of event to 
typedef int32_di(*FP_DI_PROC_EVENT_HANDLER)(DI_EVENT EventCode, FD_DEVICE_HANDLE hDevice);


#ifndef FX_OPEN_APIS
#define FX_OPEN_APIS		

/*!
 * Creating the new handle to open the instrument if the new instrumet was opend.
 * @brief a handle creation 
 * @param[out]  hDevice an open handle to the instrument which was not opened.  
 * @return result of function 
 * @attention To control the another instrument, call this function again to 
 * get another handle to it. 

 */
DI_ERROR_TYPES FXAPI DIEnumDevice(FD_DEVICE_HANDLE* hDevice);

/*!
 * Closing the open handle 
 * @brief handle close
 * @param[in]  hDevice a valid handle to close 
 * @return DI_ERROR_TYPES
 * @attention 
 *	- must set FD_DEVICE_HANDLE to 0 after calling this API
 *	- must call this API for each of all open FD_DEVICE_HANDLEs indivisually before the system is closed.
 */
DI_ERROR_TYPES FXAPI DICloseDevice(FD_DEVICE_HANDLE hDevice);

#endif	// end of FX_OPEN_APIS


/*!
 * Getting all kinds of the measurement property 
 * @brief measurement property 
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] Type the requested property in  DIPROPERTIES 
 * @param[in,out] pParam a pointer to a varialble that receives a requested parameter
 * @return DI_ERROR_TYPES
 * @pre the property DI_PR_REMOTEMODE must be the remote mode
 */
DI_ERROR_TYPES FXAPI DIGetProperty(FD_DEVICE_HANDLE hDevice, DIPROPERTIES Type, int32_di* pParam);


/*!
 * Setting all kinds of the measurement property
 * @brief measurement property
 * @param[in] hDevice a vlid handle to control the instrument
 * @param[in] Type the requested proprety in  DIPROPERTIES
 * @param[in,out] pParam a pointer to a variable that contains a requested parameter
 * @return DI_ERROR_TYPES
 * @pre the property DI_PR_REMOTEMODE must be the remote mode
 */
DI_ERROR_TYPES FXAPI DISetProperty(FD_DEVICE_HANDLE hDevice, DIPROPERTIES Type, const int32_di* pParam);


/*!
 * Getting the status of all kinds of the buttons 
 * @brief buttons status
 * @param[in] hDevice a valid handle to control the instrument
 * @param[out] State a pointer to a KEYSTATUS[KEY_NO] that receives the status of each buttons 
 * @return DI_ERROR_TYPES
 * @pre the property DI_PR_REMOTEMODE must be the remote mode
 */
DI_ERROR_TYPES FXAPI DIGetButtonStatus(FD_DEVICE_HANDLE hDevice, KEYSTATUS State[KEY_NO]);


/*!
 * Measuring the sample according the measurement mode setting 
 * @brief measurement
 * @param[in] hDevice a valid handle to control
 * @return DI_ERROR_TYPES
 * @pre the property DI_PR_REMOTEMODE must be the remote mode
 */
DI_ERROR_TYPES FXAPI DIDoMeasurement(FD_DEVICE_HANDLE hDevice);


/*!
 * Performing the instrument calibration
 * @brief instrument calibration
 * @param[in] hDevice a vaild handle to control the instrument
 * @param[in] CalType  a requested kind calibration 
 * @return DI_ERROR_TYPES 
 * @pre the property DI_PR_REMOTEMODE must be the remote mode
 */
DI_ERROR_TYPES FXAPI DIDoCalibration(FD_DEVICE_HANDLE hDevice, DI_CAL_TYPES CalType);


/*!
 * Getting the number of the sample data to measure by the last measuremnt
 * @brief the number of data 
 * @param[in] hDevice a valid handle to control the instrument
 * @return the number of the sample data to measure
 * @pre the property DI_PR_REMOTEMODE must be the remote mode
 */
int32_di FXAPI DIGetNumSamples(FD_DEVICE_HANDLE hDevice);


/*!
 * Getting a spectal data to measure by the last measurement
 * @brief spectural data
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] Index a number that shows the requsted data to measure, which starts from 0. 
 * @param[out] pData a pointer to the buffer that receives the requested data
 * @param[in] nLength the length of the buffer 
 * @return DI_ERROR_TYPES
 * @pre the property DI_PR_REMOTEMODE must be the remote mode
 * @attention Index must be within the value DIGetNumSamples() returns.
 * @attention In spot measuremet, Index must be 0.
 */
DI_ERROR_TYPES FXAPI DIGetSpectrum(FD_DEVICE_HANDLE hDevice, int32_di Index, real* pData, int32_di nLength);


/*!
 *  Getting a colorimetric data to measure by the last measurement
 * @brief colorimetric data
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] Index a number that shows the requsted data to measurem, which starts from 0. 
 * @param[out] Color a pointer to the buffer that receives the requested colorimetry
 * @return DI_ERROR_TYPES 
 * @pre the property DI_PR_REMOTEMODE must be the remote mode
 * @attention Index must be within the value DIGetNumSamples() returns.
 * @attention In spot measuremet, Index must be 0.
 *  when DI_PR_COLORSPACE is COLORSPACE_CIELAB, Color has L*, a* and b*.
 *  when DI_PR_COLORSPACE is COLORSPACE_CIEXYZ, Color has X, Y and Z.
 *  when DI_PR_COLORSPACE is COLORSPACE_CIEXYY, Color has x, y and Y.
 *  when DI_PR_COLORSPACE is COLORSPACE_CIELCH, Color has L*, C* and h.
 *  when DI_PR_COLORSPACE is COLORSPACE_HUNTERLAB, Color has Hunter L, Hunter a and Hunter b.
 *  when DI_PR_MEASMODE is MEAS_IRRADIANCE, Color has Ev, T and ��uv.
 */
DI_ERROR_TYPES FXAPI DIGetColor(FD_DEVICE_HANDLE hDevice, int32_di Index, real Color[3]);



/*!
 * Getting a color index to measure by the last measurement
 * @brief color index
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] Index a number that shows the requsted data to measurem, which starts from 0. 
 * @param[out] ColorIndex a pointer to the buffer that receives the requested data.
 * @return DI_ERROR_TYPES 
 * @pre the property DI_PR_REMOTEMODE must be the remote mode
 * @attention Index must be within the value DIGetNumSamples() returns.
 * @attention In spot measuremet, Index must be 0.
 *	- when DI_PR_INDEX_TYPE is the white index, a ColorIndex has WI and Tint.
 *  - when DI_PR_INDEX_TYPE is the Brightness, a ColorIndex has Brightness.
 *  - when DI_PR_INDEX_TYPE is the Fluorescence Whitening Intensity, a ColorIndex has the Delta B(difference of the D65 brightness and UV-cut Brightness), D65 Brightness, and UV-cut Brightness.
 */
DI_ERROR_TYPES FXAPI DIGetColorIndex(FD_DEVICE_HANDLE hDevice, int32_di Index, real ColorIndex[3]);


/*!
 * Getting color densities to measure by the last measument. 
 * @brief color densities
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] Index a number that shows the requsted data to measurem, which starts from 0 
 * @param[out] pDensity a DI_DENSITY structure that receives the requested data. 
 * @return DI_ERROR_TYPES
 * @pre the property DI_PR_REMOTEMODE must be the remote mode
 * @attention Index must be within the value DIGetNumSamples() returns.
 * @attention In spot measuremet, Index must be 0.
 */
DI_ERROR_TYPES FXAPI DIGetDensity(FD_DEVICE_HANDLE hDevice, int32_di Index, DI_DENSITY* pDensity);

/*!
 * Getting pre radiance factor to measure by the last measument. 
 * @brief pre radiance factor
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] Index a number that shows the requsted data to measurem, which starts from 0 
 * @param[out] pPreRadFactor a DI_PRERADFACTOR structure that receives the requested data. 
 * @return DI_ERROR_TYPES
 * @pre the property DI_PR_REMOTEMODE must be the remote mode
 * @attention Index must be within the value DIGetNumSamples() returns.
 * @attention In spot measuremet, Index must be 0.
 */
DI_ERROR_TYPES FXAPI DIGetPreRadianceFactor(FD_DEVICE_HANDLE hDevice, int32_di index, DI_PRERADFACTOR* pPreRadFactor);

/*!
 * Log the data shown on the disply of the instrument.
 * @brief data report
 * @param[in] hDevice a valid handle to control the instrument
 * @param[out] pReort the pointer to the data will be redcived
 * @return DI_ERROR_TYPES
 * @pre the remote mode propterty  must be the logger mode.  
 */
DI_ERROR_TYPES FXAPI DIGetReport(FD_DEVICE_HANDLE hDevice, REPORT* pReort);

/*!
 * Registering  a user illuminant
 * @brief user illuminat registration
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] Illumination the pointer to the SPD of illuminant  
 * @return DI_ERROR_TYPES
 * @pre the property DI_PR_REMOTEMODE must be the remote mode
 */
DI_ERROR_TYPES FXAPI DIRegUserIlluminant(FD_DEVICE_HANDLE hDevice, real Illumination[ILLUMINANT_LEN]);

/*!
 * Getting the user illuminat that is registerd 
 * @brief 
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] Illumination a pointer to the buffer that receives 
 * @return DI_ERROR_TYPES
 * @pre the property DI_PR_REMOTEMODE must be the remote mode
 */
DI_ERROR_TYPES FXAPI DIGetUserIlluminant(FD_DEVICE_HANDLE hDevice, real Illumination[ILLUMINANT_LEN]);

/*!
 * Getting the version of the modules of the SDK
 * @brief version
 * @param[out] DLLVersion a pointer to VERSION structures that recieves the version
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DIGetVersion(VERSION DLLVersion[DLL_TYPES_NO]);

/*!
 * Getting all kinds of the instument information
 * @brief instrument information
 * @param[in] hDevice a valid handle to control the instrument
 * @param[out] pDeviceID a pointer to a DEVID structure that receives the instrument information 
 * @return DI_ERROR_TYPES
 * @pre the property DI_PR_REMOTEMODE must be the remote mode
 */
DI_ERROR_TYPES FXAPI DIGetDevInfo(FD_DEVICE_HANDLE hDevice, DEVID* pDeviceID);


/*!
 * Setting the hardware condition of the instrument
 * @brief hardware condtion setting
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in,out] pSetting a pointer to DISETTINGS structure that contains the requested condition information
 * @return DI_ERROR_TYPES 
 */
DI_ERROR_TYPES FXAPI DISetDeviceCondition(FD_DEVICE_HANDLE hDevice, const DISETTINGS* pSetting);

/*!
 * Getting the hardware condition of the instrument
 * @brief hardware condtion
 * @param[in] hDevice a valid handle to control the instrument
 * @param[out] pSetting a pointer to DISETTINGS structure that receives the condition information of   
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DIGetDeviceCondition(FD_DEVICE_HANDLE hDevice, DISETTINGS* pSetting);



/*! Getting the reference colors / densities to the instrument
 * @brief reference color/density
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] BankNo the bunk number to get the reference colors/densities 
 * @param[in] Type type of reference data, densities or colors
 * @param[out] pTarget a pointer to a DI_TARGET_DATA union receives the reference data
 * @return DI_ERROR_TYPES
 * @attention 
 *	- BankNo = DEFAULT_TARGET_BANK_NO(0) : to get the default tolerance
 *      - BankNo = 1 to 30 : the bank number of referece data
 * @attention 
 */
DI_ERROR_TYPES FXAPI DIGetTargetData(FD_DEVICE_HANDLE hDevice, int32_di BankNo, TARGET_TYPES Type, DI_TARGET_DATA* pTarget);


/*! Setting the reference colors / densities to the instrument
 * @brief reference color/density
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] BankNo the bunk number of the requested reference colors/densities
 * @param[in] Type type of reference data, densities or colors
 * @param[out] pTarget a pointer to a DI_TARGET_DATA union that contains the 
 * @attention 
 *	- BankNo = DEFAULT_TARGET_BANK_NO(0) : to set the default tolerance 
 *  	- BankNo = 1 to 30 : the bank number to set the reference data
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DISetTargetData(FD_DEVICE_HANDLE hDevice, int32_di BankNo, TARGET_TYPES Type, const DI_TARGET_DATA* pTarget);


/*! Clearing the reference colors / densities
 * @brief reference data clear
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] BankNo the bank number of the reference data to clear 
 * @param[in] Type type of reference data, densities or colors
 * @attention 
 *  	- BankNo = 1 ~ 30 : the bank number to 
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DIClearTargetData(FD_DEVICE_HANDLE hDevice, int32_di BankNo, TARGET_TYPES Type);


/*! Setting the measurement condition to the instrument
 * @brief measurement condition setting
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] pSettings a pointer to a MEASSETTINGS structure that contains the requested measurement condition 
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DISetMeasSettings(FD_DEVICE_HANDLE hDevice, const MEASSETTINGS* pSettings);


/*! Getting the measurement condition to the instrument 
 * @brief  measurement condition getting
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] pSettings a pointer to a MEASSETTINGS structure that receives the requested measurement condition
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DIGetMeasSettings(FD_DEVICE_HANDLE hDevice, MEASSETTINGS* pSettings);

/*! Getting total radiance factors under the selectable illuminant to measure by the last measurement
 * @brief  total radiance facotors getting
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] pSettings a pointer to a MEASSETTINGS structure that receives the requested measurement condition
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DIGetVirRadianceFactor(FD_DEVICE_HANDLE hDevice, int32_di Index, DI_RADFACTOR *pRadianceFactor, const DI_PRERADFACTOR *pPreRadFactor, DI_ILLUMINANTS Illuminant);

/*! Getting capability of instrument 
 * @brief  measurement condition getting
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] pSettings a pointer to a MEASSETTINGS structure that receives the requested measurement condition
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DIGetCapability(FD_DEVICE_HANDLE hDevice, uint32_di* pCapability);

/*! Analyzing capability information
 * @brief  capability information analyzing
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] pSettings a pointer to a MEASSETTINGS structure that receives the requested measurement condition
 * @return DI_ERROR_TYPES
 */
int32_di FXAPI DIAnalyzeCapability(FD_DEVICE_HANDLE hDevice, const uint32_di Capability, DI_CAPABILITY capabilityType);

/*! Setting the data of user calibration plate
 * @brief The data of user calibration plate setting
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] UserRefData The data of user calibration plate
 * @param[in] SerialNo Serial No. of user calibration plate
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DISetUserRefData(FD_DEVICE_HANDLE hDevice, real UserRefData[], int32_di SerialNo);

/*! Getting the data of user calibration plate
 * @brief The data of user calibration plate getting
 * @param[in] hDevice a valid handle to control the instrument
 * @param[out] UserRefData The data of user calibration plate
 * @param[out] SerialNo Serial No. of user calibration plate
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DIGetUserRefData(FD_DEVICE_HANDLE hDevice, real UserRefData[], int32_di* SerialNo);

/*! Getting the data of user calibration plate
 * @brief The data of user calibration plate getting
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] index the index number of the requested Color Set
 * @param[out] pColorSet the pointer to a DI_CERTIFICATION_COLOR_SET structure that receives the requested Color Set
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DIGetCertificationColorSet(FD_DEVICE_HANDLE hDevice, int32_di index, DI_CERTIFICATION_COLORSET *pColorSet);

/*! Register the Color Set
 * @brief Register the Color Set to the instrument
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] index the index number of the requested Color Set
 * @param[in] pColorSet the pointer to a DI_CERTIFICATION_COLOR_SET structure that register to the instrument
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DISetCertificationColorSet(FD_DEVICE_HANDLE hDevice, int32_di index, DI_CERTIFICATION_COLORSET *pColorSet);

/*! Clearing the color set
 * @brief Clear the Color Set that stores in the instrument
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] index the index number that will clear a Color Set
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DIClearCertificationColorSet(FD_DEVICE_HANDLE hDevice, int32_di index);

/*! Calculate Spot color density
 * @brief Calculate Spot color density
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] index the index number that will get spot color density
 * @param[in/out] pWavelength the wavelength to calculate spot color density 
 * @param[out] pSpotColorDensity Spot color density value which is calculated
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DIGetSpotColorDensity(FD_DEVICE_HANDLE hDevice, int32_di index, int32_di *pWavelength, real *pSpotColorDensity);

/*! Registering the backing conversion data to instrument
 * @brief Registering the backing conversion data
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] pBackingData the backing conversion data for registering to instrument
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DISetBackingConversionData(FD_DEVICE_HANDLE hDevice, DI_BACKING_DATA *pBackingData);

/*! Getting the backing conversion data which is stored in instrument
 * @brief Getting the backing conversion data
 * @param[in] hDevice a valid handle to control the instrument
 * @param[out] pBackingData the buffer for storing the acquired backing conversion data
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DIGetBackingConversionData(FD_DEVICE_HANDLE hDevice, DI_BACKING_DATA *pBackingData);

/*! Setting recognition method for polarization filter in instrument
 * @brief Setting for polarization filter
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] polFilterSetting Recognition type for polarization Filter in instrument
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DISetPolFilterCondition(FD_DEVICE_HANDLE hDevice, DI_POLFILTER_TYPE polFilterSetting);

/*! Getting the conditions for polarization filter in instrument
 * @brief Getting conditions for polarization filter
 * @param[in] hDevice a valid handle to control the instrument
 * @param[out] pPolFilterSetting Recognition type for polarization filter in instrument
 * @param[out] pPolStatus Recognition status for polarization filter in instrument
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DIGetPolFilterCondition(FD_DEVICE_HANDLE hDevice, DI_POLFILTER_TYPE *pPolFilterSetting, DI_POLFILTER_STATUS *pPolStatus);

/*! Getting all measurement conditions in the instrument 
 * @brief Getting all measurement conditons
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] pSettingsAll the buffer for storing the acquired measurement conditions from instrument
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DIGetMeasurementConditionAll(FD_DEVICE_HANDLE hDevice, DI_MEASSETTINGS_ALL *pSettingsAll);

/*! Setting all measurement conditions to the instrument 
 * @brief Setting all measurement conditions
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] pSettingsAll Measurement conditions for set to instrument
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DISetMeasurementConditionAll(FD_DEVICE_HANDLE hDevice, DI_MEASSETTINGS_ALL *pSettingsAll);

/*! Perform zero calibration for polarization filter
 * @brief Zero calibration for polarization filter
 * @param[in] hDevice a valid handle to control the instrument
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DIDoPolarizationCalib(FD_DEVICE_HANDLE hDevice);

/*! Getting calibration status in instrument
 * @brief Getting calibration status
 * @param[in] hDevice a valid handle to control the instrument
 * @param[in] status Calibration status in instrument
 * @return DI_ERROR_TYPES
 */
DI_ERROR_TYPES FXAPI DIGetCalibrateStatus(FD_DEVICE_HANDLE hDevice, int32_di* status);

/*!
 * @example ScanMeasurement.cpp
 * This is a example of how to use APIs for scanning measurement.
 */

/*!
 * @example SpotMeasurement.cpp
 * This is a example of how to use APIs for spot measurement of reflectance.
 */


/*!
 * @example IlluminanceMeasurement.cpp
 * This is a example of how to use APIs for illuminant measurement.
 */

/*!
 * @example InstrumentSetting.cpp
 * This is a example of how to use APIs for setting instrument.
 */

DI_ERROR_TYPES FXAPI DIStartTriggerScan(FD_DEVICE_HANDLE hDevice);
DI_ERROR_TYPES FXAPI DIStopTriggerScan(FD_DEVICE_HANDLE hDevice, int32_di* pNum);
DI_ERROR_TYPES FXAPI DIGetTriggerScanData(FD_DEVICE_HANDLE hDevice);

#if defined(__cplusplus)
}
#endif

#endif /* FX_API_H_*/
