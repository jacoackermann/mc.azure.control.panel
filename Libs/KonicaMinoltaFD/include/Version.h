/*!
 * Version 
 * Copyright (C) KONICA MINOLTA, INC. 2009-2013 All rights reserved.
 * @brief Eiger Application Programming Interface Fucntions
 * @file Version.h
 * @author KONICA MINOLTA, INC
 * @date Created on 2010/1/19
 */


#ifndef _EIGER_VERSION_H_
#define _EIGER_VERSION_H_

#if defined(_WIN32) || defined(__APPLE__)
#pragma pack(push,4) 
#endif

#include "FxTypeDef.h"

/*!
 * common version structure
 * @brief common version structure 
 * @attention the invalid number is 0.00.0000
 */
typedef struct tVERSION
{
	uint32_di Major;	//!< the major version number(2-digit)
	uint32_di Minor;	//!< the minor version number(2-digit)
	uint32_di Free;	//!< the reserve version number(4-digit) 
} VERSION;

/*!
 * type of programs of the body
 * @brief type of the firmware
 */
typedef enum tPRG_FIRM_TYPES
{
	PRG_FIRM_MAIN,	//!< the main program of the body
	PRG_FIRM_BOOT,	//!< the boot program of the bopy
	PRG_FIRM_SUB,	//!< the sub-program of the body
	PRG_FIRM_PLD,	//!< the sub-program of the body
	RPG_FIRM_NO,	//!< the number of programs of the body
}PRG_FIRM_TYPES;

/*!
 * versions for each of modules in SDK 
 */
typedef VERSION SDK_VERSION[4];

//! FIRMWARE versions
typedef VERSION FIRMVERSIONS[RPG_FIRM_NO];


#if defined(_WIN32) || defined(__APPLE__)
#pragma pack(pop) 
#endif

#endif
