/*!
 * Defines the report data format
 * Copyright (C) KONICA MINOLTA, INC. 2009-2013 All rights reserved.
 * @brief definitions of data types of report function  
 * @file Report.h
 * @author KONICA MINOLTA, INC
 * @date Created on 2010/04/12
 */


#ifndef REPORT_H_
#define REPORT_H_


#if defined(__cplusplus)
extern "C"{
#endif


#if defined(_WIN32) || defined(__APPLE__)
#pragma pack(push,4) 
#endif

//! the maximum number of the elements of REPORT 
#define MAX_REPORT_ELEMENTS		7


/*!
 * Definition of report data tag
 */
typedef enum enumCODE_TAG_REPORT
{
	CODE_TAG_REPORT_NULL			= 0x0000,	//!< Termination

	// Densities
	CODE_TAG_REPORT_D_RED			= 0x0100,	//!< absolute density C
	CODE_TAG_REPORT_D_GREEN			= 0x0101,	//!< absolute density M
	CODE_TAG_REPORT_D_BLUE			= 0x0102,	//!< absolute density Y
	CODE_TAG_REPORT_D_VISUAL		= 0x0103,	//!< absolute density K
	CODE_TAG_REPORT_D_MAJOR			= 0x0104,	//!< major color density
	CODE_TAG_REPORT_REL_D_RED		= 0x0105,	//!< relative density C from the density of the substrate 
	CODE_TAG_REPORT_REL_D_GREEN		= 0x0106,	//!< relative density M from the density of the substrate
	CODE_TAG_REPORT_REL_D_BLUE		= 0x0107,	//!< relative density Y from the density of the substrate
	CODE_TAG_REPORT_REL_D_VISUAL 	= 0x0108,	//!< relative density K from the density of the paper
	CODE_TAG_REPORT_PAPER_D_RED		= 0x0109,	//!< absolute red filtered density of the substrate
	CODE_TAG_REPORT_PAPER_D_GREEN	= 0x010a,	//!< absolute green filtered density of the substrate
	CODE_TAG_REPORT_PAPER_D_BLUE	= 0x010b,	//!< absolute blue filtered density of the substrate
	CODE_TAG_REPORT_PAPER_D_VISUAL	= 0x010c,	//!< absolute visual density of the substrate
	CODE_TAG_REPORT_D				= 0x010d,	//!< absolute density with unknown filter
	CODE_TAG_REPORT_REL_D			= 0x010e,	//!< relative density with unknown filter

	// Tone Value
	CODE_TAG_REPORT_TONE_VALUE		= 0x0110,	//!< tone value
	// Dot Gain
	CODE_TAG_REPORT_DOT_GAIN		= 0x0111,	//!< common dot gain
	CODE_TAG_REPORT_DOT_GAIN_H		= 0x0111,	//!< dot gain for high tone value range
	CODE_TAG_REPORT_DOT_GAIN_M		= 0x0112,	//!< dot gain for middle tone value range
	CODE_TAG_REPORT_DOT_GAIN_L		= 0x0113,	//!< dot gain for low tone range
	CODE_TAG_REPORT_DOT_GAIN_RED	= 0x0114,	//!< dot gain for red
	CODE_TAG_REPORT_DOT_GAIN_GREEN	= 0x0115,	//!< dot gain for green
	CODE_TAG_REPORT_DOT_GAIN_BLUE	= 0x0116,	//!< dot gain for blue
	CODE_TAG_REPORT_DOT_GAIN_VISUAL	= 0x0117,	//!< dot gain for visual

	// Spot color density
	CODE_TAG_REPORT_SPOTCOLOR_D     = 0x0120,	//!< absolute spot color density
	CODE_TAG_REPORT_REL_SPOTCOLOR_D = 0x0520,	//!< relative spot color density from the density of the substrate

	// Density differece
	CODE_TAG_REPORT_D_RED_DE		= 0x1100,	//!< difference of absolute density with red filter
	CODE_TAG_REPORT_D_GREEN_DE		= 0x1101,	//!< difference of absolute density with green filter
	CODE_TAG_REPORT_D_BLUE_DE		= 0x1102,	//!< difference of absolute density with blue filter
	CODE_TAG_REPORT_D_VISUAL_DE		= 0x1103,	//!< difference of absolute density with visual filter
	CODE_TAG_REPORT_D_MAJOR_DE		= 0x1104,	//!< difference of absolute density with major filter
	CODE_TAG_REPORT_REL_D_RED_DE	= 0x1105,	//!< difference of relative density with red filter
	CODE_TAG_REPORT_REL_D_GREEN_DE	= 0x1106,	//!< difference of relative density with green filter
	CODE_TAG_REPORT_REL_D_BLUE_DE	= 0x1107,	//!< difference of relative density with blue filter
	CODE_TAG_REPORT_REL_D_VISUAL_DE = 0x1108,	//!< difference of relative density with visual filter

	// Spot color density differece
	CODE_TAG_REPORT_SPOTCOLOR_D_DE     = 0x1120,//!< difference of absolute spot color density
	CODE_TAG_REPORT_REL_SPOTCOLOR_D_DE = 0x1520,//!< difference of relative spot color density

	// color
	CODE_TAG_REPORT_XYZ_X			= 0x0A00,	//!< CIE X
	CODE_TAG_REPORT_XYZ_Y			= 0x0A01,	//!< CIE Y
	CODE_TAG_REPORT_XYZ_Z			= 0x0A02,	//!< CIE Z
	CODE_TAG_REPORT_xyY_x			= 0x0A03,	//!< CIE x
	CODE_TAG_REPORT_xyY_y			= 0x0A04,	//!< CIE y
	CODE_TAG_REPORT_xyY_Y			= 0x0A05,	//!< CIE Y
	CODE_TAG_REPORT_LAB_L			= 0x0A06,	//!< CIE L*
	CODE_TAG_REPORT_LAB_A			= 0x0A07,	//!< CIE a*
	CODE_TAG_REPORT_LAB_B			= 0x0A08,	//!< CIE b*
	CODE_TAG_REPORT_LAB_C			= 0x0A09,	//!< CIE C*
	CODE_TAG_REPORT_LAB_H			= 0x0A0A,	//!< CIE h
	CODE_TAG_REPORT_HUNTER_L		= 0x0A0B,	//!< Hunter L
	CODE_TAG_REPORT_HUNTER_A		= 0x0A0C,	//!< Hunter a
	CODE_TAG_REPORT_HUNTER_B		= 0x0A0D,	//!< Hunter b
	// color difference
	CODE_TAG_REPORT_XYZ_X_DE		= 0x1A00,	//!< delta X
	CODE_TAG_REPORT_XYZ_Y_DE		= 0x1A01,	//!< delta Y
	CODE_TAG_REPORT_XYZ_Z_DE		= 0x1A02,	//!< delta Z
	CODE_TAG_REPORT_xyY_x_DE		= 0x1A03,	//!< delta x
	CODE_TAG_REPORT_xyY_y_DE		= 0x1A04,	//!< delta y
	CODE_TAG_REPORT_xyY_Y_DE		= 0x1A05,	//!< delta Y
	CODE_TAG_REPORT_LAB_L_DE		= 0x1A06,	//!< delta L*
	CODE_TAG_REPORT_LAB_A_DE		= 0x1A07,	//!< delta a*
	CODE_TAG_REPORT_LAB_B_DE		= 0x1A08,	//!< delta b*
	CODE_TAG_REPORT_LAB_C_DE		= 0x1A09,	//!< delta C*
	CODE_TAG_REPORT_LAB_H_DE		= 0x1A0A,	//!< delta H*
	CODE_TAG_REPORT_HUNTER_L_DE		= 0x1A0B,	//!< Hunter delta L
	CODE_TAG_REPORT_HUNTER_A_DE		= 0x1A0C,	//!< Hunter delta a
	CODE_TAG_REPORT_HUNTER_B_DE		= 0x1A0D,	//!< Hunter delta b
	// delta E
	CODE_TAG_REPORT_LAB_DE			= 0x2A01,	//!< delta E*
	CODE_TAG_REPORT_LAB_DE_94		= 0x2A02,	//!< delta E*94
	CODE_TAG_REPORT_LAB_DE_CMC		= 0x2A03,	//!< delta E*CMC
	CODE_TAG_REPORT_LAB_DE_2000		= 0x2A04,	//!< delta E2000
	CODE_TAG_REPORT_HUNTER_DE		= 0x2A05,	//!< Hunterdelta E
	// brightness
	CODE_TAG_REPORT_B_ISO			= 0x0C00,	//!< ISO brightness
	CODE_TAG_REPORT_B_D65			= 0x0C01,	//!< D65 brightness
	CODE_TAG_REPORT_B_420			= 0x0C02,	//!< brightness with 420nm cut filter
	CODE_TAG_REPORT_F_PAPER			= 0x0C03,	//!< Fluorescence whitening intensity	
	// Whiteness
	CODE_TAG_REPORT_WI_CIE			= 0x0C10,	//!< WI CIE
	CODE_TAG_REPORT_TINT_CIE		= 0x0C11,	//!< TInt CIE
	CODE_TAG_REPORT_WI_ASTM			= 0x0C12,	//!< WI ASTM
	CODE_TAG_REPORT_TINT_ASTM		= 0x0C13,	//!< Tint ASTM
	// Index difference
	CODE_TAG_REPORT_B_ISO_DE		= 0x1C00,	//!< difference of ISO Brightness
	CODE_TAG_REPORT_B_D65_DE		= 0x1C01,	//!< difference of D65 Brightness 
	CODE_TAG_REPORT_B_420_DE		= 0x1C02,	//!< difference of Brightness with 420nm cutoff filter 
	// illuminance
	CODE_TAG_REPORT_xyEv_Ev			= 0x1D00,	//!< Ev
	CODE_TAG_REPORT_xyEv_x			= 0x1D01,	//!< x
	CODE_TAG_REPORT_xyEv_y			= 0x1D02,	//!< y
	// luminance
	CODE_TAG_REPORT_xyLv_Lv			= 0x1E00,	//!< Lv
	CODE_TAG_REPORT_xyLv_x			= 0x1E01,	//!< x
	CODE_TAG_REPORT_xyLv_y			= 0x1E02,	//!< y
	// Tdeltauv
	CODE_TAG_REPORT_Tduv_T			= 0x1F00,	//!< Tcp
	CODE_TAG_REPORT_Tduv_duv		= 0x1F01,	//!< duv
	// Gray ballance
	CODE_TAG_REPORT_GLAYBALANCE_LAB_A_DE_1ST	= 0x0C20,//!< 1st difference between measured a* and Wanted_a*
	CODE_TAG_REPORT_GLAYBALANCE_LAB_B_DE_1ST	= 0x0C21,//!< 1st difference between measured b* and Wanted_b*
	CODE_TAG_REPORT_GLAYBALANCE_LAB_A_DE_2ND	= 0x0C22,//!< 2nd difference between measured a* and Wanted_a*
	CODE_TAG_REPORT_GLAYBALANCE_LAB_B_DE_2ND	= 0x0C23,//!< 2nd difference between measured b* and Wanted_b*
	CODE_TAG_REPORT_GLAYBALANCE_LAB_A_DE_3RD	= 0x0C24,//!< 3rd difference between measured a* and Wanted_a*
	CODE_TAG_REPORT_GLAYBALANCE_LAB_B_DE_3RD	= 0x0C25,//!< 3rd difference between measured b* and Wanted_b*
	// midtone spread
	CODE_TAG_REPORT_MIDTONESPREAD	= 0x0C26,	//!< midtone spread
	// Trapping																									
	CODE_TAG_REPORT_TRAP			= 0x0B00,	//!< trapping

} CODE_TAG_REPORT;

/*!
 * structure of element of report data
 * @brief structure of element of report data
 */
typedef struct tREPROTELEMENT
{
	CODE_TAG_REPORT		Tag;		//!< the tag of data
	real			Value;		//!< value of data type Tag shows
}
REPORTELEMENT;

/*!
 * report data structure
 */
typedef struct tREPORT
{
	int32_di		Num;				//!< the number of avalilable elements
	REPORTELEMENT	Element[MAX_REPORT_ELEMENTS];	//!< element of report
}
REPORT;


#if defined(_WIN32) || defined(__APPLE__)
#pragma pack(pop) 
#endif


#if defined(__cplusplus)
}
#endif

#endif /* REPORT_H_ */
