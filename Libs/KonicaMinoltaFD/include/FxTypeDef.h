/*!
 * The header file to redefine a type of variable each OS (Windows/MacOS), each CPU type (32bit/64bit)
 * Copyright (C) KONICA MINOLTA, INC. 2011-2013 All rights reserved.
 * @brief Data definition
 * @file FxTypeDef.h
 * @author KONICA MINOLTA, INC
 * @date Created on 2011/02/17
 */
#ifndef __FX_TYPE_H__
#define __FX_TYPE_H__



typedef float real_calib;



#if defined(_WIN32) || defined(_WIN64)
	#include <windows.h>		// The define for Windows OS (WORD,DWORD etc.)
	#define KMAPI __stdcall		//Windowsはstdcall
#elif defined(__APPLE__)
	#include <wchar.h>
	#define KMAPI				//Windows以外は_cdecl
	#define TRUE 1
	#define FALSE 0
#else
	#define KMAPI				//Windows以外は_cdecl
	#define TRUE 1
	#define FALSE 0
#endif

#if defined(_WIN32)
	/* For Windows OS [32bit] */
	typedef	char				int8_di;			// 8bit (signed)
	typedef	unsigned char		uint8_di;			// 8bit (unsigned)
	typedef	short				int16_di;			// 16bit(signed)
	typedef	unsigned short		uint16_di;			// 16bit(unsigned)
	typedef	int					int32_di;			// 32bit(signed)
	typedef	unsigned int		uint32_di;			// 32bit(unsigned)
	typedef	long long			int64_di;			// 64bit(signed)
	typedef	unsigned long long	uint64_di;			// 64bit(unsigned)
#elif defined(_WIN64)
	/* For Windows OS [64bit] */
	typedef	char				int8_di;			// 8bit (signed)
	typedef	unsigned char		uint8_di;			// 8bit (unsigned)
	typedef	short				int16_di;			// 16bit(signed)
	typedef	unsigned short		uint16_di;			// 16bit(unsigned)
	typedef	int					int32_di;			// 32bit(signed)
	typedef	unsigned int		uint32_di;			// 32bit(unsigned)
	typedef	long long			int64_di;			// 64bit(signed)
	typedef	unsigned long long	uint64_di;			// 64bit(unsigned)
#elif defined(__APPLE__)
	/* For MacOS [64bit] */
	typedef	char				int8_di;			// 8bit (signed)
	typedef	unsigned char		uint8_di;			// 8bit (unsigned)
	typedef	short				int16_di;			// 16bit(signed)
	typedef	unsigned short		uint16_di;			// 16bit(unsigned)
	typedef	int					int32_di;			// 32bit(signed)
	typedef	unsigned int		uint32_di;			// 32bit(unsigned)
	typedef	long long			int64_di;			// 64bit(signed)
	typedef	unsigned long long	uint64_di;			// 64bit(unsigned)
	typedef uint16_di			WORD;
	typedef uint32_di			DWORD;
	typedef uint8_di			BYTE;
#else
	/* For MacOS [32bit] */	/* with Firmware */
	typedef	char				int8_di;			// 8bit (signed)
	typedef	unsigned char		uint8_di;			// 8bit (unsigned)
	typedef	short				int16_di;			// 16bit(signed)
	typedef	unsigned short		uint16_di;			// 16bit(unsigned)
	typedef	int					int32_di;			// 32bit(signed)
	typedef	unsigned long		uint32_di;			// 32bit(unsigned)
	typedef	long long			int64_di;			// 64bit(signed)
	typedef	unsigned long long	uint64_di;			// 64bit(unsigned)

	typedef uint16_di			WORD;
	typedef uint32_di			DWORD;
	typedef uint8_di			BYTE;
	
#endif


#endif /*__FX_TYPE_H__*/
