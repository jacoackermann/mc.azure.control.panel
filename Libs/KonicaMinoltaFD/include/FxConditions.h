/*!
 * conditions of FD-7/FD-5 device  
 * Copyright (C) KONICA MINOLTA, INC. 2011-2013 All rights reserved.
 * @brief FD-7/FD-5 Condition definitions
 * @file FxConditions.h
 * @author KONICA MINOLTA, INC
 * @date Created on 2010/03/03
 */

#ifndef _FX_CODITIONS_H_
#define _FX_CODITIONS_H_

#include "ColorConditions.h"
#include "Version.h"


//! definition of type real
typedef float real;

#if defined(_WIN32)
	/* For Windows OS [32bit] */
	#include <wchar.h>
#elif defined(_WIN64)
	/* For Windows OS [64bit] */
	typedef uint16_di wchar_t;
#elif defined(__APPLE__)
	/* For MacOS [64bit] */ /* For MacOS [32bit] */
	#include <wchar.h>
#else
	/* with Firmware */
	#include <wchar.h>
#endif

#if defined(_WIN32) || defined(__APPLE__)
#pragma pack(push,4) 
#endif

//! code of spectral data
#define spctcode(interval,max,min)	((interval<<16)|((max/10)<<8)|(min/10)) 
#define TARGETNAME_SIZE 12			//Size of Target name

/*!
 * type of spctral data code
 */
typedef enum tSPCTTYPES
{
	SPCT_01_3873 = spctcode(1,380,730),	// from 380nm to 730nm, 1 nm inteval 
	SPCT_05_3873 = spctcode(5,380,730),	// from 380nm to 730nm, 5 nm inteval 	
	SPCT_10_3873 = spctcode(10,380,730),	// from 380nm to 730nm, 10 nm inteval 
	SPCT_05_3073 = spctcode(5,300,730),	// from 300nm to 730nm, 5 nm inteval 
	SPCT_10_3073 = spctcode(10,300,730),	// from 300nm to 730nm, 10 nm inteval 
	SPCT_10_3674 = spctcode(10,360,730),	// from 360nm to 730nm, 10 nm inteval  
}
SPCTTYPES;

/*!
 * measurement mode
 */
typedef enum tDIMEASMODE
{
	MEAS_REF_SPOT = 0,		//!< one shot measurement for reflectrance
	MEAS_REF_SCAN,			//!< scan measurement for reflectance 
	MEAS_RADIANCE,			//!< measurement for radiometry
	MEAS_IRRADIANCE,		//!< measurement for illuminance
	MEAS_TRIGGER_SCAN,		//!< scan measurement for trigger

	DIMEASMODE_FIRST = MEAS_REF_SPOT,
	DIMEASMODE_LAST = MEAS_TRIGGER_SCAN,
	DIMEASMODE_DEFAULT = MEAS_REF_SPOT,
}DIMEASMODE;

/*!
 * remote mode
 * @brief remote mode
 */
typedef enum tDIREMOTEMODE
{
	DI_RMODE_LOGGER,	//!< logger mode
	DI_RMODE_REMOTE,	//!< remote mode
	DI_RMODE_NO,

	DI_RMODE_FIRST = DI_RMODE_LOGGER,
	DI_RMODE_LAST = DI_RMODE_REMOTE,
	DI_RMODE_DEFAULT = DI_RMODE_LOGGER,
}
DIREMOTEMODE;

/*!
 * type of calibration mode
 * @brief type of calibration mode
 */
typedef enum tDI_CALIBRATEMODE
{
	DI_CALIBRATEMODE_WHITE,			//!< A calibration by using white calibration plate bundled with FD-7
	DI_CALIBRATEMODE_USER,			//!< A calibration by using calibration plate which is not bundled with FD-7

	DI_CALIBRATEMODE_FIRST = DI_CALIBRATEMODE_WHITE,
	DI_CALIBRATEMODE_LAST = DI_CALIBRATEMODE_USER,
	DI_CALIBRATEMODE_DEFAULT = DI_CALIBRATEMODE_WHITE
}
DI_CALIBRATEMODE;

/*!
 * type of measurement condition
 * @brief type of measument condtion
 * @attention see ISO13655
 * @attention MEASCOND_M3 is not supported and not available.
 */
typedef enum tMEASCONDITIONS
{
	MEASCOND_M0 = 0,					//!< M0 (light source CIE A illuminant)
	MEASCOND_M1,						//!< M1 (light source CIE D50 illuminant)
	MEASCOND_M2,						//!< M2 (light source CIE A illuminat with UV cutoff filter at 410nm)
	MEASCOND_M3,						//!< M3 (light source actual device illuminant with polarizers)
	
	MEASCOND_FIRST = MEASCOND_M0,
	MEASCOND_LAST = MEASCOND_M3,
	MEASCOND_DEFAULT = MEASCOND_M0,				//!< default 
}
MEASCONDITIONS;

/*!
 * type of button status
 * @brief type of button status
 */
typedef enum tKEYSTATUS
{
	KEY_STATE_OFF,			//!< OFF
	KEY_STATE_ON,			//!< ON
	KEY_NOT_AVILABLE,		//!< not available
}
KEYSTATUS;

/*!
 * type of button
 * @brief type of button
 */
typedef enum tKEY_TYPES
{
	KEY_MEAS_BUTTON,		//!< measurement button, which is used in scan measurement, illumiant measurement
	KEY_DETECT_KEY,			//!< detection key, which is used in one shot measurement for reflectance
	KEY_NO,				
}
KEY_TYPES;

/*!
 *  type of beep status
 */
typedef enum tBEEP {
	BEEP_OFF,	//!< OFF
	BEEP_ON		//!< ON
} BEEP;

/*!
 *  status of physical filter equipment 
 */
typedef enum tPHYSICAL_FILTER
{
	FILTER_NOT = 0,					//!< not equipped 
	FILTER_POL,					//!< equipping the polarizing filter 
	FILTER_UV,					//!< equipping the UV cut filter (not supported)

	PHYSFILTER_FIRST = FILTER_NOT,
	PHYSFILTER_LAST = FILTER_UV,
	FILTER_DEFAULT = FILTER_NOT,			
}
PHYSICAL_FILTER;

/*!
 * display setting strucure
 * @brief display settings structure
 * @attention Contrast is not supported and must be 0.
 */
typedef struct tLCDPARM
{
	enum LCD{Normal, Inverse}	View;		//!< view directtion
	int32_di Contrast;							//!< contrast level
}LCDPARM;

typedef LCDPARM DISPLAY;			//!< display of the device		

/*!
 * structure for datetime settings
 * @brief week setting 
 */

typedef	enum eWeek
{
	Sunday,
	Monday,
	Tuesday,
	Wednesday,
	Thursday,
	Friday,
	Saturday
}WEEK;

typedef struct tDEVDATETIME
{
	uint32_di Year;		//!< the year number from 2010
	uint32_di Month;		//!< the month number from 1 to 12
	uint32_di Day;		//!< the day number from 1 to 31
	uint32_di Hour;		//!< the hour number from 0 to 23
	uint32_di Minute;		//!< the minute number from 0 to 59
	uint32_di Second;		//!< the sencond number from 0 to 59
	WEEK Week; //!< kind of day
}	
DEVDATETIME;

typedef DEVDATETIME DIDATETIME;			//!< definition of DIDATETIME

/*!
 *  type of language in the display
 *  @attention ISO639 code
 */
typedef enum tLANGCODE
{
	LANG_ENG,					//!< Engilish
	LANG_JPN,					//!< Japanese
	LANG_DEU,					//!< German
	LANG_FRE,					//!< French
	LANG_CHI,					//!< Chinese 
	LANG_SPA,					//!< Spanish 
	LANG_NO,					//!< the number of availabe languages
	LANG_DEFAULT = LANG_ENG,	//!< default 
}
LANGCODE;

/*!
 * type of calibration warning settings
 */
typedef enum tUSERCAL_LIMIT
{
	USERCAL_LIMIT_3H,			//!< 3 hrs. later after user calibration
	USERCAL_LIMIT_6H,			//!< 6 hrs. later after user calibration
	USERCAL_LIMIT_12H,			//!< 12 hrs. later after user calibration
	USERCAL_LIMIT_24H,			//!< 24 hrs. later after user calibration
	USERCAL_LIMITLESS,			//!< not be shown
	USERCAL_LIMIT_NUM,			//!< the number of availabe types
	USERCAL_LIMIT_DEFAULT = USERCAL_LIMIT_12H,	//!< default 
	USERCAL_LIMIT_FIRST = USERCAL_LIMIT_3H,
	USERCAL_LIMIT_LAST = USERCAL_LIMITLESS,
}
USERCAL_LIMIT;

/*!
 *  type of factory warning status
 */
typedef enum tFACTORYCAL_WRN {
	FACTORYCAL_WRN_ON,		//!< ON 
	FACTORYCAL_WRN_OFF		//!< OFF
} FACTORYCAL_WRN;






/*!
 * type of date format
 */
typedef enum tTYPE_DATEFORMAT {
	TYPE_YYMMDD,				//!< big endian forms, starting with the year
	TYPE_MMDDYY,				//!< middle endian forms, starting with the month
	TYPE_DDMMYY,				//!< little endian forms, starting with the day
	DATEFORMAT_DEFAULT = TYPE_YYMMDD,	//!< default 
	DATEFORMAT_FIRST = TYPE_YYMMDD,
	DATEFORMAT_LAST = TYPE_DDMMYY,
}
TYPE_DATEFORMAT;

//! product code
typedef uint32_di	DEV_PRODUCT_CODE;
//! factory code
typedef int8_di		DEV_FACTORY_CODE[1];
//! serial number 
typedef uint32_di	DEV_SERIAL_CODE;
//! accessories code
typedef uint32_di	DEV_ACC_CODE;
//! variation code
typedef uint32_di	DEV_VARIATION_CODE; 

/*!
 * instrument information structure
 */
typedef struct tDEVID
{
	int8_di 			Product[4];		//!< product code
	DEV_FACTORY_CODE	Factory;		//!< factory code
	DEV_SERIAL_CODE		Serial;			//!< serial number 
	VERSION			Firmware[RPG_FIRM_NO];	//!< version of the firmware
	DEV_ACC_CODE		WhiteRefNo;		//!< product number of the white referece borad
	DEV_ACC_CODE		Illuminator;		//!< product number of the ambient light adapter
	DEV_ACC_CODE		PolFilter;		//!< product number of the polarizing filter
	DIDATETIME			FactoryDate;	//!< the date when the device was cariblated at the factory of the manufacture
	DEV_VARIATION_CODE	Variation;		//!< variation code
}DEVID;


/*!
 *	the moudules that the SDK contains 
 */
typedef enum tDLL_TYPES
{
	DLL_LIB_FX = 0,		//!< for providing the interface to the application
	DLL_LIB_DI_CALIB,		//!< for operating calibration and measurement
	DLL_LIB_DI_COM,			//!< for communicating with the instrument
	DLL_LIB_DI_CALCOLOR,		//!< for computing colorimatery and so on. 
	DLL_TYPES_NO,
}
EIGER_DLL_TYPES;

/*!
 *	DISETTINGS parameter definition
 */
typedef	enum eSettingType
{
	COND_DATETIME,			//!< Data and time setting
	COND_DISPLAY,			//!< LCD display setting
	COND_BEEP,				//!< Beep setting
	COND_LANGUAGE,			//!< Language setting
	COND_FILTER,			//!< Physical filter setting
	COND_DATEFORMAT,		//!< Date format setting
	COND_UCAL_LIMIT,		//!< --------
} SETTINGTYPE;

/*!
 * structure which contains each of device settings   
 * @brief stdevice setting parameter
 * @attention COND_FILTER and PHYSICAL_FILTER are not supported now. Must not use them.
 */
typedef struct tDISETTINGS
{
	/* Select valid parameter */
	SETTINGTYPE SettingType;

	/*!
	 * parameter union for settings
	 */
	union tPARAMS
	{
		DIDATETIME	Datetime;	//!< DIDATETIME which you need to set/obtain for the datetime when you should select COND_DATETIME
		DISPLAY		Display;	//!< DISPLAY which you need to set/obtain for the display setting when you should select COND_DISPLAY 
		BEEP		Beep;		//!< BEEP which you need to set/obtain for beep setting	for when you should select COND_BEEP
		LANGCODE	Lang;		//!< LANGCODE which you need to set/obtain for displaying language when you should select COND_LANGUAGE
		PHYSICAL_FILTER PhysicalFilter;		//!< PHYSICAL_FILTER  which you need to set/obtain for filter when you should select COND_FILTER
		TYPE_DATEFORMAT	DateFormat;			//!< TYPE_DATEFORMAT which you need to set/obtain for ...
		USERCAL_LIMIT	UserCalLimit;		//!< USERCAL_LIMIT which you need to set/obtain for ...
		//
	}
	PARAM;
}
DISETTINGS;

#define TARGET_BANK_START		1	//!< start index of the target banks
#define TARGET_BANK_END			30	//!< end index of the target banks

#define COLORSET_START			0	//!< start index of the Color Set
#define COLORSET_END			49	//!< end index of the Color Set

//! the number of banks of target data 
#define TARGET_DATA_BANK_NUM	(TARGET_BANK_END+1)

//! the number of banks of the target data for density
#define TARGET_DENSITY_BANK_NUM	TARGET_DATA_BANK_NUM
//! the number of banks of the target data for color
#define TARGET_COLOR_BANK_NUM

//! a bank number whose bank has defult target values
#define DEFAULT_TARGET_BANK_NO	0
//! definition 
#define TARGET_BANK_NO(bank_no)	(bank_no+1)

//! the maximum name length of the certification color set
#define COLOR_SET_NAME_SIZE 12

//! the maximum name length of the certification color data
#define COLOR_DATA_NAME_SIZE 9

//! the number of the certification color data
#define COLOR_DATA_NUM	15

//! Calibration status
#define CALIB_STATUS_NA					0	//!< Calibration is not applicable
#define CALIB_STATUS_COMPLETED			1	//!< Calibration is completed. So calibration is not required.
#define CALIB_STATUS_RECOMMENDED		2	//!< Although calibration have completed, calibration is recommended to ensure good measurement accuracy.
#define CALIB_STATUS_WHITE_UNCOMPLETED	3	//!< White calibration is not performed. So white calibration must be performed.
#define CALIB_STATUS_ZERO_UNCOMPLETED	4	//!< Zero calibration is not performed. So zero calibration must be performed.
#define CALIB_STATUS_REQUIRED			5	//!< Although calibration is completed, calibration is required to ensure good measurement accuracy.

/*! target density strucute
 * @brief target density structure 
 */
typedef struct tDITARGETDENSITY
{
	real	Tolerance[DENFILTER_NO];					//!< each tolerances for r/g/b filter  
	int32_di		EnableTolerance[DENFILTER_NO];		//!< the flag to decide if available
	int8_di			 Name[TARGETNAME_SIZE+1];			//!< Data name
	uint8_di	 MeasSrc;								//!< The measurement source for the density measurement
	DI_DENSITYSTATUS Status;							//!< density status
	DI_DENSITY		 TargetDensity;						//!< the reference values of the denisties, which are to be absolute densities
}
DI_TARGET_DENSITY;

/*! target color structure
 * @brief target color structure 
 * @attention only CIE XYZ can be available.
 */
typedef struct tDITARGETCOLOR
{
	real			Tolerance;					//!< torerance for each kinds of delta E  
	int8_di			Name[TARGETNAME_SIZE+1];	//!< Data name
	uint8_di	MeasSrc;						//!< The measurement source for the color measurement
	DI_ILLUMINANTS  ObsIlluminant;				//!< illuminant when the target colors were obtained  
	DI_OBSERVER		Obs;						//!< observer angle when the target colors were obtained
	DI_COLORSPACES  ColorSpace;					//!< Color space
	real			TargetColor[3];				//!< target/reference colors which can be CIE XYZ only
}
DI_TARGET_COLOR;

/*!
 *	the types of target data, 
 */
typedef enum eTARGETTYPES
{ 
	TARGET_TYPE_DENSITY = 1,	//!< for density target	 
	TARGET_TYPE_COLOR,			//!< for color target
}
TARGET_TYPES;


/*! target data union
 * @brief target data union  
 */
typedef union uDITARGETDATA
{
	DI_TARGET_DENSITY	TargetDensity;			//!< the values are used when the target densities are set
	DI_TARGET_COLOR		TargetColor;			//!< the values are used when the target colors are set
	real			DefaultTolerance[4];		//!< the values are used when the default tolerances are set
}
DI_TARGET_DATA;


/*!
 * the kinds of diplaying output which was measured
 */ 
typedef enum tDISP_TYPES
{
  DISP_ABS,		//!< absolute values display
  DISP_DIFF,	//!< comparison display 
  DISP_JUDGE,	//!< judgement output display
}
DISP_TYPES;

/*!
 * the type of backing
 */
typedef enum enumBACKING_TYPE
{
	DI_BACKING_UNKNOWN,		//!< unknown / undefined
	DI_BACKING_BLACK,		//!< black backing
	DI_BACKING_WHITE,		//!< white backing

	DI_BACKING_FIRST		= DI_BACKING_UNKNOWN,
	DI_BACKING_LAST			= DI_BACKING_WHITE,
	DI_BACKING_DEFAULT		= DI_BACKING_UNKNOWN,
}
DI_BACKING;

/*!
 * Setting of backing conversion
 */
typedef enum eBACKING_CONVERSION
{
	DI_BACKING_CONVERSION_OFF,
	DI_BACKING_CONVERSION_ON,
	DI_BACKING_CONVERSION_DEFAULT = DI_BACKING_CONVERSION_OFF,
}
DI_BACKING_CONVERSION;

/*!
 * Calculation method in trapping measurement
 */
typedef enum eTRAPPING_TYPE
{
	TRAPPING_BRUNNER,
	TRAPPING_PREUCIL,
	TRAPPING_DEFAULT = TRAPPING_PREUCIL
}
DI_TRAPPING_TYPE;

/*!
 * Type of calculation for Spot color density
 */
typedef enum eSPOTDENSITY_TYPE
{
	SPOTDENSITY_AUTO,	//!< Detect the wavelength which is Dmax automatically
	SPOTDENSITY_FIX,	//!< Specify any wavelength (380-730 nm :1nm pitch)
	SPOTDENSITY_DEFAULT = SPOTDENSITY_AUTO
}
DI_SPOTDENSITY_TYPE;

/*!
 * Type of recognition for polarization Filter in instrument
 */
typedef enum ePOLFILTER_TYPE
{
	POLARIZATION_FILTER_NOT = 0,		//!< Recognizing with not attaching the polarization filter
	POLARIZATION_FILTER_HAVE,			//!< Recognizing with attaching the polarization filter
	POLARIZATION_FILTER_AUTO,			//!< Recognizing automatically in white calibration
	POLARIZATION_FILTER_DEFAULT = POLARIZATION_FILTER_AUTO
}
DI_POLFILTER_TYPE;

/*!
 * Status of polarization filter which is recognized in instrument
 */
typedef enum ePOLFILTER_STATUS
{
	POLFILTER_STATUS_NOT = 0,			//!< Status which polarization filter is not attached	
	POLFILTER_STATUS_HAVE,				//!< Status which polarization filter is attached	
	POLFILTER_STATUS_DEFAULT = POLFILTER_STATUS_NOT
}
DI_POLFILTER_STATUS;

/*!
 * Ink type which estimate in Target Match measurement
 */
typedef enum eDI_TARGETMATCH_DENS_TYPE
{
	TARGETMATCH_DENS_TYPE_AUTO = 0,
	TARGETMATCH_DENS_TYPE_SPOT,
	DI_TARGETMATCH_DENS_TYPE_DEFAULT = TARGETMATCH_DENS_TYPE_AUTO
}
DI_TARGETMATCH_DENS_TYPE;

/*!
 * Judgement setting in ISO Check measurement
 */
typedef enum eDI_CERTIFICATION_JUDGEMENT
{
	CERTIFICATION_JUDGEMENT_OFF = 0,
	CERTIFICATION_JUDGEMENT_ON,
	DI_CERTIFICATION_JUDGEMENT_DEFAULT = CERTIFICATION_JUDGEMENT_OFF
}
DI_CERTIFICATION_JUDGEMENT;

/*!
 * Setting whether to enable color set
 */
typedef enum eDI_ENABLED_COLORSET
{
	DISABLE_COLORSET = 0,
	ENABLE_COLORSET,
	DI_ENABLED_COLORSET_DEFAULT = DISABLE_COLORSET
}
DI_ENABLED_COLORSET;

/*!
 * Setting whether to enable color data
 */
typedef enum eDI_ENABLED_COLORDATA
{
	DISABLE_COLORDATA = 0,
	ENABLE_COLORDATA,
	DI_ENABLED_COLORDATA_DEFAULT = DISABLE_COLORDATA
}
DI_ENABLED_COLORDATA;

/*!
 * Setting whether to enable data for measurement condition (Dot gain for plate measurent / Gray balance)
 */
typedef enum eDI_ENABLED_DATA
{
	DISABLE_DATA = 0,
	ENABLE_DATA,
	DI_ENABLED_DATA_DEFAULT = DISABLE_DATA
}
DI_ENABLED_DATA;

/*!
 * Setting whether to enable tone data
 */
typedef enum eDI_ENABLED_TONEDATA
{
	DISABLE_TONEDATA = 0,
	ENABLE_TONEDATA,
	DI_ENABLED_TONEDATA_DEFAULT = DISABLE_TONEDATA
}
DI_ENABLED_TONEDATA;

/*!
 * Setting whether to calculate Mid-Tone Spread
 */
typedef enum eDI_CAL_MIDTONE_SPREAD
{
	NOCAL_MIDTONE_SPREAD = 0,
	CAL_MIDTONE_SPREAD,
	DI_CAL_MIDTONE_SPREAD_DEFAULT = NOCAL_MIDTONE_SPREAD
}
DI_CAL_MIDTONE_SPREAD;

/*! 
 *  Measurement conditions structure which is memorized in the body of the device
 */
typedef struct tMEASSETTINGS
{
	DI_DENSITYBASE		DensityBase;			//!< the base density, e.g. density of the substate
	DI_DENSITYSTATUS	DensityStatus;			//!< Density Status, e.g. statut T etc.
	DI_DENFILTERS		DensityFilter;			//!< color filter for density
	real				CoefYN;					//!< the Yule-Nielsen coefficient 
	DISP_TYPES			DispType;				//!< displying type which shows absolute values  or difference values 
	DI_ILLUMINANTS		MeasIlluminant;			//!< Measurement Source for measurement
	DI_ILLUMINANTS		DensityIlluminant;		//!< reserved. 
	DI_ILLUMINANTS		ObsIlluminant;			//!< Observer Illuminant
	DI_OBSERVER			Obs;					//!< Observer Angle, 2 degree or 10 degree 
	DI_COLORSPACES		ColorSpace;				//!< Color space, e.g. CIE XYZ, CIE L*a*b* and so on
	DI_FORMULA			Formula;				//!< the formura of color difference
	COLOR_INDEX_TYPES   ColorIdx;				//!< the type of paper index
	PARAMETRIC			ParaCMC;				//!< the parametoric coefficients for CMC, h cannot be used.
	PARAMETRIC			ParaDE94;				//!< the parametoric coefficients for dE94
	PARAMETRIC			ParaDE00;				//!< the parametoric coefficients for dE2000


	uint8_di	DG_H_target;		//!< Value of the target dot gain 1(1-99)
	uint8_di	DG_M_target;		//!< Value of the target dot gain 2(1-99)
	uint8_di	DG_L_target;		//!< Value of the target dot gain 3(1-99)
	uint8_di	DG_M_Enabled;		//!< Status of the target dot gain 2(0x01: invalid 0x02: valid)
	uint8_di	DG_L_Enabled;		//!< Status of the target dot gain 3(0x01: invalid 0x02: valid)

	uint8_di	DenActTargetMode;	//!< Setting of target density(0x01: Auto 0x02: Manual)
	uint8_di	DenActTargetNo;		//!< The target number selected as target density(1-30)
	uint8_di	ClrActTargetMode;	//!< Setting of target color selection(0x01: Auto 0x02: Manual)
	uint8_di	ClrActTargetNo;		//!< The target number selected as target color(1-30)
}
MEASSETTINGS;


/*! 
 *  Measurement conditions which are added in FD-7 Ver.1.20
 */
typedef struct tDIMEASSETTINGS_VER120 {
	DI_TRAPPING_TYPE			TrappingType;			//!< Calculation method in trapping measurement
	uint8_di					MS_C_target;			//!< Cyan value for using in calculating Mid-Tone Spread
	uint8_di					MS_M_target;			//!< Magenta value for using in calculating Mid-Tone Spread
	uint8_di					MS_Y_target;			//!< Yellow value for using in calculating Mid-Tone Spread
	uint8_di					GB_H_target;			//!< Target Value 1 for using in calculating Gray balance
	uint8_di					GB_M_target;			//!< Target Value 2 for using in calculating Gray balance
	uint8_di					GB_L_target;			//!< Target Value 3 for using in calculating Gray balance
	DI_ENABLED_DATA				GB_M_Enabled;			//!< Flag whether to calculate Gray balance 2
	DI_ENABLED_DATA				GB_L_Enabled;			//!< Flag whether to calculate Gray balance 3
	uint8_di					DG_PS_H_target;			//!< Dot gain 1 value for using in Plate measurement
	uint8_di					DG_PS_M_target;			//!< Dot gain 2 value for using in PS measurement
	uint8_di					DG_PS_L_target;			//!< Dot gain 3 value for using in PS measurement
	DI_ENABLED_DATA				DG_PS_M_Enabled;		//!< Flag whether to calculate DotGain 2 in Plate measurement
	DI_ENABLED_DATA				DG_PS_L_Enabled;		//!< Flag whether to calculate DotGain 3 in Plate measurement
	real						CoefYN_PS;				//!< Yule-Nielsen coefficient in PS measurement
	DI_SPOTDENSITY_TYPE			SpotDensityType;		//!< Type of calculating the Spot color density
	uint16_di					SpotDensity_EvalWL;		//!< Wavelength for using in estimating the Spot color density
	DI_BACKING					BackingType;			//!< Backing type
	DI_BACKING_CONVERSION		BackingConversion;		//!< Backing conversion setting
	int8_di						ColorsetNo;				//!< Color set No. which is used in ISO check measurement
	DI_TARGETMATCH_DENS_TYPE	TargetMatchDensType;	//!< Ink type which estimate in Target Match measurement
}
DIMEASSETTINGS_VER120;

/*! 
 *  Measurement conditions which can set all measurement conditions
 */
typedef struct tMEASSETTINGS_ALL
{
	MEASSETTINGS MeasSettingsV100;						//!< The structure of measurement conditions which exists in FD-7 Ver.1.00
	DIMEASSETTINGS_VER120 MeasSettingsV120;				//!< The structure of measurement conditions which are added in FD-7 Ver.1.20
}
DI_MEASSETTINGS_ALL;

/*! 
 *  Color data conditions which is used with certification(ISO check) feature
 */
typedef struct tDICERTIFICATION_COLORDATA
{
	int8_di						ColorDataName[COLOR_DATA_NAME_SIZE+1];		//!< Color data name
	real						ColorValue[3];								//!< Colorimetric value(L*a*b*)
	DI_ENABLED_COLORDATA		EnabledColorData;							//!< Enabled flag of color data
	real						Tolerance[4];								//!< Tolerance(dE*, dL*, da*, db*)
	DI_CERTIFICATION_JUDGEMENT	EnabledJudgement;							//!< Judgement Setting
}
DI_CERTIFICATION_COLORDATA;

/*! 
 *  Tone data conditions which is used with certification(ISO check) feature
 */
typedef struct tDI_TONE_DATA
{
	real	ToneValue;		//!< Tone Value (1 to 99)
	real	TVI;			//!< Tone Value Increase (-99.9 to 99.9)
	real	Tolerance;		//!< Tolerance (0.1 to 99.9)
}
DI_TONE_DATA;

/*! 
 *  Mid-tone spread conditions which is used with certification(ISO check) feature
 */
typedef struct tDIMIDTONE_SPREAD
{
	DI_CAL_MIDTONE_SPREAD	EnableCalculation;		//!< Setting for calculating Mid-Tone Spread
	int32_di				CalculationToneValue;	//!< Tone value which is used in calculating Mid-Tone Spread
	int32_di				CalculationData[3];		//!< Tone data which is used in calculating Mid-Tone Spread
	real					Tolerance;				//!< Tolerance for Mid-Tone Spread
}
DI_MIDTONE_SPREAD;

/*! 
 *  Tone data Set which is used with certification(ISO check) feature
 */
typedef struct tDI_TONE_DATA_SET
{
	DI_ENABLED_TONEDATA		Enable_ToneDataSet;		//!< Enabled flag of tone data set
	int32_di				BasedColorData;			//!< Color data which is the base for Tone data
	int8_di					Enable_Tone1;			//!< Enabled flag of Tone data1
	DI_TONE_DATA			Tone1;					//!< Tone data1
	int8_di					Enable_Tone2;			//!< Enabled flag of Tone data2
	DI_TONE_DATA			Tone2;					//!< Tone data2
	int8_di					Enable_Tone3;			//!< Enabled flag of Tone data3
	DI_TONE_DATA			Tone3;					//!< Tone data3
}
DI_TONE_DATA_SET;

/*! 
 *  Color Set which is used with certification(ISO check) feature
 */
typedef struct tDICERTIFICATION_COLORSET
{
	int8_di						ColorSetName[COLOR_SET_NAME_SIZE+1];		//!< ColorSet Name
	DI_CERTIFICATION_COLORDATA	ColorData[COLOR_DATA_NUM];					//!< Color data
	DI_ILLUMINANTS				ObsIlluminant;								//!< Illuminance
	DI_OBSERVER					Obs;										//!< Observer
	DI_ILLUMINANTS				MeasIlluminant;								//!< Measurement conditions
	DI_FORMULA					ColorDiffFormula;							//!< Color difference formula
	PARAMETRIC					Parametric;									//!< Parametric coefficients
	DI_BACKING					BackingType;								//!< The setting of backing type
	DI_POLFILTER_STATUS			PolFilterStatus;							//!< The status of polarization filter
	int32_di					PaperNo;									//!< The index number which is registered paper data
	DI_DENSITYSTATUS			DensityStatus;								//!< Density status for tone value
	DI_ILLUMINANTS				DensityIlluminant;							//!< Measurement Condition for tone value
	real						CoefYN;										//!< Yule-Nielsen coefficient
	DI_TONE_DATA_SET			ToneDataSet[8];								//!< Tone data set
	DI_MIDTONE_SPREAD			MidToneSpread;								//<! Mid-Tone Spread data
}
DI_CERTIFICATION_COLORSET;

/*! 
 *  Backing data which is used in backing conversion
 */
typedef struct tBACKING_DATA
{
	DI_BACKING BackingColor;		//!< Backing condition (White or Black, not use Unknown)
	DI_PRERADFACTOR Data;			//!< Backing data
}
DI_BACKING_DATA;

typedef enum eCAPABILITY
{
	CAP_MEAS_SCAN				= 0x00001,		//!< Scan Measurement
	CAP_MEAS_SPOT				= 0x00002,		//!< Spot Measurement
	CAP_MEAS_ILL				= 0x00004,		//!< Illuminance Measurement
	CAP_GET_REPORT				= 0x00008,		//!< Data log Measurement
	CAP_GET_SPEC				= 0x00010,		//!< Getting spectral data
	CAP_GET_COLOR				= 0x00020,		//!< Getting color data
	CAP_GET_DENSITY				= 0x00040,		//!< Getting density
	CAP_GET_COLORINDEX			= 0x00080,		//!< Getting color index
	CAP_REG_USER_ILLUMINANT		= 0x00100,		//!< Registering user illuminant
	CAP_GET_DEV_INFO			= 0x00200,		//!< Getting device information
	CAP_DEVICE_CONDITION		= 0x00400,		//!< Device setting
	CAP_GET_PRE_RAD_FACTOR		= 0x00800,		//!< Getting pre rad factor
	CAP_GET_VIR_RAD_FACTOR		= 0x01000,		//!< Getting rad factor
	CAP_CAL_COLORDIFF			= 0x02000,		//!< Calculating color difference (not supported)
	CAP_CAL_DOT_AREA			= 0x04000,		//!< Calculating Dot Area (not supported)
	CAP_CAL_DOT_GAIN			= 0x08000,		//!< Calculating Dot Gain (not supported)
	CAP_CAL_METAMERI			= 0x10000,		//!< Calculating Metamerisum index (not supported)
	CAP_GET_SPOT_COLOR_DENSITY	= 0x20000,		//!< Gettting spot color density 
	CAP_CERTIFICATION_COLORSET	= 0x40000,		//!< Certification color setting
	CAP_BACKING_CONVERSION		= 0x80000,		//!< Backing conversion data setting
	CAP_MEMBER_NUM				= 20,			//!< Number of Enumeration (not supported)
}
DI_CAPABILITY;

#if defined(_WIN32) || defined(__APPLE__)
#pragma pack(pop) 
#endif


#endif



