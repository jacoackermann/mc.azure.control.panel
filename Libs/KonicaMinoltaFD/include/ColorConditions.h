/*!
 * type definition of color, density and so on
 * Copyright (C) KONICA MINOLTA, INC. 2011-2013 All rights reserved.
 * @brief color type definition
 * @file ColorConditions.h
 * @author KONICA MINOLTA, INC
 * @date Created on 2010/03/03
 */
#ifndef _COLOR_CONDITIONS_H_
#define _COLOR_CONDITIONS_H_


#ifndef SPC_LENGTH__
/*! macro function to get the length of array 
 * @a max is maximum wavelength, @a min is minimum wavelength�A@a inteval is interval of wavelength
 */
#define SPC_LENGTH(max,min,interval)	((max-min)/interval+1)


//! minimum wavelength for spectral radiance factor
#define RADFACTOR_BEGIN			380
//! maximum wavelength for spectral radiance factor
#define RADFACTOR_END			730
//! wavelength interval for spectral radiance factor
#define RADFACTOR_PITCH			10

//! minimum wavelength for spectral reflectrance factor 
#define REFFACTOR_BEGIN			RADFACTOR_BEGIN
//! maximum wavelength for spectral reflectrance factor 
#define REFFACTOR_END			RADFACTOR_END
//! wavelength interval for spectral reflectance factor
#define REFFACTOR_PITCH			RADFACTOR_PITCH

//! minimum wavelength for spectral radiance 
#define RADIANCE_BEGIN			360
//! maximum wavelength for spectral radiance 
#define RADIANCE_END			730
//! interval fro spectral radiance
#define RADIANCE_PITCH			5

//! minimum wavelength for illuminant 
#define ILLUMINANT_BEGIN		300
//! maximum wavelength for illuminant 
#define ILLUMINANT_END			780
//! interval of wavelength of illuminant
#define ILLUMINANT_PITCH		5

//! begin of wavelength of SPD of irradiance
#define IRRADIANCE_BEGIN		RADIANCE_BEGIN
//! end of wavelength of SPD of irradiance
#define IRRADIANCE_END			RADIANCE_END
//! interval of wavelength of SPD of irradiance
#define IRRADIANCE_PITCH		RADIANCE_PITCH

//! the length of arrays of spectral radiance factors
#define RADFACTOR_LEN			SPC_LENGTH(RADFACTOR_END,RADFACTOR_BEGIN,RADFACTOR_PITCH)
//! the length of arrays of spectral elemental factors
#define PRE_RADFACTOR_LEN		SPC_LENGTH(730,360,10)
//! the length of arrays of spectral reflectance factors
#define REFFACTOR_LEN			SPC_LENGTH(REFFACTOR_END,REFFACTOR_BEGIN,REFFACTOR_PITCH)
//! the length of arrays of spectral radiance
#define RADIANCE_LEN			SPC_LENGTH(RADIANCE_END,RADIANCE_BEGIN,RADIANCE_PITCH)
//! the length of arrays of spectral irradiance
#define IRRADIANCE_LEN			SPC_LENGTH(IRRADIANCE_END,IRRADIANCE_BEGIN,IRRADIANCE_PITCH)
//! the length of arrays of spectral irradiance for user illuminant
#define ILLUMINANT_LEN			SPC_LENGTH(ILLUMINANT_END,ILLUMINANT_BEGIN,ILLUMINANT_PITCH)


//! the maximum length of array of spetral data
#define MAX_SPCTL_LEN			RADIANCE_LEN
#endif

#define SIZE_SPEC_10	RADFACTOR_LEN	//!< the length of spectral data 380�`730nm, interval 1nm. 
#define SIZE_SPEC_01	(730-380+1)		//!< the length of spectral data 380�`730nm, interval 5nm. Must ignore.
#define SIZE_SPEC_05	RADIANCE_LEN	//!< the length of spectral data 380�`730nm, interval 10nm.

/*!
 * type of observer angle 
 */
typedef enum tOBSERVER
{
	OBS_02DEG = 0,		//!< 2 degree
	OBS_10DEG,			//!< 10 degree
	OBS_NO,
	
	OBS_FIRST = OBS_02DEG,
	OBS_LAST = OBS_10DEG,
	OBS_DEFAULT = OBS_02DEG,
}
DI_OBSERVER;


/*!
 * type of illuminant
 */
typedef enum tILLUMINANTS
{
	DI_ILLUMINANT_A = 0,	//! < CIE illuminant A
	DI_ILLUMINANT_C,		//! < CIE illuminant C
	DI_ILLUMINANT_D50,		//! < CIE illuminant D50
	DI_ILLUMINANT_D65,		//! < CIE illuminant D65
	DI_ILLUMINANT_ID50,		//! < CIE illuminant ID50
	DI_ILLUMINANT_ID65,		//! < CIE illuminant ID65
	DI_ILLUMINANT_F2,		//! < fluorescent lamp F2
	DI_ILLUMINANT_F6,		//! < fluorescent lamp F6
	DI_ILLUMINANT_F7,		//! < fluorescent lamp F7
	DI_ILLUMINANT_F8,		//! < fluorescent lamp F8
	DI_ILLUMINANT_F9,		//! < fluorescent lamp F9
	DI_ILLUMINANT_F10,		//! < fluorescent lamp F10
	DI_ILLUMINANT_F11,		//! < fluorescent lamp F11
	DI_ILLUMINANT_F12,		//! < fluorescent lamp F12
	DI_ILLUMINANT_USER1,

	DI_ILLUMINANT_USER_RESERVE = DI_ILLUMINANT_USER1+9,
	DI_ILLUMINANT_NO,

	DI_ILLUMINANT_FIRST		= DI_ILLUMINANT_A,
	DI_ILLUMINANT_LAST		= DI_ILLUMINANT_USER_RESERVE,
	DI_ILLUMINANT_DEFAULT	= DI_ILLUMINANT_D50,	

	DI_ILLUMINANT_UVCUT		= 0xA0,
	DI_ILLUMINANT_POL		= 0xA1,

	DI_ILLUMINANT_DEFAULT_MEA = DI_ILLUMINANT_D50,	//!< default illuminant for measurement source
	DI_ILLUMINANT_DEFAULT_OBS = DI_ILLUMINANT_D50,	//!< default illuminant for computing colorimetry

	DI_ILLUMINANT_LAST_MEA	= DI_ILLUMINANT_USER1,	
	DI_ILLUMINANT_LAST_OBS	= DI_ILLUMINANT_USER1,	

	MEAS_COND_M0			= DI_ILLUMINANT_A,
	MEAS_COND_M1			= DI_ILLUMINANT_D50,
	MEAS_COND_M2			= DI_ILLUMINANT_UVCUT,
	MEAS_COND_M3			= DI_ILLUMINANT_POL,

	DI_RADIANCE = 0xF0,
	DI_IRRADIANCE,
	
	DI_ILLUMINANT_NOT = 0xFF,
}
DI_ILLUMINANTS;


/*!
 * type of color space
 */
typedef enum eCOLORSPACES
{
	COLORSPACE_CIELAB = 0,			//!< CIE L*a*b*
	COLORSPACE_CIELCH,				//!< CIE L*C*h
	COLORSPACE_CIEXYZ,				//!< CIE XYZ			
	COLORSPACE_CIEXYY,				//!< CIE xyY
	COLORSPACE_HUNTERLAB,			//!< HUNTER LAB
	COLORSPACE_CIELUV,				//!< CIE L*u*v*  (not be supported)
	COLORSPACE_EVTDUV,				//!< Ev,T and delta uv (use only for measurement of illuminance)
	COLORSPACE_NO,

	COLORSPACE_FIRST = COLORSPACE_CIELAB,
	COLORSPACE_LAST = COLORSPACE_HUNTERLAB,
	COLORSPACE_DEFAULT = COLORSPACE_CIELAB,		
}
DI_COLORSPACES;


/*!
 * type of color difference formula
 */
typedef enum eFORMULA
{
	FORMULA_CIELAB_DE = 0,			//!< CIELAB 1976
	FORMULA_CIELAB_DE_CMC,			//!< Delta E CMC 
	FORMULA_CIELAB_DE_94,			//!< Delta E 1994
	FORMULA_CIELAB_DE_2000,			//!< Delta E 2000
	FORMULA_HUNTERLAB_DE,			//!< hunter Lab
	FORMULA_NO,

	FORMULA_FIRST = FORMULA_CIELAB_DE,
	FORMULA_LAST = FORMULA_HUNTERLAB_DE,
	FORMULA_DEFAULT = FORMULA_CIELAB_DE_2000,
}
DI_FORMULA;


/*!
 * Density status types
 */
typedef enum eDENSITYSTATUS
{
	DEN_STATUS_ISO_T = 0,				//!< ISO Status T
	DEN_STATUS_ISO_E,					//!< ISO Status E
	DEN_STATUS_ISO_A,					//!< ISO Status A
	DEN_STATUS_ISO_I,					//!< ISO Status I
	DEN_STATUS_DIN,						//!< DIN 16536
	DEN_STATUS_DINNB,					//!< DIN 16536 (Narrow band) (not supported)
	DEN_STATUS_NO,

	DEN_STATUS_FIRST = DEN_STATUS_ISO_T,
	DEN_STATUS_LAST = DEN_STATUS_DIN,
	DEN_STATUS_DEFAULT = DEN_STATUS_ISO_E,		
}
DI_DENSITYSTATUS;

/*!
 * type of base of density
 */
typedef enum eDENSITYBASE
{
	DEN_BASE_ABS	= 0x01,				//!< for absolute density  
	DEN_BASE_WHITE,						//!< for relative density to subsdtrate
}
DI_DENSITYBASE;


/*!
 * Desity filter types
 */
typedef enum eDENFILTERS
{
	DENFILTER_RED = 0,					//!< red filter
	DENFILTER_GREEN = 1,				//!< green filter
	DENFILTER_BLUE = 2,					//!< blue filter
	DENFILTER_VIS = 3,					//!< visual filter
	DENFILTER_NO,						//!< a number of filter types

	DENFILTER_ALL,						//!< all filters to get allcolor densities , r, g, b and visual
	DENFILTER_AUTO,						//!< auto filter to get density by major filter

	DENFILTER_FIRST = DENFILTER_RED,
	DENFILTER_LAST = DENFILTER_VIS,

	DENFILTER_CYAN = DENFILTER_RED,
	DENFILTER_MAGENTA = DENFILTER_GREEN,
	DENFILTER_YELLOW = DENFILTER_BLUE,
	DENFILTER_BLACK = DENFILTER_VIS,
}
DI_DENFILTERS;

/*!
 *  type of color index
 */ 
typedef enum eCOLORINDEXTYPES
{
	CLRIDX_BRIGHTNESS_INDEX = 0,		//!< brightness
	CLRIDX_WHITENESS_INDEX,				//!< whiteness
	CLRIDX_FLUOR_INTENSITY,				//!< Fluorescence Whitening Intensity
	CLRIDX_MI,							//!< metamerism index (not supported)
	CLRIDX_NO,

	CLRIDX_FIRST = CLRIDX_BRIGHTNESS_INDEX,	
	CLRIDX_LAST = CLRIDX_FLUOR_INTENSITY,
	CLRIDX_DEFAULT = CLRIDX_WHITENESS_INDEX,
}
COLOR_INDEX_TYPES;

/*!
 * real color type definition
 */
typedef float real_color;

/*!
 *  a structure of parametric paremeters for color difference computing 
 */
typedef struct tPARAMETRIC
{
	real_color l;
	real_color c;
	real_color h;
}
PARAMETRIC;


/*!
 * DENSITY structure
 */
typedef struct tDENSITY
{
	real_color r;			//!< density by red filter
	real_color g;			//!< density by green filter
	real_color b;			//!< density by blue filter
	real_color vis;			//!< density by visual filter
}
DI_DENSITY;


/*!
 *  whiteness structure
 */
typedef struct tWHITENESS
{
	DI_OBSERVER Obs;		//!< obserber angle
	DI_ILLUMINANTS Illuminant;	//!< illuminant types
	real_color Whiteness;		//!< whiteness 
	real_color Tint;		//!< tint
}
IDX_WHITENESS;

/*! 
 * Color index 
 */
typedef union uCOLORINDEX
{
	real_color	Brightness;	//!< brightness
	IDX_WHITENESS	Whiteness;	//!< whiteness structure
}
COLORINDEX;

typedef struct tPRERADFACTOR
{
	real_color prefactor_ex[PRE_RADFACTOR_LEN];
	real_color prefactor_in[PRE_RADFACTOR_LEN];
	real_color preilluminant_ex[PRE_RADFACTOR_LEN];
	real_color preilluminant_in[PRE_RADFACTOR_LEN];
}
DI_PRERADFACTOR;

typedef struct tRADFACTOR
{
	real_color Data[RADFACTOR_LEN];
}
DI_RADFACTOR;

#endif
