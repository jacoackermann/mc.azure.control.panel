using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Castle.Core.Logging;

using Colorware.Core.Config;
using Colorware.Core.ErrorReporting;
using Colorware.Core.Functional.Option;
using Colorware.Core.Globalization;

using JetBrains.Annotations;

using Microsoft.Win32;

namespace Colorware.Core.Export {
    [UsedImplicitly]
    public class ExportManager : IExportManager {
        private readonly IConfig config;
        private readonly ILogger log;
        private readonly IErrorReporter errorReporter;

        private const string DefaultExtKey = "Defaults.ExportManager.Extension";
        private const string CopyExportsKey = "Defaults.ExportManager.CopyOnExport";

        private const string ExportDirectoryName = "exports";

        public ExportManager([NotNull] IErrorReporter errorReporter,
                             [NotNull] ILogger logger,
                             [NotNull] IConfig config) {
            this.config = config ?? throw new ArgumentNullException(nameof(config));
            log = logger ?? throw new ArgumentNullException(nameof(logger));
            this.errorReporter = errorReporter ?? throw new ArgumentNullException(nameof(errorReporter));
        }

        public IImmutableList<IExporter> Exporters { get; private set; } = ImmutableList<IExporter>.Empty;

        public void AddExporterIfNotYetAdded(IExporter exporter) {
            if (exporter == null) throw new ArgumentNullException(nameof(exporter));

            if (!Exporters.Any(e =>
                                   e.Name == exporter.Name &&
                                   e.ExportableTypes.OrderBy(x => x)
                                    .SequenceEqual(exporter.ExportableTypes.OrderBy(x => x)))) {
                Exporters = Exporters.Add(exporter);
            }
        }

        /// <summary>
        /// Shows a file selector dialog with any registered exporters for the type that is passed in as a
        /// parameter. When the user selects a file and exporter the appropriate exporter returned in withExporter
        /// and the file path is returned.
        /// If the user cancelled the file selection, Option.Nothing is returned.
        /// </summary>
        /// <param name="item">The item to export.</param>
        /// <param name="withExporter">The returned exporter used by the user.</param>
        public Option<string> GetExporterAndFilePath(object item, out IExporter withExporter) {
            withExporter = null;

            var exporters = ExportersFor(item).ToList();
            if (!exporters.Any()) {
                errorReporter.ReportError(LanguageManager._("Misc.ExportManager.NoExporters") + item.GetType().Name,
                                          LanguageManager._("Misc.ExportManager.NoExportersCap"));
            }

            var saveDialog = new SaveFileDialog {
                OverwritePrompt = true,
                Filter = buildSaveDialogFilter(exporters),
                RestoreDirectory = true,
                FilterIndex = config.GetInt(DefaultExtKey, 0)
            };
            if (saveDialog.ShowDialog() != true)
                return Option<string>.Nothing;

            var index = saveDialog.FilterIndex - 1;
            if (index < 0 || index > exporters.Count)
                return Option<string>.Nothing; // Error?

            withExporter = exporters.ElementAt(index);
            config.Set(DefaultExtKey, saveDialog.FilterIndex.ToString());

            return saveDialog.FileName.ToOption();
        }

        /// <summary>
        /// Shows a file selector dialog for the exporter type that is passed in as a parameter.
        /// When the user selects a file, the file path is returned.
        /// If the user cancelled the file selection, Option.Nothing is returned.
        /// </summary>
        /// <param name="withExporter">The returned exporter used by the user.</param>
        public Option<string> GetExportFilePath(IExporter withExporter) {
            return GetExportFilePath(withExporter, null);
        }

        /// <summary>
        /// Shows a file selector dialog for the exporter type that is passed in as a parameter.
        /// When the user selects a file, the file path is returned.
        /// If the user cancelled the file selection, Option.Nothing is returned.
        /// </summary>
        /// <param name="withExporter">The returned exporter used by the user.</param>
        /// <param name="defaultName">defaultName is the initial name shown in the export dialog</param>
        public Option<string> GetExportFilePath(IExporter withExporter, string defaultName) {
            Debug.Assert(withExporter != null);
            var saveDialog = new SaveFileDialog {
                Filter = buildSaveDialogFilter(new[] {withExporter}),
                RestoreDirectory = true
            };

            if (!string.IsNullOrEmpty(defaultName))
                saveDialog.FileName = defaultName;

            if (saveDialog.ShowDialog() != true)
                return Option<string>.Nothing;

            return saveDialog.FileName.ToOption();
        }

        /// <summary>
        /// Exports the given item with the given exporter to the given path.
        /// </summary>
        /// <param name="item">The item to export.</param>
        /// <param name="exportFileParameters">Contains the client name, job number and the time of the data to export.</param>
        /// <param name="withExporter">The exporter to use.</param>
        /// <param name="toPath">The path to export to.</param>
        public async Task<bool> ExportAsync(object item, ExportFileParameters exportFileParameters,
                                            IExporter withExporter, string toPath) {
            var tempFileName = Path.GetTempFileName();
            var stream = new FileStream(tempFileName, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
            var finalPath = generateFinalPath(toPath, exportFileParameters);
            var args = new ExportParameters(stream, item, finalPath);
            try {
                await withExporter.ExportAsync(args);
            }
            catch (Exception e) {
                stream.Close();
                var msg = string.Format(LanguageManager._("Misc.ExportManager.TempFileFailed"), tempFileName, finalPath,
                                        e.Message);
                log.Error(msg, e);
                errorReporter.ReportError(msg, LanguageManager._("Misc.ExportManager.TempFileFailedCap"));
                return false;
            }
            stream.Close();

            try {
                // Copy final file:
                File.Copy(tempFileName, finalPath, true);
                copyBackup(tempFileName);
                File.Delete(tempFileName);
            }
            catch (Exception e) {
                var msg = string.Format(LanguageManager._("Misc.ExportManager.TempFileFailed"), tempFileName, finalPath,
                                        e.Message);
                log.Error(msg, e);
                errorReporter.ReportError(msg, LanguageManager._("Misc.ExportManager.TempFileFailedCap"));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Take the given export path. If timestamping is enabled this will be added to the path, otherwise
        /// the original path is returned.
        /// </summary>
        /// <param name="initialPath">The initial path to use as a template.</param>
        /// <returns>The final path, possibly timestamped if the option is enabled.</returns>
        private string generateFinalPath(string initialPath, ExportFileParameters exportFileParameters) {
            var finalPath = initialPath;
            if (GlobalConfigKeys.ExportManagerAddTimeStampToExports) {
                var directory = Path.GetDirectoryName(finalPath) ?? "";
                var filename = Path.GetFileName(finalPath);
                var newFilename = $"{exportFileParameters.Time:yyyy_MM_dd__HH_mm_ss}_{filename}";
                finalPath = Path.Combine(directory, newFilename);
            }

            if (GlobalConfigKeys.PlaceInClientJobFolder) {
                var directory = Path.GetDirectoryName(finalPath) ?? "";
                var filename = Path.GetFileName(finalPath);
                if (!string.IsNullOrEmpty(exportFileParameters.ClientName))
                    directory = Path.Combine(directory, exportFileParameters.ClientName);
                if (!string.IsNullOrEmpty(exportFileParameters.JobNumber))
                    directory = Path.Combine(directory, exportFileParameters.JobNumber);
                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);
                finalPath = Path.Combine(directory, filename);
            }
            return finalPath;
        }

        private void copyBackup(string filePath) {
            try {
                var doBackup = config.GetBool(CopyExportsKey, false);
                if (!doBackup)
                    return;
                var dataPath = GlobalConfig.GetDataPath();
                var exportDir = Path.Combine(dataPath, ExportDirectoryName);
                if (!Directory.Exists(exportDir))
                    Directory.CreateDirectory(exportDir);
                var backupFilename = generateBackupFilename();
                var backupPath = Path.Combine(exportDir, backupFilename);
                File.Copy(filePath, backupPath, true);
            }
            catch (Exception e) {
                log.Error("Could not save export backup", e);
            }
        }

        private string generateBackupFilename() {
            var now = DateTime.Now;
            return now.ToString("yyyy_MM_dd__HH_mm_ss.bak", CultureInfo.InvariantCulture);
        }

        private string buildSaveDialogFilter(IEnumerable<IExporter> exporters) {
            var items = exporters.Select(e => $"{e.Name}|*.{e.Extension}");
            return string.Join("|", items.ToArray());
        }

        #region Find appropriate exporters
        public IImmutableList<IExporter> ExportersFor(object item) {
            if (item == null) throw new ArgumentNullException(nameof(item));

            return ExportersFor(item.GetType());
        }

        public IImmutableList<IExporter> ExportersFor(Type type) {
            if (type == null) throw new ArgumentNullException(nameof(type));

            return
                Exporters.Where(e => e.IsActive && e.ExportableTypes.Any(t => t.IsAssignableFrom(type)))
                         .OrderBy(e => e.Name).ToImmutableList();
        }

        public IExporter ExporterFor(object item, string name) {
            return ExporterFor(item.GetType(), name);
        }

        public IExporter ExporterFor(Type type, string name) {
            if (type == null) throw new ArgumentNullException(nameof(type));
            if (name == null) throw new ArgumentNullException(nameof(name));

            return
                Exporters.FirstOrDefault(
                    e => e.IsActive && e.Name == name && e.ExportableTypes.Any(t => t.IsAssignableFrom(type)));
        }
        #endregion
    }
}