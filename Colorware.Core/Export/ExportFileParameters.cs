﻿using System;

using JetBrains.Annotations;

namespace Colorware.Core.Export {
    public class ExportFileParameters {
        [CanBeNull]
        public string ClientName;

        [CanBeNull]
        public string JobNumber;

        [NotNull]
        public DateTime Time;

        public ExportFileParameters(DateTime time) {
            ClientName = null;
            JobNumber = null;
            Time = time;
        }

        public ExportFileParameters(string clientName, string jobNumber, DateTime time) {
            ClientName = clientName;
            JobNumber = jobNumber;
            Time = time;
        }
    }
}