using System;
using System.IO;

using JetBrains.Annotations;

namespace Colorware.Core.Export {
    public class ExportParameters {
        [NotNull]
        public Stream OutStream { get; private set; }

        [NotNull]
        public object Item { get; private set; }

        [NotNull]
        public string FullPath { get; private set; }

        public ExportParameters([NotNull] Stream outStream, [NotNull] object item, [NotNull] string fullPath) {
            if (outStream == null) throw new ArgumentNullException(nameof(outStream));
            if (item == null) throw new ArgumentNullException(nameof(item));
            if (fullPath == null) throw new ArgumentNullException(nameof(fullPath));

            OutStream = outStream;
            Item = item;
            FullPath = fullPath;
        }
    }
}