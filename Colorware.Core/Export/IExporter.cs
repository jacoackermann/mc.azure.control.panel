using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.Export {
    public interface IExporter {
        /// <summary>
        /// Denotes wheter this exporter is active. Can be used for license checks as well as context checks.
        /// </summary>
        bool IsActive { get; }

        /// <summary>
        /// Denotes the type of object this exporter can export.
        /// </summary>
        [NotNull]
        IEnumerable<Type> ExportableTypes { get; }

        /// <summary>
        /// Extension used by any files in this format.
        /// </summary>
        [NotNull]
        string Extension { get; }

        /// <summary>
        ///  Name of this exporter.
        /// </summary>
        [NotNull]
        string Name { get; }

        /// <summary>
        /// Perform actual export. The data should be written to the supplied stream.
        /// </summary>
        /// <param name="parameters">Data related to the export.</param>
        Task ExportAsync([NotNull] ExportParameters parameters);
    }
}