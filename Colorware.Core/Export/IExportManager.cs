using System;
using System.Collections.Immutable;
using System.Threading.Tasks;

using Colorware.Core.Functional.Option;

using JetBrains.Annotations;

namespace Colorware.Core.Export {
    public interface IExportManager {
        IImmutableList<IExporter> Exporters { get; }

        void AddExporterIfNotYetAdded([NotNull] IExporter exporter);

        /// <summary>
        /// Shows a file selector dialog with any registered exporters for the type that is passed in as a
        /// parameter. When the user selects a file and exporter the appropriate exporter returned in withExporter
        /// and the file path is returned.
        /// If the user cancelled the file selection, Option.Nothing is returned.
        /// </summary>
        /// <param name="item">The item to export.</param>
        /// <param name="withExporter">The returned exporter used by the user.</param>
        Option<string> GetExporterAndFilePath(object item, out IExporter withExporter);

        /// <summary>
        /// Shows a file selector dialog for the exporter type that is passed in as a parameter.
        /// When the user selects a file, the file path is returned.
        /// If the user cancelled the file selection, Option.Nothing is returned.
        /// </summary>
        /// <param name="withExporter">The returned exporter used by the user.</param>
        Option<string> GetExportFilePath(IExporter withExporter);

        /// <summary>
        /// Shows a file selector dialog for the exporter type that is passed in as a parameter.
        /// When the user selects a file, the file path is returned.
        /// If the user cancelled the file selection, Option.Nothing is returned.
        /// </summary>
        /// <param name="withExporter">The returned exporter used by the user.</param>
        /// <param name="defaultName">defaultName is the initial name shown in the export dialog</param>
        Option<string> GetExportFilePath(IExporter withExporter, string defaultName);

        /// <summary>
        /// Exports the given item with the given exporter to the given path.
        /// </summary>
        /// <param name="item">The item to export.</param>
        /// <param name="exportFileParameters">Contains the client name, job number and the time of the data to export.</param>
        /// <param name="withExporter">The exporter to use.</param>
        /// <param name="toPath">The path to export to.</param>
        Task<bool> ExportAsync([NotNull] object item, ExportFileParameters exportFileParameters,
                               [NotNull] IExporter withExporter, [NotNull] string toPath);

        [NotNull]
        IImmutableList<IExporter> ExportersFor([NotNull] object item);


        [NotNull]
        IImmutableList<IExporter> ExportersFor([NotNull] Type type);
    }
}