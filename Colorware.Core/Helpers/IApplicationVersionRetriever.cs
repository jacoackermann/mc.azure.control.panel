﻿namespace Colorware.Core.Helpers {
    /// <inheritdoc cref="GetVersion"/>
    public interface IApplicationVersionRetriever {
        /// <summary>
        /// Retrieve the full version of the application in string format.
        /// </summary>
        /// <returns>The version of the application in the format <c>x.y.z</c></returns>
        string GetVersion();
    }
}