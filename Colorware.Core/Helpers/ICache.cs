﻿using System;
using System.Threading.Tasks;

namespace Colorware.Core.Helpers {
    public interface ICache<in TKey, TValue> {
        /// <summary>
        /// Gets a value from the cache that was stored using the specified key.
        /// 
        /// If no value was stored for the given key, 'value' is set to the type's default value.
        /// </summary>
        /// <returns>False iff no value was previously added for the given key.</returns>
        bool Get(TKey key, out TValue value);


        /// <summary>
        /// Stores a value to the cache for a given key.
        /// </summary>
        void Set(TKey key, TValue value);

        /// <summary>
        /// Clear the cache.
        /// </summary>
        void Clear();
    }

    public interface IThreadSafeCache<in TKey, TValue> {
        /// <summary>
        /// Provides a threadsafe interface for a cache. When the method is called
        /// this locks the access to the cache until the handler has completed.
        /// </summary>
        /// <typeparam name="TResult">The return valu eof the handler.</typeparam>
        /// <param name="handler">A function to handle the cache.</param>
        /// <returns>The result of the handler.</returns>
        TResult With<TResult>(Func<ICache<TKey, TValue>, TResult> handler);

        /// <summary>
        /// Async version of <see cref="With{TResult}"/>.
        /// </summary>
        Task<TResult> WithAsync<TResult>(Func<ICache<TKey, TValue>, Task<TResult>> handler);
    }

    public static class CacheExtensions {
        /// <summary>
        /// Get the value from the cache, if the value is not in the cache, the generator function is called.
        /// The generated value is then cached and returned.
        /// </summary>
        /// <typeparam name="TKey">The key type.</typeparam>
        /// <typeparam name="TValue">The value type.</typeparam>
        /// <param name="cache">The cache to use.</param>
        /// <param name="key">The key to retrieve the data for.</param>
        /// <param name="generator">A generator function which will be called if the data was not in the cache.</param>
        /// <returns>The item from the cache if it's present, the value from the generator function otherwise.</returns>
        public static TValue Get<TKey, TValue>(this ICache<TKey, TValue> cache, TKey key, Func<TKey, TValue> generator) {
            TValue item;
            if (cache.Get(key, out item)) {
                return item;
            }
            item = generator(key);
            cache.Set(key, item);
            return item;
        }

        /// <summary>
        /// Like <see cref="Get{TKey,TValue}(Colorware.Core.Helpers.ICache{TKey,TValue},TKey,System.Func{TKey,TValue})"/>,
        /// but using an async variant.
        /// </summary>
        public static async Task<TValue> GetAsync<TKey, TValue>(this ICache<TKey, TValue> cache, TKey key,
                                                     Func<TKey, Task<TValue>> generator) {
            TValue item;
            if (cache.Get(key, out item)) {
                return item;
            }
            item = await generator(key);
            cache.Set(key, item);
            return item;
        }

        public static TValue Get<TKey, TValue>(this IThreadSafeCache<TKey, TValue> threadSafeCache, TKey key, Func<TKey, TValue> generator) {
            return threadSafeCache.With(cache => cache.Get(key, generator));
        }

        public static Task<TValue> GetAsync<TKey, TValue>(this IThreadSafeCache<TKey, TValue> threadSafeCache,
                                                                TKey key, Func<TKey, Task<TValue>> generator) {
            return threadSafeCache.WithAsync(cache => cache.GetAsync(key, generator));
        }

        /*
        public static TValue Set<TKey, TValue>(this IThreadSafeCache<TKey, TValue> threadSafeCache, TKey key, TValue value) {
            return threadSafeCache.With(cache => {
                cache.Set(key, value);
                return value;
            });
        }
        */
    }
}