﻿using System;
using System.Diagnostics;

namespace Colorware.Core.Helpers {
    /// <summary>
    /// Class to easily measure performance between two points within code.
    /// Implemented as static for ease of use.
    /// </summary>
    public static class PerformanceTimer {
        private class TimerWrapper : IDisposable {
            /// <summary>
            /// Name of this timer.
            /// </summary>
            public readonly String Name;

            /// <summary>
            /// stopwatch for keeping track of elapsed time.
            /// </summary>
            private readonly Stopwatch stopwatch;

            public TimerWrapper(string name) {
                Name = name;
                stopwatch = new Stopwatch();
            }

            public void Start() {
                stopwatch.Start();
            }

            public TimeSpan Stop() {
                stopwatch.Stop();
                return stopwatch.Elapsed;
            }

            private void stopAndWriteAfterDispose() {
                var lapse = Stop();
                var oldColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("**** Exiting block (" + Name + "): " + lapse);
                Console.ForegroundColor = oldColor;
            }

            public void Dispose() {
                stopAndWriteAfterDispose();
            }
        }

        public static IDisposable Start(String blockName, bool suppressWrite = false) {
            var tw = new TimerWrapper(blockName);
            if (!suppressWrite) {
                Console.WriteLine("**** Entering block (" + blockName + ")");
            }
            tw.Start();
            return tw;
        }

        public static void Stop(object timer) {
            var tw = (TimerWrapper)timer;
            var lapse = tw.Stop();
            Console.WriteLine("**** Exiting block (" + tw.Name + "): " + lapse);
        }
        public static void Stop(object timer, TimeSpan onlyShowIfLongerThanThis) {
            var tw = (TimerWrapper)timer;
            var lapse = tw.Stop();
            if (lapse >= onlyShowIfLongerThanThis) {
                var oldColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("**** Exiting block (" + tw.Name + "): " + lapse);
                Console.ForegroundColor = oldColor;
            }
        }

        public static void Pause(object timer) {
            var tw = (TimerWrapper)timer;
            tw.Stop();
        }

        public static void UnPause(object timer) {
            var tw = (TimerWrapper)timer;
            tw.Start();
        }
    }
}