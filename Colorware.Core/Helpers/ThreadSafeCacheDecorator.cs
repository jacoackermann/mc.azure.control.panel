﻿using System;
using System.Threading;
using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.Helpers {
    public class ThreadSafeCacheDecorator<TKey, TValue> : IThreadSafeCache<TKey, TValue> {
        private readonly ICache<TKey, TValue> decoratedCache;
        private readonly SemaphoreSlim semaphore = new SemaphoreSlim(1, 1);

        public ThreadSafeCacheDecorator([NotNull] ICache<TKey, TValue> decoratedCache) {
            if (decoratedCache == null) throw new ArgumentNullException(nameof(decoratedCache));
            this.decoratedCache = decoratedCache;
        }

        public TResult With<TResult>(Func<ICache<TKey, TValue>, TResult> handler) {
            try {
                semaphore.Wait();
                return handler(decoratedCache);
            }
            finally {
                semaphore.Release();
            }


        }

        public async Task<TResult> WithAsync<TResult>(Func<ICache<TKey, TValue>, Task<TResult>> handler) {
            try {
                await semaphore.WaitAsync();
                return await handler(decoratedCache);
            }
            finally {
                semaphore.Release();
            }
        }
    }
}