﻿using System;

using JetBrains.Annotations;

namespace Colorware.Core.Helpers {
    public interface IBrowserOpener {
        void OpenUrl(string url);
    }

    [UsedImplicitly]
    public static class BrowserOpenerExtensions {
        public static void OpenUrl(this IBrowserOpener opener, Uri url) {
            opener.OpenUrl(url.AbsoluteUri);
        }
    }
}