using System;
using System.Collections.Generic;
using System.Linq;

namespace Colorware.Core.Helpers {
    public static class TimeHelper {
        private static readonly DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);

        public static DateTime FromUnixTimestamp(long timestamp) {
            return FromUnixTimestamp((double)timestamp);
        }

        public static DateTime FromUnixTimestamp(double timestamp) {
            return origin.AddSeconds(timestamp);
        }

        /// <summary>
        /// Calculates the maximum timespan from a series of <see cref="DateTime"/> entries and returns this as
        /// a formated string using some simple rules. See <see cref="FormatTimeSpan(TimeSpan)"/> for more information.
        /// </summary>
        /// <param name="timeEntries">The items to calculate the timespan for.</param>
        /// <returns>The formatted timespan representing the maximum time between all entries.</returns>
        public static string FormatTimeSpan(IEnumerable<DateTime> timeEntries) {
            if (timeEntries.Count() <= 1)
                return "0";
            var earliestTime = timeEntries.Min();
            var latestTime = timeEntries.Max();
            return FormatTimeSpan(earliestTime, latestTime);
        }

        /// <summary>
        /// Calculates a timespan between two dates and formats the resulting <see cref="TimeSpan"/> according to some
        /// simple rules. See <see cref="FormatTimeSpan(TimeSpan)"/> for more information.
        /// </summary>
        /// <param name="time1">The first time entry.</param>
        /// <param name="time2">The second time entry.</param>
        /// <returns>A string representing the time span.</returns>
        public static string FormatTimeSpan(DateTime time1, DateTime time2) {
            var earliestTime = time1 > time2 ? time2 : time1;
            var latestTime = time1 > time2 ? time1 : time2;
            var span = latestTime - earliestTime;
            return FormatTimeSpan(span);
        }

        /// <summary>
        /// Formats a timespan according to some simple rules:
        /// If the span is less than a minute, format like "[seconds]s".
        /// If the span is more than two days, format like "[days]d [hours]h".
        /// Otherwise format like "[hours]h [minutes]m".
        /// </summary>
        /// <param name="span"></param>
        /// <returns></returns>
        public static string FormatTimeSpan(TimeSpan span) {
            if (span.Minutes <= 0) {
                return string.Format("{0}s", span.Seconds);
            }
            if (span.Days > 2)
                return string.Format("{0}d {1}h", span.ToString("%d"), span.ToString("%h"));
            return string.Format("{0}h {1}m", span.ToString("%h"), span.ToString("%m"));
        }
    }
}