﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using Castle.Core.Internal;
using Castle.MicroKernel.Registration;

using JetBrains.Annotations;

namespace Colorware.Core.Helpers {
    public static class ReflectionHelpers {
        [NotNull]
        public static T CreateInstance<T>([NotNull] string typeName) {
            var type = Type.GetType(typeName);
            if (type == null)
                throw new InvalidOperationException($"Could not get type of class '{typeName}'");

            return CreateInstance<T>(type);
        }

        /// <summary>
        /// See <see cref="CreateInstanceUsingContainer{T}(Type)"/> for info.
        /// </summary>
        /// <typeparam name="T">Base type of the instance.</typeparam>
        /// <param name="typeName">Concrete type of the instance.</param>
        /// <returns>A new instance.</returns>
        [NotNull]
        public static T CreateInstanceUsingContainer<T>([NotNull] string typeName) {
            var type = Type.GetType(typeName);
            if (type == null)
                throw new InvalidOperationException($"Could not get type of class '{typeName}'");

            return CreateInstanceUsingContainer<T>(type);
        }

        /// <summary>
        /// Create a new intance for the passed in <see cref="type"/> using the current
        /// <see cref="GlobalContainer.Current"/> instance to resolve dependencies. This currently
        /// works by checking if the requested type is already registered. If it is not, it will be
        /// added to the container.
        /// </summary>
        /// <typeparam name="T">Base type of the instance.</typeparam>
        /// <param name="type">Concrete type of the instance.</param>
        /// <returns>A new instance.</returns>
        [NotNull]
        public static T CreateInstanceUsingContainer<T>([NotNull] Type type) {
            var container = GlobalContainer.Current;
            if (!container.Kernel.HasComponent(type)) {
                container.Register(
                    Component.For(type)
                             .LifestyleTransient()
                             .IsFallback()
                             .Named(type.FullName));
            }

            return (T)container.Resolve(type);
        }

        [NotNull]
        public static T CreateInstance<T>([NotNull] Type type) {
            return (T)Activator.CreateInstance(type, true);
        }

        [NotNull]
        public static T CreateInstance<T>() {
            return Activator.CreateInstance<T>();
        }

        /// <summary>
        /// Creates a delegate that invokes the setter for 'property' on a given instance with a given parameter.
        /// 
        /// Using the created delegate is faster than invoking the set method via reflection.
        /// </summary>
        /// <param name="property">The property to create the setter delegate for.</param>
        /// <returns>The setter delegate, takes a class instance and set value parameter.</returns>
        public static Action<object, object> CreateSetterDelegate([NotNull] this PropertyInfo property) {
            var method = property.SetMethod;
            var paramType = method.GetParameters().Single().ParameterType;

            /* Declare the delegate parameters */
            // The instance to call the setter on
            var instanceExpr = Expression.Parameter(typeof(object));
            // The parameter to pass to the setter
            var paramExpr = Expression.Parameter(typeof(object));

            // Create the delegate
            // NB: the conversions are needed to go from 'object' to the parameter types
            var setterCall = Expression.Call(
                Expression.Convert(instanceExpr, method.DeclaringType),
                method,
                Expression.Convert(paramExpr, paramType)
                );

            return Expression.Lambda<Action<object, object>>(setterCall, instanceExpr, paramExpr)
                             .Compile();
        }

        // From v3.3.0 of Castle.Core.Internal.AttributesUtil (was dropped in later versions)
        public static bool HasAttribute<T>(this MemberInfo member) where T : Attribute {
            return member.GetAttributes<T>().FirstOrDefault() != null;
        }
        
        /// <summary>
        /// Dynamic equivalent of default(type).
        /// </summary>
        public static object GetDefault(Type type) {
            return type.IsValueType ? Activator.CreateInstance(type) : null;
        }

        [ItemNotNull, NotNull]
        public static IEnumerable<Type> GetDerivedTypesFromAssembly([NotNull] Assembly assembly, [NotNull] Type baseType) {
            if (assembly == null) throw new ArgumentNullException(nameof(assembly));
            if (baseType == null) throw new ArgumentNullException(nameof(baseType));

            return assembly.GetTypes().Where(t => t != baseType && baseType.IsAssignableFrom(t));
        }
    }
}