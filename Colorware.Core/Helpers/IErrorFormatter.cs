﻿using System.Collections.Generic;

using Colorware.Core.Functional.Result;

namespace Colorware.Core.Helpers {
    /// <summary>
    /// Formats a list of errors into a single string. This can be used for display or storage purposes.
    /// </summary>
    public interface IErrorFormatter {
        string Format(IEnumerable<IError> errors);
    }
}