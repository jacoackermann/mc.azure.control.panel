﻿using System.Windows;

using Colorware.Core.Data.Models;

namespace Colorware.Core.Helpers {
    public static class InterpolationHelper {
        public static double InterpolatePointsYForXLinear(Point p1, Point p2, double x) {
            return Lerp(p1.Y, p2.Y, (x - p1.X) / (p2.X - p1.X));
        }

        public static double Lerp(double a, double b, double fraction) {
            return (a * (1.0 - fraction)) + (b * fraction);
        }

        public static double InterpolateDotgainLinear(IDefaultDotgainEntry lowerEntry, IDefaultDotgainEntry higherEntry,
            int percentage) {
            return InterpolatePointsYForXLinear(new Point(lowerEntry.Percentage, lowerEntry.Dotgain),
                new Point(higherEntry.Percentage, higherEntry.Dotgain), percentage);
        }
    }
}