﻿using System;

namespace Colorware.Core.Helpers {
    public static class ParseDoubleHelper {
        /// <summary>
        /// Helper method for use in properties (originating from requirements arissen in the CDS module).
        /// This can be used to parse a user entered double value, which was entered as a string given
        /// optional decimalpoint restricitions as well as optional min and max values. This can be
        /// used when UpdateSourceTrigger has been set to Property to allow for instantanious updates
        /// to the property while permitting validation while not interfering with the text the user is inputting.
        ///
        /// The output string will be updated to use the number of specified decimal points if decimalPoints
        /// has been given.
        /// </summary>
        /// <param name="str">The input value.</param>
        /// <param name="decimalPoints">The number of decimal points to keep.</param>
        /// <param name="minValue">The minimal resulting value.</param>
        /// <param name="maxValue">The maximum restuling value.</param>
        /// <returns>The value as a double or null if it could not be parsed.</returns>
        public static double? Parse(ref string str, int? decimalPoints = null, double? minValue = null, double? maxValue = null) {
            if (decimalPoints.HasValue) {
                // Check that the string has the form /x+(.x+)?/ and split on the dot.
                var parts = str.Split(new[] {'.'}, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length >= 2) {
                    // If the string has at least a single dot...
                    if (parts[1].Length > decimalPoints.Value) {
                        // ... and the second part has more than <c>decimalPoint</c> characters.
                        // We make sure that the string is 'rounded' to <c>decimalPoint</c> decimals by
                        // clipping the second part to a maximum length of <c>decimalPoint</c>.
                        str = parts[0] + "." + parts[1].Substring(0, decimalPoints.Value);
                    }
                }
            }
            // Check for a valid number by trying to parse the value:
            double asDouble;
            if (double.TryParse(str, out asDouble)) {
                // Check that the resulting value is actually within the required range:
                if (minValue.HasValue && asDouble < minValue.Value) {
                    asDouble = minValue.Value;
                    if (decimalPoints.HasValue)
                        str = asDouble.ToString("F" + decimalPoints.Value);
                }
                if (maxValue.HasValue && asDouble > maxValue.Value) {
                    asDouble = maxValue.Value;
                    if (decimalPoints.HasValue)
                        str = asDouble.ToString("F" + decimalPoints.Value);
                }
                // Return the actual value.
                return asDouble;
            }
            else {
                // Invalid value, return null.
                return null;
            }
        }
    }
}