﻿using System;

namespace Colorware.Core.Helpers {
    public static class MemoryHelper {
        /// <summary>
        /// Asks the garbage collector to reclaim as much memory as possible.
        /// 
        /// This call blocks the entire process while busy.
        /// </summary>
        public static void Cleanup() {
            // Yes, the double Collect() is necessary
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}