using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Management;
using System.Net.NetworkInformation;

namespace Colorware.Core.Helpers {
    public static class SystemInfoHelper {
        public static string GetOperatingSystem() {
            try {
                var operatingSystems = getWmiPropertyInfo("Win32_OperatingSystem");

                var os = operatingSystems.FirstOrDefault();
                if (os != null)
                    return os["Caption"].ToString();
            }
            catch (Exception) {
            }

            return "?";
        }

        public static IEnumerable<string> GetProcessors() {
            try {
                var cpus = getWmiPropertyInfo("Win32_Processor");

                return
                    cpus.Select(
                        cpu =>
                            string.Format("{0} - {1} - {2} ({3} MHz)", cpu["Manufacturer"], cpu["Name"], cpu["Version"],
                                cpu["CurrentClockSpeed"]));
            }
            catch (Exception) {
            }

            return new List<string>();
        }

        public static IEnumerable<string> GetRamModules() {
            try {
                var ramModules = getWmiPropertyInfo("Win32_PhysicalMemory");

                return
                    ramModules.Select(
                        ram =>
                            string.Format(CultureInfo.InvariantCulture, "{0} - {1} - {2} - {3:F2} MB",
                                ram["Manufacturer"], ram["Model"], ram["Version"], (UInt64)ram["Capacity"] / 1e6));
            }
            catch (Exception) {
            }

            return new List<string>();
        }

        public static IEnumerable<string> GetLogicalDisks() {
            // Don't need to know this from ourselves, it's just used to
            // guess what kind of machine is running the server/client at customers
#if !DEBUG
            try {
                var disks = getWmiPropertyInfo("Win32_LogicalDisk");

                return
                    disks.Select(
                        disk =>
                            string.Format(CultureInfo.InvariantCulture, "{0} ({1}) - {2} ({3}) - Free: {4:F2}/{5:F2} GB",
                                disk["DeviceID"], disk["VolumeName"], disk["Description"], disk["FileSystem"],
                                disk["FreeSpace"] == null ? 0 : (UInt64)disk["FreeSpace"] / 1e9,
                                disk["Size"] == null ? 0 : (UInt64)disk["Size"] / 1e9));
            }
            catch (Exception) {
            }
#endif

            return new List<string>();
        }

        public static string GetMemoryInfo() {
            try {
                var operatingSystems = getWmiPropertyInfo("Win32_OperatingSystem");

                var os = operatingSystems.FirstOrDefault();
                if (os != null)
                    return string.Format(CultureInfo.InvariantCulture,
                        "Free Physical Memory: {0:F2}/{1:F2} GB\nFree Virtual Memory: {2:F2}/{3:F2} GB\nFree Space in Paging Files: {4:F2}/{5:F2} GB\n" +
                        "Max Process Memory: {6:F2} GB", KB2GB(os["FreePhysicalMemory"]),
                        KB2GB(os["TotalVisibleMemorySize"]),
                        KB2GB(os["FreeVirtualMemory"]), KB2GB(os["TotalVirtualMemorySize"]),
                        KB2GB(os["FreeSpaceInPagingFiles"]), KB2GB(os["SizeStoredInPagingFiles"]),
                        KB2GB(os["MaxProcessMemorySize"]));
            }
            catch (Exception) {
            }

            return "?";
        }

        public static IEnumerable<string> GetGpus() {
            try {
                var videoControllers = getWmiPropertyInfo("Win32_VideoController");

                return
                    videoControllers.Select(
                        gpu => string.Format("{0} - {1}", gpu["Description"], gpu["VideoModeDescription"]));
            }
            catch {
            }

            return new List<string>();
        }

        public static IEnumerable<string> GetNetworkInterfaces() {
            try {
                NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();

                return
                    interfaces.Select(
                        i =>
                            string.Format("{0} ({1}) - {2} - {3} @ {4:F2} Mbit/s", i.Name, i.NetworkInterfaceType,
                                i.Description, i.OperationalStatus, i.Speed / 1e6));
            }
            catch (Exception) {
                return new List<string>();
            }
        }

        public static string GetCultureSettings() {
            return
                string.Format("Current Culture: {0}\nCurrent UI Culture: {1}\nInstalled UI Culture: {2}",
                    getCultureInfo(CultureInfo.CurrentCulture), getCultureInfo(CultureInfo.CurrentUICulture),
                    getCultureInfo(CultureInfo.CurrentUICulture));
        }

        public static string GetTimezoneInfo() {
            var currentZone = TimeZone.CurrentTimeZone;
            return string.Format("Timezone: {0} (UTC offset: {1}), Daylight saving time: {2}", currentZone.StandardName,
                currentZone.GetUtcOffset(DateTime.Now), currentZone.IsDaylightSavingTime(DateTime.Now));
        }

        private static string getCultureInfo(CultureInfo ci) {
            return string.Format("{0} ({1}), Decimal seperator: '{2}', Group seperator: '{3}'", ci.Name, ci.EnglishName,
                ci.NumberFormat.NumberDecimalSeparator, ci.NumberFormat.NumberGroupSeparator);
        }

        private static double KB2GB(object bytes) {
            return KB2GB((UInt64)bytes);
        }

        private static double KB2GB(UInt64 bytes) {
            return bytes / 1e6;
        }

        private static IEnumerable<ManagementObject> getWmiPropertyInfo(string property) {
            using (var searcher = new ManagementObjectSearcher("SELECT * FROM " + property))
                return searcher.Get().Cast<ManagementObject>();
        }
    }
}