﻿using System;
using System.Collections.Generic;
using System.Text;

using Colorware.Core.Functional.Result;

namespace Colorware.Core.Helpers.ErrorFormatting {
    public class NewLineErrorFormatter : IErrorFormatter {
        public string Format(IEnumerable<IError> errors) {
            var sb = new StringBuilder();
            foreach (var error in errors) {
                formatError(error, sb);
            }
            return sb.ToString();
        }

        private void formatError(IError error, StringBuilder sb) {
            var asException = error.GetErrorRaw() as Exception;
            if (asException != null) {
                formatException(asException, sb);
            }
            else {
                sb.Append(error.GetError());
                sb.AppendLine();
                sb.AppendLine();
            }
        }

        private void formatException(Exception ex, StringBuilder sb) {
            var asAggregateException = ex as AggregateException;
            if (asAggregateException != null) {
                sb.Append(ex.Message);
                sb.AppendLine();
                sb.AppendLine();
                foreach (var aEx in asAggregateException.InnerExceptions) {
                    formatException(aEx, sb);
                }
            }
            else {
                sb.Append(ex.Message);
                sb.AppendLine();
                if (ex.InnerException != null) {
                    formatException(ex.InnerException, sb);
                    sb.AppendLine();
                    sb.AppendLine();
                }
            }
        }
    }
}