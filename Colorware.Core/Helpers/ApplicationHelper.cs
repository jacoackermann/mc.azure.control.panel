using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Hosting;

using Colorware.Cryptography.Decryption;
using Colorware.Cryptography.KeyGenerators;

using JetBrains.Annotations;

namespace Colorware.Core.Helpers {
    public static class ApplicationHelper {
        [CanBeNull]
        private static FileVersionInfo getFileVersionInfoForVersion() {
            const string versionSpecifyingDll = "Colorware.Core.dll";

            try {
                FileVersionInfo info;
                if (File.Exists(versionSpecifyingDll))
                    // Client
                    info = FileVersionInfo.GetVersionInfo(versionSpecifyingDll);
                else {
                    // Server
                    var path = HostingEnvironment.MapPath("~/bin/" + versionSpecifyingDll);
                    info = FileVersionInfo.GetVersionInfo(path);
                }
                return info;
            }
            catch (Exception) {
                return null;
            }
        }

        [CanBeNull]
        private static string versionVerbose;

        [NotNull]
        public static string GetVersionVerbose() {
            if (versionVerbose != null)
                return versionVerbose;

            var info = getFileVersionInfoForVersion();
            if (info == null)
                return "?";

            versionVerbose = $"{info.ProductVersion} (Build: {info.FileVersion})";

            return versionVerbose;
        }

        [CanBeNull]
        private static string version;

        [NotNull]
        public static string GetVersion() {
            // Caching to prevent disk I/O for every request
            if (version != null)
                return version;

            var info = getFileVersionInfoForVersion();
            if (info?.FileVersion == null)
                return "?";
            version = info.FileVersion;

            return version;
        }

        /// <summary>
        /// Returns first two digits of the version number.
        /// </summary>
        /// <returns>First two digits if valid version was found.</returns>
        public static string GetMajorVersion() {
            var fullVersion = GetVersion();
            var parts = fullVersion.Split('.');
            if (parts.Length < 2)
                return "?";
            return parts[0] + "." + parts[1];
        }

        /// <summary>
        /// Returns only the build number (last digit) from the full version number.
        /// </summary>
        /// <returns>The build number.</returns>
        public static string GetBuildVersion() {
            var fullVersion = GetVersion();
            var parts = fullVersion.Split('.');
            if (parts.Length < 1)
                return "?";
            for (var i = parts.Length - 1; i >= 0; i--) {
                if (!string.IsNullOrEmpty(parts[i]) && parts[i] != "0")
                    return parts[i];
            }
            return parts[0];
        }

        /// <summary>
        /// Retrieves the encrypted build date.
        /// </summary>
        /// <returns>The build date.</returns>
        public static DateTime? GetBuildDate() {
            var assembly = Assembly.GetExecutingAssembly();
            var attributes = (AssemblyMetadataAttribute[])assembly.GetCustomAttributes(typeof(AssemblyMetadataAttribute), true);
            var buildDateAttribute = attributes.FirstOrDefault(a => a.Key == "BuildDate");
            if (buildDateAttribute == null)
                return null;
            return decryptDate(buildDateAttribute.Value);
        }

        private static DateTime decryptDate(string encryptedDate) {
            var decryptor = new RijndaelSymmetricDecryptor();
            var generator = new ApplicationStaticSymmetricKeyGenerator();
            var key = generator.Generate();
            var bytes = Convert.FromBase64String(encryptedDate);
            var dateStr = decryptor.Decrypt(key, bytes);
            return DateTime.ParseExact(dateStr, "O", CultureInfo.InvariantCulture);
        }

        public static string ApplicationName => "MeasureColor";

        public static string GetMemoryUsageInfo() {
            var proc = Process.GetCurrentProcess();
            long privateMemSize = 0;
            long workingSet = 0;
            try {
                privateMemSize = proc.PrivateMemorySize64;
                workingSet = proc.WorkingSet64;
            }
            catch (Exception) {
            }

            try {
                // Note that the working set can be bigger than the private memory size because
                // the working set shares some space with other processes (loaded DLLs etc.)
                return string.Format(CultureInfo.InvariantCulture,
                                     "GC Total Memory: {0:F2} Mb\nPrivate Memory Usage: {1:F2} Mb\nRAM Usage: {2:F2} Mb",
                                     GC.GetTotalMemory(false) / 1e6, privateMemSize / 1e6, workingSet / 1e6);
            }
            catch (Exception) {
            }

            return "?";
        }

        public static bool IsDebugBuild() {
#if DEBUG
            return true;
#else
            return false;
#endif
        }
    }
}