﻿using System.Collections.Generic;
using System.Diagnostics;

using Colorware.Core.Color;
using Colorware.Core.Enums;
using Colorware.Core.Functional.Option;

namespace Colorware.Core.Helpers {
    [DebuggerDisplay("TviEstimatorEntry<T>({Percentage})")]
    public struct TviEstimatorEntry<T> {
        public TviEstimatorEntry(int percentage, Spectrum spectrum, T tag) {
            Percentage = percentage;
            Spectrum = spectrum;
            Tag = tag;
        }

        public int Percentage { get; }
        public Spectrum Spectrum { get; }
        public T Tag { get; }
    }

    [DebuggerDisplay("TviEstimatorResult<T>(R={Ratio}, D={Tvi}, {First}, {Second})")]
    public struct TviEstimatorResult<T> {
        public TviEstimatorResult(TviEstimatorEntry<T> first, double firstTvi, TviEstimatorEntry<T> second,
                                  double secondTvi, double ratio, double tvi) {
            First = first;
            Second = second;
            Ratio = ratio;
            Tvi = tvi;
            FirstTvi = firstTvi;
            SecondTvi = secondTvi;
        }

        public TviEstimatorEntry<T> First { get; }
        public double FirstTvi { get; }
        public TviEstimatorEntry<T> Second { get; }
        public double SecondTvi { get; }
        public double Ratio { get; }
        public double Tvi { get; }
    }

    public interface IBestTargetTviEstimator {
        Option<TviEstimatorResult<T>> Estimate<T>(IEnumerable<TviEstimatorEntry<T>> entries,
                                                  double target,
                                                  DotgainMethod dotgainMethod,
                                                  SpectralMeasurementConditions measurementConditions,
                                                  DensityMethod densityMethod,
                                                  WhiteBase whiteBase);
    }
}