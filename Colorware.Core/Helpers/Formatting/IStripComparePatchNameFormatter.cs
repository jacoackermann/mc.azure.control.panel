﻿using Colorware.Core.Data.Models.StripCompareInterface;

namespace Colorware.Core.Helpers.Formatting {
    public class IStripComparePatchNameFormatter : IValueFormatter<IStripComparePatch> {
        private static IValueFormatter<IStripComparePatch> @default;

        public static IValueFormatter<IStripComparePatch> Default {
            get {
                if (@default == null)
                    @default = new IStripComparePatchNameFormatter();
                return @default;
            }
        }

        public string Format(IStripComparePatch patch) {
            if (patch.IsDotgain || patch.IsDotgainOnly)
                return string.Format("{0} ({1})", patch.Name, patch.Tint);
            return patch.Name;
        }
    }
}