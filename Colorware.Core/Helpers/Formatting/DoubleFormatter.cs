﻿using System.Globalization;

namespace Colorware.Core.Helpers.Formatting {
    public class DoubleFormatter : IValueFormatter<double> {
        private readonly string formatSpecifier;

        public static readonly DoubleFormatter Default = new DoubleFormatter();

        private DoubleFormatter() : this("F") {
        }

        public DoubleFormatter(string formatSpecifier) {
            this.formatSpecifier = formatSpecifier;
        }

        public string Format(double value) {
            return value.ToString(formatSpecifier, CultureInfo.InvariantCulture);
        }
    }
}