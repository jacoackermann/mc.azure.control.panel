﻿using System.Globalization;

namespace Colorware.Core.Helpers.Formatting {
    public class SizeInBytesFormatter : IValueFormatter<double> {
        public string Format(double valueInBytes) {
            var value = valueInBytes / 1024;
            if (value <= 1024)
                return formatToString(value, "kB");
            value /= 1024;
            if (value <= 1024)
                return formatToString(value, "MB");
            value /= 1024;
            return formatToString(value, "GB"); 
        }

        private static string formatToString(double value, string units) {
            return value.ToString("F", CultureInfo.InvariantCulture) + units;
        }
    }
}