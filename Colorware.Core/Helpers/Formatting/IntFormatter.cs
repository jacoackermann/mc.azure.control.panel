﻿using System.Globalization;

namespace Colorware.Core.Helpers.Formatting {
    public class IntFormatter : IValueFormatter<int> {
        public static readonly IntFormatter Default = new IntFormatter();

        public string Format(int value) {
            return value.ToString(CultureInfo.InvariantCulture);
        }
    }
}