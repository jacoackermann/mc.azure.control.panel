﻿namespace Colorware.Core.Helpers.Formatting {
    /// <summary>
    /// Defines an interface for formatting a type into a string for displaying to the user.
    /// </summary>
    /// <typeparam name="T">The type the instance can format.</typeparam>
    public interface IValueFormatter<in T> {
        string Format(T value);
    }
}