using System;

namespace Colorware.Core.Helpers.Attributes {
    [AttributeUsage(AttributeTargets.Field)]
    public class ShowInSelections : Attribute {
        public bool ShouldShow = true;

        public ShowInSelections(bool shouldShow) {
            ShouldShow = shouldShow;
        }
    }
}