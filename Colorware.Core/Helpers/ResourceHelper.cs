﻿using System;
using System.Reflection;

namespace Colorware.Core.Helpers {
    public static class ResourceHelper {
        public static String ReadStringResource(string resourceLocation) {
            var a = Assembly.GetExecutingAssembly();
            return ReadStringResource(resourceLocation, a);
        }

        public static String ReadStringResource(string resourceLocation, Assembly a) {
            var resNames = a.GetManifestResourceNames();
            foreach (var s in resNames) {
                if (s == resourceLocation) {
                    using (var stream = a.GetManifestResourceStream(s)) {
                        if (stream == null)
                            throw new Exception("Could not read resource");
                        var buffer = new byte[stream.Length];
                        stream.Read(buffer, 0, buffer.Length);
                        // Skip first 3 encoding characters.
                        return System.Text.Encoding.UTF8.GetString(buffer, 3, buffer.Length - 3);
                    }
                }
            }
            throw new Exception("Resource not found");
        }
    }
}