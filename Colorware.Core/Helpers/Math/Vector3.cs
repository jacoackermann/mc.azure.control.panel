using System;
using System.Globalization;

namespace Colorware.Core.Helpers.Math {
    /// <summary>
    /// Very simple Vector class. Probably not very efficient and doesn't offer a lot of functionality.
    /// </summary>
    public class Vector3 {
        public double X, Y, Z;

        public Vector3() : this(0.0) {
        }

        public Vector3(double initValue) : this(initValue, initValue, initValue) {
        }

        public Vector3(double x, double y, double z) {
            X = x;
            Y = y;
            Z = z;
        }

        public static Vector3 operator +(Vector3 a, Vector3 b) {
            return new Vector3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }
        public static Vector3 operator -(Vector3 a, Vector3 b) {
            return new Vector3(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }
        public static Vector3 operator *(Vector3 a, double b) {
            return new Vector3(a.X * b, a.Y * b, a.Z * b);
        }
        public static Vector3 operator /(Vector3 a, double b) {
            return new Vector3(a.X / b, a.Y / b, a.Z / b);
        }

        // The dot product
        public static double operator %(Vector3 a, Vector3 b) {
            return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
        }

        // The wedge product, as defined in Clifford Algebra, to get a TriVector
        public static double Wedge(Vector3 a, Vector3 b, Vector3 c) {
            return a.X * b.Y * c.Z - a.X * b.Z * c.Y - a.Y * b.X * c.Z + a.Y * b.Z * c.X + a.Z * b.X * c.Y - a.Z * b.Y * c.X;
        }

        public double Length() {
            return System.Math.Sqrt(this % this);
        }

        public static Vector3 operator *(Matrix3 m, Vector3 v) {
            return new Vector3(
                m.M11 * v.X + m.M12 * v.Y + m.M13 * v.Z,
                m.M21 * v.X + m.M22 * v.Y + m.M23 * v.Z,
                m.M31 * v.X + m.M32 * v.Y + m.M33 * v.Z
                );
        }

        #region Overrides of Object
        public override string ToString() {
            return String.Format("Vector3({0}, {1}, {2})"
                , X.ToString(CultureInfo.InvariantCulture)
                , Y.ToString(CultureInfo.InvariantCulture)
                , Z.ToString(CultureInfo.InvariantCulture)
                );
        }
        #endregion
    }
}