using System;
using System.Diagnostics;
using System.Globalization;

namespace Colorware.Core.Helpers.Math {
    /// <summary>
    /// Very simple 3x3 matrix class. Probably not very efficient and doesn't offer a lot of functionality.
    /// </summary>
    public class Matrix3 {
        public double[] Values;

        public double M11 {
            get { return Values[0]; }
            set { Values[0] = value; }
        }

        public double M12 {
            get { return Values[1]; }
            set { Values[1] = value; }
        }

        public double M13 {
            get { return Values[2]; }
            set { Values[2] = value; }
        }

        public double M21 {
            get { return Values[3]; }
            set { Values[3] = value; }
        }

        public double M22 {
            get { return Values[4]; }
            set { Values[4] = value; }
        }

        public double M23 {
            get { return Values[5]; }
            set { Values[5] = value; }
        }

        public double M31 {
            get { return Values[6]; }
            set { Values[6] = value; }
        }

        public double M32 {
            get { return Values[7]; }
            set { Values[7] = value; }
        }

        public double M33 {
            get { return Values[8]; }
            set { Values[8] = value; }
        }

        public Matrix3()
            : this(0.0) {
        }

        public Matrix3(double initValue) {
            Values = new[] {
                initValue, initValue, initValue,
                initValue, initValue, initValue,
                initValue, initValue, initValue,
            };
        }

        public Matrix3(double[] values) {
            Debug.Assert(values.Length == 9);
            Values = values;
        }

        public static Matrix3 Identity() {
            return new Matrix3(new[] {
                1.0, 0.0, 0.0,
                0.0, 1.0, 0.0,
                0.0, 0.0, 1.0
            });
        }

        public static Matrix3 operator *(Matrix3 first, Matrix3 second) {
            return new Matrix3(new[] {
                first.M11 * second.M11 + first.M12 * second.M21 + first.M13 * second.M31,
                first.M11 * second.M12 + first.M12 * second.M22 + first.M13 * second.M32,
                first.M11 * second.M13 + first.M12 * second.M23 + first.M13 * second.M33,
                first.M21 * second.M11 + first.M22 * second.M21 + first.M23 * second.M31,
                first.M21 * second.M12 + first.M22 * second.M22 + first.M23 * second.M32,
                first.M21 * second.M13 + first.M22 * second.M23 + first.M23 * second.M33,
                first.M31 * second.M11 + first.M32 * second.M21 + first.M33 * second.M31,
                first.M31 * second.M12 + first.M32 * second.M22 + first.M33 * second.M32,
                first.M31 * second.M13 + first.M32 * second.M23 + first.M33 * second.M33,
            });
        }

        #region Overrides of Object
        public override string ToString() {
            return String.Format("Matrix3({0}, {1}, {2} | {3}, {4}, {5} | {6}, {7}, {8})"
                , M11.ToString(CultureInfo.InvariantCulture)
                , M12.ToString(CultureInfo.InvariantCulture)
                , M13.ToString(CultureInfo.InvariantCulture)
                , M21.ToString(CultureInfo.InvariantCulture)
                , M22.ToString(CultureInfo.InvariantCulture)
                , M23.ToString(CultureInfo.InvariantCulture)
                , M31.ToString(CultureInfo.InvariantCulture)
                , M32.ToString(CultureInfo.InvariantCulture)
                , M33.ToString(CultureInfo.InvariantCulture)
                );
        }
        #endregion
    }
}