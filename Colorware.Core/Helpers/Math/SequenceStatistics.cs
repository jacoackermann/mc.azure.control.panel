using System;
using System.Collections.Generic;
using System.Linq;

namespace Colorware.Core.Helpers.Math {
    public class SequenceStatistics {
        public List<double> Items { get; private set; }

        public SequenceStatistics() {
            Items = new List<double>();
        }

        public SequenceStatistics(IEnumerable<double> items) {
            Items = new List<double>(items);
        }

        public void Add(double item) {
            Items.Add(item);
        }

        public int Count {
            get { return Items.Count; }
        }

        public double Sum {
            get {
                if (Items.Count <= 0)
                    return 0;
                return Items.Sum();
            }
        }

        public double Average {
            get {
                if (Items.Count <= 0)
                    return 0;
                return Items.Average();
            }
        }

        public double Max {
            get {
                if (Items.Count <= 0)
                    return 0;
                return Items.Max();
            }
        }

        public double Min {
            get {
                if (Items.Count <= 0)
                    return 0;
                return Items.Min();
            }
        }

        public double Variance() {
            throw new NotImplementedException(); // TODO.
        }
    }
}