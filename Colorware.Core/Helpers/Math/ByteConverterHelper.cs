﻿using System;

namespace Colorware.Core.Helpers.Math {
    public static class ByteConverterHelper {
        public static long ConvertKiloBytesToBytes(double kilobytes) {
            return Convert.ToInt64(kilobytes * 1024);
        }

        public static long ConvertMegaBytesToBytes(double megabytes) {
            return Convert.ToInt64(megabytes * 1024 * 1024);
        }

        public static long ConvertGigaBytesToBytes(double gigabytes) {
            return Convert.ToInt64(gigabytes * 1024 * 1024 * 1024);
        }

        public static double ConvertBytesToKilobytes(long bytes) {
            return bytes / 1024f;
        }

        public static double ConvertBytesToMegabytes(long bytes) {
            return ConvertBytesToKilobytes(bytes) / 1024f;
        }

        public static double ConvertBytesToGigabytes(long bytes) {
            return ConvertBytesToMegabytes(bytes) / 1024f;
        }
    }
}