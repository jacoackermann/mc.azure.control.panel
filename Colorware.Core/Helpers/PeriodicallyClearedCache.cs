﻿using System;
using System.Collections.Concurrent;

namespace Colorware.Core.Helpers {
    /// <summary>
    /// A thread safe cache that is periodically cleared.
    /// </summary>
    public class PeriodicallyClearedCache<TKey, TValue> : ICache<TKey, TValue> {
        private readonly ConcurrentDictionary<TKey, TValue> cacheDictionary = new ConcurrentDictionary<TKey, TValue>();

        private DateTime lastCacheClearTime = DateTime.Now;

        /// <summary>
        /// Interval after which the cache should be cleared.
        /// </summary>
        public TimeSpan CacheClearingInterval { get; set; }

        /// <param name="cacheClearingInterval">Interval after which the cache should be cleared.</param>
        public PeriodicallyClearedCache(TimeSpan cacheClearingInterval) {
            CacheClearingInterval = cacheClearingInterval;
        }

        public bool Get(TKey key, out TValue value) {
            // Clear cache if it's getting too old
            if (DateTime.Now - lastCacheClearTime > CacheClearingInterval) {
                lastCacheClearTime = DateTime.Now;
                cacheDictionary.Clear();
                value = default(TValue);
                return false;
            }

            return cacheDictionary.TryGetValue(key, out value);
        }

        public void Set(TKey key, TValue value) {
            cacheDictionary[key] = value;
        }

        public void Clear() {
            cacheDictionary.Clear();
        }
    }
}