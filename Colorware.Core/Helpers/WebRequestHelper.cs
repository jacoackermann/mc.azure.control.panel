﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

using Castle.Core.Logging;

using Colorware.Core.Helpers.WebRequestHelperExceptions;
using Colorware.Core.WebApi;

using JetBrains.Annotations;

namespace Colorware.Core.Helpers {
    [UsedImplicitly]
    public class WebRequestHelper {
        private readonly ILogger log;

        private readonly ProtoBufFormatter protoBufFormatter = new ProtoBufFormatter();

        /// <summary>
        /// The version of the running application, we crosscheck this with the version returned by the server.
        /// </summary>
        private readonly string applicationVersion;

        // Use Protocol Buffer formatter to deserialize the result.
        private MediaTypeFormatter[] formatters {
            get { return new MediaTypeFormatter[] {protoBufFormatter}; }
        }

        public WebRequestHelper([NotNull] ILogger log,
                                [NotNull] IApplicationVersionRetriever applicationVersionRetriever) {
            if (log == null) throw new ArgumentNullException("log");
            if (applicationVersionRetriever == null)
                throw new ArgumentNullException(nameof(applicationVersionRetriever));
            this.log = log;
            applicationVersion = applicationVersionRetriever.GetVersion();
        }

        private static readonly Dictionary<Uri, HttpClient> currentClients = new Dictionary<Uri, HttpClient>();

        public static HttpClient GetProtoBufHttpClient(Uri baseAddress) {
            HttpClient client;
            if (currentClients.TryGetValue(baseAddress, out client))
                return client;
            client = new HttpClient {BaseAddress = baseAddress};
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-protobuf"));
            currentClients[baseAddress] = client;
            return client;
        }

        /// <exception cref="WebApiUnhandledServerException">Unhandled exception occurred on the server.</exception>
        /// <exception cref="TaskCanceledException" />
        public async Task<T> GetAsProtoBufAsync<T>(Uri baseAddress, string resourceUri,
                                                   CancellationToken cancellationToken) {
            var client = GetProtoBufHttpClient(baseAddress);
            using (var response = await client.GetAsync(resourceUri, cancellationToken).ConfigureAwait(false)) {
                throwIfInMaintenanceMode(response);
                throwIfIncompatibleVersions(response);
                if (response.StatusCode != HttpStatusCode.OK) {
                    var error = await readAsProtoBufAsync<ExceptionErrorMessageContent>(response, cancellationToken)
                                    .ConfigureAwait(false);
                    logAndThrowError(resourceUri,
                                     WebApiUnhandledServerException.Create(resourceUri, error.ExceptionInfos));
                }

                return
                    await response.Content.ReadAsAsync<T>(formatters, cancellationToken).ConfigureAwait(false);
            }
        }

        /// <exception cref="WebApiUnhandledServerException">Unhandled exception occurred on the server.</exception>
        public Task<T> GetAsProtoBufAsync<T>(Uri baseAddress, string resourceUri) {
            return GetAsProtoBufAsync<T>(baseAddress, resourceUri, CancellationToken.None);
        }

        /// <exception cref="WebApiUnhandledServerException">Unhandled exception occurred on the server.</exception>
        private void logAndThrowError([NotNull] string url, [NotNull] WebApiUnhandledServerException exception) {
            log.ErrorFormat("Web API request error: '{0}'", url);
            throw exception;
        }

        /// <exception cref="WebApiUnhandledServerException">Unhandled exception occurred on the server.</exception>
        /// <exception cref="TaskCanceledException" />
        public async Task PostAsProtoBufAsync<TParameter>([NotNull] Uri baseAddress, [NotNull] string resourceUri,
                                                          TParameter parameter, CancellationToken cancellationToken) {
            if (baseAddress == null) throw new ArgumentNullException("baseAddress");
            if (resourceUri == null) throw new ArgumentNullException("resourceUri");

            var client = GetProtoBufHttpClient(baseAddress);
            await PostAsProtoBufAsync(client, resourceUri, parameter, cancellationToken).ConfigureAwait(false);
        }

        /// <exception cref="WebApiUnhandledServerException">Unhandled exception occurred on the server.</exception>
        public Task PostAsProtoBufAsync<TParameter>([NotNull] Uri baseAddress, [NotNull] string resourceUri,
                                                    TParameter parameter) {
            return PostAsProtoBufAsync(baseAddress, resourceUri, parameter, CancellationToken.None);
        }

        /// <exception cref="WebApiUnhandledServerException">Unhandled exception occurred on the server.</exception>
        /// <exception cref="TaskCanceledException" />
        public async Task<TResult> PostAsProtoBufAsync<TParameter, TResult>([NotNull] Uri baseAddress,
                                                                            [NotNull] string resourceUri,
                                                                            TParameter parameter,
                                                                            CancellationToken cancellationToken) {
            if (baseAddress == null) throw new ArgumentNullException("baseAddress");
            if (resourceUri == null) throw new ArgumentNullException("resourceUri");

            var client = GetProtoBufHttpClient(baseAddress);
            return await PostAsProtoBufAsync<TParameter, TResult>(client, resourceUri, parameter, cancellationToken)
                       .ConfigureAwait(false);
        }

        /// <exception cref="WebApiUnhandledServerException">Unhandled exception occurred on the server.</exception>
        public Task<TResult> PostAsProtoBufAsync<TParameter, TResult>([NotNull] Uri baseAddress,
                                                                      [NotNull] string resourceUri,
                                                                      TParameter parameter) {
            return PostAsProtoBufAsync<TParameter, TResult>(baseAddress, resourceUri, parameter,
                                                            CancellationToken.None);
        }

        /// <exception cref="WebApiUnhandledServerException">Unhandled exception occurred on the server.</exception>
        /// <exception cref="TaskCanceledException" />
        public async Task PostAsProtoBufAsync<TParameter>([NotNull] HttpClient client, [NotNull] string resourceUri,
                                                          TParameter parameter, CancellationToken cancellationToken) {
            if (client == null) throw new ArgumentNullException("client");
            if (resourceUri == null) throw new ArgumentNullException("resourceUri");

            using (var response = await client.PostAsync(resourceUri, parameter, protoBufFormatter, cancellationToken)
                                              .ConfigureAwait(false)) {
                throwIfInMaintenanceMode(response);
                throwIfIncompatibleVersions(response);
                // NoContent is allowed in this overload as we expect no return value
                if (response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.NoContent) {
                    var error = await readAsProtoBufAsync<ExceptionErrorMessageContent>(response, cancellationToken)
                                    .ConfigureAwait(false);
                    logAndThrowError(resourceUri,
                                     WebApiUnhandledServerException.Create(resourceUri, error.ExceptionInfos));
                }
            }
        }

        /// <exception cref="WebApiUnhandledServerException">Unhandled exception occurred on the server.</exception>
        public Task PostAsProtoBufAsync<TParameter>([NotNull] HttpClient client, [NotNull] string resourceUri,
                                                    TParameter parameter) {
            return PostAsProtoBufAsync(client, resourceUri, parameter, CancellationToken.None);
        }

        /// <exception cref="WebApiUnhandledServerException">Unhandled exception occurred on the server.</exception>
        /// <exception cref="TaskCanceledException" />
        public async Task<TResult> PostAsProtoBufAsync<TParameter, TResult>([NotNull] HttpClient client,
                                                                            [NotNull] string resourceUri,
                                                                            TParameter parameter,
                                                                            CancellationToken cancellationToken) {
            if (client == null) throw new ArgumentNullException("client");
            if (resourceUri == null) throw new ArgumentNullException("resourceUri");

            using (var response = await client.PostAsync(resourceUri, parameter, protoBufFormatter, cancellationToken)
                                              .ConfigureAwait(false)) {
                throwIfInMaintenanceMode(response);
                throwIfIncompatibleVersions(response);
                if (response.StatusCode != HttpStatusCode.OK) {
                    var error = await readAsProtoBufAsync<ExceptionErrorMessageContent>(response, cancellationToken)
                                    .ConfigureAwait(false);
                    logAndThrowError(resourceUri,
                                     WebApiUnhandledServerException.Create(resourceUri, error.ExceptionInfos));
                }

                return await response.Content.ReadAsAsync<TResult>(formatters, cancellationToken).ConfigureAwait(false);
            }
        }

        /// <exception cref="WebApiUnhandledServerException">Unhandled exception occurred on the server.</exception>
        public Task<TResult> PostAsProtoBufAsync<TParameter, TResult>([NotNull] HttpClient client,
                                                                      [NotNull] string resourceUri,
                                                                      TParameter parameter) {
            return PostAsProtoBufAsync<TParameter, TResult>(client, resourceUri, parameter, CancellationToken.None);
        }

        /// <exception cref="TaskCanceledException" />
        private Task<TResult> readAsProtoBufAsync<TResult>([NotNull] HttpResponseMessage response,
                                                           CancellationToken cancellationToken) {
            if (response == null) throw new ArgumentNullException("response");

            return response.Content.ReadAsAsync<TResult>(formatters, cancellationToken);
        }

        public async Task<T> GetAsJsonAsync<T>([NotNull] Uri baseAddress, [NotNull] string resourceUri,
                                               CancellationToken cancellationToken) {
            if (baseAddress == null) throw new ArgumentNullException("baseAddress");
            if (resourceUri == null) throw new ArgumentNullException("resourceUri");

            using (var client = new HttpClient {BaseAddress = baseAddress}) {
                try {
                    using (var response = await client.GetAsync(resourceUri, cancellationToken).ConfigureAwait(false)) {
                        throwIfInMaintenanceMode(response);
                        throwIfIncompatibleVersions(response);
                        if (response.StatusCode != HttpStatusCode.OK)
                            throw new Exception(await response.Content.ReadAsStringAsync().ConfigureAwait(false));

                        return await response.Content.ReadAsAsync<T>(cancellationToken).ConfigureAwait(false);
                    }
                }
                catch (HttpRequestException) {
                    throw;
                }
                catch (Exception ex) {
                    throw TranslateErrorFromJSon(ex);
                }
            }
        }

        public Task<T> GetAsJsonAsync<T>([NotNull] Uri baseAddress, [NotNull] string resourceUri) {
            if (baseAddress == null) throw new ArgumentNullException("baseAddress");
            if (resourceUri == null) throw new ArgumentNullException("resourceUri");

            return GetAsJsonAsync<T>(baseAddress, resourceUri, CancellationToken.None);
        }

        public async Task PostAsJsonAsync<TParameter>([NotNull] Uri baseAddress, [NotNull] string resourceUri,
                                                      TParameter parameter) {
            if (baseAddress == null) throw new ArgumentNullException("baseAddress");
            if (resourceUri == null) throw new ArgumentNullException("resourceUri");

            using (var client = new HttpClient {BaseAddress = baseAddress}) {
                try {
                    using (var response = await client.PostAsJsonAsync(resourceUri, parameter).ConfigureAwait(false)) {
                        throwIfInMaintenanceMode(response);
                        throwIfIncompatibleVersions(response);
                        // NoContent is allowed in this overload as we expect no return value
                        if (response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.NoContent)
                            throw new Exception(await response.Content.ReadAsStringAsync().ConfigureAwait(false));
                    }
                }
                catch (HttpRequestException) {
                    throw;
                }
                catch (Exception ex) {
                    throw TranslateErrorFromJSon(ex);
                }
            }
        }

        /// <exception cref="AggregateException" />
        /// <exception cref="HttpRequestException" />
        /// <exception cref="Exception" />
        public async Task<TResult> PostAsJsonAsync<TParameter, TResult>([NotNull] Uri baseAddress,
                                                                        [NotNull] string resourceUri,
                                                                        TParameter parameter) {
            if (baseAddress == null) throw new ArgumentNullException("baseAddress");
            if (resourceUri == null) throw new ArgumentNullException("resourceUri");

            using (var client = new HttpClient {BaseAddress = baseAddress}) {
                TResult result;

                try {
                    using (var response = await client.PostAsJsonAsync(resourceUri, parameter).ConfigureAwait(false)) {
                        throwIfInMaintenanceMode(response);
                        throwIfIncompatibleVersions(response);
                        if (response.StatusCode != HttpStatusCode.OK)
                            throw new Exception(await response.Content.ReadAsStringAsync().ConfigureAwait(false));

                        result = await response.Content.ReadAsAsync<TResult>().ConfigureAwait(false);
                    }
                }
                catch (HttpRequestException) {
                    throw;
                }
                catch (Exception ex) {
                    throw TranslateErrorFromJSon(ex);
                }

                return result;
            }
        }

        private class JsonErrorMessage {
            public string Message { get; set; }
            public string ExceptionMessage { get; set; }
            public JsonErrorMessage InnerException { get; set; }
            public Dictionary<string, string[]> ModelState { get; set; }
        }

        /// <exception cref="Exception" />
        public static Exception TranslateErrorFromJSon([NotNull] Exception ex) {
            if (ex == null) throw new ArgumentNullException("ex");

            try {
                var error = new JavaScriptSerializer().Deserialize<JsonErrorMessage>(ex.Message);
                var msg = error.ExceptionMessage;
                if (error.ModelState != null && error.ModelState.Any())
                    return new AggregateException(msg, error.ModelState.Select(generateExceptionForModelStateEntry));

                var inner = error.InnerException;
                while (inner != null) {
                    msg += Environment.NewLine;
                    msg += inner.ExceptionMessage;
                    inner = inner.InnerException;
                }

                return new Exception(msg);
            }
            catch {
                // Throw original.
                return ex;
            }
        }

        private static Exception generateExceptionForModelStateEntry([NotNull] KeyValuePair<string, string[]> pair) {
            var msg = string.Join("; ", pair.Value);
            return new Exception(msg);
        }

        /// <summary>
        /// Check if the server is in 'maintenance mode' (ServiceUnavailable) and throw an <see
        /// cref="ServerIsInMaintenanceModeException"/> if it is.
        /// </summary>
        /// <param name="response">The response of the request.</param>
        /// <exception cref="ServerIsInMaintenanceModeException">
        /// Throw when the server returns status code <see cref="HttpStatusCode.ServiceUnavailable"/>.
        /// </exception>
        private void throwIfInMaintenanceMode(HttpResponseMessage response) {
            if (response.StatusCode == HttpStatusCode.ServiceUnavailable) {
                throw new ServerIsInMaintenanceModeException("Server returned 'HttpStatusCode.StatusUnavailable'");
            }
        }

        private void throwIfIncompatibleVersions(HttpResponseMessage response) {
            if (response.StatusCode != HttpStatusCode.OK) {
                return;
            }
            IEnumerable<string> values;
            if(response.Headers.TryGetValues(ApplicationVersionResponseHeader.HeaderName, out values)) {
                var value = values.First();
                if (value != applicationVersion) {
                    throw new ServerHasDifferentVersionException($"Client version: {applicationVersion}, server version: {value}");
                }
            }
        }
    }
}