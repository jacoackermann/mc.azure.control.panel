﻿using System;

using JetBrains.Annotations;

namespace Colorware.Core.Helpers {
    /// <summary>
    /// <para>
    /// <inheritdoc cref="IApplicationVersionRetriever"/>
    /// </para>
    /// <para>
    /// This version caches the version retrieved from an inner retriever. The inner retriever's <see
    /// cref="GetVersion"/> method will only be called once.
    /// </para>
    /// </summary>
    public class CachedApplicationVersionRetriever : IApplicationVersionRetriever {
        private readonly IApplicationVersionRetriever innerRetriever;
        private string cachedVersion;

        public CachedApplicationVersionRetriever([NotNull] IApplicationVersionRetriever innerRetriever) {
            if (innerRetriever == null) throw new ArgumentNullException(nameof(innerRetriever));
            this.innerRetriever = innerRetriever;
        }

        public string GetVersion() =>
            cachedVersion ?? (cachedVersion = innerRetriever.GetVersion());
    }
}