﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Colorware.Core.Data.Specification;
using Colorware.Core.Extensions;

using JetBrains.Annotations;

namespace Colorware.Core.Helpers {
    public static class BaseModelReflectionHelper {
        public static ServiceModel GetServiceModel([NotNull] Type type) {
            type = GlobalContainer.Current.GetImplementationType(type);

            var services = (ServiceModel[])type.GetCustomAttributes(typeof(ServiceModel), true);
            if (services.Length != 1)
                throw new InvalidProgramException(
                    "Model was not properly annotated, could not find ServiceModel annotation: " + type.Name);
            return services[0];
        }

        public static String GetTableName<T>() where T : IBaseModel {
            return GetTableName(typeof(T));
        }

        public static String GetTableName(Type type) {
            var model = GetServiceModel(type);
            if (model.TableName != null)
                return model.TableName;

            return model.ServiceName;
        }

        public static bool GetAccessTable([NotNull] Type type,
                                          [CanBeNull] out string accessTable,
                                          [CanBeNull] out string accessField) {
            var attr = (RemoteClientAttribute)type.GetCustomAttribute(typeof(RemoteClientAttribute));
            accessTable = attr?.AccessTable;
            accessField = attr?.AccessField;
            return attr != null;
        }

        [CanBeNull]
        public static PropertyInfo GetRelationPropertiesWithParentKeyName<T>(string parentKeyName) {
            return GetRelationPropertiesWithParentKeyName(typeof(T), parentKeyName);
        }

        [CanBeNull]
        public static PropertyInfo GetRelationPropertiesWithParentKeyName(Type type, string parentKeyName) {
            var props = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);

            return props.FirstOrDefault(p => {
                var related = GetCustomAttribute<Related>(p);
                return related != null && related.ParentKeyName == parentKeyName;
            });
        }

        [CanBeNull]
        public static PropertyInfo GetPropertyWithServiceFieldListingName(Type type, string serviceFieldListingName) {
            // TODO: Caching if on hot path
            return GetServiceFieldProperties(type).FirstOrDefault(
                p => p.GetCustomAttribute<ServiceField>().ListingName.Equals(serviceFieldListingName));
        }

        private static readonly ConcurrentDictionary<Type, PropertyInfo> primaryKeyPropertyCache =
            new ConcurrentDictionary<Type, PropertyInfo>();

        [NotNull]
        public static PropertyInfo GetPrimaryKeyProperty(Type type) {
            type = GlobalContainer.Current.GetImplementationType(type);

            PropertyInfo propertyInfo;
            if (primaryKeyPropertyCache.TryGetValue(type, out propertyInfo))
                return propertyInfo;

            var props = GetServiceFieldProperties(type);

            foreach (var prop in props.Where(
                p => {
                    var serviceField = GetCustomAttribute<ServiceField>(p);
                    return serviceField != null && serviceField.IsPrimaryKey;
                })) {
                primaryKeyPropertyCache.TryAdd(type, prop);
                return prop;
            }

            throw new InvalidOperationException("BaseModel has no Id field: " + type.AssemblyQualifiedName);
        }

        [NotNull]
        public static string GetPrimaryKeyColumn(Type type) {
            var primaryKeyProp = GetPrimaryKeyProperty(type);
            var serviceFieldAttr = GetCustomAttribute<ServiceField>(primaryKeyProp);
            if (serviceFieldAttr == null) {
                throw new InvalidOperationException(
                    $"Primary key property has no {nameof(ServiceField)} attribute. Type ({type.AssemblyQualifiedName})\tProperty ({primaryKeyProp.Name})");
            }

            return serviceFieldAttr.ListingName;
        }

        private static readonly ConcurrentDictionary<Type, PropertyInfo[]> relatedPropertyInfoCache =
            new ConcurrentDictionary<Type, PropertyInfo[]>();

        public static PropertyInfo[] GetRelatedProperties(Type type) {
            type = GlobalContainer.Current.GetImplementationType(type);

            PropertyInfo[] properties;

            if (relatedPropertyInfoCache.TryGetValue(type, out properties))
                return properties;

            var props = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            properties = props.Where(prop => GetCustomAttribute<Related>(prop) != null).ToArray();
            relatedPropertyInfoCache.TryAdd(type, properties);

            return properties;
        }

        /// <summary>
        /// Find a default property that can be used to preview an instance for this type.
        /// </summary>
        /// <param name="type">The type to use.</param>
        /// <returns>A default property or null if none could be found.</returns>
        [CanBeNull]
        public static PropertyInfo GetDefaultDisplayProperty(Type type) {
            var props = GetServiceFieldProperties(type);
            // <-- May want to check for special attribute here and return that...
            var firstString = props.FirstOrDefault(prop => prop.PropertyType == typeof(string));
            if (firstString != null)
                return firstString;
            return props.FirstOrDefault();
        }

        public static PropertyInfo[] GetServiceFieldProperties<T>() {
            return GetServiceFieldProperties(typeof(T));
        }

        private static readonly ConcurrentDictionary<Type, PropertyInfo[]> serviceFieldPropertyInfoCache =
            new ConcurrentDictionary<Type, PropertyInfo[]>();

        public static PropertyInfo[] GetServiceFieldProperties(Type type) {
            type = GlobalContainer.Current.GetImplementationType(type);

            PropertyInfo[] properties;

            if (serviceFieldPropertyInfoCache.TryGetValue(type, out properties))
                return properties;

            var props = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            properties = props.Where(prop => GetCustomAttribute<ServiceField>(prop) != null).ToArray();
            serviceFieldPropertyInfoCache.TryAdd(type, properties);

            return properties;
        }

        public static IEnumerable<ServiceField> GetServiceFieldsForAllProperties(Type type) {
            var propertyInfos = GetServiceFieldProperties(type);
            return
                propertyInfos.Select(GetCustomAttribute<ServiceField>).
                              ToArray();
        }

        private static readonly ConcurrentDictionary<Tuple<PropertyInfo, Type>, object> customAttributeCache =
            new ConcurrentDictionary<Tuple<PropertyInfo, Type>, object>();

        [CanBeNull]
        public static TAttribute GetCustomAttribute<TAttribute>(PropertyInfo property) where TAttribute : Attribute {
            object attribute;

            var keyTuple = Tuple.Create(property, typeof(TAttribute));
            if (customAttributeCache.TryGetValue(keyTuple, out attribute))
                return (TAttribute)attribute;

            attribute = property.GetCustomAttribute<TAttribute>(false);
            customAttributeCache.TryAdd(keyTuple, attribute);

            return (TAttribute)attribute;
        }

        private static readonly ConcurrentDictionary<PropertyInfo, Action<object, object>> setterDelegatesCache =
            new ConcurrentDictionary<PropertyInfo, Action<object, object>>();

        public static void CallSetter(this PropertyInfo property, object instance, object value) {
            Action<object, object> setterDelegate;

            if (!setterDelegatesCache.TryGetValue(property, out setterDelegate)) {
                setterDelegate = property.CreateSetterDelegate();
                setterDelegatesCache.TryAdd(property, setterDelegate);
            }

            setterDelegate(instance, value);
        }

        /// <summary>
        /// Grab the default sort order for the given type and return it.
        /// </summary>
        /// <param name="type">The type to get the sortorder for. It should have a <see cref="ServiceModel"/> attribute.</param>
        /// <returns>The default sort order field name or null.</returns>
        public static string GetSortField(Type type) {
            return GetServiceModel(type).DefaultSortOrder;
        }

        /// <summary>
        /// Get the sort order direction for the given type and return it.
        /// </summary>
        /// <param name="type">The type to return the sort order direction for. Should have a <see cref="ServiceModel"/> attribute.</param>
        /// <returns>The sort order direction for the type.</returns>
        public static ServiceModel.SortOrders GetSortOrderDirection(Type type) {
            return GetServiceModel(type).SortOrderDirection;
        }
    }
}