﻿using System;

namespace Colorware.Core.Helpers.WebRequestHelperExceptions {
    /// <summary>
    /// Exception that is thrown to indicate that a connection to the server was attempted, but the
    /// server returned a response that made us assume it was in maintenance mode or oherwise unable
    /// to respond at the time (Service Unavailable).
    /// </summary>
    public class ServerIsInMaintenanceModeException : Exception {
        public ServerIsInMaintenanceModeException(string message) : base(message) { }
        public ServerIsInMaintenanceModeException(string message, Exception innerException) : base(message, innerException) { }
    }
}