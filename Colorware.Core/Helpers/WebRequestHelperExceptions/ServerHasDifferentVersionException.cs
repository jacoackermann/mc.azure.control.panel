﻿using System;

namespace Colorware.Core.Helpers.WebRequestHelperExceptions {
    /// <summary>
    /// When attemting to connect to the server, the server returned a response that contained a
    /// different version header than the version we are currently running with.
    /// </summary>
    public class ServerHasDifferentVersionException : Exception {
        public ServerHasDifferentVersionException(string message) : base(message) { }
        public ServerHasDifferentVersionException(string message, Exception innerException) : base(message, innerException) { }
    }
}