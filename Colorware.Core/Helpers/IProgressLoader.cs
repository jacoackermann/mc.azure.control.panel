﻿using System.ComponentModel;

namespace Colorware.Core.Helpers {
    /// <summary>
    /// Keeps count of loading and loaded items (doesn't show any interface, just for administration).
    /// </summary>
    public interface IProgressLoader : INotifyPropertyChanged {
        int Count { get; }
        int ItemsDone { get; }
        bool IsDone { get; }
        bool IsNotDone { get; }
        void Reset();
        void Begin(int addCount = 1);
        void End(int removeCount = 1);
    }
}