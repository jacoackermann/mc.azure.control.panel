﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

using Colorware.Core.Logging;

using JetBrains.Annotations;

namespace Colorware.Core.Helpers {
    /// <inheritdoc />
    [UsedImplicitly]
    public class ServerConnectionChecker : IServerConnectionChecker {
        private readonly IApplicationVersionRetriever versionRetriever;
        private readonly ILog log;
        private const string VersionApiPath = "/api/ConnectionStatus/Version";

        public ServerConnectionChecker([NotNull] IApplicationVersionRetriever versionRetriever,
                                       [NotNull] ILog log) {
            if (versionRetriever == null) throw new ArgumentNullException(nameof(versionRetriever));
            if (log == null) throw new ArgumentNullException(nameof(log));
            this.versionRetriever = versionRetriever;
            this.log = log;
        }

        /// <inheritdoc />
        public async Task<ServerConnectionStatus> CheckConnection(Uri url, CancellationToken token) {
            try {
                log.Info($"Checking connection to '{url.AbsoluteUri}'");
                using (var client = new HttpClient()) {
                    var finalUri = new Uri(url, VersionApiPath);
                    var response = await client.GetAsync(finalUri, token);
                    if (response.StatusCode == HttpStatusCode.ServiceUnavailable) {
                        log.Info("Got statuscode 'ServiceUnavailable', server is probably in maintenance mode.");
                        // This probably means that the server is in "Maintenance mode".
                        return ServerConnectionStatus.MaintenanceMode;
                    }

                    if (response.StatusCode == HttpStatusCode.InternalServerError) {
                        log.Info($"Got status code `{response.StatusCode}`, server does not seem to be operating correctly");
                        return ServerConnectionStatus.InternalServerError;
                    }

                    if (response.StatusCode != HttpStatusCode.OK) {
                        log.Info(
                            $"Got status code '{response.StatusCode}, server does not seem to support the called service.");
                        return ServerConnectionStatus.IncompatibleService;
                    }

                    var responseStr = await response.Content.ReadAsStringAsync();
                    if (!versionMatchesWithLocal(responseStr)) {
                        log.Info("Client and server version did not match.");
                        return ServerConnectionStatus.IncompatibleVersion;
                    }

                    log.Info("Server is available!");
                    return ServerConnectionStatus.Available;
                }
            }
            // NB: the when clause is important because the HttpClient sometimes throws an OperationCanceledException
            // when actually a timeout occured
            catch (OperationCanceledException) when (token.IsCancellationRequested) {
                return ServerConnectionStatus.Unavailable;
            }
            // Usually when there is no server listening (i.e. 'connection refused')
            catch (HttpRequestException e) when ((e.InnerException as WebException)?.Status ==
                                                 WebExceptionStatus.ConnectFailure) {
                log.Info($"Server could not be contacted at: {url.AbsoluteUri}");
                return ServerConnectionStatus.Unavailable;
            }
            catch (Exception ex) {
                log.Warn($"Exception while checking server connection: {ex.Message}", ex);
                return ServerConnectionStatus.Unavailable;
            }
        }

        private bool versionMatchesWithLocal(string remoteVersion) {
            var localVersion = versionRetriever.GetVersion();
            log.Info($"Checking client/server version: {localVersion}/{remoteVersion}");
            return remoteVersion == localVersion;
        }
    }
}