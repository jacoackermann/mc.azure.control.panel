﻿using System;
using System.Diagnostics;

using Colorware.Core.ErrorReporting;

using JetBrains.Annotations;

namespace Colorware.Core.Helpers {
    public class SystemBrowserOpener : IBrowserOpener {
        private readonly IErrorReporter errorReporter;

        public SystemBrowserOpener([NotNull] IErrorReporter errorReporter) {
            if (errorReporter == null) throw new ArgumentNullException("errorReporter");
            this.errorReporter = errorReporter;
        }

        public void OpenUrl(string url) {
            try {
                Process.Start(url);
            }
            catch {
                try {
                    // Fallback to IE.
                    var startInfo = new ProcessStartInfo("iexplore.exe", url);
                    Process.Start(startInfo);
                }
                catch(Exception ex) {
                    var msg = "Could not open URL: " + url + " (" + ex.Message + ")";
                    errorReporter.ReportError(msg, "Could not start browser", ex);
                }
            }
        }
    }
}