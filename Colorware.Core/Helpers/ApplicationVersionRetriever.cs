﻿namespace Colorware.Core.Helpers {
    /// <inheritdoc />
    public class ApplicationVersionRetriever : IApplicationVersionRetriever {
        /// <inheritdoc />
        public string GetVersion() {
            return ApplicationHelper.GetVersion();
        }
    }
}