﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Colorware.Core.Helpers {
    public enum ServerConnectionStatus {
        /// <summary>
        /// The connection could be established and is valid.
        /// </summary>
        Available,
        /// <summary>
        /// No connection could be made (server down?).
        /// </summary>
        Unavailable,
        /// <summary>
        /// A connection could be established, but the server was most likely in maintenance mode.
        /// </summary>
        MaintenanceMode,
        /// <summary>
        /// A connection could be established, but the server did not respond in a way that we expected.
        /// This could indicate that the server was not actually an (up-to-date) MeasureColor server.
        /// </summary>
        IncompatibleService,
        /// <summary>
        /// A connection could be established and the server did respond, but the response indicates
        /// that the version and server versions do not match.
        /// </summary>
        IncompatibleVersion,
        /// <summary>
        /// The server returned a code indicating an internal server error. This most likely
        /// indicates a configuration error or an invalid database connection.
        /// </summary>
        InternalServerError
    }

    /// <inheritdoc cref="CheckConnection"/>
    public interface IServerConnectionChecker {
        /// <summary>
        /// Checks the connection to the MeasureColor server given by a known URL. Returns the status as
        /// an <see cref="ServerConnectionStatus"/> member.
        /// </summary>
        /// <param name="url">The base url of the server to check.</param>
        /// <param name="token">Cancellation token.</param>
        /// <returns>The status of the connection.</returns>
        Task<ServerConnectionStatus> CheckConnection(Uri url, CancellationToken token);
    }
}