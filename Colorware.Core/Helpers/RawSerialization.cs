﻿using System;
using System.Runtime.InteropServices;

namespace Colorware.Core.Helpers {
    public static class RawSerialization {
        public static object RawDeserialize(byte[] rawData, Type theType) {
            return RawDeserialize(rawData, 0, theType);
        }

        /// <summary>
        /// Can be used to take raw data coming in from a C/C++ module and transform it into
        /// a .NET object of the given type.
        /// </summary>
        /// <param name="rawData">The raw bytes to convert.</param>
        /// <param name="position">The offset where the object info starts in the rawData array.</param>
        /// <param name="theType">The type to convert to.</param>
        /// <returns>An instance of the object with of type <paramref name="theType" /></returns>
        public static object RawDeserialize(byte[] rawData, int position, Type theType) {
            int rawSize = Marshal.SizeOf(theType);
            if (rawSize > rawData.Length - position)
                return null;
            IntPtr buffer = Marshal.AllocHGlobal(rawSize);
            Marshal.Copy(rawData, position, buffer, rawSize);
            object result = Marshal.PtrToStructure(buffer, theType);
            Marshal.FreeHGlobal(buffer);
            return result;
        }

        /// <summary>
        /// Take a .NET object and serialize it into a byte[] array which can be used to pass data into
        /// a C/C++ module.
        /// </summary>
        /// <param name="obj">The object to serialize.</param>
        /// <returns>The raw byte data.</returns>
        public static byte[] RawSerialize(object obj) {
            int rawSize = Marshal.SizeOf(obj);
            IntPtr buffer = Marshal.AllocHGlobal(rawSize);
            Marshal.StructureToPtr(obj, buffer, false);
            var rawData = new byte[rawSize];
            Marshal.Copy(buffer, rawData, 0, rawSize);
            Marshal.FreeHGlobal(buffer);
            return rawData;
        }
    }
}