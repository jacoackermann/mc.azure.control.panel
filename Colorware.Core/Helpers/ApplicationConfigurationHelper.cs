﻿using System;
using System.Configuration;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;

namespace Colorware.Core.Helpers {
    /// <summary>
    /// Helper class for handling application configuration items (ConfigurationManager). Currently only holds
    /// web service related helpers.
    /// </summary>
    public static class ApplicationConfigurationHelper {
        /// <summary>
        /// Get the endpoint by the given name from the default application configuration.
        /// </summary>
        /// <param name="endPointName">The name of the endpoint</param>
        /// <returns>The endpoint</returns>
        public static ChannelEndpointElement GetEndPoint(string endPointName) {
            return GetEndPoint(endPointName, null);
        }

        /// <summary>
        /// Get the endpoint by the given name from the file with the given config path.
        /// </summary>
        /// <param name="endPointName">The name of the endpoint</param>
        /// <param name="configurationPath">The path of the config file</param>
        /// <returns>The endpoint</returns>
        public static ChannelEndpointElement GetEndPoint(string endPointName, string configurationPath) {
            var clientSection = (ClientSection)getSection(configurationPath, "system.serviceModel/client");
            foreach (ChannelEndpointElement endPoint in clientSection.Endpoints) {
                if (endPoint.Name == endPointName)
                    return endPoint;
            }
            return null;
        }

        /// <summary>
        /// Get the binding by the given name from the default application configuration.
        /// </summary>
        /// <param name="bindingName">The name of the binding</param>
        /// <returns>The binding</returns>
        public static Binding GetBinding(string bindingName) {
            return GetBinding(bindingName, null);
        }

        /// <summary>
        /// Get the binding by the given name from the file with the given config path.
        /// </summary>
        /// <param name="bindingName">The name of the binding</param>
        /// <param name="configurationPath">The path of the config file</param>
        /// <returns>The binding</returns>
        public static Binding GetBinding(string bindingName, string configurationPath) {
            var bindingsSection = (BindingsSection)getSection(configurationPath, "system.serviceModel/bindings");
            foreach (BindingCollectionElement bindingElement in bindingsSection.BindingCollections) {
                foreach (var configuredBinding in bindingElement.ConfiguredBindings) {
                    if (bindingName == configuredBinding.Name) {
                        var binding = createBindingForType(bindingElement.BindingType);
                        configuredBinding.ApplyConfiguration(binding);
                        return binding;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Get a section from the default config or the config with the given name.
        /// </summary>
        /// <param name="filePath">The path of the config file to use. If null, the default application config will be used.</param>
        /// <param name="section">The section to retrieve.</param>
        /// <returns>The section.</returns>
        private static object getSection(string filePath, string section) {
            if (filePath == null)
                return ConfigurationManager.GetSection(section);
            var map = new ExeConfigurationFileMap {ExeConfigFilename = filePath};
            var config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
            return config.GetSection(section);
        }

        /// <summary>
        /// Create a binding instance for the given binding type.
        /// </summary>
        /// <param name="bindingType">The type to create an instance for.</param>
        /// <returns>An instance of the binding.</returns>
        private static Binding createBindingForType(Type bindingType) {
            var constructor = bindingType.GetConstructor(new Type[] {});
            if (constructor == null)
                throw new Exception("Could not create instance for type: " + bindingType.Name);
            var instance = constructor.Invoke(new object[] {});
            return (Binding)instance;
        }
    }
}