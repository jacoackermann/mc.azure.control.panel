﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Colorware.Core.Color;
using Colorware.Core.Enums;
using Colorware.Core.Functional.Option;

using JetBrains.Annotations;

namespace Colorware.Core.Helpers.BestTargetDotgainEstimator {
    public class BestTargetTviEstimator : IBestTargetTviEstimator {
        [Pure]
        public Option<TviEstimatorResult<T>> Estimate<T>(IEnumerable<TviEstimatorEntry<T>> dotgainEstimatorEntries,
                                                         double targetTvi,
                                                         DotgainMethod dotgainMethod,
                                                         SpectralMeasurementConditions measurementConditions,
                                                         DensityMethod densityMethod,
                                                         WhiteBase whiteBase) {
            var entries = dotgainEstimatorEntries as TviEstimatorEntry<T>[] ?? dotgainEstimatorEntries.ToArray();
            var solid = entries.FirstOrDefault(x => x.Percentage == 100);
            var paper = entries.FirstOrDefault(x => x.Percentage == 0);
            var tints = entries.Where(x => x.Percentage > 0 && x.Percentage < 100 && x.Spectrum != null).ToArray();

            // For debugging: dumpCurves(dotgainEstimatorEntries);

            // Note that FirstOrDefault will return default(StructType) if StructType is a struct. In
            // this case, the spectrum will be null.
            if (solid.Spectrum == null ||
                paper.Spectrum == null ||
                !tints.Any()) return Option<TviEstimatorResult<T>>.Nothing;

            // Create a list of tints that include the dotgain for the tint.
            var finalTints =
                dotgainMethod == DotgainMethod.Spectral
                    ? calculateSpectralToneValue(tints, solid, paper, measurementConditions, densityMethod, whiteBase)
                    : calculateDensityToneValue(tints, solid, paper, densityMethod, whiteBase);

            // Create pairs and find the pair that contains the target dotgain we are looking for.
            var ordered = finalTints
                // Readd paper and solid to complete the curve.
                .Concat(new[] {
                    new FinalEntry<T>(0, paper.Spectrum, 0, paper.Tag),
                    new FinalEntry<T>(100, solid.Spectrum, 100, solid.Tag)
                })
                .OrderBy(x => x.Percentage);

            // Get the pair surrounding the sought after target value.
            var suroundingPair =
                ordered.Zip(ordered.Skip(1), (prev, cur) => Tuple.Create(prev, cur))
                       .FirstOrDefault(x => x.Item1.Tvi <= targetTvi && x.Item2.Tvi >= targetTvi);
            if (suroundingPair == null)
                return Option<TviEstimatorResult<T>>.Nothing;

            var tvi1 = suroundingPair.Item1.Tvi;
            var tvi2 = suroundingPair.Item2.Tvi;
            var ratio = (targetTvi - tvi1) / (tvi2 - tvi1);

            return new TviEstimatorResult<T>(
                new TviEstimatorEntry<T>(suroundingPair.Item1.Percentage, suroundingPair.Item1.Spectrum,
                                         suroundingPair.Item1.Tag),
                suroundingPair.Item1.Tvi,
                new TviEstimatorEntry<T>(suroundingPair.Item2.Percentage, suroundingPair.Item2.Spectrum,
                                         suroundingPair.Item2.Tag),
                suroundingPair.Item2.Tvi,
                ratio,
                targetTvi)
                .ToOption();
        }

        private static void dumpCurves<T>(IEnumerable<TviEstimatorEntry<T>> dotgainEstimatorEntries) {
            var sb = new StringBuilder();
            sb.AppendLine("--------");
            foreach (var e in dotgainEstimatorEntries) {
                sb.AppendLine($"{e.Percentage}:{e.Spectrum.GetStringValue()}");
            }
            sb.AppendLine("--------");
            Debug.WriteLine(sb.ToString());
        }

        private IEnumerable<FinalEntry<T>> calculateSpectralToneValue<T>(IEnumerable<TviEstimatorEntry<T>> tints,
                                                                         TviEstimatorEntry<T> solid,
                                                                         TviEstimatorEntry<T> paper,
                                                                         SpectralMeasurementConditions
                                                                             measurementConditions,
                                                                         DensityMethod densityMethod,
                                                                         WhiteBase whiteBase) {
            var solidLab = solid.Spectrum.ToLab(measurementConditions);
            var paperLab = paper.Spectrum.ToLab(measurementConditions);
            var component =
                whiteBase == WhiteBase.Absolute
                    ? solid.Spectrum.ToDensities(densityMethod).GetFilteredComponent()
                    : solid.Spectrum.ToDensities(densityMethod, paper.Spectrum).GetFilteredComponent();
            return tints.Select(
                tint =>
                new FinalEntry<T>(tint.Percentage,
                                  tint.Spectrum,
                                  // Note, passing a value of 0 will calculate the tone value instead of the dotgain here.
                                  Dotgain.MurrayDaviesSpectral(paperLab,
                                                               solidLab,
                                                               tint.Spectrum.ToLab(measurementConditions),
                                                               0, component, true),
                                  tint.Tag));
        }

        private IEnumerable<FinalEntry<T>> calculateDensityToneValue<T>(IEnumerable<TviEstimatorEntry<T>> tints,
                                                                        TviEstimatorEntry<T> solid,
                                                                        TviEstimatorEntry<T> paper,
                                                                        DensityMethod densityMethod,
                                                                        WhiteBase whiteBase) {
            var solidCmyk =
                whiteBase == WhiteBase.Absolute
                    ? solid.Spectrum.ToDensities(densityMethod)
                    : solid.Spectrum.ToDensities(densityMethod, paper.Spectrum);
            var paperCmyk =
                whiteBase == WhiteBase.Absolute
                    ? paper.Spectrum.ToDensities(densityMethod)
                    : paper.Spectrum.ToDensities(densityMethod, paper.Spectrum);
            return tints.Select(
                tint => {
                    var tintCmyk =
                        whiteBase == WhiteBase.Absolute
                            ? tint.Spectrum.ToDensities(densityMethod)
                            : tint.Spectrum.ToDensities(densityMethod, paper.Spectrum);
                    return new FinalEntry<T>(tint.Percentage,
                                             tint.Spectrum,
                                             // Note, passing a value of 0 will calculate the tone value instead of the dotgain here.
                                             Dotgain.MurrayDaviesDensities(paperCmyk, solidCmyk, tintCmyk, 0),
                                             tint.Tag);
                });
        }

        /// 
        [DebuggerDisplay("FinalEntry<T>({Percentage} => {Tvi})")]
        private struct FinalEntry<T> {
            public FinalEntry(int percentage, Spectrum spectrum, double tvi, T tag) {
                Percentage = percentage;
                Spectrum = spectrum;
                Tvi = tvi;
                Tag = tag;
            }

            public readonly int Percentage;
            public readonly Spectrum Spectrum;
            public readonly double Tvi;
            public readonly T Tag;
        }
    }
}