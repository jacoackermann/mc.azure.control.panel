﻿namespace Colorware.Core.Helpers {
    public static class ApplicationVersionResponseHeader {
        /// <summary>
        /// Name of the response header to send back from a request to the server. This header will
        /// then hold the application version of the server which can be checked by the client and
        /// other callees.
        /// </summary>
        public const string HeaderName = "X-MC-Version";
    }
}