﻿using Colorware.Core.Mvvm;

namespace Colorware.Core.Helpers {
    public class ProgressLoader : DefaultNotifyPropertyChanged, IProgressLoader {
        private readonly object locker = new object();

        #region Properties
        private int count;

        public int Count {
            get { return count; }
            // Private for thread-safety!
            private set {
                count = value;
                OnPropertyChanged("Count");

                OnPropertyChanged("IsNotDone");
                OnPropertyChanged("IsDone");
            }
        }

        private int itemsDone;

        public int ItemsDone {
            get { return itemsDone; }
            // Private for thread-safety!
            private set {
                itemsDone = value;

                OnPropertyChanged("IsNotDone");
                OnPropertyChanged("IsDone");
            }
        }

        public bool IsDone {
            get { return ItemsDone >= Count; }
        }

        public bool IsNotDone {
            get { return !IsDone; }
        }
        #endregion

        #region Methods
        public void Reset() {
            lock (locker) {
                Count = 0;
                ItemsDone = 0;
            }
        }

        public void Begin(int addCount = 1) {
            lock (locker) {
                Count += addCount;
            }
        }

        public void End(int removeCount = 1) {
            lock (locker) {
                ItemsDone += removeCount;
            }
        }
        #endregion

        public override string ToString() {
            return string.Format("ProgressLoader({0}/{1})", ItemsDone, Count);
        }
    }
}