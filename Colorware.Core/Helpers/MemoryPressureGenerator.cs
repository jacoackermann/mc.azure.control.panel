﻿using System;

namespace Colorware.Core.Helpers {
    /// <summary>
    /// Use this to notify the GC we have just reserved some unmanaged memory that
    /// can be reclaimed by collecting garbage and running finalizers.
    /// 
    /// Example scenario:
    /// When using bitmap related classes, a lot of unmanaged memory (e.g. 20MB per bitmap for
    /// a normal sized image) is used. The GC doesn't notice this memory is used because it's 
    /// unmanaged, it only sees the small managed portion the bitmap classes themselves use.
    /// In effect, the GC won't run to reclaim unmanaged memory of already disposed bitmaps even
    /// though  our process' memory usage can already be very high due to unmanaged resources not
    /// being disposed.
    /// 
    /// Note that you may still need to call GC.WaitForPendingFinalizers() when allocating and
    /// de-allocating unmanaged resources very rapidly. (Otherwise the Finalizer thread won't
    /// be able to finish running the Finalizers before you're allocating more memory)
    /// </summary>
    public class MemoryPressureGenerator {
        public static void GeneratePressure(long numBytesPressure) {
            // ReSharper disable once ObjectCreationAsStatement
            new MemoryPressureGenerator(numBytesPressure);
        }

        private readonly long numBytesPressure;

        private MemoryPressureGenerator(long numBytesPressure) {
            this.numBytesPressure = numBytesPressure;

            GC.AddMemoryPressure(numBytesPressure);
        }

        // When the GC runs all finalizers, this finalizer is run as well because
        // nothing is holding onto this instance. The result is that the memory pressure
        // we added is removed automatically after the finalizer has run.
        ~MemoryPressureGenerator() {
            GC.RemoveMemoryPressure(numBytesPressure);
        }
    }
}