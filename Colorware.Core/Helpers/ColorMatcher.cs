﻿using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Color;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.Compare;
using Colorware.Core.Enums;
using Colorware.Core.Extensions;

using JetBrains.Annotations;

namespace Colorware.Core.Helpers {
    public static class ColorMatcher {
        public enum MultiMatchReason {
            /// <summary>
            /// Multiple matches found close together.
            /// </summary>
            TooClose,

            /// <summary>
            /// Closest match was further away than minDeltaEToConsiderWrongMatch.
            /// </summary>
            WrongMatch,
        }

        /// <summary>
        /// Callback used when adding samples and multiple possible reference matches are detected.
        /// In this case the callback will be called to determine which reference the sample should be added to.
        /// </summary>
        /// <param name="possibleMatches">Possible matches for the sample.</param>
        /// <param name="reason">Reason the callback was called.</param>
        public delegate T MultiMatchCallback<T>(
            IReadOnlyList<MultipleMatchItem<T>> possibleMatches, MultiMatchReason reason)
            where T : class, ILabConvertible;

        [CanBeNull]
        public static T MatchSampleSimple<T>(ISample sample, IEnumerable<T> potentialItems, IToleranceSet toleranceSet,
                                             MultiMatchCallback<T> multiMatchCallback, int maxResults)
            where T : class, ILabConvertible {
            if (!potentialItems.Any())
                return null;

            if (potentialItems.HasExactly(1))
                return potentialItems.First();

            // NB: don't parallelize this, takes too much memory for very large collections!
            var bestMatches = potentialItems
                .TakeOrdered(maxResults,
                             item => toleranceSet.DeltaEUsingCurrentMethod(item.AsLab, sample.AsLab));

            return multiMatchCallback(
                bestMatches.Select(item => new MultipleMatchItem<T>(item, sample, toleranceSet)).ToList(),
                MultiMatchReason.TooClose);
        }

        /// <summary>
        /// Matches a sample to a list of patches and returns the patch closest to the sample (Delta-E wise). Calls
        /// the supplied multi-match callback when more than 1 patch is close to the sample.
        /// </summary>
        /// <param name="sample">The sample.</param>
        /// <param name="potentialItems">The potential patches.</param>
        /// <param name="toleranceSet">The tolerance set to use for comparison.</param>
        /// <param name="multiMatchCallback">The multi match callback.</param>
        /// <returns></returns>
        [CanBeNull]
        public static T MatchSample<T>([NotNull] ISample sample,
                                       [NotNull] IEnumerable<T> potentialItems,
                                       [NotNull] IToleranceSet toleranceSet,
                                       [CanBeNull] MultiMatchCallback<T> multiMatchCallback)
            where T : class, ILabConvertible {
            if (sample == null) throw new ArgumentNullException(nameof(sample));
            if (potentialItems == null) throw new ArgumentNullException(nameof(potentialItems));
            if (toleranceSet == null) throw new ArgumentNullException(nameof(toleranceSet));

            if (!potentialItems.Any())
                return null;

            var multiMatchDeltaEScalar =
                Config.Config.DefaultConfig.GetDouble("Options.ColorMatcher.MultiMatchDeltaEScalar",
                                                      1.5);
            var maxWrongMatchDeltaEScalar =
                Config.Config.DefaultConfig.GetDouble("Options.ColorMatcher.MaxWrongMatchDeltaEScalar", 3);

            var deltaETolerance = toleranceSet.DeltaEToleranceFor(ToleranceGroups.Primaries);

            var potentials = potentialItems.Select(item => new MultipleMatchItem<T>(item, sample, toleranceSet))
                                           .OrderBy(item => item.DeltaE)
                                           .ToList();

            // Get closest match.
            var closest = potentials.MinBy(item => item.DeltaE);

            if (multiMatchCallback == null || potentials.Count == 1)
                return closest.MatchItem; // Nothing left to do, just use closest.

            if (closest.DeltaE > deltaETolerance) {
                // Calculate potentials based on expanded deltaE circle based on config value.
                var maxWrongDeltaE = deltaETolerance * maxWrongMatchDeltaEScalar;
                var potentialsInRangeForWrongMatch = potentials.Where(p => p.DeltaE <= maxWrongDeltaE)
                                                               .Take(50) // Limit memory usage
                                                               .ToList();

                if (potentialsInRangeForWrongMatch.Count == 1)
                    // Still only one color in range. Use that one.
                    return potentialsInRangeForWrongMatch.First().MatchItem;

                if (potentialsInRangeForWrongMatch.Count <= 0)
                    // Nothing close enough. Just use whole reference list and show warning.
                    return multiMatchCallback(potentials, MultiMatchReason.WrongMatch);
                else
                // Too far away from anything, user has to select the right color from the list of colors limited
                // by max delta E as calculated from config value.
                    return multiMatchCallback(potentialsInRangeForWrongMatch, MultiMatchReason.TooClose);
            }

            // Calculate distance from closest reference to other references.
            foreach (var potential in potentials)
                potential.CalculateDeltaEToClosestReference(closest);

            // The filter limit to check for other possible matches.
            var deltaELimitForPotentials = deltaETolerance * multiMatchDeltaEScalar;
            var potentialsInRange = potentials.Where(p => p.DeltaEToClosestReference <= deltaELimitForPotentials);

            if (potentialsInRange.Count() <= 1)
                // Only the closest is in range.
                return closest.MatchItem;

            return multiMatchCallback(potentialsInRange.ToList(), MultiMatchReason.TooClose);
        }
    }
}