﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Colorware.Core.Helpers {
    /// <summary>
    /// Helper to check the connection status of the server. Currently used for the login and initial setup.
    /// </summary>
    public class ServerStatusChecker {
        // Represents the current server status.
        public enum ServerStatus {
            /// <summary>
            /// Could connect to the server given by Host.
            /// </summary>
            Connected,

            /// <summary>
            /// Could not connect to the server given by Host.
            /// </summary>
            Disconnected,

            /// <summary>
            /// Currently in the process of trying to connect to the server given by Host.
            /// </summary>
            Trying,
        }

        public async Task<ServerStatus> CheckServerStatus(string url, CancellationToken token) {
            if (url == null) {
                return ServerStatus.Trying;
            }
            try {
                using (var client = new WebClient()) {
                    token.ThrowIfCancellationRequested();
                    token.Register(client.CancelAsync);
                    await client.DownloadStringTaskAsync(new Uri(url));
                    return ServerStatus.Connected;
                }
            }
            catch (Exception) {
                return ServerStatus.Disconnected;
            }
        }
    }
}