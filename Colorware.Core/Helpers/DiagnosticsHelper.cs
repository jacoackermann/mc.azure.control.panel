﻿using System;
using System.Linq;

using Colorware.Core.Plugins.Devices;

using JetBrains.Annotations;

namespace Colorware.Core.Helpers {
    public static class DiagnosticsHelper {
        public static string ServerJiraSummary;

        private static string jiraSummary;

        public static string GetJiraHardwareSummary() {
            if (jiraSummary != null)
                return jiraSummary;

            try {
                var os = SystemInfoHelper.GetOperatingSystem();
                var processors = string.Join("\n", SystemInfoHelper.GetProcessors());
                var ramModules = string.Join("\n", SystemInfoHelper.GetRamModules());
                var disks = string.Join("\n", SystemInfoHelper.GetLogicalDisks());
                var gpus = string.Join("\n", SystemInfoHelper.GetGpus());
                var netInterfaces = string.Join("\n", SystemInfoHelper.GetNetworkInterfaces());

                var memInfo = SystemInfoHelper.GetMemoryInfo();
                var memUsage = ApplicationHelper.GetMemoryUsageInfo();

                var cultureSettings = SystemInfoHelper.GetCultureSettings();
                var timeInfo = SystemInfoHelper.GetTimezoneInfo();

                jiraSummary = string.Format(@"*OS*
{0}

*Hardware*
{1}

{2}

{3}

{4}

{{noformat:nopanel=true}}{5}{{noformat}}

*Memory*
{6}

{7}

*Culture & Time*
{8}

{9}", os, processors, ramModules, disks, gpus, netInterfaces, memInfo, memUsage, cultureSettings, timeInfo);
            }
            catch (Exception) {
                return "?";
            }

            return jiraSummary;
        }

        public static string GetDeviceInfoJira([CanBeNull] IDeviceManager dm) {
            if (dm == null)
                return "?";

            try {
                var devicesInfo = dm.Devices.Select(
                    d =>
                    $"\n{d.Name} (Connected: {d.IsConnected}, Calibrated: {d.IsFullyCalibratedForWhitebase}, Mode: {d.LastSetMeasurementMode}, Whitebase: {d.LastSetDensityWhiteBase}" +
                    $", Illumination: {d.LastSetIlluminant}, Observer: {d.LastSetObserver}, Dens. method: {d.LastSetDensityMethod}," +
                    $" Dens. m-cond: {d.LastSetDensityMCondition}, Spect. m-cond: {d.LastSetSpectralMCondition})");


                return
                    $"*Device manager settings*\nMode: {dm.MeasurementMode}, Whitebase: {dm.DensityWhiteBase}, Illumination: {dm.Illuminant}" +
                    $", Observer: {dm.Observer}, Dens. method: {dm.DensityMethod}, Dens. m-cond: {dm.DensityMCondition}," +
                    $" Spect. m-cond: {dm.SpectralMCondition})\n\n*Devices*\n{string.Concat(devicesInfo)}";
            }
            catch (Exception) {}

            return "?";
        }
    }
}