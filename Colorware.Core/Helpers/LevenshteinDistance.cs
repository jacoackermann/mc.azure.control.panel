﻿namespace Colorware.Core.Helpers {
    public static class LevenshteinDistance {
        /// <summary>
        /// Calculate the Levenshtein Distance between two strings.
        /// </summary>
        /// <param name="a">First string.</param>
        /// <param name="b">Second string.</param>
        /// <returns>
        /// The Levenshtein Distance between string <see cref="a"/> and string <see cref="b"/>.
        /// </returns>
        /// <remarks>Implementation from https://stackoverflow.com/questions/9453731/how-to-calculate-distance-similarity-measure-of-given-2-strings</remarks>
        public static int Calculate(string a, string b) {
            if (string.IsNullOrEmpty(a) || string.IsNullOrEmpty(b)) return 0;

            var lengthA = a.Length;
            var lengthB = b.Length;
            var distances = new int[lengthA + 1, lengthB + 1];
            for (var i = 0; i <= lengthA; distances[i, 0] = i++) { }
            for (var j = 0; j <= lengthB; distances[0, j] = j++) { }

            for (var i = 1; i <= lengthA; i++)
            for (var j = 1; j <= lengthB; j++) {
                var cost = b[j - 1] == a[i - 1] ? 0 : 1;
                distances[i, j] = System.Math.Min(
                    System.Math.Min(distances[i - 1, j] + 1, distances[i, j - 1] + 1),
                    distances[i - 1, j - 1] + cost);
            }
            return distances[lengthA, lengthB];
        }
    }
}