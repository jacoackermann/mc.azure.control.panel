﻿using System;
using System.Globalization;

namespace Colorware.Core.Helpers {
    public static class Comparisons {
        public static int CmykOrderComparison(char c1, char c2) {
            var s1 = c1.ToString(CultureInfo.InvariantCulture);
            var s2 = c2.ToString(CultureInfo.InvariantCulture);

            if (s1.Equals("K", StringComparison.InvariantCultureIgnoreCase))
                return s2.Equals("K", StringComparison.InvariantCultureIgnoreCase) ? 0 : 1;

            if (s2.Equals("K", StringComparison.InvariantCultureIgnoreCase))
                return -1;

            return String.Compare(s1, s2, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}