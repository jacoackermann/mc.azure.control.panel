using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Data.Models;

namespace Colorware.Core.Helpers {
    public static class InkZonePadder {
        public static List<T> PadInkZones<T>(IEnumerable<T> currentItems, int endInkZone, Func<int, T> generateUndefined)
            where T : IInkZone {
            var result = new List<T>();
            var items = currentItems as IList<T> ?? currentItems.ToList();
            if (!items.Any()) {
                return result;
            }
            var firstItem = items.First();
            var lastItem = items.Last();
            if (firstItem.InkZone > lastItem.InkZone)
                padInvertedZones(items, endInkZone, generateUndefined, result);
            else
                padRegularZones(items, endInkZone, generateUndefined, result);
            return result;
        }

        private static void padRegularZones<T>(IEnumerable<T> currentItems, int endInkZone,
            Func<int, T> generateUndefined, List<T> result) where T : IInkZone {
            var current = 1;
            foreach (var item in currentItems) {
                while (current < item.InkZone) {
                    var undefined = generateUndefined(current);
                    result.Add(undefined);
                    current++;
                }
                result.Add(item);
                current = item.InkZone + 1;
            }
            while (current <= endInkZone) {
                result.Add(generateUndefined(current));
                current++;
            }
        }

        private static void padInvertedZones<T>(IEnumerable<T> currentItems, int endInkZone,
            Func<int, T> generateUndefined, List<T> result) where T : IInkZone {
            var current = endInkZone;
            foreach (var item in currentItems) {
                while (current > item.InkZone) {
                    var undefined = generateUndefined(current);
                    result.Add(undefined);
                    current--;
                }
                result.Add(item);
                current = item.InkZone - 1;
            }
            while (current > 0) {
                result.Add(generateUndefined(current));
                current--;
            }
        }
    }
}