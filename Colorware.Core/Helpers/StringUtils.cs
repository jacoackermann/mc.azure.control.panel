﻿using System;

using Colorware.Core.Extensions;

using JetBrains.Annotations;

namespace Colorware.Core.Helpers {
    public static class StringUtils {
        [NotNull]
        public static string ParseEscapeCharacters([NotNull] string str) {
            if (str == null) throw new ArgumentNullException(nameof(str));

            // Splitting necessary to allow literal '\'
            var split = str.Split(@"\\");

            for (var i = 0; i < split.Length; i++)
                split[i] = split[i]
                    .Replace(@"\0", "\0")
                    .Replace(@"\a", "\a")
                    .Replace(@"\b", "\b")
                    .Replace(@"\f", "\f")
                    .Replace(@"\n", "\n")
                    .Replace(@"\r", "\r")
                    .Replace(@"\t", "\t")
                    .Replace(@"\v", "\v");

            return string.Join(@"\", split);
        }
    }
}