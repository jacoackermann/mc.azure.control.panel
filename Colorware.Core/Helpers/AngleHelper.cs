﻿namespace Colorware.Core.Helpers {
    public static class AngleHelper {
        public static double Rad2Deg(double rad) {
            return (180.0 / System.Math.PI) * rad;
        }

        public static double Deg2Rad(double deg) {
            return (System.Math.PI / 180.0) * deg;
        }

        public static double MinimizeAngle(double angle) {
            while (angle < -180)
                angle += 360;
            while (angle > 180)
                angle -= 360;
            return angle;
        }

        public static double AngleDifferenceDegrees(double deg1, double deg2) {
            var difference = deg2 - deg1;
            return MinimizeAngle(difference);
        }

        public static double DotProduct(double x1, double y1, double z1, double x2, double y2, double z2) {
            return x1 * x2 + y1 * y2 + z1 * z2;
        }

        public static double VectorLength(double x, double y, double z) {
            return System.Math.Sqrt(x * x + y * y + z * z);
        }

        /// <summary>
        /// Calculate the angle between two vectors.
        /// </summary>
        /// <param name="x1">The x1.</param>
        /// <param name="y1">The y1.</param>
        /// <param name="z1">The z1.</param>
        /// <param name="x2">The x2.</param>
        /// <param name="y2">The y2.</param>
        /// <param name="z2">The z2.</param>
        /// <returns>Angle of vectors in radians.</returns>
        public static double AngleBetweenVectors(double x1, double y1, double z1, double x2, double y2, double z2) {
            var dot = DotProduct(x1, y1, z1, x2, y2, z2);
            var l1 = VectorLength(x1, y1, z1);
            var l2 = VectorLength(x2, y2, z2);
            return System.Math.Acos(dot / (l1 * l2));
        }

        public static double AngleBetweenVectorsInDegrees(double x1, double y1, double z1, double x2, double y2,
            double z2) {
            return Rad2Deg(AngleBetweenVectors(x1, y1, z1, x2, y2, z2));
        }
    }
}