﻿using Colorware.Core.Extensions;
using Colorware.Core.Logging;

using JetBrains.Annotations;

namespace Colorware.Core.Helpers {
    public static class BarcodeUtils {
        public static string RawStartDelimiter {
            get { return Config.Config.DefaultConfig.Get("Options.Barcode.StartDelimiter", ""); }
        }

        public static string RawEndDelimiter {
            get { return Config.Config.DefaultConfig.Get("Options.Barcode.EndDelimiter", ""); }
        }

        public static string StartDelimiter {
            get { return StringUtils.ParseEscapeCharacters(RawStartDelimiter); }
        }

        public static string EndDelimiter {
            get { return StringUtils.ParseEscapeCharacters(RawEndDelimiter); }
        }

        public static string StripDelimiters([NotNull] string barcode) {
            string result = barcode;

            if (!barcode.StartsWith(StartDelimiter))
                LogManager.GetLogger(typeof(BarcodeUtils)).Warn(
                    "Barcode delimiter stripping: Barcode '{0}' does not start with specified start delimiter '{1}'!");
            else
                result = result.Remove(0, StartDelimiter.Length);

            if (!barcode.EndsWith(EndDelimiter))
                LogManager.GetLogger(typeof(BarcodeUtils)).Warn(
                    "Barcode delimiter stripping: Barcode '{0}' does not end with specified end delimiter '{1}'!");
            else
                result = result.Truncate(result.Length - EndDelimiter.Length);

            return result;
        }


        /// <summary>
        /// Determines whether the specified string is a barcode.
        /// </summary>
        /// <param name="possibleBarcode">String that is possibly a barcode.</param>
        /// <returns>
        ///   <c>true</c> if the specified possible barcode is a barcode; otherwise, <c>false</c>.
        /// </returns>
        /// <remarks>Note that this doesn't test the actual string format, but only checks if the string begins
        /// and ends with the specified delimiters (<c>StartDelimiter</c> and <c>EndDelimiter</c>.</remarks>
        public static bool IsBarcode([NotNull] string possibleBarcode) {
            if (StartDelimiter == "" && EndDelimiter == "")
                return false;

            return possibleBarcode.StartsWith(StartDelimiter) && possibleBarcode.EndsWith(EndDelimiter);
        }
    }
}