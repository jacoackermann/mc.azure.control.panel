﻿using JetBrains.Annotations;

namespace Colorware.Core.Color {
    public interface IHasDensities : IHasDensity {
        [NotNull]
        CMYK Densities { get; }
    }
}