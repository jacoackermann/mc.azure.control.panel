﻿namespace Colorware.Core.Color {
    public interface IHasDensity {
        double Density { get; }
    }
}