﻿using System;
using System.ComponentModel;

using Colorware.Core.Helpers;

using JetBrains.Annotations;

namespace Colorware.Core.Color {
    public class Lch : IComparable, INotifyPropertyChanged {
        /// <summary>
        /// When sorting, colors are considered to belong to the same 'color group' as long as their
        /// Hues are within this range from eachother. This provides a more natural order than
        /// purely sorting by Lch.
        /// </summary>
        private const int SortingHueTolerance = 10;

        public double L { get; }

        // ReSharper disable once InconsistentNaming
        public double c { get; }

        // ReSharper disable once InconsistentNaming
        public double h { get; }

        public SpectralMeasurementConditions MeasurementConditions { get; }

        public Lch(double l, double c, double h, [NotNull] SpectralMeasurementConditions measurementConditions) {
            if (measurementConditions == null) throw new ArgumentNullException(nameof(measurementConditions));

            L = l;
            this.c = c;
            this.h = h;
            MeasurementConditions = measurementConditions;
        }

        private Lab asLab;

        public Lab AsLab => asLab ?? (asLab = toLab());

        private Lab toLab() {
            var newA = c * Math.Cos(AngleHelper.Deg2Rad(h));
            var newB = c * Math.Sin(AngleHelper.Deg2Rad(h));
            return new Lab(L, newA, newB, MeasurementConditions);
        }

        #region Overrides of Object
        public override string ToString() {
            return FormattableString.Invariant($"Lch({L}, {c}, {h})");
        }

        // NB: this comparison groups similar colors together, it's not useful in the mathematical sense
        public int CompareTo(object obj) {
            Lch other = obj as Lch;

            // Sort by Hue, then Luminosity, then Chroma
            if (Math.Abs(h - other.h) < SortingHueTolerance) {
                if (L == other.L)
                    return c.CompareTo(other.c);

                return L.CompareTo(other.L);
            }

            return h.CompareTo(other.h);
        }
        #endregion

        // Anti memory leak
#pragma warning disable 0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 0067
    }
}