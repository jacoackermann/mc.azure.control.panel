﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

using Colorware.Core.Color.Support;
using Colorware.Core.Enums;
using Colorware.Core.Extensions;
using Colorware.Core.Helpers;

using JetBrains.Annotations;

namespace Colorware.Core.Color {
    public class Lab : IEquatable<Lab>, INotifyPropertyChanged {
        #region Cie94 Defaults
        public const double Cie94DefaultForKl = 1.0; // Default (2.0 for textiles).
        public const double Cie94DefaultForKc = 1.0; // Default (2.0 for textiles).
        public const double Cie94DefaultForKh = 1.0; // Default (2.0 for textiles).
        public const double Cie94DefaultForK1 = 0.045; // Graphics arts defaults (0.048 for textiles).
        public const double Cie94DefaultForK2 = 0.015; // Grapics arts defaults (0.014 for textiles).

        public const double CieCmcDefaultForL = 2.0;
        public const double CieCmcDefaultForC = 1.0;
        #endregion

        #region Cie2000 Defaults
        public const double Cie2000DefaultForKl = 1.0;
        public const double Cie2000DefaultForKc = 1.0;
        public const double Cie2000DefaultForKh = 1.0;
        #endregion

        public double L { get; }

        // ReSharper disable once InconsistentNaming
        public double a { get; }

        // ReSharper disable once InconsistentNaming
        public double b { get; }

        public SpectralMeasurementConditions MeasurementConditions { get; }

        public double Angle {
            get {
                if (a.NearlyEquals(0))
                    return 0;

                var angle = Math.Atan(b / a);
                if (a > 0) {
                    if (b < 0) // 4th quadrant.
                        angle = 2 * Math.PI + angle;
                    // else { 1st quadrant, do nothing. }
                }
                else {
                    if (b < 0) // 3th quadrant.
                        angle = Math.PI + angle;
                    else // 2nd quadrant.
                        angle = Math.PI + angle;
                }
                return angle;
            }
        }

        public Lab(double l, double a, double b, [NotNull] SpectralMeasurementConditions measurementConditions) {
            if (measurementConditions == null) throw new ArgumentNullException(nameof(measurementConditions));

            L = l;
            this.a = a;
            this.b = b;
            MeasurementConditions = measurementConditions;
        }

        public Lab([NotNull] LabTuple labTuple, [NotNull] SpectralMeasurementConditions mc)
            : this(labTuple.L, labTuple.a, labTuple.b, mc) {
            if (labTuple == null) throw new ArgumentNullException(nameof(labTuple));
        }

        [NotNull, Pure]
        private XYZ toXYZ() {
            if (MeasurementConditions.Illuminant == Illuminant.Unknown ||
                MeasurementConditions.Observer == Observer.Unknown)
                throw new Exception("Can't convert Lab to XYZ because the illuminant and/or observer are unknown!");

            var tristimulusReference =
                TriStimulusReferenceData.GetTristimulusReferenceDataFor(MeasurementConditions.Illuminant,
                                                                        MeasurementConditions.Observer);

            var varY = (L + 16.0) / 116.0;
            var varX = a / 500.0 + varY;
            var varZ = varY - b / 200.0;

            // TODO: Optimize pow(var_., 3), currently it's calculated twice (though the compiler might take care of it).
            if (Math.Pow(varY, 3) > 0.008856) varY = Math.Pow(varY, 3);
            else varY = (varY - 16.0 / 116.0) / 7.787;
            if (Math.Pow(varX, 3) > 0.008856) varX = Math.Pow(varX, 3);
            else varX = (varX - 16.0 / 116.0) / 7.787;
            if (Math.Pow(varZ, 3) > 0.008856) varZ = Math.Pow(varZ, 3);
            else varZ = (varZ - 16.0 / 116.0) / 7.787;

            var refX = tristimulusReference.XRef;
            var refY = tristimulusReference.YRef;
            var refZ = tristimulusReference.ZRef;

            var x = refX * varX; //ref_x =  95.047  Observer= 2°, Illuminant= D65
            var y = refY * varY; //ref_y = 100.000
            var z = refZ * varZ; //ref_z = 108.883
            return new XYZ(x, y, z, MeasurementConditions);
        }

        [NotNull, Pure]
        private Lch toLch() {
            var varH = AngleHelper.Rad2Deg(Math.Atan2(b, a)); // atan2(b, a);

            if (varH < 0.0) varH += 360;
            else if (varH > 360) varH -= 360;
            //if ( var_H > 0.0 ) var_H = ( var_H / PI ) * 180.0;
            //else               var_H = 360.0 - ( fabs( var_H ) / PI ) * 180.0;

            var cieL = L;
            var cieC = Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
            var cieH = varH;
            return new Lch(cieL, cieC, cieH, MeasurementConditions);
        }

        #region Delta calculations
        /// <summary>
        /// Implements DeltaE CIE 76.
        /// </summary>
        /// <param name="lab1">The first Lab color.</param>
        /// <param name="lab2">The Second Lab color.</param>
        /// <returns></returns>
        public static double DeltaE76([NotNull] Lab lab1, [NotNull] Lab lab2) {
            if (lab1 == null) throw new ArgumentNullException(nameof(lab1));
            if (lab2 == null) throw new ArgumentNullException(nameof(lab2));

            lab1.MeasurementConditions.EnsureSpectrallyCompatibleWith(lab2.MeasurementConditions);

            var ldif = lab1.L - lab2.L;
            var adif = lab1.a - lab2.a;
            var bdif = lab1.b - lab2.b;
            return Math.Sqrt(ldif * ldif + adif * adif + bdif * bdif);
        }

        /// <summary>
        /// Implements DeltaE CMC.
        /// </summary>
        /// <param name="lab1">The first Lab color.</param>
        /// <param name="lab2">The second Lab color.</param>
        /// <param name="l">The l value (typically 2).</param>
        /// <param name="c">The c value (typically 1).</param>
        /// <returns></returns>
        public static double DeltaECMC([NotNull] Lab lab1, [NotNull] Lab lab2, double l, double c) {
            if (lab1 == null) throw new ArgumentNullException(nameof(lab1));
            if (lab2 == null) throw new ArgumentNullException(nameof(lab2));

            lab1.MeasurementConditions.EnsureSpectrallyCompatibleWith(lab2.MeasurementConditions);

            if (l == 0)
                throw new ArgumentOutOfRangeException(nameof(l), "'l' parameter of CMC delta-E method must not be 0!");

            if (c == 0)
                throw new ArgumentOutOfRangeException(nameof(c), "'c' parameter of CMC delta-E method must not be 0!");

            var c1 = Math.Sqrt(lab1.a * lab1.a + lab1.b * lab1.b);
            var c2 = Math.Sqrt(lab2.a * lab2.a + lab2.b * lab2.b);
            var deltaC = c1 - c2;

            var deltaL = lab1.L - lab2.L;
            var deltaA = lab1.a - lab2.a;
            var deltaB = lab1.b - lab2.b;

            var deltaHPart = deltaA * deltaA + deltaB * deltaB - deltaC * deltaC;
            if (deltaHPart < 0) deltaHPart = 0;
            var deltaH = Math.Sqrt(deltaHPart);

            var h1 = AngleHelper.Rad2Deg(Math.Atan2(lab1.b, lab1.a));
            if (h1 < 0.0) h1 += 360.0;
            if (h1 >= 360.0) h1 -= 360.0;

            var f = Math.Sqrt(Math.Pow(c1, 4) / (Math.Pow(c1, 4) + 1900.0));
            double T;
            if (164.0 <= h1 && h1 <= 345.0) {
                T = 0.56 + Math.Abs(0.2 * Math.Cos(AngleHelper.Deg2Rad(h1 + 168.0)));
            }
            else {
                T = 0.36 + Math.Abs(0.4 * Math.Cos(AngleHelper.Deg2Rad(h1 + 35.0)));
            }


            double sL;
            if (lab1.L < 16.0) {
                sL = 0.511;
            }
            else {
                sL = (0.040975 * lab1.L) / (1.0 + 0.01765 * lab1.L);
            }
            var sC = ((0.0638 * c1) / (1.0 + 0.0131 * c1)) + 0.638;
            var sH = sC * (f * T + 1.0 - f);


            var part1 = deltaL / (l * sL);
            var part2 = deltaC / (c * sC);
            var part3 = deltaH / (sH);

            var result = Math.Sqrt(part1 * part1 + part2 * part2 + part3 * part3);
            /*
            if (double.IsNaN(result)) {
                var msg = String.Format("deltaA: {0}, deltaB: {1}, deltaC: {2}, for c1: {3}, for c2: {4}", deltaA, deltaB, deltaC, (lab1.a * lab1.a + lab1.b * lab1.b), (lab2.a * lab2.a + lab2.b * lab2.b));
                throw new Exception(msg);
            }
            */
            return result;
        }

        public static double DeltaE94(Lab lab1, Lab lab2) {
            return DeltaE94(lab1, lab2,
                            Cie94DefaultForKl, Cie94DefaultForKc, Cie94DefaultForKh,
                            Cie94DefaultForK1,
                            Cie94DefaultForK2
                );
        }

        public static double DeltaE94([NotNull] Lab lab1, [NotNull] Lab lab2, double K_l, double K_c, double K_h,
                                      double K_1, double K_2) {
            if (lab1 == null) throw new ArgumentNullException(nameof(lab1));
            if (lab2 == null) throw new ArgumentNullException(nameof(lab2));

            lab1.MeasurementConditions.EnsureSpectrallyCompatibleWith(lab2.MeasurementConditions);

            double C_1 = Math.Sqrt(lab1.a * lab1.a + lab1.b * lab1.b);
            double C_2 = Math.Sqrt(lab2.a * lab2.a + lab2.b * lab2.b);

            double delta_L = lab1.L - lab2.L;
            double delta_a = lab1.a - lab2.a;
            double delta_b = lab1.b - lab2.b;

            double delta_C = C_1 - C_2;
            double delta_H = Math.Sqrt(delta_a * delta_a + delta_b * delta_b - delta_C * delta_C);

            /*
            double K_1 = 0.045; // => Graphics arts; TODO: 0.048 for textiles.
            double K_2 = 0.015; // => Graphics arts; TODO: 0.014 for textiles.
            double K_l = 1.0;   // => Defaults; TODO: 2.0 for textiles.
            double K_c = 1.0;
            double K_h = 1.0;
            */

            double S_l = 1.0;
            double S_c = 1 + K_1 * C_1;
            double S_h = 1 + K_2 * C_1;

            double part_1 = delta_L / (K_l * S_l);
            double part_2 = delta_C / (K_c * S_c);
            double part_3 = delta_H / (K_h * S_h);

            return Math.Sqrt(part_1 * part_1 + part_2 * part_2 + part_3 * part_3);
        }

        public static double DeltaE2000([NotNull] Lab lab1, [NotNull] Lab lab2) {
            if (lab1 == null) throw new ArgumentNullException(nameof(lab1));
            if (lab2 == null) throw new ArgumentNullException(nameof(lab2));

            return DeltaE2000(lab1, lab2, Cie2000DefaultForKl, Cie2000DefaultForKc, Cie2000DefaultForKh);
        }

        public static double DeltaE2000([NotNull] Lab lab1, [NotNull] Lab lab2, double K_l, double K_c, double K_h) {
            if (lab1 == null) throw new ArgumentNullException(nameof(lab1));
            if (lab2 == null) throw new ArgumentNullException(nameof(lab2));

            lab1.MeasurementConditions.EnsureSpectrallyCompatibleWith(lab2.MeasurementConditions);

            double L_acc_over = (lab1.L + lab2.L) / 2.0;
            double C_1 = Math.Sqrt(lab1.a * lab1.a + lab1.b * lab1.b);
            double C_2 = Math.Sqrt(lab2.a * lab2.a + lab2.b * lab2.b);
            double C_over = (C_1 + C_2) / 2.0;
            double C_over_to_the_power_7 = Math.Pow(C_over, 7.0);
            double G = (1.0 - Math.Sqrt(C_over_to_the_power_7 / (C_over_to_the_power_7 + Math.Pow(25.0, 7.0)))) / 2.0;
            double a_1_acc = lab1.a * (1.0 + G);
            double a_2_acc = lab2.a * (1.0 + G);
            double C_1_acc = Math.Sqrt(a_1_acc * a_1_acc + lab1.b * lab1.b);
            double C_2_acc = Math.Sqrt(a_2_acc * a_2_acc + lab2.b * lab2.b);
            double C_acc_over = (C_1_acc + C_2_acc) / 2.0;

            double h_1_acc = AngleHelper.Rad2Deg(Math.Atan2(lab1.b, a_1_acc)); // => atan(lab1.b / a_1_acc)
            if (h_1_acc < 0)
                h_1_acc += 360.0;

            double h_2_acc = AngleHelper.Rad2Deg(Math.Atan2(lab2.b, a_2_acc)); // => atan(lab2.b / a_2_acc)
            if (h_2_acc < 0)
                h_2_acc += 360.0;

            double h_acc_diff = Math.Abs(h_1_acc - h_2_acc);
            double H_acc_over = 0.0;
            if (h_acc_diff > 180.0)
                H_acc_over = (h_1_acc + h_2_acc + 360.0) / 2.0;
            else
                H_acc_over = (h_1_acc + h_2_acc) / 2.0;


            double T = 1.0 -
                       0.17 * Math.Cos(AngleHelper.Deg2Rad(H_acc_over - 30.0)) +
                       0.24 * Math.Cos(AngleHelper.Deg2Rad(2.0 * H_acc_over)) +
                       0.32 * Math.Cos(AngleHelper.Deg2Rad(3.0 * H_acc_over + 6.0)) -
                       0.20 * Math.Cos(AngleHelper.Deg2Rad(4.0 * H_acc_over - 63.0));

            double delta_h_acc = 0.0;
            if (h_acc_diff <= 180.0) {
                delta_h_acc = h_2_acc - h_1_acc;
            }
            else if (h_2_acc <= h_1_acc) {
                delta_h_acc = h_2_acc - h_1_acc + 360.0;
            }
            else {
                delta_h_acc = h_2_acc - h_1_acc - 360.0;
            }

            double delta_L_acc = lab2.L - lab1.L;
            double delta_C_acc = C_2_acc - C_1_acc;

            double delta_H_acc = 2.0 * Math.Sqrt(C_1_acc * C_2_acc) * Math.Sin(AngleHelper.Deg2Rad(delta_h_acc / 2.0));

            double L_acc_over__minus__50__squared = Math.Pow(L_acc_over - 50.0, 2.0);
            double S_l = 1.0 +
                         ((0.015 * L_acc_over__minus__50__squared) / Math.Sqrt(20.0 + L_acc_over__minus__50__squared));
            double S_c = 1.0 + 0.045 * C_acc_over;
            double S_h = 1.0 + 0.015 * C_acc_over * T;

            double delta_theta = 30.0 * Math.Exp(-Math.Pow((H_acc_over - 275.0) / 25.0, 2));

            double C_acc_over_to_the_power_7 = Math.Pow(C_acc_over, 7.0);
            double R_c = Math.Sqrt(C_acc_over_to_the_power_7 / (C_acc_over_to_the_power_7 + Math.Pow(25.0, 7.0)));
            double R_t = -2.0 * R_c * Math.Sin(AngleHelper.Deg2Rad(2.0 * delta_theta));

            double part_1 = delta_L_acc / (K_l * S_l);
            double part_2 = delta_C_acc / (K_c * S_c);
            double part_3 = delta_H_acc / (K_h * S_h);
            double part_4 = R_t * part_2 * part_3;

            return Math.Sqrt(part_1 * part_1 + part_2 * part_2 + part_3 * part_3 + part_4);
        }

        public static double DeltaH([NotNull] Lab lab1, [NotNull] Lab lab2) {
            if (lab1 == null) throw new ArgumentNullException(nameof(lab1));
            if (lab2 == null) throw new ArgumentNullException(nameof(lab2));

            lab1.MeasurementConditions.EnsureSpectrallyCompatibleWith(lab2.MeasurementConditions);

            var c1 = Math.Sqrt(lab1.a * lab1.a + lab1.b * lab1.b);
            var c2 = Math.Sqrt(lab2.a * lab2.a + lab2.b * lab2.b);

            // double delta_L = lab1.l - lab2.l;
            var deltaA = lab1.a - lab2.a;
            var deltaB = lab1.b - lab2.b;

            var deltaC = c1 - c2;
            var deltaH = Math.Sqrt(deltaA * deltaA + deltaB * deltaB - deltaC * deltaC);
            if (double.IsNaN(deltaH)) // Might happen because of rounding in the previous line.
                return 0;
            return deltaH;
        }

        public static double ChromaPlus([NotNull] Lab reference, [NotNull] Lab sample) {
            if (reference == null) throw new ArgumentNullException(nameof(reference));
            if (sample == null) throw new ArgumentNullException(nameof(sample));

            reference.MeasurementConditions.EnsureSpectrallyCompatibleWith(sample.MeasurementConditions);

            var referenceLch = reference.AsLch;
            var sampleLch = sample.AsLch;

            var factorL = 1.0f + (100.0f - reference.L) / 30.0f;
            var factorC = 1.0f + (referenceLch.c / 100.0f);

            var lDiff = reference.L - sample.L;
            var cDiff = (sampleLch.c - referenceLch.c);

            var result = (lDiff * factorL + cDiff * factorC) / 2.0;
            result *= (2.5 * 0.4);
            return result;
        }

        public double DeltaE76([NotNull] Lab other) {
            return DeltaE76(this, other);
        }

        public double DeltaE94([NotNull] Lab other, double K_l, double K_c, double K_h, double K_1, double K_2) {
            return DeltaE94(this, other, K_l, K_c, K_h, K_1, K_2);
        }

        public double DeltaE94([NotNull] Lab other) {
            return DeltaE94(this, other);
        }

        public double DeltaE2000([NotNull] Lab other) {
            return DeltaE2000(this, other);
        }

        public double DeltaE2000([NotNull] Lab other, double K_l, double K_c, double K_h) {
            return DeltaE2000(this, other, K_l, K_c, K_h);
        }

        public double DeltaECMC([NotNull] Lab other, double l, double c) {
            return DeltaECMC(this, other, l, c);
        }

        public double DeltaH([NotNull] Lab other) {
            return DeltaH(this, other);
        }

        public double ChromaPlus([NotNull] Lab other) {
            return ChromaPlus(this, other);
        }

        [NotNull]
        public Lab RelativeOffsetOf([NotNull] Lab other) {
            if (other == null) throw new ArgumentNullException(nameof(other));

            MeasurementConditions.EnsureSpectrallyCompatibleWith(other.MeasurementConditions);

            return new Lab(L - other.L, a - other.a, b - other.b, MeasurementConditions);
        }

        /// <summary>
        /// Calculate distance in a-b plane.
        /// </summary>
        /// <param name="other"></param>
        /// are compatible and throw and exception otherwise.</param>
        /// <returns></returns>
        public double Distance2D([NotNull] Lab other) {
            if (other == null) throw new ArgumentNullException(nameof(other));

            MeasurementConditions.EnsureSpectrallyCompatibleWith(other.MeasurementConditions);

            var dA = a - other.a;
            var dB = b - other.b;
            return Math.Sqrt(dA * dA + dB * dB);
        }
        #endregion

        [NotNull]
        public RGB AsRGB => AsXYZ.AsRGB;

        [CanBeNull]
        private Lch asLch;

        [NotNull]
        public Lch AsLch => asLch ?? (asLch = toLch());

        [CanBeNull]
        private XYZ asXYZ;

        [NotNull]
        public XYZ AsXYZ => asXYZ ?? (asXYZ = toXYZ());

        [NotNull]
        public LabTuple Tuple => new LabTuple(L, a, b);

        public string AsString => $"{L}, {a}, {b}";

        public System.Windows.Media.Color AsColor => AsXYZ.AsRGB.AsColor;

        public double[] ToCMYKPercentages() {
            return AsRGB.ToCMYKPercentages();
        }

        public static Lab operator +([NotNull] Lab first, [NotNull] Lab second) {
            first.MeasurementConditions.EnsureSpectrallyCompatibleWith(second.MeasurementConditions);

            return new Lab(first.L + second.L, first.a + second.a, first.b + second.b, first.MeasurementConditions);
        }

        public static Lab operator -([NotNull] Lab first, [NotNull] Lab second) {
            first.MeasurementConditions.EnsureSpectrallyCompatibleWith(second.MeasurementConditions);

            return new Lab(first.L - second.L, first.a - second.a, first.b - second.b, first.MeasurementConditions);
        }

        public static Lab operator /([NotNull] Lab first, [NotNull] Lab second) {
            first.MeasurementConditions.EnsureSpectrallyCompatibleWith(second.MeasurementConditions);

            return new Lab(first.L / second.L, first.a / second.a, first.b / second.b, first.MeasurementConditions);
        }

        public static Lab operator /([NotNull] Lab first, double scalar) {
            return new Lab(first.L / scalar, first.a / scalar, first.b / scalar, first.MeasurementConditions);
        }

        public static Lab operator *([NotNull] Lab first, [NotNull] Lab second) {
            first.MeasurementConditions.EnsureSpectrallyCompatibleWith(second.MeasurementConditions);

            return new Lab(first.L * second.L, first.a * second.a, first.b * second.b, first.MeasurementConditions);
        }

        public static Lab operator *([NotNull] Lab first, double scalar) {
            return new Lab(first.L * scalar, first.a * scalar, first.b * scalar, first.MeasurementConditions);
        }

        #region Overrides of Object
        public override string ToString() {
            return FormattableString.Invariant($"Lab({L}, {a}, {b})");
        }
        #endregion

        /// <summary>
        /// Scale on chroma value by converting to Lch, scaling and converting back to Lab.
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        [Pure]
        public Lab ScaleOnChroma(double d) {
            return new Lch(AsLch.L, AsLch.c * d, AsLch.h, MeasurementConditions).AsLab;
        }

        /// <summary>
        /// Scale towards a perceived (non real!) white point.
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        [Pure]
        public Lab ScaleTowardsWhite(double d) {
            return new Lab(100 - (100 - L) * d, a * d, b * d, MeasurementConditions);
        }

        public bool Equals(Lab other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return L.NearlyEquals(other.L) &&
                   a.NearlyEquals(other.a) &&
                   b.NearlyEquals(other.b) &&
                   MeasurementConditions.SpectrallyCompatibleWith(other.MeasurementConditions);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Lab)obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = L.GetHashCode();
                hashCode = (hashCode * 397) ^ a.GetHashCode();
                hashCode = (hashCode * 397) ^ b.GetHashCode();
                hashCode = (hashCode * 397) ^ (MeasurementConditions?.GetHashCode() ?? 0);
                return hashCode;
            }
        }

        public static bool operator ==(Lab left, Lab right) {
            return Equals(left, right);
        }

        public static bool operator !=(Lab left, Lab right) {
            return !Equals(left, right);
        }

        // Anti memory leak
#pragma warning disable 0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 0067
    }

    public static class LabExtensions {
        public static Lab Sum([NotNull] this IEnumerable<Lab> source) {
            if (source == null) throw new ArgumentNullException(nameof(source));

            return source.Aggregate((x, y) => x + y);
        }

        public static Lab Sum<T>([NotNull] this IEnumerable<T> source, [NotNull] Func<T, Lab> selector) {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (selector == null) throw new ArgumentNullException(nameof(selector));

            return source.Select(selector).Aggregate((x, y) => x + y);
        }
    }
}