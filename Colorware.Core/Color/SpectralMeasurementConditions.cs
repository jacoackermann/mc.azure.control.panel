﻿using System;

using Colorware.Core.Enums;

using JetBrains.Annotations;

namespace Colorware.Core.Color {
    /// <summary>
    /// This is enough for a color representation in MC, we don't require the density M-condition (which is available
    /// in MeasurementConditions). The density M-condition is only stored in MeasurementConditionsConfig and used when
    /// measuring, but it's discarded when the measurement is stored.
    /// </summary>
    public class SpectralMeasurementConditions : IEquatable<SpectralMeasurementConditions> {
        public static readonly SpectralMeasurementConditions Undefined =
            new SpectralMeasurementConditions(Illuminant.Unknown, Observer.Unknown, MCondition.M0);

        public SpectralMeasurementConditions(Illuminant illuminant, Observer observer, MCondition spectralMCondition) {
            Illuminant = illuminant;
            Observer = observer;
            SpectralMCondition = spectralMCondition;
        }

        public Illuminant Illuminant { get; }
        public Observer Observer { get; }
        public MCondition SpectralMCondition { get; }

        /// <summary>
        /// "Spectrally compatible" means: comparing Spectra/XYZ/Lab of these two makes sense.
        /// 
        /// Literally: illuminant, observer and spectral m-condition are the same.
        /// </summary>
        public void EnsureSpectrallyCompatibleWith([NotNull] SpectralMeasurementConditions other) {
            if (other == null) throw new ArgumentNullException(nameof(other));

            if (!SpectrallyCompatibleWith(other))
                throw new Exception($"Incompatible spectral measurement conditions!\n\nthis: {this}\nother: {other}");
        }

        /// <summary>
        /// "Spectrally compatible" means: comparing XYZ/Lab of these two makes sense.
        /// 
        /// Literally: illuminant, observer and spectral m-condition are the same.
        /// </summary>
        public bool SpectrallyCompatibleWith([NotNull] SpectralMeasurementConditions other) {
            if (other == null) throw new ArgumentNullException(nameof(other));

            return IllObsMatch(other) && SpectralMCondition == other.SpectralMCondition;
        }

        public bool IllObsMatch([NotNull] SpectralMeasurementConditions other) {
            if (other == null) throw new ArgumentNullException(nameof(other));

            return Illuminant == other.Illuminant && Observer == other.Observer;
        }

        public bool Equals(SpectralMeasurementConditions other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Illuminant == other.Illuminant && Observer == other.Observer &&
                   SpectralMCondition == other.SpectralMCondition;
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((SpectralMeasurementConditions)obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = (int)Illuminant;
                hashCode = (hashCode * 397) ^ (int)Observer;
                hashCode = (hashCode * 397) ^ (int)SpectralMCondition;
                return hashCode;
            }
        }

        public static bool operator ==(SpectralMeasurementConditions left, SpectralMeasurementConditions right) {
            return Equals(left, right);
        }

        public static bool operator !=(SpectralMeasurementConditions left, SpectralMeasurementConditions right) {
            return !Equals(left, right);
        }

        public override string ToString() {
            return $"{Illuminant}/{Observer.ToHumanString()}, Spec: {SpectralMCondition}";
        }
    }
}