using System;

using JetBrains.Annotations;

namespace Colorware.Core.Color {
    /// <summary>
    /// Note: Dotgain is `the tone value` - `the tint` (percentage)
    /// </summary>
    public static class Dotgain {
        public static double MurrayDaviesDensities(double paperWhiteDensity, double fullColorDensity,
                                                   double targetPatchDensity, double percentage) {
            var top = 1.0f - Math.Pow(10.0f, paperWhiteDensity - targetPatchDensity);
            var bottom = 1.0f - Math.Pow(10.0f, paperWhiteDensity - fullColorDensity);

            var result = (top / bottom) * 100.0f - percentage;

            return double.IsNaN(result) || double.IsInfinity(result) ? 0 : result;
        }

        public static double MurrayDaviesDensities([NotNull] CMYK paperWhite,
                                                   [NotNull] CMYK fullColor,
                                                   [NotNull] CMYK targetPatch,
                                                   double percentage) {
            var componentOfTarget = fullColor.GetFilteredComponent();
            var paperWhiteDensity = paperWhite.GetFilteredValue(componentOfTarget);
            var fullColorDensity = fullColor.GetFilteredValue(componentOfTarget);
            var targetDensity = targetPatch.GetFilteredValue(componentOfTarget);
            return MurrayDaviesDensities(paperWhiteDensity, fullColorDensity, targetDensity, percentage);
        }

        public static double MurrayDaviesSpectral(double paperWhite, double fullColor, double targetPatch,
                                                  double percentage) {
            var top = paperWhite - targetPatch;
            var bottom = paperWhite - fullColor;
            var result = (top / bottom) * 100.0f - percentage;

            return double.IsNaN(result) || double.IsInfinity(result) ? 0 : result;
        }

        // 'applyCyanIsoCorrection' specifies whether to apply the correction as specified in
        // ISO/FDIS 15339-2:2014, A3, alinea 3, or not.
        public static double MurrayDaviesSpectral(
            [NotNull] Lab paperWhite,
            [NotNull] Lab fullColor,
            [NotNull] Lab targetPatch,
            int percentage,
            CmykComponents activeCmykComponent,
            bool applyCyanIsoCorrection) {
            var sampleActiveWhite = getActiveComponent(paperWhite, activeCmykComponent, applyCyanIsoCorrection);
            var sampleActiveFull = getActiveComponent(fullColor, activeCmykComponent, applyCyanIsoCorrection);
            var sampleActivePatch = getActiveComponent(targetPatch, activeCmykComponent, applyCyanIsoCorrection);
            return MurrayDaviesSpectral(sampleActiveWhite, sampleActiveFull, sampleActivePatch, percentage);
        }

        /// <summary>
        /// See NEN-ISO 20654 for method details.
        /// </summary>
        /// <param name="paperWhite">The LAB of the paperwhite patch to use.</param>
        /// <param name="solid">The LAB of the solid patch to use.</param>
        /// <param name="tint">The LAB of the tint patch to use.</param>
        /// <returns>The Spot Color Tone Value (SCTV) for the given input combination.</returns>
        public static double SpotColorToneValue(Lab paperWhite, Lab solid, Lab tint, double percentage) {
            double vx(Lab lab) => lab.L + 116 * lab.a / 500;
            double vy(Lab lab) => lab.L;
            double vz(Lab lab) => lab.L - 116 * lab.b / 200;
            // V*s: solid.
            var vxs = vx(solid);
            var vys = vy(solid);
            var vzs = vz(solid);
            // V*p: paper.
            var vxp = vx(paperWhite);
            var vyp = vy(paperWhite);
            var vzp = vz(paperWhite);
            // V*t: tint.
            var vxt = vx(tint);
            var vyt = vy(tint);
            var vzt = vz(tint);
            var top = Math.Pow(vxt - vxp, 2) + Math.Pow(vyt - vyp, 2) + Math.Pow(vzt - vzp, 2);
            var bottom = Math.Pow(vxs - vxp, 2) + Math.Pow(vys - vyp, 2) + Math.Pow(vzs - vzp, 2);
            var sctv = 100 * Math.Sqrt(top / bottom);
            return sctv - percentage;
        }

        private static double getActiveComponent([NotNull] Lab lab, CmykComponents cmykComponent,
                                                 bool applyCyanIsoCorrection) {
            switch (cmykComponent) {
                case CmykComponents.C:
                    // Optional correction according to ISO/FDIS 15339-2:2014, A3, alinea 3
                    return applyCyanIsoCorrection ? (lab.AsXYZ.X - (0.55 * lab.AsXYZ.Z)) : lab.AsXYZ.X;
                case CmykComponents.M:
                    return lab.AsXYZ.Y;
                case CmykComponents.Y:
                    return lab.AsXYZ.Z;
                case CmykComponents.K:
                    return lab.AsXYZ.Y;
                default:
                    throw new ArgumentOutOfRangeException(nameof(cmykComponent));
            }
        }
    }
}