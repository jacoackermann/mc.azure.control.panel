﻿namespace Colorware.Core.Color {
    /// <summary>
    /// Signifies that the class has a delta-E override option.
    /// </summary>
    public interface IToleranceDeltaEOverride {
        /// <summary>
        /// When <c>true</c>, <see cref="DeltaEToleranceOverride"/> should be used for any delta-E calculations in addition to
        /// any external tolerance settings.
        /// </summary>
        bool UseDeltaEToleranceOverride { get; }

        /// <summary>
        /// The delta-E tolerance to use when <see cref="UseDeltaEToleranceOverride"/> is <c>true</c>.
        /// </summary>
        double DeltaEToleranceOverride { get; }
    }
}