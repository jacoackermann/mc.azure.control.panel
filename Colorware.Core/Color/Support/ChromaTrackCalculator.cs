﻿using System;
using System.Diagnostics;

using Colorware.Core.Color.Support.Exceptions;

using JetBrains.Annotations;

namespace Colorware.Core.Color.Support {
    /// <summary>
    /// See here for more information:
    /// http://johnthemathguy.blogspot.nl/2012/09/why-does-my-cyan-have-blues.html
    /// http://en.wikipedia.org/wiki/Beer%E2%80%93Lambert_law
    /// </summary>
    public static class ChromaTrackCalculator {
        /// <summary>
        /// Calculate a resulting spectrum for the given input parameters using Beer's Law.
        /// </summary>
        /// <param name="solidSpectrum">The spectrum of the solid color.</param>
        /// <param name="paperwhiteSpectrum">The spectrum of the paperwhite.</param>
        /// <param name="kFactor">The `k' factor. </param>
        /// <returns>The resulting spectrum.</returns>
        ///<exception cref="ChromaTrackCalculationException">Thrown when an invalid condition is encountered (for example, an invalid curve)</exception>
        public static Spectrum Calculate([NotNull] Spectrum solidSpectrum, [NotNull] Spectrum paperwhiteSpectrum,
                                         double kFactor) {
            if (solidSpectrum == null) throw new ArgumentNullException(nameof(solidSpectrum));
            if (paperwhiteSpectrum == null) throw new ArgumentNullException(nameof(paperwhiteSpectrum));

            var startWL = Math.Max(solidSpectrum.StartWavelength, paperwhiteSpectrum.StartWavelength);
            var endWL = Math.Min(solidSpectrum.EndWaveLength, paperwhiteSpectrum.EndWaveLength);
            solidSpectrum = solidSpectrum.ClipToWavelength(startWL, endWL);
            paperwhiteSpectrum = paperwhiteSpectrum.ClipToWavelength(startWL, endWL);

            // Impart these limits for now. If these are to be removed we propably need to clip the spectral curves
            // and make sure that values can be retrieved by interpolation.
            Debug.Assert(solidSpectrum.SpectrumSamples.Count == paperwhiteSpectrum.SpectrumSamples.Count);

            var solidValues = solidSpectrum.SpectrumSamples;
            var paperValues = paperwhiteSpectrum.SpectrumSamples;
            var resultingValues = new double[solidValues.Count];
            for (var i = 0; i < solidValues.Count; i ++)
                resultingValues[i] = Calculate(solidValues[i].Value, paperValues[i].Value, kFactor);

            var newSpectrum = new Spectrum(resultingValues, solidSpectrum.StartWavelength, solidSpectrum.EndWaveLength);
            return newSpectrum;
        }

        /// <summary>
        /// Calculate a single value of a spectrum curve at a given wavelength.
        /// </summary>
        /// <param name="solidIntensity">The solid intensity.</param>
        /// <param name="paperIntensity">The paper intensity.</param>
        /// <param name="kFactor">The `k' factor.</param>
        /// <returns>The resulting intensity.</returns>
        /// <exception cref="ChromaTrackCalculationException">Thrown when an invalid result is produced (generally due to an invalid input value).</exception>
        public static double Calculate(double solidIntensity, double paperIntensity, double kFactor) {
            // val <- ((s_1 / p)^k) * p;
            var result = Math.Pow(solidIntensity / paperIntensity, kFactor) * paperIntensity;
            if (double.IsNaN(result)) {
                if (paperIntensity <= 0.0)
                    throw new ChromaTrackCalculationException("Invalid paper curve detected (value was 0)");
                throw new ChromaTrackCalculationException(
                    "Could not calculate ink prediction trajectory (invalid curves)");
            }
            return result;
        }
    }
}