﻿using System;

using Colorware.Core.Enums;

namespace Colorware.Core.Color.Support {
    public class TriStimulusReference {
        public readonly String Name;
        public readonly String Notes;
        public readonly double XRef;
        public readonly double YRef;
        public readonly double ZRef;
        public readonly Illuminant Illuminant;
        public readonly Observer Observer;

        public TriStimulusReference(String name, Illuminant illuminant, Observer observer, String notes, double xRef,
            double yRef, double zRef) {
            Name = name;
            Notes = notes;
            XRef = xRef;
            YRef = yRef;
            ZRef = zRef;
            Illuminant = illuminant;
            Observer = observer;
        }

        /// <summary>
        /// Calculate a tristimulus white reference from a given spectral power distribution and observer discrete color matching funciton.
        /// See http://en.wikipedia.org/wiki/D65#Definition and http://en.wikipedia.org/wiki/CIE_1931_chromaticity_diagram#Color_matching_functions
        /// for more details.
        /// </summary>
        /// <param name="spd">Spectral power distribution to calculate for.</param>
        /// <param name="dcmf">Color matching function to calculate for.</param>
        /// <returns>A new TriStiumulusReference entry.</returns>
        public static TriStimulusReference CalculateTriStimulusReference(SpectralPowerDistribution spd,
            DiscreteColorMatchingFunction dcmf) {
            double x = 0, y = 0, z = 0;
            foreach (var entry in dcmf.Data) {
                var spdValue = spd.InterpolateDistributionAt(entry.WaveLength);
                x += entry.X * spdValue;
                y += entry.Y * spdValue;
                z += entry.Z * spdValue;
            }
            // 96.422, 100.0, 82.521
            var finalX = x / (x + y + z);
            var finalY = y / (x + y + z);
            var finalZ = z / (x + y + z);

            // Normalize values.
            var scalar = 100.0 / finalY;
            var xScaled = scalar * finalX;
            var zScaled = scalar * finalZ;

            var illuminant = spd.Illuminant;
            var observer = dcmf.Observer;
            var name = String.Format("{0} {1}", illuminant, observer);
            return new TriStimulusReference(name, illuminant, observer, "N/A", xScaled, 100.0, zScaled);
        }
    }
}