﻿using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Extensions;

using JetBrains.Annotations;

namespace Colorware.Core.Color.Support {
    public static class DotgainAlgorithms {
        /// <summary>
        /// Estimates the halftone spectrum according to the Murray-Davies equation.
        /// 
        /// Equation 1 in "A Universal Model for Halftone Reflectance", Patrick Noffke; John Seymour, 2012
        /// </summary>
        /// <param name="solid">The solid reflectance values.</param>
        /// <param name="paperWhite">The paperwhite reflectance values.</param>
        /// <param name="tint">The tint percentage in (0,1).</param>
        [NotNull, Pure]
        public static IReadOnlyList<double> EstimateHalftoneSpectrumMurrayDavies([NotNull] IEnumerable<double> solid,
            [NotNull] IEnumerable<double> paperWhite, double tint) {
            var result = paperWhite.Mul(1 - tint).Add(solid.Mul(tint))
                                   .ToList();
            return result;
        }

        /// <summary>
        /// Estimates the halftone spectrum according to the Beer's law equation.
        /// 
        /// Equation 4 in "A Universal Model for Halftone Reflectance", Patrick Noffke; John Seymour, 2012
        /// </summary>
        /// <param name="solid">The solid reflectance values.</param>
        /// <param name="paperWhite">The paperwhite reflectance values.</param>
        /// <param name="tint">The tint percentage in (0,1).</param>
        [NotNull, Pure]
        public static IReadOnlyList<double> EstimateHalftoneSpectrumBeersLaw([NotNull] IEnumerable<double> solid,
            [NotNull] IEnumerable<double> paperWhite, double tint) {
            var result = solid.Div(paperWhite).Pow(tint).Mul(paperWhite)
                              .ToList();
            return result;
        }

        /// <summary>
        /// Calculates the optimal value for the 'dot hardness' parameter h for use in the Noffke-Seymour
        /// equation.
        /// 
        /// Optimal means the value for which the difference between the estimated halftone spectrum and
        /// the actual measured halftone spectrum is minimal.
        /// 
        /// This is equation 14 in "A Universal Model for Halftone Reflectance", Patrick Noffke; John Seymour, 2012
        /// </summary>
        /// <param name="md">The Murray-Davies estimated halftone reflectance values.</param>
        /// <param name="bl">The Beer's Law estimated halftone reflectance values.</param>
        /// <param name="measuredTintPatch">The measured tint (halftone) patch reflectance values.</param>
        [Pure]
        public static double CalculateNoffkeSeymourOptimalH([NotNull] IEnumerable<double> md,
            [NotNull] IEnumerable<double> bl,
            [NotNull] IEnumerable<double> measuredTintPatch) {
            var top = measuredTintPatch.Sub(md)
                                       .Mul(bl.Sub(md))
                                       .Sum();

            var bottom = bl.Sub(md).Pow(2).Sum();

            // Is in [0, 1] for sane inputs
            var h = top / bottom;
            return h;
        }

        /// <summary>
        /// Estimates the halftone spectrum according to the Noffke-Seymour equation.
        ///  
        /// Equation 12 in "A Universal Model for Halftone Reflectance", Patrick Noffke; John Seymour, 2012
        /// The formula is called the "Murray-Davies-Beer Linear Equation" in that paper, but referred
        /// to as the Noffke-Seymour equation by John Seymour himself on his blog.
        /// </summary>
        /// <param name="md">The Murray-Davies estimated halftone reflectance values.</param>
        /// <param name="bl">The Beer's Law estimated halftone reflectance values.</param>
        /// <param name="h">The 'dot hardness' parameter, should be in [0, 1].</param>
        [NotNull, Pure]
        public static IReadOnlyList<double> EstimateHalftoneSpectrumNoffkeSeymour([NotNull] IEnumerable<double> md,
            [NotNull] IEnumerable<double> bl, double h) {
            var result = md.Mul(h).Add(bl.Mul(1 - h))
                           .ToList();
            return result;
        }
    }
}