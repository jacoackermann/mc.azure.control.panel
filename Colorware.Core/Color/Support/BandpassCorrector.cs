﻿using System;
using System.Linq;

using JetBrains.Annotations;

namespace Colorware.Core.Color.Support {
    /// <summary>
    /// Can be used to correct spectral data that has not been bandpass corrected, e.g. before
    /// converting the spectrum to XYZ using Table 5.
    /// </summary>
    /// <remarks>As in ASTM E2729-09</remarks>
    public static class BandpassCorrector {
        [Pure]
        public static Spectrum Correct([NotNull] Spectrum spectrum) {
            if (spectrum == null) throw new ArgumentNullException(nameof(spectrum));

            var ss = spectrum.SpectrumSamples.ToList();

            if (ss.Count < 5)
                throw new Exception("Bandpass correction method is undefined for < 5 samples.");

            // NB: Sample 1 and N remain the same

            // Second and next-to-last are special
            ss[1] = new Spectrum.SpectrumSample(ss[1].WaveLength,
                                                -0.10 * ss[0].Value
                                                + 1.21 * ss[1].Value
                                                - 0.12 * ss[2].Value
                                                + 0.01 * ss[3].Value);

            ss[ss.Count - 2] = new Spectrum.SpectrumSample(ss[ss.Count - 2].WaveLength,
                                                           -0.10 * ss[ss.Count - 1].Value
                                                           + 1.21 * ss[ss.Count - 2].Value
                                                           - 0.12 * ss[ss.Count - 3].Value
                                                           + 0.01 * ss[ss.Count - 4].Value);

            // Rest of samples are treated the same
            for (int i = 2; i < ss.Count - 2; i++) {
                ss[i] = new Spectrum.SpectrumSample(ss[i].WaveLength,
                                                    0.01 * ss[i - 2].Value
                                                    - 0.12 * ss[i - 1].Value
                                                    + 1.22 * ss[i].Value
                                                    - 0.12 * ss[i + 1].Value
                                                    + 0.01 * ss[i + 2].Value);
            }

            return new Spectrum(ss);
        }
    }
}