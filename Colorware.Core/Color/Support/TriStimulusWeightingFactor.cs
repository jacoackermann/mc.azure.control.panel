﻿using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Extensions;

namespace Colorware.Core.Color.Support {
    public class TriStimulusWeightingFactor : IEquatable<TriStimulusWeightingFactor> {
        public TriStimulusWeightingFactor(double wx, double wy, double wz) {
            Wx = wx;
            Wy = wy;
            Wz = wz;
        }

        public double Wx { get; set; }
        public double Wy { get; set; }
        public double Wz { get; set; }

        public static TriStimulusWeightingFactor operator +(TriStimulusWeightingFactor w1, TriStimulusWeightingFactor w2
            ) {
            return new TriStimulusWeightingFactor(w1.Wx + w2.Wx, w1.Wy + w2.Wy, w1.Wz + w2.Wz);
        }

        public bool Equals(TriStimulusWeightingFactor other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return Wx.NearlyEquals(other.Wx) && Wy.NearlyEquals(other.Wy) && Wz.NearlyEquals(other.Wz);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TriStimulusWeightingFactor)obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = Wx.GetHashCode();
                hashCode = (hashCode * 397) ^ Wy.GetHashCode();
                hashCode = (hashCode * 397) ^ Wz.GetHashCode();
                return hashCode;
            }
        }
    }

    public static class TriStimulusWeightingFactorExtensions {
        // Note that we don't use Aggregate() because it would do to many allocations
        // of TriStimulusWeightingFactor
        public static TriStimulusWeightingFactor Sum(this IEnumerable<TriStimulusWeightingFactor> source) {
            var result = new TriStimulusWeightingFactor(0, 0, 0);

            foreach (var wf in source) {
                result.Wx += wf.Wx;
                result.Wy += wf.Wy;
                result.Wz += wf.Wz;
            }

            return result;
        }

        public static TriStimulusWeightingFactor Sum<T>(this IEnumerable<T> source,
            Func<T, TriStimulusWeightingFactor> selector) {
            return source.Select(selector).Sum();
        }
    }
}