﻿using System;

using Colorware.Core.Enums;
using Colorware.Core.Helpers.Math;

namespace Colorware.Core.Color.Support {
    public static class ChromaticAdaptation {
        public class ScalingMethod {
            public readonly String Name;
            public readonly Matrix3 Matrix; // M_a
            public readonly Matrix3 InverseMatrix; // (M_a)^(-1) (a.k.a. the inverse of M_a).

            public ScalingMethod(String name, Matrix3 matrix, Matrix3 inverseMatrix) {
                Name = name;
                Matrix = matrix;
                InverseMatrix = inverseMatrix;
            }
        }

        private static Matrix3 calculateAdaptionTransformation(Illuminant sourceIllumiant, Observer sourceObserver,
                                                               Illuminant destIlluminant, Observer destObserver,
                                                               ScalingMethod scalingMethod) {
            var sourceTristimulus = TriStimulusReferenceData.GetTristimulusReferenceDataFor(sourceIllumiant,
                                                                                            sourceObserver);
            var destTristimulus = TriStimulusReferenceData.GetTristimulusReferenceDataFor(destIlluminant, destObserver);

            var sourceResponseConeDomain = scalingMethod.Matrix *
                                           new Vector3(sourceTristimulus.XRef, sourceTristimulus.YRef,
                                                       sourceTristimulus.ZRef);
            var destResponseConeDomain = scalingMethod.Matrix *
                                         new Vector3(destTristimulus.XRef, destTristimulus.YRef, destTristimulus.ZRef);
            var responseConeDomain = new Matrix3(new[] {
                                                     destResponseConeDomain.X / sourceResponseConeDomain.X, 0, 0,
                                                     0, destResponseConeDomain.Y / sourceResponseConeDomain.Y, 0,
                                                     0, 0, destResponseConeDomain.Z / sourceResponseConeDomain.Z
                                                 });

            return scalingMethod.InverseMatrix * (responseConeDomain * scalingMethod.Matrix);
        }

        // Keep some cached data. Chances are that we are going to transform a lot of colors
        // using the same settings and we don't really want to calculate the matrix for each
        // color.
        private static Illuminant lastSourceIlluminant, lastDestIlluminant;
        private static Observer lastSourceObserver, lastDestObserver;
        private static ScalingMethod lastScalingMethod;
        private static Matrix3 lastAdaptionTransformation;

        public static Matrix3 GetAdaptionTransformation(Illuminant sourceIllumiant, Observer sourceObserver,
                                                        Illuminant destIlluminant, Observer destObserver,
                                                        ScalingMethod scalingMethod) {
            if (sourceIllumiant == lastSourceIlluminant &&
                destIlluminant == lastDestIlluminant &&
                sourceObserver == lastSourceObserver &&
                destObserver == lastDestObserver &&
                scalingMethod == lastScalingMethod &&
                lastAdaptionTransformation != null)
                return lastAdaptionTransformation;

            lastSourceIlluminant = sourceIllumiant;
            lastDestIlluminant = destIlluminant;
            lastSourceObserver = sourceObserver;
            lastDestObserver = destObserver;
            lastScalingMethod = scalingMethod;
            lastAdaptionTransformation = calculateAdaptionTransformation(sourceIllumiant, sourceObserver,
                                                                         destIlluminant, destObserver, scalingMethod);
            return lastAdaptionTransformation;
        }

        public static XYZ Transform(XYZ xyz, Illuminant destIllumint, Observer destObserver,
                                    ScalingMethod scalingMethod) {
            var adaptionTransformation = GetAdaptionTransformation(xyz.MeasurementConditions.Illuminant,
                                                                   xyz.MeasurementConditions.Observer, destIllumint,
                                                                   destObserver, scalingMethod);
            var result = adaptionTransformation * new Vector3(xyz.X, xyz.Y, xyz.Z);
            return new XYZ(result.X, result.Y, result.Z,
                           new SpectralMeasurementConditions(destIllumint, destObserver,
                                                             xyz.MeasurementConditions.SpectralMCondition));
        }
    }
}