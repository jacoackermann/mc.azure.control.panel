﻿using System;
using System.Linq;

using Colorware.Core.Enums;
using Colorware.Core.Extensions;

namespace Colorware.Core.Color.Support {
    public static class TriStimulusReferenceData {
        // These are taken from Table 5 in ASTM standard E308-13 (the White Point values)

        // ReSharper disable InconsistentNaming
        public static readonly TriStimulusReference A_2 = new TriStimulusReference(
            "A 2Deg",
            Illuminant.A, Observer.TwoDeg,
            "N/A",
            109.850, 100.0, 35.585
        );

        public static readonly TriStimulusReference A_10 = new TriStimulusReference(
            "A 10Deg",
            Illuminant.A, Observer.TenDeg,
            "N/A",
            111.144, 100.0, 35.200
        );

        public static readonly TriStimulusReference C_2 = new TriStimulusReference(
            "C 2Deg",
            Illuminant.C, Observer.TwoDeg,
            "N/A",
            98.074, 100.0, 118.232
        );

        public static readonly TriStimulusReference C_10 = new TriStimulusReference(
            "C 10Deg",
            Illuminant.C, Observer.TenDeg,
            "N/A",
            97.285, 100.0, 116.145
        );

        public static readonly TriStimulusReference D50_2 = new TriStimulusReference(
            "D50 2Deg",
            Illuminant.D50, Observer.TwoDeg,
            "N/A",
            96.422, 100.0, 82.521
        );

        public static readonly TriStimulusReference D50_10 = new TriStimulusReference(
            "D50 10Deg",
            Illuminant.D50, Observer.TenDeg,
            "N/A",
            96.720, 100.0, 81.427
        );

        public static readonly TriStimulusReference D55_2 = new TriStimulusReference(
            "D55 2Deg",
            Illuminant.D55, Observer.TwoDeg,
            "N/A",
            95.682, 100.0, 92.149
        );

        public static readonly TriStimulusReference D55_10 = new TriStimulusReference(
            "D55 10Deg",
            Illuminant.D55, Observer.TenDeg,
            "N/A",
            95.799, 100.0, 90.926
        );

        public static readonly TriStimulusReference D65_2 = new TriStimulusReference(
            "D65 2Deg",
            Illuminant.D65, Observer.TwoDeg,
            "Television sRgb colorspace",
            95.047, 100.0, 108.883
        );

        public static readonly TriStimulusReference D65_10 = new TriStimulusReference(
            "D65 10Deg",
            Illuminant.D65, Observer.TenDeg,
            "N/A",
            94.811, 100.0, 107.304
        );

        public static readonly TriStimulusReference D75_2 = new TriStimulusReference(
            "D75 2Deg",
            Illuminant.D75, Observer.TwoDeg,
            "N/A",
            94.972, 100.0, 122.638
        );

        public static readonly TriStimulusReference D75_10 = new TriStimulusReference(
            "D75 10Deg",
            Illuminant.D75, Observer.TenDeg,
            "N/A",
            94.416, 100.0, 120.641
        );

        public static readonly TriStimulusReference F2_2 = new TriStimulusReference(
            "F2 2Deg",
            Illuminant.F2, Observer.TwoDeg,
            "N/A",
            99.186, 100.0, 67.393
        );

        public static readonly TriStimulusReference F2_10 = new TriStimulusReference(
            "F2 10Deg",
            Illuminant.F2, Observer.TenDeg,
            "N/A",
            103.279, 100.0, 69.027
        );

        public static readonly TriStimulusReference F7_2 = new TriStimulusReference(
            "F7 2Deg",
            Illuminant.F7, Observer.TwoDeg,
            "N/A",
            95.041, 100.0, 108.747
        );

        public static readonly TriStimulusReference F7_10 = new TriStimulusReference(
            "F7 10Deg",
            Illuminant.F7, Observer.TenDeg,
            "N/A",
            95.792, 100.0, 107.686
        );

        public static readonly TriStimulusReference F11_2 = new TriStimulusReference(
            "F11 2Deg",
            Illuminant.F11, Observer.TwoDeg,
            "N/A",
            100.962, 100.0, 64.350
        );

        public static readonly TriStimulusReference F11_10 = new TriStimulusReference(
            "F11 10Deg",
            Illuminant.F11, Observer.TenDeg,
            "N/A",
            103.863, 100.0, 65.607
        );

        // ReSharper restore InconsistentNaming

        public static readonly TriStimulusReference F12Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F1,
                                                               DiscreteColorMatchingFunctionData.Cie1931TwoDegrees);

        public static readonly TriStimulusReference F110Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F1,
                                                               DiscreteColorMatchingFunctionData.Cie1964TenDegrees);

        public static readonly TriStimulusReference F32Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F3,
                                                               DiscreteColorMatchingFunctionData.Cie1931TwoDegrees);

        public static readonly TriStimulusReference F310Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F3,
                                                               DiscreteColorMatchingFunctionData.Cie1964TenDegrees);

        public static readonly TriStimulusReference F42Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F4,
                                                               DiscreteColorMatchingFunctionData.Cie1931TwoDegrees);

        public static readonly TriStimulusReference F410Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F4,
                                                               DiscreteColorMatchingFunctionData.Cie1964TenDegrees);

        public static readonly TriStimulusReference F52Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F5,
                                                               DiscreteColorMatchingFunctionData.Cie1931TwoDegrees);

        public static readonly TriStimulusReference F510Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F5,
                                                               DiscreteColorMatchingFunctionData.Cie1964TenDegrees);

        public static readonly TriStimulusReference F62Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F6,
                                                               DiscreteColorMatchingFunctionData.Cie1931TwoDegrees);

        public static readonly TriStimulusReference F610Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F6,
                                                               DiscreteColorMatchingFunctionData.Cie1964TenDegrees);

        public static readonly TriStimulusReference F82Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F8,
                                                               DiscreteColorMatchingFunctionData.Cie1931TwoDegrees);

        public static readonly TriStimulusReference F810Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F8,
                                                               DiscreteColorMatchingFunctionData.Cie1964TenDegrees);

        public static readonly TriStimulusReference F92Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F9,
                                                               DiscreteColorMatchingFunctionData.Cie1931TwoDegrees);

        public static readonly TriStimulusReference F910Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F9,
                                                               DiscreteColorMatchingFunctionData.Cie1964TenDegrees);

        public static readonly TriStimulusReference F102Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F10,
                                                               DiscreteColorMatchingFunctionData.Cie1931TwoDegrees);

        public static readonly TriStimulusReference F1010Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F10,
                                                               DiscreteColorMatchingFunctionData.Cie1964TenDegrees);

        public static readonly TriStimulusReference F122Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F12,
                                                               DiscreteColorMatchingFunctionData.Cie1931TwoDegrees);

        public static readonly TriStimulusReference F1210Deg =
            TriStimulusReference.CalculateTriStimulusReference(SpectralPowerDistributionData.F12,
                                                               DiscreteColorMatchingFunctionData.Cie1964TenDegrees);

        public static TriStimulusReference GetTristimulusReferenceDataFor(Illuminant illuminant, Observer observer) {
            switch (observer) {
                case Observer.TwoDeg:
                    switch (illuminant) {
                        case Illuminant.A:
                            return A_2;
                        case Illuminant.C:
                            return C_2;
                        case Illuminant.D50:
                            return D50_2;
                        case Illuminant.D55:
                            return D55_2;
                        case Illuminant.D65:
                            return D65_2;
                        case Illuminant.D75:
                            return D75_2;
                        case Illuminant.F1:
                            return F12Deg;
                        case Illuminant.F2:
                            return F2_2;
                        case Illuminant.F3:
                            return F32Deg;
                        case Illuminant.F4:
                            return F42Deg;
                        case Illuminant.F5:
                            return F52Deg;
                        case Illuminant.F6:
                            return F62Deg;
                        case Illuminant.F7:
                            return F7_2;
                        case Illuminant.F8:
                            return F82Deg;
                        case Illuminant.F9:
                            return F92Deg;
                        case Illuminant.F10:
                            return F102Deg;
                        case Illuminant.F11:
                            return F11_2;
                        case Illuminant.F12:
                            return F122Deg;
                        default:
                            throw new ArgumentOutOfRangeException("illuminant");
                    }
                case Observer.TenDeg:
                    switch (illuminant) {
                        case Illuminant.D50:
                            return D50_10;
                        case Illuminant.D55:
                            return D55_10;
                        case Illuminant.D65:
                            return D65_10;
                        case Illuminant.D75:
                            return D75_10;
                        case Illuminant.A:
                            return A_10;
                        case Illuminant.C:
                            return C_10;
                        case Illuminant.F1:
                            return F110Deg;
                        case Illuminant.F2:
                            return F2_10;
                        case Illuminant.F3:
                            return F310Deg;
                        case Illuminant.F4:
                            return F410Deg;
                        case Illuminant.F5:
                            return F510Deg;
                        case Illuminant.F6:
                            return F610Deg;
                        case Illuminant.F7:
                            return F7_10;
                        case Illuminant.F8:
                            return F810Deg;
                        case Illuminant.F9:
                            return F910Deg;
                        case Illuminant.F10:
                            return F1010Deg;
                        case Illuminant.F11:
                            return F11_10;
                        case Illuminant.F12:
                            return F1210Deg;
                        default:
                            throw new ArgumentOutOfRangeException("illuminant");
                    }
                default:
                    throw new ArgumentOutOfRangeException("observer");
            }
        }

        public static TriStimulusReference FindMatchingTristimulusReferenceFor(XYZTuple xyz) {
            return SetupHelpers
                .ImplementedIlluminants
                .SelectMany(ill => SetupHelpers.SelectableObservers.Select(obs => GetTristimulusReferenceDataFor(ill, obs)))
                .MinBy(reference =>
                           xyz.Distance(
                               new XYZTuple(reference.XRef, reference.YRef, reference.ZRef)));
        }
    }
}