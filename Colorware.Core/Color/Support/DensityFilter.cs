
        /// <summary>
        /// Returned by density calculation functions when no proper density could be calculated
        /// (due to invalid inputs).
        /// </summary>
        private const double InvalidDensityConstant = 9.9999999;

        /// <summary>
        /// Calculates the density value from a spectrum for a given filter.
        /// 
        /// Guaranteed to return values in range [0, double.MaxValue].
        /// </summary>


            // Prevent anything outside [0, double.MaxValue], instead, return something that resembles black.
            // This should signify to the user something strange is going on with the patch.
            // (It's currently the best we can do without refactoring (e.g. returning Result<double>))
            if (result < 0 || double.IsInfinity(result) || double.IsNaN(result))
                result = InvalidDensityConstant;
