﻿using System;

using Colorware.Core.Helpers;

namespace Colorware.Core.Color.Support {
    public static class ColorantReferenceData {
        // These are temporary empirical values
        // The hue tuples are target, deviation pairs, to denote a range of [target - deviation, target + deviation]
        
        private static readonly Tuple<double, double> hueCyan = new Tuple<double, double>(
            235.0, 10.0
            );

        private static readonly Tuple<double, double> hueMagenta = new Tuple<double, double>(
            2.0, 10.0
            );

        private static readonly Tuple<double, double> hueYellow = new Tuple<double, double>(
            94.0, 5.0
            );

        private static readonly Tuple<double, double> hueOrange = new Tuple<double, double>(
            43.0, 20.0
            );

        private static readonly Tuple<double, double> hueGreen = new Tuple<double, double>(
            168.0, 20.0
            );

        private static readonly Tuple<double, double> hueViolet = new Tuple<double, double>(
            295.0, 20.0
            );

        private static readonly double chromaKey = 10.0;

        public enum ColorantReference {
            Cyan,
            Magenta,
            Yellow,
            Key,
            Orange,
            Green,
            Violet,
            Unknown
        };

        private static bool isHueWithinRange(double hue, Tuple<double, double> hueTarget) {
            return (Math.Abs(AngleHelper.AngleDifferenceDegrees(hue, hueTarget.Item1)) <= hueTarget.Item2);
        }

        // Given some colorant, find if it matches any reference colorant
        public static ColorantReference GetColorantReference(Lch colorant) {
            if (colorant.c < chromaKey)
                return ColorantReference.Key;
            else if (isHueWithinRange(colorant.h, hueCyan))
                return ColorantReference.Cyan;
            else if (isHueWithinRange(colorant.h, hueMagenta))
                return ColorantReference.Magenta;
            else if (isHueWithinRange(colorant.h, hueYellow))
                return ColorantReference.Yellow;
            else if (isHueWithinRange(colorant.h, hueOrange))
                return ColorantReference.Orange;
            else if (isHueWithinRange(colorant.h, hueGreen))
                return ColorantReference.Green;
            else if (isHueWithinRange(colorant.h, hueViolet))
                return ColorantReference.Violet;
            return ColorantReference.Unknown;
        }
    }
}