﻿using System;
using System.Collections.Generic;

using Colorware.Core.Enums;

namespace Colorware.Core.Color.Support {
    /// <summary>
    /// Used in XYZ/Lab conversions. See DiscreteColorMatchingFunctionData for concrete data.
    /// </summary>
    public class DiscreteColorMatchingFunction {
        public class DiscreteColorMatchingEntry {
            public DiscreteColorMatchingEntry(double wavelength, double x, double y, double z) {
                WaveLength = wavelength;
                X = x;
                Y = y;
                Z = z;
            }

            public readonly double WaveLength;
            public readonly double X, Y, Z;
        }

        public String Name = "NA";
        public readonly Observer Observer;
        public readonly double WaveLengthSpacing;
        public readonly List<DiscreteColorMatchingEntry> Data;

        public int SampleSize {
            get { return Data.Count; }
        }

        public DiscreteColorMatchingFunction(String name, Observer observer, double waveLengthSpacing,
            DiscreteColorMatchingEntry[] newData) {
            Name = name;
            Observer = observer;
            WaveLengthSpacing = waveLengthSpacing;
            Data = new List<DiscreteColorMatchingEntry>(newData);
        }
    }
}