﻿using System;

namespace Colorware.Core.Color.Support.Exceptions {
    /// <summary>
    /// Used in <see cref="ChromaTrackCalculator"/> to indicate that there was an error
    /// while calculating a result.
    /// </summary>
    [Serializable]
    public class ChromaTrackCalculationException : Exception {
        public ChromaTrackCalculationException(string message) : base(message) {
        }
    }
}