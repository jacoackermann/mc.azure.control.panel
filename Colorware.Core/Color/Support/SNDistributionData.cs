﻿namespace Colorware.Core.Color.Support {
    internal static class SNDistributionData {
        public static readonly SNDistribution Distribution = new SNDistribution(
            10.0,
            new SNDistribution.SNDistributionSample[] {
                #region Samples
                new SNDistribution.SNDistributionSample(300, 0.04, 0.02, 0.0),
                new SNDistribution.SNDistributionSample(310, 6.00, 4.50, 2.0),
                new SNDistribution.SNDistributionSample(320, 29.60, 22.40, 4.0),
                new SNDistribution.SNDistributionSample(330, 55.30, 42.00, 8.5),
                new SNDistribution.SNDistributionSample(340, 57.30, 40.60, 7.8),
                new SNDistribution.SNDistributionSample(350, 61.80, 41.60, 6.7),
                new SNDistribution.SNDistributionSample(360, 61.50, 38.00, 5.3),
                new SNDistribution.SNDistributionSample(370, 68.80, 43.40, 6.1),
                new SNDistribution.SNDistributionSample(380, 63.40, 38.50, 3.0),
                new SNDistribution.SNDistributionSample(390, 65.80, 35.00, 1.2),
                new SNDistribution.SNDistributionSample(400, 94.80, 43.40, -1.1),
                new SNDistribution.SNDistributionSample(410, 104.80, 46.30, -0.5),
                new SNDistribution.SNDistributionSample(420, 105.90, 43.90, -0.7),
                new SNDistribution.SNDistributionSample(430, 96.80, 37.10, -1.2),
                new SNDistribution.SNDistributionSample(440, 113.90, 36.70, -2.6),
                new SNDistribution.SNDistributionSample(450, 125.60, 35.90, -2.9),
                new SNDistribution.SNDistributionSample(460, 125.50, 32.60, -2.8),
                new SNDistribution.SNDistributionSample(470, 121.30, 27.90, -2.6),
                new SNDistribution.SNDistributionSample(480, 121.30, 24.30, -2.6),
                new SNDistribution.SNDistributionSample(490, 113.50, 20.10, -1.8),
                new SNDistribution.SNDistributionSample(500, 113.10, 16.20, -1.5),
                new SNDistribution.SNDistributionSample(510, 110.80, 13.20, -1.3),
                new SNDistribution.SNDistributionSample(520, 106.50, 8.60, -1.2),
                new SNDistribution.SNDistributionSample(530, 108.80, 6.10, -1.0),
                new SNDistribution.SNDistributionSample(540, 105.30, 4.20, -0.5),
                new SNDistribution.SNDistributionSample(550, 104.40, 1.90, -0.3),
                new SNDistribution.SNDistributionSample(560, 100.00, 0.00, 0.0),
                new SNDistribution.SNDistributionSample(570, 96.00, -1.60, 0.2),
                new SNDistribution.SNDistributionSample(580, 95.10, -3.50, 0.5),
                new SNDistribution.SNDistributionSample(590, 89.10, -3.50, 2.1),
                new SNDistribution.SNDistributionSample(600, 90.50, -5.80, 3.2),
                new SNDistribution.SNDistributionSample(610, 90.30, -7.20, 4.1),
                new SNDistribution.SNDistributionSample(620, 88.40, -8.60, 4.7),
                new SNDistribution.SNDistributionSample(630, 84.00, -9.50, 5.1),
                new SNDistribution.SNDistributionSample(640, 85.10, -10.90, 6.7),
                new SNDistribution.SNDistributionSample(650, 81.90, -10.70, 7.3),
                new SNDistribution.SNDistributionSample(660, 82.60, -12.00, 8.6),
                new SNDistribution.SNDistributionSample(670, 84.90, -14.00, 9.8),
                new SNDistribution.SNDistributionSample(680, 81.30, -13.60, 10.2),
                new SNDistribution.SNDistributionSample(690, 71.90, -12.00, 8.3),
                new SNDistribution.SNDistributionSample(700, 74.30, -13.30, 9.6),
                new SNDistribution.SNDistributionSample(710, 76.40, -12.90, 8.5),
                new SNDistribution.SNDistributionSample(720, 63.30, -10.60, 7.0),
                new SNDistribution.SNDistributionSample(730, 71.70, -11.60, 7.6),
                new SNDistribution.SNDistributionSample(740, 77.00, -12.20, 8.0),
                new SNDistribution.SNDistributionSample(750, 65.20, -10.20, 6.7),
                new SNDistribution.SNDistributionSample(760, 47.70, -7.80, 5.2),
                new SNDistribution.SNDistributionSample(770, 68.60, -11.20, 7.4),
                new SNDistribution.SNDistributionSample(780, 65.00, -10.40, 6.8),
                new SNDistribution.SNDistributionSample(790, 66.00, -10.60, 7.0),
                new SNDistribution.SNDistributionSample(800, 61.00, -9.70, 6.4),
                new SNDistribution.SNDistributionSample(810, 53.30, -8.30, 5.5),
                new SNDistribution.SNDistributionSample(820, 58.90, -9.30, 6.1),
                new SNDistribution.SNDistributionSample(830, 61.90, -9.80, 6.5)
                #endregion
            }
            );
    }
}