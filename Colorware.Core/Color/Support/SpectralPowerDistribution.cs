using System;

using Colorware.Core.Enums;

namespace Colorware.Core.Color.Support {
    public class SpectralPowerDistribution {
        public class SpectralPowerDistributionSample {
            public readonly double WaveLength;
            public readonly double Value;

            public SpectralPowerDistributionSample(double waveLength, double value) {
                WaveLength = waveLength;
                Value = value;
            }
        }

        public readonly double SampleSpacing;
        public readonly SpectralPowerDistributionSample[] Samples;
        public readonly Illuminant Illuminant;

        public int SampleCount {
            get { return Samples.Length; }
        }

        public SpectralPowerDistributionSample FirstSample {
            get { return Samples[0]; }
        }

        public SpectralPowerDistributionSample LastSample {
            get { return Samples[Samples.Length - 1]; }
        }

        public SpectralPowerDistribution(double sampleSpacing, Illuminant illuminant,
            SpectralPowerDistributionSample[] samples) {
            SampleSpacing = sampleSpacing;
            Samples = samples;
            Illuminant = illuminant;
        }

        public double InterpolateDistributionAt(double wavelength) {
            if (wavelength <= FirstSample.WaveLength)
                return FirstSample.Value;
            if (wavelength >= LastSample.WaveLength)
                return LastSample.Value;


            var nearestExistingWL = (int)(wavelength / SampleSpacing) * SampleSpacing;
            var idx = (int)((nearestExistingWL - FirstSample.WaveLength) / SampleSpacing);

            if (idx > Samples.Length - 2)
                throw new ArgumentOutOfRangeException("Wavelength must be lower than last wavelength!");

            // Find `matching' two entries (eg, the requested wavelength should be between the found entries):
            var sample0 = Samples[idx];
            var sample1 = Samples[idx + 1];

            var waveLengthRange = sample1.WaveLength - sample0.WaveLength;
            double wavelengthNormalizedPerc = (wavelength - sample0.WaveLength) / waveLengthRange;
            double oneMinusWavelengthNormalizedPerc = (sample1.WaveLength - wavelength) / waveLengthRange;
            double value = sample0.Value * oneMinusWavelengthNormalizedPerc +
                           sample1.Value * wavelengthNormalizedPerc;
            // return sample0.Value;
            return value;

            /*
            for (var i = 0; i < Samples.Length - 1; i ++) {
                var sample0 = Samples[i];
                var sample1 = Samples[i + 1];
                if (sample0.WaveLength <= wavelength && sample1.WaveLength > wavelength) {
                    var waveLengthRange = sample1.WaveLength - sample0.WaveLength;
                    double wavelengthNormalizedPerc = (wavelength - sample0.WaveLength) / waveLengthRange;
                    double oneMinusWavelengthNormalizedPerc = (sample1.WaveLength - wavelength) / waveLengthRange;
                    double value = sample0.Value * oneMinusWavelengthNormalizedPerc +
                                   sample1.Value * wavelengthNormalizedPerc;
                    // return sample0.Value;
                    return value;
                }
            }
            
            throw new Exception("Requested wavelength value is not present: " + wavelength);
            */
        }
    }
}