﻿using System.Collections.Generic;

namespace Colorware.Core.Color.Support {
    internal class SNDistribution {
        public class SNDistributionSample {
            public SNDistributionSample(double waveLength, double s0, double s1, double s2) {
                WaveLength = waveLength;
                S0 = s0;
                S1 = s1;
                S2 = s2;
            }

            public readonly double WaveLength;
            public readonly double S0, S1, S2;
        }

        public readonly double SampleSpacing;
        public readonly List<SNDistributionSample> Samples;

        public int SampleSize {
            get { return Samples.Count; }
        }

        public SNDistributionSample FirstSample {
            get { return Samples[0]; }
        }

        public SNDistributionSample LastSample {
            get { return Samples[Samples.Count - 1]; }
        }

        public SNDistribution(double sampleSpacing, SNDistributionSample[] samples) {
            SampleSpacing = sampleSpacing;
            Samples = new List<SNDistributionSample>(samples);
        }
    }
}