﻿using System;

namespace Colorware.Core.Color.Support {
    public static class G7 {
        // ReSharper disable InconsistentNaming

        /// <summary>
        /// The 3-color near-neutral tone-scale as defined in CGATS TR15 5.2.
        /// </summary>
        /// <param name="C"></param>
        /// <returns>The CMYK value for the near-neutral grey</returns>
        public static CMYK NearNeutralToneScale(double C) {
            double MY = 0.7470 * C - 4.100E-4 * C * C + 2.940E-5 * C * C * C;
            return new CMYK(C, MY, MY, 0.0);
        }


        // The value for Rr given in CGATS TR15 5.4
        static double Rr3 = 0.956649;
        // The value for Rr given in CGATS TR15 5.5
        static double RrK = 0.978223;

        /// <summary>
        /// The 3-color Neutral Print Density (NPD), the lightness metric adopted for the 3-color near-neutral tone scale
        /// as defined in CGATS TR15 5.4.
        /// </summary>
        /// <param name="C">The tone value for Cyan in [0, 100]</param>
        /// <param name="densityCMY">The density of the CMY overprint</param>
        /// <param name="densitySubstrate">The density of the substrate, often 0</param>
        /// <returns>The Neutral Print Density (NPD)</returns>
        public static double NeutralPrintDensity3(double C, double densityCMY, double densitySubstrate) {
            double Yd = Math.Pow(10.0, -densityCMY);
            double Yl = Math.Pow(10.0, -densitySubstrate);
            double Ra = 1.0 - Yd / Yl;
            double C2 = C * C;
            double TVI = 1.31587 * C - 2.21633E-2 * C2 + 1.32926E-4 * C * C2 - 4.288E-7 * C2 * C2;
            double Yr = 1.0 - Rr3 * (C + TVI) / 100.0;
            double Yc = Math.Pow(0.7 + 0.3 * Math.Pow(Yd / Yl, 1.0 / 3.0), 3.0);

            if (Yr > Yc)
                return -Math.Log10(Yr);
            else
                return -Math.Log10(Yr - (Ra - Rr3) * Math.Pow((Yc - Yr) / (Yc - 1.0 + Rr3), Ra / 2.0 + 1.0));
        }


        /// <summary>
        /// The Black Neutral Print Density (NPD), the lightness metric adopted for the black tone scale
        /// as defined in CGATS TR15 5.5.
        /// </summary>
        /// <param name="K">The tone value for Black in [0, 100]</param>
        /// <param name="densityK">The density of the black solid</param>
        /// <param name="densitySubstrate">The density of the substrate, often 0</param>
        /// <returns>The Neutral Print Density (NPD)</returns>
        public static double NeutralPrintDensityK(double K, double densityK, double densitySubstrate) {
            double Yd = Math.Pow(10.0, -densityK);
            double Yl = Math.Pow(10.0, -densitySubstrate);
            double Ra = 1.0 - Yd / Yl;
            double K2 = K * K;
            double TVI = 0.967175 * K - 1.525445E-2 * K2 + 9.1347E-5 * K * K2 - 3.552E-7 * K2 * K2;
            double Yr = 1.0 - RrK * (K + TVI) / 100.0;
            double Yc = Math.Pow(0.7 + 0.3 * Math.Pow(Yd / Yl, 1.0 / 3.0), 3.0);

            if (Yr > Yc)
                return -Math.Log10(Yr);
            else
                return -Math.Log10(Yr - (Ra - RrK) * Math.Pow((Yc - Yr) / (Yc - 1.0 + RrK), Ra / 2.0 + 1.0));
        }

        /// <summary>
        /// Computes the CIE L* component (of CIE L*a*b*) corresponding to the 3-color NPD for the given parameters.
        /// This uses the definition in Annex C of CGATS TR15.
        /// </summary>
        /// <param name="C">The tone value for Cyan in [0, 100]</param>
        /// <param name="densityCMY">The density of the CMY overprint</param>
        /// <param name="densitySubstrate">The density of the substrate, often 0</param>
        /// <param name="conditions">The spectral measurement conditions to convert for</param>
        /// <returns>CIE L* value</returns>
        public static double GetTargetL3(double C, double densityCMY, double densitySubstrate,
                                         SpectralMeasurementConditions conditions) {
            double NPD = NeutralPrintDensity3(C, densityCMY, 0.0);
            double Y = 100.0 * Math.Pow(10.0, -densitySubstrate - NPD);
            return new XYZ(0.0, Y, 0.0, conditions).AsLab.L;
        }

        /// <summary>
        /// Computes the CIE L* component (of CIE L*a*b*) corresponding to the Black NPD for the given parameters.
        /// This uses the definition in Annex C of CGATS TR15.
        /// </summary>
        /// <param name="K">The tone value for Black in [0, 100]</param>
        /// <param name="densityK">The density of the black solid</param>
        /// <param name="densitySubstrate">The density of the substrate, often 0</param>
        /// <param name="conditions">The spectral measurement conditions to convert for</param>
        /// <returns>CIE L* value</returns>
        public static double GetTargetLK(double K, double densityK, double densitySubstrate,
                                         SpectralMeasurementConditions conditions) {
            double NPD = NeutralPrintDensityK(K, densityK, 0.0);
            double Y = 100.0 * Math.Pow(10.0, -densitySubstrate - NPD);
            return new XYZ(0.0, Y, 0.0, conditions).AsLab.L;
        }

        /// <summary>
        /// Computes the target CIE L*a*b* value for the given grey balance patch.
        /// This computes a new a* and b* value using the given L* of the patch.
        /// This is the linear grey balance as defined in CGATS TR15 5.3.
        /// </summary>
        /// <param name="C">The tone value for Cyan in [0, 100]</param>
        /// <param name="densityCMY">The density of the CMY overprint</param>
        /// <param name="densitySubstrate">The density of the substrate, often 0</param>
        /// <param name="overprintCMY">The CIE L*a*b* value of the CMY overprint</param>
        /// <param name="substrate">The CIE L*a*b* value of the substrate</param>
        /// <returns>Target CIE L*a*b*</returns>
        public static Lab GetTargetLabCMY(double C, double densityCMY, double densitySubstrate, Lab overprintCMY,
                                          Lab substrate) {
            double targetL = GetTargetL3(C, densityCMY, densitySubstrate, substrate.MeasurementConditions);
            double targetA = substrate.a * (1.0 - C / 100.0);
            double targetB = substrate.b * (1.0 - C / 100.0);
            return new Lab(targetL, targetA, targetB, substrate.MeasurementConditions);
        }

        /// <summary>
        /// Computes the target CIE L*a*b* value for the given black tone patch.
        /// This computes a new a* and b* value using the given L* of the patch.
        /// This is the black tone scale as defined in CGATS TR15 5.3.
        /// </summary>
        /// <param name="K">The tone value for Black in [0, 100]</param>
        /// <param name="densityK">The density of the K solid</param>
        /// <param name="densitySubstrate">The density of the substrate, often 0</param>
        /// <param name="solidK">The CIE L*a*b* value of the K solid</param>
        /// <param name="substrate">The CIE L*a*b* value of the substrate</param>
        /// <returns>Target CIE L*a*b*</returns>
        public static Lab GetTargetLabK(double K, double densityK, double densitySubstrate, Lab solidK, Lab substrate) {
            double targetL = GetTargetLK(K, densityK, densitySubstrate, substrate.MeasurementConditions);
            double targetA = substrate.a * (1.0 - K / 100.0);
            double targetB = substrate.b * (1.0 - K / 100.0);
            return new Lab(targetL, targetA, targetB, substrate.MeasurementConditions);
        }

        /// <summary>
        /// Computes the target CIE L*a*b* value for the given patch.
        /// This computes a new a* and b* value using the given L* of the patch.
        /// This is the grey balance as defined in Annex C of CGATS TR15.
        /// </summary>
        /// <param name="C">The tone value for Cyan in [0, 100]</param>
        /// <param name="densityCMY">The density of the CMY overprint</param>
        /// <param name="densitySubstrate">The density of the substrate, often 0</param>
        /// <param name="substrate">The CIE XYZ value of the substrate</param>
        /// <returns>Target CIE L*a*b*</returns>
        public static Lab GetTargetLabByXYZ(double C, double densityCMY, double densitySubstrate, XYZ substrate) {
            double NPD = NeutralPrintDensity3(C, densityCMY, densitySubstrate);
            TriStimulusReference triStimulusReference =
                TriStimulusReferenceData.GetTristimulusReferenceDataFor(substrate.MeasurementConditions.Illuminant,
                                                                        substrate.MeasurementConditions.Observer);

            double Yd = Math.Pow(10.0, -densityCMY);
            double Yl = Math.Pow(10.0, -densitySubstrate);
            double Y = 100.0 * Math.Pow(10.0, -densitySubstrate - NPD);
            double X = triStimulusReference.XRef / 100.0 * Y;
            double Z = triStimulusReference.ZRef / 100.0 * Y;

            //TODO: can be skipped in case the substrate is neutral
            double Cx = (substrate.X - triStimulusReference.XRef * Yl) / (triStimulusReference.XRef * (Yl - Yd));
            X += Cx * (X - triStimulusReference.XRef * Yd);
            double Cz = (substrate.Z - triStimulusReference.ZRef * Yl) / (triStimulusReference.ZRef * (Yl - Yd));
            Z += Cz * (Z - triStimulusReference.ZRef * Yd);

            return new XYZ(X, Y, Z, substrate.MeasurementConditions).AsLab;
        }

        /// <summary>
        /// Computes the target CIE L*a*b* value for the given patch.
        /// This computes a new a* and b* value using the given L* of the patch.
        /// This is the grey balance as defined in NEN-ISO 12647-2 section 4.2.8.
        /// </summary>
        /// <param name="C">The tone value for Cyan in [0, 100]</param>
        /// <param name="densityCMY">The density of the CMY overprint</param>
        /// <param name="densitySubstrate">The density of the substrate, often 0</param>
        /// <param name="overprintCMY">The CIE L*a*b* value of the CMY overprint</param>
        /// <param name="substrate">The CIE L*a*b* value of the substrate</param>
        /// <returns>Target CIE L*a*b*</returns>
        public static Lab GetTargetLabISO(double C, double densityCMY, double densitySubstrate, Lab overprintCMY,
                                          Lab substrate) {
            double targetL = GetTargetL3(C, densityCMY, densitySubstrate, substrate.MeasurementConditions);
            double targetA = substrate.a * (1.0 - 0.85 * (substrate.L - targetL) / (substrate.L - overprintCMY.L));
            double targetB = substrate.b * (1.0 - 0.85 * (substrate.L - targetL) / (substrate.L - overprintCMY.L));
            return new Lab(targetL, targetA, targetB, substrate.MeasurementConditions);
        }

        // ReSharper restore InconsistentNaming
    }
}