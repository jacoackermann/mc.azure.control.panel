﻿using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Extensions;

namespace Colorware.Core.Color.Support {
    public class AbridgedDensityWeightingFactor : IEquatable<AbridgedDensityWeightingFactor> {
        public AbridgedDensityWeightingFactor(double blue, double green, double red) {
            Blue = blue;
            Green = green;
            Red = red;
        }

        /// <summary>Blue filter, Yellow density channel.</summary>
        public double Blue { get; set; }
        /// <summary>Green filter, Magenta density channel.</summary>
        public double Green { get; set; }
        /// <summary>Red filter, Cyan density channel.</summary>
        public double Red { get; set; }

        public static AbridgedDensityWeightingFactor operator +(
            AbridgedDensityWeightingFactor w1, AbridgedDensityWeightingFactor w2) {
            return new AbridgedDensityWeightingFactor(w1.Blue + w2.Blue, w1.Green + w2.Green, w1.Red + w2.Red);
        }

        public bool Equals(AbridgedDensityWeightingFactor other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return Blue.NearlyEquals(other.Blue) && Green.NearlyEquals(other.Green) && Red.NearlyEquals(other.Red);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((AbridgedDensityWeightingFactor)obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = Blue.GetHashCode();
                hashCode = (hashCode * 397) ^ Green.GetHashCode();
                hashCode = (hashCode * 397) ^ Red.GetHashCode();
                return hashCode;
            }
        }
    }

    public static class AbridgedDensityWeightingFactorExtensions {
        // Note that we don't use Aggregate() because it would do to many allocations
        // of AbridgedDensityWeightingFactor
        public static AbridgedDensityWeightingFactor Sum(this IEnumerable<AbridgedDensityWeightingFactor> source) {
            var result = new AbridgedDensityWeightingFactor(0, 0, 0);

            foreach (var wf in source) {
                result.Blue += wf.Blue;
                result.Green += wf.Green;
                result.Red += wf.Red;
            }

            return result;
        }

        public static AbridgedDensityWeightingFactor Sum<T>(this IEnumerable<T> source,
                                                            Func<T, AbridgedDensityWeightingFactor> selector) {
            return source.Select(selector).Sum();
        }
    }
}