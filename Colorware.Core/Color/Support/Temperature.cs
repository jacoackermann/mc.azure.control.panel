﻿using System;

namespace Colorware.Core.Color.Support {
    public static class Temperature {
        public static bool TemperatureToXY(double temperature, out double resultingX, out double resultingY) {
            if (4000 <= temperature && temperature <= 7000) {
                double part_0 = -4.6070e9 / Math.Pow(temperature, 3);
                double part_1 = 2.9678e6 / Math.Pow(temperature, 2);
                double part_2 = 0.09911e3 / temperature;
                resultingX = part_0 + part_1 + part_2 + 0.244063;
            }
            else if (7000 < temperature && temperature <= 25000) {
                double part_0 = -2.0064e9 / Math.Pow(temperature, 3);
                double part_1 = 1.9018e6 / Math.Pow(temperature, 2);
                double part_2 = 0.24748e3 / temperature;
                resultingX = part_0 + part_1 + part_2 + 0.237040;
            }
            else {
                resultingX = 0;
                resultingY = 0;
                return false; // Might be an error. TODO: Add error handling?
            }
            resultingY = -3.0 * Math.Pow(resultingX, 2) + 2.870 * (resultingX) - 0.275;
            // DEBUG_OUTF( "temp_to_xy: %f\n    => x: %f, y: %f\n", temperature, resultingX, resultingY );
            return true;
        }
    }
}