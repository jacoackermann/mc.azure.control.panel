﻿using System;

using Colorware.Core.Enums;

namespace Colorware.Core.Color.Support {
    // ISO 5-3:2009
    public static class AbridgedDensityWeightingFactorTables {
        public const int FirstWaveLength = 340;
        public const int LastWaveLength = 770;
        public const int Interval = 10;

        public static AbridgedDensityWeightingFactor[] GetTable(DensityMethod densityMethod) {
            switch (densityMethod) {
                case DensityMethod.StatusE:
                    return StatusE;
                case DensityMethod.StatusT:
                    return StatusT;
                default:
                    throw new ArgumentOutOfRangeException(nameof(densityMethod), densityMethod, null);
            }
        }

        // ReSharper disable InconsistentNaming

        // Table 8, first column (ISO 5 visual)
        public static readonly double[] Visual = {
            0.000,
            0.000,
            0.000,
            0.000,
            0.000,
            0.000,
            0.001,
            0.002,
            0.008,
            0.027,
            0.061,
            0.117,
            0.209,
            0.362,
            0.620,
            1.039,
            1.792,
            3.087,
            4.754,
            6.321,
            7.598,
            8.569,
            9.220,
            9.456,
            9.219,
            8.547,
            7.545,
            6.358,
            5.077,
            3.716,
            2.559,
            1.6340,
            0.972,
            0.533,
            0.290,
            0.147,
            0.075,
            0.040,
            0.020,
            0.010,
            0.005,
            0.003,
            0.002,
            0.000
        };

        // Table 11
        public static readonly AbridgedDensityWeightingFactor[] StatusT = {
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.001, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.003, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.013, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.039, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.210, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.832, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(2.454, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(5.530, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(8.530, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(11.448, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(13.261, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(14.039, -0.001, 0.000),
            new AbridgedDensityWeightingFactor(13.648, -0.010, 0.000),
            new AbridgedDensityWeightingFactor(11.932, 0.173, 0.000),
            new AbridgedDensityWeightingFactor(9.143, 0.964, 0.000),
            new AbridgedDensityWeightingFactor(5.602, 5.466, 0.000),
            new AbridgedDensityWeightingFactor(2.568, 12.880, 0.000),
            new AbridgedDensityWeightingFactor(0.741, 17.593, 0.000),
            new AbridgedDensityWeightingFactor(0.006, 18.994, 0.000),
            new AbridgedDensityWeightingFactor(0.000, 16.786, 0.000),
            new AbridgedDensityWeightingFactor(0.001, 12.597, 0.000),
            new AbridgedDensityWeightingFactor(0.000, 8.011, 0.003),
            new AbridgedDensityWeightingFactor(0.000, 4.208, -0.042),
            new AbridgedDensityWeightingFactor(0.000, 1.715, -0.373),
            new AbridgedDensityWeightingFactor(0.000, 0.482, 11.492),
            new AbridgedDensityWeightingFactor(0.000, 0.125, 30.713),
            new AbridgedDensityWeightingFactor(0.000, 0.016, 27.283),
            new AbridgedDensityWeightingFactor(0.000, 0.001, 17.361),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 8.037),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 3.198),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 1.521),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.495),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.153),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.091),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.048),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.016),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.003),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.001),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.000)
        };

        // Table 12
        public static readonly AbridgedDensityWeightingFactor[] StatusE = {
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(-0.003, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.039, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.536, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(2.422, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(5.837, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(11.200, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(15.793, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(18.702, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(17.464, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(14.343, -0.001, 0.000),
            new AbridgedDensityWeightingFactor(8.886, -0.010, 0.000),
            new AbridgedDensityWeightingFactor(3.517, 0.172, 0.000),
            new AbridgedDensityWeightingFactor(1.106, 0.964, 0.000),
            new AbridgedDensityWeightingFactor(0.159, 5.466, 0.000),
            new AbridgedDensityWeightingFactor(-0.001, 12.880, 0.000),
            new AbridgedDensityWeightingFactor(0.001, 17.593, 0.000),
            new AbridgedDensityWeightingFactor(0.000, 18.994, 0.000),
            new AbridgedDensityWeightingFactor(0.000, 16.786, 0.000),
            new AbridgedDensityWeightingFactor(0.000, 12.597, 0.000),
            new AbridgedDensityWeightingFactor(0.000, 8.011, 0.003),
            new AbridgedDensityWeightingFactor(0.000, 4.208, -0.042),
            new AbridgedDensityWeightingFactor(0.000, 1.715, -0.373),
            new AbridgedDensityWeightingFactor(0.000, 0.482, 11.492),
            new AbridgedDensityWeightingFactor(0.000, 0.125, 30.713),
            new AbridgedDensityWeightingFactor(0.000, 0.016, 27.283),
            new AbridgedDensityWeightingFactor(0.000, 0.001, 17.361),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 8.037),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 3.198),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 1.521),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.495),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.153),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.091),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.048),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.016),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.003),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.001),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.000),
            new AbridgedDensityWeightingFactor(0.000, 0.000, 0.000)
        };

        // ReSharper restore InconsistentNaming
    }
}