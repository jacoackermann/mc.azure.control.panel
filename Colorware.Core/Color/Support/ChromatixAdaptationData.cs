using Colorware.Core.Helpers.Math;

namespace Colorware.Core.Color.Support {
    public static class ChromatixAdaptationData {
        public static readonly ChromaticAdaptation.ScalingMethod XYZScaling =
            new ChromaticAdaptation.ScalingMethod("XYZScaling",
                Matrix3.Identity(), Matrix3.Identity());

        public static readonly ChromaticAdaptation.ScalingMethod Bradford =
            new ChromaticAdaptation.ScalingMethod("Bradford",
                new Matrix3(new[] {
                    0.8951000, 0.2664000, -0.1614000,
                    -0.7502000, 1.7135000, 0.0367000,
                    0.0389000, -0.0685000, 1.0296000
                }),
                new Matrix3(new[] {
                    0.9869929, -0.1470543, 0.1599627,
                    0.4323053, 0.5183603, 0.0492912,
                    -0.0085287, 0.0400428, 0.9684867
                }));

        public static readonly ChromaticAdaptation.ScalingMethod VonKries =
            new ChromaticAdaptation.ScalingMethod("VonKries",
                new Matrix3(new[] {
                    0.4002400, 0.7076000, -0.0808100,
                    -0.2263000, 1.1653200, 0.0457000,
                    0.0000000, 0.0000000, 0.9182200
                }),
                new Matrix3(new[] {
                    1.8599364, -1.1293816, 0.2198974,
                    0.3611914, 0.6388125, -0.0000064,
                    0.0000000, 0.0000000, 1.0890636
                }));
    }
}