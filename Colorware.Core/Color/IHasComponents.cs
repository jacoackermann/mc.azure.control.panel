﻿using Colorware.Core.Extensions;

namespace Colorware.Core.Color {
    public interface IHasComponents {
        double Cyan { get; }
        double Magenta { get; }
        double Yellow { get; }
        double Key { get; }
    }

    public static class HasCompontensExtensions {
        public static bool IsKey(this IHasComponents hasComponents) {
            return hasComponents.Cyan.NearlyEquals(0) &&
                   hasComponents.Magenta.NearlyEquals(0) &&
                   hasComponents.Yellow.NearlyEquals(0) &&
                   hasComponents.Key > 0;
        }

        public static bool IsThreeColorComponent(this IHasComponents hasComponents) {
            return hasComponents.Cyan > 0 &&
                   hasComponents.Magenta > 0 &&
                   hasComponents.Yellow > 0 &&
                   hasComponents.Key.NearlyEquals(0);
        }
    }
}