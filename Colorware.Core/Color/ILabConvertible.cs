﻿using JetBrains.Annotations;

namespace Colorware.Core.Color {
    public interface ILabConvertible {
        [NotNull]
        Lab AsLab { get; } 
    }
}