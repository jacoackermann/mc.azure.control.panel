﻿using JetBrains.Annotations;

namespace Colorware.Core.Color {
    /// <summary>
    /// Use this to pass around raw Lab values without measurement conditions.
    /// 
    /// Does not actually represent a color due to missing measurement conditions.
    /// </summary>
    public class LabTuple {
        public double L { get; }
        public double a { get; }
        public double b { get; }

        public LabTuple(double l, double a, double b) {
            L = l;
            this.a = a;
            this.b = b;
        }

        [NotNull]
        public static LabTuple Zero { get; } = new LabTuple(0, 0, 0);
        

        public static LabTuple operator +([NotNull] LabTuple first, [NotNull] LabTuple second) {
            return new LabTuple(first.L + second.L, first.a + second.a, first.b + second.b);
        }

        public static LabTuple operator -([NotNull] LabTuple first, [NotNull] LabTuple second) {
            return new LabTuple(first.L - second.L, first.a - second.a, first.b - second.b);
        }

        public static LabTuple operator /([NotNull] LabTuple first, [NotNull] LabTuple second) {
            return new LabTuple(first.L / second.L, first.a / second.a, first.b / second.b);
        }

        public static LabTuple operator /([NotNull] LabTuple first, double scalar) {
            return new LabTuple(first.L / scalar, first.a / scalar, first.b / scalar);
        }

        public static LabTuple operator *([NotNull] LabTuple first, [NotNull] LabTuple second) {
            return new LabTuple(first.L * second.L, first.a * second.a, first.b * second.b);
        }

        public static LabTuple operator *([NotNull] LabTuple first, double scalar) {
            return new LabTuple(first.L * scalar, first.a * scalar, first.b * scalar);
        }
    }
}