﻿namespace Colorware.Core.Color {
    /// <summary>
    /// This is a crutch used for failing OOP design, don't use it if you don't
    /// need it!
    /// </summary>
    public interface IHasLabAndDensities : IHasDensities, ILabConvertible {}
}