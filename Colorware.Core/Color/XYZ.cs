﻿using System;

using Colorware.Core.Color.Support;
using Colorware.Core.Enums;

using JetBrains.Annotations;

namespace Colorware.Core.Color {
    public class XYZ {
        public double X { get; }
        public double Y { get; }
        public double Z { get; }
        public SpectralMeasurementConditions MeasurementConditions { get; }

        public XYZ(double x, double y, double z, [NotNull] SpectralMeasurementConditions measurementConditions) {
            if (measurementConditions == null) throw new ArgumentNullException(nameof(measurementConditions));

            X = x;
            Y = y;
            Z = z;
            MeasurementConditions = measurementConditions;
        }

        private System.Windows.Media.Color? asColor;

        public System.Windows.Media.Color AsColor => asColor ?? (asColor = AsRGB.AsColor).Value;

        private RGB asRGB;

        /// <summary>
        /// NB: Always uses D65/2 in conversion!
        /// </summary>
        public RGB AsRGB => asRGB ?? (asRGB = toRGB());

        // ReSharper disable InconsistentNaming
        [NotNull]
        private RGB toRGB() {
            double var_x = X / 100.0;
            double var_y = Y / 100.0;
            double var_z = Z / 100.0;

            /*
            //Observer. = 2deg, Illuminant = D65
              double var_r = var_x *  3.2406 + var_y * -1.5372 + var_z * -0.4986;
              double var_g = var_x * -0.9689 + var_y *  1.8758 + var_z *  0.0415;
              double var_b = var_x *  0.0557 + var_y * -0.2040 + var_z *  1.0570;
            */
            //Observer. = 2deg, Illuminant = D65 (Colormatch)
            double var_r = var_x * 2.64229 + var_y * -1.22343 + var_z * -0.393014;
            double var_g = var_x * -1.22343 + var_y * 2.05902 + var_z * 0.0159614;
            double var_b = var_x * 0.0821698 + var_y * -0.280725 + var_z * 1.45599;

            if (var_r > 0.0031308) var_r = 1.055 * Math.Pow(var_r, 1.0 / 2.4) - 0.055;
            else var_r = 12.92 * var_r;
            if (var_g > 0.0031308) var_g = 1.055 * Math.Pow(var_g, 1.0 / 2.4) - 0.055;
            else var_g = 12.92 * var_g;
            if (var_b > 0.0031308) var_b = 1.055 * Math.Pow(var_b, 1.0 / 2.4) - 0.055;
            else var_b = 12.92 * var_b;

            double r = var_r * 255.0;
            double g = var_g * 255.0;
            double b = var_b * 255.0;
            if (r < 0) r = 0.0;
            if (r > 255) r = 255.0;
            if (g < 0) g = 0.0;
            if (g > 255) g = 255.0;
            if (b < 0) b = 0.0;
            if (b > 255) b = 255.0;

            return new RGB(r, g, b);
        }

        private Lab asLab;

        public Lab AsLab => asLab ?? (asLab = toLab());

        /// <remarks>As in ASTM E308-13 (7.5.1)</remarks>
        private Lab toLab() {
            if (MeasurementConditions.Illuminant == Illuminant.Unknown ||
                MeasurementConditions.Observer == Observer.Unknown) {
                throw new Exception("Can't convert XYZ to Lab because the illuminant and/or observer is unknown!");
            }

            var tristimulusReference =
                TriStimulusReferenceData.GetTristimulusReferenceDataFor(MeasurementConditions.Illuminant,
                                                                        MeasurementConditions.Observer);

            double ref_x = tristimulusReference.XRef; //ref_X =  95.047  Observer= 2°, Illuminant= D65
            double ref_y = tristimulusReference.YRef; //ref_Y = 100.000
            double ref_z = tristimulusReference.ZRef; //ref_Z = 108.883

            double var_x = X / ref_x;
            double var_y = Y / ref_y;
            double var_z = Z / ref_z;

            const double factor1 = 841.0 / 108.0;
            const double factor2 = 4.0 / 29.0;
            const double threshold = 0.00885645167903563081717167575546;

            if (var_x > threshold)
                var_x = Math.Pow(var_x, (1.0 / 3.0));
            else
                var_x = (factor1 * var_x) + factor2;

            if (var_y > threshold)
                var_y = Math.Pow(var_y, (1.0 / 3.0));
            else
                var_y = (factor1 * var_y) + factor2;

            if (var_z > threshold)
                var_z = Math.Pow(var_z, (1.0 / 3.0));
            else
                var_z = (factor1 * var_z) + factor2;

            double l = (116.0 * var_y) - 16.0;
            double a = 500.0 * (var_x - var_y);
            double b = 200.0 * (var_y - var_z);

            return new Lab(l, a, b, MeasurementConditions);
        }

        // ReSharper restore InconsistentNaming

        [NotNull]
        public XYZTuple Tuple => new XYZTuple(X, Y, Z);

        #region Overrides of Object
        public override string ToString() {
            return FormattableString.Invariant($"XYZ({X}, {Y}, {Z})");
        }
        #endregion

        public static XYZ operator +(XYZ first, XYZ second) {
            first.MeasurementConditions.EnsureSpectrallyCompatibleWith(second.MeasurementConditions);

            return new XYZ(first.X + second.X, first.Y + second.Y, first.Z + second.Z, first.MeasurementConditions);
        }

        public static XYZ operator -(XYZ first, XYZ second) {
            first.MeasurementConditions.EnsureSpectrallyCompatibleWith(second.MeasurementConditions);

            return new XYZ(first.X - second.X, first.Y - second.Y, first.Z - second.Z, first.MeasurementConditions);
        }

        public static XYZ operator /(XYZ first, XYZ second) {
            first.MeasurementConditions.EnsureSpectrallyCompatibleWith(second.MeasurementConditions);

            return new XYZ(first.X / second.X, first.Y / second.Y, first.Z / second.Z, first.MeasurementConditions);
        }

        public static XYZ operator /(XYZ first, double scalar) {
            return new XYZ(first.X / scalar, first.Y / scalar, first.Z / scalar, first.MeasurementConditions);
        }

        public static XYZ operator *(XYZ first, XYZ second) {
            first.MeasurementConditions.EnsureSpectrallyCompatibleWith(second.MeasurementConditions);

            return new XYZ(first.X * second.X, first.Y * second.Y, first.Z * second.Z, first.MeasurementConditions);
        }

        public static XYZ operator *(XYZ first, double scalar) {
            return new XYZ(first.X * scalar, first.Y * scalar, first.Z * scalar, first.MeasurementConditions);
        }

        public double Distance(XYZ other) {
            XYZ diff = this - other;
            return Math.Sqrt(diff.X * diff.X + diff.Y * diff.Y + diff.Z * diff.Z);
        }
    }
}