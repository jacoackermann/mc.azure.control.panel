﻿namespace Colorware.Core.Color {
    public class NamedLab : Lab {
        public string Name { get; }

        // Both inheritance and composition for backwards compatibility
        public Lab AsLab { get; }

        public NamedLab(Lab lab, string name) : base(lab.L, lab.a, lab.b, lab.MeasurementConditions) {
            Name = name;
            AsLab = lab;
        }
    }
}