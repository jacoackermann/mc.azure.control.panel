﻿using System;

using JetBrains.Annotations;

namespace Colorware.Core.Color {
    /// <summary>
    /// Use this to pass around raw XYZ values without measurement conditions.
    /// 
    /// Does not actually represent a color due to missing measurement conditions.
    /// </summary>
    public class XYZTuple {
        public double X { get; }
        public double Y { get; }
        public double Z { get; }

        public XYZTuple(double x, double y, double z) {
            X = x;
            Y = y;
            Z = z;
        }

        [NotNull]
        public static XYZTuple Zero { get; } = new XYZTuple(0, 0, 0);

        public static XYZTuple operator +(XYZTuple first, XYZTuple second) {
            return new XYZTuple(first.X + second.X, first.Y + second.Y, first.Z + second.Z);
        }

        public static XYZTuple operator -(XYZTuple first, XYZTuple second) {
            return new XYZTuple(first.X - second.X, first.Y - second.Y, first.Z - second.Z);
        }

        public static XYZTuple operator /(XYZTuple first, XYZTuple second) {
            return new XYZTuple(first.X / second.X, first.Y / second.Y, first.Z / second.Z);
        }

        public static XYZTuple operator /(XYZTuple first, double scalar) {
            return new XYZTuple(first.X / scalar, first.Y / scalar, first.Z / scalar);
        }

        public static XYZTuple operator *(XYZTuple first, XYZTuple second) {
            return new XYZTuple(first.X * second.X, first.Y * second.Y, first.Z * second.Z);
        }

        public static XYZTuple operator *(XYZTuple first, double scalar) {
            return new XYZTuple(first.X * scalar, first.Y * scalar, first.Z * scalar);
        }

        public double Distance(XYZTuple other) {
            var diff = this - other;
            return Math.Sqrt(diff.X * diff.X + diff.Y * diff.Y + diff.Z * diff.Z);
        }
    }
}