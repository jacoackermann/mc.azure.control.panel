﻿using System;

using JetBrains.Annotations;

namespace Colorware.Core.Color {
    public class LabAndDensities : IHasLabAndDensities {
        public double Density => Densities.GetFilteredValue();
        public CMYK Densities { get; }
        public Lab AsLab { get; }

        public LabAndDensities([NotNull] CMYK densities, [NotNull] Lab asLab) {
            if (densities == null) throw new ArgumentNullException(nameof(densities));
            if (asLab == null) throw new ArgumentNullException(nameof(asLab));

            Densities = densities;
            AsLab = asLab;
        }
    }
}