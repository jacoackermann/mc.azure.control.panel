using Colorware.Core.Data.Models.StripCompareInterface;

namespace Colorware.Core.Color {
    public static class Trapping {
        // Preucil trapping:
        // % Trap = {(Density of overprint � Density of 1st ink) / Density of 2nd ink} x 100
        // (Use filter of 2nd ink)
        // 1st ink => 1st ink on paper.
        // Reverse ink sequence list and go from left to right!

        /// <summary>
        /// Calculate Preucil trapping using given parameters.
        /// Note this does't actually check the order of the solids with the printing order and assumes that the
        /// order is correct!
        /// </summary>
        /// <param name="overprint">The overprint</param>
        /// <param name="solid1">The first solid, assumed to be the first printed ink.</param>
        /// <param name="solid2">The second solid, assumed to be the second printed ink.</param>
        /// <returns></returns>
        public static double Preucil(IStripComparePatch overprint, IStripComparePatch solid1, IStripComparePatch solid2) {
            return Preucil(overprint.SampleDensity, solid1.SampleDensity, solid2.SampleDensity);
        }

        public static double Preucil(CMYK overprint, CMYK solid1, CMYK solid2) {
            var componentFilter = solid2.GetFilteredComponent();
            var overprintDensity = overprint.GetFilteredValue(componentFilter);
            var solid1Density = solid1.GetFilteredValue(componentFilter);
            var solid2Density = solid2.GetFilteredValue(componentFilter);
            return Preucil(overprintDensity, solid1Density, solid2Density);
        }

        private static double Preucil(double overprintDensity, double solid1Density, double solid2Density) {
            return ((overprintDensity - solid1Density) / solid2Density) * 100.0;
        }
    }
}