﻿using System;
using System.Windows.Media;

using Colorware.Core.Enums;

namespace Colorware.Core.Color {
    public class RGB {
        public double R { get; }
        public double G { get; }
        public double B { get; }

        public RGB()
            : this(0, 0, 0) {}

        public RGB(double r, double g, double b) {
            R = r;
            G = g;
            B = b;
        }

        public RGB(System.Drawing.Color color) : this(color.R, color.G, color.B) {}

        public RGB(System.Windows.Media.Color color) : this(color.R, color.G, color.B) {}

        public double[] ToCMYPercentages() {
            var c = 1 - R / 255.0;
            var m = 1 - G / 255.0;
            var y = 1 - B / 255.0;
            return new[] {c, m, y};
        }

        public double[] ToCMYKPercentages() {
            var cmy = ToCMYPercentages();
            var c = cmy[0];
            var m = cmy[1];
            var y = cmy[2];
            var k = 1.0;
            if (c < k) k = c;
            if (m < k) k = m;
            if (y < k) k = y;
            if (k >= 0.999 && k <= 1.001) {
                c = 0;
                m = 0;
                y = 0;
            }
            else {
                c = (c - k) / (1 - k);
                m = (m - k) / (1 - k);
                y = (y - k) / (1 - k);
            }
            return new[] {c, m, y, k};
        }

        private System.Windows.Media.Color toColor() {
            return System.Windows.Media.Color.FromArgb(255, (byte)R, (byte)G, (byte)B);
        }

        private System.Windows.Media.Color? asColor;

        public System.Windows.Media.Color AsColor => asColor ?? (asColor = toColor()).Value;

        private XYZ asXYZ;

        /// <summary>
        /// NB: Will always be D65/2!
        /// </summary>
        public XYZ AsXYZ => asXYZ ?? (asXYZ = toXYZ());

        private XYZ toXYZ() {
            var varR = R / 255; //R from 0 to 255
            var varG = G / 255; //G from 0 to 255
            var varB = B / 255; //B from 0 to 255

            if (varR > 0.04045) varR = Math.Pow((varR + 0.055) / 1.055, 2.4);
            else varR = varR / 12.92;
            if (varG > 0.04045) varG = Math.Pow((varG + 0.055) / 1.055, 2.4);
            else varG = varG / 12.92;
            if (varB > 0.04045) varB = Math.Pow((varB + 0.055) / 1.055, 2.4);
            else varB = varB / 12.92;

            varR = varR * 100;
            varG = varG * 100;
            varB = varB * 100;

            //Observer. = 2°, Illuminant = D65
            var x = varR * 0.4124 + varG * 0.3576 + varB * 0.1805;
            var y = varR * 0.2126 + varG * 0.7152 + varB * 0.0722;
            var z = varR * 0.0193 + varG * 0.1192 + varB * 0.9505;
            return new XYZ(x, y, z,
                           new SpectralMeasurementConditions(Illuminant.D65, Observer.TwoDeg, MCondition.M0));
        }

        /// <summary>
        /// NB: Will always be D65/2!
        /// </summary>
        public Lab AsLab => AsXYZ.AsLab;

        public double Distance(RGB rhs) {
            var dR = rhs.R - R;
            var dG = rhs.G - G;
            var dB = rhs.B - B;

            return Math.Sqrt(dR * dR + dG * dG + dB * dB);
        }

        public string ToHtmlColor() {
            var rByte = (byte)R;
            var gByte = (byte)G;
            var bByte = (byte)B;
            var asHtmlString = $"#{rByte:X2}{gByte:X2}{bByte:X2}";
            return asHtmlString;
        }

        public static RGB ParseHtmlColor(string backgroundColor) {
            var convertFromString = ColorConverter.ConvertFromString(backgroundColor);
            if (convertFromString != null) {
                var color = (System.Windows.Media.Color)convertFromString;
                return new RGB(color);
            }
            throw new Exception("Invalid HTML color: " + backgroundColor);
            /*
            var asChars = backgroundColor.ToCharArray();
            if(asChars.Length <= 0)
                throw new ArgumentException("Empty HTML color string", "backgroundColor");
            if(asChars[0] == '#' && asChars.Length != 9 || asChars.Length != 8)
                throw new ArgumentException("Invalid HTML color string: " + backgroundColor, "backgroundColor");
            var baseIdx = 0;
            if (asChars[0] == '#')
                baseIdx = 1;

            var r = parseHtmlColor(asChars[baseIdx + 0], asChars[baseIdx + 1]);
            var g = parseHtmlColor(asChars[baseIdx + 2], asChars[baseIdx + 3]);
            var b = parseHtmlColor(asChars[baseIdx + 4], asChars[baseIdx + 5]);
            return new RGB(r, g, b);
            */
        }
    }
}