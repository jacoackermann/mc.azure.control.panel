﻿using System;

using Colorware.Core.Enums;

namespace Colorware.Core.Color {
    public class MeasurementConditions : SpectralMeasurementConditions, IEquatable<MeasurementConditions> {
        public MeasurementConditions(Illuminant illuminant,
                                     Observer observer,
                                     MCondition densityMCondition,
                                     MCondition spectralMCondition) : base(illuminant, observer, spectralMCondition) {
            DensityMCondition = densityMCondition;
        }

        public new static readonly MeasurementConditions Undefined =
            new MeasurementConditions(Illuminant.Unknown, Observer.Unknown, MCondition.M0, MCondition.M0);

        // We only care about this in a MeasurementConditionsConfig and when measuring, it is
        // not used when comparing references with samples
        public MCondition DensityMCondition { get; }

        public override string ToString() {
            return $"{Illuminant}/{Observer.ToHumanString()}, Dens: {DensityMCondition}, Spec: {SpectralMCondition}";
        }

        public bool Equals(MeasurementConditions other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && DensityMCondition == other.DensityMCondition;
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((MeasurementConditions)obj);
        }

        public override int GetHashCode() {
            unchecked {
                return (base.GetHashCode() * 397) ^ (int)DensityMCondition;
            }
        }

        public static bool operator ==(MeasurementConditions left, MeasurementConditions right) {
            return Equals(left, right);
        }

        public static bool operator !=(MeasurementConditions left, MeasurementConditions right) {
            return !Equals(left, right);
        }
    }
}