﻿using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Enums;
using Colorware.Core.Extensions;

using JetBrains.Annotations;

namespace Colorware.Core.Color {
    public enum CmykComponents {
        C,
        M,
        Y,
        K
    }

    public class CMYK : IEquatable<CMYK> {
        /// <summary>
        /// Value for legacy filter method. This is the method that we used before implementing the
        /// method Techkon uses. This is here for backwards compatiblity as this method can return
        /// different values. This legacy method is activated by setting the local config setting
        /// "Defaults.UseTechkonVisualDensityMethod" to false.
        /// </summary>
        private const double DensityFilterTresholdLegacy = 0.2;

        // Settings for new style (Techkon) filter method.
        private const double DensityFilterTresholdMinimum = 0.03;
        private const double DensityFilterTresholdMaximum = 1.5;
        private const double DensityFilterTresholdFactor = 1.66;
        private static readonly bool useTechkonDensityFilterMethod;

        static CMYK() {
            useTechkonDensityFilterMethod =
                Config.Config.DefaultConfig.GetBool("Defaults.UseTechkonVisualDensityMethod", true);
        }

        // ReSharper disable InconsistentNaming

        public double C { get; }
        public double M { get; }
        public double Y { get; }
        public double K { get; }

        public double ValueOfHighestComponent => GetValueOfHighestComponent();

        public CMYK(double c, double m, double y, double k) {
            C = c;
            M = m;
            Y = y;
            K = k;
        }

        /// <summary>
        /// Builds a <see cref="CMYK"/> instance with <see cref="component"/> set to <see
        /// cref="value"/> and the rest of the components set to 0.
        /// </summary>
        /// <param name="value">The value to set the selected component to.</param>
        /// <param name="component">The component to set, the rest will be 0.</param>
        public CMYK(double value, CmykComponents component) {
            switch (component) {
                case CmykComponents.C:
                    C = value;
                    M = 0;
                    Y = 0;
                    K = 0;
                    break;

                case CmykComponents.M:
                    C = 0;
                    M = value;
                    Y = 0;
                    K = 0;
                    break;

                case CmykComponents.Y:
                    C = 0;
                    M = 0;
                    Y = value;
                    K = 0;
                    break;

                case CmykComponents.K:
                    C = 0;
                    M = 0;
                    Y = 0;
                    K = value;
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(component), component, null);
            }
        }

        [Pure]
        public double GetValueOfHighestComponent() {
            return Math.Max(Math.Max(C, M), Math.Max(Y, K));
        }

        [Pure]
        public double GetValueOfLowestComponent() {
            return Math.Min(Math.Min(C, M), Math.Min(Y, K));
        }

        [Pure]
        public CmykComponents GetHighestComponent() {
            var comp = CmykComponents.C;
            var val = C;
            if (M > val) {
                comp = CmykComponents.M;
                val = M;
            }
            if (Y > val) {
                comp = CmykComponents.Y;
                val = Y;
            }
            if (K > val) {
                comp = CmykComponents.K;
            }
            return comp;
        }

        /// <summary>
        /// Determine the component by using the heuristics provided by Techkon: If (All Densities
        /// less than 0.03D ) VISUAL Else if (All Densities greater than 1.50D ) VISUAL Else if
        /// (Highest Density (CMYK) greater than 1.66 * lowest Density (CMYK)) color of highest
        /// density Else VISUAL Basically, components where all values are too high or too low result
        /// in K. Otherwise, the maximum must be a given factor greater than the minimum.
        /// </summary>
        /// <returns>The component</returns>
        [Pure]
        public CmykComponents GetFilteredComponent() {
            if (useTechkonDensityFilterMethod) {
                // Techkon method.
                var max = Math.Max(Math.Max(Math.Max(C, M), Y), K);
                var min = Math.Min(Math.Min(Math.Min(C, M), Y), K);
                if (max < DensityFilterTresholdMinimum || min > DensityFilterTresholdMaximum)
                    return CmykComponents.K;
                if (max > DensityFilterTresholdFactor * min)
                    return GetHighestComponent();
                return CmykComponents.K;
            }
            else {
                // Legacy method.
                var max = Math.Max(Math.Max(C, M), Y);
                var min = Math.Min(Math.Min(C, M), Y);
                var dist = Math.Abs(max - min);
                var threshold = DensityFilterTresholdLegacy * max;
                if (dist >= threshold)
                    return GetHighestComponent();
                return CmykComponents.K;
            }
        }

        /// <summary>
        /// Returns the value of the filtered component, see GetFilteredComponent
        /// </summary>
        /// <returns>The value for the component</returns>
        [Pure]
        public double GetFilteredValue() {
            if (useTechkonDensityFilterMethod) {
                var max = Math.Max(Math.Max(Math.Max(C, M), Y), K);
                var min = Math.Min(Math.Min(Math.Min(C, M), Y), K);
                if (max < DensityFilterTresholdMinimum || min > DensityFilterTresholdMaximum)
                    return K;
                if (max > DensityFilterTresholdFactor * min)
                    return max;
                return K;
            }
            else {
                var max = Math.Max(Math.Max(C, M), Y);
                var min = Math.Min(Math.Min(C, M), Y);
                var dist = Math.Abs(max - min);
                var threshold = DensityFilterTresholdLegacy * max;
                if (dist >= threshold)
                    return Math.Max(max, K);
                return K;
            }
        }

        /// <summary>
        /// Return the component for the given filter.
        /// </summary>
        /// <param name="cmykComponent">The component to return for.</param>
        /// <returns>The value for the given component.</returns>
        [Pure]
        public double GetFilteredValue(CmykComponents cmykComponent) {
            switch (cmykComponent) {
                case CmykComponents.C:
                    return C;

                case CmykComponents.M:
                    return M;

                case CmykComponents.Y:
                    return Y;
                case CmykComponents.K:
                    return K;

                default:
                    throw new ArgumentOutOfRangeException(nameof(cmykComponent));
            }
        }

        /// <summary>
        /// Calculate the sum of all the components.
        /// </summary>
        /// <returns>The sum of all the components.</returns>
        [Pure]
        public double Sum() {
            return C + M + Y + K;
        }
        #region Overrides of Object
        public override string ToString() {
            return FormattableString.Invariant($"CMYK({C}, {M}, {Y}, {K})");
        }
        public string ToCsvString() {
            var tab = '\t';
            return $"{C}{tab}{M}{tab}{Y}{tab}{K}";
        }
        #endregion Overrides of Object

        public bool Equals(CMYK other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return C.NearlyEquals(other.C) && M.NearlyEquals(other.M) && Y.NearlyEquals(other.Y) &&
                   K.NearlyEquals(other.K);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((CMYK)obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = C.GetHashCode();
                hashCode = (hashCode * 397) ^ M.GetHashCode();
                hashCode = (hashCode * 397) ^ Y.GetHashCode();
                hashCode = (hashCode * 397) ^ K.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(CMYK left, CMYK right) {
            return Equals(left, right);
        }
        public static bool operator !=(CMYK left, CMYK right) {
            return !Equals(left, right);
        }

        #region Operators
        public static CMYK operator -(CMYK first, CMYK second) {
            return new CMYK(first.C - second.C,
                            first.M - second.M,
                            first.Y - second.Y,
                            first.K - second.K);
        }

        public static CMYK operator -(CMYK first, double value) {
            return new CMYK(first.C - value, first.M - value, first.Y - value, first.K - value);
        }

        public static CMYK operator +(CMYK first, CMYK second) {
            return new CMYK(first.C + second.C,
                            first.M + second.M,
                            first.Y + second.Y,
                            first.K + second.K);
        }

        public static CMYK operator +(CMYK first, double value) {
            return new CMYK(first.C + value, first.M + value, first.Y + value, first.K + value);
        }

        public static CMYK operator /(CMYK first, double value) {
            return new CMYK(first.C / value, first.M / value, first.Y / value, first.K / value);
        }
        #endregion Operators

        /// <summary>
        /// Reconstructs a CMYK instance from a single densitometric value using a given sample
        /// reference to decide what component is represented by the single value.
        /// </summary>
        /// <param name="sampleReference"></param>
        /// <param name="densityValue"></param>
        /// <returns></returns>
        public static CMYK BuildFromSingleValue(CMYK sampleReference, double densityValue) {
            var component = sampleReference.GetFilteredComponent();
            return BuildFromSingleValue(component, densityValue);
        }

        public static CMYK BuildFromSingleValue(CmykComponents component, double densityValue) {
            switch (component) {
                case CmykComponents.C:
                    return new CMYK(densityValue, 0, 0, 0);
                case CmykComponents.M:
                    return new CMYK(0, densityValue, 0, 0);
                case CmykComponents.Y:
                    return new CMYK(0, 0, densityValue, 0);
                case CmykComponents.K:
                    return new CMYK(0, 0, 0, densityValue);

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        // TODO PV-359: Test if this works OK
        public static CMYK BuildFromSingleValueByGuessingComponent([NotNull] Spectrum spectrumToGuessFrom,
                                                                   double densityValue) {
            if (spectrumToGuessFrom == null) throw new ArgumentNullException(nameof(spectrumToGuessFrom));

            return BuildFromSingleValue(spectrumToGuessFrom.ToDensities(DensityMethod.StatusE).GetFilteredComponent(),
                                        densityValue);
        }
    }

    public static class CMYKExtensions {
        public static CMYK Sum(this IEnumerable<CMYK> source) {
            return source.Aggregate((x, y) => x + y);
        }

        public static CMYK Sum<T>(this IEnumerable<T> source, Func<T, CMYK> selector) {
            return source.Select(selector).Aggregate((x, y) => x + y);
        }
    }
}