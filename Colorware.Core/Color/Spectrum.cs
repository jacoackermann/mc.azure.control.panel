﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;
using System.Linq;
using System.Text;

using Colorware.Core.Color.Support;
using Colorware.Core.Enums;
using Colorware.Core.Extensions;
using Colorware.Core.Helpers;
using Colorware.Core.Serialization;

using JetBrains.Annotations;

namespace Colorware.Core.Color {
    public class Spectrum {
        public class SpectrumSample {
            public SpectrumSample(double waveLength, double value) {
                WaveLength = waveLength;
                Value = value;
            }

            public double WaveLength { get; }
            public double Value { get; }

            public override string ToString() {
                return $"SpectrumSample({WaveLength}, {Value})";
            }
        }

        public double StartWavelength { get; }
        public double EndWaveLength { get; }

        [ItemNotNull, NotNull]
        public IImmutableList<SpectrumSample> SpectrumSamples { get; }

        [NotNull]
        public IImmutableList<double> ReflectanceValues { get; }

        [NotNull]
        public IImmutableList<double> WavelengthValues { get; }

        /// <remarks>In nm.</remarks>
        public double SampleInterval => (EndWaveLength - StartWavelength) / (SpectrumSamples.Count - 1);

        public Spectrum([NotNull] IEnumerable<double> reflectanceValues, double startWaveLength, double endWaveLength) {
            if (reflectanceValues == null) throw new ArgumentNullException(nameof(reflectanceValues));

            StartWavelength = startWaveLength;
            EndWaveLength = endWaveLength;

            var interval = (EndWaveLength - StartWavelength) / (reflectanceValues.Count() - 1);

            SpectrumSamples = reflectanceValues.Select((v, i) => new SpectrumSample(StartWavelength + i * interval, v))
                                               .ToImmutableList();

            ReflectanceValues = SpectrumSamples.Select(v => v.Value).ToImmutableList();
            WavelengthValues = SpectrumSamples.Select(v => v.WaveLength).ToImmutableList();
        }

        public Spectrum([NotNull] string values, double startWaveLength, double endWaveLength, string seperator)
            : this(parseValueString(values, seperator), startWaveLength, endWaveLength) { }

        public Spectrum(double startWaveLength, double waveLengthInterval, [NotNull] string valueString,
                        [NotNull] string seperator)
            : this(startWaveLength, waveLengthInterval, parseValueString(valueString, seperator)) { }

        public Spectrum(double startWaveLength, double waveLengthInterval, IEnumerable<double> reflectanceValues)
            : this(reflectanceValues, startWaveLength,
                   startWaveLength + (reflectanceValues.Count() - 1) * waveLengthInterval) { }

        private static IEnumerable<double> parseValueString([NotNull] string valueString, [NotNull] string seperator) {
            if (valueString == null) throw new ArgumentNullException(nameof(valueString));
            if (seperator == null) throw new ArgumentNullException(nameof(seperator));

            IEnumerable<double> dValues;
            // Serialized string?
            if (valueString.Length > 0 && valueString[0] == '%') {
                dValues = deserializePackedSpectrum(valueString);
            }
            else if (valueString.Length > 1 && valueString[0] == '$') {
                dValues = SpectrumCipherSerializer.Deserialize(valueString.Substring(1));
            }
            // Deserialized string?
            else {
                var sValues = valueString.Trim().Split(seperator);
                if (sValues.Length <= 0)
                    throw new Exception("Invalid spectral string: " + valueString);

                try {
                    dValues = sValues.Select(v => double.Parse(v, CultureInfo.InvariantCulture)).ToList();
                }
                catch {
                    throw new Exception("Invalid spectral string: " + valueString);
                }
            }

            return dValues;
        }

        public Spectrum([NotNull] IEnumerable<SpectrumSample> sampleValues) : this(
            sampleValues.Select(sv => sv.Value), sampleValues.First().WaveLength, sampleValues.Last().WaveLength) { }

        [Pure]
        [NotNull]
        public Lab ToLab(SpectralMeasurementConditions measurementConditions) {
            return ToXYZ(measurementConditions).AsLab;
        }

        /// <remarks>As in E308-13, 10nm only, using Table 5</remarks>
        [Pure]
        [NotNull]
        public XYZ ToXYZ(SpectralMeasurementConditions measurementConditions) {
            if ((int)SampleInterval != 10)
                throw new InvalidOperationException(
                    $"Can only convert Spectrum to XYZ if sample interval is 10nm! Current interval is {SampleInterval}nm.");

            var weightsTable = TristimulusWeightingFactorsTable5.GetTable(measurementConditions.Illuminant,
                                                                          measurementConditions.Observer);

            var xyz = new XYZ(0, 0, 0, measurementConditions);

            const int step = TristimulusWeightingFactorsTable5.Interval;
            const int tableStart = TristimulusWeightingFactorsTable5.FirstWaveLength;
            const int tableEnd = TristimulusWeightingFactorsTable5.LastWaveLength;

            foreach (var s in SpectrumSamples) {
                TriStimulusWeightingFactor w;
                // First is special
                if ((int)s.WaveLength == (int)StartWavelength)
                    w = weightsTable.Take((int)(((s.WaveLength - tableStart) / step) + 1)).Sum();
                // Last is special
                else if ((int)s.WaveLength == (int)EndWaveLength)
                    w = weightsTable.Reverse().Take((int)(((tableEnd - s.WaveLength) / step) + 1)).Sum();
                // Regular
                else
                    w = weightsTable[((int)s.WaveLength - tableStart) / step];

                xyz += new XYZ(w.Wx * s.Value, w.Wy * s.Value, w.Wz * s.Value, measurementConditions);
            }

            return xyz;
        }

        public string GetStringValue() {
            if (Config.Config.DefaultConfig.GetBool("Options.Defaults.StorePackedSpectrum", true))
                return serializeAsPackedSpectrum(SpectrumSamples);

            var sb = new StringBuilder();
            var i = 0;
            foreach (var entry in SpectrumSamples) {
                if (i > 0)
                    sb.Append(',');
                else
                    i++;

                sb.Append(entry.Value.ToString(CultureInfo.InvariantCulture));
            }

            return sb.ToString();
        }

        private string serializeAsPackedSpectrum(IEnumerable<SpectrumSample> spectrumSamples) {
            var result = SpectrumSerializer.Serialize(spectrumSamples.Select(value => value.Value));
            return "%" + result;
        }

        private static IEnumerable<double> deserializePackedSpectrum(string s) {
            var str = s.Substring(1); // Remove '%' character.
            return SpectrumSerializer.Deserialize(str);
        }

        public string SerializeAsEncryptedSpectrum() {
            return "$" + SpectrumCipherSerializer.Serialize(ReflectanceValues);
        }

        /// <remarks>As in ISO 5-3:2009, B.4 (10nm only)</remarks>
        [Pure, NotNull]
        public CMYK ToDensities(DensityMethod densityMethod) {
            if ((int)SampleInterval != 10)
                throw new InvalidOperationException(
                    $"Can only convert Spectrum to Densities if sample interval is 10nm! Current interval is {SampleInterval}nm.");

            var table = AbridgedDensityWeightingFactorTables.GetTable(densityMethod);
            var visualTable = AbridgedDensityWeightingFactorTables.Visual;

            var densities = new CMYK(0, 0, 0, 0);

            const int step = AbridgedDensityWeightingFactorTables.Interval;
            const int tableStart = AbridgedDensityWeightingFactorTables.FirstWaveLength;
            const int tableEnd = AbridgedDensityWeightingFactorTables.LastWaveLength;

            foreach (var s in SpectrumSamples) {
                AbridgedDensityWeightingFactor w;
                // Independent of DensityMethod
                double visualW;

                // First is special (add all weights before our Spectrum's range to the first weight of our range)
                if ((int)s.WaveLength == (int)StartWavelength) {
                    var numElements = (int)(((s.WaveLength - tableStart) / step) + 1);
                    w = table.Take(numElements).Sum();
                    visualW = visualTable.Take(numElements).Sum();
                }
                // Last is special (add all weights after our Spectrum's range to the first weight of our range)
                else if ((int)s.WaveLength == (int)EndWaveLength) {
                    var numElements = (int)(((tableEnd - s.WaveLength) / step) + 1);
                    w = table.Reverse().Take(numElements).Sum();
                    visualW = visualTable.Reverse().Take(numElements).Sum();
                }
                // Regular
                else {
                    var idx = ((int)s.WaveLength - tableStart) / step;
                    w = table[idx];
                    visualW = visualTable[idx];
                }

                densities += new CMYK(w.Red * s.Value, w.Green * s.Value, w.Blue * s.Value, visualW * s.Value);
            }

            return new CMYK(-Math.Log10(densities.C / 100), -Math.Log10(densities.M / 100),
                            -Math.Log10(densities.Y / 100), -Math.Log10(densities.K / 100));
        }

        [Pure]
        public CMYK ToDensities(DensityMethod densityStandard, [NotNull] Spectrum paperWhite) {
            return ToDensities(densityStandard) - paperWhite.ToDensities(densityStandard);
        }

        public override string ToString() {
            return $"Spectrum(Start={StartWavelength}, End={EndWaveLength}, NumberOfValues={SpectrumSamples.Count})";
        }

        [NotNull]
        public Spectrum ClipToWavelength(double startWL, double endWL) {
            if (startWL.NearlyEquals(StartWavelength) && endWL.NearlyEquals(EndWaveLength))
                return this;

            var newValues = SpectrumSamples.Where(v => v.WaveLength >= startWL && v.WaveLength <= endWL);

            return new Spectrum(newValues);
        }

        [NotNull]
        public static Spectrum InterpolateSpectra([NotNull] Spectrum first, [NotNull] Spectrum second) {
            if (first == null) throw new ArgumentNullException(nameof(first));
            if (second == null) throw new ArgumentNullException(nameof(second));

            return InterpolateSpectra(first, second, 0.5);
        }

        public static Spectrum InterpolateSpectra(Spectrum first, Spectrum second, double ratio) {
            var result =
                first.SpectrumSamples.Select(
                    (v1, i) =>
                        new SpectrumSample(v1.WaveLength,
                                           InterpolationHelper.Lerp(
                                               v1.Value, second.SpectrumSamples[i].Value, ratio)));

            return new Spectrum(result);
        }
    }
}