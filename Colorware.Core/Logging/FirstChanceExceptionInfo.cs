using System;

using JetBrains.Annotations;

namespace Colorware.Core.Logging {
    public class FirstChanceExceptionInfo {
        public FirstChanceExceptionInfo([NotNull] Exception exception, [NotNull] string stackTrace, DateTime timestamp) {
            if (exception == null) throw new ArgumentNullException("exception");
            if (stackTrace == null) throw new ArgumentNullException("stackTrace");

            Exception = exception;
            StackTrace = stackTrace;
            Timestamp = timestamp;
        }

        [NotNull]
        public readonly Exception Exception;

        [NotNull]
        public readonly string StackTrace;

        public readonly DateTime Timestamp;
    }
}