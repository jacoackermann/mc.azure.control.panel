﻿using System;

namespace Colorware.Core.Logging {
    public interface ILog {
        void Debug(string msg, params object[] args);
        void Debug(string msg, Exception e);
        void Info(string msg, params object[] args);
        void Info(string msg, Exception e);
        void Warn(string msg, params object[] args);
        void Warn(string msg, Exception e);
        void Error(string msg, params object[] args);
        void Error(string msg, Exception e);
    }
}