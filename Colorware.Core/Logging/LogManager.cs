using System;

using JetBrains.Annotations;

namespace Colorware.Core.Logging {
    [UsedImplicitly]
    public class LogManager : ILogManager {
        public ILog Get(string name) {
            return GetLogger(name);
        }

        public ILog Get(Type type) {
            return GetLogger(type);
        }

        public static ILog GetLogger(String name) {
            var log = NLog.LogManager.GetLogger(name);
            return wrapLog(log);
        }

        public static ILog GetLogger(Type type) {
            var log = NLog.LogManager.GetLogger(type.Name);
            return wrapLog(log);
        }

        private static ILog wrapLog(NLog.Logger log) {
            return new Log(log);
        }

        public static void Flush() {
            NLog.LogManager.Flush();
        }
    }
}