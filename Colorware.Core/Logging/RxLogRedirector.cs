﻿using System;

using Splat;

namespace Colorware.Core.Logging {
    /// <summary>
    /// Redirects ReactiveUI logging to a specified logger
    /// 
    /// (NLog via Castle Windsor's ILogger in our case)
    /// </summary>
    public class RxLogRedirector : ILogger {
        private readonly Castle.Core.Logging.ILogger log;

        public RxLogRedirector(Castle.Core.Logging.ILogger logger) {
            log = logger;
        }

        public void Write(string message, LogLevel logLevel) {
            switch (logLevel) {
                case LogLevel.Debug:
                    log.Debug(message);
                    break;
                case LogLevel.Info:
#if DEBUG
                    // Rx library seems to log some uninteresting stuff so we
                    // just ignore it in release builds
                    log.Info(message);
#endif
                    break;
                case LogLevel.Warn:
                    log.Warn(message);
                    break;
                case LogLevel.Error:
                    log.Error(message);
                    break;
                case LogLevel.Fatal:
                    log.Fatal(message);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("logLevel");
            }
        }

        // Not relevant for us, we redirect all levels
        public LogLevel Level { get; set; }
    }
}