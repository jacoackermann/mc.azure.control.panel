﻿using System;

using NLog;

namespace Colorware.Core.Logging {
    public class Log : ILog {
        private readonly Logger log;

        public Log(Logger log) {
            this.log = log;
        }

        public void Debug(string msg, params object[] args) {
            log.Debug(msg, args);
        }

        public void Debug(string msg, Exception e) {
            log.Debug(e, msg);
        }

        public void Info(string msg, params object[] args) {
            log.Info(msg, args);
        }

        public void Info(string msg, Exception e) {
            log.Info(e, msg);
        }

        public void Warn(string msg, params object[] args) {
            log.Warn(msg, args);
        }

        public void Warn(string msg, Exception e) {
            log.Warn(e, msg);
        }

        public void Error(string msg, params object[] args) {
            log.Error(msg, args);
        }

        public void Error(string msg, Exception e) {
            log.Error(e, msg);
        }
    }
}