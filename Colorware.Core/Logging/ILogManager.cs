using System;

namespace Colorware.Core.Logging {
    public interface ILogManager {
        ILog Get(string name);
        ILog Get(Type type);
    }
}