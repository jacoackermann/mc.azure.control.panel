﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.Logging {
    public static class FirstChanceExceptionLogger {
        private const string MaxStoredExceptionsConfigKey = "Options.MaxStoredExceptions";

        private static readonly Queue<FirstChanceExceptionInfo> stackTraces = new Queue<FirstChanceExceptionInfo>();

        private static readonly int maxStoredExceptions =
            Config.Config.DefaultConfig.GetInt(MaxStoredExceptionsConfigKey, 6);

        private static readonly object queueLock = new object();

        public static void Log([NotNull] Exception e) {
            if (e == null) throw new ArgumentNullException("e");

            // Occur whenever a task/operation is cancelled, expected
            if ((e is TaskCanceledException || e is OperationCanceledException))
                return;

            // Occurs whenever a web request is cancelled, expected
            var webException = e as WebException;
            if (webException != null && webException.Status == WebExceptionStatus.RequestCanceled)
                return;

            lock (queueLock) {
                // The first stack trace of a first chance exception is the most useful as it is the most deep
                // (a first-chance exception can occur multiple times for the same exception, each time it occurs
                // one level higher in the call stack)
                if (stackTraces.Any(s => s.Exception == e))
                    return;

                if (stackTraces.Count >= maxStoredExceptions)
                    stackTraces.Dequeue();

                stackTraces.Enqueue(new FirstChanceExceptionInfo(e, Environment.StackTrace, DateTime.Now));
            }
        }

        public static IEnumerable<FirstChanceExceptionInfo> GetFirstChanceExceptionsInfo() {
            return stackTraces;
        }
    }
}