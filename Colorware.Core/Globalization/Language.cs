using System;
using System.Collections.Generic;

using Colorware.Core.Helpers;

using JetBrains.Annotations;

namespace Colorware.Core.Globalization {
    public class Language {
        /// <summary>
        /// Maps translation keys to sentences.
        /// </summary>
        private readonly Dictionary<String, String> translations;

        public static readonly Language Empty = new Language(string.Empty, new Dictionary<string, string>());

        public String LanguageId { get; private set; }

        public Language(string languageId, Dictionary<string, string> translations) {
            this.translations = translations;
            LanguageId = languageId;
        }

        [NotNull]
        public String Translate([NotNull] String key) {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentException("Translation key must not be empty or null!");

            String ret;
            if (translations.ContainsKey(key)) {
                ret = StringUtils.ParseEscapeCharacters(translations[key]);
                if (string.IsNullOrEmpty(ret))
                    ret = key;
            }
            else
                ret = key;

            return ret;
        }
    }
}