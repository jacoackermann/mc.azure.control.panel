﻿using System;

using JetBrains.Annotations;

namespace Colorware.Core.Globalization {
    [AttributeUsage(AttributeTargets.Field)]
    public class EnumLanguage : Attribute {
        /// <summary>
        /// Specifies the key to use for translations.
        /// </summary>
        [NotNull]
        public string TranslationKey { get; }

        /// <summary>
        /// Specifies a backup to use if the translation key cannot be used to infer the proper translation.
        /// </summary>
        /// <remarks>This is currently used for the server where translation files are not available.</remarks>
        [CanBeNull]
        public string DefaultTranslation { get; }

        public EnumLanguage([NotNull] string translationKey) {
            DefaultTranslation = null;
            TranslationKey = translationKey;
        }

        /// <param name="translationKey">Translation key to find the proper translation if available.</param>
        /// <param name="defaultTranslation">
        /// When no translation can be found for the given key, use this value instead.
        /// </param>
        public EnumLanguage([NotNull] string translationKey, [NotNull] string defaultTranslation) {
            TranslationKey = translationKey ?? throw new ArgumentNullException(nameof(translationKey));
            DefaultTranslation = defaultTranslation ?? throw new ArgumentNullException(nameof(defaultTranslation));
        }
    }
}