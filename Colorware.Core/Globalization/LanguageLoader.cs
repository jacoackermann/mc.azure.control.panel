using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Xml;

using Colorware.Core.Logging;

using JetBrains.Annotations;

namespace Colorware.Core.Globalization {
    public static class LanguageLoader {
        private const String TranslationsDir = "Translations";

        private static String langDir {
            get {
                var currentDir = getCurrentDirectory();
                return Path.Combine(currentDir, TranslationsDir);
            }
        }

        public static Language Load(String languageId) {
            LogManager.GetLogger(typeof(LanguageLoader)).Info("Loading language: " + languageId);
            return loadFromDirectory(languageId, langDir);
        }

        private static String getCurrentDirectory() {
            var assembly = Assembly.GetEntryAssembly() ?? Assembly.GetExecutingAssembly();
            return Path.GetDirectoryName(assembly.Location);
        }

        private static Language loadFromDirectory(string languageId, string langDir) {
            if (!Directory.Exists(langDir)) {
                LogManager.GetLogger(typeof(LanguageLoader)).Error("No 'translations' directory exists");
                return new Language("No languages available",
                    new Dictionary<string, string>());
            }

            // Naming convention is enforced by translation system export so we can make the optimization
            // to only find names containing the language ID
            var files = Directory.GetFiles(langDir, "*" + languageId + "*.xml", SearchOption.AllDirectories);

            Array.Sort(files);

            var translations = new Dictionary<String, String>();
            foreach (var filename in files)
                loadFromFile(languageId, filename, translations);

            var language = new Language(languageId, translations);
            return language;
        }

        private static void loadFromFile(string languageId, string filename, Dictionary<string, string> translations) {
            try {
                var settings = new XmlReaderSettings {
                    IgnoreWhitespace = false,
                    IgnoreComments = true,
                    IgnoreProcessingInstructions = true
                };
                var foundLanguageDirective = false;
                using (var reader = XmlReader.Create(filename, settings)) {
                    if (!reader.Read())
                        return;
                    while (true) {
                        if (reader.NodeType != XmlNodeType.Element) {
                            if (!reader.Read()) return;
                            continue;
                        }
                        if (reader.Name == "Properties") {
                            foundLanguageDirective = true;
                            var id = reader.GetAttribute("Language");
                            if (id == null || id != languageId)
                                return;
                            if (!reader.Read()) return;
                            continue;
                        }
                        if (!foundLanguageDirective)
                            continue;
                        if (reader.Name == "Entry") {
                            var id = reader.GetAttribute("Id");
                            if (id == null) {
                                if (!reader.Read()) return;
                                continue;
                            }
                            var value = reader.ReadElementContentAsString();
                            translations[id] = value;

                            continue;
                        }
                        if (!reader.Read()) return;
                    }
                }
            }
            catch (Exception e) {
                LogManager.GetLogger(typeof(LanguageLoader)).Error(
                    "Couldn't read language file '" + filename + "' while trying to load language '" + languageId, e);
                MessageBox.Show(
                    "Error while reading language file: '" + filename + "'\nSome translations may not be available\n\n" +
                    e, "Language file error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        [NotNull]
        public static IReadOnlyList<string> GetAvailableLanguages() {
            var languages = new HashSet<string>();

            if (!Directory.Exists(langDir)) {
                LogManager.GetLogger(typeof(LanguageLoader)).Error("No 'translations' directory exists");
                return languages.ToList();
            }

            var files = Directory.GetFiles(langDir, "*.xml", SearchOption.AllDirectories);
            Array.Sort(files);

            foreach (var filename in files)
                try {
                    var settings = new XmlReaderSettings {
                        IgnoreWhitespace = true,
                        IgnoreComments = true,
                        IgnoreProcessingInstructions = true
                    };

                    using (var reader = XmlReader.Create(filename, settings)) {
                        while (reader.Read()) {
                            if (reader.NodeType != XmlNodeType.Element)
                                continue;

                            if (reader.Name == "Properties") {
                                var lang = reader.GetAttribute("Language");
                                if (lang != null) {
                                    languages.Add(lang);
                                    break;
                                }
                            }
                        }
                    }
                }
                catch (Exception e) {
                    LogManager.GetLogger(typeof(LanguageLoader)).Error(
                        "Couldn't read language file '" + filename + "' while trying to get available languages.", e);
                    MessageBox.Show(
                        "Error while reading language file: '" + filename +
                        "\n\n" +
                        e, "Language file error", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            return languages.ToList();
        }
    }
}