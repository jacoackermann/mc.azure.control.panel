﻿using System;
using System.Collections.Generic;

namespace Colorware.Core.Globalization {
    public interface ILanguageEntryOverrider {
        void ClearOverrides();
        void Override(string key, string value);
    }

    public static class LanguageEntryOverriderExtensions {
        public static void Override(this ILanguageEntryOverrider overrider, IEnumerable<Tuple<string, string>> entries) {
            foreach (var entry in entries) {
                overrider.Override(entry.Item1, entry.Item2);
            }
        }
    }
}