﻿using System;
using System.Collections.Generic;

using JetBrains.Annotations;

namespace Colorware.Core.Globalization {
    /// <summary>
    /// Wraps <see cref="LanguageManager"/> for backwards compatibility reasons.
    /// The current <see cref="ILanguageManager"/> interface defines an instance <see cref="ILanguageManager._"/> method, while
    /// <see cref="LanguageManager"/> defines a static <see cref="LanguageManager._"/> method. Whenever all references to the static version are
    /// removed, it should be save to refactor the current <see cref="LanguageManager"/> instance and remove this wrapper class.
    /// </summary>
    [UsedImplicitly]
    public class LanguageManagerWrapper : ILanguageManager, ILanguageEntryOverrider {
        public IReadOnlyList<string> AvailableLanguages {
            get { return LanguageManager.Current.AvailableLanguages; }
        }

        public void Load(string languageId) {
            LanguageManager.Current.Load(languageId);
        }

        public string _(string key) {
            return LanguageManager._(key);
        }

        public string _F(String key, params object[] args) {
            return LanguageManager._F(key, args);
        }

        public void ClearOverrides() {
            LanguageManager.Current?.ClearOverrides();
        }

        public void Override(string key, string value) {
            LanguageManager.Current?.Override(key, value);
        }
    }
}