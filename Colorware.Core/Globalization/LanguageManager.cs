using System;
using System.Collections.Generic;

using JetBrains.Annotations;

namespace Colorware.Core.Globalization {
    public class LanguageManager {
        private const string DefaultLang = "en-US";

        private static LanguageManager current;

        /// <summary>
        /// Singleton instance.
        /// </summary>
        public static LanguageManager Current {
            get { return current ?? (current = new LanguageManager()); }
        }

        /// <summary>
        /// Instance of the currently active language.
        /// </summary>
        public Language CurrentLanguage { get; set; }

        private Language defaultLanguage;

        /// <summary>
        /// Provides the functionality to use global overrides for specific language settings.
        /// </summary>
        private Dictionary<string, string> overrides = new Dictionary<string, string>();

        /// <summary>
        /// Gets the default language (en-US), used mainly as a fallback when no translation is
        /// for some key.
        /// </summary>
        public Language DefaultLanguage {
            get { return defaultLanguage ?? (defaultLanguage = LanguageLoader.Load(DefaultLang)); }
            set { defaultLanguage = value; }
        }

        public void Load(string languageId) {
            CurrentLanguage = LanguageLoader.Load(languageId);

            if (languageId == DefaultLang)
                DefaultLanguage = CurrentLanguage;
        }

        /// <summary>
        /// Clear all current language entry overrides.
        /// </summary>
        public void ClearOverrides() {
            overrides.Clear();
        }

        /// <summary>
        /// Set an override for the specific key. From now on, any requests for the given key will be
        /// overridden by <see cref="value"/>.
        /// </summary>
        /// <param name="key">The key to provide the override for.</param>
        /// <param name="value">The value to override with.</param>
        public void Override([NotNull] string key, [NotNull] string value) {
            if (key == null) throw new ArgumentNullException(nameof(key));
            if (value == null) throw new ArgumentNullException(nameof(value));
            overrides[key.ToLower()] = value;
        }

        /// <summary>
        /// Helper function to cut back on typing. Underscore syntax inspired by gettext.
        /// </summary>
        /// <param name="key">The text key for the translation.</param>
        /// <returns>The translated value (or the default if the key was not present).</returns>
        [NotNull]
        public static string _([NotNull] string key) {
            if (key == null) throw new ArgumentNullException(nameof(key));

            if (Current == null) {
                return key;
            }
            // Check for an available override.
            if (Current.overrides.TryGetValue(key.ToLowerInvariant(), out var overrideValue)) {
                return overrideValue;
            }
            if (Current.CurrentLanguage == null) {
                return key;
            }
            var trans = Current.CurrentLanguage.Translate(key);
            // Try to get the translation from the default language (en-US) if the current language
            // has no translation for the given key
            if (trans.Equals(key) &&
                Config.Config.DefaultConfig.GetBool("Options.Translation.UseFallbackLanguage", true))
                return Current.DefaultLanguage.Translate(key);
            else
                return trans;
        }

        /// <summary>
        /// As <see cref="_"/>, but adds formatting parameters.
        /// </summary>
        /// <param name="key">The text key for the translation.</param>
        /// <param name="args">The arguments for the <see cref="string.Format(string,object)"/> function.</param>
        /// <returns>The translated value with the arguments spliced in using string formatting.</returns>
        public static string _F(string key, params object[] args) {
            return string.Format(_(key), args);
        }

        /// <summary>
        /// Helper function to translate an enum member which is tagged by an
        /// EnumLanguage attribute.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string TranslateEnum(object item) {
            var fieldInfo = item.GetType().GetField(item.ToString());
            if (fieldInfo == null)
                return "???";
            var attributes = (EnumLanguage[])fieldInfo.GetCustomAttributes(typeof(EnumLanguage), true);
            // value.GetType().GetCustomAttributes(typeof(EnumLanguage), true);
            if (attributes.Length <= 0)
                return "???";
            return _(attributes[0].TranslationKey);
        }

        private IReadOnlyList<string> availableLanguages;

        [NotNull]
        public IReadOnlyList<string> AvailableLanguages {
            get { return availableLanguages ?? (availableLanguages = LanguageLoader.GetAvailableLanguages()); }
        }
    }
}