using System.Collections.Generic;

using JetBrains.Annotations;

namespace Colorware.Core.Globalization {
    public interface ILanguageManager {
        IReadOnlyList<string> AvailableLanguages { get; }
        void Load(string languageId);

        [NotNull, Pure]
        string _([NotNull] string key);
        [Pure]
        string _F(string key, params object[] args);
    }
}