﻿using System;
using System.Collections.Generic;

using Colorware.Core.Config;
using Colorware.Core.Functional.Option;
using Colorware.Core.Plugins;
using Colorware.Core.Plugins.Devices;
using Colorware.Core.Plugins.Modules;

using JetBrains.Annotations;

namespace Colorware.Core {
    /// <summary>
    /// Define standard keys used for user profiles. These are defined here so they are accessible
    /// from within the HTML management area and other places.
    /// </summary>
    public static class UserProfileDefinitions {
        /// <summary>
        /// Determines which module should be auto-started when set.
        /// </summary>
        public static readonly StringConfigItem PreferredStartupModule =
            new StringConfigItem("Options.PreferredModule", string.Empty);

        /// <summary>
        /// Define access to modules.
        /// </summary>
        public static class Modules {
            /// <summary>
            /// This 'namespace' is used to make sure all the module access keys are the same. To make this more general while checking,
            /// the remainder of the keys should be the classname of the module!
            /// </summary>
            private const string SectionKeyNamespace = "Modules.Enabled.";

            /// <summary>
            /// Helper function to create a namespace for a given module name.
            /// </summary>
            private static string ns(string moduleId) {
                return SectionKeyNamespace + moduleId;
            }

            public static string Ns([NotNull] IModule module) {
                if (module == null) throw new ArgumentNullException(nameof(module));

                return ns(module.GetId());
            }

            /// <param name="moduleType">NB: Must implement IModule!</param>
            public static string Ns([NotNull] Type moduleType) {
                if (moduleType == null) throw new ArgumentNullException(nameof(moduleType));

                if (!typeof(IModule).IsAssignableFrom(moduleType)) {
                    throw new ArgumentException(
                        $"{nameof(moduleType)} must implement IModule to create a Module config namespace for it!");
                }

                return ns(moduleType.GetComponentId());
            }

            // NB: these strings must match the ComponentId strings set on the Module classes

            public static readonly BoolConfigItem ProcessControl =
                new BoolConfigItem(ns("PCModule"), true);

            public static readonly BoolConfigItem MultiSpot =
                new BoolConfigItem(ns("MSModule"), true);

            public static readonly BoolConfigItem AutoImportModule =
                new BoolConfigItem(ns("AIModule"), true);

            public static readonly BoolConfigItem ColorbarEditorModule =
                new BoolConfigItem(ns("CEModule"), true);

            public static readonly BoolConfigItem ImageModule =
                new BoolConfigItem(ns("ImageModule"), true);

            public static readonly BoolConfigItem PantoneLiveModule =
                new BoolConfigItem(ns("PLModule"), true);

            public static readonly BoolConfigItem PrintabilityModule =
                new BoolConfigItem(ns("PModule"), true);

            public static readonly BoolConfigItem ProofCertificationModule =
                new BoolConfigItem(ns("ProofCertModule"), true);

            public static readonly BoolConfigItem ReferenceManagerModule =
                new BoolConfigItem(ns("RMModule"), true);

            public static readonly BoolConfigItem SpotColorToolModule =
                new BoolConfigItem(ns("SCTModule"), true);

            public static readonly BoolConfigItem FlexiblesModule =
                new BoolConfigItem(ns("FlexiblesModule"), true);

            public static readonly BoolConfigItem RemoteSupportModule =
                new BoolConfigItem(ns("SOSModule"), true);

            public static readonly BoolConfigItem SystemManagementModule =
                new BoolConfigItem(ns("ManagementSiteModule"), true);
        }

        /// <summary>
        /// Defines screen names for Process Control Module.
        /// </summary>
        public static class ProcessControlScreens {
            /// <summary>
            /// This 'namespace' is used to make sure all the module access keys are the same. To make this more general while checking,
            /// the remainder of the keys should be the classname of the module!
            /// </summary>
            private const string SectionKeyNamespace = "ProcessControl.Screens.";

            /// <summary>
            /// Helper function to create a namespace for a given module name.
            /// </summary>
            private static string ns(string moduleName) {
                return SectionKeyNamespace + moduleName;
            }

            public static readonly BoolConfigItem InkZoneOverviewScreen =
                new BoolConfigItem(ns("InkZoneOverview"), true);

            public static readonly BoolConfigItem BalanceResultScreen =
                new BoolConfigItem(ns("BalanceResultScreen"), true);

            public static readonly BoolConfigItem DigitalPrintScreen =
                new BoolConfigItem(ns("DigitalPrintScreen"), true);

            public static readonly BoolConfigItem ProcessScreen =
                new BoolConfigItem(ns("ProcessScreen"), true);

            public static readonly BoolConfigItem FlexoResultScreen =
                new BoolConfigItem(ns("FlexoResult"), true);

            public static readonly BoolConfigItem GamutResultScreen =
                new BoolConfigItem(ns("GamutResultSubClass"), true);

            public static readonly BoolConfigItem DotgainOverviewScreen =
                new BoolConfigItem(ns("DotgainOverviewScreen"), true);

            public static readonly BoolConfigItem CtpCurveAnalysisScreen =
                new BoolConfigItem(ns("CtpCurveAnalysisScreen"), true);

            public static readonly BoolConfigItem RawListScreen =
                new BoolConfigItem(ns("RawListScreen"), true);

            public static readonly BoolConfigItem SpotAnalysisScreen =
                new BoolConfigItem(ns("SpotAnalysisScreen"), true);

            public static readonly BoolConfigItem StatisticsScreen =
                new BoolConfigItem(ns("Statistics"), true);

            public static readonly BoolConfigItem G7AnalysisScreen =
                new BoolConfigItem(ns("G7AnalysisScreen"), true);

            public static readonly BoolConfigItem OpacityMeasurementScreen =
                new BoolConfigItem(ns("OpacityMeasurement"), true);

            public static readonly BoolConfigItem CustomMeasurementFieldsScreen =
                new BoolConfigItem(ns("CustomMeasurementFields"), true);

            public static readonly BoolConfigItem HeaderSummaryButton =
                new BoolConfigItem(ns("HeaderSummaryForProcessControl"), true);

            public static readonly BoolConfigItem ToggleSheetSideButton =
                new BoolConfigItem(ns("ToggleSheetSideButton"), true);

            public static readonly BoolConfigItem ToggleOkSheetButton =
                new BoolConfigItem(ns("ToggleOkSheetButton"), true);

            public static readonly BoolConfigItem ToggleProductionModeButton =
                new BoolConfigItem(ns("ToggleProductionModeButton"), true);

            public static readonly BoolConfigItem ResultWrenchButton =
                new BoolConfigItem(ns("ResultWrench"), true);

            public static readonly BoolConfigItem HeaderScoreButton =
                new BoolConfigItem(ns("ProcessControlHeaderScore"), true);

            public static readonly BoolConfigItem ImportMeasurementButton =
                new BoolConfigItem(ns("ImportMeasurementButton"), true);

            public static readonly BoolConfigItem DensitiesButton =
                new BoolConfigItem(ns("DensitiesButton"), true);

            public static readonly BoolConfigItem ExportButton =
                new BoolConfigItem(ns("ExportButton"), true);

            //Options.Measurement.ForceRemarks
            public static readonly BoolConfigItem ForceRemarks =
                new BoolConfigItem("Options.Measurement.ForceRemarks", false);

            public static readonly BoolConfigItem CustomReferenceButton =
                new BoolConfigItem(ns("CustomReference"), true);
        }

        /// <summary>
        /// Stuff that's common across process type modules.
        /// </summary>
        public static class Process {
            /// <summary>
            /// This 'namespace' is used to make sure all the module access keys are the same. To make this more general while checking,
            /// the remainder of the keys should be the classname of the module!
            /// </summary>
            private const string SectionKeyNamespace = "Options.Process.";

            /// <summary>
            /// Helper function to create a namespace for a given module name.
            /// </summary>
            private static string ns(string moduleName) {
                return SectionKeyNamespace + moduleName;
            }

            public static readonly BoolConfigItem GoToProductionAfterSettingOkSheet =
                new BoolConfigItem(ns("GoToProductionAfterSettingOkSheet"), true);
        }

        /// <summary>
        /// Access to various elements and functions.
        /// </summary>
        public static class ElementAccess {
            private const string SectionKeyNamespace = "Access.";

            /// <summary>
            /// Helper function to create a namespace for a given module name.
            /// </summary>
            private static string ns(string section, string action) {
                return SectionKeyNamespace + section + "." + action;
            }

            public static BoolConfigItem AddClient { get; } = new BoolConfigItem(ns("Client", "Add"), true);

            public static BoolConfigItem AddJob { get; } = new BoolConfigItem(ns("Job", "Add"), true);

            public static BoolConfigItem DuplicateJob { get; } = new BoolConfigItem(ns("Job", "Duplicate"), true);

            public static BoolConfigItem SettingsJob { get; } = new BoolConfigItem(ns("Job", "Settings"), true);

            public static BoolConfigItem DeleteJob { get; } = new BoolConfigItem(ns("Job", "DeleteJob"), true);

            // Determins global available functions (Create, Update, Delete) for job presets.

            public static BoolConfigItem CrudJobPresets { get; } = new BoolConfigItem(
                ns("JobSetup", "ModifyJobPresets"), true);

            /// <remarks>This is kind of problematic for now, so this is not implemented further at this time (2015-06-25).</remarks>
            /*
            private readonly static BoolConfigItem addMeasurement = new BoolConfigItem(Ns("Measurement", "Add"), true);
            public static BoolConfigItem AddMeasurement { get { return addMeasurement; } }
            */
            public static BoolConfigItem DeleteMeasurement { get; } =
                new BoolConfigItem(ns("Measurement", "Delete"), true);

            public static BoolConfigItem DeleteSequenceSample { get; } =
                new BoolConfigItem(ns("Measurement", "DeleteSample"), true);

            public static BoolConfigItem SaveMeasurementAsReference { get; } =
                new BoolConfigItem(ns("Measurement", "SaveAsReference"), true);

            public static BoolConfigItem AddPreset { get; } = new BoolConfigItem(ns("Preset", "Add"), true);

            /// <remarks>This is kind of problematic for now, so this is not implemented further at this time (2015-06-25).</remarks>
            /*
            private readonly static BoolConfigItem overwritePreset = new BoolConfigItem(Ns("Preset", "Overwrite"), true);
            public static BoolConfigItem OverwritePreset { get { return overwritePreset; } }
            */

            // Add and edit are combined, because it makes no sense to be able to create a reference and then not being allowed
            // to edit it (except maybe when importing, but we ignore that for now)
            public static BoolConfigItem AddAndEditReference { get; } = new BoolConfigItem(
                ns("Reference", "AddAndEdit"), true);

            public static BoolConfigItem DeleteReference { get; } = new BoolConfigItem(ns("Reference", "Delete"), true);

            // Add and edit are combined, because it makes no sense to be able to create a printing condition and then not being allowed
            // to edit it

            public static BoolConfigItem AddAndEditPrintingCondition { get; } =
                new BoolConfigItem(ns("PrintingCondition", "AddAndEdit"), true);

            public static BoolConfigItem DeletePrintingCondition { get; } =
                new BoolConfigItem(ns("PrintingCondition", "Delete"), true);


            public static BoolConfigItem EnableSecondaryButtonCommands { get; } =
                new BoolConfigItem(ns("ClickAndHold", "Use"), true);
        }

        /// <summary>
        /// Defines profile settings related to process control job setup.
        /// </summary>
        public static class ProcessControlJobSetup {
            public enum JobSetupWorkflowType {
                Simple,
                Standard,
                Advanced
            }

            /// <summary>
            /// This 'namespace' is used to make sure all the module access keys are the same. To make this more general while checking,
            /// the remainder of the keys should be the classname of the module!
            /// </summary>
            private const string SectionKeyNamespace = "ProcessControl.JobSetup.";

            /// <summary>
            /// Helper function to create a namespace for a given module name.
            /// </summary>
            private static string ns(string itemName) {
                return SectionKeyNamespace + itemName;
            }

            public static EnumConfigItem<JobSetupWorkflowType> WorkflowType { get; } =
                new EnumConfigItem<JobSetupWorkflowType>(ns("WorkflowType"), JobSetupWorkflowType.Advanced);
        }

        public static class Devices {
            private const bool DefaultActiveStatus = true;
            private const string SectionKeyNamespace = "Devices.";

            private static string ns(string itemName) {
                return SectionKeyNamespace + itemName;
            }

            private static string makeDeviceIsActiveKey([NotNull] string id) {
                if (id == null) throw new ArgumentNullException(nameof(id));

                return ns($"IsActive.{id}");
            }

            /// <param name="deviceType">NB: Must inherit from IDevice.</param>
            /// <returns></returns>
            public static bool GetActiveStatusForDevice([NotNull] Type deviceType) {
                if (deviceType == null) throw new ArgumentNullException(nameof(deviceType));

                if (!typeof(IDevice).IsAssignableFrom(deviceType))
                    throw new ArgumentException($"Type must implement {nameof(IDevice)} to get its active status!");

                var deviceId = getOldDeviceId(deviceType).OrElse(deviceType.GetComponentId());

                return new BoolConfigItem(makeDeviceIsActiveKey(deviceId), DefaultActiveStatus).Value;
            }

            /// <param name="deviceType">NB: Must inherit from IDevice.</param>
            public static void SetActiveStatusForDevice([NotNull] Type deviceType, bool status) {
                if (deviceType == null) throw new ArgumentNullException(nameof(deviceType));

                if (!typeof(IDevice).IsAssignableFrom(deviceType))
                    throw new ArgumentException($"Type must implement {nameof(IDevice)} to set its active status!");

                var deviceId = getOldDeviceId(deviceType).OrElse(deviceType.GetComponentId());

                var item = new BoolConfigItem(makeDeviceIsActiveKey(deviceId), DefaultActiveStatus);
                item.Value = status;
            }

            private static Option<string> getOldDeviceId([NotNull] Type deviceType) {
                if (!typeof(IDevice).IsAssignableFrom(deviceType))
                    throw new ArgumentException($"Type must implement {nameof(IDevice)} to get the old style ID!");

                // NB: Strings on the left must match the ComponentId names on the Device classes
                var newToOldIdMappings = new Dictionary<string, string> {
                    {"Exact-2017-1", "ExactDevice"},
                    {"i1Pro-2017-1", "EyeOneDevice"},
                    {"FiveHundred-2017-1", "FiveHundredDevice"},
                    // There was also an Itx2DevicePlugin, but we can't map 2 devices to one in this direction
                    // so any old ITX2 key is in effect ignored
                    {"IntelliTrax-2017-1", "ItxDevice"},
                    {"SpectroEye-2017-1", "SpectroEyeDevice"},
                    {"SpectroDens-2017-1", "SpectroDensDevice"},
                    {"SpectroDrive-2017-1", "SpectroDriveDevice"},
                    {"SpectroJet-2017-1", "SpectroJetDevice"},
                };

                return newToOldIdMappings.TryGetValue(deviceType.GetComponentId(), out string oldId)
                           ? oldId.ToOption()
                           : Option<string>.Nothing;
            }
        }

        /// <summary>
        /// Defines profile settings related to Multi Spot option screens.
        /// </summary>
        public static class OptionsMultiSpot {
            private const string SectionKeyNamespace = "Options.MultiSpot.";

            /// <summary>
            /// Helper function to create a namespace for a given module name.
            /// </summary>
            private static string ns(string itemName) {
                return SectionKeyNamespace + itemName;
            }

            public static readonly BoolConfigItem AutoStartNewSequence =
                new BoolConfigItem(ns("AutoStartNewSequence"), true);
        }
    }
}