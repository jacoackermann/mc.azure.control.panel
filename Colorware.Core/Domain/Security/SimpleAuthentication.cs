﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Colorware.Core.Domain.Security
{
    /// <summary>
    /// Represents a way of encoding a username/password pair.
    ///
    /// The password uses a <see cref="SecureString"/> instance to make reading the password form
    /// memory more difficult. Note that this will only provide the maximum amount of security if the
    /// password string itself was never entered into memory in the first place. As such it would be
    /// safest to create a <see cref="SecureString"/> instance from code and use AppendChar from a
    /// char feeding source (such as the commandline) and never convert the whole password into a string.
    ///
    /// The class implements <see cref="IDisposable"/> which can be used to force the disposal of the
    /// password after you are done with it.
    /// </summary>
    public class SimpleAuthentication : IDisposable {

        /// <summary>
        /// Username for a given user.
        /// </summary>
        public string Username { get; }

        /// <summary>
        /// (Unencrypted)
        /// </summary>
        public SecureString Password { get; }

        /// <summary>
        /// Provides a username and password as strings. Note that there is inherently an insecure
        /// component to this. The password is converted into a <see cref="SecureString"/>, but the
        /// string that it is created from is insecure and can still be read if it remains in memory.
        /// </summary>
        public SimpleAuthentication(string username, string password) {
            Username = username;
            Password = new SecureString();
            foreach (var c in password)
                Password.AppendChar(c);
            Password.MakeReadOnly();
        }

        /// <summary>
        /// Provides a <see cref="SecureString"/> directly. Note that the password is made readonly
        /// and can not be subsequently modified after this instance has been created.
        /// </summary>
        public SimpleAuthentication(string username, SecureString password) {
            Username = username;
            Password = password;
            if (!Password.IsReadOnly())
                Password.MakeReadOnly();
        }

        public string GetUnencryptedPassword() {
            var bstr = Marshal.SecureStringToBSTR(Password);
            try {
                return Marshal.PtrToStringBSTR(bstr);
            }
            finally {
                Marshal.FreeBSTR(bstr);
            }
        }

        public void Dispose() {
            Password.Dispose();
        }
    }
}