namespace Colorware.Core.Service {
    public interface IWebApiResult {
        bool Error { get; }
    }
}