﻿using System;
using System.IO;

using JetBrains.Annotations;

namespace Colorware.Core.Service {
    public interface IICCFileService {
        Guid SendICCFile([NotNull] MemoryStream file);

        [CanBeNull]
        MemoryStream GetICCFile(Guid guid);
    }
}