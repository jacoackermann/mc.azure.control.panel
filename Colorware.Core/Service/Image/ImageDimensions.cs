namespace Colorware.Core.Service.Image {
    public class ImageDimensions {
        public ImageDimensions(int width, int height) {
            Width = width;
            Height = height;
        }

        public int Width { get; private set; }

        public int Height { get; private set; }

        public double GetPercentualTargetHeight(int targetY) {
            if (Height > 0) {
                return targetY / (double)Height;
            }
            return 0.0;
        }

        public double GetPercentualTargetWidth(int targetX) {
            if (Width > 0) {
                return targetX / (double)Width;
            }
            return 0.0;
        }
    }
}