﻿using System;
using System.Drawing;
using System.IO;

using Colorware.Core.Enums;

using JetBrains.Annotations;

namespace Colorware.Core.Service.Image {
    public interface IImageService {
        string GetStoragePath(Guid guid, ImageSize imageSize);

        Guid SendImage([NotNull] Bitmap bitmap);

        [CanBeNull]
        Bitmap GetImage(Guid guid, ImageSize imageSize);

        [CanBeNull]
        Stream GetImageStream(Guid guid, ImageSize imageSize);

        [NotNull]
        ImageDimensions GetImageDimensions(Guid guid, ImageSize imageSize);
    }
}