﻿using System;

namespace Colorware.Core.Authentication {
    public interface IReadOnlyUser {
        long Id { get; }
        string Username { get; }
        string Password { get; }
        bool IsAdmin { get; }
        /// <summary>
        /// A remote client is currently defined as a user with limited access that is only allowed to measure
        /// jobs and has limited other permissions.
        /// </summary>
        bool IsRemoteClient { get; }
        long CompanyId { get; }
        long UserGroupId { get; }
        string CompanyAddress { get; }
        string CompanyName { get; }
        string CompanyCity { get; }
        string CompanyTelephone { get; }

        Guid ForcedProductGuid { get; }

        /// <summary>
        /// Indicates whether remote clients are active on the server.
        /// TODO: Should probably move this somewhere more appropriate.
        /// </summary>
        bool RemoteClientsEnabled { get; }
        string CustomReportsUrl { get; }

    }
}