﻿using System;

namespace Colorware.Core.Authentication {
    public interface IUser : IReadOnlyUser {
        new long Id { get; set; }
        new string Username { get; set; }
        new string Password { get; set; }
        new bool IsAdmin { get; set; }
        new long CompanyId { get; set; }
        new long UserGroupId { get; set; }
        new string CompanyAddress { get; set; }
        new string CompanyName { get; set; }
        new string CompanyCity { get; set; }
        new string CompanyTelephone { get; set; }
        new Guid ForcedProductGuid { get; set; }
        new string CustomReportsUrl { get; set; }
    }
}