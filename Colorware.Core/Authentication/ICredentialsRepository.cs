﻿using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.Authentication {
    public interface ICredentialsRepository {
        [NotNull, ItemCanBeNull]
        Task<ICredentials> GetCredentials();
    }
}