﻿using System;

namespace Colorware.Core.Authentication {
    public class LoginEventArgs : EventArgs {
        public ICredentials Credentials { get; private set; }

        public LoginEventArgs(ICredentials credentials) {
            Credentials = credentials;
        }
    }
}