﻿using System.Threading.Tasks;

namespace Colorware.Core.Authentication {
    /// <summary>
    /// Handler interface. All instances will be called when user logs out of the system.
    /// </summary>
    public interface ILogoutHandler {
        Task OnLogout();
    }
}