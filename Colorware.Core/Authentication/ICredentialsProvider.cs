﻿using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.Authentication {
    public interface ICredentialsProvider {
        [NotNull, ItemCanBeNull]
        Task<ICredentials> GetCredentials();
    }
}