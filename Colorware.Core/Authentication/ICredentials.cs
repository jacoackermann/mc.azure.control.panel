﻿namespace Colorware.Core.Authentication {
    public interface ICredentials {
        string Username { get; }
        string Password { get; }
        IUser User { get; }
    }
}