using System;
using System.Collections.Generic;

using Colorware.Core.Color;

using JetBrains.Annotations;

namespace Colorware.Core.Algorithms.PatchRecognition {
    internal class OverprintGuess {
        public OverprintGuess([NotNull] Spectrum overprint, [NotNull] IEnumerable<Spectrum> parentSolids) {
            if (overprint == null) throw new ArgumentNullException("overprint");
            if (parentSolids == null) throw new ArgumentNullException("parentSolids");

            Overprint = overprint;
            ParentSolids = parentSolids;
        }

        [NotNull]
        public Spectrum Overprint { get; private set; }

        [NotNull]
        public IEnumerable<Spectrum> ParentSolids { get; private set; }
    }
}