using System;
using System.Diagnostics;

using Colorware.Core.Color;

using JetBrains.Annotations;

namespace Colorware.Core.Algorithms.PatchRecognition {
    [DebuggerDisplay("Tint={Tint}, Patch = {Patch}")]
    internal class TintPatchGuess {
        public TintPatchGuess([NotNull] Spectrum solid, [NotNull] Spectrum patch, double tint) {
            if (solid == null) throw new ArgumentNullException("solid");
            if (patch == null) throw new ArgumentNullException("patch");

            Solid = solid;
            TintPatch = patch;
            Tint = tint;
        }

        public Spectrum Solid { get; private set; }

        [NotNull]
        public Spectrum TintPatch { get; private set; }

        public double Tint { get; private set; }
    }
}