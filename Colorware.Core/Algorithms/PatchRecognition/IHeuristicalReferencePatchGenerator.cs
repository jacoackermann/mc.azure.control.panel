﻿using System.Collections.Generic;

using Colorware.Core.Color;
using Colorware.Core.Data.Models;

using JetBrains.Annotations;

namespace Colorware.Core.Algorithms.PatchRecognition {
    /// <summary>
    /// Generates a list of ReferencePatches from a list of Samples.
    /// </summary>
    public interface IHeuristicalReferencePatchGenerator {
        [NotNull, Pure]
        IReadOnlyList<IReferencePatch> Convert([NotNull] IReadOnlyList<Spectrum> samples,
            [NotNull] Spectrum paperWhite,
            [NotNull] IMeasurementConditionsConfig measurementConditionsConfig);

        [NotNull, Pure]
        IReadOnlyList<IReferencePatch> Convert([NotNull] IReadOnlyList<Spectrum> samples,
            [NotNull] IMeasurementConditionsConfig measurementConditionsConfig);
    }
}