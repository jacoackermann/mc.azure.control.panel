using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

using Colorware.Core.Color;
using Colorware.Core.Color.Support;
using Colorware.Core.Data;
using Colorware.Core.Data.Models;
using Colorware.Core.Data.References;
using Colorware.Core.Enums;
using Colorware.Core.Extensions;

using JetBrains.Annotations;

// ReSharper disable PossibleMultipleEnumeration

namespace Colorware.Core.Algorithms.PatchRecognition {
    /// <summary>
    /// Generates a list of ReferencePatches from a list of Samples, using some heuristics
    /// to set meta-data properties such as patch name, type, tint etc.
    /// </summary>
    [UsedImplicitly]
    public class HeuristicalReferencePatchGenerator : IHeuristicalReferencePatchGenerator {
        private readonly IReferencePatchCreator referencePatchCreator;
        private readonly ILocalServiceModelIdGenerator localServiceModelIdGenerator;

        public HeuristicalReferencePatchGenerator([NotNull] IReferencePatchCreator referencePatchCreator,
                                                  [NotNull] ILocalServiceModelIdGenerator localServiceModelIdGenerator) {
            if (referencePatchCreator == null) throw new ArgumentNullException(nameof(referencePatchCreator));
            if (localServiceModelIdGenerator == null)
                throw new ArgumentNullException(nameof(localServiceModelIdGenerator));

            this.referencePatchCreator = referencePatchCreator;
            this.localServiceModelIdGenerator = localServiceModelIdGenerator;
        }

        [Pure]
        public IReadOnlyList<IReferencePatch> Convert(IReadOnlyList<Spectrum> samples, Spectrum paperWhite,
                                                      IMeasurementConditionsConfig measurementConditionsConfig) {
            if (samples == null) throw new ArgumentNullException(nameof(samples));
            if (paperWhite == null) throw new ArgumentNullException(nameof(paperWhite));
            if (measurementConditionsConfig == null)
                throw new ArgumentNullException(nameof(measurementConditionsConfig));

            var pwSpectrum = findPaperWhite(samples, paperWhite);

            return convert(samples, pwSpectrum, measurementConditionsConfig);
        }

        public IReadOnlyList<IReferencePatch> Convert(IReadOnlyList<Spectrum> samples,
                                                      IMeasurementConditionsConfig measurementConditionsConfig) {
            if (samples == null) throw new ArgumentNullException(nameof(samples));
            if (measurementConditionsConfig == null)
                throw new ArgumentNullException(nameof(measurementConditionsConfig));

            var pw = findPaperWhite(samples);

            return convert(samples, pw, measurementConditionsConfig);
        }

        // TODO: move to IRPC?
        private IReferencePatch createSolid(string name, Spectrum spectrum, IMeasurementConditionsConfig mc) {
            var p = referencePatchCreator.CreateReferencePatch(mc.GetMeasurementConditions());

            p.PatchType = PatchTypes.Solid;
            p.Name = name;
            p.Percentage = 100;
            p.AsSpectrum = spectrum;
            p.DefaultDensity = spectrum.ToDensities(mc.DensityStandard).GetFilteredValue();
            p.SetLabAndMeasurementConditions(spectrum.ToLab(mc.GetMeasurementConditions()));

            return p;
        }

        private IReferencePatch createSolid(CmykComponents cmykComponent, [NotNull] Spectrum spectrum,
                                            [NotNull] IMeasurementConditionsConfig mc) {
            var p = createSolid(cmykComponent.ToString(), spectrum, mc);

            if (cmykComponent == CmykComponents.C)
                p.Cyan = 100;
            else if (cmykComponent == CmykComponents.M)
                p.Magenta = 100;
            else if (cmykComponent == CmykComponents.Y)
                p.Yellow = 100;
            else if (cmykComponent == CmykComponents.K)
                p.Key = 100;

            return p;
        }

        private IReferencePatch createOverprint([NotNull] IEnumerable<IReferencePatch> parents,
                                                [NotNull] Spectrum spectrum,
                                                [NotNull] IMeasurementConditionsConfig measurementConditionsConfig) {
            if (parents.Count() == 2)
                return createOverprint(parents.ElementAt(0), parents.ElementAt(1), spectrum, measurementConditionsConfig);

            if (parents.Count() == 3)
                return createOverprint(parents.ElementAt(0), parents.ElementAt(1), parents.ElementAt(2), spectrum,
                                       measurementConditionsConfig);

            throw new ArgumentException("Can only create overprint with 2 or 3 parents!", nameof(parents));
        }

        private IReferencePatch createOverprint([NotNull] IReferencePatch parent1, [NotNull] IReferencePatch parent2,
                                                [CanBeNull] IReferencePatch parent3, [NotNull] Spectrum spectrum,
                                                [NotNull] IMeasurementConditionsConfig mc) {
            var p = referencePatchCreator.CreateReferencePatch(mc.GetMeasurementConditions());

            p.PatchType = PatchTypes.Overprint;
            p.Name = parent1.Name + parent2.Name + (parent3 != null ? parent3.Name : "") + "100";
            p.Percentage = 100;
            p.Parent1Id = parent1.Id;
            p.Parent2Id = parent2.Id;
            p.Parent3Id = parent3?.Id ?? 0;
            p.AsSpectrum = spectrum;
            p.SetLabAndMeasurementConditions(spectrum.ToLab(mc.GetMeasurementConditions()));

            p.Cyan = new[] {parent1.Cyan, parent2.Cyan, parent3?.Cyan ?? 0}.Max();
            p.Magenta = new[] {parent1.Magenta, parent2.Magenta, parent3?.Magenta ?? 0}.Max();
            p.Yellow = new[] {parent1.Yellow, parent2.Yellow, parent3?.Yellow ?? 0}.Max();
            p.Key = new[] {parent1.Key, parent2.Key, parent3?.Key ?? 0}.Max();

            return p;
        }

        private IReferencePatch createOverprint([NotNull] IReferencePatch parent1, [NotNull] IReferencePatch parent2,
                                                [NotNull] Spectrum spectrum,
                                                [NotNull] IMeasurementConditionsConfig measurementConditionsConfig) {
            return createOverprint(parent1, parent2, null, spectrum, measurementConditionsConfig);
        }

        private IReferencePatch createDotgain(TintPatchGuess tintPatchGuess, IReferencePatch parent,
                                              IMeasurementConditionsConfig mc) {
            var p = referencePatchCreator.CreateReferencePatch(mc.GetMeasurementConditions());

            p.PatchType = PatchTypes.Dotgain;
            // TODO: more intelligent, probably needs more info from top (what are other guesses?)
            p.Percentage = roundTint(tintPatchGuess.Tint);
            p.Name = parent.Name;
            p.Parent1Id = parent.Id;
            p.AsSpectrum = tintPatchGuess.TintPatch;
            p.SetLabAndMeasurementConditions(tintPatchGuess.TintPatch.ToLab(mc.GetMeasurementConditions()));

            // TODO: CMYK seperation

            return p;
        }

        private int roundTint(double tintGuess) {
            return (int)(Math.Round(tintGuess * 10) * 10);
        }

        private int grayUniqueId = 0;

        private IReferencePatch createGrayBalance(GrayPatchGuess grayPatchGuess, IMeasurementConditionsConfig mc) {
            var p = referencePatchCreator.CreateReferencePatch(mc.GetMeasurementConditions());

            p.PatchType = grayPatchGuess.Type;
            p.Name = "Gray00-00-00_" + ++grayUniqueId;
            p.Percentage = 100;
            p.AsSpectrum = grayPatchGuess.GrayPatch;
            p.DefaultDensity = grayPatchGuess.GrayPatch.ToDensities(mc.DensityStandard).GetFilteredValue();
            p.SetLabAndMeasurementConditions(grayPatchGuess.GrayPatch.ToLab(mc.GetMeasurementConditions()));

            return p;
        }

        private IReferencePatch createPaperwhite(Spectrum paperWhite, IMeasurementConditionsConfig mc) {
            var p = referencePatchCreator.CreateReferencePatch(mc.GetMeasurementConditions());

            p.PatchType = PatchTypes.Paperwhite;
            p.Name = "PW";
            p.AsSpectrum = paperWhite;
            p.SetLabAndMeasurementConditions(paperWhite.ToLab(mc.GetMeasurementConditions()));

            return p;
        }

        private IReadOnlyList<IReferencePatch> convert([NotNull] IReadOnlyList<Spectrum> samples,
                                                       [CanBeNull] Spectrum paperWhite,
                                                       [NotNull] IMeasurementConditionsConfig
                                                           measurementConditionsConfig) {
            if (samples != null && !samples.Contains(paperWhite))
                throw new ArgumentException(
                    "The supplied paperwhite sample must be in the collection of supplied samples!", nameof(paperWhite));

            // Start out with all patches as undefined, and fill them in best as we can
            var result =
                samples.Select(
                    _ =>
                    referencePatchCreator.CreateReferencePatch(measurementConditionsConfig.GetMeasurementConditions()))
                       .ToList();

            var unknownSamples = samples.Where((s, i) => result[i].IsUndefined);

            if (paperWhite != null)
                result[samples.IndexOf(paperWhite)] = createPaperwhite(paperWhite, measurementConditionsConfig);

            // Solids
            var k = findSolid(unknownSamples, measurementConditionsConfig, CmykComponents.K, paperWhite);
            if (k != null)
                result[samples.IndexOf(k)] = createSolid(CmykComponents.K, k, measurementConditionsConfig);

            var c = findSolid(unknownSamples, measurementConditionsConfig, CmykComponents.C, paperWhite);
            if (c != null)
                result[samples.IndexOf(c)] = createSolid(CmykComponents.C, c, measurementConditionsConfig);

            var y = findSolid(unknownSamples, measurementConditionsConfig, CmykComponents.Y, paperWhite);
            if (y != null)
                result[samples.IndexOf(y)] = createSolid(CmykComponents.Y, y, measurementConditionsConfig);

            var m = findSolid(unknownSamples, measurementConditionsConfig, CmykComponents.M, paperWhite);
            if (m != null)
                result[samples.IndexOf(m)] = createSolid(CmykComponents.M, m, measurementConditionsConfig);

            var solids = new[] {c, m, y, k};

            result = findAndFillDuplicates(samples, result);

            // Overprints
            var overprintTuples = new[] {
                new[] {c, m},
                new[] {c, y},
                new[] {m, y},
                new[] {c, m, y}
            };

            var overprints = findOverprints(unknownSamples, overprintTuples);
            overprints.ForEach(o =>
                               result[samples.IndexOf(o.Overprint)] =
                               createOverprint(o.ParentSolids.Select(p => result[samples.IndexOf(p)]), o.Overprint,
                                               measurementConditionsConfig));

            // Dotgains
            var tintPatches = paperWhite != null
                                  ? findTintPatches(unknownSamples, solids, paperWhite)
                                  : new List<TintPatchGuess>();

            tintPatches.ForEach(p =>
                                result[samples.IndexOf(p.TintPatch)] =
                                createDotgain(p, result[samples.IndexOf(p.Solid)], measurementConditionsConfig));

            // Graybalance patches
            var grayPatches = findGrays(unknownSamples, measurementConditionsConfig);

            grayPatches.ForEach(
                p => result[samples.IndexOf(p.GrayPatch)] = createGrayBalance(p, measurementConditionsConfig));

            return result;
        }

        [NotNull, Pure]
        private List<IReferencePatch> findAndFillDuplicates(IEnumerable<Spectrum> samples,
                                                            [NotNull] IEnumerable<IReferencePatch> generatedPatches) {
            if (samples.Count() != generatedPatches.Count())
                throw new ArgumentException("'samples' and 'generatedPatches' list count must be equal!");

            var i = 0;

            return samples.Zip(generatedPatches, (sample, patch) => {
                if (patch.IsDefined) {
                    Console.WriteLine("{0}:\t{1}", i++, "DEFINED");
                    return patch;
                }

                // Find best match (if any)
                var match = generatedPatches
                    .Where(candidate => candidate.IsDefined)
                    .Select(candidate => new {
                        Patch = candidate,
                        Diff = sample.ReflectanceValues.AbsDiff(
                            candidate.AsSpectrum.ReflectanceValues).Sum()
                    })
                    .OrderBy(v => v.Diff)
                    .FirstOrDefault();
                // TODO
                //.FirstOrDefault(v => v.Diff < 0.75);

                if (match.Diff > 0.75) {
                    Console.WriteLine("{0}:\tNO MATCH:\t{1}", i++, match.Diff);
                    return patch;
                }

                Console.WriteLine("{0}:\t{1}", i++, match.Diff);

                var clone = match.Patch.Clone<IReferencePatch>(); // TODO: Should this be CloneNew?
                clone.Id = localServiceModelIdGenerator.GenerateId();
                clone.AsSpectrum = sample;
                clone.SetLabAndMeasurementConditions(sample.ToLab(clone.MeasurementConditions));
                return clone;
            }).ToList();
        }

        [NotNull]
        private IReadOnlyList<GrayPatchGuess> findGrays(IEnumerable<Spectrum> samples, IMeasurementConditionsConfig mc) {
            return samples
                // Filter on 3 simple heuristics
                .Where(s => {
                    var s510 = s.SpectrumSamples.FirstOrDefault(v => v.WaveLength.NearlyEquals(510));
                    var s440 = s.SpectrumSamples.FirstOrDefault(v => v.WaveLength.NearlyEquals(440));
                    var higherAt510ThanAt440 = s510 != null && s440 != null && s510.Value > s440.Value;

                    var maxDiff = s.SpectrumSamples.MaxBy(v => v.Value).Value -
                                  s.SpectrumSamples.MinBy(v => v.Value).Value;

                    var isStrictlyDescendingBetween510And570 =
                        s.SpectrumSamples.Where(v => v.WaveLength >= 510 && v.WaveLength <= 570)
                         .Select(v => v.Value)
                         .IsStrictlyDescending();

                    return higherAt510ThanAt440 && maxDiff <= 0.35 && isStrictlyDescendingBetween510And570;
                })
                // Determine gray type
                .Select(s => {
                    var lab = s.ToLab(mc.GetMeasurementConditions());
                    // Magic thresholds pulled from PV DB
                    if (lab.L > 35 && lab.L <= 51)
                        return new GrayPatchGuess(s, PatchTypes.BalanceShadow);

                    if (lab.L > 51 && lab.L <= 68)
                        return new GrayPatchGuess(s, PatchTypes.BalanceMidtone);

                    if (lab.L > 68 && lab.L <= 90)
                        return new GrayPatchGuess(s, PatchTypes.BalanceHighlight);

                    return new GrayPatchGuess(s, PatchTypes.Balance);
                })
                .ToList();
        }

        [NotNull]
        private IReadOnlyList<TintPatchGuess> findTintPatches([NotNull] IEnumerable<Spectrum> samples,
                                                              [NotNull] IEnumerable<Spectrum> solids,
                                                              [NotNull] Spectrum paperWhite) {
            // Finds dotgain patches for every solid, ignoring the already found dotgain patches
            return solids.Aggregate(new List<TintPatchGuess>().AsEnumerable(),
                                    (list, s) =>
                                    list.Concat(findTintPatches(samples.Except(list.Select(v => v.TintPatch)), s,
                                                                paperWhite)))
                         .ToList();
        }

        [NotNull]
        private IReadOnlyList<TintPatchGuess> findTintPatches([NotNull] IEnumerable<Spectrum> samples,
                                                              [NotNull] Spectrum solid, [NotNull] Spectrum paperWhite) {
            return
                samples.Select(
                    s => new {Sample = s, Tint = findDigitalTintPercentageNoffkeSeymour(solid, paperWhite, s)})
                       .Where(v => v.Tint.HasValue)
                       .Select(v => new TintPatchGuess(solid, v.Sample, v.Tint.Value))
                       .ToList();
        }

        private double? findDigitalTintPercentageNoffkeSeymour([NotNull] Spectrum solid, [NotNull] Spectrum paperWhite,
                                                               [NotNull] Spectrum measuredTintPatch) {
            // Find the tint percentage 'a' for which the calculated curve most closely
            // matches our measured curve
            var scores = LinqExtensions.Range(0.01, 99, 0.01).Select(a => {
                var md = DotgainAlgorithms.EstimateHalftoneSpectrumMurrayDavies(solid.ReflectanceValues,
                                                                                paperWhite.ReflectanceValues, a);
                var bl = DotgainAlgorithms.EstimateHalftoneSpectrumBeersLaw(solid.ReflectanceValues,
                                                                            paperWhite.ReflectanceValues, a);
                var h = DotgainAlgorithms.CalculateNoffkeSeymourOptimalH(md, bl, measuredTintPatch.ReflectanceValues);

                var halftone = DotgainAlgorithms.EstimateHalftoneSpectrumNoffkeSeymour(md, bl, h);

                var absDiff = halftone.AbsDiff(measuredTintPatch.ReflectanceValues).Sum();

                return new {AbsDiff = absDiff, Percentage = a};
            });
            var winner = scores.MinBy(v => v.AbsDiff);

            // Usually < 0.4, but just to be safe
            return winner.AbsDiff < 0.7 ? winner.Percentage : default(double?);
        }

        [NotNull]
        private Spectrum findBlackestSample([NotNull] IEnumerable<Spectrum> samples) {
            return samples.MinBy(sample => sample.SpectrumSamples.Sum(s => s.Value));
        }

        [CanBeNull]
        private Spectrum findPaperWhite([NotNull] IEnumerable<Spectrum> samples, Spectrum referencePaperWhite) {
            return samples.MinBy(sample => sample.ReflectanceValues.AbsDiff(referencePaperWhite.ReflectanceValues)
                                                 .Sum());
        }

        [CanBeNull]
        private Spectrum findPaperWhite([NotNull] IEnumerable<Spectrum> samples) {
            // TODO: introduce threshold
            return samples.MaxBy(s => s.ReflectanceValues.Sum());
        }

        private double getComponentWeight([NotNull] CMYK cmyk, double componentValue) {
            // Math.Max is necessary because with paperwhite as density whitebase, samples can get negative densities
            // (as the paperwhite is substracted from the measured patch)
            return Math.Max(componentValue, 0.0001) /
                   (Math.Max(cmyk.C, 0.0001) + Math.Max(cmyk.M, 0.0001) + Math.Max(cmyk.Y, 0.0001) +
                    Math.Max(cmyk.K, 0.0001) - Math.Max(componentValue, 0.0001));
        }

        [CanBeNull]
        private Spectrum findSolid([NotNull] IEnumerable<Spectrum> samples,
                                   [NotNull] IMeasurementConditionsConfig measurementConditionsConfig,
                                   CmykComponents cmykComponent, [CanBeNull] Spectrum paperWhite) {
            var ds = measurementConditionsConfig.DensityStandard;
            var pw = paperWhite ??
                     new Spectrum(
                         samples.ElementAt(0).SpectrumSamples.Select(v => new Spectrum.SpectrumSample(v.WaveLength, 0)));

            switch (cmykComponent) {
                case CmykComponents.C:
                    return samples.Where(s => getComponentWeight(s.ToDensities(ds, pw), s.ToDensities(ds, pw).C) > 0.65)
                                  .OrderByDescending(s => s.ToDensities(ds, pw).C).FirstOrDefault();
                case CmykComponents.M:
                    return samples.Where(s => getComponentWeight(s.ToDensities(ds, pw), s.ToDensities(ds, pw).M) > 0.6)
                                  .OrderByDescending(s => s.ToDensities(ds, pw).M).FirstOrDefault();
                case CmykComponents.Y:
                    return samples.Where(s => getComponentWeight(s.ToDensities(ds, pw), s.ToDensities(ds, pw).Y) > 2.5)
                                  .OrderByDescending(s => s.ToDensities(ds, pw).Y).FirstOrDefault();
                case CmykComponents.K:
                    // TODO: sheet @ wassenaar was 1.02 and CMY overprint was higher than K... combine with
                    // heuristics related to spectrum
                    return
                        samples.OrderByDescending(s => s.ToDensities(ds, pw).K).FirstOrDefault(s => s.ToDensities(ds, pw).K > 1.5);

                default:
                    throw new ArgumentOutOfRangeException(nameof(cmykComponent));
            }
        }

        /// <summary>
        /// Finds an overprint sample for 2 or 3 given parent solid samples.
        /// 
        /// Uses the heuristic that the reflectance value at a certain wavelength in the overprint curve 
        /// lies close to the lowest reflectance value of the two solid curves at that wavelength.
        /// </summary>
        [NotNull]
        private IEnumerable<Spectrum> findOverprints([NotNull] IEnumerable<Spectrum> samples,
                                                     [NotNull] IEnumerable<Spectrum> solids) {
            if (solids.Count() < 2 || solids.Count() > 3)
                throw new ArgumentException("Can only find overprints for lists of 2 or 3 solids!");

            var scores = samples.Select(s => {
                var solidsMinCurve =
                    solids.Select(solid => solid.ReflectanceValues)
                          .Aggregate((result, spectrum) => (IImmutableList<double>)result.Min(spectrum));

                var minCurveDiff = solidsMinCurve.AbsDiff(s.ReflectanceValues);
                return new {TotalMinDiff = minCurveDiff.Sum(), Sample = s};
            });

            return scores.Where(s => s.TotalMinDiff < 1.0).Select(s => s.Sample);
        }

        [NotNull]
        private IEnumerable<OverprintGuess> findOverprints([NotNull] IEnumerable<Spectrum> samples,
                                                           [NotNull] IEnumerable<IEnumerable<Spectrum>> solids) {
            // Finds overprints for every tuple of solids, ignoring the already found overprints
            return solids.Aggregate(new List<OverprintGuess>().AsEnumerable(),
                                    (list, parents) =>
                                    list.Concat(findOverprints(samples.Except(list.Select(v => v.Overprint)), parents)
                                                    .Select(o => new OverprintGuess(o, parents)))
                ).ToList();
        }
    }
}