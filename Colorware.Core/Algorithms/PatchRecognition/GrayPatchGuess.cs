﻿using System;

using Colorware.Core.Color;
using Colorware.Core.Enums;

using JetBrains.Annotations;

namespace Colorware.Core.Algorithms.PatchRecognition {
    internal class GrayPatchGuess {
        public GrayPatchGuess([NotNull] Spectrum grayPatch, PatchTypes type) {
            if (grayPatch == null) throw new ArgumentNullException("grayPatch");

            GrayPatch = grayPatch;
            Type = type;
        }

        [NotNull]
        public Spectrum GrayPatch { get; private set; }

        public PatchTypes Type { get;private set; }
    }
}