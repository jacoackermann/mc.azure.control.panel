﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;

using Colorware.Core.Algorithms.DeltaEShapeApproximation.DeltaEStrategies;
using Colorware.Core.Color;

namespace Colorware.Core.Algorithms.DeltaEShapeApproximation {
    public class DeltaEShapeApproximator {
        private const double Accuracy = 0.001;
        private const int MaxItterations = 50;

        /// <summary>
        /// Strategy used to calculate points.
        /// </summary>
        private readonly IDeltaEStrategy strategy;

        /// <summary>
        /// The reference point (or center) that is used. DeltaEs are calculated related to this point.
        /// </summary>
        private readonly Lab reference;

        public DeltaEShapeApproximator(IDeltaEStrategy strategy, Lab reference) {
            this.strategy = strategy;
            this.reference = reference;
        }


        /// <summary>
        /// Find a solution for a single point at the given angle.
        /// </summary>
        /// <param name="angle">The angle in radians.</param>
        /// <param name="lOther">The L value to calculate the delta-E intersection for. This would be the L value of the sample.</param>
        /// <param name="deltaE">The delta E to calculate the delta-E intersection for.</param>
        /// <returns>
        /// The interpolated point.
        /// </returns>
        /// <exception cref="Exception">Thrown if a solution could not be found.</exception>
        public Point ApproximateForAngle(double angle, double lOther, double deltaE) {
            //start interval for radius (0, 500).
            var r = regulaFalsiAux(lOther, deltaE, angle, 0, 500, Accuracy, MaxItterations);
            var a2 = reference.a + r * Math.Sin(angle);
            var b2 = reference.b + r * Math.Cos(angle);
            return new Point(a2, b2);
        }

        public List<Point> GeneratePointList(double lOther, double deltaE) {
            var points = new List<Point>();
            const double step = Math.PI / 180.0; // Generate a point for each degree.
            for (var angle = 0.0; angle < 2 * Math.PI; angle += step) {
                var p = ApproximateForAngle(angle, lOther, deltaE);
                points.Add(p);
            }
            return points;
        }

        private double deltaEForAngleAndRadius(double lOther, double r, double angle) {
            var a = reference.a + r * Math.Sin(angle);
            var b = reference.b + r * Math.Cos(angle);
            var probe = new Lab(lOther, a, b, reference.MeasurementConditions);
            return strategy.DeltaE(reference, probe);
        }

        private double regulaFalsiAux(double lOther, double deltaE, double phi, double left, double right,
            double accuracy, int maxiterations) {
            //Standard regula falsi algorithm. See internet for this, there are zillions of examples.
            int iter;
            var side = 0;
            var current = 0.0;
            var fleft = deltaEForAngleAndRadius(lOther, left, phi) - deltaE;
            var fright = deltaEForAngleAndRadius(lOther, right, phi) - deltaE;

            for (iter = 0; iter < maxiterations; iter++) {
                current = (fleft * right - fright * left) / (fleft - fright);
                if (Math.Abs(right - left) < accuracy * Math.Abs(right + left)) break;
                var fcurrent = deltaEForAngleAndRadius(lOther, current, phi) - deltaE;

                if (fcurrent * fright > 0) {
                    right = current;
                    fright = fcurrent;
                    if (side == -1) fleft /= 2;
                    side = -1;
                }
                else if (fleft * fcurrent > 0) {
                    left = current;
                    fleft = fcurrent;
                    if (side == +1) fright /= 2;
                    side = +1;
                }
                else break;
            }
            if (iter >= maxiterations) {
                var e = new Exception("Error in solve: no solution") {Source = "DeltaEShapeApproximator"};
                throw (e);
            }
            return current;
        }

        public static void DumpPointList(string path, List<Point> list) {
            using (var sw = new StreamWriter(path)) {
                sw.WriteLine("x,y");
                foreach (var p in list) {
                    sw.WriteLine("{0},{1}", p.X, p.Y);
                }
            }
        }
    }
}