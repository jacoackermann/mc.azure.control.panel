﻿using Colorware.Core.Color;

namespace Colorware.Core.Algorithms.DeltaEShapeApproximation.DeltaEStrategies {
    public class Cie2000DeltaEStrategy : IDeltaEStrategy {
        private readonly double kl, kc, kh;

        public Cie2000DeltaEStrategy()
            : this(Lab.Cie2000DefaultForKl, Lab.Cie2000DefaultForKc, Lab.Cie2000DefaultForKh) {
        }

        public Cie2000DeltaEStrategy(double kl, double kc, double kh) {
            this.kl = kl;
            this.kc = kc;
            this.kh = kh;
        }

        #region Implementation of IDeltaEStrategy
        public double DeltaE(Lab reference, Lab other) {
            return Lab.DeltaE2000(reference, other, kl, kc, kh);
        }
        #endregion
    }
}