﻿using Colorware.Core.Color;

namespace Colorware.Core.Algorithms.DeltaEShapeApproximation.DeltaEStrategies {
    public interface IDeltaEStrategy {
        double DeltaE(Lab reference, Lab other);
    }
}