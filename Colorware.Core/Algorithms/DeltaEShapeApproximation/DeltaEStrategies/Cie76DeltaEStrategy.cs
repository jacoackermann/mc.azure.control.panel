﻿using Colorware.Core.Color;

namespace Colorware.Core.Algorithms.DeltaEShapeApproximation.DeltaEStrategies {
    public class Cie76DeltaEStrategy : IDeltaEStrategy {
        public double DeltaE(Lab reference, Lab other) {
            return reference.DeltaE76(other);
        }
    }
}