﻿using Colorware.Core.Color;

namespace Colorware.Core.Algorithms.DeltaEShapeApproximation.DeltaEStrategies {
    public class CieCMCDeltaEStrategy : IDeltaEStrategy {
        private readonly double l, c;

        public CieCMCDeltaEStrategy()
            : this(Lab.CieCmcDefaultForL, Lab.CieCmcDefaultForC) {
        }

        public CieCMCDeltaEStrategy(double l, double c) {
            this.l = l;
            this.c = c;
        }

        public double DeltaE(Lab reference, Lab other) {
            return reference.DeltaECMC(other, l, c);
        }
    }
}