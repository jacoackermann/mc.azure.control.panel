﻿using Colorware.Core.Color;

namespace Colorware.Core.Algorithms.DeltaEShapeApproximation.DeltaEStrategies {
    public class Cie94DeltaEStrategy : IDeltaEStrategy {
        private readonly double kl, kc, kh, k1, k2;

        public Cie94DeltaEStrategy()
            : this(Lab.Cie94DefaultForKl, Lab.Cie94DefaultForKc, Lab.Cie94DefaultForKh,
                Lab.Cie94DefaultForK1, Lab.Cie94DefaultForK2) {
        }

        public Cie94DeltaEStrategy(double kl, double kc, double kh, double k1, double k2) {
            this.kl = kl;
            this.kc = kc;
            this.kh = kh;
            this.k1 = k1;
            this.k2 = k2;
        }

        #region Implementation of IDeltaEStrategy
        public double DeltaE(Lab reference, Lab other) {
            return Lab.DeltaE94(reference, other, kl, kc, kh, k1, k2);
        }
        #endregion
    }
}