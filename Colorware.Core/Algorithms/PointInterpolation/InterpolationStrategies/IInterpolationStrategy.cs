using System.Collections.Generic;
using System.Windows;

namespace Colorware.Core.Algorithms.PointInterpolation.InterpolationStrategies {
    public interface IInterpolationStrategy {
        Point FindPointForX(double x);
        List<InterpolatedPoint> GenerateInterpolatedPointList(double xStep);
    }
}