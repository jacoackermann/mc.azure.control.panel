﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

using Colorware.Core.Logging;

using CWModel;

namespace Colorware.Core.Algorithms.PointInterpolation.InterpolationStrategies {
    public class SplineFitInterpolationStrategy : IInterpolationStrategy {
        private readonly List<Point> points;

        private const double Rho = 1.0;

        private bool curveIsEmpty = false;
        private alglib.spline1dinterpolant sX; //method used for creating the spline
        private alglib.spline1dfitreport i;

        public SplineFitInterpolationStrategy(List<Point> points) {
            this.points = points;
            createCurve(points);
        }

        public Point FindPointForX(double x) {
            return new Point(x, getCurveY(x));
        }

        public List<InterpolatedPoint> GenerateInterpolatedPointList(double xStep) {
            const double bias = 0.01;
            var generatedPoints = new List<InterpolatedPoint>();
            for (var x = 0.0; x <= 100.0; x += xStep) {
                var y = getCurveY(x);
                generatedPoints.Add(new InterpolatedPoint(new Point(x, y), true));
            }
            var existingPoints = points.Select(p => new InterpolatedPoint(p, false)).ToList();

            return
                existingPoints.Concat(
                    generatedPoints.Where(gp => !existingPoints.Any(ep => Math.Abs(gp.X - ep.X) <= bias)))
                              .OrderBy(p => p.X)
                              .ToList();

            // TODO: boolean to request max 11 points (no originals in between) (useful for exporters that require a fixed amount of points)
        }

        private void createCurve(List<Point> points) {
            curveIsEmpty = false;
            if (points.Count <= 2) {
                curveIsEmpty = true;
                return;
            }
            sX = new alglib.spline1dinterpolant();
            i = new alglib.spline1dfitreport();
            // int inf;

            var x = points.Select(p => p.X).ToArray();
            var y = points.Select(p => p.Y).ToArray();

            // See: http://www.alglib.net/translator/man/manual.csharp.html

            // alglib.spline1dfitpenalized(x, y, x.Length, Rho, out inf, out sX, out i);

            if (points.Count >= 4) {
                // Catmull-Rom based splines.
                try {
                    alglib.spline1dbuildcatmullrom(x, y, out sX);
                }
                    // Alternative for when Catmull-Rom can't handle certain specific situations
                    // (e.g. unsorted dotgain values or dotgain values that are too close to each other)
                catch (Exception ex) {
                    LogManager.GetLogger(GetType()).Error(ex.Message, ex);
                    try {
                        int info;
                        alglib.spline1dfithermite(x, y, 4, out info, out sX, out i);
                    }
                    catch (Exception exInner) {
                        LogManager.GetLogger(GetType()).Error(ex.Message, exInner);
                        curveIsEmpty = true;
                    }
                }
            }
            else {
                // Use cubic splines as a backup when we don't have enough actual points for a Catmull-Rom spline.
                // Due to the if at the start of this method this should mean that we have 3 points if
                // we reach this point.
                try {
                    alglib.spline1dbuildcubic(x, y, x.Length, 0, 0, 0, 0, out sX);
                }
                catch (Exception) {
                    // spline1dbuildcubic has failed us for whatever reason. Most likely this was caused
                    // by a point setup that was to extreme to interpolate.
                    curveIsEmpty = true;
                }
            }

            //// Hermite based (not the same results as the old implementation, results in a smoother curve!)
            // int info;
            // alglib.spline1dfithermite(x, y, 4, out info, out sX, out i);
        }

        private double getCurveY(double x) {
            if (curveIsEmpty)
                return 0.0;

            return alglib.spline1dcalc(sX, x);
        }
    }
}