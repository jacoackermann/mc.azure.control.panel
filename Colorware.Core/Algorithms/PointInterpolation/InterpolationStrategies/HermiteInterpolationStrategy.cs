using System;
using System.Collections.Generic;
using System.Windows;

namespace Colorware.Core.Algorithms.PointInterpolation.InterpolationStrategies {
    public class HermiteInterpolationStrategy : IInterpolationStrategy {
        /// <summary>
        /// Actual points to use.
        /// </summary>
        private readonly List<Point> points;

        /// <summary>
        /// Pin down start and end point?
        /// </summary>
        public bool PinPoints { get; set; }

        public double Bias { get; set; }
        public double Tension { get; set; }
        public double Step { get; set; }

        #region Constructors
        public HermiteInterpolationStrategy(List<Point> points)
            : this(points, 0, 0, 0.0001) {
        }

        public HermiteInterpolationStrategy(List<Point> points, double bias, double tension, double step) {
            PinPoints = true;
            Bias = bias;
            Tension = tension;
            Step = step;
            this.points = points;
        }
        #endregion

        #region Interpolation
        private double interpolate(double y0, double y1, double y2, double y3, double mu) {
            var mu2 = mu * mu;
            var mu3 = mu2 * mu;
            var m0 = (y1 - y0) * (1.0 + Bias) * (1.0 - Tension) / 2.0 +
                     (y2 - y1) * (1.0 - Bias) * (1.0 - Tension) / 2.0;
            var m1 = (y2 - y1) * (1.0 + Bias) * (1.0 - Tension) / 2.0 +
                     (y3 - y2) * (1.0 - Bias) * (1.0 - Tension) / 2.0;
            var a0 = 2.0 * mu3 - 3.0 * mu2 + 1.0;
            var a1 = mu3 - 2.0 * mu2 + mu;
            var a2 = mu3 - mu2;
            var a3 = (-2.0) * mu3 + 3.0 * mu2;
            return a0 * y1 + a1 * m0 + a2 * m1 + a3 * y2;
        }

        private List<Point> getPinnedPoints(List<Point> originalPoints) {
            if (!PinPoints) {
                // Use original points, no need to alter collection.
                return originalPoints;
            }
            else {
                // Copy points adding start and end points if required.
                var points = new List<Point>();
                var firstPoint = originalPoints[0];
                for (int i = 0; i < 4; i++) {
                    points.Add(firstPoint);
                }
                points.AddRange(originalPoints);
                var lastPoint = originalPoints[originalPoints.Count - 1];
                for (int i = 0; i < 4; i++) {
                    points.Add(lastPoint);
                }
                return points;
            }
        }
        #endregion

        #region Implementation of IInterpolationStrategy
        public Point FindPointForX(double x) {
            if (points.Count <= 0)
                return new Point();

            var pinnedPoints = getPinnedPoints(points);
            var bestFound = pinnedPoints[0];
            if (pinnedPoints.Count <= 4)
                return bestFound;

            var foundAny = false;

            var p0 = pinnedPoints[0];
            var p1 = pinnedPoints[1];
            var p2 = pinnedPoints[2];
            var p3 = pinnedPoints[3];

            for (int i = 4; i < pinnedPoints.Count; i++) {
                for (double mu = 0; mu < 1.0; mu += Step) {
                    var newX = interpolate(p0.X, p1.X, p2.X, p3.X, mu);
                    var newY = interpolate(p0.Y, p1.Y, p2.Y, p3.Y, mu);
                    if (! foundAny || Math.Abs(x - newX) < Math.Abs(x - bestFound.X)) {
                        foundAny = true;
                        bestFound = new Point(newX, newY);
                    }
                }
                p0 = p1;
                p1 = p2;
                p2 = p3;
                p3 = pinnedPoints[i];
            }
            return bestFound;
        }

        public List<InterpolatedPoint> GenerateInterpolatedPointList(double xStep) {
            var result = new List<InterpolatedPoint>();
            if (points.Count <= 0)
                return result;

            var pinnedPoints = getPinnedPoints(points);
            if (pinnedPoints.Count <= 4)
                return result;

            var lastPoint = pinnedPoints[pinnedPoints.Count - 1];

            var p0 = pinnedPoints[0];
            var p1 = pinnedPoints[1];
            var p2 = pinnedPoints[2];
            var p3 = pinnedPoints[3];

            for (int i = 4; i < pinnedPoints.Count; i++) {
                double xToFind = p1.X + xStep;
                // Add first point as real (non virtual) point.
                result.Add(new InterpolatedPoint(p1, false));
                for (double mu = 0; mu < 1.0; mu += Step) {
                    double x = interpolate(p0.X, p1.X, p2.X, p3.X, mu);
                    double y = interpolate(p0.Y, p1.Y, p2.Y, p3.Y, mu);
                    if (x >= xToFind) {
                        result.Add(new InterpolatedPoint(new Point(x, y), true));
                        while (x >= xToFind) {
                            xToFind += xStep;
                            if (xToFind >= lastPoint.X) {
                                result.Add(new InterpolatedPoint(lastPoint, false));
                                // No scoped break command, so meh...
                                goto breakAll;
                            }
                        }
                    }
                }
                p0 = p1;
                p1 = p2;
                p2 = p3;
                p3 = pinnedPoints[i];
            }
            breakAll:
            if (PinPoints)
                // Remove first three and last point. These are added due to pinning the points.
                return result.GetRange(3, result.Count - 3);
            return result;
        }
        #endregion
    }
}