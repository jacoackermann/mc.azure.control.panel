﻿using System.Windows;

namespace Colorware.Core.Algorithms.PointInterpolation {
    public struct InterpolatedPoint {
        public bool IsVirtual { get; }

        public readonly double X;
        public readonly double Y;

        public InterpolatedPoint(Point point, bool isVirtual) {
            X = point.X;
            Y = point.Y;
            IsVirtual = isVirtual;
        }

        #region Overrides of ValueType
        public override string ToString() {
            return $"InterpolatedPoint({X:F}, {Y:F}, {IsVirtual})";
        }
        #endregion
    }
}
