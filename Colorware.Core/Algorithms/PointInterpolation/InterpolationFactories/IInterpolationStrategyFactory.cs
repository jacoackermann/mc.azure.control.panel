﻿using System.Collections.Generic;
using System.Windows;

using Colorware.Core.Algorithms.PointInterpolation.InterpolationStrategies;

namespace Colorware.Core.Algorithms.PointInterpolation.InterpolationFactories {
    public interface IInterpolationStrategyFactory {
        IInterpolationStrategy Create(List<Point> points);
    }
}