﻿using System.Collections.Generic;
using System.Windows;

using Colorware.Core.Algorithms.PointInterpolation.InterpolationStrategies;

namespace Colorware.Core.Algorithms.PointInterpolation.InterpolationFactories {
    public class HermiteInterpolationStrategyFactory : IInterpolationStrategyFactory {
        private readonly double defaultBias;
        private readonly double defaultTension;
        private readonly double defaultInterpolationStep;

        public HermiteInterpolationStrategyFactory() : this(0, 0, 0.001) {
        }

        public HermiteInterpolationStrategyFactory(double defaultBias, double defaultTension,
            double defaultInterpolationStep) {
            this.defaultBias = defaultBias;
            this.defaultTension = defaultTension;
            this.defaultInterpolationStep = defaultInterpolationStep;
        }


        public IInterpolationStrategy Create(List<Point> points) {
            return Create(points, defaultBias, defaultTension, defaultInterpolationStep);
        }

        public IInterpolationStrategy Create(List<Point> points, double bias, double tension, double interpolationStep) {
            return new HermiteInterpolationStrategy(points, bias, tension, interpolationStep);
        }
    }
}