﻿using System.Collections.Generic;
using System.Windows;

using Colorware.Core.Algorithms.PointInterpolation.InterpolationStrategies;

namespace Colorware.Core.Algorithms.PointInterpolation.InterpolationFactories {
    public class SplineFitInterpolationStrategyFactory : IInterpolationStrategyFactory {
        public IInterpolationStrategy Create(List<Point> points) {
            return new SplineFitInterpolationStrategy(points);
        }
    }
}