using System.Collections.Generic;
using System.Linq;
using System.Windows;

using Colorware.Core.Algorithms.PointInterpolation.InterpolationFactories;
using Colorware.Core.Algorithms.PointInterpolation.InterpolationStrategies;

using JetBrains.Annotations;

namespace Colorware.Core.Algorithms.PointInterpolation {
    public class PointInterpolator {
        private readonly IInterpolationStrategyFactory strategyFactory;

        /// <summary>
        /// Current instance of the interpolation strategy.
        /// </summary>
        private IInterpolationStrategy strategy;

        /// <summary>
        /// States that the strategy should be recreated with the latest set of points.
        /// </summary>
        private bool strategyNeedsRecreation = true;

        public List<Point> Points { get; private set; }

        #region Constructors
        public PointInterpolator(IInterpolationStrategyFactory strategyFactory)
            : this(new List<Point>(), strategyFactory) {
        }

        public PointInterpolator(List<Point> points, IInterpolationStrategyFactory strategyFactory) {
            this.strategyFactory = strategyFactory;
            Points = points;
        }
        #endregion

        #region Collection modifiers
        public void Add(Point p) {
            Points.Add(p);
            strategyNeedsRecreation = true;
        }

        public void Remove(Point p) {
            Points.Remove(p);
            strategyNeedsRecreation = true;
        }

        public void AddRange(IEnumerable<Point> points) {
            Points.AddRange(points);
            strategyNeedsRecreation = true;
        }

        /// <summary>
        /// Select only the points that fit inside the given clipping area.
        /// </summary>
        /// <param name="minX">Part of clipping bounds.</param>
        /// <param name="maxX">Part of clipping bounds.</param>
        /// <param name="minY">Part of clipping bounds.</param>
        /// <param name="maxY">Part of clipping bounds.</param>
        public void ClipPoints(double minX, double maxX, double minY, double maxY) {
            Points = Points.Where(p =>
                p.X >= minX &&
                p.X <= maxX &&
                p.Y >= minY &&
                p.Y <= maxY)
                           .ToList();
            strategyNeedsRecreation = true;
        }
        #endregion

        [Pure]
        public Point FindPointForX(double x) {
            updateStrategyIfRequired();
            return strategy.FindPointForX(x);
        }

        /// <summary>
        /// Generates an interpolated point list for X values between 0 and 100 with
        /// the specified step width.
        /// </summary>
        /// <param name="xStep">The x step width.</param>
        [Pure]
        public List<InterpolatedPoint> GenerateInterpolatedPointList(double xStep) {
            updateStrategyIfRequired();
            return strategy.GenerateInterpolatedPointList(xStep);
        }

        private void updateStrategyIfRequired() {
            if (strategyNeedsRecreation) {
                strategy = strategyFactory.Create(Points);
                strategyNeedsRecreation = false;
            }
        }
    }
}