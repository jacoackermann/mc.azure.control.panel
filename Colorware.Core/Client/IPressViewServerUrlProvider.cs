﻿namespace Colorware.Core.Client {
    public interface IPressViewServerUrlProvider {
        string GetUrl();
    }
}