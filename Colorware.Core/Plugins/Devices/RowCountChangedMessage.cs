namespace Colorware.Core.Plugins.Devices {
    public class RowCountChangedMessage : IDeviceMessage {
        public int RowCount { get; }

        public RowCountChangedMessage(int rowCount) {
            RowCount = rowCount;
        }
    }
}