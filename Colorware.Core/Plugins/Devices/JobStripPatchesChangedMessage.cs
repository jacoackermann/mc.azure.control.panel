using System;
using System.Collections.Immutable;

using Colorware.Core.Data.Models;

using JetBrains.Annotations;

namespace Colorware.Core.Plugins.Devices {
    public class JobStripPatchesChangedMessage : IDeviceMessage {
        [NotNull]
        public IImmutableList<IJobStripPatch> JobStripPatches { get; }

        public JobStripPatchesChangedMessage([NotNull] IImmutableList<IJobStripPatch> jobStripPatches, int rowCount) {
            JobStripPatches = jobStripPatches ?? throw new ArgumentNullException(nameof(jobStripPatches));

            RowCount = rowCount > 0 ? rowCount : 1;

            PatchesPerRow = JobStripPatches.Count / RowCount;
        }

        public int RowCount { get; }

        public int PatchesPerRow { get; }
    }
}