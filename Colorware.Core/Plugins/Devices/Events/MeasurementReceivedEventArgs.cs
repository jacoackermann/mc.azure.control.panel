﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;

using Colorware.Core.Data.Models;

using JetBrains.Annotations;

namespace Colorware.Core.Plugins.Devices.Events {
    public class MeasurementReceivedEventArgs : EventArgs {
        /// <summary>
        /// Used by the device manager to check if the handling of measurement events should be stopped
        /// for the remaining handlers in the chain. This can be used in, for example, a popup to stop any other
        /// part of the interface from handling the measurement as long as the popup is active.
        /// </summary>
        public bool StopPropagation { get; set; }

        /// <summary>
        /// The Samples for this event.
        /// </summary>
        [NotNull]
        public IImmutableList<ISample> Samples { get; }

        public MeasurementSource MeasurementSource { get; }

        public MeasurementReceivedEventArgs([NotNull] IImmutableList<ISample> samples,
                                            MeasurementSource measurementSource) {
            Samples = samples ?? throw new ArgumentNullException(nameof(samples));
            MeasurementSource = measurementSource;
        }
    }

    public enum MeasurementSource {
        Device,
        Importer
    }
}