﻿using System;
using System.Collections.Immutable;
using System.Threading.Tasks;
using System.Windows.Media;

using Colorware.Core.Data.Models;
using Colorware.Core.Enums;
using Colorware.Core.Functional;
using Colorware.Core.Functional.Option;
using Colorware.Core.Functional.Result;
using Colorware.Core.Gui.Desktop.Popups;
using Colorware.Core.Plugins;
using Colorware.Core.Plugins.Devices;

using JetBrains.Annotations;

namespace Colorware.Core.Plugins.Devices {
    /// <summary>
    /// Implementations promise all Task-returning methods run non-blocking.
    /// 
    /// NB: Currently, if an implementation can't perform a certain set-function, it should just return Result.Ok
    /// to pretend it actually did perform it.
    /// </summary>
    // TODO: Device property getters currently have no use, so I've commented them out until we decide if we ever gonna need them
    // (BaseDevice stores a LastSetXXX for every property, it's flawed because the device can get out-of-sync with that value, but
    // it's what we have to use for now)
    public interface IDevice {
        [NotNull]
        IObservable<IImmutableList<ISample>> Measurements { get; }

        [NotNull]
        IObservable<Unit> Disconnected { get; }

        [NotNull]
        IObservable<Unit> Connected { get; }

        /// <summary>
        /// Indicates when a device starts its measuring cycle.
        /// 
        /// NB: Not all devices use this, generally only devices that perform long scanning measurements.
        /// </summary>
        IObservable<Unit> MeasurementStarted { get; }

        /// <summary>
        /// Indicates when the device stops meausring (should only occur after a matching MeasurementStarted).
        /// 
        /// NB: Not all devices use this, generally only devices that perform long scanning measurements.
        /// </summary>
        IObservable<Unit> MeasurementStopped { get; }

        bool IsConnected { get; }
        //        Task<Result<MeasurementMode>> GetMeasurementMode();
        Task<Result<Unit>> SetMeasurementMode(MeasurementMode measurementMode);

        //        Task<Result<Illuminant>> GetIlluminant();
        Task<Result<Unit>> SetIlluminant(Illuminant illuminant);

        //        Task<Result<Observer>> GetObserver();
        Task<Result<Unit>> SetObserver(Observer observer);

        //        Task<Result<MCondition>> GetDensityMCondition();
        Task<Result<Unit>> SetDensityMCondition(MCondition densityMCondition);

        //        Task<Result<MCondition>> GetSpectralMCondition();
        Task<Result<Unit>> SetSpectralMCondition(MCondition spectralMCondition);

        //        Task<Result<WhiteBase>> GetDensityWhiteBase();
        Task<Result<Unit>> SetDensityWhiteBase(WhiteBase whiteBase);

        //        Task<Result<DensityMethod>> GetDensityMethod();
        Task<Result<Unit>> SetDensityMethod(DensityMethod densityMethod);

        //        Task<Result<double>> GetPatchWidth();
        Task<Result<Unit>> SetPatchWidth(double patchWidth);

        //        Task<Result<double>> GetPatchHeight();
        Task<Result<Unit>> SetPatchHeight(double patchHeight);

        //        Task<Result<double>> GetScanLength();
        Task<Result<Unit>> SetScanLength(double scanLength);

        //        Task<Result<IMeasurementConditionsConfig>> GetMeasurementConditions();
        Task<Result<Unit>> SetMeasurementConditions([NotNull] IMeasurementConditionsConfig conditionsConfig);

        string Name { get; }
        string Serial { get; }
        string Status { get; }
        double StartWavelength { get; }
        double EndWavelength { get; }

        bool CanMeasureTriggered { get; }

        /// <summary>
        /// Can the device have its scanlength locked? SpectroJet only for now.
        /// </summary>
        bool CanLockScanLength { get; }

        /// <summary>
        /// Whether the device can be toggled between Spot and Row measurement mode or not (programmatically).
        /// </summary>
        bool CanToggleMeasurementMode { get; }

        Visual DevicePreview { get; }

        Task<Result<Unit>> Connect();
        Task<Result<Unit>> Disconnect();

        Task<Result<Unit>> TriggerMeasurement();


        /// <summary>
        /// Gets a value indicating whether this instance can calibrate.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance can calibrate; otherwise, <c>false</c>.
        /// </value>
        bool CanCalibrate { get; }

        /// <summary>
        /// True if sensor is calibrated.
        /// 
        /// NB: non-calibratable devices should always return true.
        /// </summary>
        bool IsSensorCalibrated { get; }

        /// <summary>
        /// True if substrate white is calibrated.
        /// 
        /// NB: non-substrate-calibratable devices should always return true.
        /// </summary>
        bool IsSubstrateWhiteCalibrated { get; }

        /// <summary>
        /// True if sensor is calibrated and substrate white is calibrated if the whitebase is relative.
        /// 
        /// This is what you want to check too see if the device is ready to perform a real measurement.
        /// </summary>
        bool IsFullyCalibratedForWhitebase { get; }

        /// <summary>
        /// Goes through the device's calibration procedure, but does not guarantee the device
        /// is actually calibrated when it is done. This is because it usually requires a lot of
        /// user interaction to perform proper calibration (and the user can cancel the procedure).
        /// 
        /// Check <see cref="IsSensorCalibrated"/>, <see cref="IsSubstrateWhiteCalibrated"/> or
        /// <see cref="IsFullyCalibratedForWhitebase"/> to see if the device is actually calibrated
        /// after the call.
        /// </summary>
        Task<Result<Unit>> TryCalibrate();

        Task ReceiveMessage([NotNull] IDeviceMessage message);

        Option<IOptionsPopupFactory<IInputOutputPopupViewModel<Unit, Unit>>> OptionsScreenFactory { get; }

        MeasurementMode LastSetMeasurementMode { get; }
        Illuminant LastSetIlluminant { get; }
        Observer LastSetObserver { get; }
        MCondition LastSetDensityMCondition { get; }
        MCondition LastSetSpectralMCondition { get; }
        WhiteBase LastSetDensityWhiteBase { get; }
        DensityMethod LastSetDensityMethod { get; }
        double LastSetPatchWidth { get; }
        double LastSetPatchHeight { get; }
        double LastSetScanLength { get; }
    }
}

public static class IDeviceExtensions {
    public static string GetId([NotNull] this IDevice device) {
        if (device == null) throw new ArgumentNullException(nameof(device));

        return device.GetType().GetComponentId();
    }
}