﻿using System;
using System.Globalization;

using Colorware.Core.Config;
using Colorware.Core.Enums;
using Colorware.Core.Functional.Option;

using JetBrains.Annotations;

namespace Colorware.Core.Plugins.Devices {
    [UsedImplicitly]
    public class DeviceCalibrationLog : IDeviceCalibrationLog {
        private const string ConfigBaseKey = "Devices.CalibrationLog.Last.Absolute.";

        private readonly IConfig config;

        public DeviceCalibrationLog([NotNull] IConfig config) {
            this.config = config ?? throw new ArgumentNullException(nameof(config));
        }

        public Option<DeviceCalibrationLogEntry> GetLastAbsoluteCalibration(string deviceComponentId) {
            if (deviceComponentId == null) throw new ArgumentNullException(nameof(deviceComponentId));

            var configKey = makeAbsoluteCalibrationConfigKey(deviceComponentId);

            return config.Get(configKey).ToOption().Select(deserializeAbsoluteCalibration);
        }

        public void LogAbsoluteCalibration(string deviceComponentId, MCondition mCondition) {
            if (deviceComponentId == null) throw new ArgumentNullException(nameof(deviceComponentId));

            var configKey = makeAbsoluteCalibrationConfigKey(deviceComponentId);

            config.Set(configKey, serializeAbsoluteCalibration(mCondition, DateTime.Now));
        }

        private string makeAbsoluteCalibrationConfigKey([NotNull] string deviceComponentId) {
            return $"{ConfigBaseKey}{deviceComponentId}";
        }

        private string serializeAbsoluteCalibration(MCondition mCondition, DateTime timestamp) {
            return $"{mCondition}|{timestamp.ToString("O", CultureInfo.InvariantCulture)}";
        }

        private DeviceCalibrationLogEntry deserializeAbsoluteCalibration(
            [NotNull] string serializedAbsoluteCalibration) {
            var parts = serializedAbsoluteCalibration.Split('|');
            if (parts.Length != 2)
                throw new Exception(
                    "Serialized absolute device calibration not in correct format! (must have 2 parts separated by a '|')");

            if (!Enum.TryParse(parts[0], out MCondition mCondition))
                throw new Exception("Could not parse M-condition specified in serialized absolute device calibration!");

            if (!DateTime.TryParse(parts[1], CultureInfo.InvariantCulture, DateTimeStyles.None, out var timestamp))
                throw new Exception("Could not parse timestamp specified in serialized absolute device calibration!");

            return new DeviceCalibrationLogEntry(mCondition, timestamp);
        }
    }
}