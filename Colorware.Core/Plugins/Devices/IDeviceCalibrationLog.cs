﻿using System;

using Colorware.Core.Enums;
using Colorware.Core.Functional.Option;

using JetBrains.Annotations;

namespace Colorware.Core.Plugins.Devices {
    public interface IDeviceCalibrationLog {
        Option<DeviceCalibrationLogEntry> GetLastAbsoluteCalibration([NotNull] string deviceComponentId);

        void LogAbsoluteCalibration([NotNull] string deviceComponentId, MCondition mCondition);
    }

    public static class IDeviceCalibrationLogExtensions {
        public static Option<DeviceCalibrationLogEntry> GetLastAbsoluteCalibration(
            [NotNull] this IDeviceCalibrationLog deviceCalibrationLog,
            [NotNull] IDevice device) {
            if (device == null) throw new ArgumentNullException(nameof(device));

            return deviceCalibrationLog.GetLastAbsoluteCalibration(device.GetId());
        }

        public static void LogAbsoluteCalibration([NotNull] this IDeviceCalibrationLog deviceCalibrationLog,
                                                  [NotNull] IDevice device,
                                                  MCondition mCondition) {
            if (device == null) throw new ArgumentNullException(nameof(device));

            deviceCalibrationLog.LogAbsoluteCalibration(device.GetId(), mCondition);
        }
    }
}