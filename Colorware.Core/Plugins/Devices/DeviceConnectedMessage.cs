﻿using System;

using JetBrains.Annotations;

namespace Colorware.Core.Plugins.Devices {
    public class DeviceConnectedMessage : IDeviceMessage {
        public string DeviceName { get; }

        public DeviceConnectedMessage([NotNull] string deviceName) {
            DeviceName = deviceName ?? throw new ArgumentNullException(nameof(deviceName));
        }
    }
}