﻿using System;

using Colorware.Core.Enums;

namespace Colorware.Core.Plugins.Devices {
    public class DeviceCalibrationLogEntry {
        public MCondition MCondition { get; }
        public DateTime Timestamp { get; }

        public DeviceCalibrationLogEntry(MCondition mCondition, DateTime timestamp) {
            MCondition = mCondition;
            Timestamp = timestamp;
        }
    }
}