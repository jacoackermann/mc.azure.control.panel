﻿using System;
using System.Collections.Immutable;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Colorware.Core.Data.Models;
using Colorware.Core.Enums;
using Colorware.Core.Extensions;
using Colorware.Core.Functional;
using Colorware.Core.Functional.Option;
using Colorware.Core.Functional.Result;
using Colorware.Core.Gui.Desktop.Popups;

using ReactiveUI;

namespace Colorware.Core.Plugins.Devices {
    /// <summary>
    /// Implements basic functions of the IDevice interface and makes some assumptions about
    /// defaults.
    /// </summary>
    public abstract class BaseDevice : ReactiveObject, IDevice, IDisposable {
        // TODO: this can be removed eventually, when all devices directly hook up their source of
        // measurements to Measurements (just an interim refactoring helper for now)
        protected readonly ISubject<IImmutableList<ISample>> measurements = new Subject<IImmutableList<ISample>>();

        protected BaseDevice() {
            Measurements = measurements;

            Connected = this.WhenAnyValue(x => x.IsConnected)
                            .Where(v => v)
                            .Select(_ => Unit.Default)
                            .Publish()
                            .RefCount();

            Disconnected = this.WhenAnyValue(x => x.IsConnected)
                               .Where(v => !v)
                               .Select(_ => Unit.Default)
                               .Publish()
                               .RefCount();

            this.WhenAnyValue(x => x.IsSensorCalibrated, x => x.IsSubstrateWhiteCalibrated,
                              x => x.LastSetDensityWhiteBase,
                              (sensorCalibrated, pwCalibrated, whitebase) =>
                              sensorCalibrated && (whitebase == WhiteBase.Absolute || pwCalibrated))
                .ToProperty(this, x => x.IsFullyCalibratedForWhitebase, out isFullyCalibratedForWhitebase, false,
                            ImmediateScheduler.Instance);


            Disconnected.Subscribe(_ => { Serial = "N/A"; });
        }

        #region IDevice Members
        public IObservable<IImmutableList<ISample>> Measurements { get; protected set; }
        public IObservable<Unit> Connected { get; }
        public IObservable<Unit> Disconnected { get; }
        public IObservable<Unit> MeasurementStarted { get; protected set; } = Observable.Never<Unit>();
        public IObservable<Unit> MeasurementStopped { get; protected set; } = Observable.Never<Unit>();

        private bool isConnected;

        public bool IsConnected {
            get => isConnected;
            set => this.RaiseAndSetIfChanged(ref isConnected, value);
        }

        private MeasurementMode lastSetMeasurementMode = MeasurementMode.Undefined;

        public MeasurementMode LastSetMeasurementMode {
            get => lastSetMeasurementMode;
            private set => this.RaiseAndSetIfChanged(ref lastSetMeasurementMode, value);
        }

        //        public abstract Task<Result<MeasurementMode>> GetMeasurementMode();

        public async Task<Result<Unit>> SetMeasurementMode(MeasurementMode measurementMode) {
            var r = await setMeasurementMode(measurementMode).Caf();

            if (r.IsOk)
                LastSetMeasurementMode = measurementMode;

            return r;
        }

        protected abstract Task<Result<Unit>> setMeasurementMode(MeasurementMode measurementMode);

        private Illuminant lastSetIlluminant = Illuminant.Unknown;

        public Illuminant LastSetIlluminant {
            get => lastSetIlluminant;
            private set => this.RaiseAndSetIfChanged(ref lastSetIlluminant, value);
        }

        //        public abstract Task<Result<Illuminant>> GetIlluminant();

        public async Task<Result<Unit>> SetIlluminant(Illuminant illuminant) {
            var r = await setIlluminant(illuminant).Caf();

            if (r.IsOk)
                LastSetIlluminant = illuminant;

            return r;
        }

        protected abstract Task<Result<Unit>> setIlluminant(Illuminant illuminant);

        private Observer lastSetObserver = Observer.Unknown;

        public Observer LastSetObserver {
            get => lastSetObserver;
            private set => this.RaiseAndSetIfChanged(ref lastSetObserver, value);
        }

        //        public abstract Task<Result<Observer>> GetObserver();

        public async Task<Result<Unit>> SetObserver(Observer observer) {
            var r = await setObserver(observer).Caf();

            if (r.IsOk)
                LastSetObserver = observer;

            return r;
        }

        protected abstract Task<Result<Unit>> setObserver(Observer observer);

        private MCondition lastSetDensityMCondition = MCondition.M0;

        public MCondition LastSetDensityMCondition {
            get => lastSetDensityMCondition;
            private set => this.RaiseAndSetIfChanged(ref lastSetDensityMCondition, value);
        }

        //        public abstract Task<Result<MCondition>> GetDensityMCondition();

        public async Task<Result<Unit>> SetDensityMCondition(MCondition densityMCondition) {
            var r = await setDensityMCondition(densityMCondition).Caf();

            if (r.IsOk) {
                LastSetDensityMCondition = densityMCondition;
                IsSubstrateWhiteCalibrated = false;
            }

            return r;
        }

        protected abstract Task<Result<Unit>> setDensityMCondition(MCondition densityMCondition);

        private MCondition lastSetSpectralMCondition = MCondition.M0;

        public MCondition LastSetSpectralMCondition {
            get => lastSetSpectralMCondition;
            private set => this.RaiseAndSetIfChanged(ref lastSetSpectralMCondition, value);
        }

        //        public abstract Task<Result<MCondition>> GetSpectralMCondition();

        public virtual async Task<Result<Unit>> SetSpectralMCondition(MCondition spectralMCondition) {
            var r = await setSpectralMCondition(spectralMCondition).Caf();

            if (r.IsOk) {
                LastSetSpectralMCondition = spectralMCondition;
                IsSensorCalibrated = false;
                IsSubstrateWhiteCalibrated = false;
            }

            return r;
        }

        protected abstract Task<Result<Unit>> setSpectralMCondition(MCondition spectralMCondition);

        private WhiteBase lastSetDensityWhiteBase = WhiteBase.Undefined;

        public WhiteBase LastSetDensityWhiteBase {
            get => lastSetDensityWhiteBase;
            private set => this.RaiseAndSetIfChanged(ref lastSetDensityWhiteBase, value);
        }

        //        public abstract Task<Result<WhiteBase>> GetDensityWhiteBase();

        public async Task<Result<Unit>> SetDensityWhiteBase(WhiteBase whiteBase) {
            var r = await setDensityWhiteBase(whiteBase).Caf();

            if (r.IsOk)
                LastSetDensityWhiteBase = whiteBase;

            return r;
        }

        protected abstract Task<Result<Unit>> setDensityWhiteBase(WhiteBase whiteBase);

        private DensityMethod lastSetDensityMethod = DensityMethod.Undefined;

        public DensityMethod LastSetDensityMethod {
            get => lastSetDensityMethod;
            private set => this.RaiseAndSetIfChanged(ref lastSetDensityMethod, value);
        }

        //        public abstract Task<Result<DensityMethod>> GetDensityMethod();

        public async Task<Result<Unit>> SetDensityMethod(DensityMethod densityMethod) {
            var r = await setDensityMethod(densityMethod).Caf();

            if (r.IsOk) {
                LastSetDensityMethod = densityMethod;
                IsSubstrateWhiteCalibrated = false;
            }

            return r;
        }

        protected abstract Task<Result<Unit>> setDensityMethod(DensityMethod densityMethod);

        private double lastSetPatchWidth = double.NaN;

        public double LastSetPatchWidth {
            get => lastSetPatchWidth;
            private set => this.RaiseAndSetIfChanged(ref lastSetPatchWidth, value);
        }

        //        public abstract Task<Result<double>> GetPatchWidth();

        public async Task<Result<Unit>> SetPatchWidth(double patchWidth) {
            var r = await setPatchWidth(patchWidth).Caf();

            if (r.IsOk)
                LastSetPatchWidth = patchWidth;

            return r;
        }

        protected abstract Task<Result<Unit>> setPatchWidth(double patchWidth);

        private double lastSetPatchHeight = double.NaN;

        public double LastSetPatchHeight {
            get => lastSetPatchHeight;
            private set => this.RaiseAndSetIfChanged(ref lastSetPatchHeight, value);
        }

        //        public abstract Task<Result<double>> GetPatchHeight();

        public async Task<Result<Unit>> SetPatchHeight(double patchHeight) {
            var r = await setPatchHeight(patchHeight).Caf();

            if (r.IsOk)
                LastSetPatchHeight = patchHeight;

            return r;
        }

        protected abstract Task<Result<Unit>> setPatchHeight(double patchHeight);

        private double lastSetScanLength = double.NaN;

        public double LastSetScanLength {
            get => lastSetScanLength;
            private set => this.RaiseAndSetIfChanged(ref lastSetScanLength, value);
        }

        //        public abstract Task<Result<double>> GetScanLength();

        public async Task<Result<Unit>> SetScanLength(double scanLength) {
            var r = await setScanLength(scanLength).Caf();

            if (r.IsOk)
                LastSetScanLength = scanLength;

            return r;
        }

        protected abstract Task<Result<Unit>> setScanLength(double scanLength);

        ///<summary>Devices should try to implement this with one call to the physical device if possible</summary>
        //        public virtual Task<Result<IMeasurementConditionsConfig>> GetMeasurementConditions() {
        //            return Result.FromAll(
        //                GetDensityMethod, GetIlluminant, GetObserver, GetDensityMCondition, GetSpectralMCondition,
        //                GetUseDensityPolFilter, GetUseSpectralPolFilter, GetWhiteBase,
        //                (densityMethod, illuminant, observer, densityMCondition, spectralMCondition, useDensityPolFilter,
        //                 useSpectralPolFilter, whiteBase) =>
        //                Task.FromResult(Result.Ok(
        //                    (IMeasurementConditionsConfig)new MeasurementConditionsConfig {
        //                        DensityStandard = densityMethod,
        //                        Illuminant = illuminant,
        //                        Observer = observer,
        //                        DensityMCondition = densityMCondition,
        //                        SpectralMCondition = spectralMCondition,
        //                        UseDensityPolFilter = useDensityPolFilter,
        //                        UseSpectralPolFilter = useSpectralPolFilter,
        //                        WhiteBase = whiteBase
        //                    })));
        //        }
        ///<summary>Devices should try to implement this with one call to the physical device if possible</summary>
        public virtual async Task<Result<Unit>> SetMeasurementConditions(IMeasurementConditionsConfig conditionsConfig) {
            if (conditionsConfig == null) throw new ArgumentNullException(nameof(conditionsConfig));

            // NB: concurrent execution isn't supported by most/all devices, so we do it one at a time
            return new[] {
                await SetDensityMethod(conditionsConfig.DensityStandard).Caf(),
                await SetIlluminant(conditionsConfig.Illuminant).Caf(),
                await SetObserver(conditionsConfig.Observer).Caf(),
                await SetDensityMCondition(conditionsConfig.DensityMCondition).Caf(),
                await SetSpectralMCondition(conditionsConfig.SpectralMCondition).Caf(),
                await SetDensityWhiteBase(conditionsConfig.DensityWhiteBase).Caf()
            }.Reduce();
        }

        public abstract string Name { get; }

        private string serial = "N/A";

        public string Serial {
            get => serial;
            set => this.RaiseAndSetIfChanged(ref serial, value);
        }

        private string status;

        public string Status {
            get => status;
            protected set => this.RaiseAndSetIfChanged(ref status, value);
        }

        public abstract double StartWavelength { get; }
        public abstract double EndWavelength { get; }

        private bool isSensorCalibrated;

        public virtual bool IsSensorCalibrated {
            get => isSensorCalibrated;
            set => this.RaiseAndSetIfChanged(ref isSensorCalibrated, value);
        }

        private bool isSubstrateWhiteCalibrated;

        public virtual bool IsSubstrateWhiteCalibrated {
            get => isSubstrateWhiteCalibrated;
            set => this.RaiseAndSetIfChanged(ref isSubstrateWhiteCalibrated, value);
        }

        private readonly ObservableAsPropertyHelper<bool> isFullyCalibratedForWhitebase;

        public virtual bool IsFullyCalibratedForWhitebase => isFullyCalibratedForWhitebase.Value;

        public virtual bool CanMeasureTriggered => false;

        public virtual bool CanLockScanLength => false;

        public virtual bool CanToggleMeasurementMode => false;

        public virtual Visual DevicePreview => new TextBlock {Text = "TODO"};

        /// <summary>
        /// Helper method to load a preview visual.
        /// </summary>
        /// <param name="location">
        /// The location of the image to load. Example syntax: "pack://application:,,,/EyeOneDevicePlugin;component/Assets/EyeOne.png"
        /// </param>
        /// <returns>A visual which can be used as a preview for the device.</returns>
        protected Visual loadImage(string location) {
            var bmImage = new BitmapImage();
            bmImage.BeginInit();
            bmImage.UriSource = new Uri(location, UriKind.Absolute);
            bmImage.EndInit();
            bmImage.Freeze();
            var container = new Grid();
            var bg = new ImageBrush(bmImage);
            bg.Stretch = Stretch.Uniform;
            bg.Freeze();
            container.Background = bg;
            container.Width = 50;
            container.Height = 50;
            return container;
        }

        public abstract Task<Result<Unit>> Connect();
        public abstract Task<Result<Unit>> Disconnect();

        public abstract Task<Result<Unit>> TriggerMeasurement();

        public virtual Task ReceiveMessage(IDeviceMessage message) {
            return Task.CompletedTask;
        }

        // TODO: remove eventually (see comment near 'measurements' field)
        protected void invokeMeasurementReceived(IImmutableList<ISample> samples) {
            measurements.OnNext(samples);
        }

        public override string ToString() {
            return Name;
        }
        #endregion

        public virtual Option<IOptionsPopupFactory<IInputOutputPopupViewModel<Unit, Unit>>>
            OptionsScreenFactory { get; } = Option<IOptionsPopupFactory<IInputOutputPopupViewModel<Unit, Unit>>>.Nothing
            ;

        public virtual bool CanCalibrate => false;

        public virtual Task<Result<Unit>> TryCalibrate() {
            throw new InvalidOperationException(
                $"Device '{Name}' does not support calibration, don't call {nameof(TryCalibrate)}()!");
        }

        public virtual void Dispose() {
            measurements.OnCompleted();
        }
    }
}