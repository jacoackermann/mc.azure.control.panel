﻿using System;

using JetBrains.Annotations;

namespace Colorware.Core.Plugins.Devices {
    public class DiscoveredDevice {
        [NotNull]
        public Type Id { get; }

        [NotNull]
        public string HumanName { get; }

        public DiscoveredDevice([NotNull] Type id, [NotNull] string humanName) {
            Id = id;
            HumanName = humanName ?? throw new ArgumentNullException(nameof(humanName));
        }
    }
}