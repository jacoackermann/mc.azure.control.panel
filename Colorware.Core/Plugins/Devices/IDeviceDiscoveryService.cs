﻿using System.Collections.Immutable;

using JetBrains.Annotations;

namespace Colorware.Core.Plugins.Devices {
    /// <summary>
    /// Discover which devices are available.
    /// </summary>
    public interface IDeviceDiscoveryService {
        [ItemNotNull, NotNull]
        IImmutableList<DiscoveredDevice> Discover();
    }
}