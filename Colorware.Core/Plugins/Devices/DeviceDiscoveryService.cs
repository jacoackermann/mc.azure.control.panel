using System;
using System.Collections.Immutable;
using System.Linq;

using Castle.Windsor;

using Colorware.Core.Globalization;

using JetBrains.Annotations;

namespace Colorware.Core.Plugins.Devices {
    [UsedImplicitly]
    public class DeviceDiscoveryService : IDeviceDiscoveryService {
        private const string DeviceNameTranslationBaseKey = "CGUI.Devices.";

        private readonly IWindsorContainer container;
        private readonly ILanguageManager languageManager;
        private readonly IComponentActiveChecker componentActiveChecker;

        public DeviceDiscoveryService([NotNull] IWindsorContainer container,
                                      [NotNull] ILanguageManager languageManager,
                                      [NotNull] IComponentActiveChecker componentActiveChecker) {
            this.container = container ?? throw new ArgumentNullException(nameof(container));
            this.languageManager = languageManager ?? throw new ArgumentNullException(nameof(languageManager));
            this.componentActiveChecker = componentActiveChecker ??
                                          throw new ArgumentNullException(nameof(componentActiveChecker));
        }

        public IImmutableList<DiscoveredDevice> Discover() {
            var deviceIds = getActiveDeviceTypesFromContainer();
            return deviceIds.Select(type => new DiscoveredDevice(type, getDeviceHumanName(type))).ToImmutableList();
        }

        [NotNull]
        private string getDeviceHumanName([NotNull] Type deviceType) {
            var translationKey = DeviceNameTranslationBaseKey + deviceType.GetComponentId();
            return languageManager._(translationKey);
        }

        [ItemNotNull, NotNull]
        private IImmutableList<Type> getActiveDeviceTypesFromContainer() {
            return
                container.Kernel.GetAssignableHandlers(typeof(IDevice))
                         .Select(deviceHandler => deviceHandler.ComponentModel.Implementation.UnderlyingSystemType)
                         .Where(componentActiveChecker.IsActive)
                         .ToImmutableList();
        }
    }
}