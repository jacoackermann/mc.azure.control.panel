﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;

using Castle.Core.Logging;

using Colorware.Core.Data.Models;
using Colorware.Core.Enums;
using Colorware.Core.Extensions;
using Colorware.Core.Functional;
using Colorware.Core.Functional.Result;
using Colorware.Core.Plugins.Devices.Events;

using JetBrains.Annotations;

namespace Colorware.Core.Plugins.Devices {
    public delegate void MeasurementReceivedEventHandler(object sender, MeasurementReceivedEventArgs e);

    // TODO: callers should check result of setters to see if the device was able to set e.g. a
    // measurement condition, and not allow measurements if the device wasn't able to. For now we
    // have to handle this in the devices themselves :/
    public interface IDeviceManager {
        Task Initialization { get; }

        Task Cleanup();

        [NotNull]
        IImmutableList<IDevice> Devices { get; }

        [NotNull]
        IImmutableList<IDevice> ConnectedDevices { get; }

        [CanBeNull]
        IDevice SelectedDevice { get; set; }

        /// <summary>
        /// Returns true if there is a connected device capable of triggering a(n auto) measurement.
        /// </summary>
        bool IsTriggeredMeasureDeviceActive { get; }

        MeasurementMode MeasurementMode { get; }
        Task<Result<Unit>> SetMeasurementMode(MeasurementMode measurementMode);

        Illuminant Illuminant { get; }
        Task<Result<Unit>> SetIlluminant(Illuminant illuminant);

        Observer Observer { get; }
        Task<Result<Unit>> SetObserver(Observer observer);

        MCondition DensityMCondition { get; }
        Task<Result<Unit>> SetDensityMCondition(MCondition densityMCondition);

        MCondition SpectralMCondition { get; }
        Task<Result<Unit>> SetSpectralMCondition(MCondition spectralMCondition);

        WhiteBase DensityWhiteBase { get; }
        Task<Result<Unit>> SetDensityWhiteBase(WhiteBase densityWhiteBase);

        DensityMethod DensityMethod { get; }
        Task<Result<Unit>> SetDensityMethod(DensityMethod densityMethod);

        double PatchWidth { get; }
        Task<Result<Unit>> SetPatchWidth(double patchWidth);

        double PatchHeight { get; }
        Task<Result<Unit>> SetPatchHeight(double patchHeight);

        double ScanLength { get; }
        Task<Result<Unit>> SetScanLengthRespectingLock(double scanLength);

        /// <summary>
        /// Like ScanLength property, but does not perform checking of scanlength lock and will thus always
        /// set the scanlength value.
        /// </summary>
        Task<Result<Unit>> SetScanLengthIgnoringLock(double scanLength);

        /// <summary>
        /// If the scanlength is locked it can only be updated when the current setting is reset.
        /// </summary>
        bool ScanLengthLocked { get; set; }

        // TODO: refactor to IObservable
        event MeasurementReceivedEventHandler MeasurementReceived;
        IObservable<IDevice> DeviceConnected { get; }
        IObservable<IDevice> DeviceDisconnected { get; }

        [NotNull]
        IMeasurementConditionsConfig MeasurementConditionsConfig { get; }

        /// <summary>
        /// Broadcast a message to all device plugins. It does not matter if the device
        /// is connected or not, the command will be sent.
        /// </summary>
        Task BroadcastMessage([NotNull] IDeviceMessage message);

        void ReceiveMeasurement(object sender, MeasurementReceivedEventArgs args);

        /// <summary>
        /// Try to find a device which is capable of triggering a measurement and call its triggering method.
        /// </summary>
        Task TriggerMeasurement();

        Task<Result<Unit>> RecalculateAndSetScanLength(int patchCount, int rowCount, double patchWidthToUse);
    }

    public static class IDeviceManagerExtensions {
        public static Task<Result<Unit>> RecalculateAndSetScanLength(
            [NotNull] this IDeviceManager deviceManager,
            [NotNull] IColorStrip cStrip,
            [NotNull] IEnumerable<IJobStripPatch> jsPatches) {
            if (cStrip == null) throw new ArgumentNullException(nameof(cStrip));
            if (jsPatches == null) throw new ArgumentNullException(nameof(jsPatches));

            return deviceManager.RecalculateAndSetScanLength(jsPatches.Count(),
                                                             cStrip.RowCount,
                                                             cStrip.PatchWidth);
        }

        public static Task<Result<Unit>> LockScanLengthToNumberOfPatches([NotNull] this IDeviceManager deviceManager,
                                                                         int numberOfPatches) {
            deviceManager.ScanLengthLocked = true;
            return deviceManager.SetScanLengthFromNumberOfPatches(numberOfPatches);
        }

        public static Task<Result<Unit>> SetScanLengthFromNumberOfPatches([NotNull] this IDeviceManager deviceManager,
                                                                          int numberOfPatches) {
            return deviceManager.SetScanLengthRespectingLock((numberOfPatches + 2) * deviceManager.PatchWidth);
        }

        public static Task<Result<Unit>> SetScanLengthOverrideFromNumberOfPatches(
            [NotNull] this IDeviceManager deviceManager,
            int numberOfPatches) {
            return deviceManager.SetScanLengthIgnoringLock((numberOfPatches + 2) * deviceManager.PatchWidth);
        }

        /// <summary>
        /// Update the measurement condition settings for all devices.
        /// 
        /// This calls all property setters individually instead of IDevice.SetMeasurementConditions(),
        /// because properties often already have the values we want to set them to (the individual
        /// setters skip them in that case).
        /// </summary>
        public static async Task<Result<Unit>> SetMeasurementConditions(
            [NotNull] this IDeviceManager dm,
            [NotNull] IMeasurementConditionsConfig conditionsConfig) {
            if (conditionsConfig == null) throw new ArgumentNullException(nameof(conditionsConfig));

            GlobalContainer.Current.Resolve<ILogger>()
                           .Info($"Setting measurement conditions {conditionsConfig.ToLogString()}");

            // NB: concurrent execution isn't supported by most/all devices, so we do it one at a time
            return new[] {
                await dm.SetDensityMethod(conditionsConfig.DensityStandard).Caf(),
                await dm.SetIlluminant(conditionsConfig.Illuminant).Caf(),
                await dm.SetObserver(conditionsConfig.Observer).Caf(),
                await dm.SetDensityMCondition(conditionsConfig.DensityMCondition).Caf(),
                await dm.SetSpectralMCondition(conditionsConfig.SpectralMCondition).Caf(),
                await dm.SetDensityWhiteBase(conditionsConfig.DensityWhiteBase).Caf()
            }.Reduce();
        }
    }
}