﻿using System;
using System.Collections.Generic;

using Colorware.Core.Functional.Option;
using Colorware.Core.Plugins.Devices;

using JetBrains.Annotations;

namespace Colorware.Core.Plugins {
    public interface IComponentActiveChecker {
        /// <summary>
        /// Checks if there is a marker for the given id that indicates that the component with said id
        /// is on or off.
        /// </summary>
        /// <param name="id">A component  id that is to be checked.</param>
        /// <returns><c>True</c> if the component should be active, <c>false</c> otherwise.</returns>
        bool IsActive([NotNull] string id);
    }

    public static class IComponentActiveCheckerExtensions {
        /// <summary>
        /// Helper method that generates a simple id from a given type and uses that to check if the
        /// type should be active.
        /// </summary>
        /// <param name="activeChecker">Checker to use for checking the active state.</param>
        /// <param name="componentType">Component to generate an id for.</param>
        public static bool IsActive(this IComponentActiveChecker activeChecker, [NotNull] Type componentType) {
            if (componentType == null) throw new ArgumentNullException(nameof(componentType));

            var componentId = componentType.GetComponentId();

            // At the time of writing (2017-08-17), device enabled/disabled setting is stored in the
            // registry using their old id (from before they were annotated with a ComponentIdAttribute). So
            // we need to use this old id, if available.
            if (typeof(IDevice).IsAssignableFrom(componentType))
                componentId = getOldDeviceId(componentType).OrElse(componentId);

            return activeChecker.IsActive(componentId);
        }

        private static Option<string> getOldDeviceId([NotNull] Type deviceType) {
            if (!typeof(IDevice).IsAssignableFrom(deviceType))
                throw new ArgumentException($"Type must implement {nameof(IDevice)} to get the old style ID!");

            // NB: Strings on the left must match the ComponentId names on the Device classes
            var newToOldIdMappings = new Dictionary<string, string> {
                {"Exact-2017-1", "ExactDevicePlugin.Plugin"},
                {"i1Pro-2017-1", "EyeOneDevicePlugin.Plugin"},
                {"FiveHundred-2017-1", "FiveHundredSeriesDevicePlugin.Plugin"},
                // There was also an Itx2DevicePlugin.Plugin, but we can't map 2 devices to one in this direction
                // so any old ITX2 key is in effect ignored
                {"IntelliTrax-2017-1", "ItxDevicePlugin.Plugin"},
                {"SpectroEye-2017-1", "SpectroEyeDevicePlugin.Plugin"},
                // The Techkon plugins used to be contained in one plugin, we can map them all to that
                // plugin since if we find an old Techkon entry in the registry, it means the user
                // wanted to disabled ALL techkon devices.
                {"SpectroDens-2017-1", "TechkonDevicePlugin.Plugin"},
                {"SpectroDrive-2017-1", "TechkonDevicePlugin.Plugin"},
                {"SpectroJet-2017-1", "TechkonDevicePlugin.Plugin"},
            };

            return newToOldIdMappings.TryGetValue(deviceType.GetComponentId(), out string oldId)
                       ? oldId.ToOption()
                       : Option<string>.Nothing;
        }
    }
}