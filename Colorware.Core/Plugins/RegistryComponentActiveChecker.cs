﻿using System;

using JetBrains.Annotations;

using Microsoft.Win32;

namespace Colorware.Core.Plugins {
    [UsedImplicitly]
    public class RegistryComponentActiveChecker : IComponentActiveChecker {
        private const string BaseRegistryKey = @"Software\Colorware\DisabledPlugins";

        /// <inheritdoc/>
        /// <remarks>
        /// Currently this is a blacklist based implementation. If an entry exists for the specified
        /// id, the plugin will be considered as inactive if the value of the entry is "", "0" or
        /// "false". Other values for the entry are currently inconsequential and will be considered as 'active'.
        /// </remarks>
        public bool IsActive(string id) {
            if (id == null) throw new ArgumentNullException(nameof(id));

            try {
                //var key = Registry.LocalMachine.OpenSubKey(BaseRegistryKey, RegistryKeyPermissionCheck.ReadSubTree, RegistryRights.ReadKey);
                var baseKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
                var key = baseKey.OpenSubKey(BaseRegistryKey);
                if (key == null) {
                    // Item is active if there is no key.
                    return true;
                }
                var value = key.GetValue(id, null) as string;
                key.Close();
                if (value == null) {
                    // Item is not in the registry, so it is active.
                    return true;
                }
                if (value == "" || value == "0" || value.ToLowerInvariant() == "false") {
                    // Item is inactive if it is "", "0" or "false".
                    return false;
                }
                // In all other cases item is deemed to be active.
                return true;
            }
            catch {
                // If an error occurs, the item is deemed to be active.
                return true;
            }
        }
    }
}