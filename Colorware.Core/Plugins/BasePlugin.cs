﻿using System.Threading.Tasks;

namespace Colorware.Core.Plugins {
    public abstract class BasePlugin : IPlugin {
        public abstract Task Register();

        public virtual Task Unregister() {
            return Task.CompletedTask;
        }
    }
}