﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;

using Castle.Core.Logging;

using Colorware.Core.Functional.Option;
using Colorware.Core.Gui.Desktop;
using Colorware.Core.Mvvm;

using JetBrains.Annotations;

using ReactiveUI;

namespace Colorware.Core.Plugins.Modules {
    public interface IModule : IClosable, IReactiveObject {
        string Name { get; }
        bool IsActive { get; }
        [CanBeNull]
        IViewModel Control { get; }
        string Description { get; }
        VisualBrush Icon { get; }

        /// <summary>
        /// The type of the options screen view model, if any.
        /// 
        /// MUST inherit from IInputOutputPopupViewModel{Unit, Unit}!
        /// </summary>
        Option<Type> OptionsScreenType { get; }
    }

    public static class IModuleExtensions {
        public static string GetId([NotNull] this IModule module) {
            if (module == null) throw new ArgumentNullException(nameof(module));

            return module.GetType().GetComponentId();
        }

        public static IModule GetModuleById(this IEnumerable<IModule> modules, [NotNull] string id) {
            if (id == null) throw new ArgumentNullException(nameof(id));

            var foundModule = modules.FirstOrDefault(m => m.GetId() == id);

            if (foundModule == null)
                GlobalContainer.Current.Resolve<ILogger>().Warn($"Module with id '{id}' not found");

            return foundModule;
        }
    }
}