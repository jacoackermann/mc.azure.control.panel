﻿using System;

namespace Colorware.Core.Plugins.Modules {
    /// <summary>
    /// Designates that a module can be used by a remote user. This is whitelist based, so if this
    /// attribute is missing from a module, it will not be usable.
    /// </summary>
    public class AllowsRemoteUserAttribute : Attribute {
    }
}