﻿using System;
using System.Reflection;

using JetBrains.Annotations;

namespace Colorware.Core.Plugins {
    /// <summary>
    /// Unique identifier to use when referring to e.g. a Device/Module/Plugin in e.g. config settings.
    /// Use  this instead of e.g. a Name property!
    /// 
    /// Useful when e.g. a new device model is introduced with the same name, but for which
    /// we need to create a different implementation (and be able to distinguish between
    /// them). Also useful when you want to rename a class and still refer to it using the
    /// same ID in config.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ComponentIdAttribute : Attribute {
        public string UniqueId { get; }

        public ComponentIdAttribute([NotNull] string uniqueId) {
            UniqueId = uniqueId ?? throw new ArgumentNullException(nameof(uniqueId));
        }
    }

    public static class ComponentIdAttributeExtensions {
        public static string GetComponentId([NotNull] this Type type) {
            if (type == null) throw new ArgumentNullException(nameof(type));

            var idAttribute = type.GetCustomAttribute<ComponentIdAttribute>();
            if (idAttribute == null)
                throw new InvalidOperationException($"Could not get ComponentId from type '{type.FullName}'");

            return idAttribute.UniqueId;
        }
    }
}