﻿using System;
using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.Plugins {
    public interface IPlugin {
        Task Register();
        Task Unregister();
    }

    public static class IPluginExtensions {
        public static string GetId([NotNull] this IPlugin plugin) {
            if (plugin == null) throw new ArgumentNullException(nameof(plugin));

            return plugin.GetType().GetComponentId();
        }
    }
}