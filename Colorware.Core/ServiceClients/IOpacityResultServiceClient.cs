﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;

namespace Colorware.Core.ServiceClients {
    public interface IOpacityResultServiceClient {
        Task<IReadOnlyCollection<IOpacityResult>> FindAllOpacityResultsForJob(long jobId);
    }
}