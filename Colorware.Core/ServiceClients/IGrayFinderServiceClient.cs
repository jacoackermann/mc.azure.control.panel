﻿using System.Collections.Immutable;
using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.ServiceClients {
    public interface IGrayFinderServiceClient {
        [NotNull]
        Task DeleteAllGrayFinderPatchesForReferences([NotNull] IImmutableList<long> referenceIds);
    }
}