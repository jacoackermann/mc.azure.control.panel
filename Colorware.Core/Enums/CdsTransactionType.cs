﻿namespace Colorware.Core.Enums {
    /// <summary>
    /// Types of CDS transactions.
    /// (All but T1 require a valid compare result).
    /// </summary>
    public enum CdsTransactionType {
        /// <summary>
        /// Transaction for job setup. Retrieves interface elements based on send data.
        /// </summary>
        T1 = 1,

        /// <summary>
        /// Transaction for intermediate results. Requests score data for running score without passing in
        /// registration or visual data.
        /// </summary>
        T2A = 2,

        /// <summary>
        /// Transaction for intermediate results. Requests score data for final score. Passes in all extra information such
        /// as registration and visual data.
        /// </summary>
        T2B = 3,

        /// <summary>
        /// Same as <see cref="T2B"/>, but submits the results to CDS and only returns a result code.
        /// </summary>
        T3 = 4
    }
}