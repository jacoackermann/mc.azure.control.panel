﻿namespace Colorware.Core.Enums {
    public enum ToleranceGroups {
        Undefined = 0,
        Primaries,
        Secondaries,
        Spotcolor,
        Density,
        GrayBalance,
        PaperWhite,
        Dotgain,
        Max,
        Average,
        Spread
    }
}