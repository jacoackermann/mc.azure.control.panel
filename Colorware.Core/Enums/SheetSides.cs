﻿using Colorware.Core.Globalization;

namespace Colorware.Core.Enums {
    public enum SheetSides {
        [EnumLanguage("CW.SheetSide.NA")]
        NA = 0,
        [EnumLanguage("CW.SheetSide.Bottom")]
        Bottom = 1,
        [EnumLanguage("CW.SheetSide.Top")]
        Top = 2
    }

    public enum FlexoSheetSides {
        [EnumLanguage("FM.JobCreation.Reverse")]
        Reverse = 1,
        [EnumLanguage("FM.JobCreation.Surface")]
        Surface = 2
    }
}