﻿using Colorware.Core.Globalization;

namespace Colorware.Core.Enums {
    public enum PatchTypes {
        // Not a real type, just means 'all types' in e.g. a request/query
        All = -1,

        Undefined = 0, // Undefined patch, irrelevant for calculations.
        Solid, // E.g. full color => Cyan 100, Magenta 100, Yellow 100, Key 100
        Dotgain, // Dotgain patch => Cyan <100, Magenta <100, Yellow <100, Key <100
        Slur, // Slur patch.
        Balance, // Balance patch (generic).
        Paperwhite, // Paperwhite patch, needed in every strip where dotgains are to be calculated.
        BalanceHighlight,
        BalanceMidtone,
        BalanceShadow,
        Overprint,
        Other,

        // Special patch, represents that this patch is only used for dotgain measurements. The dotgains themselves
        // are retrieved from a general dotgain curve.
        DotgainOnly
    }

    public enum FocusPatch {
        [EnumLanguage("CW.PatchTypes.FocusPatch.Solid")]
        Solid = 0,

        [EnumLanguage("CW.PatchTypes.FocusPatch.Highlight")]
        Highlight,

        [EnumLanguage("CW.PatchTypes.FocusPatch.Midtone")]
        Midtone,

        [EnumLanguage("CW.PatchTypes.FocusPatch.Shadow")]
        Shadow
    }
}