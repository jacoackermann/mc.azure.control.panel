﻿namespace Colorware.Core.Enums {
    // Note that this is actually an int in the DB and it can contain
    // custom values
    public enum JobTypes {
        Offset = 0,
        MultiSpot = 1,
        Flexo = 2,
        Printability = 3,
        Image = 4
    }
}