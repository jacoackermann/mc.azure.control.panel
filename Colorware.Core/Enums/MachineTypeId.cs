﻿namespace Colorware.Core.Enums {
    // NB: these values are very much dependent on the script that populates
    // the MachineType table!
    public enum MachineTypeId {
        OffsetPress = 1,
        DigitalProofer = 2,
        DigitalPrinter = 3,
        FlexoPress = 4
    }
}