namespace Colorware.Core.Enums {
    public enum MeasurementTypes : long {
        Error = 0,
        // Check MeasurementType ids for values.
        Production = 1,
        DigitalProof = 2,
        CtpCurve = 5
    }
}