﻿namespace Colorware.Core.Enums {
    public enum OpacityTypes {
        NotApplicable = 0,
        Technidyne = 1,
        Spectrophotometer = 2
    }
}