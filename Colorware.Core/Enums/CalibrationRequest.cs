﻿namespace Colorware.Core.Enums {
    public enum CalibrationRequest {
        // General calibration, some would call it 'absolute'
        Sensor,
        Paperwhite,
        Both
    }
}