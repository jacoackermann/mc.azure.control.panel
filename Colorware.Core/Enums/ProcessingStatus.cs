﻿namespace Colorware.Core.Enums {
    /// <summary>
    /// Describes the status/result for the export of a single item.
    /// </summary>
    public enum ProcessingStatus : short {
        /// <summary>
        /// The measurement has been marked to explicitely be send over.
        /// </summary>
        ToBeDone = 1,
        /// <summary>
        /// The send executed succesfully.
        /// </summary>
        Succeeded = 2,
        /// <summary>
        /// The send failed in some way.
        /// </summary>
        Failed = 3,
        /// <summary>
        /// A connection to the server could not be made, probably want to try again later.
        /// </summary>
        NoServerConnection = 4
    }
}