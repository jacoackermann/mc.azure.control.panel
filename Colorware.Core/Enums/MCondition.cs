﻿using Colorware.Core.Globalization;

namespace Colorware.Core.Enums {
    public enum MCondition : byte {
        /// <summary>Old (tungsten) lamp.</summary>
        [EnumLanguage("CW.DeviceSetup.MCondition.M0")]
        M0 = 0,

        /// <summary>Standardized D50 lamp (in practice always LED).</summary>
        [EnumLanguage("CW.DeviceSetup.MCondition.M1")]
        M1 = 1,

        ///<summary>UV cut (old (tungsten) lamp + filter or LED without UV).</summary>
        [EnumLanguage("CW.DeviceSetup.MCondition.M2")]
        M2 = 2,

        ///<summary>Polarized, any lamp.</summary>
        [EnumLanguage("CW.DeviceSetup.MCondition.M3")]
        M3 = 3
    }
}