using Colorware.Core.Globalization;

namespace Colorware.Core.Enums {
    public enum InkZoneDensityGraphTolerance {
        [EnumLanguage("CW.InkZoneDensityGraphTolerance.Value005")]
        Value005 = 0,

        [EnumLanguage("CW.InkZoneDensityGraphTolerance.Value010")]
        Value010 = 1,

        [EnumLanguage("CW.InkZoneDensityGraphTolerance.AutomaticScale")]
        AutomaticScale = 2
    }
}