﻿using System;

using Colorware.Core.Globalization;
using Colorware.Core.Helpers.Attributes;

namespace Colorware.Core.Enums {
    public enum MeasurementMode {
        Row,
        Spot,
        Undefined
    }

    public enum Illuminant {
        [EnumLanguage("CW.DeviceSetup.Illumination.D50")]
        D50 = 0,

        [EnumLanguage("CW.DeviceSetup.Illumination.D55")]
        D55 = 1,

        [EnumLanguage("CW.DeviceSetup.Illumination.D65")]
        D65 = 2,

        [EnumLanguage("CW.DeviceSetup.Illumination.D75")]
        D75 = 3,

        [EnumLanguage("CW.DeviceSetup.Illumination.A")]
        A = 4,

        [ShowInSelections(false)]
        [EnumLanguage("CW.DeviceSetup.Illumination.B")]
        B = 5,

        [EnumLanguage("CW.DeviceSetup.Illumination.C")]
        C = 6,

        [ShowInSelections(false)]
        [EnumLanguage("CW.DeviceSetup.Illumination.F1")]
        F1 = 7,

        [EnumLanguage("CW.DeviceSetup.Illumination.F2")]
        F2 = 8,

        [ShowInSelections(false)]
        [EnumLanguage("CW.DeviceSetup.Illumination.F3")]
        F3 = 9,

        [ShowInSelections(false)]
        [EnumLanguage("CW.DeviceSetup.Illumination.F4")]
        F4 = 10,

        [ShowInSelections(false)]
        [EnumLanguage("CW.DeviceSetup.Illumination.F5")]
        F5 = 11,

        [ShowInSelections(false)]
        [EnumLanguage("CW.DeviceSetup.Illumination.F6")]
        F6 = 12,

        [EnumLanguage("CW.DeviceSetup.Illumination.F7")]
        F7 = 13,

        [ShowInSelections(false)]
        [EnumLanguage("CW.DeviceSetup.Illumination.F8")]
        F8 = 14,

        [ShowInSelections(false)]
        [EnumLanguage("CW.DeviceSetup.Illumination.F8")]
        F9 = 15,

        [ShowInSelections(false)]
        [EnumLanguage("CW.DeviceSetup.Illumination.F10")]
        F10 = 16,

        [EnumLanguage("CW.DeviceSetup.Illumination.F11")]
        F11 = 17,

        [ShowInSelections(false)]
        [EnumLanguage("CW.DeviceSetup.Illumination.F12")]
        F12 = 18,

        [ShowInSelections(false)]
        [EnumLanguage("CW.DeviceSetup.Illumination.Unknown")]
        Unknown = -1
    }

    public enum Observer {
        [EnumLanguage("CW.DeviceSetup.Observer.TwoDeg")]
        TwoDeg = 0,

        [EnumLanguage("CW.DeviceSetup.Observer.TenDeg")]
        TenDeg = 1,

        [ShowInSelections(false)]
        [EnumLanguage("CW.DeviceSetup.Observer.Unknown")]
        Unknown = -1
    }

    public static class ObserverExtensions {
        public static string ToHumanString(this Observer observer) {
            switch (observer) {
                case Observer.TwoDeg:
                    return "2°";
                case Observer.TenDeg:
                    return "10°";
                case Observer.Unknown:
                    return "Unknown";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    /// <summary>Only affects densities! (spectral is always absolute in the industry)</summary>
    public enum WhiteBase {
        /// <summary>Physical sample densities are the final sample densities.</summary>
        Absolute = 0,
        /// <summary>The substrate acts as the white point when measuring/calculating densities.
        /// In effect, the substrate densities are subtracted from the physical sample densities
        /// to yield the final sample densities.</summary>
        Substrate,
        [ShowInSelections(false)]
        Undefined = -1
    }

    public enum DensityMethod {
        /// <summary>ISO Status E (mainly EU). Differs from Status T on the yellow channel.<para/>
        /// 
        /// Replacement for DIN_16536 in practice (but not exactly, according to ISO); Techkon's
        /// conversion yields differences with DIN up to 0.10 on the yellow channel.</summary>
        [EnumLanguage("CW.DeviceSetup.DensityMethod.StatusE")]
        StatusE = 0,
        /// <summary>ISO Status T (also called ANSI Status T, mainly US). Differs from Status E on the yellow channel.</summary>
        [EnumLanguage("CW.DeviceSetup.DensityMethod.StatusT")]
        StatusT,
        [ShowInSelections(false)]
        Undefined = -1
    }

    public enum DotgainMethod {
        /// <summary>
        /// Specifies that Murray-Davies Densitometric calculated tone values should be used.
        /// </summary>
        [EnumLanguage("CW.DeviceSetup.DotgainMethod.Density", "Murray-Davies Densitometric")]
        Density = 0,
        /// <summary>
        /// Specifies that Murray-Davies Spectrally calculated tone values should be used.
        /// </summary>
        [EnumLanguage("CW.DeviceSetup.DotgainMethod.Spectral", "Murray-Davies Spectral")]
        Spectral,
        /// <summary>
        /// Specifies that NEN-ISO 20654 Spot Color Tone Value calculations for tone values should be used.
        /// </summary>
        /// <remarks>Also called: SCTV, SCHMOO or ISO 20654</remarks>
        /// <remarks>See PV-2167 for implementation ticket.</remarks>
        [EnumLanguage("CW.DeviceSetup.DotgainMethod.SpotColorToneValue", "Spot Color Tone Value")]
        SpotColorToneValue
    }

    public enum DeltaEMethod {
        Cie76 = 0,
        CMC,
        Cie94,
        Cie2000
    }

    public enum ColorbarAlignment {
        [EnumLanguage("CW.DeviceSetup.ColorbarAlignment.Auto")]
        Auto = 0,

        [EnumLanguage("CW.DeviceSetup.ColorbarAlignment.Left")]
        Left,

        [EnumLanguage("CW.DeviceSetup.ColorbarAlignment.Center")]
        Center,

        [EnumLanguage("CW.DeviceSetup.ColorbarAlignment.Right")]
        Right,

        [EnumLanguage("CW.DeviceSetup.ColorbarAlignment.ForceLeft")]
        ForceLeft
    }
}