﻿namespace Colorware.Core.Enums {
    /// <summary>
    /// Attribute types used within CDS.
    /// </summary>
    public enum CdsAttributeTypes {
        Color,
        Tvi,
        Density,
        Registration,
        Visual,
        Unknown
    }
}