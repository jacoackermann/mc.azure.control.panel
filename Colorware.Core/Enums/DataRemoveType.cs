﻿namespace Colorware.Core.Enums {
    public enum DataRemoveType {
        AllBeforeDate = 1,
        AllBeforeDateExceptOkSheets = 2
    }
}