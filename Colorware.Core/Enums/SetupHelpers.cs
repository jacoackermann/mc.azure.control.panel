using System;
using System.Collections.Generic;
using System.Linq;

using Castle.Core.Internal;

using Colorware.Core.Helpers;
using Colorware.Core.Helpers.Attributes;

namespace Colorware.Core.Enums {
    public static class SetupHelpers {
        private static IReadOnlyList<Illuminant> implementedIlluminants;

        /// <summary>
        /// Illuminants actually implemented. Useful for generating lists.
        /// </summary>
        public static IReadOnlyList<Illuminant> ImplementedIlluminants {
            get {
                if (implementedIlluminants != null)
                    return implementedIlluminants;

                var enumNames = typeof(Illuminant).GetEnumNames();

                return implementedIlluminants = typeof(Illuminant).GetMembers()
                                                                  .Where(m => enumNames.Contains(m.Name) &&
                                                                              !(m.HasAttribute<ShowInSelections>() &&
                                                                                !m.GetAttribute<ShowInSelections>()
                                                                                  .ShouldShow))
                                                                  .Select(member =>
                                                                          (Illuminant)
                                                                          Enum.Parse(typeof(Illuminant), member.Name))
                                                                  .ToList();
            }
        }

        public static readonly Observer[] SelectableObservers = {
            Observer.TwoDeg, Observer.TenDeg
        };

        public static readonly MCondition[] SelectableMConditions = {
            MCondition.M0, MCondition.M1, MCondition.M2, MCondition.M3
        };

        public static readonly DensityMethod[] DensityMethods = {
            DensityMethod.StatusE, DensityMethod.StatusT
        };

        private static IReadOnlyList<DeltaEMethod> deltaEMethods;

        public static IReadOnlyList<DeltaEMethod> DeltaEMethods {
            get {
                return deltaEMethods ??
                       (deltaEMethods = Enum.GetValues(typeof(DeltaEMethod)).Cast<DeltaEMethod>().ToList());
            }
        }

        private static IReadOnlyList<PatchTypes> selectablePatchTypes;

        public static IReadOnlyList<PatchTypes> SelectablePatchTypes {
            get {
                return selectablePatchTypes ??
                       (selectablePatchTypes = Enum.GetValues(typeof(PatchTypes)).Cast<PatchTypes>().ToList());
            }
        }

        private static IReadOnlyList<PatchReferenceType> patchReferenceTypes;

        public static IReadOnlyList<PatchReferenceType> PatchReferenceTypes {
            get {
                return patchReferenceTypes ??
                       (patchReferenceTypes =
                        Enum.GetValues(typeof(PatchReferenceType)).Cast<PatchReferenceType>().ToList());
            }
        }
    }
}