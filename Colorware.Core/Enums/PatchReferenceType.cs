﻿using Colorware.Core.Globalization;

namespace Colorware.Core.Enums {
    public enum PatchReferenceType {
        [EnumLanguage("CW.PatchReferenceTypes.Process")]
        Process = 0,
        [EnumLanguage("CW.PatchReferenceTypes.ProcessC")]
        ProcessC = 1,
        [EnumLanguage("CW.PatchReferenceTypes.ProcessM")]
        ProcessM = 2,
        [EnumLanguage("CW.PatchReferenceTypes.ProcessY")]
        ProcessY = 3,
        [EnumLanguage("CW.PatchReferenceTypes.ProcessK")]
        ProcessK = 4,
        [EnumLanguage("CW.PatchReferenceTypes.Spot")]
        Spot = 5
    }
}