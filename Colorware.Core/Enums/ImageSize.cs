﻿namespace Colorware.Core.Enums {
    public enum ImageSize {
        Tiny,
        Small,
        Full
    }
}