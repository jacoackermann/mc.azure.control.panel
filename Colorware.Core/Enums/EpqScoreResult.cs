﻿namespace Colorware.Core.Enums {
    public enum EpqScoreResult {
        Unknown = 5,
        Excellent = 4,
        Good = 3,
        Satisfactory = 2,
        Poor = 1,
        Fail = 0
    }
}