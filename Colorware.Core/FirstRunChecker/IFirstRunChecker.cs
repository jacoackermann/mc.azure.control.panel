﻿namespace Colorware.Core.FirstRunChecker {
    public interface IFirstRunChecker {
        /// <summary>
        /// Is this the first startup of the application? 
        /// When this returns true, we can do additional startup options.
        /// </summary>
        /// <returns><c>True</c> if this is the first run, <c>false</c> otherwise.</returns>
        bool GetIsFirstRun();

        /// <summary>
        /// Mark that first run has been completed so the next application startup <see
        /// cref="GetIsFirstRun"/> will return false. Note that after calling this method, <see
        /// cref="GetIsFirstRun"/> should still return true until the application has shutdown.
        /// </summary>
        void MarkFirstRunDone();
    }
}