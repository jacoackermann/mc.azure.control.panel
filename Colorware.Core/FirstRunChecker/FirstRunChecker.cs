﻿using System;

using Colorware.Core.Config;
using Colorware.Core.Functional.Option;

using JetBrains.Annotations;

namespace Colorware.Core.FirstRunChecker {
    public class FirstRunChecker : IFirstRunChecker {
        private readonly IConfig config;
        private const string FirstRunCheckKey = "Colorware.Core.FirstRunChecker.FirstRun";
        private Option<bool> wasFirstRun;

        public FirstRunChecker([NotNull] IConfig config) {
            this.config = config ?? throw new ArgumentNullException(nameof(config));
        }

        public bool GetIsFirstRun() {
            return wasFirstRun.OrElse(() => {
                var value = config.GetBool(FirstRunCheckKey, true);
                wasFirstRun = value.ToOption();
                return value;
            });
        }

        public void MarkFirstRunDone() {
            if (!wasFirstRun.HasValue) {
                // Get the value before marking.
                GetIsFirstRun();
            }
            config.SetBool(FirstRunCheckKey, false);
        }
    }
}