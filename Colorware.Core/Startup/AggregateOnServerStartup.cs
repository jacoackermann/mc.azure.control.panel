﻿using System;
using System.Collections.Generic;
using System.Linq;

using Castle.Windsor;

using JetBrains.Annotations;

namespace Colorware.Core.Startup {
    /// <summary>
    /// Contains multiple <see cref="IOnServerStartup"/> instances. This is used to wrap multiple
    /// implementations and call them on server startup.
    /// </summary>
    public class AggregateOnServerStartup : IOnServerStartup {
        private readonly IEnumerable<IOnServerStartup> innerStartups;
        public AggregateOnServerStartup([NotNull] IEnumerable<IOnServerStartup> innerStartups) {
            this.innerStartups = innerStartups ?? throw new ArgumentNullException(nameof(innerStartups));
        }

        public int StartupOrder => 0;

        public void Configure(IWindsorContainer container) {
            foreach (var startup in innerStartups.OrderBy(x => x.StartupOrder)) {
                startup.Configure(container);
            }
        }
    }
}