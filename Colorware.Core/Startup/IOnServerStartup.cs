﻿using Castle.Windsor;

namespace Colorware.Core.Startup {
    /// <summary>
    /// Defines a discoverable interface which is used to handle additional startup configurations
    /// on the Colorware server. Implementations are called at the end of regular server startup and are passed
    /// the current dependency injection container so they can register any additional components if required.
    /// Additionaly, implementations can just do any other startup configuration that is required by the
    /// implementation.
    /// </summary>
    /// <remarks>
    /// The startup order of different registered implementations should be considered non-deterministic
    /// and is determined by the dependency injection container.
    /// </remarks>
    public interface IOnServerStartup {
        /// <summary>
        /// Determines a relative start order.
        /// </summary>
        int StartupOrder { get; }

        /// <summary>
        /// Called on startup. Implementations should configure themselves and optionally register additional
        /// dependencies in the passed in container.
        /// </summary>
        /// <param name="container"></param>
        void Configure(IWindsorContainer container);
    }
}