﻿using System;
using System.Collections.Generic;

namespace Colorware.Core.Serialization {
    public class SpectrumCipherSerializer {
        /// <summary>
        /// The cipher to use to encrypt spectral data.
        /// </summary>
        private static readonly byte[] cipherKey = { 122, 13, 250, 137, 240, 174, 116, 28, 16, 199, 97, 222, 174, 149, 165, 210 };

        public static string Serialize(IEnumerable<double> values) {
            var byteArray = SpectrumSerializer.SerializeToBytes(values);
            var ciphered = cipher(byteArray);
            return Convert.ToBase64String(ciphered);
        }

        public static IEnumerable<double> Deserialize(string packedValues) {
            var byteArray = Convert.FromBase64String(packedValues);
            var deciphered = decipher(byteArray);
            return SpectrumSerializer.DeserializeFromBytes(deciphered);
        }

        private static byte[] cipher(byte[] byteArray) {
            var result = new byte[byteArray.Length];
            for (int i = 0; i < byteArray.Length; i++) {
                result[i] = (byte)(byteArray[i] ^ cipherKey[i % cipherKey.Length]);
            }
            return result;
        }

        private static byte[] decipher(byte[] byteArray) {
            // This should work as long as the same cipher transformation on ciphered input returns the original input
            // as is the case with xor.
            return cipher(byteArray);
        }
    }
}