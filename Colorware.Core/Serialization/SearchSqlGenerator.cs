﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

using JetBrains.Annotations;

namespace Colorware.Core.Serialization {
    /// <summary>
    /// Converts a saved XML search file to a SQL query used to perform the search.
    /// <remarks>Code ported directly from Ruby source (advanced_search_controller.rb)</remarks>
    /// </summary>
    public class SearchSqlGenerator {
        private class XmlRule {
            public string RuleType { get; set; }
            public int GroupId { get; set; }
            public string Relation { get; set; }
            public string FirstTerm { get; set; }
            public string SecondTerm { get; set; }
            public string TableName { get; set; }
            public string LinkProperty { get; set; }
        }

        public string GenerateFromString([NotNull] string str) {
            if (str == null) throw new ArgumentNullException("str");

            using (var xmlReader = XmlReader.Create(new StringReader(str)))
                return Generate(xmlReader);
        }

        public string Generate([NotNull] XmlReader xml) {
            if (xml == null) throw new ArgumentNullException("xml");

            var strResourceName = string.Empty;
            var rules = new List<XmlRule>();
            var ruleCount = -1;

            while (true) {
                if (xml.NodeType == XmlNodeType.Element) {
                    if (xml.Name == "Sentence") {
                        strResourceName = xml.GetAttribute("SearchResource");
                    }
                    else if (xml.Name == "Rule") {
                        ruleCount++;
                        var rule = new XmlRule();
                        rules.Add(rule);
                        rule.RuleType = xml.GetAttribute("RuleType");
                        rule.GroupId = int.Parse(xml.GetAttribute("GroupId"));
                    }
                    else if (xml.Name == "Relation") {
                        rules[ruleCount].Relation = xml.GetAttribute("Relation");
                    }
                    else if (xml.Name == "FirstTerm") {
                        var rule = rules[ruleCount];
                        rule.FirstTerm = xml.GetAttribute("Property");
                        var model = xml.GetAttribute("Model");
                        if (model == "reference")
                            rule.TableName = "color_reference";
                        else
                            rule.TableName = model.Replace('-', '_');
                        rule.LinkProperty = xml.GetAttribute("LinkProperty");
                    }
                    else if (xml.Name == "SecondTerm") {
                        rules[ruleCount].SecondTerm = xml.GetAttribute("Value");
                    }
                }
                if (!xml.Read() || xml.EOF)
                    break;
            }

            strResourceName += 's';
            // Create initial sql query:
            var sql = new StringBuilder();
            sql.AppendFormat("SELECT {0}.* FROM {0}", strResourceName);

            // Join tables:
            var joined = new List<string>();
            foreach (var xmlRule in rules) {
                if (xmlRule.RuleType == "AND")
                    continue;
                if (xmlRule.RuleType == "OR")
                    continue;
                if (xmlRule.TableName + 's' == strResourceName)
                    continue;
                if (joined.Contains(xmlRule.TableName))
                    continue;
                joined.Add(xmlRule.TableName);
                sql.AppendFormat(" LEFT JOIN {0}s ON {1}.{2} = {0}s.id",
                    xmlRule.TableName, strResourceName, xmlRule.LinkProperty);
            }

            // Create 'where' conditions:
            if (rules.Count > 0) {
                sql.Append(" WHERE ");
                var currentGroup = -1;
                var opened = 0;
                foreach (var xmlRule in rules) {
                    if (xmlRule.GroupId > currentGroup) {
                        sql.Append(" ( ");
                        currentGroup = xmlRule.GroupId;
                        opened ++;
                    }
                    else if (xmlRule.GroupId < currentGroup) {
                        sql.Append(" ) ");
                        currentGroup = xmlRule.GroupId;
                        opened--;
                    }
                    sql.Append(" ");
                    ruleToSqlCondition(sql, xmlRule);
                }
                while (opened > 0) {
                    sql.Append(")");
                    opened--;
                }
            }
            return sql.ToString();
        }

        private void ruleToSqlCondition(StringBuilder sql, XmlRule xmlRule) {
            if (xmlRule.RuleType == "AND" || xmlRule.RuleType == "OR") {
                sql.Append(xmlRule.RuleType);
            }
            else {
                var firstTerm = xmlRule.TableName + "s." + xmlRule.FirstTerm;
                getSqlCondition(sql, firstTerm, xmlRule.SecondTerm, xmlRule.Relation);
            }
        }

        private void getSqlCondition(StringBuilder sql, string firstTerm, string secondTerm, string relationCode) {
            if (relationCode == "NUM_EQ" || relationCode == "STR_EQUAL" || relationCode == "ENUM_IS")
                sql.AppendFormat("{0} = '{1}'", firstTerm, secondTerm);
            else if (relationCode == "STR_CONTAINS")
                sql.AppendFormat("{0} LIKE '%{1}%'", firstTerm, secondTerm);
            else if (relationCode == "NUM_NEQ" || relationCode == "ENUM_IS_NOT")
                if (firstTerm.EndsWith("_id")) {
                    // Oh boy... Hopefully this won't explode at some awkward moment in the future.
                    // This generation fails if we try to match an integer field, so we are guessing that
                    // any field ending in "_id" is an integer field and thus should not be treated as a string/varchar
                    // by putting quotes around it.
                    sql.AppendFormat("{0} <> {1}", firstTerm, secondTerm);
                }
                else {
                    sql.AppendFormat("{0} <> '%{1}%'", firstTerm, secondTerm);
                }
            else if (relationCode == "NUM_GT")
                sql.AppendFormat("{0} > '{1}'", firstTerm, secondTerm);
            else if (relationCode == "NUM_GTE")
                sql.AppendFormat("{0} >= '{1}'", firstTerm, secondTerm);
            else if (relationCode == "NUM_LT")
                sql.AppendFormat("{0} < '{1}'", firstTerm, secondTerm);
            else if (relationCode == "NUM_LTE")
                sql.AppendFormat("{0} <= '{1}'", firstTerm, secondTerm);
            else if (relationCode == "BOOL_TRUE")
                sql.AppendFormat("{0} >= '1'", firstTerm);
            else if (relationCode == "BOOL_FALSE")
                sql.AppendFormat("{0} <= '0'", firstTerm);
            else if (relationCode == "DATE_LAST_X_DAYS")
                sql.AppendFormat("DATEADD(day, -{1}, GETDATE()) <= {0}", firstTerm, secondTerm);
            else if (relationCode == "DATE_LAST_X_MOUNTS")
                sql.AppendFormat("DATEADD(month, -{1}, GETDATE()) <= {0}", firstTerm, secondTerm);
            else if (relationCode == "DATE_LATER")
                sql.AppendFormat("{0} > '{1}'", firstTerm, secondTerm);
            else if (relationCode == "DATE_EARLIER")
                sql.AppendFormat("{0} < '{1}'", firstTerm, secondTerm);
            else {
                throw new Exception("Invalid code: " + relationCode);
            }
        }
    }
}