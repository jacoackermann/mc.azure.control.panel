using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Colorware.Core.Serialization {
    public static class SpectrumSerializer {
        public static string Serialize(IEnumerable<double> values) {
            var byteArray = SerializeToBytes(values);
            var encoded = Convert.ToBase64String(byteArray);
            return encoded;
        }

        public static IEnumerable<double> Deserialize(string packedValues) {
            var byteArray = Convert.FromBase64String(packedValues);
            var decoded = DeserializeFromBytes(byteArray);
            return decoded;
        }
        public static byte[] SerializeToBytes(IEnumerable<double> values) {
            var byteArray = new byte[values.Count() * sizeof(float)];
            var byteArrayIndex = 0;
            foreach (var value in values) {
                var asBytes = BitConverter.GetBytes((float)value);
                Array.Copy(asBytes, 0, byteArray, byteArrayIndex, asBytes.Length);
                byteArrayIndex += sizeof(float);
            }
            return byteArray;
        }

        public static IEnumerable<double> DeserializeFromBytes(byte[] byteArray) {
            Debug.Assert(byteArray.Length % sizeof(float) == 0);
            var numValues = byteArray.Length / sizeof(float);
            var result = new double[numValues];
            for (int byteArrayIndex = 0, targetIndex = 0; targetIndex < numValues; byteArrayIndex+= sizeof(float), targetIndex++) {
                var value = BitConverter.ToSingle(byteArray, byteArrayIndex);
                result[targetIndex] = value;
            }
            return result;
        }
    }
}