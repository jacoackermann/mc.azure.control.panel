﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using JetBrains.Annotations;

using ProtoBuf;

namespace Colorware.Core.WebApi {
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class ExceptionErrorMessageContent {
        private ExceptionErrorMessageContent() {
        }

        public ExceptionErrorMessageContent([NotNull] Exception e) {
            if (e == null) throw new ArgumentNullException("e");

            ExceptionInfos = generateExceptionInfoFromException(e);
        }

        public List<ExceptionInfo> ExceptionInfos { get; private set; }

        [NotNull]
        private List<ExceptionInfo> generateExceptionInfoFromException(Exception e) {
            var result = new List<ExceptionInfo>();

            var currentException = e;
            while (currentException != null) {
                var sb = new StringBuilder();
                sb.Append(currentException.Message);

                // SQL Exception specifics
                var sqlExcept = currentException as SqlException;
                if (sqlExcept != null) {
                    sb.AppendLine();

                    if (sqlExcept.Data.Contains("Query"))
                        sb.AppendLine("\nQuery: " + sqlExcept.Data["Query"]);

                    if (sqlExcept.Data.Contains("QueryParameters"))
                        sb.AppendLine("\nQuery parameters\n" +
                                      string.Join("\n",
                                          ((IEnumerable<DbParameter>)sqlExcept.Data["QueryParameters"]).Select(
                                              p => string.Format("{0}: {1}", p.ParameterName, p.Value))));
                }

                result.Add(new ExceptionInfo(currentException.GetType(), currentException.StackTrace ?? string.Empty,
                    sb.ToString()));

                currentException = currentException.InnerException;
            }

            return result;
        }
    }
}