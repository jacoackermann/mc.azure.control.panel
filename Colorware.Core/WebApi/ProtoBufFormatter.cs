﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using ProtoBuf.Meta;

namespace Colorware.Core.WebApi {
    // 2014-10-08: Adapted from WebApiContrib.Formatting.ProtoBuf
    public class ProtoBufFormatter : MediaTypeFormatter {
        private static readonly MediaTypeHeaderValue mediaType = new MediaTypeHeaderValue("application/x-protobuf");

        private RuntimeTypeModel model = RuntimeTypeModel.Default;

        public RuntimeTypeModel Model {
            get { return model; }
            set { model = value; }
        }

        public ProtoBufFormatter() {
            SupportedMediaTypes.Add(mediaType);
        }

        public override bool CanReadType(Type type) {
            // Optimization
            return true;
            //return Model.CanSerialize(type);
        }

        public override bool CanWriteType(Type type) {
            // Optimization
            return true;
            //return Model.CanSerialize(type);
        }

        public override Task<object> ReadFromStreamAsync(Type type, Stream stream, HttpContent content,
            IFormatterLogger formatterLogger) {
            var item = Task.FromResult(Model.Deserialize(stream, null, type));
            return item;
        }

        public override Task WriteToStreamAsync(Type type, object value, Stream stream, HttpContent content,
            TransportContext transportContext) {
            Model.Serialize(stream, value);
            return Task.CompletedTask;
        }
    }
}