﻿using System;

using JetBrains.Annotations;

using ProtoBuf;

namespace Colorware.Core.WebApi {
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class ExceptionInfo {
        private ExceptionInfo() {
        }

        public ExceptionInfo([NotNull] Type exceptionType, [CanBeNull] string stackTrace, [NotNull] string error) {
            if (exceptionType == null) throw new ArgumentNullException("exceptionType");
            if (stackTrace == null) throw new ArgumentNullException("stackTrace");
            if (error == null) throw new ArgumentNullException("error");

            ExceptionType = exceptionType.FullName;
            StackTrace = stackTrace;
            Error = error;
        }

        [NotNull]
        public string ExceptionType { get; private set; }

        [NotNull]
        public string StackTrace { get; private set; }

        [NotNull]
        public string Error { get; private set; }
    }
}