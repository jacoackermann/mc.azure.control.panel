﻿using System;

using ProtoBuf;

namespace Colorware.Core.WebApi.ProtoBufSurrogates {
    /// <summary>
    /// Surrogate that allows ProtoBuf to (de)serialize DateTimeOffset instances.
    /// 
    /// Don't use for other purposes.
    /// 
    /// See: https://stackoverflow.com/questions/7046506/can-i-serialize-arbitrary-types-with-protobuf-net/7046868
    /// </summary>
    [ProtoContract]
    public struct DateTimeOffsetSurrogate {
        [ProtoMember(1)]
        public long DateTimeTicks { get; set; }

        [ProtoMember(2)]
        public short OffsetMinutes { get; set; }

        public static implicit operator DateTimeOffsetSurrogate(DateTimeOffset value) {
            return new DateTimeOffsetSurrogate {
                DateTimeTicks = value.Ticks,
                OffsetMinutes = (short)value.Offset.TotalMinutes
            };
        }

        public static implicit operator DateTimeOffset(DateTimeOffsetSurrogate value) {
            return new DateTimeOffset(value.DateTimeTicks, TimeSpan.FromMinutes(value.OffsetMinutes));
        }
    }
}