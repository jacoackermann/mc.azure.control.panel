﻿using System;
using System.Collections.Generic;
using System.Linq;

using JetBrains.Annotations;

namespace Colorware.Core.WebApi {
    /// <summary>
    /// An exception, to be thrown on the client, that indicates an unhandled exception occurred
    /// at the server while handling a Web API request.
    /// 
    /// Contains the unhandled server exceptions as inner exceptions of type Exception.
    /// </summary>
    public class WebApiUnhandledServerException : Exception {
        public static WebApiUnhandledServerException Create([NotNull] string requestUrl,
            [NotNull] IEnumerable<ExceptionInfo> serverExceptions) {
            if (requestUrl == null) throw new ArgumentNullException("requestUrl");
            if (serverExceptions == null) throw new ArgumentNullException("serverExceptions");

            var message =
                string.Format(
                    "An unhandled exception occurred on the server while handling a Web API request!\n\n" + "URL: {0}",
                    requestUrl);

            return new WebApiUnhandledServerException(message, generateInnerException(serverExceptions));
        }

        [CanBeNull]
        private static Exception generateInnerException(IEnumerable<ExceptionInfo> serverExceptions) {
            if (!serverExceptions.Any())
                return null;

            var info = serverExceptions.First();

            return new Exception(string.Format("{0}: {1}\n{2}", info.ExceptionType, info.Error, info.StackTrace),
                generateInnerException(serverExceptions.Skip(1)));
        }

        private WebApiUnhandledServerException(string message) : base(message) {
        }

        private WebApiUnhandledServerException(string message, Exception inner) : base(message, inner) {
        }
    }
}