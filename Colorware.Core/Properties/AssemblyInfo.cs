﻿using System.Reflection;
using System.Runtime.InteropServices;
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
using System.Windows.Markup;

[assembly: AssemblyTitle("Colorware.Core")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Colorware")]
[assembly: AssemblyProduct("Colorware.Core")]
[assembly: AssemblyCopyright("Copyright (c) Colorware 2008-2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: Obfuscation(Feature = "Apply to type Colorware.Core.UserProfileDefinitions.*: all", Exclude = true, ApplyToMembers = true)]
[assembly: Obfuscation(Feature = "Apply to type Colorware.Core.WebApi.*: all", Exclude = true, ApplyToMembers = true)]
[assembly: Obfuscation(Feature = "Apply to type Colorware.Core.License.Models.*: all", Exclude = true, ApplyToMembers = true)]
[assembly: Obfuscation(Feature = "Apply to type Colorware.Core.License.Service.*: all", Exclude = true, ApplyToMembers = true)]
[assembly: Obfuscation(Feature = "Apply to type Colorware.Core.Data.*: all", Exclude = true, ApplyToMembers = true)]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("c3f14edc-0719-4e16-bd2e-bb6c47fed804")]

#if MEASURE_COLOR
[assembly: XmlnsDefinition("MeasureColor", "System")]
#else
[assembly: XmlnsDefinition("PressView", "System")]
#endif

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("4.5.0.*")]

// Defines the build date of the application. This corresponds to the support contract a client can use which
// is determined by the license. Currently the date is encrypted with the application wide symmetric key.
// To generate a new key, the script located at ./Scripts/EncryptedDateHelper.fsx can be used:
// 
// Date corresponds to: 2014-12-19T17:06:23.3989423+01:00
[assembly: AssemblyMetadata("BuildDate", "uzi3FAIgzFnZctELagvHQdlaxa5VrbFsNxk5O/snN0y37F4QpKS0vupZF8MfklKN")]

[assembly: AssemblyVersion("18.1.60")]
[assembly: AssemblyInformationalVersion("18.1.60")]