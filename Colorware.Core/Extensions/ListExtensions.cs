﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using JetBrains.Annotations;

namespace Colorware.Core.Extensions {
    public static class ListExtensions {
        // Adds sorting functionality to ObservableCollection
        public static void BubbleSort(this IList o) {
            for (int i = o.Count - 1; i >= 0; i--) {
                for (int j = 1; j <= i; j++) {
                    object o1 = o[j - 1];
                    object o2 = o[j];
                    if (((IComparable)o1).CompareTo(o2) > 0) {
                        o.Remove(o1);
                        o.Insert(j, o1);
                    }
                }
            }
        }

        // Removes all items from an ObservableCollection matching a predicate
        public static ObservableCollection<T> RemoveAll<T>(
            this ObservableCollection<T> coll, Func<T, bool> condition) {
            var itemsToRemove = coll.Where(condition).ToList();

            foreach (var itemToRemove in itemsToRemove)
                coll.Remove(itemToRemove);

            return coll;
        }

        // Removes all items from an IList matching a predicate
        public static int RemoveAll<T>(this IList<T> list, Predicate<T> match) {
            var count = 0;

            for (int i = list.Count - 1; i >= 0; i--) {
                if (match(list[i])) {
                    ++count;
                    list.RemoveAt(i);
                }
            }

            return count;
        }

        public static string ToStringExtension<T>(this IEnumerable<T> list) {
            return "[" + string.Join(", ", list) + "]";
        }

        public static IEnumerable<double> Add(this IEnumerable<double> first, IEnumerable<double> second) {
            using (IEnumerator<double> e1 = first.GetEnumerator())
            using (IEnumerator<double> e2 = second.GetEnumerator())
                while (e1.MoveNext() && e2.MoveNext())
                    yield return e1.Current + e2.Current;
        }

        public static IEnumerable<double> Add(this IEnumerable<double> source, double c) {
            foreach (var v in source)
                yield return v + c;
        }

        public static IEnumerable<double> Sub(this IEnumerable<double> first, IEnumerable<double> second) {
            using (IEnumerator<double> e1 = first.GetEnumerator())
            using (IEnumerator<double> e2 = second.GetEnumerator())
                while (e1.MoveNext() && e2.MoveNext())
                    yield return e1.Current - e2.Current;
        }

        public static IEnumerable<double> Sub(this IEnumerable<double> source, double c) {
            foreach (var v in source)
                yield return v - c;
        }

        public static IEnumerable<double> Mul(this IEnumerable<double> source, IEnumerable<double> second) {
            using (IEnumerator<double> e1 = source.GetEnumerator())
            using (IEnumerator<double> e2 = second.GetEnumerator())
                while (e1.MoveNext() && e2.MoveNext())
                    yield return e1.Current * e2.Current;
        }

        public static IEnumerable<double> Mul(this IEnumerable<double> source, double c) {
            foreach (var v in source)
                yield return v * c;
        }

        public static IEnumerable<double> Div(this IEnumerable<double> first, IEnumerable<double> second) {
            using (IEnumerator<double> e1 = first.GetEnumerator())
            using (IEnumerator<double> e2 = second.GetEnumerator())
                while (e1.MoveNext() && e2.MoveNext())
                    yield return e1.Current / e2.Current;
        }

        public static IEnumerable<double> Div(this IEnumerable<double> source, double c) {
            foreach (var v in source)
                yield return v / c;
        }

        public static IEnumerable<double> Pow(this IEnumerable<double> first, IEnumerable<double> second) {
            using (IEnumerator<double> e1 = first.GetEnumerator())
            using (IEnumerator<double> e2 = second.GetEnumerator())
                while (e1.MoveNext() && e2.MoveNext())
                    yield return Math.Pow(e1.Current, e2.Current);
        }

        public static IEnumerable<double> Pow(this IEnumerable<double> source, double c) {
            foreach (var v in source)
                yield return Math.Pow(v, c);
        }

        public static IEnumerable<double> Square(this IEnumerable<double> source) {
            foreach (var v in source)
                yield return v * v;
        }

        public static IEnumerable<double> Min(this IEnumerable<double> first, IEnumerable<double> second) {
            using (IEnumerator<double> e1 = first.GetEnumerator())
            using (IEnumerator<double> e2 = second.GetEnumerator())
                while (e1.MoveNext() && e2.MoveNext())
                    yield return Math.Min(e1.Current, e2.Current);
        }

        public static IEnumerable<double> Max(this IEnumerable<double> first, IEnumerable<double> second) {
            using (IEnumerator<double> e1 = first.GetEnumerator())
            using (IEnumerator<double> e2 = second.GetEnumerator())
                while (e1.MoveNext() && e2.MoveNext())
                    yield return Math.Max(e1.Current, e2.Current);
        }

        public static IEnumerable<double> AbsDiff(this IEnumerable<double> first, IEnumerable<double> second) {
            using (IEnumerator<double> e1 = first.GetEnumerator())
            using (IEnumerator<double> e2 = second.GetEnumerator())
                while (e1.MoveNext() && e2.MoveNext())
                    yield return Math.Abs(e1.Current - e2.Current);
        }

        public static bool IsStrictlyAscending(this IEnumerable<double> source) {
            return source.Zip(source.Skip(1), (v1, v2) => v2 > v1).All(v => v);
        }

        public static bool IsStrictlyDescending(this IEnumerable<double> source) {
            return source.Reverse().IsStrictlyAscending();
        }

        public static LinkedList<T> ToLinkedList<T>([NotNull] this IEnumerable<T> source) {
            if (source == null) throw new ArgumentNullException(nameof(source));

            return new LinkedList<T>(source);
        }
    }
}