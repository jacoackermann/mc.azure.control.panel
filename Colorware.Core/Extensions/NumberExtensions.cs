﻿using System;

namespace Colorware.Core.Extensions {
    public static class NumberExtensions {
        // http://floating-point-gui.de/errors/comparison/
        // http://stackoverflow.com/questions/3874627/floating-point-comparison-functions-for-c-sharp
        // This method is about as good as it gets for C# (there still are some very rare problems, see comments to accepted answer)
        public static bool NearlyEquals(this float a, float b, float epsilon) {
            float absA = Math.Abs(a);
            float absB = Math.Abs(b);
            float diff = Math.Abs(a - b);

            // ReSharper disable CompareOfFloatsByEqualityOperator
            if (a == b) {
                // shortcut, handles infinities
                return true;
            }
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            else if (a == 0 || b == 0 || diff < float.MinValue) {
                // a or b is zero or both are extremely close to it
                // relative error is less meaningful here
                return diff < (epsilon * float.MinValue);
            }
            else {
                // use relative error
                return diff / (absA + absB) < epsilon;
            }
            // ReSharper restore CompareOfFloatsByEqualityOperator
        }

        public static bool NearlyEquals(this float a, float b) {
            // ReSharper disable once IntroduceOptionalParameters.Global
            return NearlyEquals(a, b, 0.000001f);
        }

        public static bool NearlyEquals(this double a, double b, double epsilon) {
            double absA = Math.Abs(a);
            double absB = Math.Abs(b);
            double diff = Math.Abs(a - b);

            // ReSharper disable CompareOfFloatsByEqualityOperator
            if (a == b) {
                // shortcut, handles infinities
                return true;
            }
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            else if (a == 0 || b == 0 || diff < double.MinValue) {
                // a or b is zero or both are extremely close to it
                // relative error is less meaningful here
                return diff < (epsilon * double.MinValue);
            }
            else {
                // use relative error
                return diff / (absA + absB) < epsilon;
            }
            // ReSharper restore CompareOfFloatsByEqualityOperator
        }

        public static bool NearlyEquals(this double a, double b) {
            // ReSharper disable once IntroduceOptionalParameters.Global
            return NearlyEquals(a, b, 0.000001D);
        }

        public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T> {
            if (val.CompareTo(min) < 0)
                return min;

            if (val.CompareTo(max) > 0)
                return max;

            return val;
        }
    }
}