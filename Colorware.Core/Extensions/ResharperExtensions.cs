﻿using Colorware.Core.Functional.Option;

using JetBrains.Annotations;

namespace Colorware.Core.Extensions {
    public static class ResharperExtensions {
        [SourceTemplate]
        public static void opt<T>(this T self) {
            self.ToOption();
        }
        
    }
}