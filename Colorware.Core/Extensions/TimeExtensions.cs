﻿using System;

namespace Colorware.Core.Extensions {
    public static class TimeExtensions {
        private static readonly DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);

        public static long ToUnixTimestamp(this DateTime date) {
            var diff = date - origin;
            return (long)Math.Floor(diff.TotalSeconds);
        }
    }
}