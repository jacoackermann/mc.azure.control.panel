﻿using System;
using System.Linq;

using Castle.Core;
using Castle.Facilities.Logging;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.ModelBuilder.Inspectors;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;

using Colorware.Core.Interface;
using Colorware.Core.Windsor;

using JetBrains.Annotations;

namespace Colorware.Core.Extensions {
    public static class WindsorExtensions {
        public static ComponentRegistration<T> NoProperties<T>(this ComponentRegistration<T> container) where T : class {
            return container.Properties(PropertyFilter.IgnoreAll);
        }

        public static BasedOnDescriptor RegisterViewModels(this FromAssemblyDescriptor descriptor) {
            return descriptor.Where(p => p.Name.EndsWith("ViewModel"))
                             .WithServiceSelf()
                             .Configure(c =>
                                        c.IsFallback()
                                         .LifestyleTransient()
                                         .NamedAutomatically(c.Implementation.Name));
        }

        public static BasedOnDescriptor RegisterViews(this FromAssemblyDescriptor descriptor) {
            return descriptor.Where(p => p.Name.EndsWith("View") || p.Name.EndsWith("Window"))
                             .WithServiceSelf()
                             .Configure(c =>
                                        c.IsFallback()
                                         .LifestyleTransient()
                                         .NamedAutomatically(c.Implementation.Name));
        }

        public static IWindsorContainer CreateChildContainer(this IWindsorContainer container) {
            var child = new WindsorContainer();
            container.AddChildContainer(child);
            child.SetDefaultConfiguration();
            addDefaultOverridingComponent(child);
            return child;
        }

        public static IWindsorContainer SetDefaultConfiguration(this IWindsorContainer container) {
            container.Kernel.ComponentModelBuilder.RemoveContributor(
                // We never want property injection
                container.Kernel.ComponentModelBuilder.Contributors.OfType<PropertiesDependenciesModelInspector>()
                         .Single()
                );

            // Enable logging.
            container.AddFacility<LoggingFacility>(f => f.UseNLog());

            // To support AsFactory() for factories
            container.AddFacility<TypedFactoryFacility>();

            // To support IEnumerable<T>, ICollection<T>, IList<T> and T[] (a list of all types available for T)
            container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel, true));

            // Add lazy support.
            container.Register(
                Component.For<ILazyComponentLoader>().ImplementedBy<LazyOfTComponentLoader>().IsFallback());

            return container;
        }

        [Pure]
        public static Type GetImplementationType(this IWindsorContainer container, [NotNull] Type type) {
            if (type == null) throw new ArgumentNullException(nameof(type));

            if (type.IsAbstract)
                return container.Kernel.GetHandler(type).ComponentModel.Implementation;

            return type;
        }

        [Pure]
        public static Type GetImplementationType<T>(this IWindsorContainer container) {
            return GetImplementationType(container, typeof(T));
        }

        /// <summary>
        /// Under some circumstances resolving types from child containers to parent containers fails. A specific instance
        /// is when a typed factory (e.g. IViewFactory[ViewType]) is defined in the parent while the type used is defined
        /// in the child. The factory will resolve in the parent, but the type will not resolve as it is known only by the child.
        /// By re-registering the factory in the child, the type should resolve as expected. Add any often used type factories
        /// and other such components here.
        /// </summary>
        /// <param name="child"></param>
        private static void addDefaultOverridingComponent(WindsorContainer child) {
            child.Register(
                // Register default view and view model factories.
                Component.For(typeof(IViewFactory<>)).AsFactory(),
                // We want parameters to be resolved in-order because we don't know what implementers will name
                // the VM parameter in their View's constructor
                Component.For(typeof(IViewFactory<,>))
                         .AsFactory(c => c.SelectedWith(new ArgumentsInOrderTypedFactoryComponentSelector())),
                Component.For(typeof(IViewModelFactory<>)).AsFactory()
                );
        }

        public static void DestroyChildContainer(this IWindsorContainer container, IWindsorContainer childContainer) {
            childContainer.Dispose();
            container.RemoveChildContainer(childContainer);
        }
    }
}