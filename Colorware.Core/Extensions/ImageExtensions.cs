﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

using Colorware.Core.Helpers;

using JetBrains.Annotations;

namespace Colorware.Core.Extensions {
    public static class ImageExtensions {
        [NotNull]
        public static Bitmap ToBitmap(this BitmapImage bitmapImage) {
            // Not very nice, is a blocking call, but needed to give GC time to
            // cleanup unused unmanaged image memory
            MemoryHelper.Cleanup();

            using (var outStream = new MemoryStream()) {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                enc.Save(outStream);

                // Save() uses twice the size of the image
                MemoryPressureGenerator.GeneratePressure(outStream.Length * 2);

                Bitmap b;
                using (var bitmap = new Bitmap(outStream)) {
                    MemoryPressureGenerator.GeneratePressure(outStream.Length * 2);

                    // return bitmap; <-- leads to problems, stream is closed/closing ...
                    b = new Bitmap(bitmap);
                    MemoryPressureGenerator.GeneratePressure(outStream.Length * 2);
                }

                return b;
            }
        }

        [NotNull]
        public static BitmapImage ToBitmapImage(this Bitmap bitmap) {
            // Not very nice, is a blocking call, but needed to give GC time to
            // cleanup unused unmanaged image memory
            MemoryHelper.Cleanup();

            using (var ms = new MemoryStream()) {
                bitmap.Save(ms, ImageFormat.Bmp);
                ms.Position = 0;
                var bi = new BitmapImage();
                bi.BeginInit();
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.StreamSource = ms;
                bi.EndInit();
                bi.Freeze();

                MemoryPressureGenerator.GeneratePressure(ms.Length * 2);

                return bi;
            }
        }

        [NotNull]
        public static byte[] ToByteArray(this Image image, ImageFormat format) {
            using (var ms = new MemoryStream()) {
                image.Save(ms, format);
                return ms.ToArray();
            }
        }

        [NotNull]
        public static Bitmap ToBitmap([NotNull] this byte[] bytes) {
            if (bytes == null) throw new ArgumentNullException("bytes");

            using (var ms = new MemoryStream(bytes))
                return new Bitmap(ms);
        }

        [NotNull]
        public static Bitmap Resize(this Bitmap b, int width, int height) {
            var result = new Bitmap(width, height);

            using (Graphics g = Graphics.FromImage(result))
                g.DrawImage(b, 0, 0, width, height);

            return result;
        }
    }
}