﻿using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Colorware.Core.Extensions {
    public static class TaskExtensions {
        /// <summary>
        /// <![CDATA[Casts a Task<Derived> to a Task<Base>]]>.
        /// </summary>
        /// <typeparam name="TBase">The base type to cast to.</typeparam>
        /// <typeparam name="TDerived">The derived type to cast from.</typeparam>
        /// <param name="task"><![CDATA[The task<T> that should be cast.]]></param>
        /// <returns></returns>
        public static Task<TBase> ToBase<TBase, TDerived>(this Task<TDerived> task) where TDerived : TBase {
            var tcs = new TaskCompletionSource<TBase>();
            task.ContinueWith(t => tcs.SetResult(t.Result), TaskContinuationOptions.OnlyOnRanToCompletion);
            task.ContinueWith(t => tcs.SetException(t.Exception.InnerExceptions), TaskContinuationOptions.OnlyOnFaulted);
            task.ContinueWith(t => tcs.SetCanceled(), TaskContinuationOptions.OnlyOnCanceled);

            return tcs.Task;
        }

        /// <summary>
        /// Short-hand for .ConfigureAwait(false)
        /// </summary>
        public static ConfiguredTaskAwaitable<T> Caf<T>(this Task<T> task) {
            return task.ConfigureAwait(false);
        }

        /// <summary>
        /// Short-hand for .ConfigureAwait(false)
        /// </summary>
        public static ConfiguredTaskAwaitable Caf(this Task task) {
            return task.ConfigureAwait(false);
        }
    }
}