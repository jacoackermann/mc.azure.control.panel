﻿using System;

namespace Colorware.Core.Extensions {
    public static class ObservableEx {
        /// <summary>
        /// Same as Observable.Defer(), but allows passing of a regular function.
        /// </summary>
        public static IObservable<T> Defer<T>(Func<T> func) {
            return func.ToObservable();
        }
    }
}