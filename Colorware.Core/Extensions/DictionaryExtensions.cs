﻿using System.Collections.Generic;

using JetBrains.Annotations;

namespace Colorware.Core.Extensions {
    [UsedImplicitly]
    public static class DictionaryExtensions {
        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key) {
            TValue value;
            dictionary.TryGetValue(key, out value);
            return value;
        }
    }
}