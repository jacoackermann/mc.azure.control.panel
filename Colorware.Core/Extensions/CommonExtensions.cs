﻿namespace Colorware.Core.Extensions {
    public static class CommonExtensions {
        public static bool IsDefault<T>(this T value) where T : struct {
            return value.Equals(default(T));
        }
    }
}