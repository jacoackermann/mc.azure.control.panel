﻿using System.Reflection;
using System.Windows;

using JetBrains.Annotations;

namespace Colorware.Core.Extensions {
    [UsedImplicitly]
    public static class WindowExtensions {
        public static bool IsShowingAsModal(this Window window) {
            return
                (bool)
                    typeof(Window).GetField("_showingAsDialog", BindingFlags.Instance | BindingFlags.NonPublic)
                                  .GetValue(window);
        }
    }
}