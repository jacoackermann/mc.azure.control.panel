﻿using System;
using System.IO;
using System.Text;

using JetBrains.Annotations;

namespace Colorware.Core.Extensions {
    public static class StringExtensions {
        /// <summary>
        /// Truncates the string to the specified length.
        /// </summary>
        /// <param name="value">The string to truncate.</param>
        /// <param name="maxLength">Length to truncate the string to.</param>
        /// <returns></returns>
        public static string Truncate([NotNull] this string value, int maxLength) {
            if (value == null) throw new ArgumentNullException("value");

            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        /// <summary>
        /// Splits a string using another string as a separator.
        /// </summary>
        /// <remarks>This is just a wrapper for String.Split() that takes an array of strings.</remarks>
        public static string[] Split([NotNull] this string s, string separator) {
            if (s == null) throw new ArgumentNullException("s");

            return s.Split(new[] {separator}, StringSplitOptions.None);
        }

        /// <summary>
        /// Splits a string using another string as a separator.
        /// </summary>
        /// <remarks>This is just a wrapper for String.Split() that takes an array of strings.</remarks>
        public static string[] Split([NotNull] this string s, string separator, StringSplitOptions options) {
            if (s == null) throw new ArgumentNullException("s");

            return s.Split(new[] {separator}, options);
        }

        public static string Replace([NotNull] this String str, [NotNull] string oldValue, string newValue,
                                     StringComparison comparison) {
            if (str == null) throw new ArgumentNullException("str");
            if (oldValue == null) throw new ArgumentNullException("oldValue");

            var sb = new StringBuilder();

            int previousIndex = 0;
            int index = str.IndexOf(oldValue, comparison);
            while (index != -1) {
                sb.Append(str.Substring(previousIndex, index - previousIndex));
                sb.Append(newValue);
                index += oldValue.Length;

                previousIndex = index;
                index = str.IndexOf(oldValue, index, comparison);
            }
            sb.Append(str.Substring(previousIndex));

            return sb.ToString();
        }

        public static MemoryStream ToStream(this string s, Encoding encoding) {
            return new MemoryStream(encoding.GetBytes(s ?? ""));
        }

        public static MemoryStream ToStream(this string s) {
            return s.ToStream(Encoding.UTF8);
        }

        public static int IndexOfNumber(this string s) {
            return s.IndexOfAny("0123456789".ToCharArray());
        }
    }
}