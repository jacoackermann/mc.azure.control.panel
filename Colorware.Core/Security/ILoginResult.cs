using System;
using System.Collections.Generic;

using Colorware.Core.Authentication;

using JetBrains.Annotations;

namespace Colorware.Core.Security {
    public interface ILoginResult {
        /// <summary>
        /// The type of failure if login failed.
        /// </summary>
        [CanBeNull]
        String LoginErrorMessage { get; }

        /// <summary>
        /// If login succeeds, this will hold the data of the logged in user.
        /// </summary>
        [CanBeNull]
        IUser LoggedInUser { get; }

        /// <summary>
        /// Server version.
        /// </summary>
        [NotNull]
        string ServerVersion { get; }

        /// <summary>
        /// Server system information, to be included in client exception reports.
        /// </summary>
        [NotNull]
        string ServerSystemInfo { get; }

        /// <summary>
        /// Time at server when this ILoginResult was sent to the client.
        /// </summary>
        DateTimeOffset ServerTime { get; }

        /// <summary>
        /// Needed because we cache the result but still want a fresh ServerTime each time an ILoginResult
        /// is sent to the client.
        /// </summary>
        void SetServerTimeToNow();

        /// <summary>
        /// When not null, will contain an indication of the used harddisk space and if the set limit
        /// has been reached.
        /// </summary>
        [CanBeNull]
        IStorageWarningCheckerResultDto HddSpaceResult { get; }

        /// <summary>
        /// When not null, will contain an indication of the used database space and if the set limit
        /// has been reached.
        /// </summary>
        [CanBeNull]
        IStorageWarningCheckerResultDto DatabaseSpaceResult { get; }


        /// <summary>
        /// Entries for the user profile that belongs to the user group that this user belongs to.
        /// </summary>
        [NotNull]
        List<ILoginUserProfileEntry> UserProfileEntries { get; }

        /// <summary>
        /// Entries that are used to override local translation keys for the current user.
        /// </summary>
        [NotNull]
        List<ITranslationOverrideEntry> TranslationOverrideEntries { get; }
    }
}