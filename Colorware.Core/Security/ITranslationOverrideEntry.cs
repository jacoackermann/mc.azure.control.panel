﻿using JetBrains.Annotations;

namespace Colorware.Core.Security {
    /// <summary>
    /// Entry to override a single translation key with the value supplied in <see cref="Value"/>.
    /// </summary>
    public interface ITranslationOverrideEntry {
        /// <summary>
        /// The translation key to override.
        /// </summary>
        [NotNull]
        string Key { get; }

        /// <summary>
        /// The value to override the entry with the given <see cref="Key"/> with.
        /// </summary>
        [NotNull]
        string Value { get; }
    }
}