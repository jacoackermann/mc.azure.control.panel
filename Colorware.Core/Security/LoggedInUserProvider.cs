﻿using System;

using Colorware.Core.Authentication;
using Colorware.Core.Functional.Option;

using JetBrains.Annotations;

namespace Colorware.Core.Security {
    [Obsolete("Use ILoggedInUserRepository instead.")]
    public static class LoggedInUserProvider {
        public static IReadOnlyUser LoggedInUser {
            [NotNull]
            get => GlobalContainer.Current.Resolve<ILoggedInUserRepository>()
                                  .GetLoggedInUser()
                                  .OrElse(() => new InvalidOperationException(
                                              $"Tried to access {LoggedInUser}.get() while no user is logged in!"));
        }

        public static bool IsUserLoggedIn =>
            GlobalContainer.Current.Resolve<ILoggedInUserRepository>().GetLoggedInUser().HasValue;
    }
}