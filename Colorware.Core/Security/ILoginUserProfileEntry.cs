﻿using JetBrains.Annotations;

namespace Colorware.Core.Security {
    public interface ILoginUserProfileEntry {
        [NotNull]
        string Key { get; }

        [NotNull]
        string Value { get; }

        bool IsSet { get; }
    }
}