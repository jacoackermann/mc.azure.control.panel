﻿using System;

using Colorware.Core.Authentication;

using ProtoBuf;

namespace Colorware.Core.Security {
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class User : IUser {
        public String Username { get; set; }

        public String Password { get; set; }

        public long Id { get; set; }

        public long CompanyId { get; set; }

        public long UserGroupId { get; set; }

        public String CompanyName { get; set; }

        public String CompanyCity { get; set; }

        public String CompanyAddress { get; set; }

        public String CompanyTelephone { get; set; }

        public Guid ForcedProductGuid { get; set; }
        public bool RemoteClientsEnabled { get; set; }

        public Boolean IsAdmin { get; set; }

        public bool IsRemoteClient { get; set; }

        public string CustomReportsUrl { get; set; }
        private User() : this("", "", false) {

        }

        public User(string username, string password, bool isRemoteClient) {
            Username = username;
            Password = password;
            IsRemoteClient = isRemoteClient;
        }
    }
}