﻿using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.Security {
    public interface IUserLoginProvider {
        // The version string is currently ignored but can be used to force clients to
        // use the right version
        [ItemNotNull]
        Task<ILoginResult> LoginAsync([NotNull] string username, [NotNull] string password, [NotNull] string version);
    }
}