﻿using System;
using System.Security;
using System.Threading.Tasks;

using Colorware.Core.Domain.Security;

using JetBrains.Annotations;

namespace Colorware.Core.Security {
    public interface IUserLoginService {
        /// <summary>
        /// NB: The version string is currently ignored but can be used to force clients to
        /// use the right version
        /// </summary>
        [ItemNotNull]
        Task<ILoginResult> Login([NotNull] string username, [NotNull] string password, [NotNull] string version);
    }

    public static class IUserLoginServiceExtensions {
        [ItemNotNull]
        public static async Task<ILoginResult> LoginOrThrow(this IUserLoginService userLoginService,
                                                            [NotNull] SimpleAuthentication authentication) {
            if (authentication == null) throw new ArgumentNullException(nameof(authentication));

            var loginResult =
                await userLoginService.Login(authentication.Username, authentication.GetUnencryptedPassword(), "");

            if (loginResult.LoggedInUser == null)
                throw new SecurityException("Invalid user credentials: " + loginResult.LoginErrorMessage);

            return loginResult;
        }
    }
}