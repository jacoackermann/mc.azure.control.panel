﻿using Colorware.Core.Authentication;
using Colorware.Core.Functional.Option;

namespace Colorware.Core.Security {
    public interface ILoggedInUserRepository {
        Option<IUser> GetLoggedInUser();
        void SetLoggedInUser(Option<IUser> user);
    }

    public static class ILoggedInUserRepositoryExtensions {
        public static Option<bool> LoggedInUserIsRemoteClient(this ILoggedInUserRepository loggedInUserRepository) {
            return loggedInUserRepository.GetLoggedInUser().Select(user => user.IsRemoteClient);
        }
    }
}