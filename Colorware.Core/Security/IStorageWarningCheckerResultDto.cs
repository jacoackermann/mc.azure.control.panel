﻿namespace Colorware.Core.Security {
    public interface IStorageWarningCheckerResultDto {
        long TotalAvailableSizeInBytes { get; }
        long ActualUsedSizeInBytes { get; }
        long CurrentLimitInBytes { get; }
        bool LimitExceeded { get; }
    }
}