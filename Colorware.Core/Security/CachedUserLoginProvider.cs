﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.Security {
    [UsedImplicitly]
    public class CachedUserLoginProvider : IUserLoginProvider {
        private readonly object cacheLock = new object();

        private readonly IUserLoginProvider wrappedProvider;

        private readonly Dictionary<string, Dictionary<string, ILoginResult>> cachedLogins =
            new Dictionary<string, Dictionary<string, ILoginResult>>();

        public CachedUserLoginProvider(IUserLoginProvider wrappedProvider) {
            this.wrappedProvider = wrappedProvider;
        }

        public async Task<ILoginResult> LoginAsync(string username, string password, string version) {
            if (username == null) throw new ArgumentNullException(nameof(username));
            if (password == null) throw new ArgumentNullException(nameof(password));
            if (version == null) throw new ArgumentNullException(nameof(version));

            var loginResult = getCachedResult(username, password);
            if (loginResult == null) {
                loginResult = await wrappedProvider.LoginAsync(username, password, version);
                cacheLoginResult(loginResult, username, password);
            }
            else {
                // Refresh timestamp in case it was obtained from the cache
                loginResult.SetServerTimeToNow();
            }

            LoggedInUserProvider.LastLoggedInUserOnThisThread = loginResult.LoggedInUser;

            return loginResult;
        }

        [CanBeNull]
        private ILoginResult getCachedResult([NotNull] string username, [NotNull] string password) {
            Dictionary<string, ILoginResult> forUsername;
            ILoginResult result;

            lock (cacheLock) {
                if (cachedLogins.TryGetValue(username, out forUsername) &&
                    forUsername.TryGetValue(password, out result))
                    return result;
            }

            return null;
        }

        private void cacheLoginResult(ILoginResult result, [NotNull] string username, string password) {
            lock (cacheLock) {
                if (!cachedLogins.ContainsKey(username))
                    cachedLogins.Add(username, new Dictionary<string, ILoginResult>());

                cachedLogins[username][password] = result;
            }
        }

        /// <summary>
        /// This should be called whenever a user's credentials change. (Otherwise
        /// users are allowed to login using their old credentials)
        /// </summary>
        public void ClearCache() {
            lock (cacheLock)
                cachedLogins.Clear();
        }
    }
}