﻿using Colorware.Core.Authentication;
using Colorware.Core.Functional.Option;

namespace Colorware.Core.Security {
    public class ClientLoggedInUserRepository : ILoggedInUserRepository {
        private Option<IUser> loggedInUser;

        public Option<IUser> GetLoggedInUser() {
            return loggedInUser;
        }

        public void SetLoggedInUser(Option<IUser> user) {
            loggedInUser = user;
        }
    }
}