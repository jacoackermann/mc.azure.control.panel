﻿using System;

using Colorware.Core.StatusReporting;

using JetBrains.Annotations;

namespace Colorware.Core {
    /// <summary>
    /// Represents a busy report scope that can be used to automatically close a <see cref="IBusyReport"/>.
    ///
    /// Example:
    /// <code>
    /// using(var scope = globalStatusReporter.AddBusyReportWithScope(new BusyReport())) {
    ///     // ...
    /// }
    /// </code>
    /// </summary>
    public class BusyReportScope : IDisposable {
        private IGlobalStatusReporter globalStatusReporter;
        private IBusyReport innerReport;

        public IBusyReport BusyReport {
            get {
                return innerReport;
            }
        }

        public BusyReportScope([NotNull] IGlobalStatusReporter globalStatusReporter, [NotNull] IBusyReport innerReport) {
            if (globalStatusReporter == null) throw new ArgumentNullException("globalStatusReporter");
            if (innerReport == null) throw new ArgumentNullException("innerReport");
            this.globalStatusReporter = globalStatusReporter;
            this.innerReport = innerReport;
        }

        public void Dispose() {
            if(globalStatusReporter == null)
                throw new NullReferenceException("Status reporter was null, this may mean that the busy reporting scope was already disposed");
            globalStatusReporter.RemoveBusyReport(innerReport);
            globalStatusReporter = null;
            innerReport = null;
        }
    }
}