using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace Colorware.Core.Config.ConfigIO {
    public class ConfigItemsExporter {
        private readonly Dictionary<string, string> configItems;

        public ConfigItemsExporter() {
            this.configItems = new Dictionary<string, string>();
        }

        public ConfigItemsExporter(Dictionary<string, string> configItems) {
            this.configItems = configItems;
        }

        public void Add(string key, string value) {
            configItems.Add(key, value);
        }

        public void Export(string path) {
            using (var stream = new FileStream(path, FileMode.Create)) {
                Export(stream);
            }
        }

        private void Export(Stream stream) {
            var settings = new XmlWriterSettings {Encoding = Encoding.UTF8, Indent = true};
            using (var xml = XmlWriter.Create(stream, settings)) {
                xml.WriteStartElement("ColorwareConfiguration");
                foreach (var entry in configItems) {
                    exportEntry(xml, entry);
                }
                xml.WriteEndElement();
            }
        }

        private void exportEntry(XmlWriter xml, KeyValuePair<string, string> entry) {
            xml.WriteStartElement("Item");
            xml.WriteAttributeString("Key", entry.Key);
            xml.WriteAttributeString("Value", entry.Value);
            xml.WriteEndElement();
        }
    }
}