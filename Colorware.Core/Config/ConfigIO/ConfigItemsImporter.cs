using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace Colorware.Core.Config.ConfigIO {
    public class ConfigItemsImporter {
        public Dictionary<string, string> Import(string filepath) {
            using (var stream = new FileStream(filepath, FileMode.Open)) {
                return Import(stream);
            }
        }

        public Dictionary<string, string> Import(Stream stream) {
            var result = new Dictionary<string, string>();
            using (var xml = XmlReader.Create(stream)) {
                // Find start tag.
                var found = false;
                while (xml.Read()) {
                    if (xml.Name == "ColorwareConfiguration") {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    throw new Exception("Invalid configuration format");
                while (true) {
                    if (!xml.Read())
                        return result;
                    if (xml.Name == "Item")
                        parseEntry(xml, result);
                }
            }
        }

        private void parseEntry(XmlReader xml, Dictionary<string, string> result) {
            var key = xml.GetAttribute("Key");
            var value = xml.GetAttribute("Value");
            if (key == null || value == null)
                return;
            result.Add(key, value);
        }
    }
}