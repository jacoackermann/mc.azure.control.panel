﻿using System.Diagnostics;

namespace Colorware.Core.Config.ConfigIO {
    /// <summary>
    /// Data transfer object that is used to represent the key/value pair used in the current
    /// configuration system.
    /// </summary>
    [DebuggerDisplay("{" + nameof(debuggerDisplay) + ",nq}")]
    public struct ConfigItemDto {
        /// <summary>
        /// Create a new object.
        /// </summary>
        /// <param name="key">The key of the configuration item.</param>
        /// <param name="value">The value of the configuration item.</param>
        public ConfigItemDto(string key, string value) {
            Key = key;
            Value = value;
        }

        public string Key { get; }
        public string Value { get; }

        private string debuggerDisplay =>
            $"ConfigItemDto({Key}, {Value})";
    }
}