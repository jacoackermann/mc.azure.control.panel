﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Functional.Result;

namespace Colorware.Core.Config.ConfigIO {
    /// <inheritdoc cref="GetConfigurationItemsAsync"/>
    public interface IConfigurationProvider {
        /// <summary>
        /// Get a collection of configuration items from an undetermined configuration source.
        /// </summary>
        /// <returns>
        /// A <see cref="Result"/> containing a collection of all configuration items retrieved or an
        /// error if something went wrong.
        /// </returns>
        Task<Result<IEnumerable<ConfigItemDto>>> GetConfigurationItemsAsync();
    }
}