﻿using System;
using System.Collections.Generic;

using JetBrains.Annotations;

using Microsoft.Win32;

namespace Colorware.Core.Config {
    public class RegistryConfig : BaseConfig {
        private const string BaseRegistryKey = @"Software\Colorware\Config";

        public RegistryConfig() {
        }

        [ContractAnnotation("defaultValue: null => CanBeNull")]
        public override string Get([NotNull] string key, [CanBeNull] string defaultValue) {
            var regKey = Registry.CurrentUser.OpenSubKey(BaseRegistryKey);
            if (regKey == null) {
                Set(key, defaultValue);
                return defaultValue;
            }
            var value = regKey.GetValue(key) as string;
            if (value == null) {
                regKey.Close();
                Set(key, defaultValue);
                return defaultValue;
            }
            regKey.Close();
            return value;
        }

        public override void Set([NotNull] string key, [CanBeNull] string value) {
            if (key == null) throw new ArgumentNullException("key");

            if (value == null)
                value = "";

            var regKey = Registry.CurrentUser.CreateSubKey(BaseRegistryKey);
            if (regKey != null) {
                regKey.SetValue(key, value, RegistryValueKind.String);
                regKey.Close();
            }
            invokeConfigValueChanged(key, value);
        }

        [NotNull]
        public override Dictionary<string, string> GetItems() {
            var result = new Dictionary<string, string>();

            var regKey = Registry.CurrentUser.OpenSubKey(BaseRegistryKey);
            if (regKey == null) {
                return result;
            }
            var subKeyNames = regKey.GetValueNames();
            foreach (var keyName in subKeyNames) {
                var subKeyKind = regKey.GetValueKind(keyName);
                if (subKeyKind != RegistryValueKind.String)
                    continue;
                var value = regKey.GetValue(keyName, "") as string;
                if (value != null)
                    result.Add(keyName, value);
            }
            regKey.Close();
            return result;
        }

        public override void Delete(string key) {
            var regKey = Registry.CurrentUser.OpenSubKey(BaseRegistryKey, true);
            if (regKey == null)
                return;
            regKey.DeleteValue(key, false);
            regKey.Close();
        }
    }
}