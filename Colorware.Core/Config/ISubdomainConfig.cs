using System;
using System.Collections.Generic;

using JetBrains.Annotations;

namespace Colorware.Core.Config
{
    public delegate void SubdomainConfigValueChangedHandler(object sender, SubdomainConfigValueChangedEventArgs e);

    public interface ISubdomainConfig
    {
        event SubdomainConfigValueChangedHandler ConfigValueChanged;
        void Notify(string subdomain, [NotNull] string key);
        void Notify(string subdomain, [NotNull] string key, string defaultValue);
        void Notify(string subdomain, [NotNull] string key, bool defaultValue);

        [CanBeNull]
        string Get(string subdomain, [NotNull] string key);

        [ContractAnnotation("defaultValue: null => CanBeNull")]
        string Get(string subdomain, [NotNull] string key, [CanBeNull] string defaultValue);

        Dictionary<string, string> GetItems(string subdomain);
        void Set(string subdomain, [NotNull] string key, [CanBeNull] string value);
        void Delete(string subdomain, [NotNull] string key);

        double GetDouble(string subdomain, [NotNull] string key, double defaultValue);
        void SetDouble(string subdomain, [NotNull] string key, double value);

        int GetInt(string subdomain, [NotNull] string key, int defaultValue);
        void SetInt(string subdomain, [NotNull] string key, int value);

        long GetLong(string subdomain, [NotNull] string key, long defaultValue);
        void SetLong(string subdomain, [NotNull] string key, long value);

        bool GetBool(string subdomain, [NotNull] string key, bool defaultValue);
        void SetBool(string subdomain, [NotNull] string key, bool value);

        DateTime GetDateTime(string subdomain, [NotNull] string key, DateTime defaultValue);
        void SetDateTime(string subdomain, [NotNull] string key, DateTime value);

        TimeSpan GetTimeSpan(string subdomain, [NotNull] string key, TimeSpan defaultValue);
        void SetTimeSpan(string subdomain, [NotNull] string key, TimeSpan value);
    }
}