﻿namespace Colorware.Core.Config {
    /// <summary>
    /// Class created for backwards compatibility. Moved all important functionality to
    /// <see cref="RegistryConfig"/>. -- 2013-12-09 Marcel
    /// </summary>
    public class Config {
        private static IConfig defaultConfig;

        public static IConfig DefaultConfig {
            get { return defaultConfig ?? (defaultConfig = new RegistryConfig()); }
            set { defaultConfig = value; }
        }
    }
}