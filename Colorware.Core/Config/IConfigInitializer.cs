﻿using System.Threading.Tasks;

namespace Colorware.Core.Config {
    /// <inheritdoc cref="InitializeDefaultValues"/>
    public interface IConfigInitializer {
        /// <summary>
        /// Initializes the configuration system with default values. Where the values originate from
        /// is not defined in this interface. Currently it is used to allow the local config to be
        /// overridden with values from a static configuration file.
        /// </summary>
        Task InitializeDefaultValues();
    }
}