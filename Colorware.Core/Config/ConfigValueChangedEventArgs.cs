using System;

namespace Colorware.Core.Config {
    public class ConfigValueChangedEventArgs : EventArgs {
        public String Key;
        public String Value;

        public ConfigValueChangedEventArgs(string key, string value) {
            Key = key;
            Value = value;
        }

        public bool ValueAsBool {
            get {
                if (Value == null)
                    return false;
                return Value == "1";
            }
        }
    }
}