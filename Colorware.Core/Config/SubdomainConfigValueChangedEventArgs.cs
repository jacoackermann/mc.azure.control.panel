using System;

namespace Colorware.Core.Config {
    public class SubdomainConfigValueChangedEventArgs : EventArgs {
        public String Key;
        public String Value;

        public SubdomainConfigValueChangedEventArgs(string key, string value) {
            Key = key;
            Value = value;
        }

        public bool ValueAsBool {
            get {
                if (Value == null)
                    return false;
                return Value == "1";
            }
        }
    }
}