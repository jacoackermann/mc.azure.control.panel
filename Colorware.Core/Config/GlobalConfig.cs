﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

using Colorware.Core.Helpers;

namespace Colorware.Core.Config {
    /// <summary>
    /// Class to hold application wide static configuration options.
    /// </summary>
    public static class GlobalConfig {
        /// <summary>
        /// Name of the application data directory.
        /// </summary>
        public const String ApplicationDataDir = "Colorware";

        public const String FlexoConfigFile = "Flexo.config";

        public static string GetFlexoConfigurationPath() {
            var appdir = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            return Path.Combine(appdir, FlexoConfigFile);
        }

        /// <summary>
        /// Gets the data path for a given file. The returned path will point to
        /// a file in the roaming user data directory.
        /// </summary>
        /// <param name="filename">The filename that is to be appended to the path to construct the full path.</param>
        /// <returns>The full path.</returns>
        public static string GetDataPath(string filename) {
            var applicationDataDir = GetDataPath();
            var fullPath = Path.Combine(applicationDataDir, filename);
            return fullPath;
        }

        public static string GetDataPath() {
            var dataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var applicationDataDir = Path.Combine(dataDir, ApplicationDataDir);
            if (!Directory.Exists(applicationDataDir))
                Directory.CreateDirectory(applicationDataDir);
            return applicationDataDir;
        }

        /// <summary>
        /// Gets application the data path. The returned path will point to the roaming user data directory.
        /// </summary>
        /// <returns>The full path.</returns>
        public static string GetDataDir() {
            var dataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var applicationDataDir = Path.Combine(dataDir, ApplicationDataDir);
            if (!Directory.Exists(applicationDataDir))
                Directory.CreateDirectory(applicationDataDir);
            return applicationDataDir;
        }

        /// <summary>
        /// Gets the program data path (%programdata%/colorware/measurecolor).
        /// </summary>
        /// <returns>The full path.</returns>
        public static string GetProgramDataDir() {
            var programDir = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            var applicationProgramDir = Path.Combine(programDir, ApplicationDataDir, ApplicationHelper.ApplicationName);
            if (!Directory.Exists(applicationProgramDir)) {
                Directory.CreateDirectory(applicationProgramDir);
            }
            return applicationProgramDir;
        }

        /// <summary>
        /// Get all Gui config configuration paths.
        /// </summary>
        /// <returns>A list of paths in which gui configuration files may be stored.</returns>
        public static List<string> GetConfigPaths() {
            var result = new List<string>();
            // Configs directory in current executable directory:
            var applicationDir = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            if (applicationDir != null) {
                var fullApplicationPath = Path.Combine(applicationDir, "Configs");
                if (Directory.Exists(fullApplicationPath))
                    result.Add(fullApplicationPath);
            }

            // Configs directory in %AppData%\Colorware
            var fullDataDirPath = GetDataPath("Configs");
            if (Directory.Exists(fullDataDirPath))
                result.Add(fullDataDirPath);
            return result;
        }
    }
}