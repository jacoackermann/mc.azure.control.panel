﻿using System;
using System.ComponentModel;

namespace Colorware.Core.Config {
    /// <summary>
    /// Helper class to wrap a configuration item, combining the definition and access
    /// of a config entry for a given key.
    /// </summary>
    public abstract class ConfigItem<T> : INotifyPropertyChanged {
        public T DefaultValue { get; private set; }

        public string Key { get; private set; }

        public T Value {
            get { return getValue(); }
            set { setValue(value); }
        }

        // ReSharper disable once InconsistentNaming
        private IConfig _config;

        protected IConfig config {
            get { return _config ?? (_config = GlobalContainer.Current.Resolve<IConfig>()); }
        }

        protected ConfigItem(string key, T defaultValue) {
            Key = key;
            DefaultValue = defaultValue;
        }

        protected abstract T getValue();
        protected abstract void setValue(T value);

        // Anti memory leak
#pragma warning disable 0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 0067
    }

    public class StringConfigItem : ConfigItem<string> {
        public StringConfigItem(string key, string defaultValue) : base(key, defaultValue) {}

        protected override string getValue() {
            return config.Get(Key, DefaultValue);
        }

        protected override void setValue(string value) {
            config.Set(Key, value);
        }
    }

    public class BoolConfigItem : ConfigItem<bool> {
        public BoolConfigItem(string key, bool defaultValue) : base(key, defaultValue) {}

        protected override bool getValue() {
            return config.GetBool(Key, DefaultValue);
        }

        protected override void setValue(bool value) {
            config.SetBool(Key, value);
        }
    }

    public class IntConfigItem : ConfigItem<int> {
        public IntConfigItem(string key) : base(key, default(int)) {}
        public IntConfigItem(string key, int defaultValue) : base(key, defaultValue) {}

        protected override int getValue() {
            return config.GetInt(Key, DefaultValue);
        }

        protected override void setValue(int value) {
            config.SetInt(Key, value);
        }
    }

    public class LongConfigItem : ConfigItem<long> {
        public LongConfigItem(string key) : base(key, default(long)) {}
        public LongConfigItem(string key, long defaultValue) : base(key, defaultValue) {}

        protected override long getValue() {
            return config.GetLong(Key, DefaultValue);
        }

        protected override void setValue(long value) {
            config.SetLong(Key, value);
        }
    }

    public class EnumConfigItem<T> : ConfigItem<T> where T : struct {
        public EnumConfigItem(string key) : base(key, default(T)) {}
        public EnumConfigItem(string key, T defaultValue) : base(key, defaultValue) {}

        protected override T getValue() {
            var asString = config.Get(Key, null);
            if (asString == null)
                return DefaultValue;
            T outAsEnum;
            if (Enum.TryParse(asString, out outAsEnum)) {
                return outAsEnum;
            }
            return DefaultValue;
        }

        protected override void setValue(T value) {
            var asString = Enum.GetName(typeof(T), value);
            config.Set(Key, asString);
        }
    }
}