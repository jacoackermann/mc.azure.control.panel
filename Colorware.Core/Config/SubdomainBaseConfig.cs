﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Colorware.Core.Config {
    /// <summary>
    /// Define a configuration base which implements most <see cref="ISubdomainConfig"/> members based on 
    /// a default <see cref="Set(string, string)"/> and <see cref="Get(string, string)"/> implementation.
    /// </summary>
    public abstract class SubdomainBaseConfig : ISubdomainConfig {
        #region Events
        public event SubdomainConfigValueChangedHandler ConfigValueChanged;

        protected void invokeConfigValueChanged(string key, string value) {
            var changed = ConfigValueChanged;
            if (changed != null) changed(this, new SubdomainConfigValueChangedEventArgs(key, value));
        }

        /// <summary>
        /// Fire a config changed event for the given key. If the key is present in the config,
        /// the value will be added to the event arguments. If the key is not yet present,
        /// the value will be null.
        /// 
        /// Can be used to keep initialization code in the same place by calling
        /// this method in a startup situation and handling the values in the event callback.
        /// 
        /// Overloads are available for handling default values.
        /// </summary>
        /// <param name="subdomain"></param>
        /// <param name="key"></param>
        public void Notify(string subdomain, string key) {
            if (key == null) throw new ArgumentNullException(nameof(key));

            var value = Get(subdomain, key);
            invokeConfigValueChanged(key, value);
        }

        public void Notify(string subdomain, string key, string defaultValue) {
            if (key == null) throw new ArgumentNullException(nameof(key));

            invokeConfigValueChanged(key, Get(subdomain, key, defaultValue));
        }

        public void Notify(string subdomain, string key, bool defaultValue) {
            if (key == null) throw new ArgumentNullException(nameof(key));

            invokeConfigValueChanged(key, GetBool(subdomain, key, defaultValue) ? "1" : "0");
        }
        #endregion

        #region Abstract members
        public abstract string Get(string subdomain, string key, string defaultValue);
        public abstract void Set(string subdomain, string key, string value);
        public abstract Dictionary<string, string> GetItems(string subdomain);
        public abstract void Delete(string subdomain, string key);
        #endregion

        #region Default implementations based on Get/Set
        public string Get(string subdomain, string key) {
            if (key == null) throw new ArgumentNullException(nameof(key));

            return Get(subdomain, key, null);
        }

        public bool GetBool(string subdomain, string key, bool defaultValue) {
            if (key == null) throw new ArgumentNullException(nameof(key));

            var value = Get(subdomain, key, defaultValue ? "1" : "0");
            return value == "1" || value.ToLowerInvariant() == "true";
        }

        public void SetBool(string subdomain, string key, bool value) {
            if (key == null) throw new ArgumentNullException(nameof(key));

            Set(subdomain, key, value ? "1" : "0");
        }

        public DateTime GetDateTime(string subdomain, string key, DateTime defaultValue) {
            if (key == null) throw new ArgumentNullException(nameof(key));

            var value = Get(subdomain, key, defaultValue.ToString("O", CultureInfo.InvariantCulture));
            try {
                return DateTime.Parse(value, CultureInfo.InvariantCulture);
            }
            catch (FormatException) {
                Set(subdomain, key, defaultValue.ToString("O", CultureInfo.InvariantCulture));
                return defaultValue;
            }
        }

        public void SetDateTime(string subdomain, string key, DateTime value) {
            if (key == null) throw new ArgumentNullException(nameof(key));

            Set(subdomain, key, value.ToString("O", CultureInfo.InvariantCulture));
        }

        public TimeSpan GetTimeSpan(string subdomain, string key, TimeSpan defaultValue) {
            if (key == null) throw new ArgumentNullException(nameof(key));

            var value = Get(subdomain, key, defaultValue.ToString("c", CultureInfo.InvariantCulture));
            try {
                return TimeSpan.Parse(value, CultureInfo.InvariantCulture);
            }
            catch (FormatException) {
                Set(subdomain, key, defaultValue.ToString("c", CultureInfo.InvariantCulture));
                return defaultValue;
            }
        }

        public void SetTimeSpan(string subdomain, string key, TimeSpan value) {
            if (key == null) throw new ArgumentNullException(nameof(key));

            Set(subdomain, key, value.ToString("c", CultureInfo.InvariantCulture));
        }

        public int GetInt(string subdomain, string key, int defaultValue) {
            if (key == null) throw new ArgumentNullException(nameof(key));

            var value = Get(subdomain, key, defaultValue.ToString(CultureInfo.InvariantCulture));
            try {
                return Int32.Parse(value, CultureInfo.InvariantCulture);
            }
            catch (FormatException) {
                Set(subdomain, key, defaultValue.ToString(CultureInfo.InvariantCulture));
                return defaultValue;
            }
        }

        public void SetInt(string subdomain, string key, int value) {
            if (key == null) throw new ArgumentNullException(nameof(key));

            Set(subdomain, key, value.ToString(CultureInfo.InvariantCulture));
        }

        public long GetLong(string subdomain, string key, long defaultValue) {
            if (key == null) throw new ArgumentNullException(nameof(key));

            var value = Get(subdomain, key, defaultValue.ToString(CultureInfo.InvariantCulture));
            try {
                return long.Parse(value, CultureInfo.InvariantCulture);
            }
            catch (FormatException) {
                Set(subdomain, key, defaultValue.ToString(CultureInfo.InvariantCulture));
                return defaultValue;
            }
        }

        public void SetLong(string subdomain, string key, long value) {
            if (key == null) throw new ArgumentNullException(nameof(key));

            Set(subdomain, key, value.ToString(CultureInfo.InvariantCulture));
        }

        public double GetDouble(string subdomain, string key, double defaultValue) {
            if (key == null) throw new ArgumentNullException(nameof(key));

            var value = Get(subdomain, key, defaultValue.ToString(CultureInfo.InvariantCulture));
            try {
                return Double.Parse(value, CultureInfo.InvariantCulture);
            }
            catch (FormatException) {
                Set(subdomain, key, defaultValue.ToString(CultureInfo.InvariantCulture));
                return defaultValue;
            }
        }

        public void SetDouble(string subdomain, string key, double value) {
            if (key == null) throw new ArgumentNullException(nameof(key));

            Set(subdomain, key, value.ToString(CultureInfo.InvariantCulture));
        }
        #endregion
    }
}