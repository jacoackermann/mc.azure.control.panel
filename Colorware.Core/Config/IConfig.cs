using System;
using System.Collections.Generic;

using JetBrains.Annotations;

namespace Colorware.Core.Config
{
    public delegate void ConfigValueChangedHandler(object sender, ConfigValueChangedEventArgs e);

    public interface IConfig
    {
        event ConfigValueChangedHandler ConfigValueChanged;
        void Notify([NotNull] string key);
        void Notify([NotNull] string key, string defaultValue);
        void Notify([NotNull] string key, bool defaultValue);

        [CanBeNull]
        string Get([NotNull] string key);

        [ContractAnnotation("defaultValue: null => CanBeNull")]
        string Get([NotNull] string key, [CanBeNull] string defaultValue);

        Dictionary<string, string> GetItems();
        void Set([NotNull] string key, [CanBeNull] string value);
        void Delete([NotNull] string key);

        double GetDouble([NotNull] string key, double defaultValue);
        void SetDouble([NotNull] string key, double value);

        int GetInt([NotNull] string key, int defaultValue);
        void SetInt([NotNull] string key, int value);

        long GetLong([NotNull] string key, long defaultValue);
        void SetLong([NotNull] string key, long value);

        bool GetBool([NotNull] string key, bool defaultValue);
        void SetBool([NotNull] string key, bool value);

        DateTime GetDateTime([NotNull] string key, DateTime defaultValue);
        void SetDateTime([NotNull] string key, DateTime value);

        TimeSpan GetTimeSpan([NotNull] string key, TimeSpan defaultValue);
        void SetTimeSpan([NotNull] string key, TimeSpan value);
    }
}