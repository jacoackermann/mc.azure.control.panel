﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Colorware.Core.Config.ConfigIO;
using Colorware.Core.Functional.Result;
using Colorware.Core.Logging;

using JetBrains.Annotations;

namespace Colorware.Core.Config.ConfigInitializer {
    public class ConfigurationProviderBackedConfigInitializer : IConfigInitializer {
        private readonly IConfigurationProvider configurationProvider;
        private readonly IConfig config;
        private readonly ILog log;

        public ConfigurationProviderBackedConfigInitializer([NotNull] IConfigurationProvider configurationProvider,
                                                            [NotNull] IConfig config,
                                                            [NotNull] ILog log) {
            if (configurationProvider == null) throw new ArgumentNullException(nameof(configurationProvider));
            if (config == null) throw new ArgumentNullException(nameof(config));
            if (log == null) throw new ArgumentNullException(nameof(log));
            this.configurationProvider = configurationProvider;
            this.config = config;
            this.log = log;
        }

        public async Task InitializeDefaultValues() {
            await configurationProvider.GetConfigurationItemsAsync()
                .Do(items => addItemsToConfig(items))
                .ElseDo(logErrors);
        }

        private void logErrors(Result<IEnumerable<ConfigItemDto>> result) {
            var errors = result.GetErrorsOrEmpty();
            if (!errors.Any()) {
                return;
            }
            var errorStr = string.Join("; ", errors.Select(x => x.GetError()));
            log.Info($"No config overrides loaded: {errorStr}");
        }

        private void addItemsToConfig(IEnumerable<ConfigItemDto> items) {
            foreach (var item in items) {
                config.Set(item.Key, item.Value);
            }
        }
    }
}