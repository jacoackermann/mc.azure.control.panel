﻿using System;

using Colorware.Core.Enums;
using Colorware.Core.Security;

using JetBrains.Annotations;

namespace Colorware.Core.Config {
    public static class GlobalConfigKeys {
        private const string SvfShouldForceTopSideKey = "Exporters.Svf.ScansideForceTop";

        public static bool SvfShouldForceTopSide {
            get => Config.DefaultConfig.GetBool(SvfShouldForceTopSideKey, false);
            set => Config.DefaultConfig.SetBool(SvfShouldForceTopSideKey, value);
        }

        public const String ShouldExportTvfAlongWithSvfKey = "Importers.Svf.ShouldExportTvfAlongWithSvf";

        public static bool ShouldExportTvfAlongWithSvf {
            get => Config.DefaultConfig.GetBool(ShouldExportTvfAlongWithSvfKey, false);
            set => Config.DefaultConfig.SetBool(ShouldExportTvfAlongWithSvfKey, value);
        }

        public const String ShouldInverseImportedSamplesKey = "Importers.General.InvertSamples";

        public static bool ShouldInvertImportedSamples {
            get => Config.DefaultConfig.GetBool(ShouldInverseImportedSamplesKey, false);
            set => Config.DefaultConfig.SetBool(ShouldInverseImportedSamplesKey, value);
        }

        /// <summary>
        /// Enable/disable trajectory display.
        /// </summary>
        private const string ChromaTrackTrajectoryEnabledConfigKey = "Defaults.ChromaTrack.Enabled";

        public static bool ChromaTrackTrajectoryEnabled {
            get => Config.DefaultConfig.GetBool(ChromaTrackTrajectoryEnabledConfigKey, true);
            set => Config.DefaultConfig.SetBool(ChromaTrackTrajectoryEnabledConfigKey, value);
        }

        /// <summary>
        /// Number of subdivisions to calculate for the trajectory.
        /// </summary>
        private const string ChromaTrackTrajectorySubdivisionsConfigKey = "Defaults.ChromaTrack.SubDivisions";

        public static int ChromaTrackTrajectorySubdivisions {
            get => Config.DefaultConfig.GetInt(ChromaTrackTrajectorySubdivisionsConfigKey, 3);
            set => Config.DefaultConfig.SetInt(ChromaTrackTrajectorySubdivisionsConfigKey, value);
        }

        /// <summary>
        /// KFactor to extend to.
        /// </summary>
        private const string ChromaTrackTrajectoryExtendsConfigKey = "Defaults.ChromaTrack.Extends";

        public static double ChromaTrackTrajectoryExtends {
            get => Config.DefaultConfig.GetDouble(ChromaTrackTrajectoryExtendsConfigKey, 0.15);
            set => Config.DefaultConfig.SetDouble(ChromaTrackTrajectoryExtendsConfigKey, value);
        }

        private const string ServerHostConfigKey = "Options.Server.Host";

        public static string ServerHost {
            [NotNull] get { return Config.DefaultConfig.Get(ServerHostConfigKey, "http://localhost:3000"); }
            [CanBeNull] set { Config.DefaultConfig.Set(ServerHostConfigKey, value); }
        }
        
        public const string LicenseHostConfigKey = "Options.License.Host";

        private const string DefaultAlignmentKey = "Defaults.ProcessControl.ColorbarAlignment";
        private const string MeasurementIsWetKey = "ProcessControl.CurrentMeasurementIsWet";
        public const string TopSideSortingKey = "Options.InkZoneSorting.Top";
        public const string BottomSideSortingKey = "Options.InkZoneSorting.Bottom";
        public const string NaSideSortingKey = "Options.InkZoneSorting.Na";

        // If true, user must fill in remarks field (through popup) before saving a measurement
        public const string MeasurementForceRemarksKey = "Options.Measurement.ForceRemarks";

        public static bool MeasurementForceRemarks {
            get => Config.DefaultConfig.GetBool(MeasurementForceRemarksKey, false);
            set => Config.DefaultConfig.SetBool(MeasurementForceRemarksKey, value);
        }

        public static readonly EnumConfigItem<ColorbarAlignment> ColorbarAlignment =
            new EnumConfigItem<ColorbarAlignment>(Core.Config.GlobalConfigKeys.DefaultAlignmentKey,
                                                  Core.Enums.ColorbarAlignment.Center);

        public static readonly BoolConfigItem MeasurementIsWet =
            new BoolConfigItem(MeasurementIsWetKey, true);

        /// <summary>
        /// Specifies the dotgain entries to show on the gamut screen. The entries
        /// are groups separated by a comma. Each entry represents a dotgain tint to
        /// be shown for the group if it is available. Items coming first in a group
        /// have a higher priority over items that come later. As an example,
        /// 20;30;25,40;50,70;80;75 should be read as
        /// (20 or 30 or 25) and (40 or 50) and (70 or 80 or 75).
        /// </summary>
        public static readonly StringConfigItem GamutDotgainGraphEntries =
            new StringConfigItem("Options.Gamut.Dotgains", "20;30;25;10,40;50,70;80;75");

        public const string TechkonDeviceSimulatorEnabledKey = "Techkon.Devices.SimulatedDeviceActive";

        public static bool TechkonDeviceSimulatorEnabled {
            get { return Config.DefaultConfig.GetBool(TechkonDeviceSimulatorEnabledKey, false); }
            set { Config.DefaultConfig.SetBool(TechkonDeviceSimulatorEnabledKey, value); }
        }

        private const String ExportManagerAddTimeStampToExportsKey = "Options.ExportManager.AddTimestamp";

        public static bool ExportManagerAddTimeStampToExports {
            get { return Config.DefaultConfig.GetBool(ExportManagerAddTimeStampToExportsKey, false); }
            set { Config.DefaultConfig.SetBool(ExportManagerAddTimeStampToExportsKey, value); }
        }

        private const String PlaceInClientJobFolderKey = "Options.ExportManager.PlaceInClientJobFolder";

        public static bool PlaceInClientJobFolder {
            get { return Config.DefaultConfig.GetBool(PlaceInClientJobFolderKey, false); }
            set { Config.DefaultConfig.SetBool(PlaceInClientJobFolderKey, value); }
        }
    }
}