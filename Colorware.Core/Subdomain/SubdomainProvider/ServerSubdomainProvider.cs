﻿using System;
using System.Web;

namespace Colorware.Core.Subdomain.SubdomainProvider {
    public static class ServerSubdomainProvider {
        public static string Subdomain {
            get {
                var returnVal = getSubdomain();
                return returnVal;
            }
        }

        //---------------------------------------------------------------------

        private static string getSubdomain() {
            try {
                var domain = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                domain = domain.Replace(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Scheme), "");

                var domainTokens = domain.Split(new[] {'.'}, StringSplitOptions.RemoveEmptyEntries);

                if (domainTokens.Length > 0) {
                    return domainTokens[0];
                }

                return "";
            }
            catch (Exception e) {
                // TODO - Subdomain!
            }

            return "";
        }
    }
}