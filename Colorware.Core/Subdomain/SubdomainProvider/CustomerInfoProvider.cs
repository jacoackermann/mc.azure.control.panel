﻿using System;

namespace Colorware.Core.Subdomain.SubdomainProvider {
    public static class CustomerInfoProvider {
        public static void GetCustomerInfo(string subdomain, out string customerName, out string customerLocation) {
            var tokens = subdomain.Split(new[] {'-'}, StringSplitOptions.RemoveEmptyEntries);

            customerName = "";
            customerLocation = "";

            if (tokens.Length > 0) {
                customerName = tokens[0];

                if (tokens.Length > 1) {
                    customerLocation = tokens[1];
                }
            }
        }
    }
}