﻿using System.Configuration;

namespace Colorware.Core.Subdomain.SubdomainProvider {
    public static class ClientSubdomainProvider {
        public static string Subdomain => ConfigurationManager.AppSettings["subdomain"];
    }
}