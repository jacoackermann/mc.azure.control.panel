﻿using System;

using Castle.Core.Logging;

using Colorware.Core.Interface.CustomerInfo;

using JetBrains.Annotations;

namespace Colorware.Core.Subdomain.ConnectionStringProvider {
    public class ServerConnectionStringProvider : IConnectionStringProvider {
        private readonly ICustomerInfoProvider customerInfoProvider;
        private readonly ILogger logger;

        //---------------------------------------------------------------------

        public string ConnectionString => getConnectionString();

        //---------------------------------------------------------------------

        public ServerConnectionStringProvider([NotNull] ICustomerInfoProvider customerInfoProvider,
                                              [NotNull] ILogger logger) {
            this.customerInfoProvider =
                customerInfoProvider ?? throw new ArgumentNullException(nameof(customerInfoProvider));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        //---------------------------------------------------------------------

        private string getConnectionString() {
            var connectionString = customerInfoProvider.GetCurrentConnectionString();
            //logger.Info($"connection string:  {connectionString}");
            return connectionString;
        }
    }
}