﻿namespace Colorware.Core.Subdomain.ConnectionStringProvider {
    public interface IConnectionStringProvider {
        string ConnectionString { get; }
    }
}