﻿using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Colorware.Core.Azure.Config;
using Colorware.Core.Azure.Enums;
using Colorware.Core.Azure.Interfaces;
using Colorware.Core.Azure.Messaging.EventGrid.Topics;

using Newtonsoft.Json;

namespace Colorware.Core.Azure.Messaging.EventGrid {
    public class EventSubscriptionManager {
        private readonly IServicePrincipleAccessTokenProvider accessTokenProvider;

        //---------------------------------------------------------------------

        public EventSubscriptionManager(IServicePrincipleAccessTokenProvider accessTokenProvider) {
            this.accessTokenProvider = accessTokenProvider;
        }

        //---------------------------------------------------------------------

        public void CreateEventGridSubscriptions() {
            // for each topic, check that all subscriptions are created

            // find these classes via reflection???
            createSubscriptions(new ServerTopic()).Wait();
        }

        //---------------------------------------------------------------------

        private async Task createSubscriptions(ITopic topic) {
            // Loop through subsriptions
            foreach (var subscription in topic.Subscriptions) {
                // Check if subscription already exists
                if (await subscriptionExists(topic.Name, subscription.Name).ConfigureAwait(false)) {
                    continue;
                }

                // Create subscription
                await createSubscription(topic.Name, topic.Controller, topic.EventType, subscription.Name,
                                         subscription.PrefixFilter,
                                         subscription.SuffixFilter)
                    .ConfigureAwait(false);

                // Wait for a while, to prevent throttling
                Thread.Sleep(50);
            }
        }

        //---------------------------------------------------------------------

        private async Task<bool> subscriptionExists(string topicName, string subscription) {
            // Check if subscription exists
            var httpClient = await createHttpClient().ConfigureAwait(false);
            var result = await httpClient.GetAsync(getEventGridSubcriptionUrl(topicName, subscription))
                                         .ConfigureAwait(false);
            return result.IsSuccessStatusCode;
        }

        //---------------------------------------------------------------------

        private async Task createSubscription(string topicName, string controller, EventGridEventTypes eventType,
                                              string subscription,
                                              string prefixFilter, string suffixFilter) {
            // Set up create subscription message
            var createSubscription = new {
                properties = new {
                    destination = new {
                        endpointType = "webhook",
                        properties = new {
                            endpointUrl =
                                $"{ConfigValueReader.EventGridSubscriptionEndpoint}/api/{controller}/{subscription}"
                        }
                    },
                    filter = new {
                        includedEventTypes = new[] {eventType.ToString()},
                        subjectBeginsWith = prefixFilter,
                        subjectEndsWith = suffixFilter,
                        subjectIsCaseSensitive = "false"
                    }
                }
            };

            // Create content to be sent
            var json = JsonConvert.SerializeObject(createSubscription);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            // Create subscription
            var httpClient = await createHttpClient().ConfigureAwait(false);
            var response = await httpClient.PutAsync(getEventGridSubcriptionUrl(topicName, subscription), content)
                                           .ConfigureAwait(false);
        }

        //---------------------------------------------------------------------

        private async Task<HttpClient> createHttpClient() {
            // Create a HTTP client
            var httpClient = new HttpClient();

            // Add key in the request headers
            var authToken = await accessTokenProvider.GetResourceManagementAccessToken();
            httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {authToken}");

            // Return the HTTP client
            return httpClient;
        }

        //---------------------------------------------------------------------

        private static string getEventGridSubcriptionUrl(string topicName, string subscription) {
            var url =
                $"https://management.azure.com/subscriptions/{ConfigValueReader.AzureSubscriptionId}/resourceGroups/{ConfigValueReader.ResourceGroupName}/providers/Microsoft.EventGrid/topics/{topicName}/providers/Microsoft.EventGrid/eventSubscriptions/{subscription}?{ConfigValueReader.EventGridApiVersion}";
            return url;
        }

        //---------------------------------------------------------------------
    }
}