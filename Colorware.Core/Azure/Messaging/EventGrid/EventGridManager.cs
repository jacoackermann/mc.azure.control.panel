﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Colorware.Core.Azure.Config;
using Colorware.Core.Azure.Interfaces;
using Colorware.Core.Azure.Messaging.EventGrid.Events;

using Newtonsoft.Json;

namespace Colorware.Core.Azure.Messaging.EventGrid {
    // check all event grid topics exist and create if not?
    // check all subscriptions exist and create if not

    public class EventGridManager : IEventGridManager {
        public async Task<string> Send(List<BaseEvent> events) {
            if (!ConfigValueReader.IsProductionMode) {
                return "Can only send events in Production Mode!";
            }

            return await sendEventsToTopic(ConfigValueReader.EventGridSecret, ConfigValueReader.EventGridTopicEndpoint,
                                           events);
        }

        /// <summary>
        /// Send events to Event Grid Topic.
        /// </summary>
        private static async Task<string> sendEventsToTopic(string sasKey, string topicEndPoint, object events) {
            // Create a HTTP client which we will use to post to the Event Grid Topic
            var httpClient = new HttpClient();

            // Add key in the request headers
            httpClient.DefaultRequestHeaders.Add("aeg-sas-key", sasKey);

            // Event grid expects event data as JSON
            var json = JsonConvert.SerializeObject(events);

            // Create request which will be sent to the topic
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            // Send request
            var result = await httpClient.PostAsync(topicEndPoint, content);

            return result.ReasonPhrase;
        }
    }
}