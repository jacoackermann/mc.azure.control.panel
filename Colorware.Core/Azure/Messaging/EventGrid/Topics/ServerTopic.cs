﻿using System.Collections.Generic;

using Colorware.Core.Azure.Enums;
using Colorware.Core.Azure.Messaging.EventGrid.Models;

namespace Colorware.Core.Azure.Messaging.EventGrid.Topics {
    public class ServerTopic : ITopic {
        public string Name => "mcserver-topic";

        public string Controller => "Maintenance";

        public EventGridEventTypes EventType => EventGridEventTypes.ServerMaintenance;

        public List<Subscription> Subscriptions => new List<Subscription> {
            new Subscription {Name = "ChangeMaintenanceMode"}
        };
    }
}