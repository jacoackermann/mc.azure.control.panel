﻿using System.Collections.Generic;

using Colorware.Core.Azure.Enums;
using Colorware.Core.Azure.Messaging.EventGrid.Models;

namespace Colorware.Core.Azure.Messaging.EventGrid.Topics {
    public interface ITopic {
        string Name { get; }

        // for lack of a better name...
        string Controller { get; }
        EventGridEventTypes EventType { get; }
        List<Subscription> Subscriptions { get; }
    }
}