﻿namespace Colorware.Core.Azure.Messaging.EventGrid.Models {
    public class ServerMaintenanceModel {
        public bool MaintenanceModeOn { get; set; }

        public ServerMaintenanceModel(bool maintenanceModeOn) {
            MaintenanceModeOn = maintenanceModeOn;
        }
    }
}