﻿using Colorware.Core.Azure.Enums;
using Colorware.Core.Azure.Messaging.EventGrid.Models;

namespace Colorware.Core.Azure.Messaging.EventGrid.Events {
    public class ServerMaintenanceEvent : BaseEvent {
        // NOTE: the Event Grid infrastructure requires a field called Data
        public ServerMaintenanceModel Data { get; set; }

        public ServerMaintenanceEvent(bool maintenanceModeOn)
            : base(EventGridEventTypes.ServerMaintenance) {
            Data = new ServerMaintenanceModel(maintenanceModeOn);
            Subject = EventGridEventTypes.ServerMaintenance.ToString();
        }
    }
}