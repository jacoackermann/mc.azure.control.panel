﻿using System;

using Colorware.Core.Azure.Enums;

namespace Colorware.Core.Azure.Messaging.EventGrid.Events {
    public class BaseEvent {
        /// Gets the unique identifier for the event.
        public string Id { get; }

        /// Gets the publisher defined path to the event subject.
        public string Subject { get; set; }

        /// Gets the registered event type for this event source.
        public string EventType { get; }

        /// Gets the time the event is generated based on the provider's UTC time.
        public string EventTime { get; }

        /// Constructor.
        public BaseEvent(EventGridEventTypes eventType) {
            Id = Guid.NewGuid().ToString();
            EventType = eventType.ToString();
            EventTime = DateTime.UtcNow.ToString("o");
        }
    }
}