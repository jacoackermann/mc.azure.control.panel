﻿using System;
using System.Globalization;
using System.Threading.Tasks;

using Colorware.Core.Azure.Config;
using Colorware.Core.Azure.Interfaces;

using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace Colorware.Core.Azure.ActiveDirectory {
    public class ServicePrinciple : IServicePrincipleAccessTokenProvider
    {
        public async Task<string> GetResourceManagementAccessToken() {
            string authority =
                String.Format(CultureInfo.InvariantCulture, ConfigValueReader.AadInstance, ConfigValueReader.Tenant);
            string clientId = ConfigValueReader.ApplicationId;
            string clientSecret = ConfigValueReader.ApplicationSecret;
            string resource = "https://management.azure.com/";

            return await getS2SAccessToken(authority, resource, clientId, clientSecret);
        }

        //---------------------------------------------------------------------

        private static async Task<string> getS2SAccessToken(
            string authority, string resource, string clientId, string clientSecret) {
            var clientCredential = new ClientCredential(clientId, clientSecret);
            AuthenticationContext context = new AuthenticationContext(authority, false);
            AuthenticationResult authenticationResult = await context.AcquireTokenAsync(
                                                            resource, // the resource (app) we are going to access with the token
                                                            clientCredential); // the client credentials
            var result = authenticationResult;
            return result.AccessToken;
        }
    }
}