﻿namespace Colorware.Core.Azure.Enums {
    public enum CustomerInfoState {
        // no customer info exists
        None,

        Creating,
        Active,

        Deactivating,
        Deactivated,

        // used with FailureReason to provide more info
        CreateFailed
    }

    public enum FailureReason {
        None,

        // activation errors
        DbCreateFailed,
        DbRestoreFailed,
        MigrationsFailed,
    }

    public enum CustomerInfoSelectOption {
        All,
        Active,
        Deactivated
    }
}