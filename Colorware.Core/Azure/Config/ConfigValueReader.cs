﻿using System.Configuration;
using System.Text.RegularExpressions;

namespace Colorware.Core.Azure.Config {
    public static class ConfigValueReader {
        public static bool IsProductionMode => bool.Parse(ConfigurationManager.AppSettings["isProductionMode"]);

        public static string ApplicationId => ConfigurationManager.AppSettings["ida:ApplicationId"];

        public static string AadInstance => ConfigurationManager.AppSettings["ida:AADInstance"];
        public static string Tenant => ConfigurationManager.AppSettings["ida:Tenant"];
        public static string PostLogoutRedirectUri => ConfigurationManager.AppSettings["ida:PostLogoutRedirectUri"];

        // Azure resource variables
        public static string ResourceGroupName => ConfigurationManager.AppSettings["resourceGroupName"];

        // TODO - can e.g. American clients be allocated in the States and Dutch clients in Europe?
        // TODO - if yes, strategy to manage this? UI? Derived from other data?

        // TODO - more than one server instance hosted in different locations?
        // eeps, the server is bound to a location... perhaps a server per significant region?
        public static string ResourceGroupLocation => ConfigurationManager.AppSettings["resourceGroupLocation"];

        public static string ServerName => ConfigurationManager.AppSettings["serverName"];
        public static string ServerAdmin => ConfigurationManager.AppSettings["serverAdmin"];
        public static string ServerAdminPassword => ConfigurationManager.AppSettings["serverAdminPassword"];

        public static string ContainedDbUser => ConfigurationManager.AppSettings["containedDbUser"];

        public static string DatabaseServiceObjective => ConfigurationManager.AppSettings["databaseServiceObjective"];
        public static string DatabaseEdition => ConfigurationManager.AppSettings["databaseEdition"];

        public static string AzureSubscriptionId => ConfigurationManager.AppSettings.Get("azureSubscriptionId");
        public static string TenantId => ConfigurationManager.AppSettings.Get("tenantId");
        public static string ApplicationSecret => ConfigurationManager.AppSettings.Get("applicationSecret");

        public static string McServerDomain => ConfigurationManager.AppSettings["mcServerDomain"];

        // used for (1) customer info in table storage and (2) all other MC server storage
        // NOTE:  this is a FULL Azure storage account
        public static string PrimaryAzureStorage => ConfigurationManager.AppSettings["StorageConnectionString"];

        // used for (1) customer database backups
        // NOTE:  this storage account is configured for BLOB storage ONLY!
        public static string SecondaryAzureStorage => ConfigurationManager.AppSettings["databaseBackupStorage"];

        //-- allocated blob container names
        public static string CustomerSqlBackupBlobContainer =>
            ConfigurationManager.AppSettings["customerSqlBackupBlobContainer"];

        //-- Mailjet service
        public static string MailjetHost => ConfigurationManager.AppSettings["mailjetHost"];
        public static int MailjetPort => int.Parse(ConfigurationManager.AppSettings["mailjetPort"]);
        public static string MailjetApiKey => ConfigurationManager.AppSettings["mailjetApiKey"];
        public static string MailjetSecretKey => ConfigurationManager.AppSettings["mailjetSecretKey"];

        //-- Event Grid Subscriptions
        public static string EventGridSubscriptionEndpoint =>
            ConfigurationManager.AppSettings["eventGridSubscriptionEndpoint"];

        //public static string EventGridTopic => ConfigurationManager.AppSettings["eventGridTopic"];
        public static string EventGridApiVersion => ConfigurationManager.AppSettings["eventGridApiVersion"];
        public static string EventGridTopicEndpoint => ConfigurationManager.AppSettings["eventGridTopicEndpoint"];
        public static string EventGridSecret => ConfigurationManager.AppSettings["eventGridSecret"];


        //-- connection strings for dev mode

        public static bool HasDevConnectionString =>
            !string.IsNullOrWhiteSpace(getDevConnectionStringFromWebConfig("ColorwareDatabase"));

        public static string GetDevConnectionString(string dbName) {
            var configConnString = getDevConnectionStringFromWebConfig("ColorwareDatabase");
            return Regex.Replace(configConnString, "Database=[^;]+", $"Database={dbName}", RegexOptions.IgnoreCase);
        }

        //---------------------------------------------------------------------

        private static string getDevConnectionStringFromWebConfig(string connectionStringName) {
            foreach (ConnectionStringSettings cs in ConfigurationManager.ConnectionStrings) {
                if (cs.Name != connectionStringName)
                    continue;

                return cs.ConnectionString;
            }

            return "";
        }
    }
}