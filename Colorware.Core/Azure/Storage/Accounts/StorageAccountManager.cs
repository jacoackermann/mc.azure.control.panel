﻿using System;

using Colorware.Core.Azure.Config;

using Microsoft.Azure.Management.Storage.Fluent;
using Microsoft.WindowsAzure.Storage;

namespace Colorware.Core.Azure.Storage.Accounts {
    public interface IStorageAccountManager {
        // Fluent storage acc interface
        IStorageAccount AppStorageAccount { get; }

        // old storage acc interface
        CloudStorageAccount PrimaryStorageAccountOld { get; }
        CloudStorageAccount SecondaryStorageAccountOld { get; }
    }

    //=========================================================================

    public class StorageAccountManager : IStorageAccountManager {
        // Fluent storage acc interface
        public IStorageAccount AppStorageAccount { get; }

        // old storage acc interface
        public CloudStorageAccount PrimaryStorageAccountOld { get; }
        public CloudStorageAccount SecondaryStorageAccountOld { get; }

        //---------------------------------------------------------------------

        public StorageAccountManager() {
            // TODO - look into using the Fluent interface for storage accounts
            //AppStorageAccount = AzureProvider.Instance.GetInterface().StorageAccounts.Define(azureStorage)
            //                                 .WithRegion(Region.Create(ConfigValueReader.ResourceGroupLocation))
            //                                 .WithNewResourceGroup(ConfigValueReader.ResourceGroupName)
            //                                 .Create();

            if (ConfigValueReader.PrimaryAzureStorage == null) {
                throw new ArgumentNullException(nameof(PrimaryStorageAccountOld));
            }

            PrimaryStorageAccountOld = CloudStorageAccount.Parse(ConfigValueReader.PrimaryAzureStorage);
            SecondaryStorageAccountOld = CloudStorageAccount.Parse(ConfigValueReader.SecondaryAzureStorage);
        }

        //---------------------------------------------------------------------
    }
}