﻿using System;

using Castle.Core.Logging;

using Colorware.Core.Azure.Storage.Accounts;

using Microsoft.WindowsAzure.Storage.Table;

namespace Colorware.Core.Azure.Storage.TableStorage {
    public static class TableStorageUtils {
        public static CloudTable GetTable(string tableName, ILogger log = null) {
            var storageAccManager = new StorageAccountManager();
            CloudTableClient tableClient = storageAccManager.PrimaryStorageAccountOld.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(tableName);

            try {
                table.CreateIfNotExists();
            }
            catch (Exception e) {
                log?.Info(e.Message);
            }

            return table;
        }
    }
}