﻿using System;

using Colorware.Core.Azure.Enums;

using Microsoft.WindowsAzure.Storage.Table;

namespace Colorware.Core.Azure.Storage.TableStorage.CustomerInfo {
    public class CustomerInfoEntity : TableEntity {
        public string Server { get; set; }
        public string DatabaseName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Domain { get; set; }

        [IgnoreProperty]
        public CustomerInfoState ActiveStateEnum { get; set; }

        public string ActiveState {
            get => ActiveStateEnum.ToString();
            set => ActiveStateEnum = (CustomerInfoState)Enum.Parse(typeof(CustomerInfoState), value);
        }

        [IgnoreProperty]
        public FailureReason FailureReasonEnum { get; set; }

        public string FailureReason {
            get => FailureReasonEnum.ToString();
            set => FailureReasonEnum = (FailureReason)Enum.Parse(typeof(FailureReason), value);
        }

        public string ConnectionString { get; set; }
        public string ServerVersion { get; set; }

        //---------------------------------------------------------------------

        public CustomerInfoEntity() {
            ActiveState = CustomerInfoState.Deactivated.ToString();
        }

        //---------------------------------------------------------------------

        public CustomerInfoEntity(string customerName, string customerLocation, string dbName,
                                  string userName, string password,
                                  string server, string domain,
                                  string connectionString, string serverVersion,
                                  CustomerInfoState activeState,
                                  FailureReason failureReason) {
            PartitionKey = customerName;
            RowKey = customerLocation;

            DatabaseName = dbName;
            UserName = userName;
            Password = password;

            Server = server;
            Domain = domain;

            ActiveStateEnum = activeState;
            FailureReasonEnum = failureReason;

            ConnectionString = connectionString;
            ServerVersion = serverVersion;
        }

        //---------------------------------------------------------------------
    }
}