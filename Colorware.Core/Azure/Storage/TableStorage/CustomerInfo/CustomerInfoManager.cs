﻿using System;
using System.Linq;

using Colorware.Core.Azure.Enums;
using Colorware.Core.Interface.CustomerInfo;
using Colorware.Core.Subdomain.SubdomainProvider;

using Microsoft.WindowsAzure.Storage.Table;

namespace Colorware.Core.Azure.Storage.TableStorage.CustomerInfo {
    public class CustomerInfoManager : ICustomerInfoProvider {
        private readonly string emptyCustomerLocationToken = "<blank>";

        //---------------------------------------------------------------------

        public bool IsCurrentCustomerActive() {
            var subdomain = ServerSubdomainProvider.Subdomain;
            CustomerInfoProvider.GetCustomerInfo(subdomain, out string customerName, out string customerLocation);

            var customerInfo = getCustomerInfo(customerName, customerLocation);
            if (Enum.TryParse<CustomerInfoState>(customerInfo.ActiveState, out var activeState)) {
                return activeState == CustomerInfoState.Active;
            }

            return false;
        }

        //---------------------------------------------------------------------

        // TODO - need some cleaning up... esp the "hidden logic" around being active or not...
        public string GetCurrentConnectionString() {
            var subdomain = ServerSubdomainProvider.Subdomain;
            CustomerInfoProvider.GetCustomerInfo(subdomain, out string customerName, out string customerLocation);

            // TODO
            // if customer is inactive, should we return "" ????
            // why should it be possible to connect with our software if the customer is no longer active?

            var customerInfo = getCustomerInfo(customerName, customerLocation);
            if (customerInfo == null || customerInfo.ActiveState != CustomerInfoState.Active.ToString()) {
                return "";
            }

            return customerInfo.ConnectionString;
        }

        //---------------------------------------------------------------------

        private CustomerInfoEntity getCustomerInfo(string customerName, string customerLocation,
                                                   CustomerInfoSelectOption option = CustomerInfoSelectOption.Active) {
            var customerInfoTable = TableStorageUtils.GetTable("CustomerInfo");

            customerLocation = string.IsNullOrWhiteSpace(customerLocation)
                                   ? emptyCustomerLocationToken
                                   : customerLocation;

            TableQuery<CustomerInfoEntity> query = new TableQuery<CustomerInfoEntity>().Where(
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, customerName),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, customerLocation)));

            var results = customerInfoTable.ExecuteQuery(query);
            var returnValue = results.FirstOrDefault();

            if (returnValue != null)
                returnValue.RowKey = returnValue.RowKey.Replace(emptyCustomerLocationToken, "");

            return returnValue;
        }

        //---------------------------------------------------------------------
    }
}