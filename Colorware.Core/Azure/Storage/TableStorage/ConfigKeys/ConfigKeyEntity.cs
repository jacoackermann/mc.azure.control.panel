﻿using Microsoft.WindowsAzure.Storage.Table;

namespace Colorware.Core.Azure.Storage.TableStorage.ConfigKeys {
    public class ConfigKeyEntity : TableEntity {
        public string Value { get; set; }

        public ConfigKeyEntity() { }

        public ConfigKeyEntity(string subdomain, string key, string value) {
            PartitionKey = subdomain;
            RowKey = key;
            Value = value;
        }
    }
}