﻿using System.Linq;

using Colorware.Core.Subdomain.SubdomainProvider;

using JetBrains.Annotations;

using Microsoft.WindowsAzure.Storage.Table;

namespace Colorware.Core.Azure.Storage.TableStorage.ConfigKeys {
    public static class AzureConfigKeys {
        private const string Tablename = "ServerConfigKeys";
        private const string ServerHostConfigKey = "Options.Server.Host";

        //---------------------------------------------------------------------

        public static string ServerHost {
            [NotNull] get { return GetConfigKey(ServerHostConfigKey, "http://localhost:3000"); }
            [CanBeNull] set { UpdateOrCreateConfigKey(ServerHostConfigKey, value); }
        }

        //---------------------------------------------------------------------

        public static string GetConfigKey(string key, string defaultValue) {
            var customerInfoTable = TableStorageUtils.GetTable(Tablename);

            TableQuery<ConfigKeyEntity> query = new TableQuery<ConfigKeyEntity>().Where(
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal,
                                                       ServerSubdomainProvider.Subdomain),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, ServerHostConfigKey)));

            var results = customerInfoTable.ExecuteQuery(query);
            var returnValue = results.FirstOrDefault();

            if (string.IsNullOrWhiteSpace(returnValue?.Value)) {
                return defaultValue;
            }

            return returnValue.Value;
        }

        //---------------------------------------------------------------------

        public static void UpdateOrCreateConfigKey(string key, string value) {
            var customerInfoTable = TableStorageUtils.GetTable(Tablename);

            ConfigKeyEntity tableEntity =
                new ConfigKeyEntity(ServerSubdomainProvider.Subdomain, key, value);
            TableOperation insertOperation = TableOperation.InsertOrReplace(tableEntity);
            customerInfoTable.Execute(insertOperation);
        }

        //---------------------------------------------------------------------

    }
}