﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Castle.Core.Logging;

using Colorware.Core.Azure.Interfaces;
using Colorware.Core.Subdomain.SubdomainProvider;

namespace Colorware.Core.Azure.Storage.BlobStorage {
    public class BlobLicenseFileStorageHelper : ILicenseFileStorageHelper {
        private readonly string containerName = "licenses";
        private readonly string filename = "license.xml";

        // license file storage - primary storage
        private readonly BlobWrapper blobWrapper = new BlobWrapper(BlobWrapper.StorageType.Primary);

        private readonly ILogger logger;

        public BlobLicenseFileStorageHelper(ILogger logger) {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public List<string> GetCurrentLicenseHolders() {
            List<string> returnValues = new List<string>();
            var blobFiles = blobWrapper.GetBlobFiles(containerName, "");

            foreach (var blobFile in blobFiles) {
                var tokens = blobFile.Uri.AbsolutePath.Split(new[] {'/'}, StringSplitOptions.RemoveEmptyEntries).ToList();
                int containerNameIndex = tokens.IndexOf(containerName);
                System.Diagnostics.Debug.Assert(tokens.Count > containerNameIndex + 1);
                returnValues.Add(tokens[containerNameIndex + 1]);
            }

            var uniqueReturnValues = returnValues.Where((item, index) => returnValues.IndexOf(item) == index).ToList();

            logCurrentLicenseHolders(uniqueReturnValues);

            return uniqueReturnValues;
        }

        public bool LicenseExists() {
            return blobWrapper.BlobFileExists(containerName, $"{ServerSubdomainProvider.Subdomain}/{filename}");
        }

        public async Task<bool> LicenseExistsAsync() {
            return await blobWrapper.BlobFileExistsAsync(containerName,
                                                         $"{ServerSubdomainProvider.Subdomain}/{filename}");
        }

        public async Task SaveLicenseAsync(string content) {
            await blobWrapper.WriteToBlobAsync(containerName, $"{ServerSubdomainProvider.Subdomain}/{filename}",
                                               content);
        }

        public async Task<string> ReadLicenseAsync() {
            return await blobWrapper.ReadBlobAsync(containerName, $"{ServerSubdomainProvider.Subdomain}/{filename}");
        }

        public string ReadLicense(string subdomain) {
            return blobWrapper.ReadBlob(containerName, $"{subdomain}/{filename}");
        }

        public async Task<string> ReadLicenseAsync(string subdomain) {
            return await blobWrapper.ReadBlobAsync(containerName, $"{subdomain}/{filename}");
        }

        public async Task DeleteLicenseAsync() {
            await blobWrapper.DeleteBlobAsync(containerName, $"{ServerSubdomainProvider.Subdomain}/{filename}");
        }

        private void logCurrentLicenseHolders(List<string> uniqueReturnValues)
        {
            logger.Info($"Current license holders:  {String.Join(", ", uniqueReturnValues)}");
        }
    }
}