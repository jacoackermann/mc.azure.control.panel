﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Colorware.Core.Azure.Storage.Accounts;

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Colorware.Core.Azure.Storage.BlobStorage {
    public class BlobWrapper {
        public enum StorageType {
            Primary,
            Secondary
        }

        //---------------------------------------------------------------------

        private readonly StorageType storageType;

        public BlobWrapper(StorageType storageType) {
            this.storageType = storageType;
        }

        //---------------------------------------------------------------------

        public List<IListBlobItem> GetBlobFiles(string containerName, string path) {
            var container = GetContainerRef(containerName);
            List<IListBlobItem> returnValue = container.ListBlobs(prefix: path, useFlatBlobListing: true).ToList();

            return returnValue;
        }

        //---------------------------------------------------------------------

        public bool BlobFileExists(string containerName, string filename) {
            var blockBlob = getCloudBlockBlob(containerName, filename);
            return blockBlob.Exists();
        }

        //---------------------------------------------------------------------

        public async Task<bool> BlobFileExistsAsync(string containerName, string filename) {
            var blockBlob = getCloudBlockBlob(containerName, filename);
            return await blockBlob.ExistsAsync();
        }

        //---------------------------------------------------------------------

        // TODO - will only work for small files, not for massive files...
        //      - need to read up on strategy for handling files of any size, esp if images are to be saved
        //      - to blob!
        public void WriteToBlob(string containerName, string filename, string content) {
            var blockBlob = getCloudBlockBlob(containerName, filename);
            blockBlob.UploadText(content);
        }

        //---------------------------------------------------------------------

        // TODO - will only work for small files, not for massive files...
        //      - need to read up on strategy for handling files of any size, esp if images are to be saved
        //      - to blob!
        public async Task WriteToBlobAsync(string containerName, string filename, string content) {
            var blockBlob = getCloudBlockBlob(containerName, filename);
            await blockBlob.UploadTextAsync(content);
        }

        //---------------------------------------------------------------------

        public string ReadBlob(string containerName, string filename) {
            var blockBlob = getCloudBlockBlob(containerName, filename);
            var retValue = blockBlob.DownloadText();
            return retValue;
        }

        //---------------------------------------------------------------------

        public async Task<string> ReadBlobAsync(string containerName, string filename) {
            var blockBlob = getCloudBlockBlob(containerName, filename);
            var retValue = await blockBlob.DownloadTextAsync();
            return retValue;
        }

        //---------------------------------------------------------------------

        public async Task DeleteBlobAsync(string containerName, string filename) {
            var blockBlob = getCloudBlockBlob(containerName, filename);
            await blockBlob.DeleteIfExistsAsync();
        }

        //---------------------------------------------------------------------

        public CloudBlobContainer GetContainerRef(string containerName, bool createIfNotExist = true) {
            CloudBlobClient blobClient = getStorageAccountManager().CreateCloudBlobClient();
            var container = blobClient.GetContainerReference(containerName);

            if (createIfNotExist && !container.Exists()) {
                try {
                    container.Create();
                }
                catch (Exception e) { }
            }
            return container;
        }

        //---------------------------------------------------------------------

        private CloudBlockBlob getCloudBlockBlob(string containerName, string filename) {
            var container = GetContainerRef(containerName);

            // TODO - need to create 'folders' in the path??
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);
            return blockBlob;
        }

        //---------------------------------------------------------------------

        private CloudStorageAccount getStorageAccountManager() {
            var storageAccManager = new StorageAccountManager();
            return storageType == StorageType.Primary
                       ? storageAccManager.PrimaryStorageAccountOld
                       : storageAccManager.SecondaryStorageAccountOld;
        }
    }
}