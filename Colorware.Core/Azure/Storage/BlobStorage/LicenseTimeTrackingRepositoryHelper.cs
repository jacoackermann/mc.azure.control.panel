﻿using Colorware.Core.Azure.Interfaces;
using Colorware.Core.Subdomain.SubdomainProvider;

namespace Colorware.Core.Azure.Storage.BlobStorage {
    public class LicenseTimeTrackingRepositoryHelper : ILicenseTimeTrackingRepositoryHelper {
        private readonly string filename = "main-settings.txt";

        // license file storage - primary storage
        private readonly BlobWrapper blobWrapper = new BlobWrapper(BlobWrapper.StorageType.Primary);

        public string Read() {
            return blobWrapper.ReadBlob("licenses", $"{ServerSubdomainProvider.Subdomain}/{filename}");
        }

        public void Write(string content) {
            blobWrapper.WriteToBlob("licenses", $"{ServerSubdomainProvider.Subdomain}/{filename}", content);
        }
    }
}