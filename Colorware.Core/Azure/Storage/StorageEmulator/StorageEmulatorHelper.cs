﻿using System.Diagnostics;

namespace Colorware.Core.Azure.Storage.StorageEmulator
{
    public static class StorageEmulatorHelper
    {
        /* Usage:
         * ======
           AzureStorageEmulator.exe init            : Initialize the emulator database and configuration.
           AzureStorageEmulator.exe start           : Start the emulator.
           AzureStorageEmulator.exe stop            : Stop the emulator.
           AzureStorageEmulator.exe status          : Get current emulator status.
           AzureStorageEmulator.exe clear           : Delete all data in the emulator.
           AzureStorageEmulator.exe help [command]  : Show general or command-specific help.
         */
        public enum StorageEmulatorCommand
        {
            Init,
            Start,
            Stop,
            Status,
            Clear
        }

        public static int StartStorageEmulator()
        {
            // simply try and start the emulator... irrespective of whether or not it is running
            return ExecuteStorageEmulatorCommand(StorageEmulatorCommand.Start);
        }

        public static int StopStorageEmulator()
        {
            // simply try and stop the emulator... irrespective of whether or not it is running
            return ExecuteStorageEmulatorCommand(StorageEmulatorCommand.Stop);
        }

        public static int ExecuteStorageEmulatorCommand(StorageEmulatorCommand command)
        {
            var start = new ProcessStartInfo
            {
                Arguments = command.ToString(),
                FileName = @"C:\Program Files (x86)\Microsoft SDKs\Azure\Storage Emulator\AzureStorageEmulator.exe",
                UseShellExecute = false
            };
            var exitCode = executeProcess(start);
            return exitCode;
        }

        private static int executeProcess(ProcessStartInfo startInfo)
        {
            int exitCode = -1;
            try
            {
                using (var proc = new Process { StartInfo = startInfo })
                {
                    proc.Start();
                    proc.WaitForExit();
                    exitCode = proc.ExitCode;
                }
            }
            catch
            {
                //
            }

            return exitCode;
        }
    }
}