﻿using System.Threading.Tasks;

namespace Colorware.Core.Azure.Interfaces {
    public interface IServicePrincipleAccessTokenProvider {
        Task<string> GetResourceManagementAccessToken();
    }
}