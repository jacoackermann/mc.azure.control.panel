﻿using System.Collections.Generic;
using System.Threading.Tasks;

// TODO - subdomain work - this interface shoudld be included somewhere else!!

namespace Colorware.Core.Azure.Interfaces {
    public interface ILicenseFileStorageHelper {
        List<string> GetCurrentLicenseHolders();
        bool LicenseExists();
        Task<bool> LicenseExistsAsync();
        Task SaveLicenseAsync(string content);
        Task<string> ReadLicenseAsync();
        string ReadLicense(string subdomain);
        Task<string> ReadLicenseAsync(string subdomain);
        Task DeleteLicenseAsync();
    }
}