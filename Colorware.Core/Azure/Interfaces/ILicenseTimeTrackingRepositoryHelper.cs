﻿namespace Colorware.Core.Azure.Interfaces {
    public interface ILicenseTimeTrackingRepositoryHelper {
        string Read();
        void Write(string content);
    }
}