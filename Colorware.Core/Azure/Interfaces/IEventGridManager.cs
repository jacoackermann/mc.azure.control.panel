﻿using System.Collections.Generic;
using System.Threading.Tasks;
        
using Colorware.Core.Azure.Messaging.EventGrid.Events;

namespace Colorware.Core.Azure.Interfaces {
    public interface IEventGridManager
    {
        Task<string> Send(List<BaseEvent> events);
    }
}