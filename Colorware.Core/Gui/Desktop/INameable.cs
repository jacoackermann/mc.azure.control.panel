﻿using JetBrains.Annotations;

namespace Colorware.Core.Gui.Desktop {
    /// <summary>
    /// Definition of NAMEABLE
    /// 1 :  worthy of being named [memorable]
    /// 2 :  capable of being named [identifiable]
    /// </summary>
    public interface INameable {
        [NotNull]
        string GetName();
    }
}