using System.Threading.Tasks;

using Colorware.Core.Mvvm;

using JetBrains.Annotations;

using ReactiveUI;

namespace Colorware.Core.Gui.Desktop.Popups {
    public interface IInputOutputPopupViewModel<in TIn, TOut> : IPopup, IViewModel {
        [NotNull]
        ReactiveCommand<object> CancelCommand { get; }

        [NotNull]
        ReactiveCommand<object> OkCommand { get; }

        bool Enabled { get; }

        [ItemNotNull, NotNull]
        Task<TOut> ProduceOutput();

        [NotNull]
        Task Init([NotNull] TIn input);
    }
}