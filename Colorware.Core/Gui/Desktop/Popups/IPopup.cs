﻿namespace Colorware.Core.Gui.Desktop.Popups {
    public interface IPopup {
        void PopupActivated();
        void PopupClosed();
    }
}