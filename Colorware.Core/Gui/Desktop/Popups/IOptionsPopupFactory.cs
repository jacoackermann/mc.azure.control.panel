﻿using Colorware.Core.Functional;

using JetBrains.Annotations;

namespace Colorware.Core.Gui.Desktop.Popups {
    public interface IOptionsPopupFactory<out TOptionsPopup>
        where TOptionsPopup : IInputOutputPopupViewModel<Unit, Unit> {
        [NotNull]
        TOptionsPopup Create();

        // NB: the optionsScreen type here should have been TOptionsPopup but that would
        // ruin the covariance of TOptionsPopup
        void Destroy([NotNull] IInputOutputPopupViewModel<Unit, Unit> optionsScreen);
    }
}