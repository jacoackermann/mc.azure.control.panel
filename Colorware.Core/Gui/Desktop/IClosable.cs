using System.Threading.Tasks;

namespace Colorware.Core.Gui.Desktop {
    public interface IClosable {
        /// <summary>
        /// Should be called when element appears on screen.
        /// </summary>
        Task OpeningAsync();

        /// <summary>
        /// Should be called when element is removed (or hidden) from screen.
        /// </summary>
        /// <param name="isHiding">
        ///     True if element is only hiding and will come back. False if element will
        ///     be completely removed.
        /// </param>
        Task ClosingAsync(bool isHiding);
    }
}