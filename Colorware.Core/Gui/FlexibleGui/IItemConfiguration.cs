﻿using JetBrains.Annotations;

namespace Colorware.Core.Gui.FlexibleGui {
    public interface IItemConfiguration {
        /// <summary>
        /// The id of this item configuration.
        /// </summary>
        string Id { get; }

        /// <summary>
        /// Human readable identifier.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// A fully qualified string path to a component that is to be instanced for this configuration.
        /// Example: "ProcessControlModule.Results.ViewModels.InkZoneOverviewViewModel, ProcessControlModule"
        /// </summary>
        string Component { get; }

        /// <summary>
        /// The position for this item. This is just a string and it is up to the user of the item to determine what
        /// positions will be valid. Note that there is a default position name that will be used when no particular
        /// position is provided in the configuration.
        /// </summary>
        string Position { get; set; }

        /// <summary>
        /// Add a new parameter value to the configuration.
        /// </summary>
        /// <param name="key">The key to hash the parameter under.</param>
        /// <param name="value">The value for the parameter.</param>
        void AddParameter(string key, string value);

        /// <summary>
        /// Checks to see if the parameter with the given key has been defined in the loaded parameter set.
        /// </summary>
        /// <param name="key">The key of the parameter to look for.</param>
        /// <returns><c>true</c> if the parameter exists, <c>false</c> otherwise.</returns>
        bool HasParameter(string key);

        /// <summary>
        /// Create a new instance for this configuration item.
        /// </summary>
        /// <typeparam name="T">The type the new instance should resolve to.</typeparam>
        /// <returns>A new instance to a class pointed to by Component which is of type T.</returns>
        [CanBeNull]
        T LoadItem<T>() where T : class;
    }
}