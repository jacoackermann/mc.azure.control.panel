﻿namespace Colorware.Core.Gui.FlexibleGui {
    public interface IGuiConfigurationLoader {
        /// <summary>
        /// Load a configuration from the given file. Any currently loaded configurations will be removed.
        /// </summary>
        /// <param name="filename">The file to load the configuration from. This should be a valid XML configuration file.</param>
        void Load(string filename);

        /// <summary>
        /// Load a configuration from the given file and merge it into the current configuration set.
        /// </summary>
        /// <param name="filename">The file to load the configuration from. This should be a valid XML configuration file.</param>
        void Merge(string filename);

        /// <summary>
        /// Load ALL configurations in the default directories as defined in the global config.
        /// </summary>
        void LoadDefaults();

        /// <summary>
        /// Return the configuration with the given id or null if no such configuration exists.
        /// </summary>
        /// <param name="configurationId">The id of the configuration to get.</param>
        /// <returns>The configuration with the given id or null if it does not exist.</returns>
        IGuiConfiguration GetConfiguration(string configurationId);
    }
}