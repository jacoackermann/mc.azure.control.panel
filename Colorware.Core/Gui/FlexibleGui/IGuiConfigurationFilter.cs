﻿namespace Colorware.Core.Gui.FlexibleGui {
    /// <summary>
    /// Defines a filter that can be applied to a <see cref="IGuiConfiguration"/> to filter out certain items.
    /// </summary>
    public interface IGuiConfigurationFilter {
        /// <summary>
        /// Determines if the passed in screen object should be allowed or not.
        /// </summary>
        /// <returns>True if allowed, false otherwise.</returns>
        bool IsAllowed(object screen);
    }
}