using System.Collections.Generic;

namespace Colorware.Core.Gui.FlexibleGui {
    public interface IGuiConfiguration {
        /// <summary>
        /// The Id of the configuration.
        /// </summary>
        string Id { get; }

        /// <summary>
        /// A human readable name for the configuration.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// The actual set of configuration items. Each item is indexed by its id.
        /// </summary>
        Dictionary<string, IItemConfiguration> ItemConfigurations { get; }

        /// <summary>
        /// Setup the configuration to use the passed in filter. Only one filter can be active at a time.
        /// </summary>
        void ApplyFilter(IGuiConfigurationFilter filter);

        /// <summary>
        /// Add an item to the configuration. The item will be hashed by its id.
        /// </summary>
        /// <param name="itemConfig">The item to add.</param>
        void AddItemConfiguration(IItemConfiguration itemConfig);

        /// <summary>
        /// Remove an item with the specified id.
        /// </summary>
        /// <param name="id">The id of the item to remove.</param>
        void RemoveItemConfiguration(string id);

        /// <summary>
        /// Filter all items by the items that have the default position and create new instances for them.
        /// </summary>
        /// <typeparam name="T">The type that the items should have.</typeparam>
        /// <returns>A list of item instances.</returns>
        List<T> LoadDefaultItems<T>() where T : class;

        /// <summary>
        /// Create new instances for all items that are set to use a given position.
        /// </summary>
        /// <typeparam name="T">The type the instances should have.</typeparam>
        /// <param name="position">The positions to filter for. Only items for this position will be created.</param>
        /// <returns>A list of item instances of type T for the given position.</returns>
        List<T> LoadItemsForPosition<T>(string position) where T : class;

        /// <summary>
        /// Disposes all loaded items. In the current implementation this means they are released via Windsor.
        /// </summary>
        void DisposeAllLoadedItems();
    }
}