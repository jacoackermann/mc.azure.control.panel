using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.ColorStripMapper;
using Colorware.Core.Data.Models.StripCompareInterface;

namespace Colorware.Core.Models.Image {
    public enum TargetDisplayMode {
        CreationMode,
        MeasureMode,
        ResultMode,
        ResultAverageMode,
        PrintMode,
        UndefinedMode
    }

    public interface IMeasureTarget : INotifyPropertyChanged {
        void GenerateSolidDotgainRelations();
        bool IsUndefined { get; }
        IEnumerable<IStripComparePatch> ResultPatches { get; set; }
        int Index { get; set; }
        bool IsSelected { get; set; }
        bool ShowColorList { get; }
        bool Blink { get; }
        bool ShowDeltaE { get; }
        bool ShowTolerance { get; }
        bool ShowQuestionMark { get; }
        bool IsPotentialTarget { get; set; }
        bool InTolerance { get; }
        Double DeltaE { get; }
        Thickness ColorPickingMargin { get; }
        Thickness ColorPickingButtonMargin { get; }
        Thickness ColorPickingTextMargin { get; }
        bool HasBeenMeasured { get; set; }
        bool IsHitTestVisible { get; set; }
        int OriginalImageWidth { get; set; }
        int OriginalImageHeight { get; set; }
        int X { get; set; }
        int Y { get; set; }
        Double ScreenX { get; set; }
        Double ScreenY { get; set; }
        Double ColorPickingOpacity { get; }
        MappableReferencePatch ReferencePatch { get; set; }
        TargetDisplayMode DisplayMode { get; set; }
        bool IsDragging { get; set; }
        String Name { get; }
        System.Windows.Media.Color AsColor { get; }
        IStripComparePatch StripComparePatch { get; set; }
        ICommand ImageMouseDownCommand { get; }
        ICommand ImageMouseMoveCommand { get; }
        bool ShowDisabledCross { get; }
        double Opacity { get; }
        event EventHandler DragStarted;
        event EventHandler DragEnded;
        event EventHandler MouseDown;
        IJobStripPatch ToJobStripPatch();
        void NotifyImageChanged(BitmapImage image, double imageDisplayWidth, double imageDisplayHeight);
    }
}