﻿using System;

using JetBrains.Annotations;

namespace Colorware.Core.StatusReporting {
    /// <summary>
    /// Reports progress back to the user.
    /// </summary>
    public interface IProgressReporter : IDisposable {
        [NotNull]
        IBusyReport Report { get; set; }

        void Start();

        void Stop();

        bool IsActive { get; }

        bool IsIndeterminate { get; }
    }
}