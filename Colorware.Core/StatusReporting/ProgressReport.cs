using System;

using JetBrains.Annotations;

using ReactiveUI;

namespace Colorware.Core.StatusReporting {
    public class ProgressReport : BusyReport, IProgressReport {
        private int max;

        public int Max {
            get { return max; }
            set { this.RaiseAndSetIfChanged(ref max, value); }
        }

        private int progress;

        public int Progress {
            get { return progress; }
            set { this.RaiseAndSetIfChanged(ref progress, value); }
        }

        private bool reportAsPercentage;

        public bool ReportAsPercentage {
            get { return reportAsPercentage; }
            set { this.RaiseAndSetIfChanged(ref reportAsPercentage, value); }
        }

        public ProgressReport([NotNull] string message) : this(message, 1) {}

        public ProgressReport([NotNull] string message, int max) : this(message, max, TimeSpan.FromSeconds(0.1)) {}

        public ProgressReport([NotNull] string message, int max, TimeSpan activationDelay)
            : this(message, max, activationDelay, false) {}

        public ProgressReport([NotNull] string message, int max, TimeSpan activationDelay, bool reportAsPercentage)
            : base(message, activationDelay) {
            if (message == null) throw new ArgumentNullException(nameof(message));

            ReportAsPercentage = reportAsPercentage;
            Max = max;
        }
    }
}