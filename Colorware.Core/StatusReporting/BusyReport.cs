﻿using System;

using JetBrains.Annotations;

using ReactiveUI;

namespace Colorware.Core.StatusReporting {
    public class BusyReport : ReactiveObject, IBusyReport {
        public static TimeSpan DefaultReportingDelay = TimeSpan.FromMilliseconds(300);

        private string message;

        public string Message {
            get { return message; }
            set { this.RaiseAndSetIfChanged(ref message, value); }
        }

        public TimeSpan ActivationDelay { get; }

        public bool IsSilent { get; }

        public BusyReport(bool isSilent) : this("") {
            IsSilent = isSilent;
        }

        public BusyReport([NotNull] string message) : this(message, DefaultReportingDelay) {}

        public BusyReport([NotNull] string message, TimeSpan activationDelay) : this(message, activationDelay, false) {}

        private BusyReport([NotNull] string message, TimeSpan activationDelay, bool isSilent) {
            if (activationDelay < TimeSpan.Zero)
                throw new ArgumentException("Activation delay must not be a negative time span!",
                                            nameof(activationDelay));

            Message = message;
            ActivationDelay = activationDelay;
            IsSilent = isSilent;
        }
    }
}