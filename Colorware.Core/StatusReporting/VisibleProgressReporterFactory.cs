using System;

using JetBrains.Annotations;

namespace Colorware.Core.StatusReporting {
    [UsedImplicitly]
    public class VisibleProgressReporterFactory : IVisibleProgressReporterFactory {
        public IVisibleProgressReporter Create() {
            return GlobalContainer.Current.Resolve<IVisibleProgressReporter>();
        }

        public void Destroy(IVisibleProgressReporter progressReporter) {
            if (progressReporter == null) throw new ArgumentNullException(nameof(progressReporter));

            // Releasing it through the container will do nothing and it won't call Dispose(), since
            // IVisibleProgressReporter is registered as being 'managedExternally'
            progressReporter.Dispose();
        }
    }
}