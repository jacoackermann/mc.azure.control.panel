using JetBrains.Annotations;

namespace Colorware.Core.StatusReporting {
    public interface IVisibleProgressReporterFactory {
        [NotNull]
        IVisibleProgressReporter Create();

        void Destroy([NotNull] IVisibleProgressReporter progressReporter);
    }
}