﻿namespace Colorware.Core.StatusReporting {
    public interface IProgressReport : IBusyReport {
        int Max { get; set; }

        int Progress { get; set; }

        // TODO
        bool ReportAsPercentage { get; set; }
    }
}