﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using System.Windows.Input;

using Colorware.Core.Extensions;
using Colorware.Core.Mvvm;
using Colorware.Core.StatusReporting.InvisibleReporter;

using JetBrains.Annotations;

namespace Colorware.Core.StatusReporting {
    // Class is re-entrant
    [UsedImplicitly]
    public class GlobalStatusReporter : DefaultNotifyPropertyChanged, IGlobalStatusReporter {
        private readonly IVisibleProgressReporterFactory visibleProgressReporterFactory;
        private readonly IInvisibleProgressReporterFactory invisibleProgressReporterFactory;

        // Using a list instead of a dictionary to be able to determine order of insertion
        [NotNull]
        private IReadOnlyList<KeyValuePair<IBusyReport, IProgressReporter>> progressReporters =
            new List<KeyValuePair<IBusyReport, IProgressReporter>>();

        public GlobalStatusReporter([NotNull] IVisibleProgressReporterFactory visibleProgressReporterFactory,
            [NotNull] IInvisibleProgressReporterFactory invisibleProgressReporterFactory) {
            if (visibleProgressReporterFactory == null)
                throw new ArgumentNullException("visibleProgressReporterFactory");
            if (invisibleProgressReporterFactory == null)
                throw new ArgumentNullException("invisibleProgressReporterFactory");

            this.visibleProgressReporterFactory = visibleProgressReporterFactory;
            this.invisibleProgressReporterFactory = invisibleProgressReporterFactory;

            HasBusyReports = false;
            HasActiveBusyReports = false;

            initStreams();
        }

        private enum ReportAction {
            AddBusyReport,
            AddProgressReport,
            RemoveBusyReport,
            RemoveProgressReport,
            StartReport
        }

        private class ReportEvent {
            private ReportEvent() {
            }

            public static ReportEvent CreateAddBusyReport(IBusyReport busyReport) {
                return new ReportEvent {
                    Action = ReportAction.AddBusyReport,
                    BusyReport = busyReport
                };
            }

            public static ReportEvent CreateAddProgressReport(IProgressReport progressReport) {
                return new ReportEvent {
                    Action = ReportAction.AddProgressReport,
                    ProgressReport = progressReport
                };
            }

            public static ReportEvent CreateRemoveBusyReport(IBusyReport busyReport) {
                return new ReportEvent {
                    Action = ReportAction.RemoveBusyReport,
                    BusyReport = busyReport
                };
            }

            public static ReportEvent CreateRemoveProgressReport(IProgressReport progressReport) {
                return new ReportEvent {
                    Action = ReportAction.RemoveProgressReport,
                    ProgressReport = progressReport
                };
            }

            public static ReportEvent CreateStartReport(IProgressReporter progressReporter, IBusyReport report) {
                return new ReportEvent {
                    Action = ReportAction.StartReport,
                    ProgressReporter = progressReporter,
                    BusyReport = report
                };
            }

            public ReportAction Action { get; private set; }

            public IBusyReport BusyReport { get; private set; }

            public IProgressReport ProgressReport { get; private set; }

            public IProgressReporter ProgressReporter { get; private set; }
        }

        // Must be synchronized because multiple threads might Add/Remove reports in parallel
        private readonly ISubject<ReportEvent, ReportEvent> reportEventStream =
            Subject.Synchronize(new Subject<ReportEvent>());

        private void initStreams() {
            reportEventStream.Subscribe(reportEvent => {
                switch (reportEvent.Action) {
                    case ReportAction.AddBusyReport:
                        addBusyReport(reportEvent.BusyReport);
                        break;
                    case ReportAction.AddProgressReport:
                        addProgressReport(reportEvent.ProgressReport);
                        break;
                    case ReportAction.RemoveBusyReport:
                        removeBusyReport(reportEvent.BusyReport);
                        break;
                    case ReportAction.RemoveProgressReport:
                        removeProgressReport(reportEvent.ProgressReport);
                        break;
                    case ReportAction.StartReport:
                        startReport(reportEvent.ProgressReporter, reportEvent.BusyReport);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            });
        }

        private bool hasBusyReports;

        public bool HasBusyReports {
            get { return hasBusyReports; }
            private set {
                hasBusyReports = value;
                OnPropertyChanged("HasBusyReports");
            }
        }

        private bool hasActiveBusyReports;

        public bool HasActiveBusyReports {
            get { return hasActiveBusyReports; }
            private set {
                hasActiveBusyReports = value;
                OnPropertyChanged("HasActiveBusyReports");
            }
        }

        private string mostRecentStatusMessage;

        public string MostRecentStatusMessage {
            get { return mostRecentStatusMessage; }
            private set {
                mostRecentStatusMessage = value;
                OnPropertyChanged("MostRecentStatusMessage");
            }
        }

        public IBusyReport AddBusyReport([NotNull] IBusyReport busyReport) {
            if (busyReport == null) throw new ArgumentNullException("busyReport");

            reportEventStream.OnNext(ReportEvent.CreateAddBusyReport(busyReport));

            return busyReport;
        }

        private void addBusyReport([NotNull] IBusyReport busyReport) {
            addAndStartReport(busyReport);
        }

        public IProgressReport AddProgressReport([NotNull] IProgressReport progressReport) {
            if (progressReport == null) throw new ArgumentNullException("progressReport");

            reportEventStream.OnNext(ReportEvent.CreateAddProgressReport(progressReport));

            return progressReport;
        }

        private void addProgressReport([NotNull] IProgressReport progressReport) {
            addAndStartReport(progressReport);
        }


        private void addAndStartReport(IBusyReport report) {
            IProgressReporter progressReporter;

            if (report.IsSilent)
                progressReporter = invisibleProgressReporterFactory.Create();
            else
                progressReporter = visibleProgressReporterFactory.Create();

            progressReporter.Report = report;

            addProgressReporter(report, progressReporter);

            Task.Run(() => reportProgressDelayed(progressReporter, report));
        }

        private async Task reportProgressDelayed(IProgressReporter progressReporter, IBusyReport busyReport) {
            await Task.Delay(busyReport.ActivationDelay);

            reportEventStream.OnNext(ReportEvent.CreateStartReport(progressReporter, busyReport));
        }

        private void startReport(IProgressReporter progressReporter, IBusyReport busyReport) {
            // Should only become active if it wasn't removed before the ActivationDelay passed
            if (!progressReporters.Any(pair => pair.Key == busyReport))
                return;

            if (!string.IsNullOrEmpty(busyReport.Message))
                MostRecentStatusMessage = busyReport.Message;

            progressReporter.Start();

            HasActiveBusyReports = true;
        }

        [NotNull]
        private IProgressReporter getProgressReporterForReport([NotNull] IBusyReport report) {
            if (report == null) throw new ArgumentNullException("report");

            if (!progressReporters.Any(pair => pair.Key == report))
                throw new ArgumentException("No progress reporter was found for the specified report!", "report");

            return progressReporters.FirstOrDefault(r => r.Key == report).Value;
        }

        public void RemoveBusyReport([NotNull] IBusyReport report) {
            if (report == null) throw new ArgumentNullException("report");

            reportEventStream.OnNext(ReportEvent.CreateRemoveBusyReport(report));
        }

        private void removeBusyReport([NotNull] IBusyReport report) {
            stopAndRemoveReport(report);
        }

        public void RemoveProgressReport([NotNull] IProgressReport report) {
            if (report == null) throw new ArgumentNullException("report");

            reportEventStream.OnNext(ReportEvent.CreateRemoveProgressReport(report));
        }

        private void removeProgressReport([NotNull] IProgressReport report) {
            stopAndRemoveReport(report);
        }

        private void stopAndRemoveReport([NotNull] IBusyReport report) {
            if (report == null) throw new ArgumentNullException("report");

            var reporter = getProgressReporterForReport(report);

            if (reporter is IInvisibleProgressReporter)
                invisibleProgressReporterFactory.Destroy((IInvisibleProgressReporter)reporter);
            else
                visibleProgressReporterFactory.Destroy((IVisibleProgressReporter)reporter);

            progressReporters = progressReporters.Where(pair => pair.Key != report).ToList();

            var reportWithText = progressReporters.LastOrDefault(pair => !string.IsNullOrEmpty(pair.Key.Message));
            MostRecentStatusMessage = reportWithText.IsDefault() ? string.Empty : reportWithText.Key.Message;

            HasBusyReports = progressReporters.Any();
            HasActiveBusyReports = progressReporters.Any(r => r.Value.IsActive);

            CommandManager.InvalidateRequerySuggested();
        }

        private void addProgressReporter(IBusyReport report, [NotNull] IProgressReporter progressReporter) {
            if (progressReporter == null) throw new ArgumentNullException("progressReporter");

            if (progressReporters.Any(pair => pair.Key == report))
                throw new ArgumentException(
                    "The specified report has already been added to the list of reporters.",
                    "report");

            progressReporters =
                progressReporters.Concat(new KeyValuePair<IBusyReport, IProgressReporter>(report, progressReporter))
                                 .ToList();

            HasBusyReports = true;
        }
    }
}