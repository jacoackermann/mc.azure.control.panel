﻿using JetBrains.Annotations;

namespace Colorware.Core.StatusReporting {
    public interface IGlobalStatusReporter {
        /// <summary>
        /// Indicates whether there are any busy reports or not. Even busy reports that have not been
        /// started yet (because of a start delay) are counted.
        /// </summary>
        bool HasBusyReports { get; }

        /// <summary>
        /// Indicates whether there are any ACTIVE busy reports or not. Only busy reports that have been
        /// started (sometimes after a specified delay) are counted.
        /// </summary>
        bool HasActiveBusyReports { get; }

        /// <summary>
        /// The status (loading) message of the most recent busy report.
        /// </summary>
        string MostRecentStatusMessage { get; }

        /// <summary>
        /// Adds a busy report.
        /// </summary>
        /// <param name="busyReport">The busy report.</param>
        /// <returns>
        /// The supplied busy report, this is just to allow short notation when creating and adding reports.
        /// </returns>
        IBusyReport AddBusyReport([NotNull] IBusyReport busyReport);

        IProgressReport AddProgressReport([NotNull] IProgressReport progressReport);

        /// <summary>
        /// Removes a busy report.
        /// </summary>
        /// <param name="report"></param>
        void RemoveBusyReport([NotNull] IBusyReport report);

        void RemoveProgressReport([NotNull] IProgressReport report);
    }

    public static class IGlobalStatusReporterExtensions {
        public static IBusyReport AddSilentBusyReport(this IGlobalStatusReporter globalStatusReporter) {
            return globalStatusReporter.AddBusyReport(new BusyReport(true));
        }

        /// <summary>
        /// Like the regular methods, but the returned <see cref="BusyReportScope"/> can be used
        /// in a using(..) pattern to automatically close the busy report.
        /// </summary>
        public static BusyReportScope AddBusyReportWithScope(this IGlobalStatusReporter globalStatusReporter,
            IBusyReport busyReport) {
            var report = globalStatusReporter.AddBusyReport(busyReport);
            return new BusyReportScope(globalStatusReporter, report);
        }

        /// <summary>
        /// Like the regular methods, but the returned <see cref="BusyReportScope"/> can be used
        /// in a using(..) pattern to automatically close the busy report.
        /// </summary>
        public static BusyReportScope AddSilentBusyReportWithScope(this IGlobalStatusReporter globalStatusReporter) {
            var report = globalStatusReporter.AddSilentBusyReport();
            return new BusyReportScope(globalStatusReporter, report);
        }
    }
}