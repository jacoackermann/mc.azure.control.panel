﻿using System;
using System.ComponentModel;

using JetBrains.Annotations;

namespace Colorware.Core.StatusReporting {
    public interface IBusyReport : INotifyPropertyChanged {
        /// <summary>
        /// A message describing why we are busy.
        /// </summary>
        [NotNull]
        string Message { get; set; }

        /// <summary>
        /// Time to wait before busy report actually becomes effective. Useful when you
        /// don't want to report busy when an operation only takes a very small amount of time.
        /// 
        /// If the report is removed before this time span has passed (after adding the report),
        /// the report won't become effective.
        /// </summary>
        TimeSpan ActivationDelay { get; }


        /// <summary>
        /// Indicates whether the report should be visible. Set to false if you want to make
        /// the system appear busy, but you don't want this particular report to be visible.
        /// </summary>
        bool IsSilent { get; }
    }
}