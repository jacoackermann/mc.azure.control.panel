﻿using System;

using Colorware.Core.ErrorReporting;

namespace Colorware.Core.StatusReporting.Dummies {
    public class DummyErrorReporter : IErrorReporter {
        public void ReportError(string message, string shortMessage, Exception ex) {
        }
    }
}