﻿using System;
using System.ComponentModel;

namespace Colorware.Core.StatusReporting.Dummies {
    public class DummyProgressReport : IProgressReport {
#pragma warning disable 0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 0067
        public string Message { get; set; }
        public TimeSpan ActivationDelay { get; private set; }
        public bool IsSilent { get; private set; }
        public int Max { get; set; }
        public int Progress { get; set; }
        public bool ReportAsPercentage { get; set; }
    }
}