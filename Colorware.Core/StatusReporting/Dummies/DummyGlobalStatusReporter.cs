﻿namespace Colorware.Core.StatusReporting.Dummies {
    public class DummyGlobalStatusReporter : IGlobalStatusReporter {
        public bool HasBusyReports { get; private set; }

        public bool HasActiveBusyReports { get; private set; }

        public string MostRecentStatusMessage { get; private set; }

        public IBusyReport AddBusyReport(IBusyReport busyReport) {
            return new DummyProgressReport();
        }

        public IProgressReport AddProgressReport(IProgressReport progressReport) {
            return new DummyProgressReport();
        }

        public void RemoveBusyReport(IBusyReport report) {
        }

        public void RemoveProgressReport(IProgressReport report) {
        }
    }
}