﻿using System;
using System.ComponentModel;

using Colorware.Core.Mvvm;

namespace Colorware.Core.StatusReporting {
    public abstract class BaseProgressReporter : DefaultNotifyPropertyChanged, IProgressReporter {
        private IBusyReport report;

        public virtual IBusyReport Report {
            get { return report; }
            set {
                if (value == null) throw new ArgumentNullException("value");

                if (report != null)
                    report.PropertyChanged -= reportOnPropertyChanged;

                report = value;

                value.PropertyChanged += reportOnPropertyChanged;

                updateFromReport();
            }
        }

        private void reportOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            updateFromReport();
        }

        protected abstract void updateFromReport();

        public virtual void Start() {
            IsActive = true;
        }

        public virtual void Stop() {
            IsActive = false;
        }

        private bool isActive;

        public virtual bool IsActive {
            get { return isActive; }
            protected set {
                isActive = value;
                OnPropertyChanged("IsActive");
            }
        }

        public virtual bool IsIndeterminate { get; private set; }

        public virtual void Dispose() {
            Stop();
        }
    }
}