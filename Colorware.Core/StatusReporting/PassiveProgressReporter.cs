using JetBrains.Annotations;

namespace Colorware.Core.StatusReporting {
    // We register this in modern modules as we just want to display the modern progress ring 
    // when there are any SingletonProgressReporters alive (instead of a popup loading message).
    //
    // The DesktopView has a default progress ring sitting on top of all other layers, so only 
    // pop-ups need to define their own rings.
    [UsedImplicitly]
    public class PassiveProgressReporter : BaseProgressReporter, IVisibleProgressReporter {
        protected override void updateFromReport() {
        }
    }
}