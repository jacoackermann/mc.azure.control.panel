﻿using JetBrains.Annotations;

namespace Colorware.Core.StatusReporting.InvisibleReporter {
    public interface IInvisibleProgressReporterFactory {
        [NotNull]
        IInvisibleProgressReporter Create();

        void Destroy([NotNull] IInvisibleProgressReporter progressReporter);
    }
}