﻿namespace Colorware.Core.StatusReporting.InvisibleReporter {
    public interface IInvisibleProgressReporter : IProgressReporter {
    }
}