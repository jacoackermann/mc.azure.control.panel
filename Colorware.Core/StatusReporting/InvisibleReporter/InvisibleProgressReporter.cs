﻿using JetBrains.Annotations;

namespace Colorware.Core.StatusReporting.InvisibleReporter {
    [UsedImplicitly]
    public class InvisibleProgressReporter : BaseProgressReporter, IInvisibleProgressReporter {
        protected override void updateFromReport() {
        }
    }
}