﻿using System;

using JetBrains.Annotations;

namespace Colorware.Core.StatusReporting.InvisibleReporter {
    [UsedImplicitly]
    public class InvisibleProgressReporterFactory : IInvisibleProgressReporterFactory {
        public IInvisibleProgressReporter Create() {
            return GlobalContainer.Current.Resolve<IInvisibleProgressReporter>();
        }

        public void Destroy(IInvisibleProgressReporter progressReporter) {
            if (progressReporter == null) throw new ArgumentNullException(nameof(progressReporter));

            // Releasing it through the container will do nothing and it won't call Dispose(), since
            // IInvisibleProgressReporter is registered as being 'managedExternally'
            progressReporter.Dispose();
        }
    }
}