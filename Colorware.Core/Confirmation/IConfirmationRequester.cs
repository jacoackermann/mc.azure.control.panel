﻿using JetBrains.Annotations;

namespace Colorware.Core.Confirmation {
    public interface IConfirmationRequester {
        [Pure]
        bool Confirm([NotNull] string message, string shortMessage);
    }

    public static class ConfirmationRequesterExtensions {
        [Pure]
        public static bool Confirm(this IConfirmationRequester requester, [NotNull] string message) {
            return requester.Confirm(message, null);
        }
    }
}