﻿namespace Colorware.Core {
    public static class LicenseCustomFields {
        public static class Modules {
            public static class Image {
                public const string MaxPanels = "9b5554e0-613a-4437-8c49-9cc6427c6c9d";
                public const string MaxTargets = "04c3d2eb-036c-47d3-90bd-7c23f246ef07";
                public const int DefaultMaxTargets = 12;
            }

            public static class ProcessControl {
                public const string MaxInks = "a53e9ee2-195e-4adb-b270-7a27076b94a6";
                public const int DefaultMaxInks = 12;
            }
        }
    }
}