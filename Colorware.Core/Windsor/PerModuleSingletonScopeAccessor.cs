﻿using System.Diagnostics;

using Castle.MicroKernel.Context;
using Castle.MicroKernel.Lifestyle.Scoped;

namespace Colorware.Core.Windsor {
    /// An attempt to provide per-module scoped lifestyle. The idea is that when a module is opened it should
    /// notify this class so that all components that are Resolved after EnterModule() is called will be
    /// associated with the currently opened module. They can then be Disposed by Windsor when the module is
    /// closed and ExitModule() is called.
    // https://github.com/castleproject/Windsor/blob/master/docs/implementing-custom-scope.md
    public class PerModuleSingletonScopeAccessor : IScopeAccessor {
        private static ThreadSafeSingletonLifetimeScope scope;

        public static void EnterModule() {
            scope = new ThreadSafeSingletonLifetimeScope();
        }

        public static void ExitModule() {
            Debug.Assert(scope != null);
            scope.Dispose();
            scope = null;
        }

        public ILifetimeScope GetScope(CreationContext context) {
            return scope;
        }

        // Can happen when user quits program before exiting module
        public void Dispose() {
            scope?.Dispose();
        }
    }
}