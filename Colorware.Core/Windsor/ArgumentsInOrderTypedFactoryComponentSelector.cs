﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;

using Castle.Facilities.TypedFactory;
using Castle.MicroKernel;

namespace Colorware.Core.Windsor {
    /// <summary>
    /// Matches passed in arguments to arguments for constructors in-order, ignoring the names. (Matching by name
    /// is the default behaviour of DefaultTypedFactoryComponentSelector)
    /// 
    /// Note that this only works as long as argument types are unique, if multiple arguments have the same type
    /// we just have to match by name to distinguish name (in that case the name must match).
    /// 
    /// This is useful for e.g. IViewFactory<TView, TViewModel>, where we don't want to force VM implementers to
    /// name their ViewModel argument to the View's constructor 'viewModel'.
    /// </summary>
    public class ArgumentsInOrderTypedFactoryComponentSelector : DefaultTypedFactoryComponentSelector {
        // NB: Debugging the Castle Windsor internals suggests the keys in the returned dictionary should be either
        // the name of the parameter that will be resolved, or the type of that parameter. (this is what the
        // DefaultDependencyResolver seems to look for)
        protected override IDictionary GetArguments(MethodInfo method, object[] arguments) {
            if (arguments == null)
                return null;

            var parameters = method.GetParameters();

            if (arguments.Count() != parameters.Count()) {
                throw new ArgumentException(
                    string.Format("Wrong number of arguments passed to factory, expected '{0}', received '{1}'!",
                        parameters.Count(), arguments.Count()), "arguments");
            }

            var args = new Arguments();
            for (int i = 0; i < parameters.Length; i++) {
                // We can only ignore the parameter name if we have no parameter of this type yet
                if (!args.Contains(parameters[i].ParameterType)) {
                    if (!parameters[i].ParameterType.IsInstanceOfType(arguments[i])) {
                        throw new ArgumentException(
                            string.Format("Argument '{0}' of type '{1}' does not match method parameter type '{2}'!", i,
                                arguments[i].GetType().FullName, parameters[i].ParameterType.FullName), "arguments");
                    }

                    args.Add(parameters[i].ParameterType, arguments[i]);
                }
                else {
                    args.Add(parameters[i].Name, arguments[i]);
                }
            }

            return args;
        }
    }
}