﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Castle.Core;
using Castle.MicroKernel;
using Castle.MicroKernel.Lifestyle.Scoped;

namespace Colorware.Core.Windsor {
    public class ThreadSafeTransientLifetimeScope : ILifetimeScope {
        private readonly object @lock = new object();
        private readonly Action<Burden> onAfterCreated;

        private List<Burden> components = new List<Burden>();

        public ThreadSafeTransientLifetimeScope(Action<Burden> onAfterCreated = null) {
            this.onAfterCreated = onAfterCreated ?? delegate { };
        }

        public void Dispose() {
            Debug.Assert(components != null, "Scope was already disposed!");

            lock (@lock) {
                components.ForEach(c => c.Release());
                components = null;
            }
        }

        // We don't actually cache anything, because we provide a Transient-in-module lifetime
        public Burden GetCachedInstance(ComponentModel model, ScopedInstanceActivationCallback createInstance) {
            lock (@lock) {
                Debug.Assert(components != null, "Scope was already disposed!");

                var burden = createInstance(onAfterCreated);
                components.Add(burden);
                return burden;
            }
        }
    }
}