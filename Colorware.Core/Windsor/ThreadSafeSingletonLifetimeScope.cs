﻿using System;
using System.Diagnostics;

using Castle.Core;
using Castle.MicroKernel;
using Castle.MicroKernel.Lifestyle.Scoped;

namespace Colorware.Core.Windsor {
    // Simplified from https://github.com/castleproject/Windsor/blob/master/docs/implementing-custom-scope.md
    public class ThreadSafeSingletonLifetimeScope : ILifetimeScope {
        private readonly object @lock = new object();
        private readonly Action<Burden> onAfterCreated;
        private ScopeCache scopeCache;

        public ThreadSafeSingletonLifetimeScope(Action<Burden> onAfterCreated = null) {
            scopeCache = scopeCache ?? new ScopeCache();
            this.onAfterCreated = onAfterCreated ?? delegate { };
        }

        public void Dispose() {
            Debug.Assert(scopeCache != null, "Scope was already disposed!");

            lock (@lock) {
                if (scopeCache == null)
                    return;

                scopeCache.Dispose();
                scopeCache = null;
            }
        }

        public Burden GetCachedInstance(ComponentModel model, ScopedInstanceActivationCallback createInstance) {
            lock (@lock) {
                Debug.Assert(scopeCache != null, "Scope was already disposed!");

                var burden = scopeCache[model];
                if (burden == null)
                    scopeCache[model] = burden = createInstance(onAfterCreated);

                return burden;
            }
        }
    }
}