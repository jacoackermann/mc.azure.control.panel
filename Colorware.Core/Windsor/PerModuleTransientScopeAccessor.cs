﻿using System.Diagnostics;

using Castle.MicroKernel.Context;
using Castle.MicroKernel.Lifestyle.Scoped;

namespace Colorware.Core.Windsor {
    // NB: this class is only necessary when calling Resolve() ourselves in a module. It exists only
    // because there's a bug in Windsor that doesn't dispose transient dependencies when a
    // child container is disposed: https://github.com/castleproject/Windsor/issues/27
    // As soon as that bug is fixed, this class and ThreadSafeTransientLifetimeScope are obsolete.
    // (their Singleton variants have slightly more use, as you can never manually release a
    // singleton instance, but now we can if we register the component scoped using PerModuleSingletonScopeAccessor)
    public class PerModuleTransientScopeAccessor : IScopeAccessor {
        private static ThreadSafeTransientLifetimeScope scope;

        public static void EnterModule() {
            scope = new ThreadSafeTransientLifetimeScope();
        }

        public static void ExitModule() {
            Debug.Assert(scope != null);
            scope.Dispose();
            scope = null;
        }

        public ILifetimeScope GetScope(CreationContext context) {
            return scope;
        }

        // Can happen when user quits program before exiting module
        public void Dispose() {
            scope?.Dispose();
        }
    }
}