﻿namespace Colorware.Core.Application {
    public struct SoftwareVersion {
        public SoftwareVersion(long major, long minor, long build) {
            Major = major;
            Minor = minor;
            Build = build;
        }

        public long Major { get; }
        public long Minor { get; }
        public long Build { get; }
    }
}