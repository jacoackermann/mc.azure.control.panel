﻿using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.Application {
    /// <summary>
    /// Should be called whenever platform/environment agnostic code wants to exit the application
    /// that is running it.
    /// </summary>
    public interface IApplicationExiter {
        /// <summary>
        /// Exits the current application using the specified parameter. Can be called from any thread.
        /// 
        /// Normal code should only exit the application by calling this function, never by
        /// calling any of the .NET exit functions like Application.ShutDown().
        /// </summary>
        /// <typeparam name="T">The type of the parameter.</typeparam>
        /// <param name="parameter">An optional parameter.</param>
        /// <returns></returns>
        Task Exit<T>([CanBeNull] T parameter);
    }

    public static class ApplicationExiterExtensions {
        public static void Exit(this IApplicationExiter applicationExiter) {
            applicationExiter.Exit<object>(null);
        }
    }
}