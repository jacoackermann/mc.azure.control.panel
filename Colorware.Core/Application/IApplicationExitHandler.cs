﻿using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.Application {
    /// <summary>
    /// Handler interface. All instances will be called when PressView exits.
    /// 
    /// Don't take too long.
    /// </summary>
    public interface IApplicationExitHandler {
        [NotNull]
        Task OnExit();

        /// <summary>
        /// Higher gets executed before lower.
        /// </summary>
        int Priority { get; }
    }
}