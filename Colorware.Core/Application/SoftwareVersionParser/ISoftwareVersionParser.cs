﻿using Colorware.Core.Functional.Option;

namespace Colorware.Core.Application.SoftwareVersionParser {
    /// <summary>
    /// Used to parse a software version string into a SoftwareVersion object
    /// </summary>
    public interface ISoftwareVersionParser {
        /// <summary>
        /// Parsing a string into a SoftwareVersion object
        /// </summary>
        /// <param name="version">a string containing a version seperated by dots (.)</param>
        /// <returns><see cref="SoftwareVersion"/></returns>
        Option<SoftwareVersion> Parse(string version);
    }
}