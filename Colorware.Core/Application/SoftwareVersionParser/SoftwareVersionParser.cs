﻿using Castle.Facilities.TypedFactory.Internal;

using Colorware.Core.Functional.Option;

namespace Colorware.Core.Application.SoftwareVersionParser {
    public class SoftwareVersionParser : ISoftwareVersionParser {
        /// <summary>
        /// Returns a SoftwareVersion if a valid version string was given
        /// </summary>
        /// <param name="version">a version string seperated by dots (.)</param>
        /// <returns>SoftwareVersion, returns <see cref="Empty"/> if given version is invalid.</returns>
        public Option<SoftwareVersion> Parse(string version) {
            if (string.IsNullOrWhiteSpace(version)) {
                return Option<SoftwareVersion>.Nothing;
            }
            var versionSplit = version.Split('.');
            long major, minor = 0, build = 0;
            if (!long.TryParse(versionSplit[0], out major))
                return Option<SoftwareVersion>.Nothing;

            if (versionSplit.Length >= 2)
                long.TryParse(versionSplit[1], out minor);

            if (versionSplit.Length >= 3)
                long.TryParse(versionSplit[2], out build);

            return new SoftwareVersion(major, minor, build).ToOption();
        }
    }
}