﻿namespace Colorware.Core.Application.SoftwareVersionRetriever {
    public interface ISoftwareVersionStringRetriever {
        string GetSoftwareVersionString();
    }
}