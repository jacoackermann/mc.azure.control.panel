﻿using Colorware.Core.Helpers;

namespace Colorware.Core.Application.SoftwareVersionRetriever {
    public class MeasureColorVersionStringRetriever : ISoftwareVersionStringRetriever {
        public string GetSoftwareVersionString() {
            return ApplicationHelper.GetVersion();
        }
    }
}