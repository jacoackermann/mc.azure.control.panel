﻿using System;

using JetBrains.Annotations;

namespace Colorware.Core.Application {
    // Ambient context for now
    public static class ServerVersionProvider {
        [NotNull]
        private static string serverVersion = "?";

        [NotNull]
        public static string ServerVersion {
            get { return serverVersion; }
            set {
                if (value == null) throw new ArgumentNullException("value");

                serverVersion = value;
            }
        }

        static ServerVersionProvider() {
            ResetToDefault();
        }

        public static void ResetToDefault() {
            ServerVersion = "?";
        }
    }
}