﻿using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.Mvvm {
    public interface IViewModel {
        /// <summary>
        /// Await this if you want to make sure the screen is fully initialized before opening it.
        /// 
        /// Newer screens usually allow opening before complete inialization, this seems more responsive
        /// to the user.
        /// 
        /// (Implementing view models should assign their async initialization tasks to this in their
        /// constructor)
        /// </summary>
        [NotNull]
        Task Initialization { get; }
    }
}