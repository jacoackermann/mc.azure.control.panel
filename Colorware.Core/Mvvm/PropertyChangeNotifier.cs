﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;

namespace Colorware.Core.Mvvm {
    // Allows us to watch a property on another dependency object for changes without causing memory leaks
    //
    // http://agsmith.wordpress.com/2008/04/07/propertydescriptor-addvaluechanged-alternative/
    // http://stackoverflow.com/questions/23682232/how-can-i-fix-the-dependencypropertydescriptor-addvaluechanged-memory-leak-on-at
    public sealed class PropertyChangeNotifier : DependencyObject, IDisposable {
        private readonly WeakReference propertySource;

        #region Constructor
        public PropertyChangeNotifier(DependencyObject propertySource, string path)
            : this(propertySource, new PropertyPath(path)) {
        }

        public PropertyChangeNotifier(DependencyObject propertySource, DependencyProperty property)
            : this(propertySource, new PropertyPath(property)) {
        }

        public PropertyChangeNotifier(DependencyObject propertySource, PropertyPath property) {
            if (null == propertySource) throw new ArgumentNullException("propertySource");
            if (null == property) throw new ArgumentNullException("property");

            this.propertySource = new WeakReference(propertySource);
            var binding = new Binding {Path = property, Mode = BindingMode.OneWay, Source = propertySource};
            BindingOperations.SetBinding(this, ValueProperty, binding);
        }
        #endregion

        #region PropertySource
        public DependencyObject PropertySource {
            get {
                try {
                    // Note, it is possible that accessing the target property
                    // will result in an exception so i’ve wrapped this check
                    // in a try catch
                    return propertySource.IsAlive
                        ? propertySource.Target as DependencyObject
                        : null;
                }
                catch {
                    return null;
                }
            }
        }
        #endregion

        #region Value
        /// <summary>
        /// Identifies the <see cref="Value"/> dependency property
        /// </summary>
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value",
            typeof(object), typeof(PropertyChangeNotifier), new FrameworkPropertyMetadata(null, onPropertyChanged));

        private static void onPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var notifier = (PropertyChangeNotifier)d;

            if (null != notifier.ValueChanged)
                notifier.ValueChanged(notifier, EventArgs.Empty);
        }

        /// <summary>
        /// Returns/sets the value of the property
        /// </summary>
        /// <seealso cref="ValueProperty"/>
        [Description("Returns/sets the value of the property")]
        [Category("Behavior")]
        [Bindable(true)]
        public object Value {
            get { return GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
        #endregion //Value

        #region Events
        public event EventHandler ValueChanged;
        #endregion // Events

        #region IDisposable Members
        public void Dispose() {
            BindingOperations.ClearBinding(this, ValueProperty);
        }
        #endregion
    }
}