﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Colorware.Core.Mvvm {
    /// <summary>
    /// Default implementation of INotifyPropertyChanged. Setters should call <c>OnPropertyChanged</c> when
    /// a new value is set to invoke the default behavior. This implementation also has a minor safe guard
    /// which checks if the property being refreshed actually exists on the current class.
    /// 
    /// <remarks>
    /// Currently there are INotifyPropertyChanged implementations in the codebase which do
    /// not use this base implementation. Might want to refactor at some point to rectify this situation.
    /// </remarks>
    /// </summary>
    public class DefaultNotifyPropertyChanged : INotifyPropertyChanged {
        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string name = null) {
            VerifyPropertyName(name);
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Uses reflection to verify that the property actually exists on the class. This serves as a
        /// backup to hopefully catch typos early.
        /// </summary>
        /// <param name="propName"></param>
        public void VerifyPropertyName(string propName) {
#if DEBUG
            if (propName == null)
                throw new ArgumentNullException(nameof(propName));

            // Conventially used to notify change of ALL properties on an object
            if (propName == string.Empty)
                return;

            var prop = GetType().GetProperty(propName,
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            if (prop == null)
                Debug.Fail($"Property '{propName}' is not available for class '{GetType().Name}'");
#endif
        }
        #endregion
    }
}