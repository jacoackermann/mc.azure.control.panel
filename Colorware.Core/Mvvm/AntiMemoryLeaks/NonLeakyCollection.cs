﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

using JetBrains.Annotations;

namespace Colorware.Core.Mvvm.AntiMemoryLeaks {
    /// <summary>
    /// Use to wrap non-INotifyCollectionChanged collections for use with WPF Bindings to prevent
    /// temporary memory leaks (see PV-1609).
    /// </summary>
    public class NonLeakyCollection<T> : IReadOnlyCollection<T>, INotifyCollectionChanged {
        private readonly IReadOnlyCollection<T> collection;

        public NonLeakyCollection([NotNull] IReadOnlyCollection<T> collection) {
            if (collection == null) throw new ArgumentNullException(nameof(collection));
            this.collection = collection;
        }

        public IEnumerator<T> GetEnumerator() {
            return collection.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return ((IEnumerable)collection).GetEnumerator();
        }

        public int Count => collection.Count;

        // Anti memory leak
#pragma warning disable 0067
        public event NotifyCollectionChangedEventHandler CollectionChanged;
#pragma warning restore 0067
    }
}