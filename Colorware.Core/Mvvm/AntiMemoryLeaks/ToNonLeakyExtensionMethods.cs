using System.Collections.Generic;
using System.Linq;

namespace Colorware.Core.Mvvm.AntiMemoryLeaks {
    public static class ToNonLeakyExtensionMethods {
        public static NonLeakyCollection<T> ToNonLeaky<T>(this IReadOnlyCollection<T> collection) {
            return new NonLeakyCollection<T>(collection);
        }

        public static NonLeakyList<T> ToNonLeaky<T>(this IReadOnlyList<T> list) {
            return new NonLeakyList<T>(list);
        }

        public static NonLeakyCollection<T> ToNonLeakyCollection<T>(this IEnumerable<T> source) {
            return new NonLeakyCollection<T>(source.ToList());
        }

        public static NonLeakyList<T> ToNonLeakyList<T>(this IEnumerable<T> source) {
            return new NonLeakyList<T>(source.ToList());
        }
    }
}