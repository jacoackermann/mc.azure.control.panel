using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

using JetBrains.Annotations;

namespace Colorware.Core.Mvvm.AntiMemoryLeaks {
    /// <summary>
    /// Use to wrap non-INotifyCollectionChanged lists for use with WPF Bindings to prevent
    /// temporary memory leaks (see PV-1609).
    /// </summary>
    public class NonLeakyList<T> : IReadOnlyList<T>, INotifyCollectionChanged {
        private readonly IReadOnlyList<T> list;

        public NonLeakyList([NotNull] IReadOnlyList<T> list) {
            if (list == null) throw new ArgumentNullException(nameof(list));
            this.list = list;
        }

        public IEnumerator<T> GetEnumerator() {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return ((IEnumerable)list).GetEnumerator();
        }

        public int Count => list.Count;

        public T this[int index] => list[index];

        // Anti memory leak
#pragma warning disable 0067
        public event NotifyCollectionChangedEventHandler CollectionChanged;
#pragma warning restore 0067
    }
}