﻿using System;

using Colorware.Core.Functional.Option;

namespace Colorware.Core.PrintQualityReport {

    /// <summary>
    /// Defines a Print Quality Provider identifier. These are used in various places to indentify
    /// which provider to use. This is basically a Guid, but wrapped into a custom struct to make it
    /// more domain-specific.
    ///
    /// Wrapping it like this provides additional functionality and makes it easier to search the
    /// code base for uses of the identifier.
    /// </summary>
    public struct PqrProviderGuid {

        /// <summary>
        /// The actual wrapped Guid.
        /// </summary>
        public Guid Guid { get; }

        private PqrProviderGuid(Guid guid) {
            Guid = guid;
        }

        /// <summary>
        /// Tries to safely parse a guid string representation.
        /// </summary>
        /// <param name="guidStr">The string to try and parse.</param>
        /// <returns>A <see cref="PqrProviderGuid"/> instance option.</returns>
        public static Option<PqrProviderGuid> TryParse(string guidStr) {
            Guid guid;
            if (Guid.TryParse(guidStr, out guid)) {
                return new PqrProviderGuid(guid).ToOption();
            }
            return Option<PqrProviderGuid>.Nothing;
        }

        /// <summary>
        /// Create a new instane from a given guid.
        /// </summary>
        /// <param name="guid">The guid to use.</param>
        /// <returns>The guid wrapped in a <see cref="PqrProviderGuid"/> instance.</returns>
        public static PqrProviderGuid FromGuid(Guid guid) {
            return new PqrProviderGuid(guid);
        }

        #region Housekeeping for equality

        public override bool Equals(object obj) {
            return obj is PqrProviderGuid && Equals((PqrProviderGuid)obj);
        }

        public bool Equals(PqrProviderGuid obj) {
            return obj.Guid.Equals(Guid);
        }

        public static bool operator ==(PqrProviderGuid guid1, PqrProviderGuid guid2) {
            return guid1.Equals(guid2);
        }

        public static bool operator !=(PqrProviderGuid guid1, PqrProviderGuid guid2) {
            return !guid1.Equals(guid2);
        }

        public override int GetHashCode() {
            return Guid.GetHashCode();
        }

        #endregion Housekeeping for equality
    }
}