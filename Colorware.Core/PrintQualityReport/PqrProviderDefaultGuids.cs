﻿using System;

namespace Colorware.Core.PrintQualityReport {

    /// <summary>
    /// Single point to keep the guids for the providers in the system. These guids are reused in a
    /// couple of places. This may not be the proper place for them tehchnically, but they were
    /// spread across multiple module and have been consolidated here for now.
    /// </summary>
    public static class PqrProviderDefaultGuids {

        /// <summary>
        /// Guid to identify reports provider.
        /// </summary>
        public static PqrProviderGuid ColorwareReportsGuid =>
            PqrProviderGuid.FromGuid(Guid.Parse("1926867E-CC89-493C-BFC2-3BC6EBBF1ECD"));

        /// <summary>
        /// Guid to identify PhotoType provider.
        /// </summary>
        public static PqrProviderGuid PhotoTypeGuid =>
            PqrProviderGuid.FromGuid(Guid.Parse("DA915BE1-CE52-4F85-BC5B-39499F497AD2"));
    }
}