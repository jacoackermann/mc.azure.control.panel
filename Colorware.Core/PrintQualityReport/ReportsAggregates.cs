﻿namespace Colorware.Core.PrintQualityReport {
    public static class ReportsAggregates {
        public const string Measurement = "Measurement";
        public const string Job = "Job";
        public const string JobStrip = "JobStrip";
        public const string MeasurementCondition = " MeasurementCondition";
        public const string Paper = "Paper";
        public const string Client = "Client";
        public const string ToleranceSet = "ToleranceSet";
        public const string Machine = "Machine";
        public const string Operator = "Operator";
        public const string InkSet = "InkSet";
        public const string Company = "Company";
        public const string MeasurementType = "MeasurementType";
        public const string MachineType = "MachineType";
        public const string Printingcondition = "PrintingCondition";
    }
}