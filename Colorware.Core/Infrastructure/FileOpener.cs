﻿using System;
using System.IO;

using JetBrains.Annotations;

namespace Colorware.Core.Infrastructure {
    /// <inheritdoc />
    public class FileOpener : IFileOpener {
        /// <inheritdoc />
        public Stream OpenFile([NotNull] string path, FileOpenMode mode) {
            if (path == null) throw new ArgumentNullException(nameof(path));
            switch (mode) {
                case FileOpenMode.ReadFromFile:
                    return File.OpenRead(path);
                case FileOpenMode.WriteToFile:
                    return File.OpenWrite(path);
                default:
                    throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
            }
        }
    }
}