﻿using System.IO;

namespace Colorware.Core.Infrastructure {
    public enum FileOpenMode {
        ReadFromFile,
        WriteToFile
    }

    /// <inheritdoc cref="OpenFile"/>
    public interface IFileOpener {
        /// <summary>
        /// Wrapper around <see cref="File.OpenRead"/> and <see cref="File.OpenWrite"/>. This
        /// interface exists to pull out the act of opening a file to allow for easier testing.
        /// </summary>
        Stream OpenFile(string path, FileOpenMode mode);
    }
}