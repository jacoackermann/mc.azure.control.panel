﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Web;

using Colorware.Core.Helpers;
using Colorware.Core.License;
using Colorware.Core.Plugins.Devices;

using JetBrains.Annotations;

namespace Colorware.Core.ExceptionReporting {
    public class JiraExceptionReportGenerator {
        [CanBeNull]
        private readonly LicenseRightsChecker licenseChecker;

        public JiraExceptionReportGenerator() {
            try {
                licenseChecker = GlobalContainer.Current.Resolve<LicenseRightsChecker>();
            }
                // Sometimes there is no container left to use when we crash
            catch {
            }
        }

        public NameValueCollection GenerateReport([CanBeNull] IDeviceManager deviceManager, string exceptionDetails,
            string name, string company, string email, string description) {
            var data = new NameValueCollection();
            data["Exception"] = HttpUtility.HtmlEncode(exceptionDetails.Replace("\n", "\n\\"));
            data["Application"] = ApplicationHelper.ApplicationName;
            data["Version"] = ApplicationHelper.GetVersionVerbose();
            data["ClientOS"] = SystemInfoHelper.GetOperatingSystem();
            data["ClientSystemInfo"] = HttpUtility.HtmlEncode(DiagnosticsHelper.GetJiraHardwareSummary());
            data["ServerSystemInfo"] = HttpUtility.HtmlEncode(DiagnosticsHelper.ServerJiraSummary);
            data["DeviceInfo"] = DiagnosticsHelper.GetDeviceInfoJira(deviceManager);

            try {
                if (licenseChecker != null) {
                    data["LicenseSerial"] = licenseChecker.LicenseKey;
                    data["LicenseExpireDate"] = licenseChecker.LicenseValidTill.ToString(CultureInfo.InvariantCulture);
                    data["LicenseIsExpired"] = licenseChecker.LicenseExpired.ToString();
                    data["LicenseIsDemo"] = licenseChecker.LicenseIsDemo.ToString();
                }
            }
            catch (Exception) {
            }

            // User supplied
            data["Name"] = HttpUtility.HtmlEncode(name);
            data["Company"] = HttpUtility.HtmlEncode(company);
            data["Email"] = HttpUtility.HtmlEncode(email);
            data["Description"] = HttpUtility.HtmlEncode(description);

            return data;
        }
    }
}