﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

using Colorware.Core.Config;
using Colorware.Core.Helpers;
using Colorware.Core.Logging;

using JetBrains.Annotations;

namespace Colorware.Core.ExceptionReporting {
    public class PlainTextExceptionReportGenerator {
        [NotNull, Pure]
        public string GenerateReport(Exception exception) {
            var sb = new StringBuilder();

            sb.AppendLine(String.Format("Timestamp: {0}\n", DateTime.Now.ToString(CultureInfo.InvariantCulture)));

            sb.AppendLine(exception.ToString());

            addFirstChanceInfo(sb);

            addLogInfo(sb);
            return sb.ToString();
        }

        private void addFirstChanceInfo(StringBuilder sb) {
            try {
                sb.AppendLine();
                sb.AppendLine("========== Start of first-chance exception info ==========");

                var firstChanceExceptions =
                    FirstChanceExceptionLogger.GetFirstChanceExceptionsInfo().OrderByDescending(e => e.Timestamp);
                var numExceptions = firstChanceExceptions.Count();

                var exceptionTexts =
                    firstChanceExceptions.Select(
                        (exceptionInfo, index) => formatFirstChanceExceptionInfo(exceptionInfo, numExceptions, index));

                sb.AppendLine(string.Concat(exceptionTexts));

                sb.AppendLine("=========== End of first-chance exception info ===========\n\n");
            }
            catch (Exception e) {
                sb.AppendLine("Exception while getting first-chance exception information");
                sb.AppendLine(e.ToString());
            }
        }

        private string formatFirstChanceExceptionInfo(FirstChanceExceptionInfo info, int numExceptions, int index) {
            return "\n" +
                   "******************************************************\n" +
                   string.Format("Exception {0}/{1} @ {2:yyyy-MM-dd HH:mm:ss.fff}\n", numExceptions - index,
                       numExceptions, info.Timestamp) +
                   "******************************************************\n" +
                   info.Exception + "\n\n" +
                   info.StackTrace + "\n\n";
        }

        private void addLogInfo(StringBuilder sb) {
            // String logPath = Directory.GetCurrentDirectory() + @"\\log.txt";
            var logPath = getLogPath();
            try {
                LogManager.Flush();

                if (!File.Exists(logPath)) {
                    sb.AppendLine("No log exists at: " + logPath);
                    return;
                }

                sb.AppendLine("========== Start of log ==========");
                sb.AppendLine(readSessionLog(logPath, 1000));
                sb.AppendLine("=========== End of log ===========");
            }
            catch (Exception e) {
                sb.AppendLine("Exception while getting log information");
                sb.AppendLine(e.ToString());
            }
        }

        private static String getLogPath() {
            var dataPath = GlobalConfig.GetDataPath("log.txt");
            if (File.Exists(dataPath))
                return dataPath;
            return Directory.GetCurrentDirectory() + @"\\log.txt";
        }

        private string readSessionLog(string path, int maxLines = 500) {
            var reverseReader = new ReverseLineReader(path);

            IEnumerable<string> lines = reverseReader;
            if (maxLines >= 0)
                lines = lines.Take(maxLines);

            return string.Join("\n", lines.TakeWhile(l => !l.EndsWith("*** PROGRAM START ***|")).Reverse());
        }
    }
}