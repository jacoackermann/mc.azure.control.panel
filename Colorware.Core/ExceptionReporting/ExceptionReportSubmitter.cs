﻿using System.Collections.Specialized;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Colorware.Core.ExceptionReporting {
    public class ExceptionReportSubmitter {
        public async Task<bool> SubmitExceptionAsync(NameValueCollection reportData) {
            using (var wc = new WebClient()) {
                var responseBytes =
                    await
                        wc.UploadValuesTaskAsync(@"http://www.colorware.eu/exception_sink.php", "POST", reportData)
                          .ConfigureAwait(false);
                var responseString = Encoding.UTF8.GetString(responseBytes);
                return responseString.Equals("OK");
            }
        }
    }
}