using System;

using JetBrains.Annotations;

namespace Colorware.Core.Input {
    public class InputValueRequest : IInputValueRequest {
        public string Name { get; private set; }
        public Type Type { get; private set; }
        public string Description { get; private set; }
        public object DefaultValue { get; private set; }
        public bool IsSecret { get; set; }
        public bool IsOptional { get; set; }
        public int EstimatedInputLength { get; set; }
        public bool IsMultiline { get; set; }
        public bool AutoAcceptIfBarcode { get; set; }

        public InputValueRequest([NotNull] string name, [NotNull] Type type, string description, object defaultValue) {
            if (name == null) throw new ArgumentNullException("name");
            if (type == null) throw new ArgumentNullException("type");

            Name = name;
            Type = type;
            Description = description;
            DefaultValue = defaultValue;
        }

        public InputValueRequest(string name, Type type) : this(name, type, null, null) {
        }

        public InputValueRequest(string name, string description, Type type) : this(name, type, description, null) {
        }

        public InputValueRequest(string name, Type type, object defaultValue) : this(name, type, null, defaultValue) {
        }
    }
}