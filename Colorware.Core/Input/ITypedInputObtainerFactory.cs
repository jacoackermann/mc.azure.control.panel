﻿namespace Colorware.Core.Input {
    public interface ITypedInputObtainerFactory {
        ITypedInputObtainer<T1> Create<T1>();
        ITypedInputObtainer<T1, T2> Create<T1, T2>();
        ITypedInputObtainer<T1, T2, T3> Create<T1, T2, T3>();
        ITypedInputObtainer<T1, T2, T3, T4> Create<T1, T2, T3, T4>();

        void Destroy<T1>(ITypedInputObtainer<T1> typedInputObtainer);
        void Destroy<T1, T2>(ITypedInputObtainer<T1, T2> typedInputObtainer);
        void Destroy<T1, T2, T3>(ITypedInputObtainer<T1, T2, T3> typedInputObtainer);
        void Destroy<T1, T2, T3, T4>(ITypedInputObtainer<T1, T2, T3, T4> typedInputObtainer);
    }
}