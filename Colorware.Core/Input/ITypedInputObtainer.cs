using System;
using System.Collections.Generic;

using JetBrains.Annotations;

namespace Colorware.Core.Input {
    //    Sample usage:
    //            var inputObtainer = typedInputObtainerFactory.Create<string, string>();
    //            var creds = inputObtainer.GetInput("Please enter your PantoneLIVE credentials",
    //                                      "PantoneLIVE account credentials",
    //                                      new TypedInputValueRequest<string>("Username", credentials.Username),
    //                                      new TypedInputValueRequest<string>("Password", credentials.Password) {
    //                                      IsSecret = true
    //                                     });
    //           typedInputObtainerFactory.Destroy(inputObtainer);
    public interface ITypedInputObtainer<T1> {
        [CanBeNull]
        Tuple<KeyValuePair<string, T1>> GetInput(ITypedInputValueRequest<T1> request);

        [CanBeNull]
        Tuple<KeyValuePair<string, T1>> GetInput(string description, ITypedInputValueRequest<T1> request);

        [CanBeNull]
        Tuple<KeyValuePair<string, T1>> GetInput(string description, string shortDescription,
            ITypedInputValueRequest<T1> request);
    }

    public interface ITypedInputObtainer<T1, T2> {
        [CanBeNull]
        Tuple<KeyValuePair<string, T1>, KeyValuePair<string, T2>> GetInput(ITypedInputValueRequest<T1> request1,
            ITypedInputValueRequest<T2> request2);

        [CanBeNull]
        Tuple<KeyValuePair<string, T1>, KeyValuePair<string, T2>> GetInput(string description,
            ITypedInputValueRequest<T1> request1, ITypedInputValueRequest<T2> request2);

        [CanBeNull]
        Tuple<KeyValuePair<string, T1>, KeyValuePair<string, T2>> GetInput(string description, string shortDescription,
            ITypedInputValueRequest<T1> request1, ITypedInputValueRequest<T2> request2);
    }

    public interface ITypedInputObtainer<T1, T2, T3> {
        [CanBeNull]
        Tuple<KeyValuePair<string, T1>, KeyValuePair<string, T2>, KeyValuePair<string, T3>> GetInput(
            ITypedInputValueRequest<T1> request1, ITypedInputValueRequest<T2> request2,
            ITypedInputValueRequest<T3> request3);

        [CanBeNull]
        Tuple<KeyValuePair<string, T1>, KeyValuePair<string, T2>, KeyValuePair<string, T3>> GetInput(string description,
            ITypedInputValueRequest<T1> request1, ITypedInputValueRequest<T2> request2,
            ITypedInputValueRequest<T3> request3);

        [CanBeNull]
        Tuple<KeyValuePair<string, T1>, KeyValuePair<string, T2>, KeyValuePair<string, T3>> GetInput(string description,
            string shortDescription, ITypedInputValueRequest<T1> request1, ITypedInputValueRequest<T2> request2,
            ITypedInputValueRequest<T3> request3);
    }

    public interface ITypedInputObtainer<T1, T2, T3, T4> {
        [CanBeNull]
        Tuple<KeyValuePair<string, T1>, KeyValuePair<string, T2>, KeyValuePair<string, T3>, KeyValuePair<string, T4>>
        GetInput(ITypedInputValueRequest<T1> request1, ITypedInputValueRequest<T2> request2,
            ITypedInputValueRequest<T3> request3, ITypedInputValueRequest<T4> request4);

        [CanBeNull]
        Tuple<KeyValuePair<string, T1>, KeyValuePair<string, T2>, KeyValuePair<string, T3>, KeyValuePair<string, T4>>
        GetInput(string description, ITypedInputValueRequest<T1> request1, ITypedInputValueRequest<T2> request2,
            ITypedInputValueRequest<T3> request3, ITypedInputValueRequest<T4> request4);

        [CanBeNull]
        Tuple<KeyValuePair<string, T1>, KeyValuePair<string, T2>, KeyValuePair<string, T3>, KeyValuePair<string, T4>>
        GetInput(string description, string shortDescription, ITypedInputValueRequest<T1> request1,
            ITypedInputValueRequest<T2> request2, ITypedInputValueRequest<T3> request3,
            ITypedInputValueRequest<T4> request4);
    }
}