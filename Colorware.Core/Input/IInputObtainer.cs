﻿using System.Collections.Generic;

using JetBrains.Annotations;

namespace Colorware.Core.Input {
    //    Sample usage:
    //            var creds =
    //                inputObtainer.GetInput(new List<IInputValueRequest> {
    //                    new InputValueRequest("Username", typeof(string), credentials.Username),
    //                    new InputValueRequest("Password", typeof(string), credentials.Password)
    //                }, "Please enter your PantoneLIVE credentials", "PantoneLIVE account credentials");
    //
    //            credentials = new PantoneLiveCredentials(creds["Username"] as string, creds["Password"] as string);
    public interface IInputObtainer {
        [CanBeNull]
        Dictionary<string, object> GetInput(IEnumerable<IInputValueRequest> inputRequests);

        [CanBeNull]
        Dictionary<string, object> GetInput(IEnumerable<IInputValueRequest> inputRequests, string description);

        [CanBeNull]
        Dictionary<string, object> GetInput(IEnumerable<IInputValueRequest> inputRequests, string description,
            string shortDescription);
    }
}