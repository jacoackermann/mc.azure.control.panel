using System;

using JetBrains.Annotations;

namespace Colorware.Core.Input {
    public class TypedInputValueRequest<T> : ITypedInputValueRequest<T> {
        [NotNull]
        public string Name { get; private set; }

        public string Description { get; private set; }

        public T DefaultValue { get; private set; }

        public bool IsSecret { get; set; }

        public bool IsOptional { get; set; }

        // Used as a hint when != 0
        public int EstimatedInputLength { get; set; }

        public bool IsMultiline { get; set; }

        public bool AutoAcceptIfBarcode { get; set; }

        public TypedInputValueRequest([NotNull] string name, string description, T defaultValue) {
            if (name == null) throw new ArgumentNullException("name");

            Name = name;
            Description = description;
            DefaultValue = defaultValue;
        }

        public TypedInputValueRequest([NotNull] string name) : this(name, null, default(T)) {
        }

        public TypedInputValueRequest([NotNull] string name, T defaultValue) : this(name, null, defaultValue) {
        }
    }
}