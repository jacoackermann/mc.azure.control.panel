using JetBrains.Annotations;

namespace Colorware.Core.Input {
    public interface ITypedInputValueRequest<out T> {
        [NotNull]
        string Name { get; }

        string Description { get; }

        T DefaultValue { get; }

        // E.g. for passwords
        bool IsSecret { get; set; }

        bool IsOptional { get; set; }

        // Used as a hint when != 0
        int EstimatedInputLength { get; set; }

        bool IsMultiline { get; set; }

        bool AutoAcceptIfBarcode { get; set; }
    }
}