using System;

using JetBrains.Annotations;

namespace Colorware.Core.Input {
    // TODO (backlog ideas): add support for: min/max length
    public interface IInputValueRequest {
        [NotNull]
        Type Type { get; }

        [NotNull]
        string Name { get; }

        string Description { get; }

        object DefaultValue { get; }

        // E.g. for passwords
        bool IsSecret { get; set; }

        bool IsOptional { get; set; }

        // Used as a hint when != 0
        int EstimatedInputLength { get; set; }

        bool IsMultiline { get; set; }

        bool AutoAcceptIfBarcode { get; set; }
    }
}