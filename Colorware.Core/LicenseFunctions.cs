﻿namespace Colorware.Core {
    public static class LicenseFunctions {
        public static class Core {
            public const string Login = "3b27d40a-9741-468d-bfbf-48d5c78bcfa6";
            public const string ImportMeasurements = "0eda4b0d-2db6-48b5-bf3e-e71ffd77cde2";
        }

        public static class Modules {
            public static class Flexibles {
                public const string Access = "24e63ea4-b5b0-45bc-94ee-8a01d1e60bf6";
            }

            public static class Epq {
                public const string Access = "959882c5-105f-4cb0-a3a0-cc151d5a262f";
            }
        }

        public static class Exporters {
            public const string AgfaApogee = "0f836cd4-5dd7-4dce-b571-90f444f7e27c";
            public const string AveragedDotgainCtp = "4354d2b1-aabd-4164-bcfd-5b781f418c47";
            public const string CalderaRip = "cd5ffff8-ced3-499b-9208-2023e3d25e5b";
            public const string CMYKOptimizer = "e0fe3397-195d-4ef4-92e0-da803a7fe794";
            public const string ColorCertSvf = "41589951-0a29-4155-be47-d9f3a2573855";
            public const string CSV = "6aa13215-9035-4622-8ce5-9786c91689d0";
            public const string Cxf = "6aa13215-9035-4622-8ce5-9786c91689d0"; // NOTE/TODO/WARNING/XXX: SAME AS CSV FOR NOW!
            public const string CGats = "67c0878d-cb13-4d5c-91d0-7e1c9e7466e3";
            public const string DotgainCtp = "697c3c5b-e7b5-45e0-bca2-37dde44389e0";
            public const string FujiXmf = "024d5402941f4513bf327d9e0a7029f4";
            public const string GMGColorServer = "75ac87cb-65c1-43e5-ae52-eda1d39410c2";
            public const string Harmony = "1ea114c3-4f9b-42e3-be8c-203de0033dda";
            public const string ITXSvf = "41589951-0a29-4155-be47-d9f3a2573855";
            public const string MultiSpotSequenceCsvExporter = "7ef336e5-1847-4e61-8e8b-8e723411eaf6";
            // NB: ScreenTrueflow is currently not used, but might come back later when we know the format
            public const string ScreenTrueflow = "42c982a4-5f9c-46f5-8eea-e260ec8045f3";
            public const string Shinohara = "31d8f8ed-442f-4144-a391-5abeaa858104";
            public const string SimpleJmf = "e1c6f411-7800-495c-896e-a8bb33f432b3";
            public const string SpotOn = "e7654277-058b-458c-8c48-9d59ab0e6cca";
            public const string TabDelimitedCtp = "52caba6e-5318-4798-b1f4-95b84e570252";
            public const string InkFormulationCxF3 = "aecb1d9a-70c8-471a-859c-a95a23297ca2";
        }

        public static class PantoneLive {
            public const string Use = "8e4ea853-8034-4371-9fb3-bbe3f9c7038b";
        }
    }
}