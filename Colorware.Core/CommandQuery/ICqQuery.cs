﻿namespace Colorware.Core.CommandQuery {

    /// <summary>
    /// Queries need to implement this marker interface to designate the return type the query
    /// handler will produce. This narrows the possible use cases per query, but should make
    /// discoverability easier while making improper usages less likely by leveraging the type system.
    ///
    /// Note that the combination of implementation and type will determine both the input and output
    /// for the query.
    /// </summary>
    /// <typeparam name="TResult">The result the query will return.</typeparam>
    public interface ICqQuery<TResult> {}
}