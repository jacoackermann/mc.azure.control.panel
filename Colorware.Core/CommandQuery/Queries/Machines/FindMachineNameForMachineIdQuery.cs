﻿namespace Colorware.Core.CommandQuery.Queries.Machines {
    public class FindMachineNameForMachineIdQuery : ICqQuery<string> {
        public long MachineId { get; set; }
        public FindMachineNameForMachineIdQuery(long machineId) {
            MachineId = machineId;
        }
    }
}