﻿using Colorware.Core.Data.Models;

namespace Colorware.Core.CommandQuery.Queries.JobPresets {

    public class FindJobPresetQuery : ICqQuery<IJobPreset> {

        public FindJobPresetQuery(long jobPresetId) {
            JobPresetId = jobPresetId;
        }

        public long JobPresetId { get; }
    }
}