﻿using System.Collections.Generic;

using Colorware.Core.Data.Models;

namespace Colorware.Core.CommandQuery.Queries.JobPresets {
    /// <summary>
    /// Returns all defined presets for a given machine.
    /// </summary>
    public class FindJobPresetsForMachineQuery : ICqQuery<IEnumerable<IJobPreset>>  {
        public FindJobPresetsForMachineQuery(long machineId) {
            MachineId = machineId;
        }
        public long MachineId { get; }
    }
}