﻿using Colorware.Core.Data.Models;
using Colorware.Core.Functional.Option;

namespace Colorware.Core.CommandQuery.Queries.PhotoType {

    /// <summary>
    /// Find an existing phototype entry for a given job id an measurement id if it exists.
    /// </summary>
    public class FindExistingPhototTypeEntryQuery : ICqQuery<Option<IPhotoTypeEntry>> {

        public FindExistingPhototTypeEntryQuery(long jobId, long measurementId) {
            JobId = jobId;
            MeasurementId = measurementId;
        }

        public long JobId { get; private set; }
        public long MeasurementId { get; private set; }
    }
}