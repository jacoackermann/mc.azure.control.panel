﻿using System;

using Colorware.Core.Data.Models;

using JetBrains.Annotations;

namespace Colorware.Core.CommandQuery.Queries.Jobs {
    /// <summary>
    /// Retrieves the printing location name for a given job.
    /// </summary>
    public class FindPrintingLocationNameForJobQuery : ICqQuery<string> {
        public FindPrintingLocationNameForJobQuery([NotNull] IJob forJob)
        {
            ForJob = forJob;
            if (forJob == null) throw new ArgumentNullException(nameof(forJob));
        }

        [NotNull]
        public IJob ForJob { get; }
    }
}