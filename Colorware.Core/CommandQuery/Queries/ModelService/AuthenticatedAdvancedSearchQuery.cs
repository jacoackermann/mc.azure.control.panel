﻿using System.Collections.Generic;

using Colorware.Core.Data.Specification;
using Colorware.Core.Domain.Security;

namespace Colorware.Core.CommandQuery.Queries.ModelService {

    /// <summary>
    /// Query to perform an AdvancedSearch taking the authentication parameters into account.
    /// </summary>
    public class AuthenticatedAdvancedSearchQuery : ICqQuery<IReadOnlyList<IBaseModel>> {

        public AuthenticatedAdvancedSearchQuery(SimpleAuthentication authentication, string modelType, string xml) {
            Authentication = authentication;
            ModelType = modelType;
            Xml = xml;
        }

        public SimpleAuthentication Authentication { get; }
        public string ModelType { get; }
        public string Xml { get; }
    }
}