﻿using System;
using System.Collections.Generic;

using Colorware.Core.Data;
using Colorware.Core.Data.Specification;

using JetBrains.Annotations;

namespace Colorware.Core.CommandQuery.Queries.ModelService {
    /// <summary>
    /// Perform a find-all query, using the supplied authentication parameters for access restrictions.
    /// </summary>
    public class FindAllQuery : ICqQuery<IReadOnlyList<IBaseModel>> {
        public FindAllQuery([NotNull] string modelType, [CanBeNull] Query query) {
            ModelType = modelType ?? throw new ArgumentNullException(nameof(modelType));
            Query = query;
        }

        [NotNull]
        public string ModelType { get; }

        [CanBeNull]
        public Query Query { get; }
    }
}