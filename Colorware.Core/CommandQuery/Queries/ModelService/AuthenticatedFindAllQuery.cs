﻿using System.Collections.Generic;

using Colorware.Core.Data;
using Colorware.Core.Data.Specification;
using Colorware.Core.Domain.Security;

namespace Colorware.Core.CommandQuery.Queries.ModelService {

    /// <summary>
    /// Perform a find-all query, using the supplied authentication parameters for access restrictions.
    /// </summary>
    public class AuthenticatedFindAllQuery : ICqQuery<IReadOnlyList<IBaseModel>> {
        public AuthenticatedFindAllQuery(SimpleAuthentication authentication, string modelType, Query query) {
            ModelType = modelType;
            Query = query;
            Authentication = authentication;
        }

        public SimpleAuthentication Authentication { get; }
        public string ModelType { get; }
        public Query Query { get; }
    }
}