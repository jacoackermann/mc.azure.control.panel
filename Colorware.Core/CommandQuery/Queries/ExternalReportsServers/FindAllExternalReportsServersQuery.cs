﻿using System.Collections.Generic;

using Colorware.Core.Data.Models;

namespace Colorware.Core.CommandQuery.Queries.ExternalReportsServers {
    /// <summary>
    /// Query to find all external reports servers.
    /// </summary>
    public class FindAllExternalReportsServersQuery : ICqQuery<IEnumerable<IExternalReportsServer>> {

    }
}