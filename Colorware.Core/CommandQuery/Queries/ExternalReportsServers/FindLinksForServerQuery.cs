﻿using System.Collections.Generic;

using Colorware.Core.Data.Models;

namespace Colorware.Core.CommandQuery.Queries.ExternalReportsServers {

    public class FindLinksForServerQuery : ICqQuery<IEnumerable<IClientForExternalServer>> {

        public FindLinksForServerQuery(long externalReportServerId) {
            ExternalReportServerId = externalReportServerId;
        }

        public long ExternalReportServerId { get; private set; }
    }
}