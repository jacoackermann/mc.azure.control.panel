﻿using System.Threading;
using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.CommandQuery {

    /// <summary>
    /// Defines a query handler. Queries do not perform side effects so given the same input system,
    /// a given query will always return the same result, no matter how many times it is run on the system.
    /// </summary>
    /// <typeparam name="TQuery">The type of query to handle.</typeparam>
    /// <typeparam name="TResult">The return type of the query.</typeparam>
    public interface ICqQueryHandler<in TQuery, out TResult> where TQuery : ICqQuery<TResult> {
        TResult Handle([NotNull] TQuery query);
    }

    /// <summary>
    /// Defines an async version of <see cref="ICqQueryHandler{TQuery,TResult}"/>
    /// </summary>
    /// <typeparam name="TQuery">The type of query to handle.</typeparam>
    /// <typeparam name="TResult">The return type of the query.</typeparam>
    public interface ICqAsyncQueryHandler<in TQuery, TResult> where TQuery : ICqQuery<TResult> {
        Task<TResult> HandleAsync([NotNull] TQuery query, CancellationToken token);
    }
}