﻿namespace Colorware.Core.CommandQuery {

    /// <summary>
    /// Commands need to implement this marker interface to mark them as a command. This primarily
    /// aids in discoverability of commands, making improper usage less likely by leveraging the type system.
    /// </summary>
    public interface ICqCommand {}

    /// <summary>
    /// Like <see cref="ICqCommand"/>, but takes a type parameter that specifies the return type that
    /// the command will produce. While commands should be fire-and-forget generally, there may be
    /// special cases where a return type is waranted.
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    public interface ICqCommand<TResult> {}
}