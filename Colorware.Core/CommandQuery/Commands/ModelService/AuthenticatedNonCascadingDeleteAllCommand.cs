﻿using Colorware.Core.Data;
using Colorware.Core.Domain.Security;

namespace Colorware.Core.CommandQuery.Commands.ModelService {

    public class AuthenticatedNonCascadingDeleteAllCommand : ICqCommand {

        public AuthenticatedNonCascadingDeleteAllCommand(SimpleAuthentication authentication, string modelType, Query query) {
            ModelType = modelType;
            Query = query;
            Authentication = authentication;
        }

        public SimpleAuthentication Authentication { get; }
        public string ModelType { get; }
        public Query Query { get; }
    }
}