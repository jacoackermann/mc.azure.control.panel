﻿using System;
using System.Collections.Generic;

using Colorware.Core.Data.Specification;

using JetBrains.Annotations;

namespace Colorware.Core.CommandQuery.Commands.ModelService {
    /// <summary>
    /// Perform a save for all passed in items, using the passed in authentication parameters for
    /// access restriction.
    ///
    /// Returns a list of saved id's. This corresponds to the result of <see cref="SaveAllBulkResult"/>.
    /// </summary>
    public class SaveAllBulkCommand : ICqCommand {
        public SaveAllBulkCommand([ItemNotNull, NotNull] IReadOnlyList<IBaseModel> dataToSave) {
            DataToSave = dataToSave ?? throw new ArgumentNullException(nameof(dataToSave));
        }

        [ItemNotNull, NotNull]
        public IReadOnlyList<IBaseModel> DataToSave { get; }
    }
}