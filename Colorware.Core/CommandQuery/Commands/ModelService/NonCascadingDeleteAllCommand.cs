﻿using System;

using Colorware.Core.Data;

using JetBrains.Annotations;

namespace Colorware.Core.CommandQuery.Commands.ModelService {
    public class NonCascadingDeleteAllCommand : ICqCommand {
        public NonCascadingDeleteAllCommand([NotNull] string modelType, Query query) {
            ModelType = modelType ?? throw new ArgumentNullException(nameof(modelType));
            Query = query;
        }

        [NotNull]
        public string ModelType { get; }

        public Query Query { get; }
    }
}