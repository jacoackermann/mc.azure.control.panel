﻿using System.Collections.Generic;

using Colorware.Core.Data.Specification;
using Colorware.Core.Domain.Security;

namespace Colorware.Core.CommandQuery.Commands.ModelService {

    /// <summary>
    /// A command to delete all passed in data. Check the authentication parameters to see if the
    /// data is allowed to be deleted.
    ///
    /// Does a cascading delete, also deleting relations.
    /// </summary>
    public class AuthenticatedCascadingDeleteAllCommand : ICqCommand {

        public AuthenticatedCascadingDeleteAllCommand(SimpleAuthentication authentication,
                                                      List<IBaseModel> dataToDelete) {
            DataToDelete = dataToDelete;
            Authentication = authentication;
        }

        public SimpleAuthentication Authentication { get; }
        public List<IBaseModel> DataToDelete { get; }
    }
}