﻿using System;
using System.Collections.Generic;

using Colorware.Core.Data.Specification;

using JetBrains.Annotations;

namespace Colorware.Core.CommandQuery.Commands.ModelService {
    /// <summary>
    /// Perform a save for all passed in items, using the passed in authentication parameters for
    /// access restriction.
    ///
    /// Returns a list of saved id's. This corresponds to the result of <see cref="SaveAllResult"/>.
    /// </summary>
    public class SaveAllCommand : ICqCommand {
        public SaveAllCommand([ItemNotNull, NotNull] List<IBaseModel> dataToSave) {
            DataToSave = dataToSave ?? throw new ArgumentNullException(nameof(dataToSave));
        }

        [ItemNotNull, NotNull]
        public List<IBaseModel> DataToSave { get; }
    }
}