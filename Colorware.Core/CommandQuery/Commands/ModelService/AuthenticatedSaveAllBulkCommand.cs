﻿using Colorware.Core.Data.Specification;
using Colorware.Core.Domain.Security;
using System.Collections.Generic;

namespace Colorware.Core.CommandQuery.Commands.ModelService {

    /// <summary>
    /// Perform a save for all passed in items, using the passed in authentication parameters for
    /// access restriction.
    ///
    /// Returns a list of saved id's. This corresponds to the result of <see cref="SaveAllBulkResult"/>.
    /// </summary>
    public class AuthenticatedSaveAllBulkCommand : ICqCommand {

        public AuthenticatedSaveAllBulkCommand(SimpleAuthentication authentication, IReadOnlyList<IBaseModel> dataToSave) {
            DataToSave = dataToSave;
            Authentication = authentication;
        }

        public SimpleAuthentication Authentication { get; }
        public IReadOnlyList<IBaseModel> DataToSave { get; }
    }
}