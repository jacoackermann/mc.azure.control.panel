﻿using System;

using Colorware.Core.Data.Models;

using JetBrains.Annotations;

namespace Colorware.Core.CommandQuery.Commands.JobPresets {

    public class SaveJobPresetCommand : ICqCommand {

        public SaveJobPresetCommand([NotNull] IJobPreset jobPreset) {
            if (jobPreset == null) throw new ArgumentNullException(nameof(jobPreset));
            JobPreset = jobPreset;
        }

        public IJobPreset JobPreset { get; }
    }
}