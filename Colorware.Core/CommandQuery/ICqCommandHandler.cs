﻿using System.Threading;
using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.CommandQuery {

    /// <summary>
    /// Defines a pure command handler interface. A command is a fire and forget implementation which
    /// has side-effects but returns no data.
    /// </summary>
    /// <typeparam name="TCommand">The type of command this handles.</typeparam>
    public interface ICqCommandHandler<in TCommand> where TCommand : ICqCommand {
        void Handle([NotNull] TCommand command);
    }

    /// <summary>
    /// Defines a pure async command handler interface. A command is a fire and forget implementation
    /// which has side-effects but returns no data.
    /// </summary>
    /// <typeparam name="TCommand">Type of command that is accepted.</typeparam>
    public interface ICqAsyncCommandHandler<in TCommand> where TCommand : ICqCommand {
        Task HandleAsync([NotNull] TCommand command, CancellationToken token);
    }

    /// <summary>
    /// Defines an impure command handler interface. Commands are meant to be fire and forget, though
    /// there is a return parameter defined here. The return parameter is meant to be used as either
    /// an error checking mechanism (returning bool or result instead of throwing an error) or for
    /// use in exceptional situations where a return parameter is required and the command isn't
    /// quite a <see cref="ICqQueryHandler{TQuery,TResult}"/> thing.
    /// </summary>
    /// <typeparam name="TCommand">Type of command that is accepted.</typeparam>
    /// <typeparam name="TResult">Type of result that is returned.</typeparam>
    public interface ICqCommandHandler<in TCommand, out TResult> where TCommand : ICqCommand<TResult> {
        TResult Handle([NotNull] TCommand command);
    }

    /// <summary>
    /// Async definition of <see cref="ICqCommandHandler{TCommand}"/>
    /// </summary>
    /// <typeparam name="TCommand">The type of command this handles.</typeparam>
    /// <typeparam name="TResult">The return type returned by this handler.</typeparam>
    public interface ICqAsyncCommandHandler<in TCommand, TResult> where TCommand : ICqCommand<TResult> {
        Task<TResult> HandleAsync([NotNull] TCommand command, CancellationToken token);
    }
}