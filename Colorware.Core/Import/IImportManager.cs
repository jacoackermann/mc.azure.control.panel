using System.Collections.Generic;
using System.Collections.Immutable;
using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.Import {
    public interface IImportManager {
        [NotNull]
        IImmutableList<IImporter> Importers { get; }

        /// <summary>
        /// Shows a file selector dialog with any registered importers for the type that is passed as a type
        /// parameter. When the user selects a file and importer the appropriate importer is called
        /// to import the passed item.
        /// </summary>
        /// <typeparam name="T">The type to import.</typeparam>
        /// <returns>
        /// The imported item, or null if user canceled or something went wrong.
        /// </returns>
        Task<T> ImportAsync<T>([NotNull] IDictionary<string, object> arguments) where T : class;

        Task<T> ImportAsync<T>() where T : class;

        Task<T> ImportAsync<T>(IImporter<T> withImporter, string path,
            [NotNull] IDictionary<string, object> arguments);

        Task<T> ImportAsync<T>(IImporter<T> withImporter, string path);

        [NotNull]
        IImmutableList<IImporter<T>> ImportersFor<T>();

        [CanBeNull]
        IImporter<T> ImporterFor<T>([NotNull] string name);
    }
}