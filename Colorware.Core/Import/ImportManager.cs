﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Colorware.Core.ErrorReporting;
using Colorware.Core.Globalization;

using JetBrains.Annotations;

using Microsoft.Win32;

namespace Colorware.Core.Import {
    [UsedImplicitly]
    public class ImportManager : INotifyPropertyChanged, IImportManager {
        private readonly IErrorReporter errorReporter;

        private const string DefaultExtKey = "Defaults.ImportManager.Extension";

        public IImmutableList<IImporter> Importers { get; }

        public ImportManager([NotNull] IErrorReporter errorReporter,
                             [NotNull] IImporter[] importers) {
            if (errorReporter == null) throw new ArgumentNullException(nameof(errorReporter));
            if (importers == null) throw new ArgumentNullException(nameof(importers));

            this.errorReporter = errorReporter;
            Importers = importers.ToImmutableList();
        }

        /// <summary>
        /// Shows a file selector dialog with any registered importers for the type that is passed as a type
        /// parameter. When the user selects a file and importer the appropriate importer is called
        /// to import the passed item.
        /// </summary>
        /// <typeparam name="T">The type to import.</typeparam>
        /// <returns>
        /// The imported item, or null if user canceled or something went wrong.
        /// </returns>
        public Task<T> ImportAsync<T>(IDictionary<string, object> arguments) where T : class {
            if (arguments == null) throw new ArgumentNullException(nameof(arguments));

            var importers = ImportersFor<T>();
            if (!importers.Any())
                errorReporter.ReportError(LanguageManager._("Misc.ImportManager.NoImporters") + typeof(T).Name,
                                          LanguageManager._("Misc.ImportManager.NoImportersCap"));

            var fileDialog = new OpenFileDialog {
                Filter = buildSaveDialogFilter(importers),
                RestoreDirectory = true,
                FilterIndex = Config.Config.DefaultConfig.GetInt(DefaultExtKey, 0)
            };
            if (fileDialog.ShowDialog() != true)
                return Task.FromResult<T>(null);

            var importer = importers.ElementAt(fileDialog.FilterIndex - 1);
            Config.Config.DefaultConfig.Set(DefaultExtKey, fileDialog.FilterIndex.ToString(CultureInfo.InvariantCulture));

            return ImportAsync(importer, fileDialog.FileName, arguments);
        }

        public Task<T> ImportAsync<T>() where T : class {
            return ImportAsync<T>(new Dictionary<string, object>());
        }

        public async Task<T> ImportAsync<T>(IImporter<T> withImporter, string path,
                                            IDictionary<string, object> arguments) {
            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read)) {
                var result = await withImporter.ImportAsync(stream, arguments);
                return result;
            }
        }

        public Task<T> ImportAsync<T>(IImporter<T> withImporter, string path) {
            return ImportAsync(withImporter, path, new Dictionary<string, object>());
        }

        private string buildSaveDialogFilter(IEnumerable<IImporter> importers) {
            var items = importers.Select(e =>
                                         $"{e.Name} (.{e.Extension})|*.{e.Extension}");
            return string.Join("|", items);
        }

        #region Find appropriate importers
        public IImmutableList<IImporter<T>> ImportersFor<T>() {
            return
                Importers.Where(e => e.ImportableType.IsAssignableFrom(typeof(T)))
                         .OrderBy(e => e.Name)
                         .Cast<IImporter<T>>()
                         .ToImmutableList();
        }

        public IImporter<T> ImporterFor<T>(string name) {
            return
                (IImporter<T>)
                Importers.FirstOrDefault(e => e.ImportableType.IsAssignableFrom(typeof(T)) && e.Name == name);
        }
        #endregion

        // Anti memory leak
#pragma warning disable 0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 0067
    }
}