﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.Import {
    public interface IImporter {
        /// <summary>
        /// Denotes the type of object this importer can import.
        /// </summary>
        [NotNull]
        Type ImportableType { get; }

        /// <summary>
        /// Extension used by any files in this format.
        /// </summary>
        [NotNull]
        string Extension { get; }

        /// <summary>
        ///  Name of this importer.
        /// </summary>
        [NotNull]
        string Name { get; }
    }

    public interface IImporter<T> : IImporter {
        /// <summary>
        /// Perform actual import.
        /// </summary>
        /// <param name="inStream">The stream to read the data from.</param>
        /// <param name="arguments">Optional dictionary with arguments to use in import process.</param>
        Task<T> ImportAsync([NotNull] Stream inStream, [NotNull] IDictionary<string, object> arguments);
    }

    public static class ImporterExtensions {
        /// <summary>
        /// Perform actual import.
        /// </summary>
        /// <param name="importer">The importer to perform the import with.</param>
        /// <param name="inStream">The stream to read the data from.</param>
        public static Task<T> ImportAsync<T>(this IImporter<T> importer, [NotNull] Stream inStream) {
            return importer.ImportAsync(inStream, new Dictionary<string, object>());
        }
    }
}