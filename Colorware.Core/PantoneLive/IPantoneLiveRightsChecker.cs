﻿using System.Threading.Tasks;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.PantoneLIVE;
using Colorware.Core.PantoneLive.Exceptions;

using JetBrains.Annotations;

namespace Colorware.Core.PantoneLive {
    public interface IPantoneLiveRightsChecker {
        /// <returns>True if the current PantoneLIVE user is allowed to access the dependent standard or if both supplied
        /// GUIDs are equal to Guid.Empty</returns>
        /// <exception cref="PantoneLiveConnectionException">The PantoneLIVE service could not be reached.</exception>
        /// <exception cref="PantoneLiveServiceException">The PantoneLIVE service returned an error response.</exception>
        Task<bool> MayAccessDependentStandard([NotNull] PantoneLiveStandardId standardId);

        /// <exception cref="PantoneLiveConnectionException">The PantoneLIVE service could not be reached.</exception>
        /// <exception cref="PantoneLiveServiceException">The PantoneLIVE service returned an error response.</exception>
        Task<bool> JobContainsForbiddenPatches([NotNull] IJob job);
    }
}