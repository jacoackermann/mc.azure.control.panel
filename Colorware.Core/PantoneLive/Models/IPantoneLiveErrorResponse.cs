﻿using Colorware.Core.PantoneLive.Enums;

using JetBrains.Annotations;

namespace Colorware.Core.PantoneLive.Models {
    public interface IPantoneLiveErrorResponse : IPantoneLiveResponse {
        PantoneLiveErrorCode ErrorCode { get; set; }

        [CanBeNull]
        string ErrorText { get; set; }

        // ReSharper disable once InconsistentNaming
        PantoneLiveErrorCode EULAStatus { get; set; }
    }
}