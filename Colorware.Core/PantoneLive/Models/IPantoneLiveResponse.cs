﻿namespace Colorware.Core.PantoneLive.Models {
    // PantoneLIVE responses have no default characteristic, but this interface
    // allows us to put some constraints on generic types for responses
    public interface IPantoneLiveResponse {
    }
}