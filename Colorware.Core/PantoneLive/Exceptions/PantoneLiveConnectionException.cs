﻿using System;
using System.Runtime.Serialization;

using JetBrains.Annotations;

namespace Colorware.Core.PantoneLive.Exceptions {
    [Serializable]
    public class PantoneLiveConnectionException : Exception {
        [UsedImplicitly]
        public PantoneLiveConnectionException() {
        }

        public PantoneLiveConnectionException(string message) : base(message) {
        }

        public PantoneLiveConnectionException(string message, Exception inner) : base(message, inner) {
        }

        protected PantoneLiveConnectionException(SerializationInfo info, StreamingContext context)
            : base(info, context) {
        }
    }
}