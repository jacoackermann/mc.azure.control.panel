﻿using System;
using System.Runtime.Serialization;

using Colorware.Core.PantoneLive.Models;

using JetBrains.Annotations;

namespace Colorware.Core.PantoneLive.Exceptions {
    [Serializable]
    public class PantoneLiveServiceException : Exception {
        private readonly IPantoneLiveErrorResponse errorResponse;

        [NotNull]
        public IPantoneLiveErrorResponse ErrorResponse {
            get { return errorResponse; }
        }

        [UsedImplicitly]
        public PantoneLiveServiceException() {
        }

        public PantoneLiveServiceException([NotNull] IPantoneLiveErrorResponse errorResponse)
            : base(errorResponse.ErrorText) {
            if (errorResponse == null) throw new ArgumentNullException("errorResponse");

            this.errorResponse = errorResponse;
        }

        public PantoneLiveServiceException([NotNull] IPantoneLiveErrorResponse errorResponse, Exception inner)
            : base(errorResponse.ErrorText, inner) {
            if (errorResponse == null) throw new ArgumentNullException("errorResponse");

            this.errorResponse = errorResponse;
        }

        protected PantoneLiveServiceException(SerializationInfo info, StreamingContext context)
            : base(info, context) {
        }
    }
}