using System;
using System.Xml.Serialization;

namespace Colorware.Core.PantoneLive.Enums {
    public enum PantoneLiveErrorCode {
        [XmlEnum("0")]
        Undefined = 0,

        [XmlEnum("1")]
        GenericDatabaseError = 1,

        [XmlEnum("2")]
        PasswordEncodingError = 2,

        [XmlEnum("3")]
        DatabaseConnectionError = 3,

        [XmlEnum("4")]
        MustChangePassword = 4,

        // User's registration not approved (user has registered but PL support have
        // not yet authorized the registration)
        [XmlEnum("5")]
        RegistrationNotApproved = 5,

        // This is usually due to too many failed sign-on attempts. The user can go
        // through the password reset procedure on the web portal.
        [XmlEnum("6")]
        UserLockedOut = 6,

        // User must either enter the correct values or use the password reset procedure
        // on the web portal
        [XmlEnum("7")]
        InvalidCredentials = 7,

        // User is not authorized to the chosen option. Contact PL support to purchase
        // additional features.
        [XmlEnum("8")]
        NotAuthorized = 8,

        // Probably reserved for later
        [XmlEnum("9")]
        NotUsed = 9,

        // User has requested an entity which doesn't exist or isn�t exposed by the web service
        [XmlEnum("10")]
        IncorrectEntitySpecified = 10,

        [XmlEnum("11")]
        NoDataFound = 11,

        // A CXF file can only contain one library or palette so one of GUID, LibraryOID or
        // PaletteOID must be selected (multiple selections also not permitted)
        [XmlEnum("12")]
        OnlyOneLibraryPerCxfAllowed = 12,

        // Note that this also happens when the machine hash is incorrect (error code 14, which
        // used to indicate this, no longer exists)
        [XmlEnum("13")]
        NoActivationCodeRegisteredToUser = 13,

        // The list of machine ID characteristics differs sufficiently from the registered
        // machine details to require re-registration. The original activation code must be
        // revoked and a new one generated at which point the application can be registered
        // to the new machine IDs.
        // NB: NOT USED ANYMORE
        [XmlEnum("14")]
        MachineIdNotRecognized = 14,

        [XmlEnum("15")]
        ActivationCodeInvalid = 15,

        [XmlEnum("16")]
        ActivationCodeAlreadyUsed = 16,

        [XmlEnum("17")]
        ApplicationCodeInvalid = 17,

        [XmlEnum("18")]
        WrongApplicationCodeForActivationCode = 18,

        [XmlEnum("19")]
        NoMachineIdsSupplied = 19,

        // The EULA needs to be signed but a 30 day grace period is allowed
        [XmlEnum("20")]
        EulaInGracePeriod = 20,

        // The EULA needs to be signed and the grace period has passed. No further PantoneLIVE
        // access is permitted until the EULA is signed.
        [XmlEnum("21")]
        EulaMustBeSigned = 21,

        [XmlEnum("22")]
        MaximumNumberOfConcurrentUserReached = 22,

        // An activation code which relates to a multi-seat license. It is not necessary to activate �
        // but the activation code must be passed on all web service calls instead.
        [XmlEnum("23")]
        MultiSeatNeedsNoActivation = 23,

        [XmlEnum("24")]
        AuthTokenInvalid = 24
    }

    public static class PantoneLiveErrorCodeExtensions {
        public static bool IndicatesUnauthorizedAccount(this PantoneLiveErrorCode errorCode) {
            switch (errorCode) {
                case PantoneLiveErrorCode.MustChangePassword:
                case PantoneLiveErrorCode.RegistrationNotApproved:
                case PantoneLiveErrorCode.UserLockedOut:
                case PantoneLiveErrorCode.InvalidCredentials:
                case PantoneLiveErrorCode.NotAuthorized:
                case PantoneLiveErrorCode.NoActivationCodeRegisteredToUser:
                case PantoneLiveErrorCode.MachineIdNotRecognized:
                case PantoneLiveErrorCode.ActivationCodeInvalid:
                case PantoneLiveErrorCode.ActivationCodeAlreadyUsed:
                case PantoneLiveErrorCode.ApplicationCodeInvalid:
                case PantoneLiveErrorCode.WrongApplicationCodeForActivationCode:
                case PantoneLiveErrorCode.EulaMustBeSigned:
                case PantoneLiveErrorCode.MaximumNumberOfConcurrentUserReached:
                    return true;
                default:
                    throw new ArgumentOutOfRangeException("errorCode");
            }
        }
    }
}