﻿using System;

using Castle.Core.Logging;

using JetBrains.Annotations;

namespace Colorware.Core.ErrorReporting {
    [UsedImplicitly]
    public class LoggingErrorReporter : IErrorReporter {
        private readonly ILogger log;

        public LoggingErrorReporter([NotNull] ILogger log) {
            if (log == null) throw new ArgumentNullException(nameof(log));
            this.log = log;
        }

        public void ReportError(string message, string shortMessage, Exception ex) {
            if (ex == null)
                log.Error(message);
            else
                log.Error(message, ex);
        }
    }
}