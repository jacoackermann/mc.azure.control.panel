﻿using System;

using Castle.Core.Logging;

using JetBrains.Annotations;

namespace Colorware.Core.ErrorReporting {
    [UsedImplicitly]
    // TODO: this sucks because it eats the original call site, which should be in the log
    public class LoggingErrorReporterDecorator : IErrorReporter {
        private readonly IErrorReporter errorReporter;
        private readonly ILogger log;

        public LoggingErrorReporterDecorator([NotNull] IErrorReporter errorReporter, [NotNull] ILogger log) {
            if (errorReporter == null) throw new ArgumentNullException(nameof(errorReporter));
            if (log == null) throw new ArgumentNullException(nameof(log));

            this.errorReporter = errorReporter;
            this.log = log;
        }

        public void ReportError(string message, string shortMessage, Exception ex) {
            log.Error(message, ex);
            errorReporter.ReportError(message, shortMessage, ex);
        }
    }
}