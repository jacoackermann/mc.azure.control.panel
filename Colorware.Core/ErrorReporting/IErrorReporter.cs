﻿using System;

using Colorware.Core.Functional.Option;

using JetBrains.Annotations;

namespace Colorware.Core.ErrorReporting {
    public interface IErrorReporter {
        void ReportError([NotNull] string message, [CanBeNull] string shortMessage, [CanBeNull] Exception ex);
    }

    public static class ErrorReporterInterfaceExtensions {
        public static void ReportError(this IErrorReporter reporter, [NotNull] string message) {
            reporter.ReportError(message, null, null);
        }

        public static void ReportError(this IErrorReporter reporter, [NotNull] string message, string shortMessage) {
            reporter.ReportError(message, shortMessage, null);
        }

        public static void ReportError(this IErrorReporter reporter, [NotNull] string message, Exception ex) {
            reporter.ReportError(message, null, ex);
        }

        public static void ReportError(this IErrorReporter reporter, [NotNull] string message,
                                       Option<Exception> maybeException) {
            maybeException.DoElse(e => reporter.ReportError(message, e),
                                  () => reporter.ReportError(message, null, null));
        }

        public static void ReportError(this IErrorReporter reporter, [NotNull] Exception ex) {
            if (ex == null) throw new ArgumentNullException(nameof(ex));

            reporter.ReportError(ex.Message, null, ex);
        }

        public static void ReportError(this IErrorReporter reporter, [NotNull] string message, string shortMessage,
                                       Option<Exception> maybeException) {
            maybeException.DoElse(e => reporter.ReportError(message, shortMessage, e),
                                  () => reporter.ReportError(message, shortMessage, null));
        }
    }
}