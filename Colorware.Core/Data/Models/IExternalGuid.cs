﻿using System;

namespace Colorware.Core.Data.Models {
    public interface IExternalGuid {
        Guid ExternalGuid { get; set; }
    }
}