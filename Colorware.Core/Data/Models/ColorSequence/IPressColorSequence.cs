﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Colorware.Core.Data.Models.ColorSequence {
    public interface IPressColorSequence : INotifyPropertyChanged {
        ObservableCollection<PressColorSequenceElement> Elements { get; set; }
        int NumberOfColors { get; set; }
        String SerializeToString();

        /// <summary>
        /// Note: This doesn't contain any color information, so this might have to be set afterwards!
        /// </summary>
        /// <param name="str"></param>
        void DeserializeFromString(String str);

        void Clear();
        void Add(PressColorSequenceElement element);
        void Add();
        void Add(int slot, System.Windows.Media.Color color);
        void Set(int index);
        void Set(int index, int slot, System.Windows.Media.Color color);
        void Set(int index, int slot, System.Windows.Media.Color color, String name);
        bool IsDefined(int index);
    }
}