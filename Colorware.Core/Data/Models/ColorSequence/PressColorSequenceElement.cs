using System;
using System.Diagnostics;
using System.Windows.Media;

using Colorware.Core.Mvvm;

namespace Colorware.Core.Data.Models.ColorSequence {
    [DebuggerDisplay("PressColorSequenceElement(Slot={Slot}, IsActive={IsActive}, Name={Name})")]
    public class PressColorSequenceElement : DefaultNotifyPropertyChanged {
        #region Properties
        private int slot;

        public int Slot {
            get { return slot; }
            set {
                slot = value;
                OnPropertyChanged("Slot");
            }
        }

        private System.Windows.Media.Color color;

        public System.Windows.Media.Color Color {
            get { return color; }
            set {
                color = value;
                OnPropertyChanged("Color");
            }
        }

        private bool isActive;

        public bool IsActive {
            get { return isActive; }
            set {
                isActive = value;
                OnPropertyChanged("IsActive");
                if (!isActive) Color = Colors.LightGray;
            }
        }

        private String name;

        public String Name {
            get { return name; }
            set {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        #endregion
    }
}