﻿using System;
using System.Threading.Tasks;

using Colorware.Core.Data.Models.StripCompareInterface.Scoring;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;
using Colorware.Core.StatusReporting;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models {
    public interface IJob : IBaseModel, IExternalGuid {
        long OtherSideId { get; set; }

        String Name { get; set; }

        String Number { get; set; }

        String Description { get; set; }

        /// <summary>
        /// Unique identifier for job. Currently used in CDS communication.
        /// </summary>
        string Guid { get; set; }

        /// <summary>
        /// SCHAWK: EPQ/CDS field, stores brand for CDS communication.
        /// </summary>
        String Brand { get; set; }

        /// <summary>
        /// SCHAWK: EPQ/CDS field, stores printer name for CDS communication.
        /// </summary>
        String PrinterName { get; set; }

        /// <summary>
        /// SCHAWK: EPQ/CDS field, stores printer location for CDS communication.
        /// </summary>
        String PrinterLocation { get; set; }

        /// <summary>
        /// SCHAWK: EPQ/CDS field, stores scoring set id for sending in transactions
        /// </summary>
        long CdsScoringSetId { get; set; }

        /// <summary>
        /// SCHAWK: EPQ/CDS field, stores scoring set name for sending in transactions
        /// </summary>
        string CdsScoringSetName { get; set; }

        double CachedScore { get; set; }

        int SheetNumber { get; set; }

        SheetSides SheetSide { get; set; }

        bool IsNa { get; set; }
        bool IsTop { get; set; }
        bool IsBottom { get; set; }
        String SheetSideName { get; }
        bool HasSheetSides { get; }
        bool HasOtherSideId { get; }

        DateTime LastDate { get; set; }

        String Sequence { get; set; }

        long MeasurementConditionId { get; set; }

        /// <summary>
        /// Semi custom field to indicate what kind of job this is.
        /// Values below 1024 are currently reserved for use by Colorware.
        /// </summary>
        int JobType { get; set; }

        /// <summary>
        /// Denotes whether this job is a flexo 'wide flexo film' job which uses different
        /// scoring preferences.
        /// </summary>
        bool IsWideFlexo { get; set; }

        int JobStripCurrentStartIndex { get; set; }

        int JobStripCurrentEndIndex { get; set; }

        String SerializedInkZoneLocks { get; set; }

        Guid ImageName { get; set; }

        InkZoneLockSet InkZoneLockSet { get; set; }

        long ToleranceSetId { get; set; }

        IBelongsTo<IToleranceSet> ToleranceSet { get; }

        long ColorStripId { get; set; }

        IBelongsTo<IColorStrip> ColorStrip { get; }

        [Obsolete("This gives a false sense of security (ColorStrip can be missing in DB)")]
        bool HasColorStrip { get; }

        long ReferenceId { get; set; }

        IBelongsTo<IReference> Reference { get; }

        long UserGroupId { get; set; }

        IBelongsTo<IUserGroup> UserGroup { get; }

        long ClientReferenceId { get; set; }

        IBelongsTo<IReference> ClientReference { get; }

        long JobStripId { get; set; }

        IBelongsTo<IJobStrip> JobStrip { get; }

        long OkSheetId { get; set; }

        IBelongsTo<IMeasurement> OkSheet { get; }

        [Obsolete("This gives a false sense of security (OkSheet can be missing in DB)")]
        bool HasOkSheet { get; }

        bool IsProductionMode { get; set; }

        long MachineId { get; set; }

        IBelongsTo<IMachine> Machine { get; }

        long PaperId { get; set; }

        IBelongsTo<IPaper> Paper { get; }

        long InkSetId { get; set; }

        IBelongsTo<IInkSet> InkSet { get; }

        MeasurementTypes MeasurementTypeId { get; set; }

        long PaperWhiteSampleId { get; set; }

        long ClientId { get; set; }

        /// <summary>
        /// User that created the job.
        /// </summary>
        long UserId { get; set; }

        /// <summary>
        /// Company that the jobs belongs to. Remote users will only be able to see the jobs
        /// for their own company.
        /// 
        /// If this is 0 there was no company available at the time of creation/migration.
        /// </summary>
        long PrintLocationCompanyId { get; set; }

        /// <summary>
        /// Company that created the job. Remote users will only be able to see the jobs
        /// for their own company.
        /// 
        /// If this is 0 there was no company available at the time of creation/migration.
        /// </summary>
        long CreatorCompanyId { get; set; }

        String CustomFieldData1 { get; set; }

        String CustomFieldData2 { get; set; }

        String CustomFieldData3 { get; set; }

        String CustomFieldData4 { get; set; }

        String CustomFieldData5 { get; set; }

        String CustomFieldData6 { get; set; }

        IBelongsTo<ISample> PaperWhiteSample { get; set; }

        IHasManyCollection<IMeasurement> Measurements { get; set; }

        IBelongsTo<IClient> Client { get; }

        IBelongsTo<IJob> OtherSide { get; set; }

        String PieceOfArt { get; set; }

        /// <summary>
        /// User that created the job.
        /// </summary>
        IBelongsTo<IDatabaseUser> User { get; }

        IHasManyCollection<ICdsInterfaceElement> CdsInterfaceElements { get; }

        IBelongsTo<IMeasurementConditionsConfig> MeasurementCondition { get; set; }

        String CachedScoreString { get; }

        IHasManyCollection<IJobInkInfo> JobInkInfos { get; set; }

        bool HasSeparatePaperWhite { get; }
        int[] SequenceArray { [NotNull] get; }

        bool ShouldUseOkSheet(IMeasurement measurement);
        bool ShouldUseOkSheet(long measurementId);

        bool AllowMeasurements { get; set; }
        IBelongsTo<ICompany> PrintLocationCompany { get; }
        IBelongsTo<ICompany> CreatorCompany { get; }

        bool UseAutomaticDensityAdjustment { get; set; }

        /// <summary>
        /// Helper function to initialize the <see cref="IJob.Guid" /> field if it is not yet set.
        /// If it is set, nothing changes. This method does not save out the job to the database, so just calling this method
        /// does not cause the value to persist.
        /// </summary>
        /// <returns><c>true</c>, if the field was initialized. <c>false</c>, if the field already had a value.</returns>
        bool InitGuidFieldIfUnset();

        void InitDefaultCustomData(IClient forClient);
        void SaveDefaultCustomData(IClient forClient);

        /// <summary>
        /// Saves the values of the passed in jobs as defaults to the configuration file.
        /// </summary>
        void SaveDefaults();

        IStripCompareScoreFactory GetCompareScoreFactory();

        /// <summary>
        /// Create a new (unsaved) measurement.
        /// Special attention has to be given to the flow change. If the job is set to
        /// production mode, the new measurement will have an accompanying flow change set.
        /// The current flow change of the job is also updated, but the job will
        /// not be saved automatically! NOTE: Do not forget to save the job if the measurement is saved!
        /// </summary>
        /// <returns>A new measurement.</returns>
        IMeasurement CreateMeasurement();

        Task RecalculateAllCachedResultsAsync([CanBeNull] IProgressReport progressReport);

        Task ToggleOkSheetAsync([NotNull] IMeasurement newMeasurement, [CanBeNull] IProgressReport progressReport);

        Task RecalculateCachedResultsAsync([CanBeNull] IMeasurement fromMeasurement,
                                           [CanBeNull] IProgressReport progressReport);

        Task CalculateAverageJobScoreAsync([NotNull] Action<bool, int> callback);
        Task ReloadCachedScore();
    }
}