﻿namespace Colorware.Core.Data.Models {
    public interface IMeasurementCustomField {
        int FieldIndex { get; }
        string Name { get; set; }
        string Value { get; set; }
        MeasurementCustomFieldType TypeOfField { get; set; }
        bool IsMultiline { get; }
        bool IsDirty { get; }
        void Clear();
    }
}