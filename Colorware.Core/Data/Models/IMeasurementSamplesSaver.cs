﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Helpers;

namespace Colorware.Core.Data.Models {
    public interface IMeasurementSamplesSaver {
        IProgressLoader Progress { get; set; }

        /// <summary>
        /// Saves the given measurement and samples, and links the samples to the measurement.
        /// </summary>
        /// <param name="measurement">The measurement to save.</param>
        /// <param name="samples">The samples to save.</param>
        /// <returns>The saved measurement.</returns>
        Task<IMeasurement> SaveAsync(IMeasurement measurement, IEnumerable<ISample> samples);
    }
}