﻿using System;

using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

namespace Colorware.Core.Data.Models {
    public interface IMeasurement : IBaseModel, ICreatedStamped, IExternalGuid {

        String Name { get; set; }

        /// <summary>
        /// Currently used as a remarks field for each measurement.
        /// </summary>
        String Content { get; set; }

        bool HasRemark { get; }

        bool IsWet { get; set; }

        bool OkSheet { get; set; }

        bool IsProductionMode { get; set; }

        int FlowChange { get; set; }

        /// <summary>
        /// Returns a status text based on the current FlowChange.
        /// FlowChange &lt; 0  ==&gt; Make ready
        /// FlowChange &gt; 0  ==&gt; Production
        /// FlowChange == 0  ==&gt; Ok Sheet
        /// Obviously, this should not be used for in-code status checks!
        /// </summary>
        String StatusText { get; }

        MeasurementTypes MeasurementType { get; set; }

        int JobStripStartIndex { get; set; }

        int JobStripEndIndex { get; set; }

        /// <summary>
        /// The identifier of the roll.
        /// </summary>
        String RollId { get; set; }

        /// <summary>
        /// Roll number.
        /// </summary>
        string FlexoRoll { get; set; }

        /// <summary>
        /// Panel identifier.
        /// </summary>
        String FlexoPanel { get; set; }

        bool IsFlexo { get; set; }

        /// <summary>
        /// Has an audit been saved locally?
        /// </summary>
        bool IsAudited { get; set; }

        /// <summary>
        /// Has an audit been submited to CDS?
        /// </summary>
        bool IsTransmitted { get; set; }

        bool HasCachedScore { get; set; }

        double CachedScore { get; set; }

        String SerializedInkZoneLocks { get; set; }

        InkZoneLockSet InkZoneLockSet { get; set; }

        long ReferenceId { get; set; }

        IBelongsTo<IReference> Reference { get; set; }

        long JobId { get; set; }

        IBelongsTo<IJob> Job { get; set; }

        long JobStripId { get; set; }

        IBelongsTo<IJobStrip> JobStrip { get; set; }
        IHasManyCollection<IEpqScore> EpqScores { get; }

        IHasManyCollection<ICachedCompareResult> CachedCompareResults { get; set; }

        IHasManyCollection<ISample> Samples { get; }

        long ScummingSampleId { get; set; }

        IBelongsTo<ISample> ScummingSample { get; }

        /// <summary>
        /// User that created the measurement.
        /// </summary>
        long UserId { get; set; }

        IBelongsTo<IDatabaseUser> User { get; }

        IHasManyCollection<IOpacityResult> OpacityResults { get; set; }

        String OneLineFormatContent { get; }

        bool HasCustomData { get; set; }
        string CustomStringField1 { get; set; }
        string CustomStringField2 { get; set; }
        string CustomStringField3 { get; set; }
        string CustomStringField4 { get; set; }
        string CustomStringField5 { get; set; }
        double CustomFloatField1 { get; set; }
        double CustomFloatField2 { get; set; }
        double CustomFloatField3 { get; set; }
        double CustomFloatField4 { get; set; }
        double CustomFloatField5 { get; set; }

        LastPqrStatus LastPqrStatus { get; set; }
    }
}