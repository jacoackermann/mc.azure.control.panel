using System.Collections.Generic;

using Colorware.Core.Data.Specification;
using Colorware.Core.Functional.Option;
using Colorware.Core.Helpers;

namespace Colorware.Core.Data.Models {
    public interface IDefaultDotgainEntry : IBaseModel {
        

        long DefaultDotgainListId { get; set; }

        int Percentage { get; set; }

        double Dotgain { get; set; }

        IBelongsTo<IDefaultDotgainList> DotgainList { get; }
    }

    public static class DefaultDotgainEntryExtensions {
        /// <returns>Nothing if the dotgain couldn't be found or interpolated.</returns>
        public static Option<double> FindDotgain(this IEnumerable<IDefaultDotgainEntry> dotgainEntries, int percentage) {
            IDefaultDotgainEntry lastEntry = null;

            foreach (var entry in dotgainEntries) {
                if (entry.Percentage == percentage)
                    return entry.Dotgain.ToOption();

                if (lastEntry != null &&
                    lastEntry.Percentage < percentage && percentage < entry.Percentage) {
                    // Will have to interpolate.
                    return InterpolationHelper.InterpolateDotgainLinear(lastEntry, entry, percentage).ToOption();
                }

                lastEntry = entry;
            }

            // TODO: we might just want to return 'percentage' here (check calling code if this is acceptable/better)
            return Option<double>.Nothing;
        }
    }
}