﻿using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IJobPresetTargetPosition : IBaseModel {
        

        int X { get; set; }

        int Y { get; set; }

        long JobPresetId { get; set; }

        int InkDefinitionsSequenceIndex { get; set; }

        bool IsSubstrate { get; set; }
    }
}