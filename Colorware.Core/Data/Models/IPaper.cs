﻿using System;

using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IPaper : IBaseModel, IExternalGuid {
        String Name { get; set; }

        int Weight { get; set; }

        long CompanyId { get; set; }

        IBelongsTo<ICompany> Company { get; }

        long PaperTypeId { get; set; }

        IBelongsTo<IPaperType> PaperType { get; }

        long PaperManufacturerId { get; set; }

        IBelongsTo<IPaperManufacturer> PaperManufacturer { get; }

        long PaperwhiteReferencePatchId { get; set; }

        IBelongsTo<IReferencePatch> PaperwhiteReferencePatch { get; }
    }
}