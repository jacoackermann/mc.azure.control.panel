using System;
using System.Collections.Generic;

using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

namespace Colorware.Core.Data.Models {
    public interface IEpqScore : IBaseModel, ICreatedStamped, IUpdatedStamped {
        long JobId { get; set; }

        long MeasurementId { get; set; }

        DateTime Production { get; set; }

        DateTime Received { get; set; }

        DateTime Recorded { get; set; }

        double VisualScore { get; set; }

        bool HasVisualScore { get; set; }

        String VisualDefects { get; set; }

        string PrinterName { get; set; }

        string PrinterLocation { get; set; }

        string Brand { get; set; }

        /// <summary>
        /// Returns/sets VisualDefects as a list of tags.
        /// </summary>
        IEnumerable<string> VisualDefectsAsList { get; set; }

        String Registrations { get; set; }

        Dictionary<int, int> RegistrationsAsDict { get; set; }

        bool HasRegistrations { get; set; }

        OpacityTypes OpacityType { get; set; }

        double OpacityLow { get; set; }

        double OpacityHigh { get; set; }

        double OpacityAverage { get; set; }

        bool WasExported { get; set; }

        DateTime LastExportDate { get; set; }

        bool WasTransmitted { get; set; }

        DateTime LastTransmitDate { get; set; }
    }
}