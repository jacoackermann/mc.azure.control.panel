﻿using System;

using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {

    public interface IDatabaseUser : IBaseModel, IExternalGuid {
        long UserGroupId { get; set; }

        long CompanyId { get; set; }

        string Name { get; set; }

        string Username { get; set; }

        string Password { get; set; }

        Guid ForcedLicenseProduct { get; set; }

        IBelongsTo<IUserGroup> UserGroup { get; }

        IBelongsTo<ICompany> Company { get; }

        /// <summary>
        /// Name to use for display purposes. Only the Name field for now.
        /// </summary>
        string DisplayName { get; }
    }
}