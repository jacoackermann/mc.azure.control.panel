﻿using System;
using System.ComponentModel;

using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IColorStrip : IBaseModel, INotifyPropertyChanged {
        

        String Name { get; set; }

        String Description { get; set; }

        double PatchWidth { get; set; }

        double PatchHeight { get; set; }

        /// <summary>
        /// Invariant: patches in the ColorStrip are equally distributed over the amount of
        /// rows specifid in this field, if possible. The invariant materializes when
        /// the strip is actually mapped and JobStripPatches are generated. ColorStripPatches
        /// themselves have no row number, but it's implied by the invariant.
        /// </summary>
        int RowCount { get; set; }

        String DefaultMapping { get; set; }

        int PatchesPerZone { get; set; }

        int NumberOfZones { get; set; }

        bool Repeats { get; set; }

        long CompanyId { get; set; }

        IBelongsTo<ICompany> Company { get; }

        IHasManyCollection<IColorStripPatch> ColorStripPatches { get; }
    }
}