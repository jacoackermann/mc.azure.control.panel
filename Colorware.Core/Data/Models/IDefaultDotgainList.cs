using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IDefaultDotgainList : IBaseModel {
        

        string Name { get; set; }

        IHasManyCollection<IDefaultDotgainEntry> DefaultDotgainEntries { get; }
    }
}