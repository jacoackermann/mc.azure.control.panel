namespace Colorware.Core.Data.Models {
    public interface IInkZone {
        int InkZone { get; set; }
    }
}