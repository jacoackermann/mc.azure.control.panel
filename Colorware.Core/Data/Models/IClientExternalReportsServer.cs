﻿using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models
{
    public interface IClientExternalReportsServer : IBaseModel {
        long ClientId { get; set; }
        IBelongsTo<IClient> Client { get; }

        long ExternalReportsServerId { get; set; }
        IBelongsTo<IExternalReportsServer> ExternalReportsServer { get; }

        bool Active { get; set; }
        PqmSendMethod SendMethod { get; set; }
    }

    public enum PqmSendMethod {
        Nothing = 0,
        Everything = 1,
        Selected = 2
    }
}
