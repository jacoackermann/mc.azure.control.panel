﻿using System;

using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IInkType : IBaseModel {
        String Name { get; set; }
    }
}