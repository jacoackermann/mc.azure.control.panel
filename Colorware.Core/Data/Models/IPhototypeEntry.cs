﻿using System;

using Colorware.Core.Data.Specification;
using Colorware.Core.Globalization;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models {

    public enum PhotoTypeUpc {
        [EnumLanguage("CP.PhotoType.Upc.A")]
        UpcA = 1,
        [EnumLanguage("CP.PhotoType.Upc.B")]
        UpcB = 2,
        [EnumLanguage("CP.PhotoType.Upc.C")]
        UpcC = 3,
        [EnumLanguage("CP.PhotoType.Upc.D")]
        UpcD = 4,
        [EnumLanguage("CP.PhotoType.Upc.F")]
        UpcF = 6
    }

    public enum PhotoTypeVisualMatch {
        [EnumLanguage("CP.PhotoType.VisualMatch.A")]
        VisualMatchA = 1,
        [EnumLanguage("CP.PhotoType.VisualMatch.C")]
        VisualMatchC = 3,
        [EnumLanguage("CP.PhotoType.VisualMatch.F")]
        VisualMatchF = 6
    }

    public interface IPhotoTypeEntry : IBaseModel {
        [CanBeNull]
        string Comments { get; set; }

        /// <summary>
        /// This should be the current date.
        /// </summary>
        DateTime CompletedDate { get; set; }

        /// <summary>
        /// Date the job was created.
        /// </summary>
        DateTime DateReceived { get; set; }

        /// <summary>
        /// Date the job was created.
        /// </summary>
        DateTime DateScheduled { get; set; }

        int? DefectClass1 { get; set; }
        int? DefectClass2 { get; set; }
        int? DefectClass3 { get; set; }
        int? DefectClass4 { get; set; }

        /// <summary>
        /// Should be between 0.0 and 0.9 inclusive.
        /// </summary>
        double HorizontalRegistration { get; set; }

        /// <summary>
        /// Should be between 0.0 and 0.9 inclusive.
        /// </summary>
        double VerticalRegistration { get; set; }

        /// <summary>
        /// Is this a production mode entry?
        /// </summary>
        bool IsProduction { get; set; }

        /// <summary>
        /// Id of the job this belongs to.
        /// </summary>
        long JobId { get; set; }

        /// <summary>
        /// Id of the measurement this belongs to.
        /// </summary>
        long MeasurementId { get; set; }

        /// <summary>
        /// Should match the entry in PhotoType.
        /// </summary>
        [NotNull]
        string PieceOfArtName { get; set; }

        /// <summary>
        /// Marks the pressrun as completed.
        ///
        /// TODO: After a pressrun is completed, we probably don't want to send in any further data.
        /// </summary>
        bool PressrunCompleted { get; set; }

        /// <summary>
        /// Date the job was created.
        /// </summary>
        DateTime PressrunDate { get; set; }

        /// <summary>
        /// Identifier for the presssheet. Corresponds to the MeasureColor measurement.
        /// </summary>
        [NotNull]
        string PressSheetIdentifier { get; set; }

        /// <summary>
        /// Date the measurement was saved (Measurement.CreatedAt).
        /// </summary>
        DateTime PrintedDate { get; set; }

        /// <summary>
        /// Measurement roll number if present.
        /// </summary>
        [CanBeNull]
        string PrinterRollNumber { get; set; }

        /// <summary>
        /// This should be the job number.
        /// </summary>
        [NotNull]
        string PrintLotNumber { get; set; }

        /// <summary>
        /// Location of the printing plant.
        /// </summary>
        [NotNull]
        string PrintPlantLocation { get; set; }

        PhotoTypeUpc Upc { get; set; }

        PhotoTypeVisualMatch VisualMatch { get; set; }
    }
}