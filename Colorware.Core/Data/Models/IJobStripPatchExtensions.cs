﻿using Colorware.Core.Extensions;

namespace Colorware.Core.Data.Models {
    public static class IJobStripPatchExtensions {
        /// <summary>
        /// Check if the patch is a CMY overprint patch.
        /// </summary>
        /// <param name="patch">The patch to check.</param>
        public static bool IsCmyOverprint(this IJobStripPatch patch) {
            return patch.IsOverprint &&
                   patch.Slot1 >= 0 &&
                   patch.Slot2 >= 0 &&
                   patch.Slot3 >= 0 &&
                   patch.Slot1 != patch.Slot2 &&
                   patch.Slot1 != patch.Slot3 &&
                   patch.Slot2 != patch.Slot3;
        }

        /// <summary>
        /// Check if the patch is a solid black patch.
        /// </summary>
        /// <param name="patch">The patch to check.</param>
        public static bool IsSolidKey(this IJobStripPatch patch) {
            return patch.IsSolid &&
                   patch.Cyan.NearlyEquals(0) &&
                   patch.Magenta.NearlyEquals(0) &&
                   patch.Yellow.NearlyEquals(0) &&
                   patch.Key > 0;
        }
    }
}