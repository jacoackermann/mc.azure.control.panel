﻿using System;

using Colorware.Core.Algorithms.DeltaEShapeApproximation.DeltaEStrategies;
using Colorware.Core.Color;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

namespace Colorware.Core.Data.Models {
    public interface IToleranceSet : IBaseModel, IExternalGuid {
        String Name { get; set; }

        DeltaEMethod DeltaEMethod { get; set; }

        String DeltaEMethodName { get; }

        String DeltaEMethodNameShort { get; }

        double DeltaCMCL { get; set; }

        double DeltaCMCC { get; set; }

        bool AverageCalculated { get; set; }

        double AverageTolerance { get; set; }

        bool DeltaHCalculated { get; set; }

        double DeltaHTolerance { get; set; }

        bool DeltaHPrimariesCalculated { get; set; }

        double DeltaHPrimariesTolerance { get; set; }

        bool DensityCalculated { get; set; }

        double DensityTolerance { get; set; }

        double DensityVariation { get; set; }

        bool PrimariesCalculated { get; set; }

        double PrimariesTolerance { get; set; }

        double PrimariesVariation { get; set; }

        bool SecondariesCalculated { get; set; }

        double SecondariesTolerance { get; set; }

        double SecondariesVariation { get; set; }

        bool SpotcolorCalculated { get; set; }

        double SpotcolorTolerance { get; set; }

        double SpotcolorVariation { get; set; }

        bool DotgainCalculated { get; set; }

        double DotgainSpread { get; set; }

        double DotgainTolerance { get; set; }

        double DotgainVariation { get; set; }

        bool DotgainDeltaECalculated { get; set; }

        double DotgainDeltaETolerance { get; set; }

        double DotgainDeltaEVariation { get; set; }

        bool GraybalanceCalculated { get; set; }

        double GraybalanceTolerance { get; set; }

        double GraybalanceVariation { get; set; }

        bool MaxCalculated { get; set; }

        double MaxTolerance { get; set; }

        bool PaperwhiteCalculated { get; set; }

        double PaperwhiteTolerance { get; set; }

        bool ScummingCalculated { get; set; }

        double ScummingTolerance { get; set; }

        bool G7Calculated { get; set; }

        double G7WeightedDeltaLAverage { get; set; }

        double G7WeightedDeltaLPeak { get; set; }

        double G7WeightedDeltaChAverage { get; set; }

        double G7WeightedDeltaChPeak { get; set; }

        int ScoreForPrimaries { get; set; }

        int ScoreForSpots { get; set; }

        int ScoreForDotgains { get; set; }

        int ScoreForSecondaries { get; set; }

        int ScoreForPaperwhite { get; set; }

        int ScoreForGraybalance { get; set; }

        int ScoreForG7 { get; set; }

        int ScoreForOther { get; set; }

        int ScoreForMax { get; set; }

        int ScoreForAverage { get; set; }

        double ScorePercentageForPrimaries { get; set; }

        double ScorePercentageForSpots { get; set; }

        double ScorePercentageForDotgains { get; set; }

        double ScorePercentageForSecondaries { get; set; }

        double ScorePercentageForPaperWhite { get; set; }

        double ScorePercentageForGraybalance { get; set; }

        double ScorePercentageForG7 { get; set; }

        double ScorePercentageForOther { get; set; }

        double ScorePercentageForMax { get; set; }

        double ScorePercentageForAverage { get; set; }

        bool OpacityMeasured { get; set; }

        double OpacityTarget { get; set; }

        double ScoreForOpacity { get; set; }

        long DefaultMeasurementConditionId { get; set; }

        IBelongsTo<IMeasurementConditionsConfig> DefaultMeasurementCondition { get; }

        bool HasDefaultMeasurementCondition { get; }

        /// <summary>
        /// If InProduction is true, this indicates that we need to use the variation and not
        /// the regular tolerance if it is available.
        /// </summary>
        bool InProduction { get; set; }

        bool IsDeltaEToleranceCalculated(ToleranceGroups toleranceGroup);
        bool IsDeltaHToleranceCalculated(ToleranceGroups toleranceGroup);
        bool IsDensityCalculated(ToleranceGroups toleranceGroup);
        bool IsDotgainToleranceCalculated(ToleranceGroups toleranceGroup);
        bool IsMaxToleranceCalculated(ToleranceGroups toleranceGroup);
        bool IsScummingCalculated(ToleranceGroups toleranceGroup);
        bool IsG7WeightedDeltaLToleranceCalculated(ToleranceGroups toleranceGroup, bool isKey, bool isThreeColorOverprint);
        bool IsG7WeightedDeltaChToleranceCalculated(ToleranceGroups toleranceGroup, bool isKey, bool isThreeColorOverprint);
        double DeltaEToleranceFor(ToleranceGroups toleranceGroup);
        double DeltaHToleranceFor(ToleranceGroups toleranceGroup);
        double DensityToleranceFor(ToleranceGroups toleranceGroup);
        double DotgainToleranceFor(ToleranceGroups toleranceGroup);
        double MaxToleranceFor(ToleranceGroups toleranceGroup);
        double ScummingToleranceFor(ToleranceGroups toleranceGroup);
        double G7WeightedDeltaLPeakToleranceFor(ToleranceGroups toleranceGroup, bool isKey, bool isThreeColorOverprint);
        double G7WeightedDeltaChPeakToleranceFor(ToleranceGroups toleranceGroup, bool isKey, bool isThreeColorOverprint);
        double G7WeightedDeltaLAverageToleranceFor(ToleranceGroups toleranceGroup, bool isKey, bool isThreeColorOverprint);
        double G7WeightedDeltaChAverageToleranceFor(ToleranceGroups toleranceGroup, bool isKey, bool isThreeColorOverprint);
        double GetScorePercentageForType(PatchTypes patchType, bool isSpot);
        double GetScoreForType(PatchTypes patchType, bool isSpot);
        double DeltaEUsingCurrentMethod(Lab refLab, Lab sampleLab);
        IDeltaEStrategy GetDeltaEStrategy();
    }
}