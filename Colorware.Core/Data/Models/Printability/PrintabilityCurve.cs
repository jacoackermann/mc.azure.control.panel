﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

using Colorware.Core.Color;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Mvvm;

using CWModel;

namespace Colorware.Core.Data.Models.Printability {
    public class PrintabilityCurve : DefaultNotifyPropertyChanged {
        /// <summary>
        /// Number of points to use for calculating the points of the interpolated curve.
        /// </summary>
        private const int NrOfPointsForDrawingInterpolatedCurve = 100;

        /// <summary>
        /// The 'elasticity' to use for the calculation of the curve.
        /// </summary>
        private readonly double rhoForModel;

        private const double DefaultRho = 0.5;

        /// <summary>
        /// The curve container to calculte for.
        /// </summary>
        private readonly CurveContainer curveContainer;

        /// <summary>
        /// The compare result to get the data from.
        /// </summary>
        private readonly IStripCompareResult compareResult;

        public IStripCompareResult CompareResult {
            get { return compareResult; }
        }

        #region Properties
        /// <summary>
        /// The solid patch this curve belongs to.
        /// </summary>
        private readonly IStripComparePatch solid;

        public IStripComparePatch Solid {
            get { return solid; }
        }

        private IStripComparePatch closestSolidToSelection;

        public IStripComparePatch ClosestSolidToSelection {
            get { return closestSolidToSelection; }
            set {
                if (closestSolidToSelection == value)
                    return;
                closestSolidToSelection = value;
                OnPropertyChanged("ClosestSolidToSelection");
                OnPropertyChanged("ClosestXYValue");
            }
        }

        private List<IStripComparePatch> solids;

        /// <summary>
        /// List of solids belonging to this curve.
        /// </summary>
        public List<IStripComparePatch> Solids {
            get { return solids; }
            set {
                solids = value;
                OnPropertyChanged("Solids");
                OnPropertyChanged("SolidsAsLabs");
            }
        }

        public IReadOnlyList<Lab> SolidsAsLabs {
            get {
                if (Solids == null)
                    return new List<Lab>();

                return Solids.Select(p => p.SampleLab).ToList();
            }
        }

        /// <summary>
        /// Points for drawing the interpolated curve.
        /// </summary>
        public KeyValuePair<double, double>[] CurvePoints {
            get { return curvePoints; }
            private set {
                curvePoints = value;
                OnPropertyChanged("CurvePoints");
            }
        }

        private KeyValuePair<double, double>[] curvePoints;

        /// <summary>
        /// Original data points.
        /// </summary>
        public KeyValuePair<double, double>[] MeasurePoints {
            get { return measurePoints; }
            private set {
                measurePoints = value;
                OnPropertyChanged("MeasurePoints");
            }
        }

        private KeyValuePair<double, double>[] measurePoints;

        private double curveXMinimum;

        /// <summary>
        /// Curve minimum X value.
        /// </summary>
        public double CurveXMinimum {
            get { return curveXMinimum; }
            set {
                curveXMinimum = value;
                OnPropertyChanged("CurveXMinimum");
            }
        }

        private double curveXMaximum;

        /// <summary>
        /// Curve maximum X value.
        /// </summary>
        public double CurveXMaximum {
            get { return curveXMaximum; }
            set {
                curveXMaximum = value;
                OnPropertyChanged("CurveXMaximum");
            }
        }

        private double curveYMinimum;

        /// <summary>
        /// Curve minimum Y value.
        /// </summary>
        public double CurveYMinimum {
            get { return curveYMinimum; }
            private set {
                curveYMinimum = value;
                OnPropertyChanged("CurveYMinimum");
            }
        }

        private double curveYMaximum;

        /// <summary>
        /// Curve maximum Y value.
        /// </summary>
        public double CurveYMaximum {
            get { return curveYMaximum; }
            private set {
                curveYMaximum = value;
                OnPropertyChanged("CurveYMaximum");
            }
        }

        private double curveYValue;

        /// <summary>
        /// Curve selected Y value. Setting this will change the root points and minimum line points.
        /// </summary>
        public double CurveYValue {
            get { return curveYValue; }
            set {
                curveYValue = value;
                calculateInterval();
                OnPropertyChanged("CurveYValue");
            }
        }

        private double selectedXValue;

        /// <summary>
        /// Selected X value on the curve.
        /// </summary>
        public double SelectedXValue {
            get { return selectedXValue; }
            set {
                selectedXValue = value;
                onSelectedXValueChanged();
                OnPropertyChanged("SelectedXValue");
            }
        }

        private double yForSelectedXValue;

        /// <summary>
        /// Y value of the curve corresponding to the selected X value (<see cref="SelectedXValue"/>).
        /// </summary>
        public double YForSelectedXValue {
            get { return yForSelectedXValue; }
            private set {
                yForSelectedXValue = value;
                OnPropertyChanged("YForSelectedXValue");
            }
        }

        public KeyValuePair<double, double>[] SelectedXYValue {
            get {
                return new[] {
                    //new KeyValuePair<double, double>(0, 0), 
                    //new KeyValuePair<double, double>(1, 1), 
                    //new KeyValuePair<double, double>(2, 2), 
                    new KeyValuePair<double, double>(SelectedXValue, YForSelectedXValue)
                };
            }
        }

        public KeyValuePair<double, double>[] ClosestXYValue {
            get {
                if (ClosestSolidToSelection == null)
                    return new KeyValuePair<double, double>[0];
                return new[] {
                    new KeyValuePair<double, double>(ClosestSolidToSelection.SampleDensity.GetFilteredValue(),
                                                     ClosestSolidToSelection.DeltaE),
                };
            }
        }

        /// <summary>
        /// Horizontal line connecting the 2 rootpoints.
        /// </summary>
        public KeyValuePair<double, double>[] MinimumLinePoints {
            get { return minimumLinePoints; }
            private set {
                minimumLinePoints = value;
                OnPropertyChanged("MinimumLinePoints");
            }
        }

        private KeyValuePair<double, double>[] minimumLinePoints;

        /// <summary>
        /// Left vertical rootmarker.
        /// </summary>
        public KeyValuePair<double, double>[] LeftRootPoints {
            get { return leftRootPoints; }
            private set {
                leftRootPoints = value;
                OnPropertyChanged("LeftRootPoints");
            }
        }

        private KeyValuePair<double, double>[] leftRootPoints;

        public double LeftRootX {
            get { return LeftRootPoints.FirstOrDefault().Key; }
        }

        /// <summary>
        /// Right vertical rootmarker.
        /// </summary>
        public KeyValuePair<double, double>[] RightRootPoints {
            get { return rightRootPoints; }
            private set {
                rightRootPoints = value;
                OnPropertyChanged("RightRootPoints");
            }
        }

        private KeyValuePair<double, double>[] rightRootPoints;

        public double RightRootX {
            get { return RightRootPoints.FirstOrDefault().Key; }
        }

        private double minRootY;

        public double MinRootY {
            get { return minRootY; }
            set {
                minRootY = value;
                OnPropertyChanged("MinRootY");
            }
        }

        private double maxRootY;

        public double MaxRootY {
            get { return maxRootY; }
            set {
                maxRootY = value;
                OnPropertyChanged("MaxRootY");
            }
        }

        /// <summary>
        /// Calculate minimum X that will determine the left extend of the plot that is displayed.
        /// </summary>
        public double PlotXMin {
            get {
                var center = (CurveXMinimum + CurveXMaximum) / 2.0;
                var target = center - 0.5;
                var rounded = Math.Round(target, 1);
                return rounded;
            }
        }

        public double PlotXMax {
            get {
                var center = (CurveXMinimum + CurveXMaximum) / 2.0;
                var target = center + 0.5;
                var rounded = Math.Round(target, 1);
                return rounded;
            }
        }

        /// <summary>
        /// Text representing the selected interval.
        /// </summary>
        public string IntervalText {
            get { return intervalText; }
            set {
                intervalText = value;
                OnPropertyChanged("IntervalText");
            }
        }

        private string intervalText;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new printability curve from the given result set.
        /// The solid is used to filter out likewise patches so the final curve will
        /// represent the printability set for the given solid.
        /// </summary>
        /// <param name="solid">The solid to create the curve for.</param>
        /// <param name="result">The compare result to get the data from.</param>
        public PrintabilityCurve(IStripComparePatch solid, IStripCompareResult result) {
            rhoForModel = Config.Config.DefaultConfig.GetDouble("Printability.Defaults.Rho", DefaultRho);
            curveContainer = new CurveContainer();
            this.solid = solid;
            compareResult = result;
            calculateData();
        }
        #endregion

        /// <summary>
        /// Take the solid patch and the data from the compare result and use it to calculate the curve data for the specified solid's family.
        /// </summary>
        private void calculateData() {
            var patchesForSolid = getAssociatedPatchesForSolid(solid, compareResult);
            Solids = new List<IStripComparePatch>(patchesForSolid);
            foreach (var patch in Solids) {
                var deltaE = patch.DeltaE;
                var density = patch.SampleDensity.GetFilteredValue();
                curveContainer.AddPoint(density, deltaE);
            }
            curveContainer.CreateModel(rhoForModel);
            // Note, these have to be called in order!
            initDefaultCurveData();
            calculateGraph();
            setDefaultInterval();
            // calculateInterval();
        }

        /// <summary>
        /// Determine the default interval to use for drawing the bandwidth.
        /// </summary>
        private void setDefaultInterval() {
            var wantedDeltaE = 5.0;
            if (wantedDeltaE > CurveYMaximum)
                wantedDeltaE = curveYMaximum;
            if (wantedDeltaE < curveYMinimum)
                wantedDeltaE = curveYMinimum;
            CurveYValue = wantedDeltaE;
        }

        /// <summary>
        /// Filter the patches in the compare result to get only the patches corresponding to the given solid patch.
        /// </summary>
        /// <param name="forSolid">The solid patch to search for.</param>
        /// <param name="stripCompareResult">The result set to get the data from.</param>
        /// <returns>A list of patches corresponding to the solid.</returns>
        private IEnumerable<IStripComparePatch> getAssociatedPatchesForSolid(IStripComparePatch forSolid,
                                                                             IStripCompareResult stripCompareResult) {
            return stripCompareResult.RawPatches.Patches.Where(p => p.IsSolid && p.SingleSlot == forSolid.SingleSlot);
        }

        private void initDefaultCurveData() {
            curveXMinimum = curveContainer.XIntervalMinimum;
            curveXMaximum = curveContainer.XIntervalMaximum;
            CurveYMinimum = curveContainer.GetCurveValue(curveContainer.XCurveMinimum);
            CurveYMaximum = curveContainer.YMaxPoints;
            CurveYValue = CurveYMinimum;
        }

        /// <summary>
        /// Calculate the points required for drawing the graph (the interpolated points).
        /// </summary>
        private void calculateGraph() {
            var newCurvePoints = new KeyValuePair<double, double>[NrOfPointsForDrawingInterpolatedCurve + 1];
            var newMeasurePoints = new KeyValuePair<double, double>[curveContainer.PointCount];

            var currentInterval = curveContainer.XIntervalMinimum;
            for (var i = 0; i <= NrOfPointsForDrawingInterpolatedCurve; i++) {
                newCurvePoints[i] = new KeyValuePair<double, double>(currentInterval,
                                                                     curveContainer.GetCurveValue(currentInterval));
                currentInterval += (curveContainer.XIntervalMaximum - curveContainer.XIntervalMinimum) /
                                   NrOfPointsForDrawingInterpolatedCurve;
            }
            for (var i = 0; i < curveContainer.PointCount; i++) {
                newMeasurePoints[i] = new KeyValuePair<double, double>(curveContainer.GetXPoint(i),
                                                                       curveContainer.GetYPoint(i));
            }
            CurvePoints = newCurvePoints;
            MeasurePoints = newMeasurePoints;
        }

        /// <summary>
        /// (Re)calculates the bandwidth interval data for the currently set <see cref="CurveYValue"/>.
        /// </summary>
        private void calculateInterval() {
            var leftroot = curveContainer.LeftRoot(CurveYValue);
            var rightroot = curveContainer.RightRoot(CurveYValue);
            var newMinimumLinePoints = new KeyValuePair<double, double>[2];
            var newLeftRootPoints = new KeyValuePair<double, double>[2];
            var newRightRootPoints = new KeyValuePair<double, double>[2];

            newMinimumLinePoints[0] = new KeyValuePair<double, double>(curveContainer.XIntervalMinimum, CurveYValue);
            newMinimumLinePoints[1] = new KeyValuePair<double, double>(curveContainer.XIntervalMaximum, CurveYValue);

            var leftRootY = curveContainer.GetCurveValue(leftroot);
            var rightRootY = curveContainer.GetCurveValue(rightroot);
            newLeftRootPoints[0] = new KeyValuePair<double, double>(leftroot, 0);
            newLeftRootPoints[1] = new KeyValuePair<double, double>(leftroot, leftRootY);
            newRightRootPoints[0] = new KeyValuePair<double, double>(rightroot, 0);
            newRightRootPoints[1] = new KeyValuePair<double, double>(rightroot, rightRootY);

            MinimumLinePoints = newMinimumLinePoints;
            LeftRootPoints = newLeftRootPoints;
            RightRootPoints = newRightRootPoints;
            IntervalText = string.Format("({0:0.00} , {1:0.00})  ({2:0.00} , {3:0.00})", CurveYValue,
                                         rightroot - leftroot, leftroot, rightroot);

            MinRootY = Math.Min(leftRootY, rightRootY);
            MaxRootY = Math.Max(leftRootY, rightRootY);

            /*
            OnPropertyChanged("MinimumLinePoints");
            OnPropertyChanged("LeftRootPoints");
            OnPropertyChanged("LeftRootX");
            OnPropertyChanged("RightRootPoints");
            OnPropertyChanged("RightRootX");
            OnPropertyChanged("MinRootY");
            OnPropertyChanged("MaxRootY");
            OnPropertyChanged("IntervalText");
            */
            OnPropertyChanged(String.Empty);
        }

        /// <summary>
        /// Called when selected x value is changed to set the corresponding y value.
        /// </summary>
        private void onSelectedXValueChanged() {
            if (SelectedXValue < curveContainer.XIntervalMinimum) {
                selectedXValue = curveContainer.XIntervalMinimum;
                OnPropertyChanged("SelectedXValue");
            }
            if (SelectedXValue > curveContainer.XIntervalMaximum) {
                selectedXValue = curveContainer.XIntervalMaximum;
                OnPropertyChanged("SelectedXValue");
            }
            YForSelectedXValue = curveContainer.GetCurveValue(SelectedXValue);
            OnPropertyChanged("SelectedXYValue");

            findClosestSolidToSelection();
        }

        private void findClosestSolidToSelection() {
            var currentDensity = SelectedXValue;
            var closestDensityDiff = 0.0;
            IStripComparePatch found = null;
            foreach (var patch in Solids) {
                var patchDensity = patch.SampleDensity.GetFilteredValue();
                var diff = Math.Abs(currentDensity - patchDensity);
                if (found == null || diff < closestDensityDiff) {
                    closestDensityDiff = diff;
                    found = patch;
                }
            }
            ClosestSolidToSelection = found;
        }

        /// <summary>
        /// Calculate the extends of the points in the curve (bounding box).
        /// </summary>
        /// <returns>
        /// The extends as a rectangle. Note that since the rectangle is
        /// a System.Windows class it returns the top/bottom as what may seem like flipped.
        /// </returns>
        public Rect GetExtends() {
            if (MeasurePoints == null)
                return new Rect();
            double minX = 0, minY = 0, maxX = 0, maxY = 0;
            var firstItteration = true;
            foreach (var measurePoint in MeasurePoints) {
                var x = measurePoint.Key;
                var y = measurePoint.Value;
                if (firstItteration) {
                    firstItteration = false;
                    minX = x;
                    maxX = x;
                    minY = y;
                    maxY = y;
                }
                else {
                    if (x < minX) minX = x;
                    if (x > maxX) maxX = x;
                    if (y < minY) minY = y;
                    if (y > maxY) maxY = y;
                }
            }
            return new Rect(new Point(minX, maxY), new Point(maxX, minY));
        }
    }
}