﻿using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Logging;
using Colorware.Core.Mvvm;

namespace Colorware.Core.Data.Models.Printability {
    public class PrintabilityResultCalculator : DefaultNotifyPropertyChanged {
        #region Properties
        private IStripCompareResult compareResult;

        public IStripCompareResult CompareResult {
            get { return compareResult; }
            set {
                compareResult = value;
                OnPropertyChanged("CompareResult");
                RecalculateData();
            }
        }

        private List<PrintabilityCurve> curves;

        public List<PrintabilityCurve> Curves {
            get { return curves; }
            set {
                curves = value;
                OnPropertyChanged("Curves");
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Initialize the result with empty data.
        /// </summary>
        public PrintabilityResultCalculator()
            : this(null) {
        }

        /// <summary>
        /// Initialize the result with the given compare result.
        /// </summary>
        /// <param name="compareResult">The compare result to initialize with.</param>
        public PrintabilityResultCalculator(IStripCompareResult compareResult) {
            CompareResult = compareResult;
        }
        #endregion

        /// <summary>
        /// Recalculate the data for the current compare result.
        /// </summary>
        public void RecalculateData() {
            if (CompareResult == null) {
                Curves = new List<PrintabilityCurve>();
                return;
            }
            var solids = getSolids(CompareResult);
            Curves = solids.Select(createCurve).Where(c => c != null).ToList();
        }

        private PrintabilityCurve createCurve(IStripComparePatch solid) {
            try {
                return new PrintabilityCurve(solid, CompareResult);
            }
            catch (Exception e) {
                LogManager.GetLogger(GetType()).Error("Could not calculate curve for " + solid.Name, e);
                return null;
            }
        }

        /// <summary>
        /// Grab all the solid from the given compare result.
        /// </summary>
        /// <param name="fromCompareResult">The compare result to get the solids from.</param>
        /// <returns>All the solids from the compare result (take from the summary patches).</returns>
        private static IEnumerable<IStripComparePatch> getSolids(IStripCompareResult fromCompareResult) {
            return fromCompareResult.SummaryPatches.Patches.Where(p => p.IsSolid);
        }

        /// <summary>
        /// Find the curve that belongs to the given reference patch id and return it.
        /// </summary>
        /// <param name="referencePatchId">The id of the reference patch.</param>
        public PrintabilityCurve GetCurveForReferenceId(long referencePatchId) {
            return Curves.FirstOrDefault(c => c.Solid.ReferencePatchId == referencePatchId);
        }
    }
}