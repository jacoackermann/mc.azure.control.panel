using Colorware.Core.Color;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models {
    public interface IMeasurementConditionsConfig : IBaseModel {
        bool IsStandard { get; set; }

        string Name { get; set; }

        DensityMethod DensityStandard { get; set; }

        /// <summary>
        /// Dotgain method to use for process colors.
        /// </summary>
        DotgainMethod DotgainMethodProcess { get; set; }

        /// <summary>
        /// Dotgain method to use for spot colors.
        /// </summary>
        DotgainMethod DotgainMethodSpot { get; set; }

        WhiteBase DensityWhiteBase { get; set; }

        Illuminant Illuminant { get; set; }

        Observer Observer { get; set; }

        MCondition DensityMCondition { get; set; }

        MCondition SpectralMCondition { get; set; }

        string ObserverName { get; }

        string ToLogString();
    }

    public static class IMeasurementConditionsConfigExtensions {
        [NotNull]
        public static MeasurementConditions GetMeasurementConditions(
            [NotNull] this IMeasurementConditionsConfig measurementConditionsConfig) {
            return new MeasurementConditions(measurementConditionsConfig.Illuminant,
                                             measurementConditionsConfig.Observer,
                                             measurementConditionsConfig.DensityMCondition,
                                             measurementConditionsConfig.SpectralMCondition);
        }

        public static DotgainMethod DotgainMethodForReference(this IMeasurementConditionsConfig mc, IReferencePatch patch) {
            return patch.IsSpot
                       ? mc.DotgainMethodSpot
                       : mc.DotgainMethodProcess;
        }
    }
}