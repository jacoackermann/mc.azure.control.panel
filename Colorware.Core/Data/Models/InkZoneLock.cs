﻿using System.Collections.Generic;

namespace Colorware.Core.Data.Models {
    /// <summary>
    /// Represents the lock status of all inkzones for a specific slot.
    /// The LockedZones property will be kept sorted by inkzone.
    /// </summary>
    public class InkZoneLock {
        public int Slot { get; private set; }
        public List<int> LockedZones;

        public InkZoneLock(int slot) {
            Slot = slot;
            LockedZones = new List<int>();
        }

        public InkZoneLock(int slot, IEnumerable<int> lockedZones) {
            Slot = slot;
            LockedZones = new List<int>(lockedZones);
            LockedZones.Sort();
        }

        public void AddLock(int zone) {
            LockedZones.Add(zone);
            LockedZones.Sort();
        }

        public void RemoveLock(int zone) {
            LockedZones.Remove(zone);
        }

        public bool IsLocked(int zone) {
            return LockedZones.Contains(zone);
        }
    }
}