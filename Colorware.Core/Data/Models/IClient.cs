﻿using System;

using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IClient : IBaseModel, IExternalGuid {
        

        String Name { get; set; }

        String Notes { get; set; }

        String CustomFieldName1 { get; set; }

        String CustomFieldName2 { get; set; }

        String CustomFieldName3 { get; set; }

        String CustomFieldName4 { get; set; }

        String CustomFieldName5 { get; set; }

        String CustomFieldName6 { get; set; }

        bool HasCustomFieldName1 { get; }
        bool HasCustomFieldName2 { get; }
        bool HasCustomFieldName3 { get; }
        bool HasCustomFieldName4 { get; }
        bool HasCustomFieldName5 { get; }
        bool HasCustomFieldName6 { get; }
        bool HasCustomFields { get; }
        IHasManyCollection<IJob> Jobs { get; }
    }
}