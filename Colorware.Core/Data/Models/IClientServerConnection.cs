﻿namespace Colorware.Core.Data.Models {
    public interface IClientServerConnection {
        long ServerId { get; set; }
        long ClientId { get; set; }
        long SendMethod { get; set; }
    }
}