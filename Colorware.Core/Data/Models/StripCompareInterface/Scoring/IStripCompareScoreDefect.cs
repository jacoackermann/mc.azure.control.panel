namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring {
    public interface IStripCompareScoreDefect {
        /// <summary>
        /// Name of the defect.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Description of the defect.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Patch the defect belongs to. May be null if this is a general defect.
        /// </summary>
        IStripCompareScoreEntry Entry { get; }
    }
}