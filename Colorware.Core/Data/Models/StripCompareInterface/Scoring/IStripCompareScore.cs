using System.Collections.Generic;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring {
    /// <summary>
    /// Overarching class containing all scoring information for a given strip compare result.
    /// </summary>
    public interface IStripCompareScore {
        /// <summary>
        /// Return the final score for the result.
        /// </summary>
        double FinalScore { get; }

        /// <summary>
        /// Return all results as categories. May be null if scoring system doesn't support categories.
        /// </summary>
        [NotNull]
        IEnumerable<IStripCompareScoreCategory> Categories { get; }

        /// <summary>
        /// Return a list of all scored patch items. This does not have to correspond to the actual patch list.
        /// </summary>
        [NotNull]
        IEnumerable<IStripCompareScoreEntry> PatchResults { get; }
    }
}