using System.Collections.Generic;

using Colorware.Core.Data.Models.StripCompareInterface.Scoring.Base;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring {
    public class StripCompareScoreEntry : IStripCompareScoreEntry {
        private readonly IStripComparePatch patch;
        private readonly double score;

        #region Implementation of IStripCompareScorePatch
        public IStripComparePatch Patch {
            get { return patch; }
        }

        public double Score {
            get { return score; }
        }

        public StripCompareScoreResult ScoreResult {
            get { return Patch.InTolerance ? StripCompareScoreResult.Pass : StripCompareScoreResult.Fail; }
        }

        public string ResultGrade {
            get { return Patch.InTolerance ? "OK" : "Fail"; }
        }

        private List<IStripCompareScoreDefect> defects;

        public IEnumerable<IStripCompareScoreDefect> Defects {
            get { return defects ?? (defects = new List<IStripCompareScoreDefect>()); }
        }
        #endregion

        public StripCompareScoreEntry(IStripComparePatch patch) {
            this.patch = patch;
            score = 0;
        }

        public void AddDefect(StripCompareScoreDefect defect) {
            ((List<IStripCompareScoreDefect>)Defects).Add(defect);
        }
    }
}