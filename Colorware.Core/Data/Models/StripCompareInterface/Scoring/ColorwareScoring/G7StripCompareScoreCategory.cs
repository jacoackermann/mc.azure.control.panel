﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Colorware.Core.Data.Models.StripCompareInterface.Scoring.Base;
using Colorware.Core.Globalization;
using System.Collections.Immutable;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring {
    public class G7StripCompareScoreCategory : IStripCompareScoreCategory {
        private readonly StripCompareSummaryCategory compareSummaryCategory;

        public G7StripCompareScoreCategory(StripCompareSummaryCategory compareSummaryCategory,
                                           double scoreForError) {
            this.compareSummaryCategory = compareSummaryCategory;
            ScoreForError = scoreForError;
        }

        public string Name { get; set; }

        /// <summary>
        /// Indicates the score to use when an erronous situation is detected.
        /// </summary>
        public double ScoreForError { get; }

        private double? score;

        public double Score {
            private set { score = value; }
            get {
                if (!score.HasValue)
                    calculateData();
                Debug.Assert(score.HasValue);
                return score.Value;
            }
        }

        public int Identifier => (int)SummaryTypes.Opacity;

        private StripCompareScoreResult? result;

        public StripCompareScoreResult Result {
            private set { result = value; }
            get {
                if (!result.HasValue)
                    calculateData();
                Debug.Assert(result.HasValue);
                return result.Value;
            }
        }

        public bool IsOk => Result == StripCompareScoreResult.Pass;
        public bool IsWarning => Result == StripCompareScoreResult.Warning;
        public bool IsFail => Result == StripCompareScoreResult.Fail;


        private ImmutableList<StripCompareScoreDefect> defects;

        public ImmutableList<StripCompareScoreDefect> Defects {
            private set { defects = value; }
            get {
                if (defects == null)
                    calculateData();
                return defects;
            }
        }

        public IEnumerable<IStripCompareScoreEntry> Entries { get; private set; }

        public void CalculateScore() {
            throw new System.NotImplementedException();
        }

        private void calculateData() {
            Score = calculateScore();
            Result = calculateResult();
            defects = calculateReasons();
        }

        private double calculateScore() {
            if (compareSummaryCategory.IsOk) {
                return 0;
            }
            return ScoreForError;
        }

        private StripCompareScoreResult calculateResult() {
            if (!compareSummaryCategory.HasResult)
                compareSummaryCategory.CalculateResult();
            return compareSummaryCategory.Result;
        }

        private ImmutableList<StripCompareScoreDefect> calculateReasons() {
            // Meh... Should find a better way to do this, but due to recent changes we should probably rewrite the scoring
            // system anyway.
            var g7Category = compareSummaryCategory as G7StripCompareSummaryCategory;
            if (g7Category == null)
                throw new InvalidOperationException(
                    "The instance of the passed in category should be of type G7StripCompareSummaryCategory");

            var reasons = new List<StripCompareScoreDefect>();

            // Defects that stop G7 Scoring
            if (!g7Category.HasSubstratePatch)
                reasons.Add(new StripCompareScoreDefect("",
                                                        LanguageManager._("CW.Data.CompareScoreCategory.G7.NoSubstrate")));
            if (!g7Category.HasOverprintCMYPatch)
                reasons.Add(new StripCompareScoreDefect("",
                                                        LanguageManager._(
                                                            "CW.Data.CompareScoreCategory.G7.NoCMYOverprint")));
            if (!g7Category.HasSolidKPatch)
                reasons.Add(new StripCompareScoreDefect("", LanguageManager._("CW.Data.CompareScoreCategory.G7.NoSolidK")));

            // Regular defects for G7 Scoring
            if (g7Category.HasSubstratePatch && g7Category.HasOverprintCMYPatch && g7Category.HasSolidKPatch) {
                if (g7Category.AverageWeightedDeltaLOutsideTolerance)
                    reasons.Add(new StripCompareScoreDefect("",
                                                            LanguageManager._(
                                                                "CW.Data.CompareScoreCategory.G7.AverageWeightedDeltaL")));
                if (g7Category.AverageWeightedDeltaChOutsideTolerance)
                    reasons.Add(new StripCompareScoreDefect("",
                                                            LanguageManager._(
                                                                "CW.Data.CompareScoreCategory.G7.AverageWeightedDeltaCh")));
                foreach (var patch in g7Category.PatchesOutsideWeightedDeltaLTolerance) {
                    var patchName = patch.Name;
                    if (patch.IsDotgain)
                        patchName = String.Format("{0} ({1})", patchName, patch.Tint);
                    reasons.Add(new StripCompareScoreDefect(patchName,
                                                            String.Format(
                                                                LanguageManager._(
                                                                    "CW.Data.CompareScoreCategory.G7.PeakWeightedDeltaL"),
                                                                patchName)));
                }
                foreach (var patch in g7Category.PatchesOutsideWeightedDeltaChTolerance) {
                    reasons.Add(new StripCompareScoreDefect(patch.Name,
                                                            String.Format(
                                                                LanguageManager._(
                                                                    "CW.Data.CompareScoreCategory.G7.PeakWeightedDeltaCh"),
                                                                patch.Name)));
                }
            }

            return reasons.ToImmutableList();
        }
    }
}