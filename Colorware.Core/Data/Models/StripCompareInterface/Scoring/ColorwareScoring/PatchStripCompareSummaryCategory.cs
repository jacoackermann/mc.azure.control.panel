﻿using System;
using System.Linq;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring {
    public class PatchStripCompareSummaryCategory : StripCompareSummaryCategory {
        public PatchStripCompareSummaryCategory(SummaryTypes summaryType) : base(summaryType) {
        }

        override public void AddPatch(IStripComparePatch patch) {
            if (!SummaryTypeHelper.PatchMatchesSummaryType(patch, SummaryType)) {
                throw new ArgumentException("Patchtype of passed patch does not match patchtype of this category",
                    nameof(patch));
            }
            if (!patch.HasTolerance)
                return;

            if (patch.InTolerance)
                PatchesInTolerance.Add(patch);
            else
                PatchesOutsideTolerance.Add(patch);
        }
        override public void CalculateResult() {
            var newResult = StripCompareScoreResult.Pass;
            if (PatchesOutsideTolerance.Any()) {
                newResult = StripCompareScoreResult.Fail; // Any patch not in tolerance means an instant fail.
            }
            else if(PatchesInTolerance.Any(patch => patch.NumberOfPatchesInTolerance < patch.NumberOfPatches)) {
                newResult = StripCompareScoreResult.Warning; // Stil `in tolerance' but not all patches are OK.
            }
            HasResult = true;
            Result = newResult;
        }

    }
}