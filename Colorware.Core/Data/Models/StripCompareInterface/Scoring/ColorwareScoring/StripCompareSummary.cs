using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

using Colorware.Core.Extensions;

using Colorware.Core.Mvvm;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring {
    /// <summary>
    /// Provides a summary of patchtypes, indicating if the patches are inside or outside of tolerance.
    /// Each patchtype is represented by an instance of StripCompareSummaryCategory.
    /// </summary>
    public class StripCompareSummary : DefaultNotifyPropertyChanged {
        #region Properties
        public IStripCompareResult CompareResult { get; }

        public bool UseG7Scoring { get; private set; }
        #endregion

        #region Calculated properties
        [NotNull]
        public Dictionary<SummaryTypes, StripCompareSummaryCategory> Categories { get; private set; }

        public StripCompareSummaryCategory Solids => GetCategoryForSummaryType(SummaryTypes.Solid);

        public StripCompareSummaryCategory Dotgain => GetCategoryForSummaryType(SummaryTypes.Dotgain);

        // Possible problem here. Balance, Shadow, Midtone, Highlight?
        public StripCompareSummaryCategory Balance => GetCategoryForSummaryType(SummaryTypes.Balance);

        public StripCompareSummaryCategory Other => GetCategoryForSummaryType(SummaryTypes.Other);

        public StripCompareSummaryCategory Paperwhite => GetCategoryForSummaryType(SummaryTypes.Paperwhite);

        public StripCompareSummaryCategory Overprint => GetCategoryForSummaryType(SummaryTypes.Overprint);

        public StripCompareSummaryCategory G7 => GetCategoryForSummaryType(SummaryTypes.G7);

        public StripCompareSummaryCategory Max => GetCategoryForSummaryType(SummaryTypes.Max);

        public StripCompareSummaryCategory Average => GetCategoryForSummaryType(SummaryTypes.Average);

        /// <summary>
        /// Determine the "IsOk" state of `minor categories'. These are all categories
        /// except for solids, dotgain and balance.
        /// </summary>
        private StripCompareScoreResult? minorCategoriesResult;

        public StripCompareScoreResult MinorCategoriesResult {
            private set {
                minorCategoriesResult = value;
                OnPropertyChanged("MinorCategoriesResult");
                OnPropertyChanged("MinorCategoriesAreOk");
                OnPropertyChanged("MinorCategoriesAreWarning");
                OnPropertyChanged("MinorCategoriesAreFail");
            }
            get {
                if (!minorCategoriesResult.HasValue)
                    minorCategoriesResult = calculateMinorCategories();
                return minorCategoriesResult.Value;
            }
        }

        private StripCompareScoreResult calculateMinorCategories() {
            var current = StripCompareScoreResult.Pass;
            Categories.Values.Where(category => category.IsMinor)
                      .ForEach(category => {
                          if (category.Result < current)
                              current = category.Result;
                      });
            return current;
        }

        public bool MinorCategoriesAreOk => MinorCategoriesResult == StripCompareScoreResult.Pass;

        public bool MinorCategoriesAreWarning => MinorCategoriesResult == StripCompareScoreResult.Warning;

        public bool MinorCategoriesAreFail => MinorCategoriesResult == StripCompareScoreResult.Fail;
        #endregion

        public StripCompareSummary([NotNull] IStripCompareResult compareResult) {
            if (compareResult == null) throw new ArgumentNullException(nameof(compareResult));

            CompareResult = compareResult;

            calculateSummary();

            Categories.Values.Where(category => category.IsMinor)
                .ForEach(category => category.PropertyChanged += onUpdateMinorCategories);
        }

        private void onUpdateMinorCategories(object sender, PropertyChangedEventArgs e) {
            MinorCategoriesResult = calculateMinorCategories();
        }

        private void calculateSummary() {
            Categories = new Dictionary<SummaryTypes, StripCompareSummaryCategory>();
            foreach (var patch in CompareResult.SummaryPatches.Patches) {
                var summaryTypeKey = SummaryTypeHelper.GetSummaryTypeForPatch(patch);
                if (!Categories.ContainsKey(summaryTypeKey))
                    Categories[summaryTypeKey] = new PatchStripCompareSummaryCategory(summaryTypeKey);

                Categories[summaryTypeKey].AddPatch(patch);
            }

            calculateMaxCategory();
            calculateAverageCategory();
            calculateOpacityCategory();
            calculateG7Category();

            if (UseG7Scoring) {
                if (Solids != null)
                    Solids.IsMinor = false;
                if (G7 != null)
                    G7.IsMinor = false;
            }
            else {
                if (Solids != null)
                    Solids.IsMinor = false;
                if (Balance != null)
                    Balance.IsMinor = false;
                if (Dotgain != null)
                    Dotgain.IsMinor = false;
            }
        }

        private void calculateMaxCategory() {
            if (!CompareResult.ToleranceSet.MaxCalculated)
                return;

            var maxPatches = SummaryTypeHelper.FilterPatchesForMax(CompareResult.RawPatches.Patches,
                                                                   CompareResult.ToleranceSet);
            var maxCategory = new MaxStripCompareSummaryCategory(SummaryTypes.Max,
                                                                 CompareResult.ToleranceSet.MaxTolerance);
            foreach (var patch in maxPatches)
                maxCategory.AddPatch(patch);

            maxCategory.CalculateResult();
            Categories[SummaryTypes.Max] = maxCategory;
        }

        private void calculateAverageCategory() {
            if (!CompareResult.ToleranceSet.AverageCalculated)
                return;

            var averagePatches = SummaryTypeHelper.FilterPatchesForAverage(CompareResult.RawPatches.Patches,
                                                                           CompareResult.ToleranceSet);
            var averageCategory = new AverageStripCompareSummaryCategory(SummaryTypes.Average,
                                                                         CompareResult.ToleranceSet.AverageTolerance);
            foreach (var patch in averagePatches)
                averageCategory.AddPatch(patch);

            averageCategory.CalculateResult();
            Categories[SummaryTypes.Average] = averageCategory;
        }

        private void calculateOpacityCategory() {
            if (!CompareResult.ToleranceSet.OpacityMeasured || !CompareResult.Measurement.OpacityResults.Any())
                return;

            var opacityCategory = new OpacityStripCompareSummaryCategory(SummaryTypes.Opacity,
                                                                         CompareResult.ToleranceSet.OpacityTarget);
            opacityCategory.SetOpacityResults(CompareResult.Measurement.OpacityResults);

            opacityCategory.CalculateResult();
            Categories[SummaryTypes.Opacity] = opacityCategory;
        }

        private void calculateG7Category() {
            UseG7Scoring = CompareResult.ToleranceSet.G7Calculated;
            if (!UseG7Scoring)
                return;

            var set = CompareResult.ToleranceSet;
            var g7Category = new G7StripCompareSummaryCategory(SummaryTypes.G7, CompareResult.SummaryPatches,
                                                               set.G7WeightedDeltaLAverage, set.G7WeightedDeltaLPeak,
                                                               set.G7WeightedDeltaChAverage, set.G7WeightedDeltaChPeak);
            foreach (var patch in CompareResult.SummaryPatches.Patches)
                g7Category.AddPatch(patch);

            g7Category.CalculateResult();
            Categories[SummaryTypes.G7] = g7Category;
        }

        /// <summary>
        /// Returns the category belonging to the passed patchType.
        /// If this category does not exist, null is returned.
        /// </summary>
        /// <param name="patchType">The patchtype to retrieve the category for.</param>
        /// <returns>The StripCompareSummaryCategory if it exists, else null.</returns>
        [CanBeNull]
        public StripCompareSummaryCategory GetCategoryForSummaryType(SummaryTypes patchType) {
            StripCompareSummaryCategory item;
            if (Categories.TryGetValue(patchType, out item))
                return item;

            return null;
        }
    }
}