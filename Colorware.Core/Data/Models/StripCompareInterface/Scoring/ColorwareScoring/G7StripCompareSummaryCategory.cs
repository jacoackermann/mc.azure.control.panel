﻿using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Data.Models.G7Analysis;
using Colorware.Core.Enums;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring {
    public class G7StripCompareSummaryCategory : StripCompareSummaryCategory {
        private double averageWeightedDeltaL = 0.0;
        private int numWeightedDeltaLPatches = 0;
        private double averageWeightedDeltaCh = 0.0;
        private int numWeightedDeltaChPatches = 0;

        private readonly double averageWeightedDeltaLTolerance;
        private readonly double peakWeightedDeltaLTolerance;
        private readonly double averageWeightedDeltaChTolerance;
        private readonly double peakWeightedDeltaChTolerance;

        private readonly IStripComparePatch substrate;
        private readonly IStripComparePatch overprintCMY;
        private readonly IStripComparePatch solidK;

        public bool HasSubstratePatch => substrate != null;
        public bool HasOverprintCMYPatch => overprintCMY != null;
        public bool HasSolidKPatch => solidK != null;

        public bool AverageWeightedDeltaLOutsideTolerance => averageWeightedDeltaL > averageWeightedDeltaLTolerance;
        public bool AverageWeightedDeltaChOutsideTolerance => averageWeightedDeltaCh > averageWeightedDeltaChTolerance;
        public List<IStripComparePatch> PatchesOutsideWeightedDeltaLTolerance { get; } = new List<IStripComparePatch>();
        public List<IStripComparePatch> PatchesOutsideWeightedDeltaChTolerance { get; } = new List<IStripComparePatch>();

        public G7StripCompareSummaryCategory(SummaryTypes summaryType, IStripComparePatches summaryPatches,
                                             double averageWeightedDeltaLTolerance, double peakWeightedDeltaLTolerance,
                                             double averageWeightedDeltaChTolerance, double peakWeightedDeltaChTolerance)
            : base(summaryType) {
            this.substrate = summaryPatches.Patches.FirstOrDefault(p => p.IsPaperWhite);
            this.overprintCMY =
                summaryPatches.Patches.FirstOrDefault(p => p.Cyan == 100 && p.Magenta == 100 && p.Yellow == 100);
            this.solidK = summaryPatches.SolidPatches.FirstOrDefault(p => p.Key == 100);
            this.averageWeightedDeltaLTolerance = averageWeightedDeltaLTolerance;
            this.peakWeightedDeltaLTolerance = peakWeightedDeltaLTolerance;
            this.averageWeightedDeltaChTolerance = averageWeightedDeltaChTolerance;
            this.peakWeightedDeltaChTolerance = peakWeightedDeltaChTolerance;
        }

        public override void AddPatch(IStripComparePatch patch) {
            // If we cannot compute G7, stop here
            if (!(HasSubstratePatch && HasOverprintCMYPatch && HasSolidKPatch))
                return;

            // K Weighted Delta-L contribution
            if (patch.IsDotgain && patch.Key > 0.0) {
                // Calculate and add Weighted Delta-L contribution
                double weightedDeltaL = G7Calculations.WeightedDeltaLK(G7Patch.FromStripComparePatch(patch), substrate.Sample, solidK.Sample);
                averageWeightedDeltaL += weightedDeltaL;
                ++numWeightedDeltaLPatches;
                if (weightedDeltaL > peakWeightedDeltaLTolerance)
                    PatchesOutsideWeightedDeltaLTolerance.Add(patch);
            }
            // CMY Weighted Delta-L/Delta-Ch contribution
            if ((patch.IsBalanceHighlight || patch.IsBalanceMidtone || patch.IsBalanceShadow ||
                 patch.PatchType == PatchTypes.Balance || patch.PatchType == PatchTypes.Other) &&
                G7Calculations.IsPatchGrayBalance(patch)) {

                // Calculate and add Weighted Delta-L contribution
                double weightedDeltaL = G7Calculations.WeightedDeltaL3(G7Patch.FromStripComparePatch(patch), substrate.Sample, overprintCMY.Sample);
                averageWeightedDeltaL += weightedDeltaL;
                ++numWeightedDeltaLPatches;
                if (weightedDeltaL > peakWeightedDeltaLTolerance)
                    PatchesOutsideWeightedDeltaLTolerance.Add(patch);

                // Calculate and add Weighted Delta-Ch contribution
                double weightedDeltaCh = G7Calculations.WeightedDeltaCh(G7Patch.FromStripComparePatch(patch), substrate.Sample, overprintCMY.Sample);
                averageWeightedDeltaCh += weightedDeltaCh;
                ++numWeightedDeltaChPatches;
                if (weightedDeltaCh > peakWeightedDeltaChTolerance)
                    PatchesOutsideWeightedDeltaChTolerance.Add(patch);
            }
        }

        public override void CalculateResult() {
            // If we cannot compute G7, stop here
            if (!(HasSubstratePatch && HasOverprintCMYPatch && HasSolidKPatch)) {
                HasResult = true;
                Result = StripCompareScoreResult.Fail;
                return;
            }

            var newResult = StripCompareScoreResult.Pass;
            averageWeightedDeltaL /= numWeightedDeltaLPatches;
            averageWeightedDeltaCh /= numWeightedDeltaChPatches;
            if (averageWeightedDeltaL > averageWeightedDeltaLTolerance ||
                averageWeightedDeltaCh > averageWeightedDeltaChTolerance ||
                PatchesOutsideWeightedDeltaLTolerance.Any() ||
                PatchesOutsideWeightedDeltaChTolerance.Any()) {
                newResult = StripCompareScoreResult.Fail;
            }
            HasResult = true;
            Result = newResult;
        }

    }
}