﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring {
    public class AverageStripCompareSummaryCategory : StripCompareSummaryCategory {
        private readonly double toleratedAverageDeltaE;

        public double AverageDeltaE { get; private set; }

        public IEnumerable<IStripComparePatch> PatchesOverAverage {
            get {
                return PatchesInTolerance.Where(p => p.DeltaE >= toleratedAverageDeltaE);
            }
        } 

        public AverageStripCompareSummaryCategory(SummaryTypes summaryType, double toleratedAverageDeltaE) : base(summaryType) {
            this.toleratedAverageDeltaE = toleratedAverageDeltaE;
        }

        public override void AddPatch(IStripComparePatch patch) {
            if (patch == null) throw new ArgumentNullException(nameof(patch));

            if (!patch.HasTolerance)
                return;

            PatchesInTolerance.Add(patch);
        }

        public override void CalculateResult() {
            if (!PatchesInTolerance.Any()) {
                Result = StripCompareScoreResult.Pass;
                return;
            }

            AverageDeltaE = PatchesInTolerance.Average(patch => patch.DeltaE);
            if (AverageDeltaE >= toleratedAverageDeltaE) {
                Result = StripCompareScoreResult.Fail;
            }
            else {
                Result = StripCompareScoreResult.Pass;
            }
            HasResult = true;
        }
    }
}