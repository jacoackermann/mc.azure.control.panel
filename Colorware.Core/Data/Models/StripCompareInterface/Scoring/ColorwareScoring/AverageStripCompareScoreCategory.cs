﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Colorware.Core.Data.Models.StripCompareInterface.Scoring.Base;
using Colorware.Core.Globalization;
using Colorware.Core.Mvvm.AntiMemoryLeaks;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring {
    public class AverageStripCompareScoreCategory : IStripCompareScoreCategory {
        private readonly StripCompareSummaryCategory compareSummaryCategory;

        public AverageStripCompareScoreCategory(StripCompareSummaryCategory compareSummaryCategory,
                                                double scoreForError) {
            this.compareSummaryCategory = compareSummaryCategory;
            ScoreForError = scoreForError;
        }

        public string Name { get; set; }

        /// <summary>
        /// Indicates the score to use when an erronous situation is detected.
        /// </summary>
        public double ScoreForError { get; }

        private double? score;

        public double Score {
            private set { score = value; }
            get {
                if (!score.HasValue)
                    calculateData();
                Debug.Assert(score.HasValue);
                return score.Value;
            }
        }

        public int Identifier => (int)SummaryTypes.Average;

        private StripCompareScoreResult? result;

        public StripCompareScoreResult Result {
            private set { result = value; }
            get {
                if (!result.HasValue)
                    calculateData();
                Debug.Assert(result.HasValue);
                return result.Value;
            }
        }

        public bool IsOk => Result == StripCompareScoreResult.Pass;
        public bool IsWarning => Result == StripCompareScoreResult.Warning;
        public bool IsFail => Result == StripCompareScoreResult.Fail;


        private IReadOnlyList<StripCompareScoreDefect> defects;

        public IReadOnlyList<StripCompareScoreDefect> Defects {
            private set { defects = value; }
            get {
                if (defects == null)
                    calculateData();
                return defects;
            }
        }

        public IEnumerable<IStripCompareScoreEntry> Entries { get; private set; }

        public void CalculateScore() {
            throw new System.NotImplementedException();
        }

        private void calculateData() {
            Score = calculateScore();
            Result = calculateResult();
            Defects = calculateReasons().ToNonLeakyList();
        }

        private double calculateScore() {
            if (compareSummaryCategory.IsOk) {
                return 0;
            }
            return ScoreForError;
        }

        private StripCompareScoreResult calculateResult() {
            if (!compareSummaryCategory.HasResult)
                compareSummaryCategory.CalculateResult();
            return compareSummaryCategory.Result;
        }

        private IEnumerable<StripCompareScoreDefect> calculateReasons() {
            // Meh... Should find a better way to do this, but due to recent changes we should probably rewrite the scoring
            // system anyway.
            var averageCategory = compareSummaryCategory as AverageStripCompareSummaryCategory;
            if (averageCategory == null)
                throw new InvalidOperationException(
                    "The instance of the passed in category should be of type AverageStripCompareSummaryCategory");
            return averageCategory.PatchesOverAverage
                                  .OrderBy(p => p.InkZone)
                                  .Select(p => new StripCompareScoreDefect(
                                                   p.Name,
                                                   LanguageManager._F("CW.Data.CompareScoreCategory.AverageExceeded",
                                                                      p.Name,
                                                                      p.Tint, p.InkZone),
                                                   new StripCompareScoreEntry(p)));
        }
    }
}