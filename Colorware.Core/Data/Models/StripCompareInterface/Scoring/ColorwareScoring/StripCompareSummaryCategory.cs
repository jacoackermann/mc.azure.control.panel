using System.Collections.Generic;

using Colorware.Core.Mvvm;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring {
    /// <summary>
    /// Provides a quality summary for a single patchtype (or category).
    /// </summary>
    public abstract class StripCompareSummaryCategory : DefaultNotifyPropertyChanged {
        #region Properties
        public List<IStripComparePatch> PatchesInTolerance { get; } = new List<IStripComparePatch>();
        public List<IStripComparePatch> PatchesOutsideTolerance { get; } = new List<IStripComparePatch>();

        public SummaryTypes SummaryType { get; }

        private bool isMinor;

        public bool IsMinor {
            get { return isMinor; }
            set {
                isMinor = value;
                OnPropertyChanged("IsMinor");
            }
        }

        public bool IsOk => Result == StripCompareScoreResult.Pass;

        public bool IsWarning => Result == StripCompareScoreResult.Warning;

        public bool IsFail => Result == StripCompareScoreResult.Fail;

        public bool ShowOk => IsOk && HasResult;

        public bool ShowWarning => IsWarning && HasResult;

        public bool ShowFail => IsFail && HasResult;

        private bool hasResult;

        public bool HasResult {
            get { return hasResult; }
            protected set {
                hasResult = value;
                OnPropertyChanged("HasResult");
            }
        }

        private StripCompareScoreResult? result;

        public StripCompareScoreResult Result {
            protected set {
                result = value;
                OnPropertyChanged("Result");
                OnPropertyChanged("IsOk");
                OnPropertyChanged("IsWarning");
                OnPropertyChanged("IsFail");
                OnPropertyChanged("ShowOk");
                OnPropertyChanged("ShowWarning");
                OnPropertyChanged("ShowFail");
            }
            get {
                if (!result.HasValue)
                    return StripCompareScoreResult.Unknown;
                return result.Value;
            }
        }

        public int PatchCount => PatchesInTolerance.Count + PatchesOutsideTolerance.Count;
        #endregion

        public StripCompareSummaryCategory(SummaryTypes summaryType) {
            SummaryType = summaryType;
            IsMinor = true;
        }

        /// <summary>
        /// Add a new patch to be considered. Generally the patches added should
        /// be of the same patchtype for the summary to make sense, so an exception will
        /// be raised if this is not the case.
        /// </summary>
        /// <param name="patch">The IStripComparePatch to add for consideration.</param>
        public abstract void AddPatch([NotNull] IStripComparePatch patch);

        public abstract void CalculateResult();

        /// <summary>
        /// Runs past all patches assigned to this category and counts how many are outside of tolerance.
        /// </summary>
        /// <returns>Number of patches outside of tolerance</returns>
        public int CalculateNumberOfFaultyPatches() {
            return PatchesOutsideTolerance.Count;
        }
    }
}