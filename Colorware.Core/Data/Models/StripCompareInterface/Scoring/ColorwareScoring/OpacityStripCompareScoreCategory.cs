﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Colorware.Core.Data.Models.StripCompareInterface.Scoring.Base;
using Colorware.Core.Globalization;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring {
    public class OpacityStripCompareScoreCategory : IStripCompareScoreCategory {
        private readonly StripCompareSummaryCategory compareSummaryCategory;

        public OpacityStripCompareScoreCategory(StripCompareSummaryCategory compareSummaryCategory,
                                                double scoreForError) {
            this.compareSummaryCategory = compareSummaryCategory;
            ScoreForError = scoreForError;
        }

        public string Name { get; set; }

        /// <summary>
        /// Indicates the score to use when an erronous situation is detected.
        /// </summary>
        public double ScoreForError { get; }

        private double? score;

        public double Score {
            private set { score = value; }
            get {
                if (!score.HasValue)
                    calculateData();
                Debug.Assert(score.HasValue);
                return score.Value;
            }
        }

        public int Identifier => (int)SummaryTypes.Opacity;

        private StripCompareScoreResult? result;

        public StripCompareScoreResult Result {
            private set { result = value; }
            get {
                if (!result.HasValue)
                    calculateData();
                Debug.Assert(result.HasValue);
                return result.Value;
            }
        }

        public bool IsOk => Result == StripCompareScoreResult.Pass;
        public bool IsWarning => Result == StripCompareScoreResult.Warning;
        public bool IsFail => Result == StripCompareScoreResult.Fail;


        private IEnumerable<StripCompareScoreDefect> defects;

        public IEnumerable<StripCompareScoreDefect> Defects {
            private set { defects = value; }
            get {
                if (defects == null)
                    calculateData();
                return defects;
            }
        }

        public IEnumerable<IStripCompareScoreEntry> Entries { get; private set; }

        public void CalculateScore() {
            throw new System.NotImplementedException();
        }

        private void calculateData() {
            Score = calculateScore();
            Result = calculateResult();
            Defects = calculateReasons();
        }

        private double calculateScore() {
            if (compareSummaryCategory.IsOk) {
                return 0;
            }
            return ScoreForError;
        }

        private StripCompareScoreResult calculateResult() {
            if (!compareSummaryCategory.HasResult)
                compareSummaryCategory.CalculateResult();
            return compareSummaryCategory.Result;
        }

        private IEnumerable<StripCompareScoreDefect> calculateReasons() {
            // Meh... Should find a better way to do this, but due to recent changes we should probably rewrite the scoring
            // system anyway.
            var opacityCategory = compareSummaryCategory as OpacityStripCompareSummaryCategory;
            if (opacityCategory == null)
                throw new InvalidOperationException(
                    "The instance of the passed in category should be of type OpacityStripCompareSummaryCategory");

            return new List<StripCompareScoreDefect> {
                new StripCompareScoreDefect("", LanguageManager._("CW.Data.CompareScoreCategory.OpacityTooLow"))
            };
        }
    }
}