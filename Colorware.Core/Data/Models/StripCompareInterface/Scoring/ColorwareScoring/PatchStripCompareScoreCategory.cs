using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Data.Models.StripCompareInterface.Scoring.Base;
using Colorware.Core.Enums;
using Colorware.Core.Globalization;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring {
    /// <summary>
    /// Represents scoring results for a single category (or patchtype).
    /// </summary>
    public class PatchStripCompareScoreCategory : IStripCompareScoreCategory {
        #region Properties
        public SummaryTypes SummaryType { get; private set; }

        private readonly IStripCompareScore parentStripCompareScore;

        /// <summary>
        /// Name for display purposes.
        /// </summary>
        public String Name { get; set; }

        public int Identifier => (int)SummaryType;

        /// <summary>
        /// Tolerance set used to get the score from.
        /// </summary>
        public IToleranceSet ToleranceSet { get; set; }

        public StripCompareSummaryCategory SummaryCategory { get; private set; }
        #endregion

        #region Calculated properties
        public bool IsOk => Result == StripCompareScoreResult.Pass;

        public bool IsWarning => Result == StripCompareScoreResult.Warning;

        public bool IsFail => Result == StripCompareScoreResult.Fail;

        private StripCompareScoreResult? result;

        public StripCompareScoreResult Result {
            private set { result = value; }
            get {
                if (!result.HasValue)
                    calculateData();
                return result.Value;
            }
        }

        private List<IStripCompareScoreEntry> entries;

        public IEnumerable<IStripCompareScoreEntry> Entries {
            get { return entries ?? (entries = new List<IStripCompareScoreEntry>()); }
        }

        public void CalculateScore() {
            throw new NotImplementedException();
        }

        private List<StripCompareScoreDefect> defects;

        public IEnumerable<StripCompareScoreDefect> Defects {
            get {
                if (defects == null)
                    calculateData();
                return defects;
            }
        }

        /// <summary>
        /// Resulting score for all faults. Should be 0 if everything is Ok.
        /// </summary>
        public double Score {
            get {
                if (!score.HasValue)
                    calculateData();
                return score.Value;
            }
        }

        private double? score;
        #endregion

        public PatchStripCompareScoreCategory([NotNull] StripCompareSummaryCategory summaryCategory,
                                              [NotNull] StripCompareScore parentStripCompareScore) {
            if (summaryCategory == null)
                throw new ArgumentNullException(nameof(summaryCategory));
            if (parentStripCompareScore == null)
                throw new ArgumentNullException(nameof(parentStripCompareScore));

            SummaryCategory = summaryCategory;
            SummaryType = summaryCategory.SummaryType;
            this.parentStripCompareScore = parentStripCompareScore;
        }

        private void calculateData() {
            score = calculateScore();
            Result = calculateResult();
            defects = calculateReasons();
        }


        private int calculateScore() {
            var totalScore = SummaryCategory
                .PatchesOutsideTolerance
                .Sum(p => ToleranceSet.GetScoreForType(p.PatchType,
                                                       p.ToleranceGroup == ToleranceGroups.Spotcolor));
            return (int)totalScore;
        }

        private StripCompareScoreResult calculateResult() {
            SummaryCategory.CalculateResult();
            return SummaryCategory.Result;
        }

        private List<StripCompareScoreDefect> calculateReasons() {
            var newResult = new List<StripCompareScoreDefect>();
            if (IsOk)
                return newResult;
            var i = 0;
            foreach (var scorePatch in parentStripCompareScore.PatchResults.Where(p => !p.Patch.IsLocked)) {
                var patch = scorePatch.Patch;
                i++;
                if (!patchMatches(patch) || patch.InTolerance)
                    continue;
                StripCompareScoreDefect defect;
                if (patch.PatchType == PatchTypes.Solid || patch.PatchType == PatchTypes.Dotgain) {
                    var translationKey = patch.ToleranceGroup == ToleranceGroups.Spotcolor
                                             ? "CW.Data.CompareScoreCategory.ReasonWithTintSpot"
                                             : "CW.Data.CompareScoreCategory.ReasonWithTintProcess";
                    defect = new StripCompareScoreDefect(patch.Name,
                                                         string.Format(
                                                             LanguageManager._(translationKey),
                                                             i, patch.Name,
                                                             patch.Tint, patch.InkZone));
                }
                else {
                    defect = new StripCompareScoreDefect(patch.Name,
                                                         string.Format(
                                                             LanguageManager._(
                                                                 "CW.Data.CompareScoreCategory.ReasonNoTint"),
                                                             i, patch.Name,
                                                             patch.InkZone));
                }

                var patchAsStripCompareScorePatch = scorePatch as StripCompareScoreEntry;
                patchAsStripCompareScorePatch?.AddDefect(defect);

                newResult.Add(defect);
            }
            return newResult;
        }

        private bool patchMatches(IStripComparePatch patch) {
            return SummaryTypeHelper.GetSummaryTypeForPatch(patch) == SummaryType;
        }
    }
}