﻿using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Enums;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring {
    /// <summary>
    /// Defines what summary categories are available.
    /// </summary>
    public enum SummaryTypes {
        Solid,
        Dotgain,
        Balance,
        Other,
        Paperwhite,
        Overprint,
        G7,
        Max,
        Minor,
        Average,
        Opacity,
        Undefined
    }

    public static class SummaryTypeHelper {
        /// <summary>
        /// Provides a mapping from a default patch type to a related summary type.
        /// </summary>
        /// <param name="patchType">The type to map.</param>
        /// <returns>The mapped type.</returns>
        public static SummaryTypes GetSummaryTypeForPatchType(PatchTypes patchType) {
            switch (patchType) {
                case PatchTypes.Solid:
                    return SummaryTypes.Solid;
                case PatchTypes.Dotgain:
                case PatchTypes.DotgainOnly:
                    return SummaryTypes.Dotgain;
                case PatchTypes.Balance:
                case PatchTypes.BalanceHighlight:
                case PatchTypes.BalanceMidtone:
                case PatchTypes.BalanceShadow:
                    return SummaryTypes.Balance;
                case PatchTypes.Paperwhite:
                    return SummaryTypes.Paperwhite;
                case PatchTypes.Overprint:
                    return SummaryTypes.Overprint;
                case PatchTypes.Other:
                    return SummaryTypes.Other;
                case PatchTypes.Undefined:
                case PatchTypes.Slur:
                    return SummaryTypes.Undefined;
                default:
                    throw new ArgumentOutOfRangeException("patchType", patchType, null);
            }
        }

        /// <summary>
        /// Provides a mapping from a default patch type to a related summary type like
        /// <see cref="GetSummaryTypeForPatchType"/>, but tries to determine if a patch is a
        /// spot color and assigns it a different <see cref="SummaryTypes"/> if it is.
        /// </summary>
        /// <param name="patch">The patch to map.</param>
        /// <returns>The mapped type.</returns>
        public static SummaryTypes GetSummaryTypeForPatch(IStripComparePatch patch) {
            return GetSummaryTypeForPatchType(patch.PatchType);
        }

        public static bool PatchMatchesSummaryType(IStripComparePatch patch, SummaryTypes summaryType) {
            var patchType = patch.PatchType;
            switch (summaryType) {
                case SummaryTypes.Solid:
                    return patchType == PatchTypes.Solid;
                case SummaryTypes.Dotgain:
                    return patchType == PatchTypes.Dotgain ||
                           patchType == PatchTypes.DotgainOnly;
                case SummaryTypes.Balance:
                    return patchType == PatchTypes.Balance ||
                           patchType == PatchTypes.BalanceHighlight ||
                           patchType == PatchTypes.BalanceMidtone ||
                           patchType == PatchTypes.BalanceShadow;
                case SummaryTypes.Other:
                    return patchType == PatchTypes.Other;
                case SummaryTypes.Paperwhite:
                    return patchType == PatchTypes.Paperwhite;
                case SummaryTypes.Overprint:
                    return patchType == PatchTypes.Overprint;
                case SummaryTypes.G7:
                    return true;
                case SummaryTypes.Max:
                    return true;
                case SummaryTypes.Minor:
                    return patchType == PatchTypes.Other ||
                           patchType == PatchTypes.Paperwhite ||
                           patchType == PatchTypes.Overprint;
                case SummaryTypes.Average:
                    return true;
                case SummaryTypes.Opacity:
                    return true;
                case SummaryTypes.Undefined:
                    return patchType == PatchTypes.Undefined;
                default:
                    throw new ArgumentOutOfRangeException("summaryType", summaryType, null);
            }
        }

        /// <summary>
        /// Take top n percentage of patches according to delta-E.
        /// </summary>
        /// <param name="patches">Patches to take from.</param>
        /// <param name="percentage">Top percentage to take. Value from 0.0 to 1.0</param>
        /// <returns>The best n% of patches.</returns>
        private static IEnumerable<IStripComparePatch> takeTopPercentage(IReadOnlyList<IStripComparePatch> patches,
                                                                         double percentage) {
            var requiredPatches = (int)Math.Floor(patches.Count * percentage);
            return patches.OrderBy(p => p.DeltaE)
                          .Take(requiredPatches);
        }

        /// <summary>
        /// Creates a set of patches that are to be graded according to the "max" criteria.
        /// The tolerance set is used to select the best n-amount of patches according to the
        /// ScorePercentageOfMax.
        /// </summary>
        /// <param name="patches">List of input patches.</param>
        /// <param name="toleranceSet">Tolerance set.</param>
        /// <returns>The best n% of patches.</returns>
        public static IEnumerable<IStripComparePatch> FilterPatchesForMax(IReadOnlyList<IStripComparePatch> patches,
                                                                          IToleranceSet toleranceSet) {
            return takeTopPercentage(patches, toleranceSet.ScorePercentageForMax);
        }

        /// <summary>
        /// Creates a set of patches that are to be graded according to the "average" criteria.
        /// The tolerance set is used to select the best n-amount of patches according to the
        /// ScorePercentageOfAverage.
        /// </summary>
        /// <param name="patches">List of input patches.</param>
        /// <param name="toleranceSet">Tolerance set.</param>
        /// <returns>The best n% of patches.</returns>
        public static IEnumerable<IStripComparePatch> FilterPatchesForAverage(IReadOnlyList<IStripComparePatch> patches, IToleranceSet toleranceSet) {
            return takeTopPercentage(patches, toleranceSet.ScorePercentageForAverage);
        }
    }
}