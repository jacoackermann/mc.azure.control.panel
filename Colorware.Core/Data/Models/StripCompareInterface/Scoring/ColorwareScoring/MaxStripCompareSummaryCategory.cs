﻿using System;
using System.Linq;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring {
    public class MaxStripCompareSummaryCategory : StripCompareSummaryCategory {
        private readonly double maxToleratedDeltaE;

        public MaxStripCompareSummaryCategory(SummaryTypes summaryType, double maxToleratedDeltaE) : base(summaryType) {
            this.maxToleratedDeltaE = maxToleratedDeltaE;
        }

        public override void AddPatch(IStripComparePatch patch) {
            if (patch == null) throw new ArgumentNullException(nameof(patch));

            if (!patch.HasTolerance)
                return;

            if (patch.DeltaE >= maxToleratedDeltaE)
                PatchesOutsideTolerance.Add(patch);
            else
                PatchesInTolerance.Add(patch);
        }

        public override void CalculateResult() {
            Result = PatchesOutsideTolerance.Any() ? StripCompareScoreResult.Fail : StripCompareScoreResult.Pass;
            HasResult = true;
        }
    }
}