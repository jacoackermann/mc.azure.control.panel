using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

using Colorware.Core.Extensions;
using Colorware.Core.Globalization;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring {
    public class StripCompareScore : IStripCompareScore, INotifyPropertyChanged {
        private readonly StripCompareSummary summary;
        private readonly IToleranceSet toleranceSet;

        #region Properties
        public IEnumerable<IStripCompareScoreCategory> Categories { get; private set; }

        public IEnumerable<IStripCompareScoreEntry> PatchResults { get; }

        public double FinalScore { get; }
        #endregion

        public StripCompareScore([NotNull] StripCompareSummary summary) {
            if (summary == null)
                throw new ArgumentNullException(nameof(summary));

            this.summary = summary;

            toleranceSet = summary.CompareResult.ToleranceSet;

            if (toleranceSet == null)
                throw new Exception("Tolerance set was not set for compare result");

            Categories = new List<IStripCompareScoreCategory>();
            PatchResults = this.summary.CompareResult.RawPatches.Patches
                               .Select(p => new StripCompareScoreEntry(p))
                               .Cast<IStripCompareScoreEntry>().ToList();

            createCategories();

            FinalScore = calculateFinalScore();
        }

        private void createCategories() {
            createCategory(SummaryTypes.Solid, LanguageManager._("CW.Data.CompareScore.Solids"));
            createCategory(SummaryTypes.Dotgain, LanguageManager._("CW.Data.CompareScore.Dotgains"));
            createCategory(SummaryTypes.Paperwhite, LanguageManager._("CW.Data.CompareScore.Paperwhite"));
            createCategory(SummaryTypes.Balance, LanguageManager._("CW.Data.CompareScore.Balance"));
            createG7Category();
            createCategory(SummaryTypes.Overprint, LanguageManager._("CW.Data.CompareScore.Overprint"));
            createCategory(SummaryTypes.Other, LanguageManager._("CW.Data.CompareScore.Other"));
            createMaxCategory();
            createAverageCategory();
            createOpacityCategory();
        }

        private void createCategory(SummaryTypes summaryType, string name) {
            var category = summary.GetCategoryForSummaryType(summaryType);
            if (category != null) {
                var scoreCategory = new PatchStripCompareScoreCategory(category, this) {
                    Name = name,
                    ToleranceSet = toleranceSet
                };

                Categories = Categories.Concat(scoreCategory).ToList();
            }
        }

        private void createMaxCategory() {
            var category = summary.GetCategoryForSummaryType(SummaryTypes.Max);
            if (category != null) {
                Categories = Categories.Concat(
                    new MaxStripCompareScoreCategory(category, toleranceSet.ScoreForMax) {
                        Name = LanguageManager._("CW.Data.CompareScore.Max")
                    }).ToList();
            }
        }

        private void createAverageCategory() {
            var category = summary.GetCategoryForSummaryType(SummaryTypes.Average);
            if (category != null) {
                Categories = Categories.Concat(
                    new AverageStripCompareScoreCategory(category, toleranceSet.ScoreForAverage) {
                        Name = LanguageManager._("CW.Data.CompareScore.Average")
                    }).ToList();
            }
        }

        private void createOpacityCategory() {
            var category = summary.GetCategoryForSummaryType(SummaryTypes.Opacity);
            if (category != null) {
                Categories = Categories.Concat(
                    new OpacityStripCompareScoreCategory(category, toleranceSet.ScoreForOpacity) {
                        Name = LanguageManager._("CW.Data.CompareScore.Opacity")
                    }).ToList();
            }
        }

        private void createG7Category() {
            var category = summary.GetCategoryForSummaryType(SummaryTypes.G7);
            if (category != null) {
                Categories = Categories.Concat(
                    new G7StripCompareScoreCategory(category, toleranceSet.ScoreForG7) {
                        Name = LanguageManager._("CW.Data.CompareScore.G7")
                    }).ToList();
            }
        }

        private double calculateFinalScore() {
            var score = 100.0;
            foreach (var category in Categories) {
                if (category.Result != StripCompareScoreResult.Pass)
                    score -= category.Score;
            }

            if (score <= 0)
                score = 0;

            return score;
        }

        // Anti memory leak
#pragma warning disable 0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 0067
    }
}