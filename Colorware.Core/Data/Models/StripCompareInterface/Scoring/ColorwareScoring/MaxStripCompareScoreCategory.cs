﻿using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Data.Models.StripCompareInterface.Scoring.Base;
using Colorware.Core.Globalization;
using Colorware.Core.Mvvm.AntiMemoryLeaks;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring {
    public class MaxStripCompareScoreCategory : IStripCompareScoreCategory {
        private readonly StripCompareSummaryCategory compareSummaryCategory;

        public MaxStripCompareScoreCategory(StripCompareSummaryCategory compareSummaryCategory, double scoreForError) {
            this.compareSummaryCategory = compareSummaryCategory;
            ScoreForError = scoreForError;

            Score = calculateScore();
            Result = calculateResult();
            Defects = calculateReasons().ToNonLeakyList();
        }

        public string Name { get; set; }

        /// <summary>
        /// Indicates the score to use when an erronous situation is detected.
        /// </summary>
        public double ScoreForError { get; }

        public double Score { get; }

        public int Identifier => (int)SummaryTypes.Max;

        public StripCompareScoreResult Result { get; }

        public bool IsOk => Result == StripCompareScoreResult.Pass;

        public bool IsWarning => Result == StripCompareScoreResult.Warning;

        public bool IsFail => Result == StripCompareScoreResult.Fail;

        public IReadOnlyList<StripCompareScoreDefect> Defects { get; }

        public IEnumerable<IStripCompareScoreEntry> Entries { get; }

        public void CalculateScore() {
            throw new NotImplementedException();
        }

        private double calculateScore() {
            return compareSummaryCategory.IsOk ? 0 : ScoreForError;
        }

        private StripCompareScoreResult calculateResult() {
            if (!compareSummaryCategory.HasResult)
                compareSummaryCategory.CalculateResult();

            return compareSummaryCategory.Result;
        }

        private IEnumerable<StripCompareScoreDefect> calculateReasons() {
            // Meh... Should find a better way to do this, but due to recent changes we should probably rewrite the scoring
            // system anyway.
            var maxCategory = compareSummaryCategory as MaxStripCompareSummaryCategory;
            if (maxCategory == null)
                throw new InvalidOperationException(
                    "The instance of the passed in category should be of type MaxStripCompareSummaryCategory");
            return maxCategory.PatchesOutsideTolerance
                              .OrderBy(p => p.InkZone)
                              .Select(p => new StripCompareScoreDefect(
                                               p.Name,
                                               LanguageManager._F("CW.Data.CompareScoreCategory.MaxExceeded", p.Name,
                                                                  p.Tint, p.InkZone),
                                               new StripCompareScoreEntry(p)));
        }
    }
}