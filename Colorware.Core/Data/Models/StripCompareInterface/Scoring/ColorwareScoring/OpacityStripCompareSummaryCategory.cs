﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring {
    public class OpacityStripCompareSummaryCategory : StripCompareSummaryCategory {
        private readonly double targetOpacity;

        public double AverageOpacity { get; private set; }

        public IEnumerable<IOpacityResult> OpacityResults { get; private set; }

        public OpacityStripCompareSummaryCategory(SummaryTypes summaryType, double targetOpacity) : base(summaryType) {
            this.targetOpacity = targetOpacity;
        }

        public override void AddPatch(IStripComparePatch patch) {
            if (patch == null) throw new ArgumentNullException(nameof(patch));

            if (!patch.HasTolerance)
                return;

            PatchesInTolerance.Add(patch);
        }

        public override void CalculateResult() {
            if (!OpacityResults.Any()) {
                Result = StripCompareScoreResult.Pass;
                return;
            }

            AverageOpacity = Math.Round(OpacityResults.Select(r => r.Opacity).ToList().Average(), 1);
            if (AverageOpacity < targetOpacity) {
                Result = StripCompareScoreResult.Fail;
            }
            else {
                Result = StripCompareScoreResult.Pass;
            }
            HasResult = true;
        }

        public void SetOpacityResults(IEnumerable<IOpacityResult> opacityResults) {
            if (opacityResults == null) throw new ArgumentNullException(nameof(opacityResults));

            OpacityResults = opacityResults;
        }
    }
}