namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring.Base {
    /// <summary>
    /// Basic implementation of <see cref="IStripCompareScoreDefect"/>. Should be sufficient for a lot of (if not all) cases.
    /// </summary>
    public class StripCompareScoreDefect : IStripCompareScoreDefect {
        private readonly string name;
        private readonly string description;
        private readonly IStripCompareScoreEntry entry;

        #region Implementation of IStripCompareScoreDefect
        public string Name {
            get { return name; }
        }

        public string Description {
            get { return description; }
        }

        public IStripCompareScoreEntry Entry {
            get { return entry; }
        }
        #endregion

        public StripCompareScoreDefect(string name, string description) {
            this.name = name;
            this.description = description;
            this.entry = null;
        }

        public StripCompareScoreDefect(string name, string description, IStripCompareScoreEntry entry) {
            this.name = name;
            this.description = description;
            this.entry = entry;
        }
    }
}