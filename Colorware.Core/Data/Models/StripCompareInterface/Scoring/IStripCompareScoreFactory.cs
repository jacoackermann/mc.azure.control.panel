namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring {
    public interface IStripCompareScoreFactory {
        /// <summary>
        /// Create a new IStripCompareScore derived instance for a given result.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        IStripCompareScore Create(IStripCompareResult result);
    }
}