using System.Collections.Generic;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring {
    /// <summary>
    /// Represents a scorable category of entries.
    /// </summary>
    public interface IStripCompareScoreCategory {
        /// <summary>
        /// Name of the category
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The resulting score for this category.
        /// </summary>
        double Score { get; }

        /// <summary>
        /// Kind of a hack for now. Used as an integer identifier for this category.
        /// Default use would be the patch type.
        /// </summary>
        int Identifier { get; }

        /// <summary>
        /// The result for this category.
        /// </summary>
        StripCompareScoreResult Result { get; }

        /// <summary>
        /// Return all the entries in this category.
        /// </summary>
        IEnumerable<IStripCompareScoreEntry> Entries { get; }

        void CalculateScore();
    }
}