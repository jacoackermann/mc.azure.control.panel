using System.Collections.Generic;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring {
    public interface IStripCompareScoreEntry {
        /// <summary>
        /// The patch this result belongs to. May be null if this is a general scoring item.
        /// </summary>
        IStripComparePatch Patch { get; }

        /// <summary>
        /// The final score for this item.
        /// </summary>
        double Score { get; }

        /// <summary>
        /// The result of the score for this item.
        /// </summary>
        StripCompareScoreResult ScoreResult { get; }

        /// <summary>
        /// Name describing the scoring result.
        /// </summary>
        string ResultGrade { get; }

        /// <summary>
        /// List of defects for this item.
        /// </summary>
        IEnumerable<IStripCompareScoreDefect> Defects { get; }
    }
}