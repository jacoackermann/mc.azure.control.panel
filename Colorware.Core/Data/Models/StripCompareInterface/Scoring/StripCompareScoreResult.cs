namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring {
    /// <summary>
    /// Possible results of a score.
    /// </summary>
    public enum StripCompareScoreResult {
        /// <summary>
        /// No data available or scoring is not applicable.
        /// Safe to ignore this result.
        /// </summary>
        Unknown = 3,

        /// <summary>
        /// No significant deficiencies.
        /// </summary>
        Pass = 2,

        /// <summary>
        /// Still OK, but noticeable deficiencies are present.
        /// </summary>
        Warning = 1,

        /// <summary>
        /// Too many deficiencies to pass.
        /// </summary>
        Fail = 0,
    }
}