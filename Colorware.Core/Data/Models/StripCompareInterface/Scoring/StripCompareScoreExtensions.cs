using System.Linq;

using Colorware.Core.Data.Models.StripCompareInterface.Scoring.EPQScoring;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring {
    public static class StripCompareScoreExtensions {
        /// <summary>
        /// Return the first entry in the IStripCompareScore.PatchResults list that belongs to the given patch.
        /// </summary>
        /// <param name="compareScore">The IStripCompareScore to search in.</param>
        /// <param name="patch">The patch to search for.</param>
        /// <returns>If present, returns the IStripCompareScoreEntry belonging to the passed in patch; returns null otherwise.</returns>
        public static IStripCompareScoreEntry FindEntryForPatch(this IStripCompareScore compareScore,
            IStripComparePatch patch) {
            return compareScore.PatchResults.FirstOrDefault(p => p.Patch.IsSame(patch));
        }

        /// <summary>
        /// Regular behavior is the same as FindEntryForPatch, but if the scoring system is EPQ
        /// only the TVI patches will be searched.
        /// </summary>
        /// <param name="compareScore">The IStripCompareScore to search in.</param>
        /// <param name="patch">The patch to search for.</param>
        /// <returns>The IStripCompareScoreEntry belonging to the passed in patch if found; null otherwise.</returns>
        public static IStripCompareScoreEntry FindDotgainForPatch(this IStripCompareScore compareScore,
            IStripComparePatch patch) {
            var asEPQScore = compareScore as IEpqStripCompareScore;
            if (asEPQScore == null) {
                // Unknown type of scoring.
                return FindEntryForPatch(compareScore, patch);
            }
            return asEPQScore.CategoryTvi.Entries.FirstOrDefault(p => p.Patch.IsSame(patch));
        }

        /// <summary>
        /// Regular behavior is the same as FindEntryForPatch, but if the scoring system is EPQ
        /// only the TVI patches will be searched.
        /// </summary>
        /// <param name="compareScore">The IStripCompareScore to search in.</param>
        /// <param name="patch">The patch to search for.</param>
        /// <returns>The IStripCompareScoreEntry belonging to the passed in patch if found; null otherwise.</returns>
        public static IStripCompareScoreEntry FindDotgainDeltaEForPatch(this IStripCompareScore compareScore,
            IStripComparePatch patch) {
            var asEPQScore = compareScore as IEpqStripCompareScore;
            if (asEPQScore == null) {
                // Unknown type of scoring.
                return FindEntryForPatch(compareScore, patch);
            }
            return asEPQScore.CategoryDeltaE50.Entries.FirstOrDefault(p => p.Patch.IsSame(patch));
        }
    }
}