using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Enums;

namespace Colorware.Core.Data.Models.StripCompareInterface.Scoring.EPQScoring {
    public interface IEpqStripCompareScore : IStripCompareScore {
        bool HasFinalScore { get; }
        StripCompareScoreResult FinalResult { get; }
        EpqScoreResult FinalRawResult { get; set; }
        IEnumerable<IStripCompareScoreCategory> EpqCompareScoreCategories { get; }
        bool HasVisual { get; }
        bool HasRegistration { get; }
        IStripCompareScoreCategory CategoryDeltaE100 { get; }
        IStripCompareScoreCategory CategoryDeltaE50 { get; }
        IStripCompareScoreCategory CategoryTvi { get; }
        IStripCompareScoreCategory CategoryVisual { get; }
        IStripCompareScoreCategory CategoryRegistration { get; }
        IStripCompareScoreCategory Other { get; }

        /// <summary>
        /// Get the associated additional data for the current score. Will create an empty entry if it does not
        /// exist yet.
        /// </summary>
        /// <returns>An EpqScore instance related to the current score.</returns>
        Task<IEpqScore> GetAdditionalScoreData();

        void SaveDefaults();
        void CalculateFinalScore();
        void InvokePropertyChanged(string property);

        /// <summary>
        /// Creates a new instance, copying over all data to this instance. The additional data will also be cloned.
        /// </summary>
        /// <returns></returns>
        Task<IEpqStripCompareScore> Clone();
    }
}