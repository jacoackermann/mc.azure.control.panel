using System.Collections.Generic;

namespace Colorware.Core.Data.Models.StripCompareInterface {
    public interface IStripCompareDotgainList {
        int Percentage { get; set; }
        List<IStripComparePatch> Patches { get; set; }
    }
}