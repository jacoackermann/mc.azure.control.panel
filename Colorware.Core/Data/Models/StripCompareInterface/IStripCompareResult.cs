using System;
using System.Collections.Generic;

using Colorware.Core.Data.Models.G7Analysis;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring.ColorwareScoring;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.StripCompareInterface {
    public interface IStripCompareResult {
        [NotNull]
        IJob Job { get; }

        [NotNull]
        IMeasurement Measurement { get; }

        [NotNull]
        IToleranceSet ToleranceSet { get; }

        [NotNull]
        IMeasurementConditionsConfig MeasurementConditionsConfig { get; }

        [NotNull]
        IMachine Machine { get; }

        [CanBeNull]
        IEnumerable<ISample> OKSheetSamples { get; }

        /// <summary>
        /// Full list of raw patches.
        /// </summary>
        [NotNull]
        IStripComparePatches RawPatches { get; }

        /// <summary>
        /// Full list of summary patches.
        /// </summary>
        [NotNull]
        IStripComparePatches SummaryPatches { get; }

        int NumberOfInkzones { get; }
        int EndInkZone { get; }
        List<IStripCompareComponentInkZones> ComponentInkzones { get; }

        bool HasInkzones { get; }

        /// <summary>
        /// Return true if the strip contains a paperwhite or if the job has an associated paperwhite measurement patch.
        /// </summary>
        bool HasDotgains { get; }

        /// <summary>
        /// Provides a summary for each patch type.
        /// </summary>
        [NotNull]
        StripCompareSummary CompareSummary { get; }

        /// <summary>
        /// Not unlike CompareSummary, but provides scoring features to calculate an overall score
        /// for this compare.
        /// </summary>
        [NotNull]
        IStripCompareScore CompareScore { get; }

        /// <summary>
        /// Gets the unfiltered raw patches (currently only used for Image to access disabled targets for
        /// display on the result screen Image).
        /// </summary>
        [CanBeNull]
        IStripComparePatches UnfilteredRawPatches { get; }

        G7AnalysisDataCollection G7AnalysisData { get; set; }

        List<IStripCompareComponentInkZones> CalculateDotgainInkZones(int slot);
        IStripCompareComponentInkZones CalculateInkZonesForFilteredPatches(Func<IStripComparePatch, bool> patchFilter);
    }
}