using System;
using System.Collections.Immutable;
using System.Linq;

using Colorware.Core.Color;
using Colorware.Core.Data.Models.G7Analysis;
using Colorware.Core.Enums;
using Colorware.Core.Extensions;

namespace Colorware.Core.Data.Models.StripCompareInterface {
    public static class StripComparePatchesExtensions {
        /// <summary>
        /// Gets the CMY overprint patch from the collection of patches
        /// </summary>
        /// <param name="stripComparePatches"></param>
        /// <returns></returns>
        public static IStripComparePatch GetCmyOverprintPatch(this IStripComparePatches stripComparePatches) {
            return getPatchForTints(stripComparePatches, PatchTypes.Overprint, 100, 100, 100, 0);
        }

        /// <summary>
        /// Gets the SolidK patch from the collection of patches
        /// </summary>
        /// <param name="stripComparePatches"></param>
        /// <returns></returns>
        public static IStripComparePatch GetSolidK(this IStripComparePatches stripComparePatches) {
            return getPatchForTints(stripComparePatches, PatchTypes.Solid, 0, 0, 0, 100);
        }

        /// <summary>
        /// Matches patches with given parameters in the Patches collection
        /// </summary>
        /// <param name="stripComparePatches"></param>
        /// <param name="patchType">Patchtype to match</param>
        /// <param name="cyan">Percentage of cyan to match</param>
        /// <param name="magenta">Percentage of magenta to match</param>
        /// <param name="yellow">Percentage of yellow to match</param>
        /// <param name="key">Percentage of key to match</param>
        /// <returns></returns>
        private static IStripComparePatch getPatchForTints(this IStripComparePatches stripComparePatches,
                                                           PatchTypes patchType, int cyan,
                                                           int magenta, int yellow, int key) {
            return stripComparePatches.Patches.FirstOrDefault(
                p => p.PatchType == patchType &&
                     p.Cyan.NearlyEquals(cyan) &&
                     p.Magenta.NearlyEquals(magenta) &&
                     p.Yellow.NearlyEquals(yellow) &&
                     p.Key.NearlyEquals(key));
        }

        /// <summary>
        /// Gets Dotgain patches
        /// </summary>
        /// <param name="stripComparePatches"></param>
        /// <returns></returns>
        public static IImmutableList<IStripComparePatch> GetKeyDotgainPatches(
            this IStripComparePatches stripComparePatches) {
            return stripComparePatches.Patches.Where(p => p.Key > 0 && p.IsDotgain).ToImmutableList();
        }

        /// <summary>
        /// Gets G7 Gray Balance Patches
        /// </summary>
        /// <param name="stripComparePatches"></param>
        /// <returns></returns>
        public static IImmutableList<IStripComparePatch> GetG7GrayBalancePatches(
            this IStripComparePatches stripComparePatches) {
            return stripComparePatches.Patches.Where(
                p =>
                (p.IsBalanceHighlight || p.IsBalanceMidtone || p.IsBalanceShadow ||
                 p.PatchType == PatchTypes.Balance) &&
                G7Calculations.IsPatchGrayBalance(p)).ToImmutableList();
        }

        /// <summary>
        /// Find G7 Highlight Contrast patch (CMY 25/19/19)
        /// </summary>
        /// <param name="stripComparePatches"></param>
        /// <returns></returns>
        public static IStripComparePatch GetHcPatch(this IStripComparePatches stripComparePatches) {
            var balancePatches = stripComparePatches.GetG7GrayBalancePatches();
            return
                balancePatches.FirstOrDefault(
                    p => p.Cyan.NearlyEquals(25) && p.Magenta.NearlyEquals(19) && p.Yellow.NearlyEquals(19));
        }

        /// <summary>
        /// Find G7 Highlight Range patch (CMY 50/40/40)
        /// </summary>
        /// <param name="stripComparePatches"></param>
        /// <returns></returns>
        public static IStripComparePatch GetHrPatch(this IStripComparePatches stripComparePatches) {
            var balancePatches = stripComparePatches.GetG7GrayBalancePatches();
            return
                balancePatches.FirstOrDefault(
                    p => p.Cyan.NearlyEquals(50) && p.Magenta.NearlyEquals(40) && p.Yellow.NearlyEquals(40));
        }

        /// <summary>
        /// Find G7 Shadow Contrast patch (CMY 75/66/66)
        /// </summary>
        /// <param name="stripComparePatches"></param>
        /// <returns></returns>
        public static IStripComparePatch GetScPatch(this IStripComparePatches stripComparePatches) {
            var balancePatches = stripComparePatches.GetG7GrayBalancePatches();
            return
                balancePatches.FirstOrDefault(
                    p => p.Cyan.NearlyEquals(75) && p.Magenta.NearlyEquals(66) && p.Yellow.NearlyEquals(66));
        }

        public static bool IsThreeColorOverprint(this IStripComparePatch patch) {
            return patch.IsOverprint &&
                   patch.Cyan > 0 &&
                   patch.Yellow > 0 &&
                   patch.Magenta > 0;
        }

        /// <summary>
        /// Is this patch a solid or dotgain key(black) patch?
        /// </summary>
        /// <param name="patch">The patch to check.</param>
        /// <returns><c>True</c> if the patch is a key patch, <c>false</c> otherwise.</returns>
        public static bool IsKeyInk(this IStripComparePatch patch) {
            return patch.IsSolid ||
                   patch.IsDotgain &&
                   (patch.Cyan.NearlyEquals(0) &&
                    patch.Magenta.NearlyEquals(0) &&
                    patch.Yellow.NearlyEquals(0) &&
                    patch.Key > 0);
        }

        /// <summary>
        /// Is this patch a solid key(black) patch?
        /// </summary>
        /// <param name="patch">The patch to check.</param>
        /// <returns><c>True</c> if the patch is a key solid patch, <c>false</c> otherwise.</returns>
        public static bool IsKeySolid(this IStripComparePatch patch) {
            return patch.IsSolid &&
                   patch.Cyan.NearlyEquals(0) &&
                   patch.Magenta.NearlyEquals(0) &&
                   patch.Yellow.NearlyEquals(0) &&
                   patch.Key > 0;
        }

        /// <summary>
        /// Checks if a patch is 'of a given component'. This means that the component has a part in
        /// the ink that is larger than 0, while the other compontents are 0.
        /// </summary>
        /// <param name="patch">The patch to check.</param>
        /// <param name="component">The component to check.</param>
        /// <returns>
        /// <c>True</c> if the patch only consists of the given component, <c>false</c> otherwise.
        /// </returns>
        public static bool IsComponent(this IStripComparePatch patch, CmykComponents component) {
            switch (component) {
                case CmykComponents.C:
                    return
                        patch.Cyan > 0 &&
                        patch.Magenta.NearlyEquals(0) &&
                        patch.Yellow.NearlyEquals(0) &&
                        patch.Key.NearlyEquals(0);
                case CmykComponents.M:
                    return
                        patch.Cyan.NearlyEquals(0) &&
                        patch.Magenta > 0 &&
                        patch.Yellow.NearlyEquals(0) &&
                        patch.Key.NearlyEquals(0);
                case CmykComponents.Y:
                    return
                        patch.Cyan.NearlyEquals(0) &&
                        patch.Magenta.NearlyEquals(0) &&
                        patch.Yellow > 0 &&
                        patch.Key.NearlyEquals(0);
                case CmykComponents.K:
                    return
                        patch.Cyan.NearlyEquals(0) &&
                        patch.Magenta.NearlyEquals(0) &&
                        patch.Yellow.NearlyEquals(0) &&
                        patch.Key > 0;
                default:
                    throw new ArgumentOutOfRangeException(nameof(component), component, null);
            }
        }
    }
}