using System;

using Colorware.Core.Color;
using Colorware.Core.Data.Models.Compare;
using Colorware.Core.Data.Models.PantoneLIVE;
using Colorware.Core.Data.Models.StripCompare;
using Colorware.Core.Enums;
using Colorware.Core.Functional.Option;
using Colorware.Core.Models.Image;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.StripCompareInterface {
    public interface IStripComparePatch : IInkZone, ILabConvertible, IHasDensities {
        string Name { get; set; }
        int Index { get; set; }
        int Slot1 { get; set; }
        int Slot2 { get; set; }
        int Slot3 { get; set; }
        bool HasSlot1 { get; }
        bool HasSlot2 { get; }
        bool HasSlot3 { get; }
        bool IsPrinted { get; set; }

        /// <summary>
        /// True when the sample is a dud.
        /// 
        /// If this is a summary patch, ALL its family samples must be duds for the ISCP to
        /// be considered a dud. If SOME are duds, they are just left out of the averaging
        /// calculations and the ISCP is NOT considered a dud.
        /// </summary>
        bool IsDud { get; }

        /// <summary>
        /// True when the difference between the sample and reference density is so large that
        /// it's considered an outlier and not useful for some purposes (e.g. press control).
        /// </summary>
        bool IsDensityOutlier { get; }

        bool IsLocked { get; set; }
        bool IsSpot { get; set; }
        int Tint { get; set; }
        PatchTypes PatchType { get; set; }
        ToleranceGroups ToleranceGroup { get; set; }
        bool HasDotgain { get; set; }
        DotgainMethod DotgainMethod { get; set; }
        String SampleSpectrum { get; set; }
        int SampleSpectrumStart { get; set; }
        int SampleSpectrumEnd { get; set; }
        bool HasSampleSpectrum { get; }
        Spectrum AsSampleSpectrum { get; }
        String ReferenceSpectrum { get; set; }
        int ReferenceSpectrumStart { get; set; }
        int ReferenceSpectrumEnd { get; set; }
        bool HasReferenceSpectrum { get; }
        Spectrum AsReferenceSpectrum { get; }
        double Cyan { get; set; }
        double Magenta { get; set; }
        double Yellow { get; set; }
        double Key { get; set; }

        [CanBeNull]
        IStripComparePatch PaperWhite { get; set; }

        /// <summary>
        /// Will hold the sample of the solid K(black) ink in the same inkzone if available.
        /// </summary>
        [CanBeNull]
        ISample SampleForSolidKParentOfInkZone { get; set; }

        /// <summary>
        /// Will hold the sample of the closest found CMY patch if available.
        /// </summary>
        [CanBeNull]
        ISample SampleForClosestCmy { get; set; }

        // For Image
        int TargetX { get; set; }
        int TargetY { get; set; }
        IMeasureTarget MeasureTarget { get; set; }

        /// <summary>
        ///  Return true if this patch is a summary patch.
        /// </summary>
        bool IsSummary { get; }

        /// <summary>
        /// Number of patches in summary. Should return 1 if not a summary patch.
        /// </summary>
        int NumberOfPatches { get; }

        /// <summary>
        /// Number of patches in tolerance for a summary patch.
        /// Return either 1 or 0 for non-summary patch.
        /// </summary>
        int NumberOfPatchesInTolerance { get; }

        /// <summary>
        /// Number of patches that need to be in tolerance for the average to be in tolerance.
        /// Return 1 for non-summary patch.
        /// </summary>
        int NumberOfPatchesInToleranceRequired { get; }


        ColorComparer<IStripReferencePatch> Comparer { get; }

        /// <summary>
        /// Return true if the sample has been calculated as "within tolerance" for the given tolerance group.
        /// </summary>
        bool InTolerance { get; set; }

        /// <summary>
        /// Return true if any type of tolerance checking is actually applicable to this patch. Return false
        /// if no tolerance checking is to be done. This basically means that the patch is in an 'undefined' state with regards
        /// to tolerance.
        /// </summary>
        bool HasTolerance { get; set; }

        // int InkZone { get; set; }
        Lab SampleLab { get; set; }
        Lab RefLab { get; set; }
        Lch SampleLch { get; set; }
        Lch RefLch { get; set; }
        CMYK SampleDensity { get; set; }
        CMYK RefDensity { get; set; }
        bool HasTrapping { get; set; }
        double Trapping { get; set; }
        double DeltaE { get; set; }
        double DeltaH { get; set; }
        double ChromaPlus { get; set; }
        CMYK GrayBalanceSample { get; set; } // Relative to paperwhite.
        CMYK GrayBalanceReference { get; set; } // Relative to paperwhite.
        CMYK GrayBalanceDifference { get; set; } // As tonevalue
        CMYK GrayBalanceDifferenceG7 { get; set; } // As tonevalue
        double SampleDotGain { get; set; }
        double RefDotGain { get; set; }
        System.Windows.Media.Color AsColor { get; }
        String PatchTypeGenericName { get; }

        double SampleDotgainForCurrentMode { get; }
        double ReferenceDotgainForCurrentMode { get; }


        /// <summary>
        /// Does this patch have a predicted density value?
        /// </summary>
        bool HasPredictedDensityResult { get; }

        /// <summary>
        /// Predicted best density delta value towards best match.
        /// </summary>
        CMYK PredictedDeltaCMYKDensityForBestMatch { get; }

        /// <summary>
        /// Predicted filtered best density delta value towards best match.
        /// </summary>
        double PredictedDeltaDensityForBestMatch { get; }

        /// <summary>
        /// Predicted delta-E for best match.
        /// </summary>
        double PredictedDeltaEForBestMatch { get; }

        /// <summary>
        /// Predicted density for best match.
        /// </summary>
        double PredictedBestDensity { get; }

        /// <summary>
        /// Predicted best match Lab.
        /// </summary>
        Lab PredictedOptimalLab { get; }

        /// <summary>
        /// The value used to calculate the scumming information.
        /// </summary>
        [CanBeNull]
        Lab ScummingSample { get; set; }

        /// <summary>
        /// Signifies that scumming information is available for this patch.
        /// Should only be true for paperwhite patches.
        /// </summary>
        bool HasScumming { get; set; }

        /// <summary>
        /// The amount of scumming for the patch (deltaE between measured paperwhite and measured rawboard).
        /// </summary>
        double Scumming { get; }

        /// <summary>
        /// Is A G7 weighted L available for this patch? If <c>true</c><see cref="G7WeightedDiffL"/>
        /// will be set.
        /// </summary>
        bool HasG7WeightedDiffL { get; }

        /// <summary>
        /// Is A G7 weighted Ch available for this patch? If <c>true</c><see
        /// cref="G7WeightedDiffCh"/> will be set.
        /// </summary>
        bool HasG7WeightedDiffCh { get; }

        /// <summary>
        /// G7 Weighted delta-L if applicable for the current patch in the current situation.
        /// </summary>
        double G7WeightedDiffL { get; }
        
        /// <summary>
        /// G7 Weighted delta-Ch if applicable for the current patch in the current situation.
        /// </summary>
        double G7WeightedDiffCh { get; }

        /// <summary>
        /// Return true if this is an `important' patch (Service announcement: "Important" to be defined later...)
        /// </summary>
        bool IsImportantPatch { get; }

        int PatchTypeSortNumber { get; }
        double DiffL { get; }
        double DiffA { get; }
        double DiffB { get; }
        double DiffC { get; }
        double DiffH { get; }
        double DiffDensity { get; }
        CMYK DiffDensityFull { get; }
        double DotgainDiff { get; }
        bool IsLighter { get; }
        bool IsDarker { get; }
        bool IsDuller { get; }
        bool IsStronger { get; }
        bool IsRedder { get; }
        bool IsYellower { get; }
        bool IsSolid { get; }
        bool IsOverprint { get; }
        bool IsPaperWhite { get; }
        bool IsBalanceHighlight { get; }
        bool IsBalanceMidtone { get; }
        bool IsBalanceShadow { get; }
        bool IsDotgain { get; }
        bool IsPantoneLivePatch { get; }

        bool ChromaPlusIsPositive { get; }
        bool ChromaPlusIsNegative { get; }

        /// <summary>
        /// When set, signifies that a tolerance override is in effect for this patch and the value
        /// denotes the tolerance to use.
        /// </summary>
        Option<double> DeltaEToleranceOverride { get; set; }

        /// <summary>
        /// If true, represents a patch which is only valid for its dotgain data.
        /// This is basically meant for spot colors with a percentage of &lt; 100% which can only
        /// be defined by using a standard dotgain curve (DefaultDotgainList/DefaultDotgainEntry).
        /// </summary>
        bool IsDotgainOnly { get; }

        bool IsDefined { get; }
        bool IsSingleSlot { get; }
        int SingleSlot { get; }

        String GuessFilter();
        // TODO
        /// <summary>
        /// Looks at the patch name (CMYK) and highest component, is not appropriate in all
        /// situations!
        /// </summary>
        CmykComponents GuessComponent(out bool isComponent);
        IStripComparePatch Copy();
        void SetSample(ISample sample);
        void UpdateCompareResults();

        /// <summary>
        /// Create a sample from this result.
        /// </summary>
        /// <returns>A new sample</returns>
        ISample AsSample();

        /// <summary>
        ///  Create a JobStripPatch from this result.
        /// </summary>
        /// <returns>A new job strip patch.</returns>
        [NotNull]
        IJobStripPatch AsJobStripPatch();

        /// <summary>
        /// Create a new reference from this result.
        /// </summary>
        /// <returns>A new reference patch.</returns>
        [Obsolete("This method disregards any PantoneLIVE related identification information, as such the reference patch obtained here does not sport the full range of features that a regular reference patch does. This can lead to problems when using this patch.")]
        IReferencePatch AsReferencePatch();

        /// <summary>
        /// Id of the related reference patch.
        /// </summary>
        long ReferencePatchId { get; }

        /// <summary>
        /// If available, this will contain the full color compare patch of the same family
        /// </summary>
        [CanBeNull]
        IStripComparePatch Solid { get; set; }

        ISample Sample { get; }

        /// <summary>
        /// Return the IStripReferencePatch associated with this compare patch.
        /// </summary>
        [NotNull]
        IStripReferencePatch ReferencePatch { get; }

        PantoneLiveStandardId PantoneLiveStandardId { get; }

        /// <summary>
        /// Update the IsPrinted property.
        /// </summary>
        void UpdateIsPrinted();

        /// <summary>
        /// Checks if this patch has any need of knowing about the passed in solid patch.
        /// </summary>
        /// <param name="solid">The solid to compare with.</param>
        /// <returns>true if the Solid property of this patch can be set to the passed in solid.</returns>
        bool CanAcceptSolid(IStripComparePatch solid);

        /// <summary>
        /// Initialize compare results for the given sample and reference data.
        /// </summary>
        void InitResults(IToleranceSet toleranceSet, IMeasurementConditionsConfig measurementConditionsConfig, int patchIndex);

        void InitChromaTrack(IToleranceSet toleranceSet, IMeasurementConditionsConfig measurementConditionsConfig);
    }
}