using System.Collections.Generic;
using System.Windows.Data;

using Colorware.Core.Color;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.StripCompareInterface {
    public interface IStripComparePatches {
        /// <summary>
        /// All patches in the list.
        /// </summary>
        IReadOnlyList<IStripComparePatch> Patches { get; }

        CollectionViewSource PatchesByInkZone { get; }
        bool InTolerance { get; }
        bool HasPaperwhite { get; }
        double MaximumDeltaE { get; }
        double AverageDeltaE { get; }
        double PrimariesDeltaE { get; }
        double PaperWhiteDeltaE { get; }
        double PrimariesDeltaH { get; }
        double GrayBalanceDeltaH { get; }
        double CyanDeltaH { get; }
        double MagentaDeltaH { get; }
        double YellowDeltaH { get; }
        double BlackDeltaH { get; }

        /// <summary>
        /// List of `important' patches.
        /// </summary>
        IReadOnlyList<IStripComparePatch> ImportantPatches { get; }

        IReadOnlyList<NamedLab> GamutSamples { get; }
        IReadOnlyList<NamedLab> GamutReferences { get; }
        IReadOnlyList<Lab> PaperWhiteOffsets { get; }
        IReadOnlyList<Lab> BalanceHightlightOffsets { get; }
        IReadOnlyList<Lab> BalanceMidtoneOffsets { get; }
        IReadOnlyList<Lab> BalanceShadowOffsets { get; }
        IReadOnlyList<IStripComparePatch> SolidPatches { get; }
        IReadOnlyList<IStripCompareDotgainList> ShowableDotgains { get; set; }
        int[] UsedSlots { get; }

        [CanBeNull]
        IStripComparePatch FirstPaperWhitePatch { get; }
    }
}