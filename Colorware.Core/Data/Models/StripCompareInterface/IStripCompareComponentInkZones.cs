using System;
using System.Collections.Generic;

namespace Colorware.Core.Data.Models.StripCompareInterface {
    public interface IStripCompareComponentInkZones {
        String Name { get; }
        int Percentage { get; set; }
        int Slot { get; }
        int SequenceNumber { get; }
        List<IStripComparePatch> Patches { get; }
        List<IStripComparePatch> SolidPatches { get; }
        System.Windows.Media.Color AverageColor { get; }
        double AverageChromaPlus { get; }
        double AverageDeltaDensity { get; }
        double AverageSampleDensity { get; }
        double AverageDeltaE { get; }
        double AverageDeltaDotgain { get; }
        double SolidReferenceDensity { get; }
        String SolidReferenceDensityName { get; }

        /// <summary>
        /// Pad the ink zones to make sure they extend the full range as given.
        /// This means adding undefined patches in front of the patches that are present
        /// to bring them into the proper positions (and counting can start from inkzone 1), but
        /// also adding patches at the end to bring the full range up to the range given (which is
        /// usually the range of the current printing/press machine).
        /// </summary>
        /// <param name="endZone">The end inkzone. This is usually defined by the used machine.</param>
        void PadInkZones(int endZone);
    }
}