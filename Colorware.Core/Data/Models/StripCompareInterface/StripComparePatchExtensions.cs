using Colorware.Core.Enums;

namespace Colorware.Core.Data.Models.StripCompareInterface {
    public static class StripComparePatchExtensions {
        /// <summary>
        /// Check if both patches are `the same'.
        /// </summary>
        /// <param name="patch1">The first patch.</param>
        /// <param name="patch2">The second patch.</param>
        /// <returns>True if patches are deemed `the same', false otherwise.</returns>
        public static bool IsSame(this IStripComparePatch patch1, IStripComparePatch patch2) {
            if (patch1 == null || patch2 == null)
                return false;
            return
                patch1.Tint == patch2.Tint &&
                patch1.PatchType == patch2.PatchType &&
                patch1.Slot1 == patch2.Slot1 &&
                patch1.Slot2 == patch2.Slot2 &&
                patch1.Slot3 == patch2.Slot3;
        }

        /// <summary>
        /// Helper function to check if a raw patch is compatible (belongs to) a given summary patch.
        /// </summary>
        /// <param name="patch">The raw patch.</param>
        /// <param name="summaryPatch">The summary patch.</param>
        /// <returns>
        /// 	<c>true</c> if the specified summary patch is compatible; otherwise, <c>false</c>.
        /// </returns>
        public static bool BelongsToSummaryPatch(this IStripComparePatch patch, IStripComparePatch summaryPatch) {
            if (patch.PatchType != summaryPatch.PatchType)
                return false;
            switch (patch.PatchType) {
                case PatchTypes.Solid:
                case PatchTypes.Dotgain:
                    return patch.Tint == summaryPatch.Tint && patch.Name == summaryPatch.Name &&
                           patch.SingleSlot == summaryPatch.SingleSlot;
                case PatchTypes.Overprint:
                    return patch.Slot1 == summaryPatch.Slot1 &&
                           patch.Slot2 == summaryPatch.Slot2 &&
                           patch.Slot3 == summaryPatch.Slot3;
                case PatchTypes.Balance:
                case PatchTypes.BalanceHighlight:
                case PatchTypes.BalanceMidtone:
                case PatchTypes.BalanceShadow:
                    return patch.PatchType == summaryPatch.PatchType;
                default:
                    return true;
            }
        }
    }
}