﻿using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public enum MeasurementCustomFieldType {
        String,
        Float
    };

    public interface IMeasurementCustomFieldsConfigurationItem : IBaseModel {
        

        bool IsEnabled { get; set; }
        bool IsMultiline { get; set; }
        string Name { get; set; }
        MeasurementCustomFieldType TypeOfField { get; set; }

        /// <summary>
        /// The measurement entry that this configuration actually corresponds with.
        /// e.g. FieldIndex 0 with TypeOfField as string is custom stringfield 0 in the measurement object.
        /// </summary>
        int FieldIndex { get; set; }

        int SortOrder { get; set; }
    }
}