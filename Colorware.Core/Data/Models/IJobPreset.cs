﻿using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;

using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models {

    public interface IJobPreset : IBaseModel, ICreatedStamped, IUpdatedStamped {
        string Name { get; set; }

        JobTypes JobType { get; set; }

        string Description { get; set; }

        long UserGroupId { get; set; }

        long MachineId { get; set; }

        /// <summary>
        /// Does this preset belong to a specific machine, or can it be used for all machines.
        /// </summary>
        bool IsShared { get; set; }

        IBelongsTo<IMachine> Machine { get; }

        long PaperTypeId { get; set; }

        IBelongsTo<IPaperType> PaperType { get; }

        long ToleranceSetId { get; set; }

        IBelongsTo<IToleranceSet> ToleranceSet { get; }

        long MeasurementConditionId { get; set; }

        IBelongsTo<IMeasurementConditionsConfig> MeasurementCondition { get; }

        long ReferenceId { get; set; }

        IBelongsTo<IReference> Reference { get; }

        long ClientReferenceId { get; set; }

        IBelongsTo<IReference> ClientReference { get; }

        long PaperId { get; set; }

        IBelongsTo<IPaper> Paper { get; }

        long InkSetId { get; set; }

        IBelongsTo<IInkSet> InkSet { get; }

        long ColorStripId { get; set; }

        IBelongsTo<IColorStrip> ColorStrip { get; }

        /// <summary>
        /// Serialized color mapping, including mapping for client colors.
        /// </summary>
        [Obsolete("Use new structures for new job setup: Get/SetInkDefinitions")]
        string ColorMapping { get; set; }

        /// <summary>
        /// Serialized color sequence.
        /// </summary>
        [Obsolete("Use new structures for new job setup: Get/SetInkDefinitions")]
        string ColorSequence { get; set; }

        [NotNull]
        IEnumerable<IJobPresetInkDefinition> GetInkDefinitions();

        void SetInkDefinitions([NotNull] IEnumerable<IJobPresetInkDefinition> newDefinitions);

        /// <summary>
        /// Serialized densities.
        /// </summary>
        string DefaultDensities { get; set; }

        /// <summary>
        /// Dotgain list per spot color in serialized form.
        /// </summary>
        string DefaultDotgainLists { get; set; }

        /// <summary>
        /// Cached result for easy display.
        /// </summary>
        string JobColors { get; set; }

        Guid ImageName { get; set; }

        int NumberOfPanels { get; set; }

        IHasManyCollection<IJobPresetTargetPosition> JobPresetTargetPositions { get; set; }

        Guid PantoneLiveClientReferenceGuid { get; set; }

        BitmapImage ThumbnailTiny { get; set; }

        IHasManyCollection<IJobPresetDisabledPatch> JobPresetDisabledPatches { get; set; }
    }
}