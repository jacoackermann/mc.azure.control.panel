﻿using System;

using Colorware.Core.Data.Specification;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models {
    public interface IReference : IBaseModel, ICreatedStamped, IUpdatedStamped, IExternalGuid {
        Guid OriginTypeGuid { get; set; }

        long DefaultDotgainListId { get; set; }

        bool IsSpotLib { get; set; }

        String Name { get; set; }

        long CompanyId { get; set; }

        IHasManyCollection<IReferencePatch> ReferencePatches { get; }

        IBelongsTo<IDefaultDotgainList> DefaultDotgainList { get; }

        IBelongsTo<ICompany> Company { get; }

        long PaperTypeId { get; set; }

        IBelongsTo<IPaperType> PaperType { get; set; }

        bool Locked { get; set; }
        
        Guid IccFileName { get; set; }

        /// <remarks>Does not guarantee that the patch is actually in our collection of patches, just looks
        /// at the ID relationship (or PL id).</remarks>
        bool IsParentOf([NotNull] IReferencePatch referencePatch);
    }

    public static class IReferenceExtensions {
        public static bool IsLikelyColorMapSpotReference([NotNull] this IReference reference) {
            if (reference == null) throw new ArgumentNullException(nameof(reference));

            return reference.Name != null && reference.Name.StartsWith("ColorMAP - ");
        }
    }
}