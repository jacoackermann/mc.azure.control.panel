﻿namespace Colorware.Core.Data.Models {
    public interface IClientForExternalServer {
        long ClientServerId { get; set; }
        bool Active { get; set; }
        long ClientId { get; set; }
        string ClientName { get; set; }
        long ServerId { get; set; }
        string ServerName { get; set; }
        long Servers { get; set; }
        PqmSendMethod SendMethod { get; set; }
    }
}