﻿using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

namespace Colorware.Core.Data.Models {
    public interface IColorMapping : IBaseModel {
        /// <summary>
        /// This is used store mappings for multiple modules/categories/families etc. in the same table.
        /// E.g: 1 could be for the SVF importer, 2 could be for importer X, 3 could be for module Y
        /// 
        /// Family '0' is reserved and means 'applies to all families'. Useful for simple colors like
        /// Black --> K, Cyan --> C etc.
        /// </summary>
        ColorMappingFamily Family { get; set; }

        /// <summary>
        /// Specifies the priority of a mapping within a family. Lower is more important.
        /// 
        /// The exact meaning is implementation specific.
        /// </summary>
        int Priority { get; set; }

        /// <summary>
        /// Name of the color in the foreign/external system/source/file etc. This can be a wildcard,
        /// regular expression, partial name etc. depending on the implementation.
        /// </summary>
        string ForeignName { get; set; }

        /// <summary>
        /// Name of the color in the local (Pressview/MC) system/source/file etc. This can be a wildcard,
        /// regular expression, partial name etc. depending on the implementation.
        /// </summary>
        string LocalName { get; set; }
    }
}