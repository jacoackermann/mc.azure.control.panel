﻿using System;

using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IPaperManufacturer : IBaseModel {
        String Name { get; set; }
    }
}