using System;

using Colorware.Core.Data.Specification;
using Colorware.Core.PrintQualityReport;

namespace Colorware.Core.Data.Models {
    public interface IExternalReportsServer : IBaseModel {
        /// <summary>
        /// Guid of the used type
        /// </summary>
        Guid TypeGuid { get; set; }
        
        /// <summary>
        /// The name of the reports server. For display.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// The URL of the server.
        /// </summary>
        string Url { get; set; }

        /// <summary>
        /// Token to verify identity
        /// </summary>
        string VerificationToken { get; set; }

        /// <summary>
        /// Username and password to verify identity
        /// </summary>
        string Username { get; set; }
        string Password { get; set; }

        /// <summary>
        /// Extra fields for systems that require more input to secure a connection
        /// </summary>
        string ExtraField1 { get; set; }
        string ExtraField2 { get; set; }
        string ExtraField3 { get; set; }
        string ExtraField4 { get; set; }

        /// <summary>
        /// Whether periodic sending of jobs and measurement information has been enabled.
        /// </summary>
        bool SendEnabled { get; set; }

        /// <summary>
        /// Holds wether the connection is valid or not
        /// </summary>
        bool ValidConnection { get; set; }

        /// <summary>
        /// Should *all* data be send to this server indiscriminately, i.e. without filtering the
        /// data? When set to <c>false</c>, additional setting will filter the data according to
        /// selected measurements or clients (also see <see cref="PqmSendMethod"/>). When set to
        /// <c>true</c> other settings will be ignored and every item in the system will eligible for sending.
        /// </summary>
        bool SendAllDataUnfiltered { get; set; }

        /// <summary>
        /// Helper function to get the <see cref="PqrProviderGuid"/> from the Guid saved in the database.
        /// </summary>
        PqrProviderGuid ProviderGuid { get; }
    }
}