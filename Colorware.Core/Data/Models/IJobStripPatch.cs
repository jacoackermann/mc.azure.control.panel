﻿using System;

using Colorware.Core.Color;
using Colorware.Core.Data.Models.StripCompare;
using Colorware.Core.Data.Specification;
using Colorware.Core.Functional.Option;

namespace Colorware.Core.Data.Models {
    public interface IJobStripPatch : IBaseModel, IStripReferencePatch, IToleranceDeltaEOverride, IExternalGuid {
        double DefaultDensity { get; set; }

        bool IsCustomized { get; set; }

        int TargetX { get; set; }

        int TargetY { get; set; }

        long JobStripId { get; set; }

        long ReferencePatchId { get; set; }

        IBelongsTo<IReferencePatch> ReferencePatch { get; set; }

        [Obsolete("This gives a false sense of security (ReferencePatch can be missing in DB)")]
        bool HasReferencePatch { get; }

        string SlotPreview { get; }

        bool IsUndefined { get; }

        bool IsDefined { get; }

        bool IsDotgainOnly { get; }

        bool IsAnyBalance { get; }

        long ColorStripPatchId { get; set; }

        System.Windows.Media.Color AsColor { get; }

        bool IsSolid { get; }

        bool IsDotgain { get; }

        bool IsDotgainLike { get; }

        bool IsPaperWhite { get; }

        bool IsOverprint { get; }

        int FirstValidSlot { get; }

        // For PantoneLIVE implementation in Image
        Option<Guid> PantoneLiveObjectGuid { get; set; }

        // For disabling patches in the color bar during job setup.
        bool IsDisabled { get; set; }

        // Define again to 'add' the setter:
        new bool UseDeltaEToleranceOverride { get; set; }
        // Define again to 'add' the setter:
        new double DeltaEToleranceOverride { get; set; }

        /// <summary>
        /// Deterimines if this color is a process or a spot color.
        /// </summary>
        new bool IsSpot { get; set; }

        /// <summary>
        /// Allow overriding of target data for G7 purposes.
        /// </summary>
        /// <param name="targetLab">The new target data to use.</param>
        void OverrideLabTargetForG7(Lab targetLab);
    }
}