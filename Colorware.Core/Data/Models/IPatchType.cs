using Colorware.Core.Enums;

namespace Colorware.Core.Data.Models {
    public interface IPatchType {
        PatchTypes PatchType { get; set; }
    }
}