﻿using Colorware.Core.Data.Models.StripCompareInterface;

namespace Colorware.Core.Data.Models.PantoneLIVE {
    public interface IPantoneLiveFilteredStripCompareResultFactory {
        IPantoneLiveFilteredStripCompareResult Create(IStripCompareResult stripCompareResult);
    }
}