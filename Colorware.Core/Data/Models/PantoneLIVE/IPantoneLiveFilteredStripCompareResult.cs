using System.Threading.Tasks;

using Colorware.Core.Data.Models.StripCompareInterface;

namespace Colorware.Core.Data.Models.PantoneLIVE {
    public interface IPantoneLiveFilteredStripCompareResult : IStripCompareResult {
        Task<IPantoneLiveFilteredStripCompareResult> Initialization { get; set; }
    }
}