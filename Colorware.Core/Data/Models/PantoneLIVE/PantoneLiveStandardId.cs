﻿using System;

namespace Colorware.Core.Data.Models.PantoneLIVE {
    /// <summary>
    /// Identifies a dependent standard in a particular palette in the PantoneLIVE system. This essentially identifies
    /// a color standard (what we call ReferencePatch).
    /// 
    /// Can be used to retrieve color data from the PantoneLIVE system.
    /// </summary>
    public class PantoneLiveStandardId : IEquatable<PantoneLiveStandardId> {
        public Guid PaletteGuid { get; }
        public Guid DependentStandardGuid { get; }

        public PantoneLiveStandardId(Guid paletteGuid, Guid dependentStandardGuid) {
            PaletteGuid = paletteGuid;
            DependentStandardGuid = dependentStandardGuid;
        }

        public bool Equals(PantoneLiveStandardId other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return PaletteGuid.Equals(other.PaletteGuid) && DependentStandardGuid.Equals(other.DependentStandardGuid);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((PantoneLiveStandardId)obj);
        }

        public override int GetHashCode() {
            unchecked {
                return (PaletteGuid.GetHashCode() * 397) ^ DependentStandardGuid.GetHashCode();
            }
        }
    }
}