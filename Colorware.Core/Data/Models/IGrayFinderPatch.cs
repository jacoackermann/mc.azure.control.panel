﻿using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IGrayFinderPatch : IBaseModel {
        byte[] Data { get; set; }

        long ReferencePatchId { get; set; }
    }
}