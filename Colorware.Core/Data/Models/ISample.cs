using Colorware.Core.Color;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models {
    /// <summary>
    /// A sample is guaranteed to have measurement conditions and a spectrum
    /// and/or Lab values.
    /// 
    /// Density values are not guaranteed.
    /// </summary>
    public interface ISample : IBaseModel, ILabConvertible, IHasDensities {
        double L { get; set; }

        // ReSharper disable InconsistentNaming
        double a { get; set; }

        // ReSharper disable InconsistentNaming
        double b { get; set; }

        Illuminant Illuminant { get; set; }

        Observer Observer { get; set; }

        MCondition MCondition { get; set; }

        [NotNull]
        SpectralMeasurementConditions MeasurementConditions { get; }

        double DensityC { get; set; }

        double DensityM { get; set; }

        double DensityY { get; set; }

        double DensityK { get; set; }

        double DensityS { get; set; }

        double SWavelength { get; set; }

        [CanBeNull]
        string Spectrum { get; }
        
        int SpectrumStart { get; }

        int SpectrumEnd { get; }

        bool HasSpectrum { get; }

        /// <summary>
        /// Custom integer field which holds information depending on context.
        /// </summary>
        long Tag { get; set; }

        long MeasurementId { get; set; }

        IBelongsTo<IMeasurement> Measurement { get; set; }

        System.Windows.Media.Color AsColor { get; }

        [CanBeNull]
        Spectrum AsSpectrum { get; }

        [NotNull]
        Lch AsLch { get; }

        [NotNull]
        string AsString { get; }

        CmykComponents CmykComponent { get; }

        /// <summary>
        /// A dud is a sample that wasn't measured properly or is considered invalid for other reasons,
        /// it shouldn't be used in calculations.
        /// 
        /// It's specifically NOT just an outlier, since an outlier can still carry information (e.g.
        /// a measurement or printing error).
        /// </summary>
        bool IsDud { get; }
    }
}