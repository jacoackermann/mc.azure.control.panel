﻿using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IInkManufacturer : IBaseModel {
        string Name { get; set; }
    }
}