using System;

namespace Colorware.Core.Data.Models.Identity {
    /// <summary>
    /// Crutch to deal with various origins when we don't have the actual model instance itself around
    /// to tell who he is (i.e. we only have info describing the identity, but not the model itself)
    /// </summary>
    public interface IModelId {
        /// <summary>
        /// The type of source this model originates from. E.g. a PV database, the PantoneLIVE system,
        /// an MIS system etc.
        /// </summary>
        Guid OriginTypeGuid { get; }

        // Can be used in the future to distinguish between multiple source of the same type, e.g.
        // a PV database at location A, a PV database at location B etc.
        //public Guid SpecificOriginGuid;
    }
}