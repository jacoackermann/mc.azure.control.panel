﻿using System;

namespace Colorware.Core.Data.Models.Identity {
    // Hardcoded for now, not ideal for loose coupling
    public static class OriginTypeGuids {
        public static readonly Guid PantoneLive = new Guid("28F68F31-03A9-4B07-86AC-76E45E6AB1E7");
        public static readonly Guid PressViewDb = new Guid("8C2DC24E-3275-4E9B-B4EB-C4423B65FA0C");
    }
}