using System;

namespace Colorware.Core.Data.Models.Identity {
    /// <summary>
    /// Identity of a model originating from a PressView database.
    /// </summary>
    // NB: inherits from IReferencePatchId because of failing OOP reasons,
    // see also comment for IReferencePatchId
    public class PressViewDatabaseId : IModelId, IReferencePatchId {
        public long DbId { get; }

        public PressViewDatabaseId(long dbId) {
            DbId = dbId;
        }

        public Guid OriginTypeGuid => OriginTypeGuids.PressViewDb;
    }
}