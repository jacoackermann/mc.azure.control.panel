﻿using System;

namespace Colorware.Core.Data.Models.Identity {
    public abstract class GenericPantoneLiveId : IPantoneLiveId {
        public Guid OriginTypeGuid => OriginTypeGuids.PantoneLive;
        public Guid PantoneLiveObjectGuid { get; }

        public GenericPantoneLiveId(Guid pantoneLiveObjectGuid) {
            PantoneLiveObjectGuid = pantoneLiveObjectGuid;
        }

        protected bool Equals(GenericPantoneLiveId other) {
            return PantoneLiveObjectGuid.Equals(other.PantoneLiveObjectGuid);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((GenericPantoneLiveId)obj);
        }

        public override int GetHashCode() {
            return PantoneLiveObjectGuid.GetHashCode();
        }

        public static bool operator ==(GenericPantoneLiveId left, GenericPantoneLiveId right) {
            return Equals(left, right);
        }

        public static bool operator !=(GenericPantoneLiveId left, GenericPantoneLiveId right) {
            return !Equals(left, right);
        }
    }
}