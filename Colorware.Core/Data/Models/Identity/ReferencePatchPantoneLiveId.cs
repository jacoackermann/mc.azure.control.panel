using System;

using Colorware.Core.Data.Models.PantoneLIVE;

namespace Colorware.Core.Data.Models.Identity {
    /// <summary>
    /// Note that there's no way to retrieve standards (ReferencePatches) from the PL system
    /// directly via the PantoneLiveGuid as far as we know, that's why we need this class
    /// specifically for ReferencePatches
    /// The PantoneLiveGuid is only needed when retrieving patches via a downloaded palette CXF,
    /// which is the old slow way to retrieve a known patch
    /// </summary>
    public class ReferencePatchPantoneLiveId : GenericPantoneLiveId, IReferencePatchId {
        public PantoneLiveStandardId PantoneLiveStandardId { get; }

        /// <summary>
        /// PL provides solids and tint patches, but there's no way to distinguish between them
        /// when retrieving them via the DependentStandard API call (they only have their own
        /// GUID in the CXF, which is an inefficient way to retrieve patches).
        /// 
        /// This is why the Tint is part of the identity.
        /// </summary>
        public int Tint { get; }

        public ReferencePatchPantoneLiveId(Guid pantoneLiveObjectGuid, PantoneLiveStandardId pantoneLiveStandardId,
            int tint)
            : base(pantoneLiveObjectGuid) {
            PantoneLiveStandardId = pantoneLiveStandardId;
            Tint = tint;
        }
        
        protected bool Equals(ReferencePatchPantoneLiveId other) {
            return base.Equals(other) && Equals(PantoneLiveStandardId, other.PantoneLiveStandardId) && Tint == other.Tint;
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ReferencePatchPantoneLiveId)obj);
        }

        public override int GetHashCode() {
            unchecked {
                int hashCode = base.GetHashCode();
                hashCode = (hashCode * 397) ^ (PantoneLiveStandardId?.GetHashCode() ?? 0);
                hashCode = (hashCode * 397) ^ Tint;
                return hashCode;
            }
        }

        public static bool operator ==(ReferencePatchPantoneLiveId left, ReferencePatchPantoneLiveId right) {
            return Equals(left, right);
        }

        public static bool operator !=(ReferencePatchPantoneLiveId left, ReferencePatchPantoneLiveId right) {
            return !Equals(left, right);
        }
    }
}