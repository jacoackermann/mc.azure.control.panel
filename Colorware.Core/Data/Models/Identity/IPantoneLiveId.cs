﻿using System;

namespace Colorware.Core.Data.Models.Identity {
    public interface IPantoneLiveId : IModelId {
        // Generic PantoneLIVE object id, not needed to retrieve a patch >= 15.3, but we still require it
        // to identify patches while working with them (e.g. in Image)
        Guid PantoneLiveObjectGuid { get; }
    }
}