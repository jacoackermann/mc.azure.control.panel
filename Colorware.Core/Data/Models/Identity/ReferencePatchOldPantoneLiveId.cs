﻿using System;

namespace Colorware.Core.Data.Models.Identity {
    /// <summary>
    /// A pre-16.1 PantoneLIVE patch id
    ///
    /// We used to only store the PantoneLiveGuid and the PaletteGuid for a JobStripPatch that
    /// originated from PL, this allowed us to retrieve the ReferencePatch by first retrieving
    /// the Palette and then searching it for the patch with a specific PantoneLiveGuid.
    /// This isn't very efficient, and later we were notified of a secret API call that allows
    /// us to retrieve it via a PalleteGuid + DependentStandardGuid, so we prefer not to use this
    /// old identity anymore, it exists only for migration purposes.
    /// 
    /// Note that the old way can only identify solids (no dotgains are included in a palette,
    /// they have to be requested using the solid's id)
    /// </summary>
    public class ReferencePatchOldPantoneLiveId : GenericPantoneLiveId, IReferencePatchId {
        public Guid PaletteGuid { get; }

        public ReferencePatchOldPantoneLiveId(Guid pantoneLiveObjectGuid, Guid paletteGuid)
            : base(pantoneLiveObjectGuid) {
            PaletteGuid = paletteGuid;
        }
    }
}