namespace Colorware.Core.Data.Models.Identity {
    /// <summary>
    /// Marker interface to enforce we don't get a GenericPantoneLiveId as a ReferencePatch
    /// identifier (that's not enough to identify a ReferencePatch)
    /// </summary>
    public interface IReferencePatchId : IModelId {
    }
}