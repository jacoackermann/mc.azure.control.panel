using System;

using Colorware.Core.Data.Models.PantoneLIVE;
using Colorware.Core.Functional.Option;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.Identity {
    public static class ReferencePatchId {
        [NotNull]
        public static IReferencePatchId Create([NotNull] IReferencePatch patch) {
            if (patch == null) throw new ArgumentNullException(nameof(patch));

            if (patch.NewInstance) {
                // New patches must have some negative id to be able to create relationships
                // between them when they're not saved yet
                if (patch.Id >= 0)
                    throw new Exception("Unsaved patches must have a negative Id to be able to refer to them!");

                return Create(patch.Id.ToOption(), Option<Guid>.Nothing, Option<Guid>.Nothing,
                    Option<Guid>.Nothing, patch.Percentage.ToOption());
            }

            return Create(patch.Id.ToOption(id => id > 0), patch.PantoneLiveObjectGuid,
                patch.PantoneLiveStandardId.Select(s => s.PaletteGuid),
                patch.PantoneLiveStandardId.Select(s => s.DependentStandardGuid), patch.Percentage.ToOption());
        }

        [NotNull]
        public static IReferencePatchId Create([NotNull] IJobPresetInkDefinition inkDefinition) {
            if (inkDefinition == null) throw new ArgumentNullException(nameof(inkDefinition));

            return Create(inkDefinition.ReferencePatchId.ToOption(id => id > 0),
                inkDefinition.PantoneLiveObjectGuid.ToOption().Select(g => new Guid(g)),
                inkDefinition.PantoneLivePaletteGuid.ToOption().Select(g => new Guid(g)),
                inkDefinition.PantoneLiveDependentStandardGuid.ToOption().Select(g => new Guid(g)),
                100.ToOption());
        }

        /// <summary>
        /// Although every parameter is an Option, we still require one of the following combinations
        /// (there's just no simple way to express this in C#):
        /// 
        /// - dbId
        /// - objectGuid, paletteId (this is a pre-16.1 PL identity)
        /// - objectGuid, paletteId, dependentStandardGuid (, tint)
        /// </summary>
        /// <param name="dbId">The database identifier.</param>
        /// <param name="objectGuid">The object unique identifier.</param>
        /// <param name="paletteId">The palette identifier.</param>
        /// <param name="dependentStandardGuid">The dependent standard unique identifier.</param>
        /// <param name="tint">The tint.</param>
        [NotNull]
        public static IReferencePatchId Create(Option<long> dbId,
            Option<Guid> objectGuid,
            Option<Guid> paletteId,
            Option<Guid> dependentStandardGuid,
            Option<int> tint) {
            return dbId
                .SelectOrElse(
                    id => new PressViewDatabaseId(id),
                    () => objectGuid.Select(
                        oGuid => paletteId.Select(
                            pId => dependentStandardGuid.SelectOrElse<Guid, IReferencePatchId>(
                                dsGuid => new ReferencePatchPantoneLiveId(oGuid, new PantoneLiveStandardId(pId, dsGuid),
                                    tint.OrElse(100)),
                                () => new ReferencePatchOldPantoneLiveId(oGuid, pId)))
                                          .OrElse(() => new ArgumentException(
                                              "Must specify a PantoneLive palette GUID to construct an identity for a ReferencePatch!",
                                              nameof(paletteId))))
                                    .OrElse(() => new ArgumentException(
                                        "Must specify a PantoneLIVE object GUID to construct an identity for a ReferencePatch if no database ID is specified!",
                                        nameof(objectGuid))));
        }
    }
}