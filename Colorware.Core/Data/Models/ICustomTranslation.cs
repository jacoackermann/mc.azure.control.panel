﻿using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    /// <summary>
    /// Represents a translation entry, used for overriding a translation for a single translation key.
    /// </summary>
    public interface ICustomTranslation : IBaseModel {
        /// <summary>
        /// The translation key to override.
        /// </summary>
        string TranslationKey { get; set; }

        /// <summary>
        /// The value to override the key with.
        /// </summary>
        string TranslationValue { get; set; }
    }
}