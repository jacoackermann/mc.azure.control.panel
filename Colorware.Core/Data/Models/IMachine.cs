﻿using System;

using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IMachine : IBaseModel, IExternalGuid {
        String Name { get; set; }

        int NumberOfColors { get; set; }

        int NumberOfInkzones { get; set; }

        double ZoneWidth { get; set; }

        long MachineTypeId { get; set; }

        IBelongsTo<IMachineType> MachineType { get; }

        long CompanyId { get; set; }

        IBelongsTo<ICompany> Company { get; }

        long DefaultToleranceId { get; set; }

        IBelongsTo<IToleranceSet> ToleranceSet { get; }

        bool HasDefaultToleranceSet { get; }
    }
}