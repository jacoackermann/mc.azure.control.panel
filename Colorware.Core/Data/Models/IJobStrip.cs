﻿using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IJobStrip : IBaseModel, IExternalGuid {
        bool IsCustomized { get; set; }

        bool IsReset { get; set; }
        
        IHasManyCollection<IJobStripPatch> JobStripPatches { get; }
    }
}