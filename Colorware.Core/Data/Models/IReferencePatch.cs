﻿using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Color;
using Colorware.Core.Data.Models.PantoneLIVE;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;
using Colorware.Core.Extensions;
using Colorware.Core.Functional.Option;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models {
    public interface IReferencePatch : IBaseModel, IHasDensity, ILabConvertible, IPatchType, IToleranceGroup,
                                       IPercentage, IExternalGuid, IToleranceDeltaEOverride, IHasComponents {
        Guid OriginTypeGuid { get; set; }

        string Name { get; set; }

        double L { get; set; }

        double a { get; set; }

        double b { get; set; }

        double x { get; set; }

        double y { get; set; }

        double z { get; set; }

        Illuminant Illuminant { get; set; }

        Observer Observer { get; set; }

        MCondition MCondition { get; set; }

        SpectralMeasurementConditions MeasurementConditions { get; }

        bool IsSpot { get; set; }

        /// <summary>
        /// Don't use for calculations!
        /// </summary>
        new double Cyan { get; set; }

        /// <summary>
        /// Don't use for calculations!
        /// </summary>
        new double Magenta { get; set; }

        /// <summary>
        /// Don't use for calculations!
        /// </summary>
        new double Yellow { get; set; }

        /// <summary>
        /// Don't use for calculations!
        /// </summary>
        new double Key { get; set; }

        double DefaultDensity { get; set; }

        double DefaultDotgain { get; set; }

        double SpectralDensity { get; set; }

        string Spectrum { get; set; }

        int SpectrumStart { get; set; }

        int SpectrumEnd { get; set; }

        double WavelengthOfSpectralDensity { get; set; }

        long Parent1Id { get; set; }

        long Parent2Id { get; set; }

        long Parent3Id { get; set; }

        long ReferenceId { get; set; }

        double GrayC { get; set; }

        double GrayM { get; set; }

        double GrayY { get; set; }

        // The GUID in the CXF's custom section belonging to the color entry
        // from which this patch was imported
        Option<Guid> PantoneLiveObjectGuid { get; set; }

        // The PantoneLIVE DependentStandard this patch was created from (if any)
        Option<PantoneLiveStandardId> PantoneLiveStandardId { get; set; }

        bool IsPantoneLivePatch { get; }

        IBelongsTo<IReference> Reference { get; }

        bool HasSpectrum { get; }

        Spectrum AsSpectrum { get; set; }

        [NotNull]
        Lch AsLch { get; }

        System.Windows.Media.Color AsColor { get; }

        bool IsSolid { get; }

        bool IsDotgain { get; }

        bool IsPaperwhite { get; }

        bool IsDefined { get; }

        bool IsUndefined { get; }

        bool IsOverprint { get; }

        bool IsAnyBalance { get; }

        [CanBeNull]
        string ExtraInfo1 { get; set; }

        [CanBeNull]
        string ExtraInfo2 { get; set; }

        void UpdateFromSample([NotNull] ISample sample);

        /// <summary>
        /// Sets the Lab and measurement conditions. Note that you can't change the
        /// M-condition to another M-condition when the ReferencePatch already has
        /// a spectrum (this would change the way the spectrum was measured,
        /// which is impossible).
        /// </summary>
        void SetLabAndMeasurementConditions([NotNull] Lab lab);
    }

    public static class IReferencePatchExtensions {
        public static bool IsOverprintOf([NotNull] this IReferencePatch overprint,
                                         [NotNull, ItemNotNull] IEnumerable<IReferencePatch> solids) {
            if (overprint == null) throw new ArgumentNullException(nameof(overprint));
            if (solids == null) throw new ArgumentNullException(nameof(solids));

            var solidPatches = solids as IReferencePatch[] ?? solids.ToArray();

            // If any of the solids is a PantoneLIVE patch, it can currently never be a parent for an overprint.
            if (solidPatches.Any(solid => solid.IsPantoneLivePatch)) {
                return false;
            }

            var parentsIds = new[] {overprint.Parent1Id, overprint.Parent2Id, overprint.Parent3Id};

            return solidPatches.All(solid => parentsIds.Any(parentId => parentId == solid.Id))
                   && parentsIds.Where(id => id > 0).Distinct().Count() == solidPatches.DistinctBy(s => s.Id).Count()
                   && parentsIds.Count(id => id > 0) == solidPatches.Count();
        }

        public static bool HasValidDensity([NotNull] this IReferencePatch patch) {
            return !patch.DefaultDensity.NearlyEquals(0);
        }
        
        public static bool CanProvideLabFor([NotNull] this IReferencePatch patch,
                                            [NotNull] SpectralMeasurementConditions mc) {
            if (patch == null) throw new ArgumentNullException(nameof(patch));
            if (mc == null) throw new ArgumentNullException(nameof(mc));

            return patch.MeasurementConditions.SpectralMCondition == mc.SpectralMCondition &&
                   (patch.MeasurementConditions.IllObsMatch(mc) || patch.HasSpectrum);
        }

        public static PatchReferenceType GetPatchReferenceTypeForPatch([NotNull] this IReferencePatch patch) {
            if (patch == null) throw new ArgumentNullException(nameof(patch));

            if (patch.IsSpot) {
                return PatchReferenceType.Spot;
            }

            if (patch.Cyan.NearlyEquals(100) && patch.Magenta.NearlyEquals(0) &&
                patch.Yellow.NearlyEquals(0) && patch.Key.NearlyEquals(0)) {
                return PatchReferenceType.ProcessC;
            }

            if (patch.Cyan.NearlyEquals(0) && patch.Magenta.NearlyEquals(100) &&
                patch.Yellow.NearlyEquals(0) && patch.Key.NearlyEquals(0)) {
                return PatchReferenceType.ProcessM;
            }

            if (patch.Cyan.NearlyEquals(0) && patch.Magenta.NearlyEquals(0) &&
                patch.Yellow.NearlyEquals(100) && patch.Key.NearlyEquals(0)) {
                return PatchReferenceType.ProcessY;
            }

            if (patch.Cyan.NearlyEquals(0) && patch.Magenta.NearlyEquals(0) &&
                patch.Yellow.NearlyEquals(0) && patch.Key.NearlyEquals(100)) {
                return PatchReferenceType.ProcessK;
            }

            return PatchReferenceType.Process;
        }
    }
}