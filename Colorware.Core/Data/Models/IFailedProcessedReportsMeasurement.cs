﻿using Colorware.Core.Enums;

namespace Colorware.Core.Data.Models {
    public interface IFailedProcessedReportsMeasurement {
        long Id { get; set; }

        /// <summary>
        /// Id of the external Reports server
        /// </summary>
        long ExternalReportsServerId { get; set; }

        /// <summary>
        /// The id of the processed measurement.
        /// </summary>
        long MeasurementId { get; set; }

        ProcessingStatus Status { get; set; }

        string Message { get; set; }

        long JobId { get; set; }
        string JobName { get; set; }
        string JobNumber { get; set; }
    }
}