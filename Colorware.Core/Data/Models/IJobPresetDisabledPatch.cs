﻿using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IJobPresetDisabledPatch : IBaseModel {

        int JobStripPatchIndex { get; set; }

        long JobPresetId { get; set; }
    }
}