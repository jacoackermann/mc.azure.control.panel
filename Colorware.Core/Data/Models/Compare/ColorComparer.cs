using System;
using System.Linq;

using Colorware.Core.Color;
using Colorware.Core.Color.Support.Exceptions;
using Colorware.Core.Data.Models.ChromaTrack;
using Colorware.Core.Data.Models.G7Analysis;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Enums;
using Colorware.Core.Functional.Option;
using Colorware.Core.Helpers;
using Colorware.Core.Logging;
using Colorware.Core.Mvvm;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.Compare {
    /// <summary>
    /// General class that compares two colors with each other and is capable of deciding if they are
    /// within a certain tolerance if a tolerance set has been assigned.
    /// TODO: Could probably do with some optimization. Currently there is some recalculation of colors
    /// to lab and colors to lch.
    /// </summary>
    /// <typeparam name="TReference">The reference target.</typeparam>
    public class ColorComparer<TReference> : DefaultNotifyPropertyChanged
        where TReference : ILabConvertible, IHasDensity, IPatchType, IToleranceGroup, IPercentage, IHasComponents {
        private const string VisualsDeltaConfigKey = "Defaults.ColorComparer.VisualsDelta";

        #region Properties
        private ISample sample;

        [CanBeNull]
        public ISample Sample {
            get { return sample; }
            set {
                sample = value;
                onAllPropertiesChanged();
            }
        }

        private TReference reference;

        [CanBeNull]
        public TReference Reference {
            get { return reference; }
            set {
                reference = value;
                onAllPropertiesChanged();
            }
        }

        private IToleranceSet toleranceSet;

        [CanBeNull]
        public IToleranceSet ToleranceSet {
            get { return toleranceSet; }
            set {
                toleranceSet = value;
                onAllPropertiesChanged();
            }
        }

        public Option<double> DeltaEToleranceOverride { get; private set; }

        private Lab scummingSample;

        [CanBeNull]
        public Lab ScummingSample {
            get { return scummingSample; }
            set {
                scummingSample = value;
                onAllPropertiesChanged();
            }
        }

        public bool HasScumming => ScummingSample != null;

        private ToleranceReport<TReference> toleranceReport;

        [CanBeNull]
        public ToleranceReport<TReference> ToleranceReport {
            get {
                if (toleranceReport == null) {
                    if (ToleranceSet != null) {
                        toleranceReport = new ToleranceReport<TReference>(this);
                    }
                }

                return toleranceReport;
            }
        }
        #endregion Properties

        #region Calculations
        #region Deltas
        public double DeltaE {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return 0.0;
                var method = DeltaEMethod.Cie76;
                if (ToleranceSet != null)
                    method = ToleranceSet.DeltaEMethod;
                switch (method) {
                    case DeltaEMethod.Cie76:
                        return Reference.AsLab.DeltaE76(Sample.AsLab);
                    case DeltaEMethod.Cie94:
                        return Reference.AsLab.DeltaE94(Sample.AsLab);
                    case DeltaEMethod.CMC:
                        double l, c;
                        if (ToleranceSet == null) {
                            l = 2;
                            c = 1;
                            LogManager.GetLogger(GetType()).Error(
                                "Tolerance set not available, can not accurately set L and C values for use with DeltaCMC, using 2 and 1 respectively.");
                        }
                        else {
                            l = ToleranceSet.DeltaCMCL;
                            c = ToleranceSet.DeltaCMCC;
                        }

                        return Reference.AsLab.DeltaECMC(Sample.AsLab, l, c);
                    case DeltaEMethod.Cie2000:
                        return Reference.AsLab.DeltaE2000(Sample.AsLab);
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double DeltaCMClUsed {
            get {
                if (ToleranceSet == null)
                    return 2;
                return ToleranceSet.DeltaCMCL;
            }
        }

        public double DeltaCMCcUsed {
            get {
                if (ToleranceSet == null)
                    return 1;
                return ToleranceSet.DeltaCMCC;
            }
        }

        public double DeltaH {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return 0.0;
                return Reference.AsLab.DeltaH(Sample.AsLab);
            }
        }

        public double ChromaPlus {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return 0.0;
                return Reference.AsLab.ChromaPlus(Sample.AsLab);
            }
        }

        public double DeltaDensity {
            get {
                if (Sample == null || Reference == null)
                    return 0.0;
                return Sample.Density - Reference.Density;
            }
        }

        public double DeltaLComponent {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return 0.0;
                return Sample.AsLab.L - Reference.AsLab.L;
            }
        }

        public double DeltaAComponent {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return 0.0;
                return Sample.AsLab.a - Reference.AsLab.a;
            }
        }

        public double DeltaBComponent {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return 0.0;
                return Sample.AsLab.b - Reference.AsLab.b;
            }
        }

        public double DeltaCComponent {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return 0.0;
                return Sample.AsLab.AsLch.c - Reference.AsLab.AsLch.c;
            }
        }

        public double DeltaHComponent {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return 0.0;
                return Sample.AsLab.AsLch.h - Reference.AsLab.AsLch.h;
            }
        }

        /// <summary>
        /// Like DeltaHComponent, but makes sure that the angle between colors will be the minimal one.
        /// </summary>
        public double DeltaHComponentAngle {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return 0.0;
                var deltaHComponent = DeltaHComponent;
                return AngleHelper.MinimizeAngle(deltaHComponent);
            }
        }

        public double Scumming {
            get {
                if (Sample == null || ToleranceSet == null || !HasScumming)
                    return 0.0;
                return ToleranceSet.DeltaEUsingCurrentMethod(Sample.AsLab, ScummingSample);
            }
        }
        #endregion Deltas

        #region Lightness, Chroma and Hue
        private readonly double visualsDelta = Config.Config.DefaultConfig.GetDouble(VisualsDeltaConfigKey, 1.5);

        public bool IsLighter {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return false;
                return Sample.AsLab.AsLch.L - Reference.AsLab.AsLch.L > visualsDelta;
            }
        }

        public bool IsDarker {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return false;
                return Sample.AsLab.AsLch.L - Reference.AsLab.AsLch.L < -visualsDelta;
            }
        }

        public bool LightnessIsEven {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return false;
                return !IsLighter && !IsDarker;
            }
        }

        public bool IsDuller {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return false;
                return Sample.AsLab.AsLch.c - Reference.AsLab.AsLch.c < -visualsDelta;
            }
        }

        public bool IsStronger {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return false;
                return Sample.AsLab.AsLch.c - Reference.AsLab.AsLch.c > visualsDelta;
            }
        }

        public bool ChromaIsEven {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return false;
                return !IsDuller && !IsStronger;
            }
        }

        public bool IsRedder {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return false;
                return Sample.AsLab.a - Reference.AsLab.a > visualsDelta;
            }
        }

        public bool IsGreener {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return false;
                return Sample.AsLab.a - Reference.AsLab.a < -visualsDelta;
            }
        }

        public bool GreenRedIsEven {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return false;
                return !IsRedder && !IsGreener;
            }
        }

        public bool IsYellower {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return false;
                return Sample.AsLab.b - Reference.AsLab.b > visualsDelta;
            }
        }

        public bool IsBluer {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return false;
                return Sample.AsLab.b - Reference.AsLab.b < -visualsDelta;
            }
        }

        public bool BlueYellowIsEven {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return false;
                return !IsYellower && !IsBluer;
            }
        }

        public String AngleIndication {
            get {
                if (Sample == null || Reference == null || Reference.PatchType == PatchTypes.Undefined)
                    return "";

                var angle = Reference.AsLab.AsLch.h;
                var deltaAngle = Sample.AsLab.AsLch.h - Reference.AsLab.AsLch.h;

                if ((angle > 0 && angle < 35) || (angle <= 360 && angle > 357))
                    return deltaAngle > 0 ? "Too red" : "Too magenta";

                if (angle >= 35 && angle < 93)
                    return deltaAngle > 0 ? "Too yellow" : "Too red";

                if (angle >= 93 && angle < 157)
                    return deltaAngle > 0 ? "Too green" : "Too yellow";

                if (angle >= 157 && angle < 233)
                    return deltaAngle > 0 ? "Too cyan" : "Too green";

                if (angle >= 233 && angle < 296)
                    return deltaAngle > 0 ? "Too blue" : "Too cyan";

                if (angle >= 296 && angle < 357)
                    return deltaAngle > 0 ? "Too magenta" : "Too blue";

                return "Unknown";
            }
        }
        #endregion Lightness, Chroma and Hue

        #region Dotgain
        public bool HasDotgain { get; private set; }

        public double SampleDotgain { get; private set; }

        public double ReferenceDotgain { get; private set; }
        #endregion Dotgain

        #region G7
        private bool hasG7;
        private bool hasG7WeightedDiffL;
        private bool hasG7WeightedDiffCh;
        private double g7WeightedDiffL;
        private double g7WeightedDiffCh;

        /// <summary>
        /// When <c>true</c> some parts of G7 are active.
        /// </summary>
        public bool HasG7 {
            get => hasG7;
            private set {
                hasG7 = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Is a weighted delta L value available?
        /// </summary>
        public bool HasG7WeightedDiffL {
            get => hasG7WeightedDiffL;
            private set {
                hasG7WeightedDiffL = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Is a weighted delta Ch value available?
        /// </summary>
        public bool HasG7WeightedDiffCh {
            get => hasG7WeightedDiffCh;
            private set {
                hasG7WeightedDiffCh = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// When <see cref="HasG7WeightedDiffL"/> is <c>true</c>, this will hold the actual value.
        /// </summary>
        public double G7WeightedDiffL {
            get => g7WeightedDiffL;
            private set {
                g7WeightedDiffL = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// When <see cref="HasG7WeightedDiffCh"/> is <c>true</c>, this will hold the actual value.
        /// </summary>
        public double G7WeightedDiffCh {
            get => g7WeightedDiffCh;
            private set {
                g7WeightedDiffCh = value;
                OnPropertyChanged();
            }
        }
        #endregion G7

        #region ChromaTrack
        private bool hasChromaTrack;

        public bool HasChromaTrack {
            get { return hasChromaTrack; }
            set {
                hasChromaTrack = value;
                OnPropertyChanged("HasChromaTrack");
            }
        }

        private double bestChromaTrackDensityDifference;

        public double BestChromaTrackDensityDifference {
            get { return bestChromaTrackDensityDifference; }
            set {
                bestChromaTrackDensityDifference = value;
                OnPropertyChanged("BestChromaTrackDensityDifference");
            }
        }

        private double bestChromaTrackPredictedDeltaE;

        public double BestChromaTrackPredictedDeltaE {
            get { return bestChromaTrackPredictedDeltaE; }
            set {
                bestChromaTrackPredictedDeltaE = value;
                OnPropertyChanged("BestChromaTrackPredictedDeltaE");
            }
        }

        private Lab bestPredictedLab;

        public Lab BestPredictedLab {
            get { return bestPredictedLab; }
            set {
                bestPredictedLab = value;
                OnPropertyChanged("BestPredictedLab");
            }
        }
        #endregion ChromaTrack
        #endregion Calculations

        public ColorComparer([NotNull] ISample sample, [NotNull] TReference reference,
                             Option<double> deltaEToleranceOverride) {
            if (sample == null) throw new ArgumentNullException(nameof(sample));
            if (reference == null) throw new ArgumentNullException(nameof(reference));

            Sample = sample;
            Reference = reference;
            DeltaEToleranceOverride = deltaEToleranceOverride;
        }

        public ColorComparer([NotNull] ISample sample, [NotNull] TReference reference,
                             [NotNull] IToleranceSet toleranceSet, Option<double> deltaEToleranceOverride)
            : this(sample, reference, deltaEToleranceOverride) {
            ToleranceSet = toleranceSet ?? throw new ArgumentNullException(nameof(toleranceSet));
        }

        public ColorComparer([NotNull] ISample sample, [NotNull] TReference reference,
                             [NotNull] IToleranceSet toleranceSet, [CanBeNull] Lab scummingSample,
                             Option<double> deltaEToleranceOverride)
            : this(sample, reference, toleranceSet, deltaEToleranceOverride) {
            ScummingSample = scummingSample;
        }

        private void onAllPropertiesChanged() {
            toleranceReport = null;
            OnPropertyChanged(string.Empty);
        }

        public void InitDotgains([NotNull] IHasLabAndDensities referenceWhite,
                                 [NotNull] IHasLabAndDensities referenceFull,
                                 [NotNull] IHasLabAndDensities referenceTarget,
                                 [NotNull] ISample sampleWhite,
                                 [NotNull] ISample sampleFull,
                                 [NotNull] ISample sampleTarget,
                                 int percentage,
                                 CmykComponents cmykComponent,
                                 DotgainMethod dotgainMethod) {
            if (referenceWhite == null) throw new ArgumentNullException(nameof(referenceWhite));
            if (referenceFull == null) throw new ArgumentNullException(nameof(referenceFull));
            if (referenceTarget == null) throw new ArgumentNullException(nameof(referenceTarget));
            if (sampleWhite == null) throw new ArgumentNullException(nameof(sampleWhite));
            if (sampleFull == null) throw new ArgumentNullException(nameof(sampleFull));
            if (sampleTarget == null) throw new ArgumentNullException(nameof(sampleTarget));

            HasDotgain = true;
            switch (dotgainMethod) {
                case DotgainMethod.Density:
                    SampleDotgain = Dotgain.MurrayDaviesDensities(sampleWhite.Densities, sampleFull.Densities,
                                                                  sampleTarget.Densities, percentage);
                    ReferenceDotgain = Dotgain.MurrayDaviesDensities(referenceWhite.Densities,
                                                                     referenceFull.Densities,
                                                                     referenceTarget.Densities, percentage);
                    break;
                case DotgainMethod.Spectral:
                    SampleDotgain = Dotgain.MurrayDaviesSpectral(sampleWhite.AsLab, sampleFull.AsLab,
                                                                 sampleTarget.AsLab,
                                                                 percentage, cmykComponent, false);
                    ReferenceDotgain = Dotgain.MurrayDaviesSpectral(referenceWhite.AsLab, referenceFull.AsLab,
                                                                    referenceTarget.AsLab, percentage, cmykComponent,
                                                                    false);
                    break;
                case DotgainMethod.SpotColorToneValue:
                    SampleDotgain =
                        Dotgain.SpotColorToneValue(sampleWhite.AsLab, sampleFull.AsLab, sampleTarget.AsLab, percentage);
                    ReferenceDotgain =
                        Dotgain.SpotColorToneValue(referenceWhite.AsLab, referenceFull.AsLab, referenceTarget.AsLab,
                                                   percentage);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            onAllPropertiesChanged();
        }

        /// <summary>
        /// Slight hack to override the dens ref dotgain with a given value. Convenient for now, but
        /// should refactor.
        /// </summary>
        public void InitDotgains([NotNull] ILabConvertible referenceWhite,
                                 [NotNull] ILabConvertible referenceFull,
                                 [NotNull] ILabConvertible referenceTarget,
                                 [NotNull] ISample sampleWhite,
                                 [NotNull] ISample sampleFull,
                                 [NotNull] ISample sampleTarget,
                                 int percentage,
                                 CmykComponents cmykComponent,
                                 double overrideReferenceDensityDotgain,
                                 DotgainMethod dotgainMethod,
                                 bool isDotgainOnly) {
            if (referenceWhite == null) throw new ArgumentNullException(nameof(referenceWhite));
            if (referenceFull == null) throw new ArgumentNullException(nameof(referenceFull));
            if (referenceTarget == null) throw new ArgumentNullException(nameof(referenceTarget));
            if (sampleWhite == null) throw new ArgumentNullException(nameof(sampleWhite));
            if (sampleFull == null) throw new ArgumentNullException(nameof(sampleFull));
            if (sampleTarget == null) throw new ArgumentNullException(nameof(sampleTarget));

            HasDotgain = true;
            switch (dotgainMethod) {
                case DotgainMethod.Density:
                    SampleDotgain = Dotgain.MurrayDaviesDensities(sampleWhite.Densities, sampleFull.Densities,
                                                                  sampleTarget.Densities, percentage);
                    ReferenceDotgain = overrideReferenceDensityDotgain;
                    break;
                case DotgainMethod.Spectral:
                    SampleDotgain = Dotgain.MurrayDaviesSpectral(sampleWhite.AsLab, sampleFull.AsLab,
                                                                 sampleTarget.AsLab,
                                                                 percentage, cmykComponent, false);
                    if (isDotgainOnly) {
                        ReferenceDotgain = overrideReferenceDensityDotgain;
                    }
                    else {
                        ReferenceDotgain = Dotgain.MurrayDaviesSpectral(referenceWhite.AsLab, referenceFull.AsLab,
                                                                        referenceTarget.AsLab, percentage,
                                                                        cmykComponent,
                                                                        false);
                    }

                    break;
                case DotgainMethod.SpotColorToneValue:
                    SampleDotgain =
                        Dotgain.SpotColorToneValue(sampleWhite.AsLab, sampleFull.AsLab, sampleTarget.AsLab, percentage);
                    if (isDotgainOnly) {
                        ReferenceDotgain = overrideReferenceDensityDotgain;
                    }
                    else {
                        ReferenceDotgain =
                            Dotgain.SpotColorToneValue(referenceWhite.AsLab, referenceFull.AsLab, referenceTarget.AsLab,
                                                       percentage);
                    }

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            onAllPropertiesChanged();
        }

        public void InitG7ForKeyDotgain(double key, [NotNull] ISample patchSample, [NotNull] ISample substrate,
                                        [NotNull] ISample solidK) {
            if (patchSample == null) throw new ArgumentNullException(nameof(patchSample));
            if (substrate == null) throw new ArgumentNullException(nameof(substrate));
            if (solidK == null) throw new ArgumentNullException(nameof(solidK));
            HasG7 = true;
            G7WeightedDiffL = -G7Calculations.WeightedDiffLK(new G7Patch(key, patchSample), substrate, solidK);
            HasG7WeightedDiffL = true;
            HasG7WeightedDiffCh = false;

            onAllPropertiesChanged();
        }

        public void InitG7ForOverprint([NotNull] ISample overprintSample, [NotNull] ISample substrate) {
            if (overprintSample == null) throw new ArgumentNullException(nameof(overprintSample));
            if (substrate == null) throw new ArgumentNullException(nameof(substrate));
            HasG7 = true;
            G7WeightedDiffCh =
                G7Calculations.WeightedDeltaCh(new G7Patch(100, overprintSample), substrate, overprintSample);
            HasG7WeightedDiffL = false;
            HasG7WeightedDiffCh = true;

            onAllPropertiesChanged();
        }

        public void InitG7ForBalance(double cyan, [NotNull] ISample balanceSample, [NotNull] ISample substrate,
                                     [NotNull] ISample overprintCmy) {
            if (balanceSample == null) throw new ArgumentNullException(nameof(balanceSample));
            if (substrate == null) throw new ArgumentNullException(nameof(substrate));
            if (overprintCmy == null) throw new ArgumentNullException(nameof(overprintCmy));
            HasG7 = true;
            G7WeightedDiffL = G7Calculations.WeightedDiffL3(new G7Patch(cyan, balanceSample), substrate, overprintCmy);
            G7WeightedDiffCh =
                G7Calculations.WeightedDeltaCh(new G7Patch(cyan, balanceSample), substrate, overprintCmy);
            HasG7WeightedDiffL = true;
            HasG7WeightedDiffCh = true;

            onAllPropertiesChanged();
        }

        private void clearG7Results() {
            hasG7 = false;
            hasG7WeightedDiffL = false;
            hasG7WeightedDiffCh = false;
            onAllPropertiesChanged();
        }

        public void InitG7ForCompareResult([CanBeNull] IStripCompareResult result) {
            if (Sample == null || Reference == null || result == null) {
                clearG7Results();
                return;
            }

            var substrate = result.SummaryPatches.FirstPaperWhitePatch;
            if (substrate == null) {
                clearG7Results();
                return;
            }
            switch (Reference.PatchType) {
                case PatchTypes.Dotgain when Reference.IsKey():
                    var solidK = result.SummaryPatches.Patches.FirstOrDefault(x => x.IsKeySolid());
                    if (solidK == null) {
                        clearG7Results();
                        return;
                    }
                    InitG7ForKeyDotgain(Reference.Key, Sample, substrate.Sample, solidK.Sample);
                    break;
                case PatchTypes.Overprint when Reference.IsThreeColorComponent():
                    InitG7ForOverprint(Sample, substrate.Sample);
                    break;
                case PatchTypes.BalanceHighlight:
                case PatchTypes.BalanceMidtone:
                case PatchTypes.BalanceShadow:
                    var overprintCmy = result.SummaryPatches.Patches.FirstOrDefault(x => x.IsThreeColorOverprint());
                    if (overprintCmy == null) {
                        clearG7Results();
                        return;
                    }
                    InitG7ForBalance(Reference.Cyan, Sample, substrate.Sample, overprintCmy.Sample);
                    break;
                default:
                    clearG7Results();
                    break;
            }
        }

        public void InitChromaTrackPredictedDensities(Spectrum paperwhiteSpectrum, IMeasurementConditionsConfig mc) {
            InitChromaTrackPredictedDensities(paperwhiteSpectrum, mc.DensityStandard);
        }

        public void InitChromaTrackPredictedDensities(Spectrum paperwhiteSpectrum, DensityMethod method) {
            if (Sample == null || Reference == null || !Sample.HasSpectrum)
                return;

            var optimizer = new ChromaTrackOptimizer(ToleranceSet);
            try {
                var result = optimizer.Calculate(Sample.AsSpectrum, paperwhiteSpectrum, Reference.AsLab);
                var bestDensityDifference = result.CalculateBestDensityDifference(method);
                var predictedBestDeltaE = result.CalculateBestDeltaE();
                HasChromaTrack = true;
                BestChromaTrackDensityDifference = bestDensityDifference;
                BestChromaTrackPredictedDeltaE = predictedBestDeltaE;
                BestPredictedLab = result.OptimalLab;
            }
            catch (ChromaTrackCalculationException) {
                HasChromaTrack = false;
                BestChromaTrackDensityDifference = 0;
                BestChromaTrackPredictedDeltaE = 0;
                BestPredictedLab = new Lab(0, 0, 0, SpectralMeasurementConditions.Undefined);
            }

            onAllPropertiesChanged();
        }
    }
}