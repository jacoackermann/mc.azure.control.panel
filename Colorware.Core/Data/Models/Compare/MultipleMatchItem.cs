using System;
using System.ComponentModel;

using Colorware.Core.Color;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.Compare {
    // This base class is needed because we can't specify generic classes in XAML
    public abstract class MultipleMatchItem : INotifyPropertyChanged {
        public abstract event PropertyChangedEventHandler PropertyChanged;
    }

    /// <summary>
    /// Simple wrapper which is created to detect multiple matches when a new color is added.
    /// This simple wraps a jobstrippatch reference with the sample we are trying to match and calculates
    /// the deltaE for each access.
    /// </summary>
    public class MultipleMatchItem<T> : MultipleMatchItem where T : ILabConvertible {
        private readonly IToleranceSet toleranceSet;
        public T MatchItem { get; }
        public ISample Sample { get; }

        public double DeltaE { get; }

        public double DeltaL { get; private set; }
        public double Deltac { get; private set; }
        public double Deltah { get; private set; }

        /// <summary>
        /// For second step. This holds the delta E to the closest found reference. This is used
        /// to find any other reference which is too close to leave out of consideration.
        /// </summary>
        public double DeltaEToClosestReference { get; private set; }

        public MultipleMatchItem([NotNull] T matchItem, [NotNull] ISample sample, [NotNull] IToleranceSet toleranceSet) {
            if (matchItem == null) throw new ArgumentNullException(nameof(matchItem));
            if (sample == null) throw new ArgumentNullException(nameof(sample));
            if (toleranceSet == null) throw new ArgumentNullException(nameof(toleranceSet));

            MatchItem = matchItem;
            Sample = sample;
            this.toleranceSet = toleranceSet;

            DeltaE = calculateDeltaE(matchItem.AsLab, sample.AsLab);

            DeltaL = matchItem.AsLab.AsLch.L - sample.AsLch.L;
            Deltac = matchItem.AsLab.AsLch.c - sample.AsLch.c;
            Deltah = matchItem.AsLab.AsLch.h - sample.AsLch.h;
        }

        private double calculateDeltaE(Lab refLab, Lab sampleLab) {
            return toleranceSet.DeltaEUsingCurrentMethod(refLab, sampleLab);
        }

        public void CalculateDeltaEToClosestReference(MultipleMatchItem<T> other) {
            var refLab = MatchItem.AsLab;
            var otherRefLab = other.MatchItem.AsLab;
            DeltaEToClosestReference = toleranceSet.DeltaEUsingCurrentMethod(refLab, otherRefLab);
        }

        // Anti memory leak
#pragma warning disable 0067
        public override event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 0067
    }
}