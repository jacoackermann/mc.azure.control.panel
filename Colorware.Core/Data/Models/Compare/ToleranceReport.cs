using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;

using Colorware.Core.Color;
using Colorware.Core.Enums;
using Colorware.Core.Functional.Option;
using Colorware.Core.Globalization;

namespace Colorware.Core.Data.Models.Compare {
    /// <summary>
    /// TODO: Add support for dotgain calculations.
    /// </summary>
    /// <typeparam name="TReference"></typeparam>
    public class ToleranceReport<TReference> : INotifyPropertyChanged
        where TReference : IHasDensity, IPatchType, IToleranceGroup, IPercentage, ILabConvertible, IHasComponents {
        private readonly ColorComparer<TReference> comparer;

        public ColorComparer<TReference> Comparer => comparer;

        public IToleranceSet ToleranceSet => Comparer.ToleranceSet;

        public ISample Sample => Comparer.Sample;

        public TReference Reference => Comparer.Reference;

        #region Properties Calculations
        public ToleranceGroups ToleranceGroup => Reference.ToleranceGroup;

        public bool IsDeltaEToleranceCalculated =>
            // Note that we can't calculate this as DotgainOnly patches do not have any real color information.
            Reference.PatchType != PatchTypes.DotgainOnly &&
            ToleranceSet.IsDeltaEToleranceCalculated(Reference.ToleranceGroup);

        public bool IsDeltaHToleranceCalculated =>
            // Note that we can't calculate this as DotgainOnly patches do not have any real color information.
            Reference.PatchType != PatchTypes.DotgainOnly &&
            ToleranceSet.IsDeltaHToleranceCalculated(Reference.ToleranceGroup);

        public bool IsDensityToleranceCalculated =>
            // Note that we can't calculate this as DotgainOnly patches do not have any real color information.
            ToleranceSet.IsDensityCalculated(Reference.ToleranceGroup);

        public bool IsDotgainToleranceCalculated => ToleranceSet.IsDotgainToleranceCalculated(Reference.ToleranceGroup);

        public bool IsMaxCalculated => ToleranceSet.IsMaxToleranceCalculated(Reference.ToleranceGroup);

        public bool IsScummingCalculated {
            get {
                if (!Comparer.HasScumming)
                    return false;
                return ToleranceSet.IsScummingCalculated(Reference.ToleranceGroup);
            }
        }

        public bool IsG7WeightedDeltaLCalculated =>
            Comparer.HasG7 &&
            ToleranceSet.IsG7WeightedDeltaLToleranceCalculated(
                Reference.ToleranceGroup,
                Reference.PatchType == PatchTypes.Dotgain && Reference.IsKey(),
                Reference.PatchType == PatchTypes.Overprint && Reference.IsThreeColorComponent());

        public bool IsG7WeightedDeltaChCalculated =>
            Comparer.HasG7 &&
            ToleranceSet.IsG7WeightedDeltaChToleranceCalculated(
                Reference.ToleranceGroup,
                Reference.PatchType == PatchTypes.Dotgain && Reference.IsKey(),
                Reference.PatchType == PatchTypes.Overprint && Reference.IsThreeColorComponent());

        public double UsedDeltaETolerance {
            get {
                if (IsDeltaEToleranceCalculated) {
                    return Comparer.DeltaEToleranceOverride.OrElse(
                        ToleranceSet.DeltaEToleranceFor(Reference.ToleranceGroup));
                }

                return 0.0;
            }
        }

        public double UsedDeltaHTolerance {
            get {
                if (IsDeltaHToleranceCalculated)
                    return ToleranceSet.DeltaHToleranceFor(Reference.ToleranceGroup);
                return 0.0;
            }
        }

        public double UsedDensityTolerance {
            get {
                if (IsDensityToleranceCalculated)
                    return ToleranceSet.DensityToleranceFor(Reference.ToleranceGroup);
                return 0.0;
            }
        }

        public double UsedDotgainTolerance {
            get {
                if (IsDotgainToleranceCalculated)
                    return ToleranceSet.DotgainToleranceFor(Reference.ToleranceGroup);
                return 0.0;
            }
        }

        public double UsedMaxTolerance {
            get {
                if (IsMaxCalculated)
                    return ToleranceSet.MaxToleranceFor(Reference.ToleranceGroup);
                return 0.0;
            }
        }

        public double UsedScummingTolerance {
            get {
                if (IsScummingCalculated)
                    return ToleranceSet.ScummingToleranceFor(Reference.ToleranceGroup);
                return 0.0;
            }
        }

        public double UsedG7WeightedDeltaLTolerance {
            get {
                if (IsG7WeightedDeltaLCalculated)
                    return ToleranceSet.G7WeightedDeltaLPeakToleranceFor(
                        Reference.ToleranceGroup,
                        Reference.PatchType == PatchTypes.Dotgain && Reference.IsKey(),
                        Reference.PatchType == PatchTypes.Overprint &&
                        Reference.IsThreeColorComponent());
                return 0.0;
            }
        }

        public double UsedG7WeightedDeltaChTolerance {
            get {
                if (IsG7WeightedDeltaChCalculated)
                    return ToleranceSet.G7WeightedDeltaChPeakToleranceFor(
                        Reference.ToleranceGroup,
                        Reference.PatchType == PatchTypes.Dotgain && Reference.IsKey(),
                        Reference.PatchType == PatchTypes.Overprint &&
                        Reference.IsThreeColorComponent());
                return 0.0;
            }
        }

        /// <summary>
        /// Was tolerance calculated on this instance?
        /// </summary>
        private bool toleranceWasCalculated;

        private bool inToleranceForDeltaE;

        public bool InToleranceForDeltaE {
            get {
                ensureToleranceCalculated();
                return inToleranceForDeltaE;
            }
        }

        private bool inToleranceForDeltaH;

        public bool InToleranceForDeltaH {
            get {
                ensureToleranceCalculated();
                return inToleranceForDeltaH;
            }
        }

        private bool inToleranceForDensities;

        public bool InToleranceForDensities {
            get {
                ensureToleranceCalculated();
                return inToleranceForDensities;
            }
        }

        private bool inToleranceForDotgain;

        public bool InToleranceForDotgain {
            get {
                ensureToleranceCalculated();
                return inToleranceForDotgain;
            }
        }

        private bool inToleranceForMax;

        public bool InToleranceForMax {
            get {
                ensureToleranceCalculated();
                return inToleranceForMax;
            }
        }

        private bool inToleranceForScumming;

        public bool InToleranceForScumming {
            get {
                ensureToleranceCalculated();
                return inToleranceForScumming;
            }
        }

        private bool inToleranceForG7WeightedDeltaL;

        public bool InToleranceForG7WeightedDeltaL {
            get {
                ensureToleranceCalculated();
                return inToleranceForG7WeightedDeltaL;
            }
        }

        private bool inToleranceForG7WeightedDeltaCh;

        public bool InToleranceForG7WeightedDeltaCh {
            get {
                ensureToleranceCalculated();
                return inToleranceForG7WeightedDeltaCh;
            }
        }

        public bool InTolerance {
            get {
                ensureToleranceCalculated();
                return InToleranceForDeltaE &&
                       InToleranceForDeltaH &&
                       InToleranceForDensities &&
                       InToleranceForDotgain &&
                       InToleranceForMax &&
                       InToleranceForScumming &&
                       InToleranceForG7WeightedDeltaL &&
                       InToleranceForG7WeightedDeltaCh;
            }
        }

        /// <summary>
        /// Returns <c>true</c> if any type of tolerance is to be calculated for this item.
        /// </summary>
        public bool HasTolerance
            => IsDeltaEToleranceCalculated || IsDeltaHToleranceCalculated || IsDensityToleranceCalculated ||
               IsDotgainToleranceCalculated || IsMaxCalculated || IsScummingCalculated ||
               IsG7WeightedDeltaLCalculated || IsG7WeightedDeltaChCalculated;

        /// <summary>
        /// Reasons this comparison is outside of tolerance. If this comparison is NOT outside of
        /// tolerance, this list will be empty.
        /// </summary>
        public List<String> Reasons { get; private set; }

        public String PrimaryReason {
            get {
                if (Reasons == null || Reasons.Count <= 0)
                    return "";
                return Reasons[0];
            }
        }
        #endregion Properties Calculations

        public ToleranceReport(ColorComparer<TReference> comparer) {
            Reasons = new List<string>();
            if (comparer.ToleranceSet == null)
                throw new InvalidOperationException("ColorComparer has no ToleranceSet instance associated to it");
            this.comparer = comparer;
        }

        public ToleranceReport(ISample sample, TReference reference, IToleranceSet toleranceSet,
                               Option<double> deltaEToleranceOverride) :
            this(new ColorComparer<TReference>(sample, reference, toleranceSet, deltaEToleranceOverride)) { }

        private void ensureToleranceCalculated() {
            if (toleranceWasCalculated)
                return;
            calculateInTolerance();
        }

        protected virtual void calculateInTolerance() {
            toleranceWasCalculated = true;
            inToleranceForDeltaE = true;
            inToleranceForDeltaH = true;
            inToleranceForDensities = true;
            inToleranceForDotgain = true;
            inToleranceForMax = true;
            inToleranceForScumming = true;
            inToleranceForG7WeightedDeltaL = true;
            inToleranceForG7WeightedDeltaCh = true;
            if (!IsDeltaEToleranceCalculated && !IsDeltaHToleranceCalculated && !IsDensityToleranceCalculated &&
                !IsDotgainToleranceCalculated && !IsMaxCalculated && !IsScummingCalculated &&
                !IsG7WeightedDeltaLCalculated && !IsG7WeightedDeltaChCalculated) {
                // Always in tolerance.
                return;
            }

            if (IsDeltaEToleranceCalculated)
                calculateDeltaETolerance();
            if (IsDeltaHToleranceCalculated)
                calculateDeltaHTolerance();
            if (IsDensityToleranceCalculated)
                calculateDensityTolerance();
            if (IsDotgainToleranceCalculated)
                calculateDotgainTolerance();
            if (IsMaxCalculated)
                calculateMaxTolerance();
            if (IsScummingCalculated)
                calculateScummingTolerance();
            if (IsG7WeightedDeltaLCalculated)
                calculateWeightedDeltaLTolerance();
            if (IsG7WeightedDeltaChCalculated)
                calculateWeightedDeltaChTolerance();
        }

        protected virtual void calculateDeltaETolerance() {
            var tolerance = UsedDeltaETolerance;
            var deltaE = Comparer.DeltaE;
            if (deltaE > tolerance) {
                inToleranceForDeltaE = false;
                addReason(LanguageManager._("CW.ToleranceReport.ReasonDeltaE"),
                          formatNumber(deltaE), formatNumber(tolerance), ToleranceGroup);
            }
            else
                inToleranceForDeltaE = true;
        }

        protected virtual void calculateDeltaHTolerance() {
            var tolerance = UsedDeltaHTolerance;
            var deltaH = Comparer.DeltaH;
            if (deltaH > tolerance) {
                inToleranceForDeltaH = false;
                addReason(LanguageManager._("CW.ToleranceReport.ReasonDeltaH"),
                          formatNumber(deltaH), formatNumber(tolerance), ToleranceGroup);
            }
            else
                inToleranceForDeltaH = true;
        }

        protected virtual void calculateDensityTolerance() {
            var tolerance = UsedDensityTolerance;
            var deltaDensity = Math.Abs(Comparer.DeltaDensity);
            if (deltaDensity > tolerance) {
                inToleranceForDensities = false;
                addReason(LanguageManager._("CW.ToleranceReport.ReasonDensity"),
                          formatNumber(Sample.Density), formatNumber(Reference.Density), formatNumber(tolerance));
            }
            else
                inToleranceForDensities = true;
        }

        protected virtual void calculateDotgainTolerance() {
            if (!Comparer.HasDotgain) {
                inToleranceForDotgain = true;
                return;
            }

            var tolerance = UsedDotgainTolerance;

            inToleranceForDotgain = checkDotgain(Comparer.SampleDotgain,
                                                 Comparer.ReferenceDotgain, tolerance);
            if (!inToleranceForDotgain) {
                addReason(LanguageManager._("CW.ToleranceReport.ReasonDotgain"),
                          formatNumber(Comparer.SampleDotgain),
                          formatNumber(Comparer.ReferenceDotgain),
                          formatNumber(tolerance));
            }
        }

        protected virtual void calculateMaxTolerance() {
            var tolerance = UsedMaxTolerance;
            var deltaE = 0.0;
            if (Reference.PatchType != PatchTypes.Undefined && Reference.PatchType != PatchTypes.DotgainOnly)
                deltaE = Comparer.DeltaE;
            if (deltaE > tolerance) {
                inToleranceForMax = false;
                addReason(LanguageManager._("CW.ToleranceReport.ReasonMaxDeltaE"),
                          formatNumber(deltaE), formatNumber(tolerance), ToleranceGroup);
            }
            else
                inToleranceForMax = true;
        }

        protected virtual void calculateScummingTolerance() {
            var tolerance = UsedScummingTolerance;
            if (comparer.Scumming > tolerance) {
                inToleranceForScumming = false;
                addReason(LanguageManager._("CW.ToleranceReport.ReasonScumming"),
                          formatNumber(comparer.Scumming), formatNumber(tolerance), ToleranceGroup);
            }
            else
                inToleranceForScumming = true;
        }

        protected virtual void calculateWeightedDeltaLTolerance() {
            var tolerance = UsedG7WeightedDeltaLTolerance;
            if (Math.Abs(comparer.G7WeightedDiffL) > tolerance) {
                inToleranceForG7WeightedDeltaL = false;
                addReason(LanguageManager._("CW.ToleranceReport.ReasonWeightedDeltaL"),
                          formatNumber(comparer.G7WeightedDiffL),
                          formatNumber(tolerance),
                          ToleranceGroup);
            }
            else {
                inToleranceForG7WeightedDeltaL = true;
            }
        }

        protected virtual void calculateWeightedDeltaChTolerance() {
            var tolerance = UsedG7WeightedDeltaChTolerance;
            if (Math.Abs(comparer.G7WeightedDiffCh) > tolerance) {
                inToleranceForG7WeightedDeltaCh = false;
                addReason(LanguageManager._("CW.ToleranceReport.ReasonWeightedDeltaCh"),
                          formatNumber(comparer.G7WeightedDiffCh),
                          formatNumber(tolerance),
                          ToleranceGroup);
            }
            else {
                inToleranceForG7WeightedDeltaCh = true;
            }
        }

        private static bool checkDotgain(double sampleDotgain, double referenceDotgain, double tolerance) {
            var delta = Math.Abs(referenceDotgain - sampleDotgain);
            return delta <= tolerance;
        }

        protected void addReason(String reason) {
            Reasons.Add(reason);
        }

        protected void addReason(String reason, params object[] args) {
            Reasons.Add(String.Format(reason, args));
        }

        /// <summary>
        /// Formats a number so it can be displayed within GUI elements.
        /// </summary>
        /// <param name="number">The number to format.</param>
        /// <returns>A string representing the formated number.</returns>
        protected String formatNumber(double number) {
            return Math.Round(number, 2).ToString(CultureInfo.InvariantCulture);
        }

        // Anti memory leak
#pragma warning disable 0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 0067
    }
}