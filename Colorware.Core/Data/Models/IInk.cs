﻿using System;

using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IInk : IBaseModel, IExternalGuid {
        String Name { get; set; }

        long InkSetId { get; set; }

        IBelongsTo<IInkSet> InkSet { get; }

        long InkTypeId { get; set; }

        IBelongsTo<IInkType> InkType { get; }

        long InkManufacturerId { get; set; }

        IBelongsTo<IInkManufacturer> InkManufacturer { get; }
    }
}