using System;

using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IPaperType : IBaseModel, IExternalGuid {
        String Name { get; set; }

        String Description { get; set; }

        int Number { get; set; }

        IHasManyCollection<IPaper> Papers { get; }

        IHasManyCollection<IReference> References { get; set; }
    }
}