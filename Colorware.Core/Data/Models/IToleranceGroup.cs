using Colorware.Core.Enums;

namespace Colorware.Core.Data.Models {
    public interface IToleranceGroup {
        ToleranceGroups ToleranceGroup { get; set; }
    }
}