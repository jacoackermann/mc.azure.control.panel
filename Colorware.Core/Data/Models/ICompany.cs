﻿using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface ICompany : IBaseModel, IExternalGuid {
        string Name { get; set; }

        string Address1 { get; set; }

        string Address2 { get; set; }

        string City1 { get; set; }

        string City2 { get; set; }

        string ContactPerson { get; set; }

        string Email { get; set; }

        string Mobile { get; set; }

        string Telephone { get; set; }

        string ZipCode1 { get; set; }

        string ZipCode2 { get; set; }

        string Country { get; set; }

        bool IsRemoteCompany { get; set; }
    }
}