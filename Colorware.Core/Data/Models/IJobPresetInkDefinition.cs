﻿namespace Colorware.Core.Data.Models {
    public interface IJobPresetInkDefinition {
        /// <summary>
        /// Position of the ink in the colorbar.
        /// </summary>
        int SlotIndex { get; set; }

        /// <summary>
        /// Position of the ink in the printing unit.
        /// </summary>
        int SequenceIndex { get; set; }

        /// <summary>
        /// Reference patch for this ink.
        /// </summary>
        long ReferencePatchId { get; set; }

        string PantoneLiveObjectGuid { get; set; }

        string PantoneLiveDependentStandardGuid { get; set; }

        string PantoneLivePaletteGuid { get; set; }

        /// <summary>
        /// Id of dotgain curve to use if no values are present. When empty, the default for the reference should be applied.
        /// </summary>
        long DefaultDotgainListId { get; set; }

        /// <summary>
        /// Density override value for solids of this ink.
        /// </summary>
        double SolidDensity { get; set; }

        /// <summary>
        /// Should <see cref="DeltaEToleranceOverride"/> be used?
        /// </summary>
        bool UseDeltaEToleranceOverride { get; set; }

        /// <summary>
        /// Tolerance to use for Delta-E when <see cref="UseDeltaEToleranceOverride"/> has been enabled.
        /// </summary>
        double DeltaEToleranceOverride { get; set; }

        /// <summary>
        /// Name of the reference for this ink.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Optional note for this ink.
        /// </summary>
        string Note { get; set; }

        /// <summary>
        /// Is this item a spot color?
        /// </summary>
        bool IsSpot { get; set; }
    }
}