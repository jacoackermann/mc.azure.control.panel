﻿using System.Collections.Generic;

using Colorware.Core.Color;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

namespace Colorware.Core.Data.Models {
    public interface ICachedCompareResult : IBaseModel, ICreatedStamped, IUpdatedStamped {
        string Name { get; set; }

        double a { get; set; }

        double b { get; set; }

        double L { get; set; }

        SpectralMeasurementConditions MeasurementConditions { get; }

        double DeltaDensity { get; set; }

        double PrintedDensity { get; set; }

        double DeltaE { get; set; }

        double DeltaH { get; set; }

        string Dotgains { get; set; }

        long InkSetId { get; set; }

        long JobId { get; set; }

        long MachineId { get; set; }

        long MeasurementId { get; set; }

        long OkSheetId { get; set; }

        long PaperId { get; set; }

        long UserId { get; set; }

        PatchTypes PatchType { get; set; }

        int Percentage { get; set; }

        string Scores { get; set; }

        int Slot1 { get; set; }

        int Slot2 { get; set; }

        int Slot3 { get; set; }

        int InkZone { get; set; }

        int SingleSlot { get; }

        System.Windows.Media.Color AsColor { get; }

        bool IsSolid { get; }

        double DeltaETwoDecimals { get; }

        /// <summary>
        /// Returns a list where each entry has the format:
        /// {percentage, sampleValue, refValue};
        /// </summary>
        List<List<double>> DotgainValues { get; set; }

        Dictionary<int, int> ScoreValues { get; set; }
    }
}