using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IOpacityResult : IBaseModel, ICreatedStamped {
        long MeasurementId { get; set; }

        double Opacity { get; set; }
    }
}