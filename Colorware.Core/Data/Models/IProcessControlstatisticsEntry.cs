﻿using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

namespace Colorware.Core.Data.Models {
    public interface IProcessControlStatisticsEntry : IBaseModel, ICreatedStamped {
        string UserName { get; set; }

        long JobId { get; set; }

        string JobName { get; set; }

        string JobNumber { get; set; }

        string JobDescription { get; set; }

        int SheetNumber { get; set; }

        string ProductionStatus { get; set; }

        long ClientId { get; set; }

        string ClientName { get; set; }

        string ClientColorbookName { get; set; }

        string ColorbarName { get; set; }

        long MeasurementId { get; set; }

        string MeasurementComment { get; set; }

        long MachineId { get; set; }

        string MachineName { get; set; }

        string PaperTypeName { get; set; }

        int SortOrder { get; set; }

        int InkZone { get; set; }

        string PatchType { get; set; }

        string SampleName { get; set; }

        int SamplePercentage { get; set; }

        double SampleL { get; set; }

        double SampleA { get; set; }

        double SampleB { get; set; }

        double ReferenceL { get; set; }

        double ReferenceA { get; set; }

        double ReferenceB { get; set; }

        double SampleDensity { get; set; }

        double ReferenceDensity { get; set; }

        double SampleDotgain { get; set; }

        double ReferenceDotgain { get; set; }

        double DeltaE { get; set; }

        double DeltaH { get; set; }

        double DeltaDensity { get; set; }

        string SampleSpectrum { get; set; }

        int SampleSpectrumStart { get; set; }

        int SampleSpectrumEnd { get; set; }

        string ToleranceSetName { get; set; }

        double ToleranceDensity { get; set; }

        double ToleranceDotgain { get; set; }

        double ToleranceDeltaE { get; set; }

        string ConditionsName { get; set; }

        string ConditionsStatus { get; set; }

        MCondition ConditionsDensityMCondition { get; set; }

        MCondition ConditionsSpectralMCondition { get; set; }

        string ConditionsIlluminant { get; set; }

        string ConditionsObserverAngle { get; set; }

        bool IsFlexo { get; set; }

        string FlexoRoll { get; set; }

        string FlexoPanel { get; set; }

        string SampleGuid { get; set; }

        string MeasurementGuid { get; set; }

        bool HasScumming { get; set; }

        double ScummingDeltaE { get; set; }

        double ScummingRawBoardL { get; set; }

        double ScummingRawBoardA { get; set; }

        double ScummingRawBoardB { get; set; }

        string RollId { get; set; }

        string CustomFieldName1 { get; set; }

        string CustomFieldData1 { get; set; }

        string CustomFieldName2 { get; set; }

        string CustomFieldData2 { get; set; }

        string CustomFieldName3 { get; set; }

        string CustomFieldData3 { get; set; }

        string CustomFieldName4 { get; set; }

        string CustomFieldData4 { get; set; }

        string CustomFieldName5 { get; set; }

        string CustomFieldData5 { get; set; }

        string CustomFieldName6 { get; set; }

        string CustomFieldData6 { get; set; }
    }
}