using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models {
    public interface IProcessedReportsModels : IBaseModel, ICreatedStamped, IUpdatedStamped {
        /// <summary>
        /// Id of the external Reports server
        /// </summary>
        long ExternalReportsServerId { get; set; }

        /// <summary>
        /// The id of the processed model.
        /// </summary>
        long ModelId { get; set; }

        /// <summary>
        /// Name of the aggregate/model to handle
        /// </summary>
        [NotNull]
        string Aggregate { get; set; }

        ProcessingStatus Status { get; set; }

        [NotNull]
        string Message { get; set; }
    }
}