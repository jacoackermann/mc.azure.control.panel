namespace Colorware.Core.Data.Models {
    public interface IPercentage {
        int Percentage { get; set; }
    }
}