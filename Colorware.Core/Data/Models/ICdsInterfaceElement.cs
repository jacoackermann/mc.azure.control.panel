﻿using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

namespace Colorware.Core.Data.Models {
    public interface ICdsInterfaceElement : IBaseModel {
        

        long JobId { get; set; }

        int FamilyId { get; set; }

        string Name { get; set; }

        int Tint { get; set; }

        string Description { get; set; }

        PatchTypes PatchType { get; set; }

        CdsAttributeTypes AttributeType { get; set; }

        IBelongsTo<IJob> Job { get; }
        bool IsDotgain { get; }
        bool IsDotgainOnly { get; }
        bool IsRegistration { get; }
        bool IsVisual { get; }

        /// <summary>
        /// Can currently only display regular values in the result overview. Visual and registration cannot be displayed.
        /// This property can be used to check or filter for elements that can be displayed.
        /// </summary>
        bool HasDisplayableAttributeType { get; }
    }
}