﻿namespace Colorware.Core.Data.Models {
    /// <summary>
    /// Indicates the status of the last pqm send for a measurement.
    /// </summary>
    public enum LastPqrStatus {
        PqrNotSend,
        PqrSendPending,
        PqrSendOk,
        PqrSendFailed
    }
}