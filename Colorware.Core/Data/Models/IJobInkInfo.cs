﻿using System;

using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IJobInkInfo : IBaseModel {

        long JobId { get; set; }

        IBelongsTo<IJob> Job { get; set; }

        /// <summary>
        /// Position of the ink in the colorbar.
        /// </summary>
        int Slot { get; set; }

        String Name { get; set; }

        String Note { get; set; }
    }
}