﻿using System;

using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IInkSet : IBaseModel, IExternalGuid {
        String Name { get; set; }

        IHasManyCollection<IInk> Inks { get; }
    }
}