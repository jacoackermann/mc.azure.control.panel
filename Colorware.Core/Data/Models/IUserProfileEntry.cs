using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IUserProfileEntry : IBaseModel {
        /// <summary>
        /// The key to refer to for this entry.
        /// </summary>
        string Key { get; set; }

        /// <summary>
        /// The Value for this entry.
        /// </summary>
        string Value { get; set; }

        /// <summary>
        /// Is this entry a custom value?
        /// </summary>
        bool IsCustom { get; set; }

        /// <summary>
        /// The user group id of the group that this item belongs to.
        /// </summary>
        long UserGroupId { get; set;}

        /// <summary>
        /// Category of an entry
        /// </summary>
        string Category { get; set; }

        /// <summary>
        /// The <see cref="UserGroup"/> this entry belongs to.
        /// </summary>
        IBelongsTo<IUserGroup> UserGroup { get; }
    }
}
