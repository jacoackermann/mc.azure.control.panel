using System;
using System.Threading.Tasks;

using Colorware.Core.Data.Models.Printability;
using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IPrintabilityResult : ICreatedStamped, IUpdatedStamped {
        string Name { get; set; }
        long ReferencePatchId { get; set; }
        long InkSetId { get; set; }
        long JobId { get; set; }
        long MeasurementId { get; set; }
        string JobName { get; set; }
        string JobNumber { get; set; }
        DateTime MeasurementDate { get; set; }
        double ChosenDensity { get; set; }
        double DeltaE { get; set; }
        bool IsWet { get; set; }
        IBelongsTo<IReferencePatch> ReferencePatch { get; }
        IBelongsTo<IInkSet> InkSet { get; }
        IBelongsTo<IJob> Job { get; }
        IBelongsTo<IMeasurement> Measurement { get; }

        /// <summary>
        /// Load all printability curves belonging to this printability result.
        /// </summary>
        Task<PrintabilityResultCalculator> LoadPrintabilityResultsCalculatorAsync();

        /// <summary>
        /// Load curve belonging to this result.
        /// </summary>
        Task<PrintabilityCurve> LoadCurveAsync();
    }
}