﻿using System;

using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;

namespace Colorware.Core.Data.Models {
    public interface IColorStripPatch : IBaseModel {
        

        String Name { get; set; }

        int Percentage { get; set; }

        int InkZone { get; set; }

        int Slot1 { get; set; }

        int Slot2 { get; set; }

        int Slot3 { get; set; }

        ToleranceGroups ToleranceGroup { get; set; }

        PatchTypes PatchType { get; set; }

        int SortOrder { get; set; }

        long ColorStripId { get; set; }

        bool HasSlot1 { get; }
        bool HasSlot2 { get; }
        bool HasSlot3 { get; }
        bool IsUndefined { get; }
    }
}