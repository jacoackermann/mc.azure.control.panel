﻿using System;

using Colorware.Core.Algorithms.DeltaEShapeApproximation.DeltaEStrategies;
using Colorware.Core.Color;
using Colorware.Core.Enums;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.ChromaTrack {
    public class ChromaTrackOptimizerResult {
        #region Properties
        /// <summary>
        /// Spectrum of the (measured) solid.
        /// </summary>
        public Spectrum SolidSpectrum { get; }

        /// <summary>
        /// Spectrum of the (measured) paper.
        /// </summary>
        public Spectrum PaperSpectrum { get; }

        /// <summary>
        /// Reference used to get the optimal result.
        /// </summary>
        public Lab ReferenceLab { get; }

        /// <summary>
        /// The resulting optimal predicted curve.
        /// </summary>
        public Spectrum OptimalCurve { get; }

        public IDeltaEStrategy StrategyUsed { get; }

        private Lab optimalLab;

        public DensityMethod? DensityMethod { get; set; }

        /// <summary>
        /// The optimal predicted Lab value.
        /// </summary>
        public Lab OptimalLab => optimalLab ?? (optimalLab = OptimalCurve.ToLab(ReferenceLab.MeasurementConditions));
        #endregion

        public ChromaTrackOptimizerResult([NotNull] Spectrum solidSpectrum,
                                          [NotNull] Spectrum paperSpectrum,
                                          [NotNull] Lab referenceLab,
                                          [NotNull] Spectrum finalCurve,
                                          [NotNull] IDeltaEStrategy strategyUsed) {
            if (solidSpectrum == null) throw new ArgumentNullException(nameof(solidSpectrum));
            if (paperSpectrum == null) throw new ArgumentNullException(nameof(paperSpectrum));
            if (referenceLab == null) throw new ArgumentNullException(nameof(referenceLab));
            if (finalCurve == null) throw new ArgumentNullException(nameof(finalCurve));
            if (strategyUsed == null) throw new ArgumentNullException(nameof(strategyUsed));

            SolidSpectrum = solidSpectrum;
            PaperSpectrum = paperSpectrum;
            ReferenceLab = referenceLab;
            OptimalCurve = finalCurve;
            StrategyUsed = strategyUsed;
        }

        public double CalculateBestDensityDifference([NotNull] IMeasurementConditionsConfig mc) {
            if (mc == null) throw new ArgumentNullException(nameof(mc));

            return CalculateBestDensityDifference(mc.DensityStandard);
        }

        public double CalculateBestDensityDifference(DensityMethod method) {
            var optimalDens = OptimalCurve.ToDensities(method);
            var currentDens = SolidSpectrum.ToDensities(method);
            var filter = currentDens.GetFilteredComponent();
            var diff = optimalDens.GetFilteredValue(filter) - currentDens.GetFilteredValue(filter);
            return diff;
        }

        public CmykComponents GetUsedFilteredComponent(
            [NotNull] IMeasurementConditionsConfig measurementConditionsConfig) {
            if (measurementConditionsConfig == null)
                throw new ArgumentNullException(nameof(measurementConditionsConfig));

            return GetUsedFilteredComponent(measurementConditionsConfig.DensityStandard);
        }

        public CmykComponents GetUsedFilteredComponent(DensityMethod method) {
            var currentDens = SolidSpectrum.ToDensities(method);
            return currentDens.GetFilteredComponent();
        }

        public CMYK CalculateBestDensityDifferenceCMYK([NotNull] IMeasurementConditionsConfig mc) {
            if (mc == null) throw new ArgumentNullException(nameof(mc));

            return CalculateBestDensityDifferenceCMYK(mc.DensityStandard);
        }

        public CMYK CalculateBestDensityDifferenceCMYK(DensityMethod method) {
            var optimalDens = OptimalCurve.ToDensities(method);
            var currentDens = SolidSpectrum.ToDensities(method);
            return optimalDens - currentDens;
        }

        public double CalculateBestDeltaE() {
            return StrategyUsed.DeltaE(OptimalLab, ReferenceLab);
        }

        public Lab AsLab => OptimalLab;
    }
}