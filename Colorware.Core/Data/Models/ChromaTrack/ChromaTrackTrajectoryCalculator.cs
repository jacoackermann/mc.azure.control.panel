﻿using Colorware.Core.Color;
using Colorware.Core.Color.Support;
using Colorware.Core.Color.Support.Exceptions;
using Colorware.Core.Config;

namespace Colorware.Core.Data.Models.ChromaTrack {
    /// <summary>
    /// Generates a `trajectory' curve for a given solid based on Beer's law. The curve passes through the
    /// solid's Lab point.
    /// </summary>
    public class ChromaTrackTrajectoryCalculator {
        public double SubDivisions { get; set; }
        public double KLimit { get; set; }

        /// <summary>
        /// Base density to use. As the calculation of the trajectory uses relative densities, this can be set to the measured density to
        /// set it as the new base. The calculated trajectory will be calculated as offsets to this value.
        /// </summary>
        public double BaseDensity { get; set; }

        /// <summary>
        /// Spectrum of the solid.
        /// </summary>
        private readonly Spectrum solidSpectrum;

        /// <summary>
        /// Spectrum of the paperwhite.
        /// </summary>
        private readonly Spectrum paperSpectrum;

        public ChromaTrackTrajectoryCalculator(Spectrum solid, Spectrum paper) {
            solidSpectrum = solid;
            paperSpectrum = paper;
            SubDivisions = GlobalConfigKeys.ChromaTrackTrajectorySubdivisions;
            KLimit = GlobalConfigKeys.ChromaTrackTrajectoryExtends;
            BaseDensity = 0;
        }

        /// <summary>
        /// Calculate a new trajectory for the given measurement condition.
        /// </summary>
        /// <param name="mc">The measurement condition to use.</param>
        /// <returns>The trajectory result.</returns>
        /// <exception cref="ChromaTrackCalculationException">Thrown when an invalid condition is encountered.</exception>
        public ChromaTrackTrajectoryResult Calculate(IMeasurementConditionsConfig mc) {
            var result = new ChromaTrackTrajectoryResult();
            var step = KLimit / SubDivisions;

            var baseDensityForOffset = solidSpectrum.ToDensities(mc.DensityStandard);

            var k1Entry = new ChromaTrackTrajectoryResultItem(solidSpectrum, paperSpectrum, 1.0, mc) {
                ShowLabel = false,
                BaseDensityForOffset = baseDensityForOffset,
                BaseDensity = BaseDensity
            };
            result.Add(k1Entry);

            for (var i = 1; i <= SubDivisions; i ++) {
                var k1 = 1.0 - i * step;
                var newSpectrum1 = ChromaTrackCalculator.Calculate(solidSpectrum, paperSpectrum, k1);
                result.Add(new ChromaTrackTrajectoryResultItem(newSpectrum1, paperSpectrum, k1, mc) {
                    BaseDensityForOffset = baseDensityForOffset,
                    BaseDensity = BaseDensity
                });

                var k2 = 1.0 + i * step;
                var newSpectrum2 = ChromaTrackCalculator.Calculate(solidSpectrum, paperSpectrum, k2);
                result.Add(new ChromaTrackTrajectoryResultItem(newSpectrum2, paperSpectrum, k2, mc) {
                    BaseDensityForOffset = baseDensityForOffset,
                    BaseDensity = BaseDensity
                });
            }
            result.Sort();
            return result;
        }
    }
}