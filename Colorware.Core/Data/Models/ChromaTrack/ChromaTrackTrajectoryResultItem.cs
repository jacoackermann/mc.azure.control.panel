﻿using Colorware.Core.Color;

namespace Colorware.Core.Data.Models.ChromaTrack {
    /// <summary>
    /// Represents a single point within a Beer's law trajectory curve.
    /// </summary>
    public class ChromaTrackTrajectoryResultItem {
        private readonly Spectrum resultingSpectrum;
        private readonly Spectrum whiteSpectrum;
        public double KFactor { get; private set; }
        private readonly IMeasurementConditionsConfig measurementConditionsConfig;

        public bool ShowLabel { get; set; }

        /// <summary>
        /// Represents the basedensity from which the density offset will be calculated.
        /// </summary>
        public CMYK BaseDensityForOffset { get; set; }

        public double BaseDensity { get; set; }

        public ChromaTrackTrajectoryResultItem(Spectrum resultingSpectrum, Spectrum whiteSpectrum, double kFactor,
            IMeasurementConditionsConfig mc) {
            this.resultingSpectrum = resultingSpectrum;
            this.whiteSpectrum = whiteSpectrum;
            KFactor = kFactor;
            measurementConditionsConfig = mc;
            ShowLabel = true;
            BaseDensity = 0;
            Name = "";
        }

        #region Implementation of INamedLabConvertible
        public Lab AsLab {
            get { return resultingSpectrum.ToLab(measurementConditionsConfig.GetMeasurementConditions()); }
        }

        public Lab RefLab { get; set; }

        private double? density;

        public double Density {
            get {
                if (!density.HasValue) {
                    var dens = resultingSpectrum.ToDensities(measurementConditionsConfig.DensityStandard);
                    /*
                    if (measurementCondition.WhiteBase == WhiteBase.Paper) {
                        var whiteDens = filter.AnsiDensityCMYK(whiteSpectrum);
                        dens = dens - whiteDens;
                    }
                    */
                    density = dens.GetFilteredValue();
                }
                return density.Value;
            }
        }

        private double? densityDiff;

        public double DensityDiff {
            get {
                if (BaseDensityForOffset == null)
                    return 0.0;
                if (!densityDiff.HasValue) {
                    var dens = resultingSpectrum.ToDensities(measurementConditionsConfig.DensityStandard);
                    var component = dens.GetFilteredComponent();
                    dens -= BaseDensityForOffset;
                    densityDiff = dens.GetFilteredValue(component);
                }
                return densityDiff.Value;
            }
        }

        public string Name {
            set {
                
            }
            get {
                if (!ShowLabel)
                    return "";

                var diff = DensityDiff;
                // var final = BaseDensity + diff;
                var sign = diff > 0 ? "+" : "";
                if (RefLab != null)
                    return $"{sign}{diff:F}";

                return $"{sign}{diff:F}d";
            }
        }
        #endregion
    }
}