﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

using Colorware.Core.Color;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.ChromaTrack {
    public class ChromaTrackTrajectoryResult : IEquatable<ChromaTrackTrajectoryResult> {
        [CanBeNull]
        private List<ChromaTrackTrajectoryResultItem> items;

        /// <summary>
        /// The resulting items representing the trajectory curve.
        /// </summary>
        [NotNull]
        public List<ChromaTrackTrajectoryResultItem> Items {
            get { return items ?? (items = new List<ChromaTrackTrajectoryResultItem>()); }
        }

        public ChromaTrackTrajectoryResult() {
        }

        public ChromaTrackTrajectoryResult(List<ChromaTrackTrajectoryResultItem> items) {
            this.items = items;
        }

        /// <summary>
        /// Add an entry to the list of values.
        /// </summary>
        /// <param name="item">The item to add.</param>
        public void Add(ChromaTrackTrajectoryResultItem item) {
            Items.Add(item);
        }

        /// <summary>
        /// Sort the current list of items.
        /// </summary>
        public void Sort() {
            Items.Sort((i1, i2) => i1.KFactor.CompareTo(i2.KFactor));
        }

        public IImmutableList<NamedLab> GetAsIlabConvertibleCollection() {
            return Items.Select(i => new NamedLab(i.AsLab, i.Name)).ToImmutableList();
        }

        public bool Equals(ChromaTrackTrajectoryResult other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return Items.SequenceEqual(other.Items);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((ChromaTrackTrajectoryResult)obj);
        }

        public override int GetHashCode() {
            return Items.GetHashCode();
        }
    }
}