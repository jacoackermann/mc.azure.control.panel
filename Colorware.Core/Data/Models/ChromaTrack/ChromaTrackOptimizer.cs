﻿using System;
using System.Diagnostics;

using Colorware.Core.Algorithms.DeltaEShapeApproximation.DeltaEStrategies;
using Colorware.Core.Color;
using Colorware.Core.Color.Support;
using Colorware.Core.Color.Support.Exceptions;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.ChromaTrack {
    /// <summary>
    /// Finds a (predicted) optimal solid point for a given sample and reference using Beer's law.
    /// </summary>
    public class ChromaTrackOptimizer {
        /// <summary>
        /// The strategy to use to calculate the delta-E distance.
        /// </summary>
        public IDeltaEStrategy Strategy;

        /// <summary>
        /// Delta used to locally differentiate the curve to determine the slope.
        /// </summary>
        public double DeltaStep = 0.001;

        /// <summary>
        /// The starting step to use. This determines how far the algorithm walks across the curve
        /// for each step in k units.
        /// </summary>
        public double StartStep = 0.5;

        /// <summary>
        /// Determines by how much the step is decreased (multiplies the current step by this number).
        /// If this number becomes to small, it may result in inaccurate results. A number of 0.5 should
        /// be optimal for now.
        /// </summary>
        public double StepDecreaseMultiplier = 0.5;

        /// <summary>
        /// Determines how far the algorithm continues until it stops. Lower numbers offer greater precision but take
        /// longer to calculate.
        /// </summary>
        public double EndStepSize = 0.01;

        public ChromaTrackOptimizer(IDeltaEStrategy strategy) {
            Debug.Assert(strategy != null);
            Strategy = strategy;
        }

        public ChromaTrackOptimizer(IToleranceSet toleranceSet) {
            Debug.Assert(toleranceSet != null);
            Strategy = toleranceSet.GetDeltaEStrategy();
        }

        /// <summary>
        /// Calculates the predicted best spectrum that can be achieved for the given solid, paper and reference.
        /// </summary>
        /// <param name="sampleSpectrum">The spectrum of the (measured) sample.</param>
        /// <param name="paperSpectrum">The spectrum of the (measured) paper.</param>
        /// <param name="referenceLab">The reference to find the closest match to.</param>
        /// <returns></returns>
        /// <exception cref="ChromaTrackCalculationException">Thrown when an invalid condition is encountered.</exception>
        [NotNull, Pure]
        public ChromaTrackOptimizerResult Calculate(Spectrum sampleSpectrum, Spectrum paperSpectrum, Lab referenceLab) {
            // Keep track of number of steps take for diagnostics purposes.
            // var steps = 0;

            // K of the left probe point.
            var kLeft = 1.0;

            // The current step size. Will decrease as we get closer to a solution.
            var currentStep = StartStep;

            // of the right probe point.
            var kRight = kLeft + currentStep;

            while (true) {
                // steps++;
                // Calculate slope of left and right probe points to compare.
                var slopeLeft = findSlope(sampleSpectrum, paperSpectrum, referenceLab, kLeft);
                var slopeRight = findSlope(sampleSpectrum, paperSpectrum, referenceLab, kRight);
                if (slopeLeft > 0 && slopeRight > 0) {
                    // Going up, go left.
                    kRight = kLeft;
                    kLeft -= currentStep;
                }
                else if (slopeLeft < 0 && slopeRight < 0) {
                    // Going down right.
                    kLeft = kRight;
                    kRight += currentStep;
                }
                else if (slopeLeft < 0 && slopeRight > 0) {
                    // Should be in the center somewhere.
                    currentStep *= StepDecreaseMultiplier;
                    if (Math.Abs(slopeLeft) < Math.Abs(slopeRight)) {
                        kRight = kLeft + currentStep;
                    }
                    else {
                        kLeft = kRight - currentStep;
                    }
                }
                else if (slopeLeft > 0 && slopeRight < 0) {
                    // Oh noes!!!
                    // May need further research on how to handle this situation.
                    // Seems to happen when the track is illogically placed in regards to the target.
                    // For example, when measuring a cyan when the target is magenta.
                    throw new ChromaTrackCalculationException("No valid solution found");
                }
                else {
                    throw new ChromaTrackCalculationException("Invalid slope");
                }
                if (currentStep <= EndStepSize) {
                    // Debug.WriteLine("Found solution in " + steps + " steps");
                    var avgK = (kLeft + kRight) / 2.0;
                    var finalCurve = ChromaTrackCalculator.Calculate(sampleSpectrum, paperSpectrum, avgK);
                    return new ChromaTrackOptimizerResult(sampleSpectrum, paperSpectrum, referenceLab, finalCurve,
                                                          Strategy);
                }
            }
        }

        /// <summary>
        /// Approximate slope for the given sample and k.
        /// </summary>
        /// <param name="sampleSpectrum">Spectrum of sample.</param>
        /// <param name="paperSpectrum">Spectrum of paper.</param>
        /// <param name="referenceLab">The reference to check the delta-E against.</param>
        /// <param name="currentK">The current k to calculate the slope for.</param>
        /// <returns></returns>
        private double findSlope(Spectrum sampleSpectrum, Spectrum paperSpectrum, Lab referenceLab, double currentK) {
            var sampleLab1 = getLabForK(sampleSpectrum, paperSpectrum, currentK, referenceLab.MeasurementConditions);
            var sampleLab2 = getLabForK(sampleSpectrum, paperSpectrum, currentK + DeltaStep,
                                        referenceLab.MeasurementConditions);

            var deltaE1 = Strategy.DeltaE(sampleLab1, referenceLab);
            var deltaE2 = Strategy.DeltaE(sampleLab2, referenceLab);
            return deltaE2 - deltaE1;
        }

        /// <summary>
        /// Return the lab value for the given k.
        /// </summary>
        /// <param name="sampleSpectrum">Spectrum of the sample.</param>
        /// <param name="paperSpectrum">Spectrum of the paper.</param>
        /// <param name="k">The k to calculate for.</param>
        /// <param name="measurementConditions">The measurement conditions to calculate under.</param>
        /// <returns></returns>
        private Lab getLabForK(Spectrum sampleSpectrum, Spectrum paperSpectrum, double k,
                               SpectralMeasurementConditions measurementConditions) {
            var result = ChromaTrackCalculator.Calculate(sampleSpectrum, paperSpectrum, k);
            return result.ToLab(measurementConditions);
        }
    }
}