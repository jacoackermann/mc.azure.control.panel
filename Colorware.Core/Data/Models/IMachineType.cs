﻿using System;

using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IMachineType : IBaseModel, IExternalGuid {
        String TypeName { get; set; }

        bool FollowFromOkSheet { get; set; }
    }
}