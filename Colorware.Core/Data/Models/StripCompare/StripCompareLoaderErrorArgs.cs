﻿using System;

namespace Colorware.Core.Data.Models.StripCompare {
    public class StripCompareLoaderErrorArgs : EventArgs {
        public String ErrorMessage { get; private set; }

        public StripCompareLoaderErrorArgs(String errorMessage) {
            ErrorMessage = errorMessage;
        }
    }
}