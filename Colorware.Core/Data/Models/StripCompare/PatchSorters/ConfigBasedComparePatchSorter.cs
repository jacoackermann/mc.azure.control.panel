﻿using System;
using System.Collections.Generic;

using Colorware.Core.Config;
using Colorware.Core.Data.Models.StripCompareInterface;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.StripCompare.PatchSorters {
    /// <summary>
    /// Will only sort patches if config key has been set to true.
    /// </summary>
    public class ConfigBasedComparePatchSorter : IStripComparePatchSorter {
        private readonly IStripComparePatchSorter innerSorter;
        private readonly string configKey;
        private readonly IConfig config;

        public ConfigBasedComparePatchSorter([NotNull] IStripComparePatchSorter innerSorter, [NotNull] string configKey,
            [NotNull] IConfig config) {
            if (innerSorter == null) throw new ArgumentNullException("innerSorter");
            if (configKey == null) throw new ArgumentNullException("configKey");
            if (config == null) throw new ArgumentNullException("config");
            this.innerSorter = innerSorter;
            this.configKey = configKey;
            this.config = config;
        }


        public IEnumerable<IStripComparePatch> Sort(IEnumerable<IStripComparePatch> patches) {
            var shouldSort = config.GetBool(configKey, false);
            if (shouldSort)
                return innerSorter.Sort(patches);
            return patches;
        }
    }
}