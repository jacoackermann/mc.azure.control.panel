﻿using System;
using System.Collections.Generic;

using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Enums;

namespace Colorware.Core.Data.Models.StripCompare.PatchSorters {
    /// <summary>
    /// Specifies different sorters depending on set SheetSide />.
    /// </summary>
    public class SheetSideDependentStripComparePatchSorter : IStripComparePatchSorter {
        public SheetSides SheetSide { get; set; }

        private readonly IStripComparePatchSorter topSideSorter, bottomSideSorder, naSorter;

        public SheetSideDependentStripComparePatchSorter(IStripComparePatchSorter topSideSorter,
            IStripComparePatchSorter bottomSideSorder, IStripComparePatchSorter naSorter) {
            this.topSideSorter = topSideSorter;
            this.bottomSideSorder = bottomSideSorder;
            this.naSorter = naSorter;
        }

        public IEnumerable<IStripComparePatch> Sort(IEnumerable<IStripComparePatch> patches) {
            switch (SheetSide) {
                case SheetSides.NA:
                    return naSorter.Sort(patches);
                case SheetSides.Bottom:
                    return bottomSideSorder.Sort(patches);
                case SheetSides.Top:
                    return topSideSorter.Sort(patches);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}