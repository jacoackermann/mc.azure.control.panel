﻿using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Data.Models.StripCompareInterface;

namespace Colorware.Core.Data.Models.StripCompare.PatchSorters {
    /// <summary>
    /// Will check if inkzone of first patch is smaller than inkzone of last patch and flip all patches if
    /// this is not the case.
    /// </summary>
    public class IncreasingStripComparePatchSorter : IStripComparePatchSorter {
        public IEnumerable<IStripComparePatch> Sort(IEnumerable<IStripComparePatch> patches) {
            var stripComparePatches = patches as IList<IStripComparePatch> ?? patches.ToList();
            var firstPatch = stripComparePatches.FirstOrDefault();
            var lastPatch = stripComparePatches.LastOrDefault();
            if (firstPatch == null || lastPatch == null)
                return stripComparePatches;
            if (firstPatch.InkZone > lastPatch.InkZone)
                return stripComparePatches.Reverse();
            return stripComparePatches;
        }
    }
}