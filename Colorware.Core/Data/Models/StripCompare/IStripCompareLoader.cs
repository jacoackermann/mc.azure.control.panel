﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;

using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring;
using Colorware.Core.Enums;
using Colorware.Core.Functional.Result;
using Colorware.Core.Helpers;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.StripCompare {
    public delegate void StripCompareLoaderErrorHandler(object sender, StripCompareLoaderErrorArgs e);

    public interface IStripCompareLoader {
        void SetOptions(StripCompareLoaderOptions newOptions);
        IJob Job { get; }
        IMachine Machine { get; }
        IMeasurement Measurement { get; }
        IColorStrip ColorStrip { get; }
        IToleranceSet ToleranceSet { get; }
        IReferencePatch WhiteReference { get; }
        IReadOnlyList<IReferencePatch> ReferencePatches { get; }
        IReadOnlyList<IJobStripPatch> JobStripPatches { get; }
        IReadOnlyList<ISample> OKSheetSamples { get; }

        IReadOnlyList<IJobStripPatch> JobStripPatchesSliced { get; }

        /// <summary>
        /// Combine the ok sheet samples and the measured samples to determine where they overlap.
        /// This overlap is then returned as the final slice.
        /// </summary>
        IReadOnlyList<ISample> SlicedOkSheetSamples { get; }

        bool HasOkSheet { get; }
        IProgressLoader Progress { get; }
        event StripCompareLoaderErrorHandler Error;
        void UpdateJobStripPatches(IReadOnlyList<IJobStripPatch> newJobStripPatches);
        Task<IStripCompareCalculator> LoadAsync(IJob job, IMeasurement measurement);

        /// <summary>
        /// Recalculate current result. Note, that all results MUST be loaded already.
        /// </summary>
        Task<IStripCompareResult> RecalculateAsync();

        /// <summary>
        /// Recalculate current result with new set of job strip patches.
        /// </summary>
        /// <param name="newJobStripPatches"></param>
        Task<IStripCompareResult> RecalculateAsync(IReadOnlyList<IJobStripPatch> newJobStripPatches);

        Task<IStripCompareResult> CalculateAsync(IJob job, IMeasurement measurement);

        Task<IStripCompareResult> CalculateAsync(IJob job, IMeasurement measurement,
            IStripCompareScoreFactory stripCompareScoreFactory);

        Task<IStripCompareResult> CalculateAsync(IJob job, IMeasurement measurement, IReadOnlyList<ISample> samples,
            IStripCompareScoreFactory stripCompareScoreFactory);

        void Clear();
    }

    public static class StripCompareLoaderExtensions {
        /// <summary>
        /// Helper to load a `IStripCompareResult` and catch any errors to return them as a <see
        /// cref="Result"/>. This way, the calling code doesn't need to listen for error events via
        /// an event handler.
        /// </summary>
        /// <param name="loader">The loader instance to use for loading.</param>
        /// <param name="job">The job to load for.</param>
        /// <param name="measurement">The measurement to load for.</param>
        /// <returns>An OK result if all went well an error result otherwise.</returns>
        public static async Task<Result<IStripCompareResult>> CalculateWithResultAsync([NotNull] this IStripCompareLoader loader, [NotNull] IJob job, [NotNull] IMeasurement measurement) {
            if (loader == null) throw new ArgumentNullException(nameof(loader));
            if (job == null) throw new ArgumentNullException(nameof(job));
            if (measurement == null) throw new ArgumentNullException(nameof(measurement));

            var errors = new List<string>();
            void errorCatcher(object sender, StripCompareLoaderErrorArgs e) {
                errors.Add(e.ErrorMessage);
            }

            loader.Error += errorCatcher;
            try {
                var result = await loader.CalculateAsync(job, measurement);
                if (errors.Any()) {
                    return Result.Fail<IStripCompareResult>(errors.Select(e => new StringError(e)).Cast<IError>().ToImmutableList());
                }
                return Result.Ok(result);
            }
            finally {
                loader.Error -= errorCatcher;
            }
        }
    }
}