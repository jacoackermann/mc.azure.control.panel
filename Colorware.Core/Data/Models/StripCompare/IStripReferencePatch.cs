using System;

using Colorware.Core.Color;
using Colorware.Core.Enums;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.StripCompare {
    public interface IStripReferencePatch : IHasDensity, ILabConvertible, IPatchType, IPercentage, IToleranceGroup,
                                            IInkZone, IHasComponents {
        string Name { get; set; }

        int Slot1 { get; set; }
        int Slot2 { get; set; }
        int Slot3 { get; set; }

        bool HasSlot1 { get; }
        bool HasSlot2 { get; }
        bool HasSlot3 { get; }

        bool IsSpot { get; }

        new double Cyan { get; }
        new double Magenta { get; }
        new double Yellow { get; }
        new double Key { get; }

        Spectrum AsSpectrum { get; }
        bool HasSpectrum { get; }
        String Spectrum { get; }
        int SpectrumStart { get; }
        int SpectrumEnd { get; }
        double L { get; set; }
        double a { get; set; }
        double b { get; set; }
        Lch AsLch { get; }

        Illuminant Illuminant { get; set; }
        Observer Observer { get; set; }
        MCondition MCondition { get; set; }

        [NotNull]
        SpectralMeasurementConditions MeasurementConditions { get; }

        // We can lookup a patch in the PL system using its DepdendentStandard GUID and
        // its Palette GUID
        Guid PantoneLiveDependentStandardGuid { get; set; }
        Guid PantoneLivePaletteGuid { get; set; }
        bool IsPantoneLivePatch { get; }

        double DefaultDotgain { get; set; }
    }
}