﻿using System.Collections.Generic;

using Colorware.Core.Data.Models.StripCompareInterface;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.StripCompare {
    public interface IStripComparePatchSorter {
        [NotNull]
        IEnumerable<IStripComparePatch> Sort([NotNull] IEnumerable<IStripComparePatch> patches);
    }
}