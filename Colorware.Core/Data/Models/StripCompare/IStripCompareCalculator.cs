using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Data.Models.StripCompareInterface.Scoring;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.StripCompare {
    public interface IStripCompareCalculator {
        IStripComparePatchSorter StripComparePatchSorter { get; set; }

        /// <summary>
        /// Set a flag to signify that Beer's Law calculations should be skipped. The final calculated results will not have this information
        /// available if this has been set to <c>true</c>.
        /// </summary>
        bool DisableChromaTrack { get; set; }

        [NotNull]
        Task<IStripCompareResult> Calculate(IReadOnlyList<ISample> samples, IStripCompareScoreFactory stripCompareScoreFactory);

        /// <summary>
        /// Calculate compare data and return the results.
        /// </summary>
        /// <param name="samples">The samples to use, should always be set and have the same count as the jobStripPatches.</param>
        /// <param name="okSheetSamples">If non null, will be used as the ok sheet samples to use. Should have the same size as the jobStripPatches.</param>
        /// <param name="stripCompareScoreFactory">The score factory to use for scoring the result.</param>
        /// <returns>The result of the compare.</returns>
        [NotNull]
        Task<IStripCompareResult> Calculate(IReadOnlyList<ISample> samples, IReadOnlyList<ISample> okSheetSamples,
            IStripCompareScoreFactory stripCompareScoreFactory);
    }
}