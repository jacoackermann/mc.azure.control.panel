using System;

using Colorware.Core.Color;
using Colorware.Core.Enums;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.StripCompare {
    /// <summary>
    /// Simple wrapper to implement ok sheet functionality. This basically
    /// wraps a job strip patch, but overrides the color information with that
    /// of the given sample.
    /// </summary>
    public class OKSheetPatch : IStripReferencePatch, IHasLabAndDensities {
        private readonly IJobStripPatch jobStripPatch;
        private readonly ISample okSheetSample;

        #region Implementation of IComparable
        public string Name {
            get { return jobStripPatch.Name; }
            set { jobStripPatch.Name = value; }
        }

        public Lab AsLab => okSheetSample.AsLab;

        public CMYK Densities => okSheetSample.Densities;
        public double Density => okSheetSample.Density;
        #endregion

        #region Implementation of IStripReferencePatch
        public int Slot1 {
            get { return jobStripPatch.Slot1; }
            set { jobStripPatch.Slot1 = value; }
        }

        public int Slot2 {
            get { return jobStripPatch.Slot2; }
            set { jobStripPatch.Slot2 = value; }
        }

        public int Slot3 {
            get { return jobStripPatch.Slot3; }
            set { jobStripPatch.Slot3 = value; }
        }

        public bool HasSlot1 => Slot1 >= 0;
        public bool HasSlot2 => Slot2 >= 0;
        public bool HasSlot3 => Slot3 >= 0;
        public bool IsSpot => jobStripPatch.IsSpot;

        public double Cyan => jobStripPatch.Cyan;

        public double Magenta => jobStripPatch.Magenta;

        public double Yellow => jobStripPatch.Yellow;

        public double Key => jobStripPatch.Key;
        public Spectrum AsSpectrum => okSheetSample.AsSpectrum;

        public bool HasSpectrum => okSheetSample.HasSpectrum;

        public PatchTypes PatchType {
            get { return jobStripPatch.PatchType; }
            set { jobStripPatch.PatchType = value; }
        }

        public int InkZone {
            get { return jobStripPatch.InkZone; }
            set { jobStripPatch.InkZone = value; }
        }

        public string Spectrum => okSheetSample.Spectrum;

        public int SpectrumStart => okSheetSample.SpectrumStart;

        public int SpectrumEnd => okSheetSample.SpectrumEnd;

        public Illuminant Illuminant {
            get { return jobStripPatch.Illuminant; }
            set { jobStripPatch.Illuminant = value; }
        }

        public Observer Observer {
            get { return jobStripPatch.Observer; }
            set { jobStripPatch.Observer = value; }
        }

        public MCondition MCondition {
            get { return jobStripPatch.MCondition; }
            set { jobStripPatch.MCondition = value; }
        }

        public bool IsPantoneLivePatch => jobStripPatch.IsPantoneLivePatch;

        public double DefaultDotgain {
            get { return jobStripPatch.DefaultDotgain; }
            set { jobStripPatch.DefaultDotgain = value; }
        }

        public SpectralMeasurementConditions MeasurementConditions => jobStripPatch.MeasurementConditions;

        public Guid PantoneLiveDependentStandardGuid {
            get { return jobStripPatch.PantoneLiveDependentStandardGuid; }
            set { jobStripPatch.PantoneLiveDependentStandardGuid = value; }
        }

        public Guid PantoneLivePaletteGuid {
            get { return jobStripPatch.PantoneLivePaletteGuid; }
            set { jobStripPatch.PantoneLivePaletteGuid = value; }
        }

        public double L {
            get { return okSheetSample.L; }
            set { okSheetSample.L = value; }
        }

        public double a {
            get { return okSheetSample.a; }
            set { okSheetSample.a = value; }
        }

        public double b {
            get { return okSheetSample.b; }
            set { okSheetSample.b = value; }
        }

        public Lch AsLch => AsLab.AsLch;
        #endregion

        #region Implementation of IPercentage
        public int Percentage {
            get { return jobStripPatch.Percentage; }
            set { jobStripPatch.Percentage = value; }
        }
        #endregion

        #region Implementation of IToleranceGroup
        public ToleranceGroups ToleranceGroup {
            get { return jobStripPatch.ToleranceGroup; }
            set { jobStripPatch.ToleranceGroup = value; }
        }
        #endregion

        public OKSheetPatch([NotNull] IJobStripPatch jobStripPatch, [NotNull] ISample okSheetSample) {
            if (jobStripPatch == null) throw new ArgumentNullException(nameof(jobStripPatch));
            if (okSheetSample == null) throw new ArgumentNullException(nameof(okSheetSample));

            this.jobStripPatch = jobStripPatch;
            this.okSheetSample = okSheetSample;
        }
    }
}