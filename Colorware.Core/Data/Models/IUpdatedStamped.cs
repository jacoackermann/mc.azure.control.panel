using System;

namespace Colorware.Core.Data.Models {
    public interface IUpdatedStamped {
        DateTime UpdatedAt { get; set; }
    }
}