using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Color;

namespace Colorware.Core.Data.Models.G7Analysis {
    public class G7GrayBalanceDeltaChData {
        #region Properties
        private List<G7GrayBalanceDeltaChDataItem> items;

        public List<G7GrayBalanceDeltaChDataItem> Items => items;

        public bool HasItems => Items != null && Items.Any();
        #endregion

        public G7GrayBalanceDeltaChData() {
            items = new List<G7GrayBalanceDeltaChDataItem>();
        }

        public G7GrayBalanceDeltaChDataItem Add(int tint, double deltaCh, Lab deltaChAsLab) {
            var item = new G7GrayBalanceDeltaChDataItem(tint, deltaCh, deltaChAsLab);
            items.Add(item);
            return item;
        }

        public void OrderByTint() {
            items = items.OrderBy(i => i.Tint).ToList();
        }
    }
}