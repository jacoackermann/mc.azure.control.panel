using Colorware.Core.Data.Models.StripCompareInterface;

namespace Colorware.Core.Data.Models.G7Analysis {
    public class G7AnalysisDataCollection {
        #region Properties
        /// <summary>
        /// Contains data for drawing the target G7 Black Neutral Print Density Curve.
        /// </summary>
        public G7NeutralPrintDensityCurve BlackTargetNpdc { get; private set; }

        public bool HasBlackTargetNpdc => BlackTargetNpdc != null && BlackTargetNpdc.HasItems;

        /// <summary>
        /// Contains data for drawing the measured G7 Black Neutral Print Density Curve.
        /// </summary>
        public G7NeutralPrintDensityCurve BlackMeasuredNpdc { get; private set; }

        public bool HasBlackMeasuredTargetNpdc => BlackMeasuredNpdc != null && BlackMeasuredNpdc.HasItems;

        /// <summary>
        /// Contains data for drawing the target G7 CMY Neutral Print Density Curve.
        /// </summary>
        public G7NeutralPrintDensityCurve CmyTargetNpdc { get; private set; }

        public bool HasCmyTargetNpdc => CmyTargetNpdc != null && CmyTargetNpdc.HasItems;

        /// <summary>
        /// Contains data for drawing the measured G7 CMY Neutral Print Density Curve.
        /// </summary>
        public G7NeutralPrintDensityCurve CmyMeasuredNpdc { get; private set; }

        public bool HasCmyMeasuredNpdc => CmyMeasuredNpdc != null && CmyMeasuredNpdc.HasItems;

        /// <summary>
        /// Contains data for drawing the G7 Black Weighted Diff L curve.
        /// </summary>
        public G7WeightedDiffLData BlackWeightedDiffLData { get; private set; }

        public bool HasBlackWeightedDiffLData => BlackWeightedDiffLData != null && BlackWeightedDiffLData.HasItems;

        /// <summary>
        /// Contains data for drawing the G7 CMY Weighted Diff L curve.
        /// </summary>
        public G7WeightedDiffLData CmyWeightedDiffLData { get; private set; }

        public bool HasCmyWeightedDiffLData => CmyWeightedDiffLData != null && CmyWeightedDiffLData.HasItems;

        /// <summary>
        /// Contains data for drawing the G7 CMY Weighted Delta Ch curve.
        /// </summary>
        public G7WeightedDeltaChData CmyWeightedDeltaChData { get; private set; }

        public bool HasCmyWeightedDeltaChData => CmyWeightedDeltaChData != null && CmyWeightedDeltaChData.HasItems;

        /// <summary>
        /// Contains data for drawing the G7 CMY target CIE a*b* points.
        /// </summary>
        public G7GrayBalanceDeltaChData CmyDeltaChData { get; private set; }

        public bool HasCmyDeltaChData => CmyDeltaChData != null && CmyDeltaChData.HasItems;
        #endregion

        public G7AnalysisDataCollection() {
            BlackTargetNpdc = new G7NeutralPrintDensityCurve();
            BlackMeasuredNpdc = new G7NeutralPrintDensityCurve();
            CmyTargetNpdc = new G7NeutralPrintDensityCurve();
            CmyMeasuredNpdc = new G7NeutralPrintDensityCurve();
            BlackWeightedDiffLData = new G7WeightedDiffLData();
            CmyWeightedDiffLData = new G7WeightedDiffLData();
            CmyWeightedDeltaChData = new G7WeightedDeltaChData();
            CmyDeltaChData = new G7GrayBalanceDeltaChData();
        }

        public void CalculateAll(IStripComparePatches patches) {
            BlackTargetNpdc = G7AnalysisDataCreator.CreateBlackTargetNpdc(patches);
            BlackMeasuredNpdc = G7AnalysisDataCreator.CreateBlackMeasuredNpdc(patches);
            CmyTargetNpdc = G7AnalysisDataCreator.CreateCmyTargetNpdc(patches);
            CmyMeasuredNpdc = G7AnalysisDataCreator.CreateCmyMeasuredNpdc(patches);
            BlackWeightedDiffLData = G7AnalysisDataCreator.CreateBlackWeightedDiffLData(patches);
            CmyWeightedDiffLData = G7AnalysisDataCreator.CreateCmyWeightedDiffLData(patches);
            CmyWeightedDeltaChData = G7AnalysisDataCreator.CreateCmyWeightedDeltaChData(patches);
            CmyDeltaChData = G7AnalysisDataCreator.CreateGreyBalanceDeltaChData(patches);
        }

        public static G7AnalysisDataCollection CreateAnalysisDataCollection(IStripComparePatches patches) {
            var result = new G7AnalysisDataCollection();

            if (patches != null) {
                result.CalculateAll(patches);
            }

            return result;
        }
    }
}