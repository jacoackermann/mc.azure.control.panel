﻿using System;

using Colorware.Core.Color;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Enums;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.G7Analysis {
    /// <summary>
    /// Abstracts information that is required for G7 functions. This information can mostly be
    /// pulled from an <see cref="IStripComparePatch"/> and a helper method is supplied to do just that.
    /// </summary>
    public class G7Patch {
        public ISample Sample { get; }
        public double Tint { get; }
        public SpectralMeasurementConditions MeasurementConditions { get; }

        public G7Patch(double tint, [NotNull] ISample sample) {
            Sample = sample ?? throw new ArgumentNullException(nameof(sample));
            Tint = tint;
            MeasurementConditions = Sample.MeasurementConditions;
        }

        public static G7Patch FromStripComparePatch([NotNull] IStripComparePatch patch) {
            if (patch == null) throw new ArgumentNullException(nameof(patch));
            switch (patch.PatchType) {
                case PatchTypes.Dotgain:
                    if (!patch.IsKeyInk()) {
                        throw new ArgumentException(
                            $"Only key patches are valid for creating a G7 patch from a dotgain patch: {patch.Name}",
                            nameof(patch));
                    }

                    return new G7Patch(patch.Key, patch.Sample);
                case PatchTypes.Overprint:
                    if (!patch.IsThreeColorOverprint()) {
                        throw new ArgumentException(
                            $"Only three color overprint patches are valid for creating a G7 patch from an overprint patch: {patch.Name}",
                            nameof(patch));
                    }

                    return new G7Patch(100, patch.Sample);
                case PatchTypes.BalanceHighlight:
                case PatchTypes.BalanceMidtone:
                case PatchTypes.BalanceShadow:
                    return new G7Patch(patch.Cyan, patch.Sample);

                default:
                    throw new ArgumentException(
                        $"Invalid patch type for creating a G7 patch: {patch.Name} [{patch.PatchType}]", nameof(patch));
            }
        }
    }
}