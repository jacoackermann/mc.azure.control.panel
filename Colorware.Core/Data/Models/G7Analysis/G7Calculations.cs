﻿using System;

using Colorware.Core.Color;
using Colorware.Core.Color.Support;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Enums;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Models.G7Analysis {
    public class G7Calculations {
        // ReSharper disable InconsistentNaming

        /// <summary>
        /// Returns whether this is a grey balance patch according to G7.
        /// </summary>
        /// <param name="patch">The patch to check</param>
        /// <returns>True if the patch is a grey balance</returns>
        public static bool IsPatchGrayBalance([NotNull] IStripComparePatch patch) {
            var greyBalance = G7.NearNeutralToneScale(patch.Cyan);
            return (int)Math.Round(patch.Magenta) == (int)Math.Round(greyBalance.M) &&
                   (int)Math.Round(patch.Yellow) == (int)Math.Round(greyBalance.Y) &&
                   (int)Math.Round(patch.Key) == (int)Math.Round(greyBalance.K);
        }

        /// <summary>
        /// Computes the visual density of the given patch, relative to the substrate
        /// </summary>
        /// <param name="patch">The patch to compute density for</param>
        /// <param name="substrate">The substrate patch</param>
        /// <returns>Visual Density</returns>
        public static double VisualDensity([NotNull] ISample patch, [NotNull] ISample substrate) {
            // Return substrate relative visual density
            if (patch.HasSpectrum && substrate.HasSpectrum)
                // ReSharper disable once PossibleNullReferenceException
                return patch.AsSpectrum.ToDensities(DensityMethod.StatusE, substrate.AsSpectrum).K;
            return patch.DensityK - substrate.DensityK;
        }

        /// <summary>
        /// Computes the absolute density of the given patch
        /// </summary>
        /// <param name="patch">The patch to compute absolute density for</param>
        /// <returns>Absolute Density</returns>
        public static double AbsoluteDensity([NotNull] ISample patch) {
            // Return absolute density for the given patch
            if (patch.HasSpectrum)
                // ReSharper disable once PossibleNullReferenceException
                return patch.AsSpectrum.ToDensities(DensityMethod.StatusE).K;
            else
                return patch.DensityK;
        }

        /// <summary>
        /// Returns the CMY Neutral Print Density value for the given tonevalue.
        /// </summary>
        /// <param name="C">The tone value for Cyan in [0, 100]</param>
        /// <param name="substrate">The substrate patch</param>
        /// <param name="overprintCMY">The CMY overprint patch</param>
        /// <returns>3-color NPD</returns>
        public static double NeutralPrintDensity3(double C, [NotNull] ISample substrate,
                                                  [NotNull] ISample overprintCMY) {
            // Visual density (K) is independent of the density method used, just pick one
            var overprintCMYDensity = VisualDensity(overprintCMY, substrate);
            return G7.NeutralPrintDensity3(C, overprintCMYDensity, 0.0);
        }

        /// <summary>
        /// Returns the K Neutral Print Density value for this patch
        /// </summary>
        /// <param name="K">The tone value for Black in [0, 100]</param>
        /// <param name="substrate">The substrate patch</param>
        /// <param name="solidK">The K solid patch</param>
        /// <returns>Black NPD</returns>
        public static double NeutralPrintDensityK(double K, [NotNull] ISample substrate, [NotNull] ISample solidK) {
            // Visual density (K) is independent of the density method used, just pick one
            var solidKDensity = VisualDensity(solidK, substrate);
            return G7.NeutralPrintDensityK(K, solidKDensity, 0.0);
        }

        /// <summary>
        /// Returns the G7 target CIE L*a*b* for the given grey-balance patch. Target L needs to be
        /// calculated without substrate relativeness. Uses the target a*b* given in CGATS TR15 5.3
        /// </summary>
        /// <param name="patchCyanComponent">
        /// The cyan component (percentage) of the grey-balance patch to compute the target CIE L*a*b*
        /// </param>
        /// <param name="substrate">The substrate patch</param>
        /// <param name="overprintCMY">The CMY overprint patch</param>
        /// <returns>Target CIE L*a*b*</returns>
        public static Lab GetTargetLabCmy(double patchCyanComponent, [NotNull] ISample substrate,
                                          [NotNull] ISample overprintCMY) {
            var overprintCMYDensity = VisualDensity(overprintCMY, substrate);
            var substrateDensity = AbsoluteDensity(substrate);
            return G7.GetTargetLabCMY(patchCyanComponent, overprintCMYDensity, substrateDensity, overprintCMY.AsLab,
                                      substrate.AsLab);
        }

        /// <summary>
        /// Returns the G7 target CIE L*a*b* for the given black tone patch. Target L needs to be
        /// calculated without substrate relativeness. Uses the target a*b* given in CGATS TR15 5.3
        /// </summary>
        /// <param name="patchKeyComponent">
        /// The key component value (percentage/tint) of the black tone patchto compute the target
        /// CIE L*a*b*
        /// </param>
        /// <param name="substrate">The substrate patch</param>
        /// <param name="solidK">The K solid patch</param>
        /// <returns>Target CIE L*a*b*</returns>
        public static Lab GetTargetLabBlack(double patchKeyComponent, [NotNull] ISample substrate,
                                            [NotNull] ISample solidK) {
            var solidKDensity = VisualDensity(solidK, substrate);
            var substrateDensity = AbsoluteDensity(substrate);
            return G7.GetTargetLabK(patchKeyComponent, solidKDensity, substrateDensity, solidK.AsLab, substrate.AsLab);
        }

        /// <summary>
        /// Returns the Delta Ch, the chroma deviation, of the given grey-balance patch.
        /// This compares the patch to the G7 target for a* and b* based on its L* value.
        /// </summary>
        /// <param name="patch">The grey-balance patch to compute the Delta Ch for</param>
        /// <param name="substrate">The substrate patch</param>
        /// <param name="overprintCMY">The CMY overprint patch</param>
        /// <returns>Delta Ch</returns>
        public static double DeltaCh([NotNull] G7Patch patch, [NotNull] ISample substrate,
                                     [NotNull] ISample overprintCMY) {
            var overprintCMYDensity = VisualDensity(overprintCMY, substrate);
            var target = G7.GetTargetLabCMY(patch.Tint, overprintCMYDensity, 0.0, overprintCMY.AsLab,
                                            substrate.AsLab);
            return patch.Sample.AsLab.Distance2D(target);
        }

        /// <summary>
        /// Returns the Weighted Delta Ch, the chroma deviation, of the grey-balance patch.
        /// This compares the patch to the G7 target for a* and b* based on its L* value.
        /// The result is weighted by the function described in section 3.2 of the
        /// G7 master program pass/fail requirements.
        /// </summary>
        /// <param name="patch">The grey-balance patch to compute the Delta Ch for</param>
        /// <param name="substrate">The substrate patch</param>
        /// <param name="overprintCMY">The CMY overprint patch</param>
        /// <returns>Weighted Delta Ch</returns>
        public static double WeightedDeltaCh([NotNull] G7Patch patch, [NotNull] ISample substrate,
                                             [NotNull] ISample overprintCMY) {
            return WeightByPercentage(patch.Tint) * DeltaCh(patch, substrate, overprintCMY);
        }

        /// <summary>
        /// Returns the difference of the L* coordinate in relation to the reference
        /// for the given grey-balance patch.
        /// If the L* of the patch is higher, the return value will be positive.
        /// If the L* of the patch is lower, the return value will be negative.
        /// </summary>
        /// <param name="patch">The grey-balance patch to compute the difference for</param>
        /// <param name="substrate">The substrate patch</param>
        /// <param name="overprintCMY">The CMY overprint patch</param>
        /// <returns>L* difference</returns>
        public static double DiffL3([NotNull] G7Patch patch, [NotNull] ISample substrate,
                                    [NotNull] ISample overprintCMY) {
            var overprintCMYDensity = VisualDensity(overprintCMY, substrate);
            var substrateDensity = AbsoluteDensity(substrate);
            double targetL = G7.GetTargetL3(patch.Tint, overprintCMYDensity, substrateDensity,
                                            patch.Sample.MeasurementConditions);
            return (patch.Sample.AsLab.L - targetL);
        }

        /// <summary>
        /// Returns the Weighted difference of the L* coordinate in relation to the reference
        /// for the given grey-balance patch.
        /// If the L* of the patch is higher, the return value will be positive.
        /// If the L* of the patch is lower, the return value will be negative.
        /// The result is weighted by the function described in section 3.2 of the
        /// G7 master program pass/fail requirements.
        /// </summary>
        /// <param name="patch">The patch to compute the difference for</param>
        /// <param name="substrate">The substrate patch</param>
        /// <param name="overprintCMY">The CMY overprint patch</param>
        /// <returns>Weighted L* difference</returns>
        public static double WeightedDiffL3([NotNull] G7Patch patch, [NotNull] ISample substrate,
                                            [NotNull] ISample overprintCMY) {
            return WeightByPercentage(patch.Tint) * DiffL3(patch, substrate, overprintCMY);
        }

        /// <summary>
        /// Returns the Delta L*, the variance of the L* coordinate in relation to the reference
        /// for the given grey-balance patch.
        /// </summary>
        /// <param name="patch">The patch to compute the variance for</param>
        /// <param name="substrate">The substrate patch</param>
        /// <param name="overprintCMY">The CMY overprint patch</param>
        /// <returns>Delta L*</returns>
        public static double DeltaL3([NotNull] G7Patch patch, [NotNull] ISample substrate,
                                     [NotNull] ISample overprintCMY) {
            var diff = DiffL3(patch, substrate, overprintCMY);
            return Math.Sqrt(diff * diff);
        }

        /// <summary>
        /// Returns the Weighted Delta L*, the variance of the L* coordinate in relation to the reference
        /// for the given grey-balance patch.
        /// The result is weighted by the function described in section 3.2 of the
        /// G7 master program pass/fail requirements.
        /// </summary>
        /// <param name="patch">The patch to compute the variance for</param>
        /// <param name="substrate">The substrate patch</param>
        /// <param name="overprintCMY">The CMY overprint patch</param>
        /// <returns>Weighted Delta L*</returns>
        public static double WeightedDeltaL3([NotNull] G7Patch patch, [NotNull] ISample substrate,
                                             [NotNull] ISample overprintCMY) {
            return WeightByPercentage(patch.Tint) * DeltaL3(patch, substrate, overprintCMY);
        }

        /// <summary>
        /// Returns the difference of the L* coordinate in relation to the reference
        /// for the given black dotgain patch.
        /// If the L* of the patch is higher, the return value will be positive.
        /// If the L* of the patch is lower, the return value will be negative.
        /// </summary>
        /// <param name="patch">The grey-balance patch to compute the difference for</param>
        /// <param name="substrate">The substrate patch</param>
        /// <param name="solidK">The K solid patch</param>
        /// <returns>L* difference</returns>
        public static double DiffLK([NotNull] G7Patch patch, [NotNull] ISample substrate,
                                    [NotNull] ISample solidK) {
            var solidKDensity = VisualDensity(solidK, substrate);
            var substrateDensity = AbsoluteDensity(substrate);
            double targetL = G7.GetTargetLK(patch.Tint, solidKDensity, substrateDensity,
                                            patch.MeasurementConditions);
            return (patch.Sample.L - targetL);
        }

        /// <summary>
        /// Returns the Weighted difference of the L* coordinate in relation to the reference
        /// for the given black dotgain patch.
        /// If the L* of the patch is higher, the return value will be positive.
        /// If the L* of the patch is lower, the return value will be negative.
        /// The result is weighted by the function described in section 3.2 of the
        /// G7 master program pass/fail requirements.
        /// </summary>
        /// <param name="patch">The patch to compute the difference for</param>
        /// <param name="substrate">The substrate patch</param>
        /// <param name="solidK">The K solid patch</param>
        /// <returns>Weighted L* difference</returns>
        public static double WeightedDiffLK([NotNull] G7Patch patch, [NotNull] ISample substrate,
                                            [NotNull] ISample solidK) {
            return WeightByPercentage(patch.Tint) * DiffLK(patch, substrate, solidK);
        }

        /// <summary>
        /// Returns the Delta L*, the variance of the L* coordinate in relation to the reference
        /// for the given black dotgain patch.
        /// </summary>
        /// <param name="patch">The patch to compute the variance for</param>
        /// <param name="substrate">The substrate patch</param>
        /// <param name="solidK">The K solid patch</param>
        /// <returns>Delta L*</returns>
        public static double DeltaLK([NotNull] G7Patch patch, [NotNull] ISample substrate,
                                     [NotNull] ISample solidK) {
            var diff = DiffLK(patch, substrate, solidK);
            return Math.Sqrt(diff * diff);
        }

        /// <summary>
        /// Returns the Weighted Delta L*, the variance of the L* coordinate in relation to the reference
        /// for the given black dotgain patch.
        /// The result is weighted by the function described in section 3.2 of the
        /// G7 master program pass/fail requirements.
        /// </summary>
        /// <param name="patch">The patch to compute the variance for</param>
        /// <param name="substrate">The substrate patch</param>
        /// <param name="solidK">The K solid patch</param>
        /// <returns>Weighted Delta L*</returns>
        public static double WeightedDeltaLK([NotNull] G7Patch patch, [NotNull] ISample substrate,
                                             [NotNull] ISample solidK) {
            return WeightByPercentage(patch.Tint) * DeltaLK(patch, substrate, solidK);
        }

        private static double WeightByPercentage(double percentage) {
            return 1.0 - Math.Max(0.0, (percentage - 50.0) / 50.0 * 0.75);
        }

        // ReSharper restore InconsistentNaming
    }
}