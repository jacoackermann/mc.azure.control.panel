using Colorware.Core.Color;
using Colorware.Core.Functional.Option;

namespace Colorware.Core.Data.Models.G7Analysis {
    /// <summary>
    /// Represents a G7 Neutral Print Density Curve item for a given color patch.
    /// </summary>
    public class G7NeutralPrintDensityCurveItem {
        #region Properties
        public int Tint { get; }

        public double VisualDensity { get; }

        public Option<Lab> TargetLab { get; }
        public Option<Lab> SampleLab { get; }

        public Option<CMYK> GrayBalanceDifference { get; }

        public double BlackNpdcDifference { get; }
        #endregion

        public G7NeutralPrintDensityCurveItem(int tint, double visualDensity, Option<Lab> targetLab,
                                              Option<Lab> sampleLab, Option<CMYK> grayBalanceDifference,
                                              double blackNpdcDifference) {
            Tint = tint;
            VisualDensity = visualDensity;
            TargetLab = targetLab;
            SampleLab = sampleLab;
            GrayBalanceDifference = grayBalanceDifference;
            BlackNpdcDifference = blackNpdcDifference;
        }
    }
}