using System.Collections.Generic;
using System.Linq;

namespace Colorware.Core.Data.Models.G7Analysis {
    public class G7WeightedDiffLData {
        #region Properties
        private List<G7WeightedDiffLDataItem> items;

        public List<G7WeightedDiffLDataItem> Items => items;

        public bool HasItems => Items != null && Items.Any();
        #endregion

        public G7WeightedDiffLData() {
            items = new List<G7WeightedDiffLDataItem>();
        }

        public G7WeightedDiffLDataItem Add(int tint, double weightedDiffL) {
            var item = new G7WeightedDiffLDataItem(tint, weightedDiffL);
            items.Add(item);
            return item;
        }

        public void OrderByTint() {
            items = items.OrderBy(i => i.Tint).ToList();
        }
    }
}