namespace Colorware.Core.Data.Models.G7Analysis {
    /// <summary>
    /// Represents a G7 Weighted Delta Ch data item for a given color patch.
    /// </summary>
    public class G7WeightedDeltaChDataItem {
        #region Properties
        public int Tint { get; }

        public double WeightedDeltaCh { get; }
        #endregion

        public G7WeightedDeltaChDataItem(int tint, double weightedDeltaCh) {
            Tint = tint;
            WeightedDeltaCh = weightedDeltaCh;
        }
    }
}