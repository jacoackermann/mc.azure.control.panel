namespace Colorware.Core.Data.Models.G7Analysis {
    /// <summary>
    /// Represents a G7 Weighted Diff L data item for a given color patch.
    /// </summary>
    public class G7WeightedDiffLDataItem {
        #region Properties
        public int Tint { get; }

        public double WeightedDiffL { get; }
        #endregion

        public G7WeightedDiffLDataItem(int tint, double weightedDiffL) {
            Tint = tint;
            WeightedDiffL = weightedDiffL;
        }
    }
}