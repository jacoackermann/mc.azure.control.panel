using System;
using System.Collections.Generic;
using System.Linq;

namespace Colorware.Core.Data.Models.G7Analysis {
    public static class G7AnalysisPeakAndAverageCalculator {
        /// <summary>
        /// Gets a list of all usable WeightedDiffLValues
        /// </summary>
        /// <param name="cmyWeightedDiffLData"></param>
        /// <returns></returns>
        private static IReadOnlyList<double> listOfUsableWeightedDiffLValues(G7WeightedDiffLData cmyWeightedDiffLData) {
            if (!cmyWeightedDiffLData.Items.Any()) {
                throw new ArgumentNullException(nameof(cmyWeightedDiffLData));
            }

            // Only use true data point values to calculate the average.
            // The first and last data points are always fixed at (0, 0) and (100,0), and must therefore be ignored.
            var count = cmyWeightedDiffLData.Items.Count - 2;
            if (count <= 0) {
                throw new ArgumentNullException(nameof(cmyWeightedDiffLData));
            }

            return cmyWeightedDiffLData.Items.GetRange(1, count)
                                       .Select(p => Math.Abs(p.WeightedDiffL))
                                       .ToList();
        }

        /// <summary>
        /// Gets a list of all usable WeightedDiffChValues
        /// </summary>
        /// <param name="cmyWeightedDiffChData"></param>
        /// <returns></returns>
        private static IReadOnlyList<double> listOfUsableWeightedDeltaChValues(G7WeightedDeltaChData cmyWeightedDiffChData) {
            if (!cmyWeightedDiffChData.Items.Any()) {
                throw new ArgumentNullException(nameof(cmyWeightedDiffChData));
            }

            // Only use true data point values to calculate the average.
            // The first data point is always fixed at (0, 0) and must therefore be ignored.
            var count = cmyWeightedDiffChData.Items.Count - 1;
            if (count <= 0) {
                throw new ArgumentNullException(nameof(cmyWeightedDiffChData));
            }

            return cmyWeightedDiffChData.Items.GetRange(1, count)
                                        .Select(p => Math.Abs(p.WeightedDeltaCh))
                                        .ToList();
        }


        /// <summary>
        /// Calculates the average CMY Weighted Delta L Average
        /// </summary>
        /// <param name="cmyWeightedDiffLData"></param>
        /// <returns></returns>
        public static double AverageWeightedDeltaL(G7WeightedDiffLData cmyWeightedDiffLData) {
            return listOfUsableWeightedDiffLValues(cmyWeightedDiffLData).Average();
        }

        /// <summary>
        /// Calculates the average CMY Weighted Delta L Peak
        /// </summary>
        /// <param name="cmyWeightedDiffLData"></param>
        /// <returns></returns>
        public static double PeakWeightedDeltaL(G7WeightedDiffLData cmyWeightedDiffLData) {
            return listOfUsableWeightedDiffLValues(cmyWeightedDiffLData).Max();
        }

        /// <summary>
        /// Calculates the average CMY Weighted Delta Ch Average
        /// </summary>
        /// <param name="cmyWeightedDeltaChData"></param>
        /// <returns></returns>
        public static double AverageWeightedDeltaCh(G7WeightedDeltaChData cmyWeightedDeltaChData) {
            return listOfUsableWeightedDeltaChValues(cmyWeightedDeltaChData).Average();
        }

        /// <summary>
        /// Calculates the average CMY Weighted Delta Ch Peak
        /// </summary>
        /// <param name="cmyWeightedDeltaChData"></param>
        /// <returns></returns>
        public static double PeakWeightedDeltaCh(G7WeightedDeltaChData cmyWeightedDeltaChData) {
            return listOfUsableWeightedDeltaChValues(cmyWeightedDeltaChData).Max();
        }
    }
}