using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Color;
using Colorware.Core.Functional.Option;

namespace Colorware.Core.Data.Models.G7Analysis {
    public class G7NeutralPrintDensityCurve {
        #region Properties
        private List<G7NeutralPrintDensityCurveItem> items;

        public List<G7NeutralPrintDensityCurveItem> Items => items;

        public bool HasItems => Items != null && Items.Any();
        #endregion

        public G7NeutralPrintDensityCurve() {
            items = new List<G7NeutralPrintDensityCurveItem>();
        }

        public G7NeutralPrintDensityCurveItem Add(int tint, double visualDensity, Option<Lab> targetLab,
                                                  Option<Lab> sampleLab, Option<CMYK> grayBalanceDifference,
                                                  double blackNpdcDifference) {
            var item = new G7NeutralPrintDensityCurveItem(tint, visualDensity, targetLab, sampleLab,
                                                          grayBalanceDifference, blackNpdcDifference);
            items.Add(item);
            return item;
        }

        public void OrderByTint() {
            items = items.OrderBy(i => i.Tint).ToList();
        }
    }
}