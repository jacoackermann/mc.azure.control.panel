using System.Collections.Generic;
using System.Linq;

namespace Colorware.Core.Data.Models.G7Analysis {
    public class G7WeightedDeltaChData {
        #region Properties
        private List<G7WeightedDeltaChDataItem> items;

        public List<G7WeightedDeltaChDataItem> Items => items;

        public bool HasItems => Items != null && Items.Any();
        #endregion

        public G7WeightedDeltaChData() {
            items = new List<G7WeightedDeltaChDataItem>();
        }

        public G7WeightedDeltaChDataItem Add(int tint, double weightedDeltaCh) {
            var item = new G7WeightedDeltaChDataItem(tint, weightedDeltaCh);
            items.Add(item);
            return item;
        }

        public void OrderByTint() {
            items = items.OrderBy(i => i.Tint).ToList();
        }
    }
}