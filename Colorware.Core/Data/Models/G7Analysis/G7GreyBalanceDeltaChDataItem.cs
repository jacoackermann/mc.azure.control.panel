using Colorware.Core.Color;

namespace Colorware.Core.Data.Models.G7Analysis {
    /// <summary>
    /// Represents a G7 Gray Balance delta Ch data item for a given color patch.
    /// </summary>
    public class G7GrayBalanceDeltaChDataItem {
        #region Properties
        public int Tint { get; }

        public double DeltaCh { get; }

        public Lab AsLab { get; }
        #endregion

        public G7GrayBalanceDeltaChDataItem(int tint, double deltaCh, Lab asLab) {
            Tint = tint;
            DeltaCh = deltaCh;
            AsLab = asLab;
        }
    }
}