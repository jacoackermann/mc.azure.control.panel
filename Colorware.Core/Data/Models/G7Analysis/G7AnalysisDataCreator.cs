﻿using System;
using System.Linq;

using Colorware.Core.Color;
using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Extensions;
using Colorware.Core.Functional.Option;

namespace Colorware.Core.Data.Models.G7Analysis {
    /// <summary>
    /// Helper class to create G7 Neutral Print Density Curve data based on the specified input.
    /// </summary>
    public static class G7AnalysisDataCreator {
        /// <summary>
        /// Creates a G7 Black target Neutral Print Density Curve based on a list of measured compare patches.
        /// When conditions for creating the curve are not met, an empty list is returned.
        /// </summary>
        /// <param name="patches">Measured compare patches from which the curve will be created.</param>
        /// <returns>Data points for a G7 Black target Neutral Print Density Curve.</returns>
        public static G7NeutralPrintDensityCurve CreateBlackTargetNpdc(IStripComparePatches patches) {
            var curve = new G7NeutralPrintDensityCurve();

            if (patches == null) {
                return curve;
            }

            var solidK = patches.GetSolidK();
            var substrate = patches.FirstPaperWhitePatch;
            if (solidK != null && substrate != null) {
                // Add visual density of the substrate
                curve.Add(0, 0, Option<Lab>.Nothing, Option<Lab>.Nothing, Option<CMYK>.Nothing, 0.0);

                // Add 1-99% target visual densities
                for (var i = 1; i < 100; i++) {
                    curve.Add(i, G7Calculations.NeutralPrintDensityK(i, substrate.Sample, solidK.Sample), Option<Lab>.Nothing,
                              Option<Lab>.Nothing, Option<CMYK>.Nothing, 0.0);
                }

                // Add visual density of measured solid K patch
                curve.Add(100, G7Calculations.VisualDensity(solidK.Sample, substrate.Sample), Option<Lab>.Nothing, Option<Lab>.Nothing,
                          Option<CMYK>.Nothing, 0.0);
            }

            return curve;
        }

        /// <summary>
        /// Creates a G7 Black measured Neutral Print Density Curve based on a list of measured compare patches.
        /// When conditions for creating the curve are not met, an empty list is returned.
        /// </summary>
        /// <param name="patches">Measured compare patches from which the curve will be created.</param>
        /// <returns>Data points for a G7 Black measured Neutral Print Density Curve.</returns>
        public static G7NeutralPrintDensityCurve CreateBlackMeasuredNpdc(IStripComparePatches patches) {
            var curve = new G7NeutralPrintDensityCurve();

            if (patches == null) {
                return curve;
            }

            var solidK = patches.GetSolidK();
            var substrate = patches.FirstPaperWhitePatch;
            var dotgainPatches = patches.GetKeyDotgainPatches();

            // There must at least be a PW, any Black Dotgain patch (eg. K50) and K100 patch to be able to create the curve.
            if (solidK != null && substrate != null && dotgainPatches.Any()) {
                // Add visual density of the substrate
                curve.Add(0, 0, substrate.SampleLab.ToOption(), substrate.SampleLab.ToOption(),
                          substrate.GrayBalanceDifference.ToOption(), 0.0);

                // Add visual densities of the measured dotgain K patches
                dotgainPatches.ForEach(
                    p => {
                        var visualDensity = G7Calculations.VisualDensity(p.Sample, substrate.Sample);
                        var neutralPrintDensity = G7Calculations.NeutralPrintDensityK(p.Tint, substrate.Sample, solidK.Sample);

                        // For the operator, a clear indicator is needed to inform him about NPDC corrections.
                        // G7 does only use Visual Density or delta-L but we like to display the result in dot size,
                        // just like the GrayFinder result.
                        // Therefore, we can calculate plain densitometric Murray-Davies TVI and display the difference
                        // between sample and target.
                        // Please note that the target TVI is based on actual substrate, actual solidK and
                        // dynamic reference density for the tint patch!

                        // Calculate the Murray-Davies TVI for the black measured patch.
                        var sampleTviBlack = Dotgain.MurrayDaviesDensities(substrate.SampleDensity.K,
                                                                           solidK.SampleDensity.K, visualDensity, p.Tint);
                        // Calculate the Murray-Davies TVI for the black target patch
                        // (use sampleDensity for substrate and Solid, Reference Density for the tint patch).
                        var targetTviBlack = Dotgain.MurrayDaviesDensities(substrate.SampleDensity.K,
                                                                           solidK.SampleDensity.K, neutralPrintDensity,
                                                                           p.Tint);
                        // The difference, only to be used in bar graph display for the operator.
                        var blackNpdcDifference = sampleTviBlack - targetTviBlack;

                        curve.Add(p.Tint, visualDensity,
                                  G7Calculations.GetTargetLabBlack(p.Key, substrate.Sample, solidK.Sample).ToOption(),
                                  p.SampleLab.ToOption(), p.GrayBalanceDifference.ToOption(), blackNpdcDifference);
                    });

                // Add visual density of measured solid K patch
                curve.Add(100, G7Calculations.VisualDensity(solidK.Sample, substrate.Sample), solidK.SampleLab.ToOption(),
                          solidK.SampleLab.ToOption(), solidK.GrayBalanceDifference.ToOption(), 0.0);

                curve.OrderByTint();
            }

            return curve;
        }

        /// <summary>
        /// Creates a G7 CMY target Neutral Print Density Curve based on a list of measured compare patches.
        /// When conditions for creating the curve are not met, an empty list is returned.
        /// </summary>
        /// <param name="patches">Measured compare patches from which the curve will be created.</param>
        /// <returns>Data points for a G7 CMY target Neutral Print Density Curve.</returns>
        public static G7NeutralPrintDensityCurve CreateCmyTargetNpdc(IStripComparePatches patches) {
            var curve = new G7NeutralPrintDensityCurve();

            if (patches == null) {
                return curve;
            }

            var cmyOverprint = patches.GetCmyOverprintPatch();
            var substrate = patches.FirstPaperWhitePatch;
            if (cmyOverprint != null && substrate != null) {
                // Add visual density of the substrate
                curve.Add(0, 0, Option<Lab>.Nothing, Option<Lab>.Nothing, Option<CMYK>.Nothing, 0.0);

                // Add 1-99% target visual densities
                for (var i = 1; i < 100; i++) {
                    curve.Add(i, G7Calculations.NeutralPrintDensity3(i, substrate.Sample, cmyOverprint.Sample), Option<Lab>.Nothing,
                              Option<Lab>.Nothing, Option<CMYK>.Nothing, 0.0);
                }

                // Add visual density of the measured CMY100 patch
                curve.Add(100, G7Calculations.VisualDensity(cmyOverprint.Sample, substrate.Sample), Option<Lab>.Nothing,
                          Option<Lab>.Nothing, Option<CMYK>.Nothing, 0.0);
            }

            return curve;
        }

        /// <summary>
        ///     Creates a G7 CMY measured Neutral Print Density Curve based on a list of measured compare patches.
        ///     When conditions for creating the curve are not met, an empty list is returned.
        /// </summary>
        /// <param name="patches">Measured compare patches from which the curve will be created.</param>
        /// <returns>Data points for a G7 CMY measured Neutral Print Density Curve.</returns>
        public static G7NeutralPrintDensityCurve CreateCmyMeasuredNpdc(IStripComparePatches patches) {
            var curve = new G7NeutralPrintDensityCurve();

            if (patches == null) {
                return curve;
            }

            var cmyOverprint = patches.GetCmyOverprintPatch();
            var substrate = patches.FirstPaperWhitePatch;
            var balancePatches = patches.GetG7GrayBalancePatches();

            // There must at least be a PW, any G7 Balance Patch (eg. BMG7 (50/40/40)) and a CMY100 patch to be able to create the curve.
            if (cmyOverprint != null && substrate != null && balancePatches.Any()) {
                // Add substrate density
                curve.Add(0, 0, substrate.SampleLab.ToOption(), substrate.SampleLab.ToOption(),
                          substrate.GrayBalanceDifference.ToOption(), 0.0);

                // Add visual densities of the measured balance patches
                balancePatches.ForEach(
                    p =>
                    curve.Add(Convert.ToInt32(p.Cyan), G7Calculations.VisualDensity(p.Sample, substrate.Sample),
                              G7Calculations.GetTargetLabCmy(p.Cyan, substrate.Sample, cmyOverprint.Sample).ToOption(),
                              p.SampleLab.ToOption(), p.GrayBalanceDifference.ToOption(), 0.0));

                // Add visual density of the measured CMY100 patch
                curve.Add(100, G7Calculations.VisualDensity(cmyOverprint.Sample, substrate.Sample), cmyOverprint.SampleLab.ToOption(),
                          cmyOverprint.SampleLab.ToOption(), cmyOverprint.GrayBalanceDifference.ToOption(), 0.0);

                curve.OrderByTint();
            }

            return curve;
        }

        /// <summary>
        /// Creates G7 Black Weighted Diff L data based on a list of measured compare patches.
        /// When conditions for creating the curve are not met, an empty list is returned.
        /// </summary>
        /// <param name="patches">Measured compare patches from which the data will be created.</param>
        /// <returns>Data points for a G7 Black Weighted Diff L curve.</returns>
        public static G7WeightedDiffLData CreateBlackWeightedDiffLData(IStripComparePatches patches) {
            var data = new G7WeightedDiffLData();

            if (patches == null) {
                return data;
            }

            var solidK = patches.GetSolidK();
            var substrate = patches.FirstPaperWhitePatch;
            var dotgainPatches = patches.GetKeyDotgainPatches();

            // There must at least be a PW, any Black Dotgain patch (eg. K50) and K100 patch to be able to create the curve.
            if (solidK != null && substrate != null && dotgainPatches.Any()) {
                // Add 0% data item
                data.Add(0, 0);

                // Add measured dotgain K weighted diff L data
                dotgainPatches.ForEach(p => data.Add(p.Tint, G7Calculations.WeightedDiffLK(G7Patch.FromStripComparePatch(p), substrate.Sample, solidK.Sample)));

                // Add 100% data item
                data.Add(100, 0);

                data.OrderByTint();
            }

            return data;
        }

        /// <summary>
        /// Creates G7 CMY Weighted Diff L data based on a list of measured compare patches.
        /// When conditions for creating the curve are not met, an empty list is returned.
        /// </summary>
        /// <param name="patches">Measured compare patches from which the data will be created.</param>
        /// <returns>Data points for a G7 CMY Weighted Diff L curve.</returns>
        public static G7WeightedDiffLData CreateCmyWeightedDiffLData(IStripComparePatches patches) {
            var data = new G7WeightedDiffLData();

            if (patches == null) {
                return data;
            }

            var cmyOverprint = patches.GetCmyOverprintPatch();
            var substrate = patches.FirstPaperWhitePatch;
            var balancePatches = patches.GetG7GrayBalancePatches();

            // There must at least be a PW, any G7 Balance Patch (eg. BMG7 (50/40/40)) and a CMY100 patch to be able to create the curve.
            if (cmyOverprint != null && substrate != null && balancePatches.Any()) {
                // Add 0% data item
                data.Add(0, 0);

                // Add weighted diff L data of the measured balance patches
                balancePatches.ForEach(
                    p => data.Add(Convert.ToInt32(p.Cyan), G7Calculations.WeightedDiffL3(G7Patch.FromStripComparePatch(p), substrate.Sample, cmyOverprint.Sample)));

                // Add 100% data item
                data.Add(100, 0);

                data.OrderByTint();
            }

            return data;
        }

        /// <summary>
        /// Creates G7 CMY Weighted Delta Ch data based on a list of measured compare patches.
        /// When conditions for creating the curve are not met, an empty list is returned.
        /// </summary>
        /// <param name="patches">Measured compare patches from which the data will be created.</param>
        /// <returns>Data points for a G7 CMY Weighted Delta Ch curve.</returns>
        public static G7WeightedDeltaChData CreateCmyWeightedDeltaChData(IStripComparePatches patches) {
            var data = new G7WeightedDeltaChData();

            if (patches == null) {
                return data;
            }

            var cmyOverprint = patches.GetCmyOverprintPatch();
            var substrate = patches.FirstPaperWhitePatch;
            var balancePatches = patches.GetG7GrayBalancePatches();

            // There must at least be a PW, any G7 Balance Patch (eg. BMG7 (50/40/40)) and a CMY100 patch to be able to create the curve.
            if (cmyOverprint != null && substrate != null && balancePatches.Any()) {
                // Add 0% data item
                data.Add(0, 0);

                // Add weighted delta Ch data of the measured balance patches
                balancePatches.ForEach(
                    p => data.Add(Convert.ToInt32(p.Cyan), G7Calculations.WeightedDeltaCh(G7Patch.FromStripComparePatch(p), substrate.Sample, cmyOverprint.Sample)));

                // Add 100% data item
                data.Add(100, G7Calculations.WeightedDeltaCh(G7Patch.FromStripComparePatch(cmyOverprint), substrate.Sample, cmyOverprint.Sample));

                data.OrderByTint();
            }

            return data;
        }

        /// <summary>
        /// Creates G7 Gray Balance delta Ch data based on a list of measured compare patches.
        /// When conditions for creating the data are not met, an empty list is returned.
        /// </summary>
        /// <param name="patches">Measured compare patches from which the data will be created.</param>
        /// <returns>G7 Gray Balance delta Ch data</returns>
        public static G7GrayBalanceDeltaChData CreateGreyBalanceDeltaChData(IStripComparePatches patches) {
            var data = new G7GrayBalanceDeltaChData();

            if (patches == null) {
                return data;
            }

            var cmyOverprint = patches.GetCmyOverprintPatch();
            var substrate = patches.FirstPaperWhitePatch;

            if (cmyOverprint != null && substrate != null) {
                // Find G7 Highlight Contrast patch (CMY 25/19/19)
                patches.GetHcPatch();
                var hcPatch = patches.GetHcPatch();
                if (hcPatch != null) {
                    var sampleLab = hcPatch.SampleLab;
                    var targetLab = G7Calculations.GetTargetLabCmy(hcPatch.Cyan, substrate.Sample, cmyOverprint.Sample);
                    var deltaChAsLab = new Lab(sampleLab.L, sampleLab.a - targetLab.a, sampleLab.b - targetLab.b,
                                               sampleLab.MeasurementConditions);
                    data.Add(Convert.ToInt32(hcPatch.Cyan), G7Calculations.DeltaCh(G7Patch.FromStripComparePatch(hcPatch), substrate.Sample, cmyOverprint.Sample),
                             deltaChAsLab);
                }

                // Find G7 Highlight Range patch (CMY 50/40/40)
                var hrPatch = patches.GetHrPatch();
                if (hrPatch != null) {
                    var sampleLab = hrPatch.SampleLab;
                    var targetLab = G7Calculations.GetTargetLabCmy(hrPatch.Cyan, substrate.Sample, cmyOverprint.Sample);
                    var deltaChAsLab = new Lab(sampleLab.L, sampleLab.a - targetLab.a, sampleLab.b - targetLab.b,
                                               sampleLab.MeasurementConditions);
                    data.Add(Convert.ToInt32(hrPatch.Cyan), G7Calculations.DeltaCh(G7Patch.FromStripComparePatch(hrPatch), substrate.Sample, cmyOverprint.Sample),
                             deltaChAsLab);
                }

                // Find G7 Shadow Contrast patch (CMY 75/66/66)
                var scPatch = patches.GetScPatch();
                if (scPatch != null) {
                    var sampleLab = scPatch.SampleLab;
                    var targetLab = G7Calculations.GetTargetLabCmy(scPatch.Cyan, substrate.Sample, cmyOverprint.Sample);
                    var deltaChAsLab = new Lab(sampleLab.L, sampleLab.a - targetLab.a, sampleLab.b - targetLab.b,
                                               sampleLab.MeasurementConditions);
                    data.Add(Convert.ToInt32(scPatch.Cyan), G7Calculations.DeltaCh(G7Patch.FromStripComparePatch(scPatch), substrate.Sample, cmyOverprint.Sample),
                             deltaChAsLab);
                }
            }

            return data;
        }
    }
}