﻿using System;
using System.Collections.Generic;

using Colorware.Core.Data.Models.StripCompareInterface;
using Colorware.Core.Enums;

namespace Colorware.Core.Data.Models {
    /// <summary>
    /// Represents a set of inkzone lock statuses.
    /// </summary>
    public class InkZoneLockSet {
        public List<InkZoneLock> InkZoneLocks { get; private set; }
        private Dictionary<int, InkZoneLock> inkZoneLocksBySlot;

        public InkZoneLockSet() {
            InkZoneLocks = new List<InkZoneLock>();
            inkZoneLocksBySlot = new Dictionary<int, InkZoneLock>();
        }

        public InkZoneLockSet(IEnumerable<InkZoneLock> inkZoneLocks) {
            InkZoneLocks = new List<InkZoneLock>(inkZoneLocks);
            reindexBySlot();
        }

        public void AddInkZoneLock(InkZoneLock inkZoneLock) {
            if (inkZoneLocksBySlot.ContainsKey(inkZoneLock.Slot)) {
                RemoveInkZoneLock(inkZoneLock.Slot);
            }
            InkZoneLocks.Add(inkZoneLock);
            inkZoneLocksBySlot.Add(inkZoneLock.Slot, inkZoneLock);
        }

        public InkZoneLock RemoveInkZoneLock(int slot) {
            inkZoneLocksBySlot.Remove(slot);
            InkZoneLock found = null;
            foreach (var inkZoneLock in InkZoneLocks) {
                if (inkZoneLock.Slot == slot) {
                    found = inkZoneLock;
                    break;
                }
            }
            if (found != null)
                InkZoneLocks.Remove(found);
            return found;
        }

        public InkZoneLock GetInkZoneLock(int slot) {
            InkZoneLock found;
            inkZoneLocksBySlot.TryGetValue(slot, out found);
            return found;
        }

        public Dictionary<int, InkZoneLock> GetInkZoneLocksBySlot() {
            return inkZoneLocksBySlot;
        }

        private void reindexBySlot() {
            inkZoneLocksBySlot = new Dictionary<int, InkZoneLock>();
            var inkZoneLockCopy = new List<InkZoneLock>(InkZoneLocks);
            foreach (var inkZoneLock in inkZoneLockCopy) {
                if (inkZoneLocksBySlot.ContainsKey(inkZoneLock.Slot)) {
                    RemoveInkZoneLock(inkZoneLock.Slot);
                }
                inkZoneLocksBySlot.Add(inkZoneLock.Slot, inkZoneLock);
            }
        }

        public bool IsLocked(IStripComparePatch patch) {
            switch (patch.PatchType) {
                case PatchTypes.Undefined:
                    return false;
                case PatchTypes.Solid:
                case PatchTypes.Dotgain:
                case PatchTypes.DotgainOnly:
                case PatchTypes.Slur:
                    return isSolidLocked(patch.SingleSlot, patch.InkZone);
                case PatchTypes.Balance:
                case PatchTypes.BalanceHighlight:
                case PatchTypes.BalanceMidtone:
                case PatchTypes.BalanceShadow:
                    return false; // Hmmmm...
                case PatchTypes.Paperwhite:
                    return false;
                case PatchTypes.Overprint:
                    if (patch.Slot1 >= 0 && isSolidLocked(patch.Slot1, patch.InkZone))
                        return true;
                    if (patch.Slot2 >= 0 && isSolidLocked(patch.Slot2, patch.InkZone))
                        return true;
                    if (patch.Slot3 >= 0 && isSolidLocked(patch.Slot3, patch.InkZone))
                        return true;
                    return false;
                case PatchTypes.Other:
                    return false;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private bool isSolidLocked(int slot, int inkZone) {
            var inkZoneLock = GetInkZoneLock(slot);
            if (inkZoneLock == null)
                return false;
            return inkZoneLock.IsLocked(inkZone);
        }
    }
}