﻿using System;

using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Models {
    public interface IUserGroup : IBaseModel {
        

        String Name { get; set; }

        String Description { get; set; }

        Boolean Admin { get; set; }
    }
}