using System;

namespace Colorware.Core.Data.Models {
    public interface ICreatedStamped {
        DateTime CreatedAt { get; set; }
    }
}