﻿using System.Collections.Generic;
using System.ComponentModel;

using Colorware.Core.Color;

namespace Colorware.Core.Data.Models.ColorStripMapper {
    public interface IColorStripMapperOld : INotifyPropertyChanged {
        SpectralMeasurementConditions MeasurementConditions { get; set; }

        IEnumerable<IReferencePatch> ReferencePatches { get; set; }
        IReadOnlyList<IColorStripPatch> ColorStripPatches { get; set; }

        int StartPatchIndex { get; set; }
        int EndPatchIndex { get; set; }
        IEnumerable<IColorStripMapping> Mappings { get; set; }

        /// <summary>
        /// Resulting patches of full strip (ignoring start and end inkzone.
        /// </summary>
        List<IJobStripPatch> ResultingPatches { get; set; }

        Dictionary<int, IDefaultDotgainList> DotgainCurvesPerSlot { get; set; }

        void UpdateMapping();
    }
}