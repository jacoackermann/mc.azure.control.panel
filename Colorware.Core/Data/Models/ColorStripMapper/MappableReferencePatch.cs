﻿using System;
using System.Collections.Generic;
using System.Globalization;

using Colorware.Core.Globalization;
using Colorware.Core.Mvvm;

namespace Colorware.Core.Data.Models.ColorStripMapper {
    /// <summary>
    /// Wraps a ReferencePatch but keeps track of wether this is a client or `normal' reference.
    /// </summary>
    public class MappableReferencePatch : DefaultNotifyPropertyChanged {
        private List<MappableReferencePatch> items;

        // This dummy to prevents binding errors
        public List<MappableReferencePatch> Items {
            get {
                return items ?? (items = new List<MappableReferencePatch> {new MappableReferencePatch(null, false)});
            }
        }

        private IReferencePatch referencePatch;

        public IReferencePatch ReferencePatch {
            get { return referencePatch; }
            set {
                referencePatch = value;
                OnPropertyChanged("ReferencePatch");
            }
        }

        private bool isClientPatch;

        public bool IsClientPatch {
            get { return isClientPatch; }
            set {
                isClientPatch = value;
                OnPropertyChanged("IsClientPatch");
            }
        }

        public String PatchTypeGroupName {
            get {
                if (ReferencePatch == null || ReferencePatch.IsUndefined)
                    return LanguageManager._("CW.Models.Undefined");
                if (IsClientPatch)
                    return LanguageManager._("CW.Models.SpotColors");

                if (ReferencePatch.IsPaperwhite &&
                    ReferencePatch.Name.Equals("PW", StringComparison.InvariantCultureIgnoreCase))
                    return "Paperwhite";

                return LanguageManager._("CW.Models.Reference");
            }
        }

        private static int uniqueId = 0;

        public string ParentColorGroupName {
            get {
                // Give patches that don't belong together a unique ID so they aren't
                // grouped together
                if (ReferencePatch == null)
                    return uniqueId++.ToString(CultureInfo.InvariantCulture);

                // Solids are grouped together with their dotgains
                if (ReferencePatch.IsSolid)
                    return ReferencePatch.Name + "-" + ReferencePatch.Id;
                else if (ReferencePatch.IsDotgain)
                    return ReferencePatch.Name + "-" + ReferencePatch.Parent1Id;
                else if (ReferencePatch.IsOverprint)
                    return "Overprints";
                else if (ReferencePatch.IsPaperwhite)
                    return "Paperwhites";
                else if (ReferencePatch.IsAnyBalance)
                    return "Gray balances";
                else
                    return uniqueId++.ToString(CultureInfo.InvariantCulture);
            }
        }

        // For Image color picker popup; this indents colors underneath their group name so that
        // users can clearly see they belong to a collapsible group
        public bool MustIndentInColorPopup {
            get {
                var rp = ReferencePatch;

                return rp != null && (rp.IsDotgain && rp.Parent1Id > 0) || HasDotgainPatches || rp.IsAnyBalance ||
                       rp.IsOverprint ||
                       (rp.IsPaperwhite && !rp.Name.Equals("PW", StringComparison.InvariantCultureIgnoreCase));
            }
        }

        public static readonly HashSet<long> SolidsWithDotgains = new HashSet<long>();

        public bool HasDotgainPatches {
            get {
                return ReferencePatch != null && ReferencePatch.IsSolid && SolidsWithDotgains.Contains(ReferencePatch.Id);
            }
        }

        public bool IsDefined {
            get {
                if (ReferencePatch == null)
                    return false;
                return ReferencePatch.IsDefined;
            }
        }

        public bool IsUndefined {
            get {
                if (ReferencePatch == null)
                    return true;
                return ReferencePatch.IsUndefined;
            }
        }

        public MappableReferencePatch(IReferencePatch referencePatch, bool isClientPatch) {
            ReferencePatch = referencePatch;
            IsClientPatch = isClientPatch;
        }

        #region Overrides of Object
        public override string ToString() {
            if (ReferencePatch == null)
                return "MappableReferencePatch(NULL)";
            return String.Format("MappableReferencePatch({0})", ReferencePatch);
        }
        #endregion
    }
}