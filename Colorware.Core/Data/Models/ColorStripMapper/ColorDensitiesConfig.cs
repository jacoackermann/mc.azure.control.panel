using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;

namespace Colorware.Core.Data.Models.ColorStripMapper {
    /// <summary>
    /// Serialized items have the form of "'slot';'value'|'slot';'value'|..."
    /// </summary>
    public static class ColorDensitiesConfig {
        private const char ItemSeparator = '|';
        private const char SlotValueSeparator = ';';

        [DebuggerDisplay("DensityConfigItem(Slot={Slot}, Value={Value})")]
        public struct DensityConfigItem {
            public int Slot;
            public double Value;

            public DensityConfigItem(int slot, double value) {
                Slot = slot;
                Value = value;
            }
        }

        public static String SerializeDensities(IEnumerable<DensityConfigItem> items) {
            var result = new StringBuilder();
            var i = 0;
            foreach (var item in items) {
                if (i > 0)
                    result.Append(ItemSeparator);
                result.Append(item.Slot.ToString(CultureInfo.InvariantCulture));
                result.Append(SlotValueSeparator);
                result.Append(item.Value.ToString(CultureInfo.InvariantCulture));
                i++;
            }
            return result.ToString();
        }

        public static List<DensityConfigItem> UnserializeDensities(String densityStr) {
            if (String.IsNullOrEmpty(densityStr))
                return new List<DensityConfigItem>();
            var result = new List<DensityConfigItem>();
            var entries = densityStr.Split(new[] {ItemSeparator});
            var entrySplitter = new[] {SlotValueSeparator};
            foreach (var entry in entries) {
                try {
                    var parts = entry.Split(entrySplitter);
                    if (parts.Length != 2)
                        continue;
                    var slot = int.Parse(parts[0], CultureInfo.InvariantCulture);
                    var value = double.Parse(parts[1], CultureInfo.InvariantCulture);
                    result.Add(new DensityConfigItem(slot, value));
                }
                catch (FormatException) {
                }
            }
            return result;
        }
    }
}