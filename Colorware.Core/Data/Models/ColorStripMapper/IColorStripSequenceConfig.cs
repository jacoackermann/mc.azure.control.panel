using System;
using System.Collections.Generic;

using Colorware.Core.Data.Models.ColorSequence;

namespace Colorware.Core.Data.Models.ColorStripMapper {
    public interface IColorStripSequenceConfig {
        void Save(IPressColorSequence sequence);
        void Load(IEnumerable<IColorStripMapping> mappings, IPressColorSequence colorSequence);

        void Load(IEnumerable<IColorStripMapping> mappings, IPressColorSequence colorSequence,
            String rawSequencesStr);

        void LoadForImageModule(IEnumerable<IJobStripPatch> patches, IPressColorSequence colorSequence,
            String rawSequencesStr);
    }
}