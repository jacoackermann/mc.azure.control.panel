﻿using System.Collections.Generic;
using System.ComponentModel;

using Colorware.Core.Color;

namespace Colorware.Core.Data.Models.ColorStripMapper {
    public interface IColorStripMapping : INotifyPropertyChanged {
        SpectralMeasurementConditions MeasurementConditions { get; set; }

        /// <summary>
        /// Slot id for this mapping.
        /// </summary>
        int Slot { get; set; }

        bool IsUndefined { get; }
        bool IsReference { get; }
        bool IsClientColor { get; }

        /// <summary>
        /// Refers to 100% patch of chosen reference.
        /// </summary>
        MappableReferencePatch ReferencePatch { get; set; }

        /// <summary>
        /// Only used in views to make editing easier.
        /// </summary>
        IReadOnlyList<IReferencePatch> SelectableReferencePatches { get; set; }

        /// <summary>
        /// Only used in views to make editing easier.
        /// </summary>
        IReadOnlyList<IReferencePatch> SelectableClientPatches { get; set; }

        IReadOnlyList<MappableReferencePatch> SelectablePatches { get; set; }

        void SetSelectableReferencePatches(IEnumerable<IReferencePatch> patches, bool isImageMode = false);
        void SetSelectableClientPatches(IEnumerable<IReferencePatch> patches);
    }
}