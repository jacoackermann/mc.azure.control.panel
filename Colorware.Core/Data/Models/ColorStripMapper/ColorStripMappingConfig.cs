using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Colorware.Core.Functional.Option;

namespace Colorware.Core.Data.Models.ColorStripMapper {
    /// <summary>
    /// Helper class to serialize and deserialize a color strip mapping setup to the application config
    /// for a given color strip.
    /// 
    /// General config layout for the value:
    /// ('c' or 'r' or 'u') '.' #index [ '.' #referenceId ] '|' ...
    /// Where 'c', 'r' and 'u' stand for client, regular and undefined references. An optional
    /// referenceId can be present for client colors that links to the reference patch for a given reference.
    /// Example r.1|c.2|c.3.12325|c.1|c.2|u.0|u.0
    /// 
    /// NOTE: Currently does not restore client colors as the update mappings function is not called when the
    /// selection of these changes!
    /// </summary>
    public class ColorStripMappingConfig {
        public enum PatchType {
            Reference,
            Client,
            Undefined
        };

        [DebuggerDisplay("MappingConfig(Index={Index}, TypeOfPatch={TypeOfPatch}, ReferenceId={ReferenceId})")]
        public struct MappingConfig {
            public PatchType TypeOfPatch;
            public int Index;
            public long ReferenceId;
            public string PantoneLiveOid;
        }

        public List<MappingConfig> LoadedConfig;

        private const String BaseConfigKey = "Defaults.Job.ColorStripMapping.";

        private readonly long colorStripId;

        public ColorStripMappingConfig(long colorStripId) {
            this.colorStripId = colorStripId;
        }

        /// <summary>
        /// Loads the specified default mapping.
        /// </summary>
        /// <param name="defaultMapping">The default mapping to de-serialize from.</param>
        /// <param name="allowDefaultOverride">if set to <c>true</c> allow the default mapping to be overriden by values in the config, else the config will be ignored and only the defaultMapping will be used.</param>
        public void Load(String defaultMapping, bool allowDefaultOverride) {
            LoadedConfig = new List<MappingConfig>();
            var str = defaultMapping;
            if (allowDefaultOverride) {
                // Select from config if override is allowed and config has a valid value.
                // If config value is not valid, we return the value to the passed in defaultMapping.
                var key = BaseConfigKey + colorStripId;
                str = Config.Config.DefaultConfig.Get(key, null);
                if (String.IsNullOrEmpty(str))
                    str = defaultMapping;
            }

            if (String.IsNullOrEmpty(str))
                return;

            var parts = str.Split(new[] {'|'}, StringSplitOptions.RemoveEmptyEntries);
            foreach (var part in parts) {
                var subParts = part.Split(new[] {'.'}, StringSplitOptions.RemoveEmptyEntries);
                if (subParts.Length == 2) {
                    try {
                        var index = int.Parse(subParts[1]);
                        var type = PatchType.Undefined;
                        if (subParts[0] == "r")
                            type = PatchType.Reference;

                        if (subParts[0] == "c")
                            type = PatchType.Client;

                        LoadedConfig.Add(new MappingConfig {Index = index, TypeOfPatch = type});
                    }
                    catch (FormatException) {
                        LoadedConfig.Add(new MappingConfig {Index = 0, TypeOfPatch = PatchType.Undefined});
                    }
                }
                else if (subParts.Length == 3) {
                    // Format c.index.referenceId
                    try {
                        if (subParts[0] != "c")
                            continue;

                        var mappingConfig = new MappingConfig {
                            Index = int.Parse(subParts[1]),
                            TypeOfPatch = PatchType.Client,
                        };

                        // PantoneLIVE patch?
                        if (subParts[2].StartsWith("P"))
                            mappingConfig.PantoneLiveOid = subParts[2].Substring(1);
                        else
                            mappingConfig.ReferenceId = int.Parse(subParts[2]);

                        LoadedConfig.Add(mappingConfig);
                    }
                    catch (FormatException) {
                        LoadedConfig.Add(new MappingConfig {Index = 0, TypeOfPatch = PatchType.Undefined});
                    }
                }
            }
        }

        public void InitMapping(int index, IColorStripMapping m) {
            if (LoadedConfig == null)
                throw new Exception("Config was not loaded");

            var indexToGrab = index;
            if (indexToGrab < 0 || indexToGrab > LoadedConfig.Count - 1)
                return;

            var mappingConfig = LoadedConfig[indexToGrab];
            if (mappingConfig.TypeOfPatch == PatchType.Undefined)
                return;

            var patches = mappingConfig.TypeOfPatch == PatchType.Reference
                              ? m.SelectableReferencePatches
                              : m.SelectableClientPatches;
            if (patches == null)
                return;

            if (mappingConfig.TypeOfPatch == PatchType.Reference) {
                if (mappingConfig.Index < 0 || mappingConfig.Index >= patches.Count)
                    return;

                var patch = patches[mappingConfig.Index];
                foreach (var csMapping in m.SelectablePatches) {
                    if (csMapping.ReferencePatch == patch) {
                        m.ReferencePatch = csMapping;
                        break;
                    }
                }
            }
            else if (mappingConfig.TypeOfPatch == PatchType.Client) {
                // PantoneLIVE?
                if (!string.IsNullOrEmpty(mappingConfig.PantoneLiveOid))
                    m.ReferencePatch =
                        m.SelectablePatches.FirstOrDefault(
                            p => {
                                try {
                                    var guidToFind = Guid.Parse(mappingConfig.PantoneLiveOid);
                                    return guidToFind.Equals(
                                        p.ReferencePatch.PantoneLiveObjectGuid.OrElse(Guid.Empty));
                                }
                                catch {
                                    return false;
                                }
                            });
                else
                    m.ReferencePatch =
                        m.SelectablePatches.FirstOrDefault(p => p.ReferencePatch.Id == mappingConfig.ReferenceId);
            }
        }
    }
}