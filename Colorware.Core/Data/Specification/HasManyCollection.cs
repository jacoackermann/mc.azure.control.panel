using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading;
using System.Threading.Tasks;

using Colorware.Core.Mvvm;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Specification {
    // TODO: Can probably bring some functionality of BelongsTo and HasManyCollection together
    // into a common base class.
    public class HasManyCollection<T> : DefaultNotifyPropertyChanged, IHasManyCollection<T> where T : IBaseModel {
        protected readonly IBaseModel parent;
        private readonly string foreignIdField;

        protected readonly SemaphoreSlim loadingSemaphore = new SemaphoreSlim(1, 1);

        // Don't use this except in the 'Items' property
        [CanBeNull]
        private volatile IReadOnlyList<T> items;

        [CanBeNull]
        protected IReadOnlyList<T> Items {
            get { return items; }
            set {
                var loadedChanged = value != items && (value == null || items == null);

                items = value;

                onNotifyCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                if (loadedChanged)
                    OnPropertyChanged("IsLoaded");
            }
        }

        public bool IsLoaded {
            get { return items != null; }
        }

        /// <inheritdoc cref="IHasManyCollection{T}.MockItems"/>
        [CanBeNull]
        public IReadOnlyList<T> MockItems {
            set { items = value; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent">The parent the collection belongs to</param>
        /// <param name="foreignIdField">The name of the foreign key DB column in the parent model's table (e.g. ink_set_id).</param>
        public HasManyCollection(string foreignIdField, IBaseModel parent) {
            this.parent = parent;
            this.foreignIdField = foreignIdField;
        }

        // NB: Optimized to wait for other GetAsync() request to finish retrieving items
        // so that we don't have to
        /// <summary>
        /// Gets the items belonging to this collection, the items will be loaded if not yet available.
        /// </summary>
        /// <param name="forceRefresh">if set to <c>true</c> the items will be reloaded before being returned, even if they are already available.</param>
        /// <param name="cancellationToken">Can be used to cancel the task.</param>
        /// <exception cref="TaskCanceledException" />
        public virtual async Task<IReadOnlyList<T>> GetAsync(bool forceRefresh, CancellationToken cancellationToken) {
            // The lock is an optimization to prevent simultaneous loads of the same collection
            // (with the lock others will wait for the first load to complete and use his result)
            await loadingSemaphore.WaitAsync(cancellationToken).ConfigureAwait(false);

            try {
                if (IsLoaded && !forceRefresh)
                    return Items;

                Items =
                    await
                    BaseModel.FindAllAsync<T>(new Query().Eq(foreignIdField, parent.Id), cancellationToken)
                             .ConfigureAwait(false);
            }
            finally {
                loadingSemaphore.Release();
            }

            return Items;
        }

        public async Task ClearLoadedData() {
            await loadingSemaphore.WaitAsync().ConfigureAwait(false);

            try {
                // Lock so Items isn't filled after we cleared it because a load might still be in progress
                Items = null;
            }
            finally {
                loadingSemaphore.Release();
            }
        }

        #region IEnumerator
        public IEnumerator<T> GetEnumerator() {
            if (!IsLoaded)
                throw new InvalidOperationException(
                    string.Format(
                        "Data collection (type '{0}') must be loaded before being accessed! Call GetAsync() first.",
                        typeof(T).Name));

            return items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            if (!IsLoaded)
                throw new InvalidOperationException(
                    string.Format(
                        "Data collection (type '{0}') must be loaded before being accessed! Call GetAsync() first.",
                        typeof(T).Name));

            return items.GetEnumerator();
        }
        #endregion

        #region INotifyCollectionChanged
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        private void onNotifyCollectionChanged(NotifyCollectionChangedEventArgs args) {
            if (CollectionChanged != null)
                // This must happen on the UI thread or else WPF will raise an exception
                System.Windows.Application.Current.Dispatcher.Invoke(() => CollectionChanged(this, args));
        }
        #endregion INotifyCollectionChanged
    }
}