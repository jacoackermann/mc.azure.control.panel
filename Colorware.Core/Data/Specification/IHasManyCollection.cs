using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Specification {
    public interface IHasManyCollection<T> : IEnumerable<T>, INotifyPropertyChanged, INotifyCollectionChanged
        where T : IBaseModel {
        bool IsLoaded { get; }

        /// <summary>
        /// Can be used to temporarily override the items in the collection at runtime for as long
        /// as the collection is kept in memory.
        /// 
        /// Changes are not persisted.
        /// </summary>
        IReadOnlyList<T> MockItems { set; }

        // NB: Optimized to wait for other GetAsync() request to finish retrieving items
        // so that we don't have to
        /// <summary>
        /// Gets the items belonging to this collection, the items will be loaded if not yet available.
        /// </summary>
        /// <param name="forceRefresh">if set to <c>true</c> the items will be reloaded before being returned, even if they are already available.</param>
        /// <param name="cancellationToken">Can be used to cancel the task.</param>
        /// <exception cref="TaskCanceledException" />
        [ItemNotNull]
        Task<IReadOnlyList<T>> GetAsync(bool forceRefresh, CancellationToken cancellationToken);

        Task ClearLoadedData();
    }

    public static class IHasManyCollectionExtensions {
        [ItemNotNull]
        public static Task<IReadOnlyList<T>> GetAsync<T>(this IHasManyCollection<T> hasManyCollection)
            where T : IBaseModel {
            // ReSharper disable once ExceptionNotDocumented
            return hasManyCollection.GetAsync(false, CancellationToken.None);
        }

        [ItemNotNull]
        public static Task<IReadOnlyList<T>> GetAsync<T>(this IHasManyCollection<T> hasManyCollection, bool forceRefresh)
            where T : IBaseModel {
            // ReSharper disable once ExceptionNotDocumented
            return hasManyCollection.GetAsync(forceRefresh, CancellationToken.None);
        }

        /// <exception cref="TaskCanceledException" />
        [ItemNotNull]
        public static Task<IReadOnlyList<T>> GetAsync<T>(this IHasManyCollection<T> hasManyCollection,
            CancellationToken cancellationToken)
            where T : IBaseModel {
            return hasManyCollection.GetAsync(false, cancellationToken);
        }
    }
}