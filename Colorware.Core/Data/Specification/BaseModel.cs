﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Service;
using Colorware.Core.Extensions;
using Colorware.Core.Helpers;

using JetBrains.Annotations;

using ReactiveUI;

namespace Colorware.Core.Data.Specification {
    public static class BaseModelExtensions {
        public static Task SaveAsync<T>([NotNull] this T model) where T : IBaseModel {
            return BaseModel.GetRepository<T>().SaveAllAsync(new[] {model});
        }

        public static Task DeleteAsync<T>([NotNull] this T model) where T : IBaseModel {
            return BaseModel.GetRepository<T>().DeleteAsync(model);
        }

        private static PropertyInfo getPrimaryKeyProperty([NotNull] IBaseModel baseModel) {
            return BaseModelReflectionHelper.GetPrimaryKeyProperty(baseModel.GetType());
        }

        [Pure]
        public static long GetPrimaryKeyValue(this IBaseModel baseModel) {
            var keyProp = getPrimaryKeyProperty(baseModel);
            return (long)keyProp.GetValue(baseModel, null);
        }

        public static void SetPrimaryKeyValue(this IBaseModel baseModel, long newId) {
            var keyProp = getPrimaryKeyProperty(baseModel);
            keyProp.SetValue(baseModel, newId, null);
        }

        [Pure]
        public static long GetForeignKey(this IBaseModel baseModel, [NotNull] string foreignKeyName) {
            var fKeyProp = baseModel.GetType().GetProperty(foreignKeyName);
            return (long)fKeyProp.GetValue(baseModel, null);
        }

        public static void SetForeignKey(this IBaseModel baseModel, [NotNull] string foreignKeyName, long value) {
            var fKeyProp = baseModel.GetType().GetProperty(foreignKeyName);
            fKeyProp.SetValue(baseModel, value, null);
        }

        [Pure]
        public static string GetTableName(this IBaseModel baseModel) {
            return BaseModelReflectionHelper.GetTableName(baseModel.GetType());
        }

        public static IReadOnlyList<PropertyInfo> GetRelatedProperties(this IBaseModel baseModel) {
            return BaseModelReflectionHelper.GetRelatedProperties(baseModel.GetType());
        }
    }

    public class BaseModel : ReactiveObject, IBaseModel, IEquatable<IBaseModel> {
        /// <summary>
        /// Name of the database field that contains a boolean which represents if the item has been deleted.
        /// </summary>
        public const string IsDeletedDatabaseFieldName = "is_deleted";

        public static void Reset() {
            untypedRepository = null;
        }

        [NotNull]
        internal static IServiceModelRepository<T> GetRepository<T>() where T : IBaseModel {
            return GlobalContainer.Current.Resolve<IServiceModelRepository<T>>();
        }

        private static IUntypedServiceModelRepository untypedRepository;

        [NotNull, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static IUntypedServiceModelRepository UntypedRepository {
            get {
                return untypedRepository ??
                       (untypedRepository = GlobalContainer.Current.Resolve<IUntypedServiceModelRepository>());
            }
        }

        /// <exception cref="TaskCanceledException" />
        [Pure, ItemNotNull]
        public static Task<IReadOnlyList<T>> FindAllAsync<T>(CancellationToken cancellationToken) where T : IBaseModel {
            return FindAllAsync<T>(new Query(), cancellationToken);
        }

        [Pure, ItemNotNull]
        public static Task<IReadOnlyList<T>> FindAllAsync<T>() where T : IBaseModel {
            // ReSharper disable once ExceptionNotDocumented
            return FindAllAsync<T>(CancellationToken.None);
        }

        /// <exception cref="TaskCanceledException" />
        [Pure, ItemNotNull]
        public static Task<IReadOnlyList<T>> FindAllAsync<T>([CanBeNull] Query query,
                                                             CancellationToken cancellationToken)
            where T : IBaseModel {
            return GetRepository<T>().FindAllAsync(query, cancellationToken);
        }

        [Pure, ItemNotNull]
        public static Task<IReadOnlyList<T>> FindAllAsync<T>([CanBeNull] Query query) where T : IBaseModel {
            // ReSharper disable once ExceptionNotDocumented
            return FindAllAsync<T>(query, CancellationToken.None);
        }

        /// <exception cref="TaskCanceledException" />
        [Pure, ItemNotNull]
        public static Task<IReadOnlyList<IBaseModel>> FindAllAsync([NotNull] Type type, [CanBeNull] Query query,
                                                                   CancellationToken cancellationToken) {
            if (type == null) throw new ArgumentNullException("type");
            return UntypedRepository.FindAllAsync(type, query, cancellationToken);
        }

        [Pure, ItemNotNull]
        public static Task<IReadOnlyList<IBaseModel>> FindAllAsync([NotNull] Type type, [CanBeNull] Query query) {
            if (type == null) throw new ArgumentNullException("type");

            // ReSharper disable once ExceptionNotDocumented
            return FindAllAsync(type, query, CancellationToken.None);
        }

        /// <exception cref="TaskCanceledException" />
        [Pure, ItemCanBeNull]
        public static Task<T> FindIncludingDeletedAsync<T>(long id, CancellationToken cancellationToken)
            where T : IBaseModel {
            return GetRepository<T>().FindIncludingDeletedAsync(id, cancellationToken);
        }

        [Pure, ItemCanBeNull]
        public static Task<T> FindIncludingDeletedAsync<T>(long id) where T : IBaseModel {
            // ReSharper disable once ExceptionNotDocumented
            return FindIncludingDeletedAsync<T>(id, CancellationToken.None);
        }

        /// <exception cref="TaskCanceledException" />
        [Pure, ItemCanBeNull]
        public static async Task<IBaseModel> FindIncludingDeletedAsync([NotNull] Type type, long id,
                                                                       CancellationToken cancellationToken) {
            if (type == null) throw new ArgumentNullException("type");

            return (await UntypedRepository
                          .FindAllAsync(type, new Query().Eq("id", id).IncludeDeleted(), cancellationToken)
                          .ConfigureAwait(false))
                .FirstOrDefault();
        }

        [Pure, ItemCanBeNull]
        public static Task<IBaseModel> FindIncludingDeletedAsync([NotNull] Type type, long id) {
            if (type == null) throw new ArgumentNullException("type");

            // ReSharper disable once ExceptionNotDocumented
            return FindIncludingDeletedAsync(type, id, CancellationToken.None);
        }

        public static Task SaveAllAsync<T>([NotNull, ItemNotNull] IReadOnlyList<T> items) where T : IBaseModel {
            if (items == null) throw new ArgumentNullException("items");

            return GetRepository<T>().SaveAllAsync(items);
        }

        public static Task DeleteAllNonCascadingAsync<T>([NotNull] Query q) where T : IBaseModel {
            if (q == null) throw new ArgumentNullException("q");

            return GetRepository<T>().DeleteNonCascadingAsync(q);
        }

        public static Task DeleteAllAsync<T>([NotNull, ItemNotNull] IReadOnlyList<T> items) where T : IBaseModel {
            if (items == null) throw new ArgumentNullException("items");

            return GetRepository<T>().DeleteAllAsync(items);
        }

        private bool newInstance = true;

        [ServiceField("id", IsPrimaryKey = true)]
        public long Id { get; set; }

        [ServiceField("is_deleted")]
        public bool IsDeleted { get; set; } = false;

        public bool NewInstance {
            get { return newInstance; }
            set { newInstance = value; }
        }

        public bool Equals(IBaseModel other) {
            if (other == null || other.NewInstance || NewInstance)
                return ReferenceEquals(this, other);

            return GetType() == other.GetType() && Id == other.Id;
        }

        /// <summary>
        /// Equality for BaseModels is defined by them being the same type and them having the same
        /// primary id.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj) {
            var other = obj as IBaseModel;

            if (other == null)
                return false;

            return Equals(other);
        }

        public override int GetHashCode() {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Perform a shallow clone copying all properties marked with a ServiceField attribute.
        /// Note that this also copies the current saved state and Id, so saving the instance will
        /// overwrite any existing data for that instance.
        /// </summary>
        /// <typeparam name="T">The type of the object being cloned.</typeparam>
        /// <returns>A new instance with it's service field properties copied from the original object instance.</returns>
        public virtual T Clone<T>() where T : IBaseModel {
            var type = GlobalContainer.Current.GetImplementationType<T>();

            var instance = ReflectionHelpers.CreateInstance<T>(type);
            instance.NewInstance = NewInstance;
            var props = BaseModelReflectionHelper.GetServiceFieldProperties<T>();
            foreach (var prop in props) {
                var value = prop.GetValue(this, null);
                prop.SetValue(instance, value, null);
            }

            return instance;
        }

        /// <summary>
        /// Like Clone, but sets the clone up to be a new object with NewInstance = true and
        /// with its ID reset.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public virtual T CloneNew<T>() where T : IBaseModel {
            var instance = Clone<T>();
            instance.NewInstance = true;
            instance.Id = 0;
            if (instance is IExternalGuid asExternalGuid) {
                asExternalGuid.ExternalGuid = Guid.NewGuid();
            }

            return instance;
        }

        public bool ServiceFieldPropertiesAllEqual(IBaseModel other) {
            return ServiceFieldPropertiesAllEqual(other, Equals);
        }

        public bool ServiceFieldPropertiesAllEqual(IBaseModel other, Func<object, object, bool> compareFunc) {
            if (compareFunc == null) throw new ArgumentNullException(nameof(compareFunc));

            if (other == null || other.GetType() != GetType())
                return false;

            return BaseModelReflectionHelper.GetServiceFieldProperties(GetType())
                                            .All(p => compareFunc(p.GetValue(this), p.GetValue(other)));
        }

        public bool ServiceFieldPropertiesAllEqualWithSqlDatePrecision(IBaseModel other) {
            return ServiceFieldPropertiesAllEqual(other, (x, y) => {
                if (x is System.Collections.IEnumerable enumerable1 &&
                    y is System.Collections.IEnumerable enumerable2) {
                    var arr1 = enumerable1.Cast<object>().ToArray();
                    var arr2 = enumerable2.Cast<object>().ToArray();
                    if (arr1.Length != arr2.Length) {
                        return false;
                    }

                    for (var i = 0; i < arr1.Length; i++) {
                        if (!Equals(arr1[i], arr2[i])) {
                            return false;
                        }
                    }

                    return true;
                }

                if (x != null && y != null && x is DateTime)
                    return
                        new SqlDateTime((DateTime)x).Equals(new SqlDateTime((DateTime)y));

                return Equals(x, y);
            });
        }
    }
}