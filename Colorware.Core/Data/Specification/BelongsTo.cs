﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Colorware.Core.Helpers;
using Colorware.Core.Mvvm;

namespace Colorware.Core.Data.Specification {
    // TODO: Can probably bring some functionality of BelongsTo and HasManyCollection together
    // into a common base class.
    public class BelongsTo<T> : DefaultNotifyPropertyChanged, IBelongsTo<T> where T : class, IBaseModel {
        public bool IsLoaded { get; private set; }

        private readonly SemaphoreSlim loadingSemaphore = new SemaphoreSlim(1, 1);

        private T item;

        public T Item {
            get {
                // If we would allow access without loading, the user wouldn't know if there is no
                // item belonging to this item or if the item simply isn't loaded yet
                if (!IsLoaded)
                    throw new InvalidOperationException(
                        string.Format(
                            "Data item (type '{0}') must be loaded before being accessed! Call GetAsync() first.",
                            typeof(T).Name));

                return item;
            }
            set {
                item = value;
                updateParentFromItem(item);
                IsLoaded = true;
                OnPropertyChanged("Item");
            }
        }

        /// <summary>
        /// Should only be used to directely set mock data when running tests or when working in design time.
        /// 
        /// This differs from Item in that MockItem doesn't update the foreign key.
        /// </summary>
        public T MockItem {
            set {
                item = value;
                IsLoaded = true;
                OnPropertyChanged("Item");
            }
        }

        /// <summary>
        /// Update the parent's foreignKey with the id of the new item.
        /// </summary>
        /// <param name="relatedItem"></param>
        private void updateParentFromItem(T relatedItem) {
            var id = 0L;
            if (relatedItem != null) {
                if (relatedItem.NewInstance)
                    throw new ArgumentException("Can only set items which are already saved.");

                id = relatedItem.Id;
            }

            // Only update to a new foreign key when it is a valid reference:
            if (id != 0) {
                parent.SetForeignKey(foreignKeyName, id);
            }
        }

        private readonly string foreignKeyName;

        /// <summary>
        /// This is currently required when deleting related items in <see cref="DatabaseModelDataExchanger"/>
        /// as this uses reflection and the private field can be obfuscated.
        /// </summary>
        public string ForeignKeyName {
            get { return foreignKeyName; }
        }

        private readonly IBaseModel parent;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="foreignKeyName">Name of the property which holds the foreign key (e.g. InkSetId)</param>
        /// <param name="parent">The item that belongs to the linked object</param>
        public BelongsTo(String foreignKeyName, IBaseModel parent) {
            this.foreignKeyName = foreignKeyName;
            this.parent = parent;
        }

        // NB: Optimized to wait for other GetAsync() request to finish retrieving the data item
        // so that we don't have to
        /// <summary>
        /// Gets the item belonging to this relation, the item will be loaded if not yet available.
        /// </summary>
        /// <param name="forceRefresh">if set to <c>true</c> the item will be reloaded before being returned, even if it's already available.</param>
        /// <param name="cancellationToken">Can be used to cancel the task.</param>
        /// <exception cref="TaskCanceledException" />
        public async Task<T> GetAsync(bool forceRefresh, CancellationToken cancellationToken) {
            // The lock is an optimization to prevent simultaneous loads of the same item
            // (with the lock others will wait for the first load to complete and use his result)
            await loadingSemaphore.WaitAsync(cancellationToken).ConfigureAwait(false);

            try {
                if (IsLoaded && !forceRefresh)
                    return Item;

                var foreignId = parent.GetForeignKey(foreignKeyName);

                Item = foreignId <= 0
                           ? null
                           : await BaseModel.FindIncludingDeletedAsync<T>(foreignId, cancellationToken).ConfigureAwait(false);

                return Item;
            }
            finally {
                loadingSemaphore.Release();
            }
        }
    }
}