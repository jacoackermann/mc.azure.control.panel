﻿using System;

namespace Colorware.Core.Data.Specification {

    /// <summary>
    /// When a model has been tagged with this attribute, it indicates that it can be read and/or
    /// written by remote client users. By default, remote client users have limited access and this
    /// attribute can be used to extend access in various ways.
    /// </summary>
    public class RemoteClientAttribute : Attribute {
        /*
        /// <summary>
        /// Defines the table where selects are to be performed. The context information for the
        /// query will be set to the id of the current user, so a view can be used that will filter
        /// results for this user.
        /// </summary>
        public string TableForSelect { get; set; }
        */

        /// <summary>
        /// Defines a view or table that would return a list of ids for the given resources which
        /// represent all the items that the current user (using the current context) has access to.
        /// </summary>
        public string AccessTable { get; set; }

        /// <summary>
        /// Name of the field that should be used in the <see cref="AccessTable"/> to get to the id of the
        /// allowed items. Defaults to 'allowed_id'.
        /// </summary>
        public string AccessField { get; set; }

        /// <summary>
        /// Can new items be created?
        /// </summary>
        public bool CanCreate { get; set; }

        /// <summary>
        /// Can existing items be updated that are owned by the user?
        /// </summary>
        public bool CanUpdateOwned { get; set; }

        /// <summary>
        /// Can existing items be deleted if they are owned by the user?
        /// </summary>
        public bool CanDeleteOwned { get; set; }

        /*
        public RemoteClientAttribute(string tableForSelect) {
            TableForSelect = tableForSelect;
        }
        */

        public RemoteClientAttribute(string accessTable) {
            AccessTable = accessTable;
            AccessField = "allowed_id";
        }
    }
}