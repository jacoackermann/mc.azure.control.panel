﻿using System;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Specification {
    [AttributeUsage(AttributeTargets.Property)]
    public class ServiceField : Attribute {
        [NotNull]
        public string ListingName { get; }

        [NotNull]
        public string SavingName { get; }

        [UsedImplicitly]
        public string DisplayName;

        public bool IsPrimaryKey = false;

        /// <summary>
        /// If object can be null we won't throw an error.
        /// </summary>
        public bool CanBeNull = true;

        /// <summary>
        /// Default value to use when object is null and is allowed. If this is null, a type dependent value is used instead.
        /// </summary>
        [CanBeNull]
        public object DefaultValue = null;

        public ServiceField([NotNull] string listingName) {
            if (listingName == null) throw new ArgumentNullException(nameof(listingName));

            ListingName = listingName;
        }
    }
}