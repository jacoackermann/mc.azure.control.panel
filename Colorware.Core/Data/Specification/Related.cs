using System;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Specification {
    public enum OnDeleteAction {
        Nothing,
        ZeroReference,
        DeleteRelated,
        RunAction
    };

    [AttributeUsage(AttributeTargets.Property)]
    public class Related : Attribute {
        /// <summary>
        /// The keyname of the parent. This is the field as it appears in the database e.g. ink_set_id.
        /// </summary>
        public string ParentKeyName;

        public OnDeleteAction OnDelete;

        /// <summary>
        /// Name of the static method on the parent class to call when an item of the parent type is deleted.
        /// 
        /// The method must have the following signature:
        /// Task MethodName(Type type, long id)
        /// 
        /// 'type' = the child's type
        /// id = the child's id
        /// </summary>
        public string Action;

        public Related() {
            OnDelete = OnDeleteAction.Nothing;
        }

        public Related([NotNull] string parentKeyName) {
            if (parentKeyName == null) throw new ArgumentNullException(nameof(parentKeyName));

            OnDelete = OnDeleteAction.Nothing;
            ParentKeyName = parentKeyName;
        }

        public Related([NotNull] string parentKeyName, OnDeleteAction onDelete) {
            if (parentKeyName == null) throw new ArgumentNullException(nameof(parentKeyName));

            ParentKeyName = parentKeyName;
            OnDelete = onDelete;
        }
    }
}