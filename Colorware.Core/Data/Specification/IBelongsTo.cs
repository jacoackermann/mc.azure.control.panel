﻿using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Specification {
    public interface IBelongsTo<T> : INotifyPropertyChanged where T : IBaseModel {
        bool IsLoaded { get; }

        [CanBeNull]
        T Item { get; set; }

        /// <summary>
        /// Should only be used to directely set mock data when running tests or when working in design time.
        ///
        /// This differs from Item in that MockItem doesn't update the foreign key.
        /// </summary>
        [CanBeNull]
        T MockItem { set; }

        /// <summary>
        /// Gets the item belonging to this relation, the item will be loaded if not yet available.
        /// </summary>
        /// <param name="forceRefresh">if set to <c>true</c> the item will be reloaded before being returned, even if it's already available.</param>
        /// <param name="cancellationToken">Can be used to cancel the task.</param>
        /// <exception cref="TaskCanceledException" />
        [ItemCanBeNull]
        Task<T> GetAsync(bool forceRefresh, CancellationToken cancellationToken);
    }

    public static class IBelongsToExtensions {
        [ItemCanBeNull]
        public static Task<T> GetAsync<T>(this IBelongsTo<T> hasManyCollection)
            where T : IBaseModel {
            // ReSharper disable once ExceptionNotDocumented
            return hasManyCollection.GetAsync(false, CancellationToken.None);
        }

        [ItemCanBeNull]
        public static Task<T> GetAsync<T>(this IBelongsTo<T> hasManyCollection, bool forceRefresh)
            where T : IBaseModel {
            // ReSharper disable once ExceptionNotDocumented
            return hasManyCollection.GetAsync(forceRefresh, CancellationToken.None);
        }

        /// <exception cref="TaskCanceledException" />
        [ItemCanBeNull]
        public static Task<T> GetAsync<T>(this IBelongsTo<T> hasManyCollection,
            CancellationToken cancellationToken)
            where T : IBaseModel {
            return hasManyCollection.GetAsync(false, cancellationToken);
        }
    }
}