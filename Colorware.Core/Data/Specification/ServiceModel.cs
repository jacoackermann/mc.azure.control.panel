﻿using System;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Specification {
    [AttributeUsage(AttributeTargets.Class), MeansImplicitUse]
    public class ServiceModel : Attribute {
        /// <summary>
        /// When sorting is used, this enumeration can be used to determine how the results are sorted.
        /// </summary>
        public enum SortOrders {
            Ascending,
            Descending
        }

        /// <summary>
        /// The name of the tag in the XML listing. E.g. for ColorStrip it would be "color-strip".
        /// </summary>
        // TODO: any point to this? isn't ServiceName enough in theory?
        public String ListingName;

        /// <summary>
        /// Nowadays: the name of the DB table (used to be the HTTP service name).
        /// </summary>
        public String ServiceName;

        /// <summary>
        /// The name of the table. This can be used if the service name differs from the table name.
        /// If this is null, ServiceName will be used. Obsolete nowadays.
        /// </summary>
        [Obsolete("Use 'ServiceName' instead")]
        // TODO: get rid of this and rename ServiceName to TableName
        public String TableName;

        /// <summary>
        /// The name to display in Management module.
        /// </summary>
        public String DisplayName;

        /// <summary>
        /// Should deletes of this model be logged? Default is true.
        /// </summary>
        public bool LogDelete = true;

        /// <summary>
        /// If this is non null, it indicates what the default ordering field should be for the results.
        /// </summary>
        public string DefaultSortOrder;

        /// <summary>
        /// When sorting is used (DefaultSortOrder != null), this determines in what direction
        /// the sort should take place.
        /// </summary>
        public SortOrders SortOrderDirection = SortOrders.Descending;
    }
}