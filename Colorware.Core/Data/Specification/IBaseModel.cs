﻿using System;

using JetBrains.Annotations;

using ReactiveUI;

namespace Colorware.Core.Data.Specification {
    public interface IBaseModel : IReactiveObject {
        long Id { get; set; }

        bool NewInstance { get; set; }

        /// <summary>
        /// Determines if the model has been 'deleted', in which case the value would be <c>true</c>.
        /// We don't actually want to expose a setter here, but this is required for serialisation scenarios.
        /// </summary>
        bool IsDeleted { get; set; }

        /// <summary>
        /// Perform a shallow clone copying all properties marked with a ServiceField attribute.
        /// Note that this also copies the current saved state and Id, so saving the instance will
        /// overwrite any existing data for that instance.
        /// </summary>
        /// <typeparam name="T">The type of the object being cloned.</typeparam>
        /// <returns>A new instance with it's service field properties copied from the original object instance.</returns>
        [NotNull, Pure]
        T Clone<T>() where T : IBaseModel;

        /// <summary>
        /// Like Clone, but sets the clone up to be a new object with NewInstance = true and
        /// with its ID reset.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        [NotNull, Pure]
        T CloneNew<T>() where T : IBaseModel;

        /// <summary>
        /// Returns true iff all properties marked with the ServiceField attribute equal their counterpart
        /// on the passed in 'other' IBaseModel.
        /// </summary>
        bool ServiceFieldPropertiesAllEqual([CanBeNull] IBaseModel other);

        /// <summary>
        /// Returns true iff all properties marked with the ServiceField attribute equal their counterpart
        /// on the passed in 'other' IBaseModel.
        /// 
        /// Property equality is determined by passed in 'compareFunc'.
        /// </summary>
        bool ServiceFieldPropertiesAllEqual([CanBeNull] IBaseModel other,
                                            [NotNull] Func<object, object, bool> compareFunc);

        /// <summary>
        /// Returns true iff all properties marked with the ServiceField attribute equal their counterpart
        /// on the passed in 'other' IBaseModel.
        /// 
        /// DateTime properties are compared at SQL 'datetime' precision, meaning they'll first be converted
        /// to SqlDateTime before being compared. This prevents invalid mismatches due to SQL's 'datetime'
        /// having less precision than C#'s DateTime.
        /// </summary>
        bool ServiceFieldPropertiesAllEqualWithSqlDatePrecision([CanBeNull] IBaseModel other);
    }
}