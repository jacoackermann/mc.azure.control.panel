﻿using JetBrains.Annotations;

namespace Colorware.Core.Data {
    [UsedImplicitly]
    public class LocalServiceModelIdGenerator : ILocalServiceModelIdGenerator {
        private long lastItemId = 0;

        public long GenerateId() {
            return --lastItemId;
        }
    }
}