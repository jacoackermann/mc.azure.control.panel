﻿using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Data.Service.PressViewModelService;
using Colorware.Core.Data.Specification;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service {
    /// <summary>
    /// Responsible for taking general model data stored in a SimpleDictionaryModelData instance and
    /// filling a passed IBaseModel deriving object with it.
    /// </summary>
    public interface IServiceModelFiller {
        [NotNull]
        IBaseModel Fill([NotNull] Type type, [NotNull] IBaseModel model, [NotNull] SimpleDictionaryModelData data);

        [NotNull]
        IReadOnlyList<IBaseModel> CreateAndFill([NotNull] Type type, [NotNull] IEnumerable<SimpleDictionaryModelData> modelData);
    }

    [UsedImplicitly]
    public static class IModelFillerExtensions {
        [NotNull]
        public static IReadOnlyList<T> CreateAndFill<T>([NotNull] this IServiceModelFiller serviceModelFiller,
            [NotNull] IEnumerable<SimpleDictionaryModelData> modelData) where T : IBaseModel, new() {
            if (serviceModelFiller == null) throw new ArgumentNullException("serviceModelFiller");
            if (modelData == null) throw new ArgumentNullException("modelData");

            return serviceModelFiller.CreateAndFill(typeof(T), modelData).Cast<T>().ToList();
        }

        [NotNull]
        public static T Fill<T>([NotNull] this IServiceModelFiller serviceModelFiller, [NotNull] T model, [NotNull] SimpleDictionaryModelData data)
            where T : IBaseModel {
            if (serviceModelFiller == null) throw new ArgumentNullException("serviceModelFiller");
            if (model == null) throw new ArgumentNullException("model");
            if (data == null) throw new ArgumentNullException("data");

            return (T)serviceModelFiller.Fill(typeof(T), model, data);
        }
    }
}