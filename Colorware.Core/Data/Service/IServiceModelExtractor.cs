﻿using System.Collections.Generic;
using System.Data;

using Colorware.Core.Data.Service.PressViewModelService;
using Colorware.Core.Data.Specification;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service {
    /// <summary>
    /// Responsible for extracting serializable data from a given model.
    /// This data can than be used to save the model using an IModelDataExchanger implementation.
    /// </summary>
    public interface IServiceModelExtractor {
        /// <summary>
        /// NB: Updates CreatedAt and UpdatedAt properties on the model if present! (and returns these updated
        /// timestamps in the result as well)
        /// </summary>
        SimpleDictionaryModelData Extract<T>([NotNull] T model) where T : IBaseModel;

        /// <summary>
        /// NB: Updates CreatedAt and UpdatedAt properties on the model if present! (and returns these updated
        /// timestamps in the result as well)
        /// </summary>
        DataTable ExtractIntoDataTable<T>([NotNull] DataTable dataTable, [NotNull] IEnumerable<T> models)
            where T : IBaseModel;
    }
}