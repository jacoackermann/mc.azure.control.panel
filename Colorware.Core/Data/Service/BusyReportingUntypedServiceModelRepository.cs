﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Colorware.Core.Data.Specification;
using Colorware.Core.Functional.Option;
using Colorware.Core.StatusReporting;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service {
    [UsedImplicitly]
    public class BusyReportingUntypedServiceModelRepository : IUntypedServiceModelRepository {
        private readonly IUntypedServiceModelRepository untypedServiceModelRepository;
        private readonly IGlobalStatusReporter globalStatusReporter;

        public BusyReportingUntypedServiceModelRepository(
            [NotNull] IUntypedServiceModelRepository untypedServiceModelRepository,
            [NotNull] IGlobalStatusReporter globalStatusReporter) {
            if (untypedServiceModelRepository == null) throw new ArgumentNullException("untypedServiceModelRepository");
            if (globalStatusReporter == null) throw new ArgumentNullException("globalStatusReporter");

            this.untypedServiceModelRepository = untypedServiceModelRepository;
            this.globalStatusReporter = globalStatusReporter;
        }

        public async Task<IReadOnlyList<IBaseModel>> FindAllAsync(Type type, Query query,
            CancellationToken cancellationToken) {
            using (globalStatusReporter.AddSilentBusyReportWithScope())
                return await untypedServiceModelRepository.FindAllAsync(type, query, cancellationToken);
        }

        public async Task SaveAllAsync(IReadOnlyList<IBaseModel> items) {
            using (globalStatusReporter.AddSilentBusyReportWithScope())
                await untypedServiceModelRepository.SaveAllAsync(items);
        }

        public async Task SaveAllBulkAsync(IReadOnlyList<IBaseModel> items, Option<long> remoteCompanyId) {
            using (globalStatusReporter.AddSilentBusyReportWithScope())
                await untypedServiceModelRepository.SaveAllBulkAsync(items, remoteCompanyId);
        }

        public async Task DeleteAllAsync(IReadOnlyList<IBaseModel> items) {
            using (globalStatusReporter.AddSilentBusyReportWithScope())
                await untypedServiceModelRepository.DeleteAllAsync(items);
        }

        public async Task DeleteNonCascadingAsync(Type type, Query query) {
            using (globalStatusReporter.AddSilentBusyReportWithScope())
                await untypedServiceModelRepository.DeleteNonCascadingAsync(type, query);
        }
    }
}