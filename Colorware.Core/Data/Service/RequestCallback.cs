﻿namespace Colorware.Core.Data.Service {
    public static class RequestCallback {
        public delegate void HandleRequestResult<in T>(T result);
    }
}