﻿using System;

using Colorware.Core.Enums;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IDatabaseCleanupRepository {
        DatabaseCleanupJobMeasurementCount GetJobAndMeasurementCount(DataRemoveType dataRemoveType, DateTime dateTime);
        void CleanDatabase(DataRemoveType dataRemoveType, DateTime dateTime);
    }

    public class DatabaseCleanupJobMeasurementCount {
        public long TotalJobs { get; set; }
        public long TotalMeasurements { get; set; }
    }
}