using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IUserProfileEntryRepository {
        /// <summary>
        /// Get all the entries for the given user group id.
        /// </summary>
        /// <param name="userGroupId">The id of the group to get the entries for.</param>
        Task<IEnumerable<IUserProfileEntry>> GetForUserGroup(long userGroupId);

        /// <summary>
        /// Save the entry.
        /// </summary>
        /// <param name="entry">The entry to save.</param>
        Task SaveEntry(IUserProfileEntry entry);

        /// <summary>
        /// Save all entries in the given list.
        /// </summary>
        /// <param name="entries">The entries to save.</param>
        Task SaveAllEntries(IEnumerable<IUserProfileEntry> entries);

        /// <summary>
        /// Delete all entries that exist for the given user group id.
        /// </summary>
        /// <param name="userGroupId">The id of the user group to delete the entries for.</param>
        Task DeleteAllEntriesForUserGroupId(long userGroupId);

        /// <summary>
        /// Copy entries from group with <see cref="fromUserGroupId"/> to the group with <see cref="toUserGroupId"/>.
        /// </summary>
        /// <param name="fromUserGroupId">The id of the originating user group.</param>
        /// <param name="toUserGroupId">The id of the target user group.</param>
        /// <remarks>All existing entries of the target group will be deleted!</remarks>
        Task CopyEntries(long fromUserGroupId, long toUserGroupId);
    }
}
