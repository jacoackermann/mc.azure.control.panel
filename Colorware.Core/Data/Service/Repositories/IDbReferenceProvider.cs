using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.Repositories {
    // TODO: add cancellation support
    public interface IDbReferenceProvider : IReferenceProvider {
        [ItemNotNull]
        Task<IReadOnlyList<IReferencePatch>> GetReferencePatchesForReferenceId(long referenceId);

        [NotNull, ItemCanBeNull]
        Task<IReferencePatch> GetReferencePatchById(long referencePatchId);

        [NotNull, ItemNotNull]
        Task<IReadOnlyList<IReferencePatch>> GetTintPatchesForSolidId(long id);

        Task<IReadOnlyList<IReference>> GetReferencesForPrintingConditionId(long printingConditionId);
    }
}