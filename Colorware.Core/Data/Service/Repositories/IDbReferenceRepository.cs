﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Color;
using Colorware.Core.Data.Models;
using Colorware.Core.Extensions;
using Colorware.Core.Functional.Option;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IDbReferenceRepository : IDbReferenceProvider, IReferenceSaver, IReferenceDeleter,
        IServiceModelRepository<IReference> {
        [NotNull]
        IReference CreateReference(bool isSpot);

        [NotNull]
        IReferencePatch CreateReferencePatch([NotNull] SpectralMeasurementConditions mc);
    }

    public static class IReferenceRepositoryExtensions {
        /// <summary>
        /// Creates a reference with the specified name and saves it and the specified reference patches.
        /// (the patches are linked to the reference)
        /// </summary>
        [ItemNotNull]
        public static async Task<IReference> CreateAndSaveReferenceWithPatches(this IDbReferenceRepository repository,
            [NotNull] IReadOnlyList<IReferencePatch> referencePatches,
            [NotNull] string referenceName,
            Option<long> companyId,
            Option<long> paperTypeId) {
            if (referencePatches == null) throw new ArgumentNullException(nameof(referencePatches));
            if (referenceName == null) throw new ArgumentNullException(nameof(referenceName));

            // TODO: we're getting rid of the book type in 15.3
            var reference = repository.CreateReference(false);
            reference.Name = referenceName;
            reference.CompanyId = companyId.OrElseDefault();
            reference.PaperTypeId = paperTypeId.OrElseDefault();

            await repository.SaveReference(reference).ConfigureAwait(false);

            referencePatches.ForEach(p => p.ReferenceId = reference.Id);

            await repository.SaveReferencePatches(referencePatches).ConfigureAwait(false);

            return reference;
        }

        /// <summary>
        /// Saves a reference with the specified reference patches, the patches are linked to the reference before being saved.
        /// </summary>
        [ItemNotNull]
        public static async Task<IReference> SaveReferenceWithPatches(this IDbReferenceRepository repository,
            IReference reference,
            [NotNull, ItemNotNull] IReadOnlyList<IReferencePatch>
                referencePatches) {
            if (referencePatches == null) throw new ArgumentNullException(nameof(referencePatches));

            // TODO: we're getting rid of the book type in 15.3

            await repository.SaveReference(reference).ConfigureAwait(false);

            referencePatches.ForEach(p => p.ReferenceId = reference.Id);

            await repository.SaveReferencePatches(referencePatches).ConfigureAwait(false);

            return reference;
        }
    }
}