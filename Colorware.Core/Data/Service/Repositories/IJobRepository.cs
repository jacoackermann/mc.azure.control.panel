﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;
using Colorware.Core.Enums;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IJobRepository : IServiceModelRepository<IJob> {
        [NotNull, ItemNotNull]
        Task<IReadOnlyList<IJob>> GetLastNJobs(JobTypes jobType, int maxResults, CancellationToken token);
        Task<IReadOnlyList<IJob>> FindJobsBefore(DateTime dateTime);

        [ItemCanBeNull]
        Task<IJob> GetJobForJobStripId(long jobStripId, CancellationToken cancellationToken);
    }
}