﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IOpacityResultRepository : IServiceModelRepository<IOpacityResult> {
        [NotNull, ItemNotNull]
        Task<IReadOnlyCollection<IOpacityResult>> FindAllForJob(long jobId);
    }
}