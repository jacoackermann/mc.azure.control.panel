﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.Identity;
using Colorware.Core.Enums;
using Colorware.Core.Functional.Option;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.Repositories {
    public enum ReferenceRequestType {
        SpotOnly,
        ReferenceOnly,
        All
    }

    public interface IReferenceProvider : IModelOrigin {
        [NotNull, ItemNotNull]
        Task<IReadOnlyList<IReference>> GetReferences(ReferenceRequestType requestType,
                                                      CancellationToken cancellationToken);

        [NotNull, ItemNotNull]
        Task<IReadOnlyList<IReferencePatch>> GetReferencePatchesForReference([NotNull] IReference reference,
                                                                             PatchTypes patchType,
                                                                             CancellationToken cancellationToken);

        [NotNull]
        Task<Option<IReferencePatch>> GetReferencePatch(IReferencePatchId referencePatchId,
                                                        CancellationToken cancellationToken);

        [NotNull, ItemNotNull]
        Task<IReadOnlyList<IReferencePatch>> GetTintPatchesForSolidPatch([NotNull] IReferencePatch solidPatch,
                                                                         CancellationToken cancellationToken);

        /// <summary>
        /// Creates an estimate of a dotgain patch given a solid patch and tint %.
        /// </summary>
        /// <param name="solidId">Solid patch id.</param>
        /// <param name="tint">Tint % of the dotgain patch to create.</param>
        /// <param name="defaultDotgainEntries">List to obtain the patch's DefaultDotgain value from.</param>
        /// <param name="measurementCondition">Measurement condition to use.</param>
        /// <returns></returns>
        [NotNull]
        Task<Option<IReferencePatch>> EstimateDotgainPatch(
            [NotNull] IReferencePatchId solidId, int tint,
            IReadOnlyList<IDefaultDotgainEntry> defaultDotgainEntries, IMeasurementConditionsConfig measurementCondition);

        [NotNull, ItemNotNull]
        Task<IReadOnlyList<IReference>> GetReferencesForPrintingCondition([NotNull] IPaperType printingCondition,
                                                                          CancellationToken cancellationToken);

        [NotNull, ItemNotNull]
        Task<IReadOnlyList<IReference>> GetReferencesWithoutPrintingCondition(CancellationToken cancellationToken);

        [NotNull, ItemNotNull]
        Task<IReadOnlyList<IReference>> GetReferencesByName([NotNull] string referenceName);

        /// <summary>
        /// Gets the reference that can provide gray balances, overprints, paperwhites etc. that can be used
        /// with the supplied reference patch.
        /// </summary>
        [NotNull, ItemCanBeNull]
        Task<Option<IReference>> GetOtherPatchProvidingReference(IReferencePatchId referencePatchId,
                                                                 CancellationToken cancellationToken);
    }

    public static class ReferenceProviderExtensions {
        [NotNull, ItemNotNull]
        public static Task<IReadOnlyList<IReference>> GetReferences(this IReferenceProvider referenceProvider,
                                                                    ReferenceRequestType requestType) {
            return referenceProvider.GetReferences(requestType, CancellationToken.None);
        }

        [NotNull, ItemNotNull]
        public static Task<IReadOnlyList<IReferencePatch>> GetReferencePatchesForReference(
            this IReferenceProvider referenceProvider, [NotNull] IReference reference, PatchTypes patchType) {
            return referenceProvider.GetReferencePatchesForReference(reference, patchType, CancellationToken.None);
        }

        [NotNull, ItemNotNull]
        public static Task<IReadOnlyList<IReferencePatch>> GetTintPatchesForSolidPatch(
            this IReferenceProvider referenceProvider, [NotNull] IReferencePatch solidPatch) {
            return referenceProvider.GetTintPatchesForSolidPatch(solidPatch, CancellationToken.None);
        }

        [NotNull]
        public static Task<Option<IReferencePatch>> GetReferencePatch(this IReferenceProvider referenceProvider,
                                                                      IReferencePatchId referencePatchId) {
            return referenceProvider.GetReferencePatch(referencePatchId, CancellationToken.None);
        }

        [NotNull, ItemNotNull]
        public static Task<IReadOnlyList<IReference>> GetReferencesForPrintingCondition(
            this IReferenceProvider referenceProvider,
            [NotNull] IPaperType printingCondition) {
            return referenceProvider.GetReferencesForPrintingCondition(printingCondition, CancellationToken.None);
        }

        [NotNull, ItemNotNull]
        public static Task<IReadOnlyList<IReference>> GetReferencesWithoutPrintingCondition(
            this IReferenceProvider referenceProvider) {
            return referenceProvider.GetReferencesWithoutPrintingCondition(CancellationToken.None);
        }

        [NotNull, ItemCanBeNull]
        public static Task<Option<IReference>> GetOtherPatchProvidingReference(
            this IReferenceProvider referenceProvider, IReferencePatchId referencePatchId) {
            return referenceProvider.GetOtherPatchProvidingReference(referencePatchId, CancellationToken.None);
        }

        [NotNull, ItemNotNull]
        public static async Task<IReadOnlyList<IReference>> GetAllReferences(
            this IEnumerable<IReferenceProvider> referenceProviders, ReferenceRequestType requestType,
            CancellationToken cancellationToken) {
            var refsTasks = referenceProviders.Select(r => r.GetReferences(requestType, cancellationToken));

            var refs = new List<IReference>();
            foreach (var refsTask in refsTasks)
                refs.AddRange(await refsTask.ConfigureAwait(false));

            return refs;
        }

        [NotNull, ItemNotNull]
        public static Task<IReadOnlyList<IReference>> GetAllReferences(
            this IEnumerable<IReferenceProvider> referenceProviders, ReferenceRequestType requestType) {
            return referenceProviders.GetAllReferences(requestType, CancellationToken.None);
        }
    }
}