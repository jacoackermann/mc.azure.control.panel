using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;
using Colorware.Core.Enums;
using Colorware.Core.Extensions;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.Repositories {
    public static class IColorMappingRepositoryExtensions {
        /// <summary>
        /// Gets all color mappings for the specified ColorMappingFamily.
        /// </summary>
        [Pure]
        public static Task<IReadOnlyList<IColorMapping>> GetColorMappings(this IColorMappingRepository repository,
                                                                          ColorMappingFamily family) {
            return repository.GetColorMappings(family, CancellationToken.None);
        }

        public static string GetMappedName([NotNull] this IEnumerable<IColorMapping> colorMappings,
                                           [NotNull] string nameToMap) {
            if (colorMappings == null) throw new ArgumentNullException(nameof(colorMappings));
            if (nameToMap == null) throw new ArgumentNullException(nameof(nameToMap));

            foreach (var colorMapping in colorMappings.OrderBy(s => s.Priority)) {
                if (colorMapping == null)
                    continue;

                var foreignName = colorMapping.ForeignName;
                var localName = colorMapping.LocalName;

                // Possibly a wildcard search and replace
                // TODO: this escaping isn't 100% correct, but better than nothing, just means
                // we can't use wildcard matching if an escaped wildcard occurs
                if (foreignName.Contains("*") && !foreignName.Contains("**")) {
                    // We want to support replacing in the middle, e.g: * A * --> * B * or * A * --> C
                    if (foreignName.StartsWith("*") && foreignName.EndsWith("*") &&
                        Regex.Matches(foreignName, "\\*").Count == 2 &&
                        (((Regex.Matches(localName, "\\*").Count == 2) && !localName.Contains("**")) ||
                         !localName.Contains('*'))) {
                        var stringToMatch = foreignName.Substring(1, foreignName.Length - 2);
                        if (nameToMap.IndexOf(stringToMatch, StringComparison.InvariantCultureIgnoreCase) >= 0) {
                            if (!localName.Contains("*"))
                                return localName;

                            return nameToMap.Replace(stringToMatch, localName.Substring(1, localName.Length - 2),
                                                     StringComparison.InvariantCultureIgnoreCase);
                        }
                    }

                    var foreignParts = foreignName.Split(new[] {'*'}, 2);

                    // Does the name match the specified wildcard search string?
                    if (nameToMap.StartsWith(foreignParts[0], StringComparison.InvariantCultureIgnoreCase) &&
                        nameToMap.EndsWith(foreignParts[1], StringComparison.InvariantCultureIgnoreCase)) {
                        string matchedPart;
                        // Wildcard at start
                        if (foreignParts[0] == "")
                            matchedPart = nameToMap.Substring(0,
                                                              nameToMap.IndexOf(foreignParts[1],
                                                                                StringComparison
                                                                                    .InvariantCultureIgnoreCase));
                        // Wildcard at end
                        else if (foreignParts[1] == "")
                            matchedPart = nameToMap.Substring(foreignParts[0].Length);
                        else
                        // Wildcard in middle
                            matchedPart = nameToMap.Substring(foreignParts[0].Length,
                                                              nameToMap.IndexOf(foreignParts[1],
                                                                                StringComparison
                                                                                    .InvariantCultureIgnoreCase) -
                                                              foreignParts[0].Length);

                        if (colorMapping.LocalName.Contains("*")) {
                            var localParts = localName.Split(new[] {'*'}, 2);

                            return localParts[0] + matchedPart + localParts[1];
                        }

                        return localName;
                    }
                }

                // TODO: adapt when escaping is improved
                if (nameToMap.Equals(colorMapping.ForeignName.Replace("**", "*"),
                                     StringComparison.InvariantCultureIgnoreCase))
                    return localName;
            }

            return nameToMap;
        }
    }
}