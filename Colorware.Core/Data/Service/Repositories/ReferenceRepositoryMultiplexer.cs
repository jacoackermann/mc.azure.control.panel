using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.Identity;
using Colorware.Core.Enums;
using Colorware.Core.Extensions;
using Colorware.Core.Functional.Option;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.Repositories {
    [UsedImplicitly]
    public class ReferenceRepositoryMultiplexer : IReferenceRepositoryMultiplexer {
        private readonly IEnumerable<IReferenceProvider> referenceProviders;
        private readonly IEnumerable<IReferenceDeleter> referenceDeleters;
        private readonly IEnumerable<IReferenceSaver> referenceSavers;

        public ReferenceRepositoryMultiplexer([NotNull] ICollection<IDbReferenceRepository> referenceRepositories,
                                              [NotNull] ICollection<IReferenceProvider> referenceProviders,
                                              [NotNull] ICollection<IReferenceDeleter> referenceDeleters,
                                              [NotNull] ICollection<IReferenceSaver> referenceSavers) {
            if (referenceRepositories == null) throw new ArgumentNullException(nameof(referenceRepositories));
            if (referenceProviders == null) throw new ArgumentNullException(nameof(referenceProviders));
            if (referenceDeleters == null) throw new ArgumentNullException(nameof(referenceDeleters));
            if (referenceSavers == null) throw new ArgumentNullException(nameof(referenceSavers));

            assertOneImplementationPerOriginGuid(referenceProviders.Concat(referenceRepositories).ToList(),
                                                 referenceDeleters.Concat(referenceRepositories).ToList(),
                                                 referenceSavers.Concat(referenceRepositories).ToList());

            // Combine Providers, Deleters and Savers with complete repositories
            this.referenceProviders =
                referenceProviders.Concat(referenceRepositories).DistinctBy(p => p.OriginTypeGuid).ToList();
            this.referenceDeleters =
                referenceDeleters.Concat(referenceRepositories).DistinctBy(d => d.OriginTypeGuid).ToList();
            this.referenceSavers =
                referenceSavers.Concat(referenceRepositories).DistinctBy(s => s.OriginTypeGuid).ToList();
        }

        private void assertOneImplementationPerOriginGuid(ICollection<IReferenceProvider> refProviders,
                                                          ICollection<IReferenceDeleter> refDeleters,
                                                          ICollection<IReferenceSaver> refSavers) {
            if (refProviders.DistinctBy(p => p.OriginTypeGuid).Count() !=
                refProviders.DistinctBy(p => new {OriginGuid = p.OriginTypeGuid, Type = p.GetType()}).Count())
                throw new Exception("There are multiple Reference Providers with the same Origin GUID!");

            if (refSavers.DistinctBy(p => p.OriginTypeGuid).Count() !=
                refSavers.DistinctBy(p => new {OriginGuid = p.OriginTypeGuid, Type = p.GetType()}).Count())
                throw new Exception("There are multiple Reference Savers with the same Origin GUID!");

            if (refDeleters.DistinctBy(p => p.OriginTypeGuid).Count() !=
                refDeleters.DistinctBy(p => new {OriginGuid = p.OriginTypeGuid, Type = p.GetType()}).Count())
                throw new Exception("There are multiple Reference Deleters with the same Origin GUID!");
        }

        public Guid OriginTypeGuid {
            get { throw new NotSupportedException(); }
        }

        public Task<IReadOnlyList<IReference>> GetReferences(ReferenceRequestType requestType,
                                                             CancellationToken cancellationToken) {
            return referenceProviders.GetAllReferences(requestType, cancellationToken);
        }

        public Task<IReadOnlyList<IReferencePatch>> GetReferencePatchesForReference(IReference reference,
                                                                                    PatchTypes patchType,
                                                                                    CancellationToken cancellationToken) {
            var refProvider = referenceProviders.FirstOrDefault(s => s.OriginTypeGuid == reference.OriginTypeGuid);

            if (refProvider == null)
                throw new ArgumentException(
                    $"There is no reference patch provider that can provide reference patches for the supplied reference (OriginGuid is '{reference.OriginTypeGuid}')",
                    nameof(reference));

            return refProvider.GetReferencePatchesForReference(reference, patchType, cancellationToken);
        }

        public Task<Option<IReferencePatch>> GetReferencePatch(IReferencePatchId referencePatchId,
                                                               CancellationToken cancellationToken) {
            var refProvider = referenceProviders.FirstOrDefault(s => s.OriginTypeGuid == referencePatchId.OriginTypeGuid);

            if (refProvider == null)
                throw new ArgumentException(
                    $"There is no reference patch provider that we can search for the supplied reference patch ID (OriginTypeGuid is '{referencePatchId.OriginTypeGuid}')",
                    nameof(referencePatchId));

            return refProvider.GetReferencePatch(referencePatchId, cancellationToken);
        }

        public Task<IReadOnlyList<IReferencePatch>> GetTintPatchesForSolidPatch(IReferencePatch solidPatch,
                                                                                CancellationToken cancellationToken) {
            var refProvider = referenceProviders.FirstOrDefault(s => s.OriginTypeGuid == solidPatch.OriginTypeGuid);

            if (refProvider == null)
                throw new ArgumentException(
                    $"There is no reference patch provider that can provide tint patches for the supplied reference (OriginGuid is '{solidPatch.OriginTypeGuid}')",
                    nameof(solidPatch));

            return refProvider.GetTintPatchesForSolidPatch(solidPatch, cancellationToken);
        }

        public Task<Option<IReferencePatch>> EstimateDotgainPatch(IReferencePatchId solidId, int tint,
                                                                  IReadOnlyList<IDefaultDotgainEntry>
                                                                      defaultDotgainEntries,
                                                                  IMeasurementConditionsConfig measurementCondition) {
            var refProvider = referenceProviders.FirstOrDefault(s => s.OriginTypeGuid == solidId.OriginTypeGuid);

            if (refProvider == null)
                throw new ArgumentException(
                    $"There is no reference patch provider that can try to create a dotgain patch for the supplied solid id (OriginGuid is '{solidId.OriginTypeGuid}')",
                    nameof(solidId));

            return refProvider.EstimateDotgainPatch(solidId, tint, defaultDotgainEntries, measurementCondition);
        }

        public async Task<IReadOnlyList<IReference>> GetReferencesForPrintingCondition(IPaperType printingCondition,
                                                                                       CancellationToken
                                                                                           cancellationToken) {
            // A printing condition has no origin yet, so just ask all repos and combine results
            var refsTasks =
                referenceProviders.Select(r => r.GetReferencesForPrintingCondition(printingCondition, cancellationToken));

            var refs = new List<IReference>();
            foreach (var refsTask in refsTasks)
                refs.AddRange(await refsTask.ConfigureAwait(false));

            return refs;
        }

        public async Task<IReadOnlyList<IReference>> GetReferencesWithoutPrintingCondition(
            CancellationToken cancellationToken) {
            var refsTasks = referenceProviders.Select(r => r.GetReferencesWithoutPrintingCondition(cancellationToken));

            var refs = new List<IReference>();
            foreach (var refsTask in refsTasks)
                refs.AddRange(await refsTask.ConfigureAwait(false));

            return refs;
        }

        async Task<IReadOnlyList<IReference>> IReferenceProvider.GetReferencesByName(string referenceName) {
            var tasks = referenceProviders.Select(rp => rp.GetReferencesByName(referenceName));
            var items = await Task.WhenAll(tasks);
            return items.SelectMany(r => r)
                        .ToList();
        }

        public Task<Option<IReference>> GetOtherPatchProvidingReference(IReferencePatchId referencePatchId,
                                                                        CancellationToken cancellationToken) {
            var refProvider = referenceProviders.FirstOrDefault(s => s.OriginTypeGuid == referencePatchId.OriginTypeGuid);

            if (refProvider == null)
                throw new ArgumentException(
                    $"There is no reference patch provider that we can search for the supplied reference patch ID (OriginTypeGuid is '{referencePatchId.OriginTypeGuid}')",
                    nameof(referencePatchId));

            return refProvider.GetOtherPatchProvidingReference(referencePatchId, cancellationToken);
        }

        public async Task SaveReferences(IReadOnlyList<IReference> references) {
            var refGroups = references.GroupBy(r => r.OriginTypeGuid);

            foreach (var refGroup in refGroups) {
                var refSaver = referenceSavers.FirstOrDefault(s => s.OriginTypeGuid == refGroup.Key);

                if (refSaver == null)
                    throw new ArgumentException(
                        $"Reference collection contains references that cannot be saved (OriginGuid is '{refGroup.Key}')",
                        nameof(references));

                await refSaver.SaveReferences(refGroup.ToList()).ConfigureAwait(false);
            }
        }

        public async Task SaveReferencePatches(IReadOnlyList<IReferencePatch> patches) {
            var patchGroups = patches.GroupBy(r => r.OriginTypeGuid);

            foreach (var patchGroup in patchGroups) {
                var refSaver = referenceSavers.FirstOrDefault(s => s.OriginTypeGuid == patchGroup.Key);

                if (refSaver == null)
                    throw new ArgumentException(
                        $"Reference patch collection contains patches that cannot be saved (OriginGuid is '{patchGroup.Key}')",
                        nameof(patches));

                await refSaver.SaveReferencePatches(patchGroup.ToList()).ConfigureAwait(false);
            }
        }

        public async Task DeleteReferences(IReadOnlyList<IReference> references) {
            var refGroups = references.GroupBy(r => r.OriginTypeGuid);

            foreach (var refGroup in refGroups) {
                var refDeleter = referenceDeleters.FirstOrDefault(s => s.OriginTypeGuid == refGroup.Key);

                if (refDeleter == null)
                    throw new ArgumentException(
                        $"Reference collection contains references that cannot be deleted (OriginGuid is '{refGroup.Key}')",
                        nameof(references));

                await refDeleter.DeleteReferences(refGroup.ToList()).ConfigureAwait(false);
            }
        }

        public async Task DeleteReferencePatches(IReadOnlyList<IReferencePatch> patches) {
            var patchGroups = patches.GroupBy(r => r.OriginTypeGuid);

            foreach (var patchGroup in patchGroups) {
                var refDeleter = referenceDeleters.FirstOrDefault(s => s.OriginTypeGuid == patchGroup.Key);

                if (refDeleter == null)
                    throw new ArgumentException(
                        $"Reference patch collection contains patches that cannot be deleted (OriginGuid is '{patchGroup.Key}')",
                        nameof(patches));

                await refDeleter.DeleteReferencePatches(patchGroup.ToList()).ConfigureAwait(false);
            }
        }

        public bool CanDeleteReferences(IEnumerable<IReference> references) {
            return
                references.DistinctBy(r => r.OriginTypeGuid)
                          .All(r => referenceDeleters.Any(d => d.OriginTypeGuid == r.OriginTypeGuid)) &&
                references.All(r => !r.Locked);
        }

        public bool CanSaveReferences(IEnumerable<IReference> references) {
            return
                references.DistinctBy(r => r.OriginTypeGuid)
                          .All(r => referenceSavers.Any(s => s.OriginTypeGuid == r.OriginTypeGuid));
        }

        public bool CanDeleteReferencePatches(IEnumerable<IReferencePatch> patches) {
            return patches.DistinctBy(p => p.OriginTypeGuid)
                          .All(p => referenceDeleters.Any(d => d.OriginTypeGuid == p.OriginTypeGuid));
        }

        public bool CanSaveReferencePatches(IEnumerable<IReferencePatch> patches) {
            return patches.DistinctBy(p => p.OriginTypeGuid)
                          .All(p => referenceSavers.Any(s => s.OriginTypeGuid == p.OriginTypeGuid));
        }
    }
}