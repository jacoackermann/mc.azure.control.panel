﻿using System.Threading.Tasks;

using Colorware.Core.Data.Models;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IClientRepository : IServiceModelRepository<IClient> {
        Task CreateNewClientAsync(IClient item);
    }
}