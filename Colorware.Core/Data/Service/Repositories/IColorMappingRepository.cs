﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;
using Colorware.Core.Enums;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IColorMappingRepository : IServiceModelRepository<IColorMapping> {
        /// <summary>
        /// Gets all color mappings for the specified ColorMappingFamily.
        /// </summary>
        /// <exception cref="TaskCanceledException" />
        [Pure]
        Task<IReadOnlyList<IColorMapping>> GetColorMappings(ColorMappingFamily family,
                                                            CancellationToken cancellationToken);

        /// <summary>
        /// Deletes all color mappings of the specified family.
        /// </summary>
        Task DeleteColorMappings(ColorMappingFamily family);
    }
}