﻿using Colorware.Core.Data.Models;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IToleranceSetRepository: IServiceModelRepository<IToleranceSet> {}
}