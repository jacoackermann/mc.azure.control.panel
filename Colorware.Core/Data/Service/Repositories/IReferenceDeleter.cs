﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IReferenceDeleter : IModelOrigin {
        Task DeleteReferences([NotNull, ItemNotNull] IReadOnlyList<IReference> references);
        Task DeleteReferencePatches([NotNull, ItemNotNull] IReadOnlyList<IReferencePatch> patches);
    }
}