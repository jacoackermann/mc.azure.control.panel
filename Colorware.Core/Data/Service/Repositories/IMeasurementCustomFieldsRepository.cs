﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IMeasurementCustomFieldsRepository {
        Task<IEnumerable<IMeasurementCustomField>> GetFieldsForMeasurementAsync(IMeasurement measurement);
        Task SaveFieldsForMeasurementAsync(IMeasurement measurement, IEnumerable<IMeasurementCustomField> fields);
        Task<IEnumerable<MeasurementCustomFieldSet>> GetFieldsForAllMeasurementsInJobAsync(IJob job);
        Task ClearFieldsForMeasurementAsync(IMeasurement measurement, IEnumerable<IMeasurementCustomField> fields);
    }
}