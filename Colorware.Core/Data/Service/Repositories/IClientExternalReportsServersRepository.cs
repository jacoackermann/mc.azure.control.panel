using System.Collections.Generic;
using System.Threading.Tasks;
using Colorware.Core.Data.Models;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IClientExternalReportsServersRepository : IServiceModelRepository<IClientExternalReportsServer> {
        Task LinkClientToExternalReportsServer(IClient item);
        Task LinkAllClientsToExternalReportsServer(IExternalReportsServer server);
        Task RemoveLinksForClient(long id);
        Task RemoveLinksForExternalReportsServer(long id);
        Task AddLinksToExternalReportsServer(IReadOnlyCollection<IClientServerConnection> clientServerConnections);
        Task RemoveNotInternalLinksForClient(long id);
        Task LinkClientToServer(IClientExternalReportsServer clientExternalReportsServer);
    }
}