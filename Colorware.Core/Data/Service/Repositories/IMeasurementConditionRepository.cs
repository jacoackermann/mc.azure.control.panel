﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IMeasurementConditionRepository : IServiceModelRepository<IMeasurementConditionsConfig> {
        /// <summary>
        /// Return measurement conditions that are not applied to a job.
        /// </summary>
        Task<IEnumerable<IMeasurementConditionsConfig>> FindAllPublicAsync();

        Task<IMeasurementConditionsConfig> ImportFromToleranceSet(IMeasurementConditionsConfig measurementCondition);
    }
}