﻿using System.Threading.Tasks;

using Colorware.Core.Data.Models;

namespace Colorware.Core.Data.Service.Repositories {
    public interface ICompanyRepository {
        /// <summary>
        /// Try to find the `primary' company in the repository (possibly the first). Can return null
        /// if no company was found.
        /// </summary>
        /// <returns>A company or <c>null</c>.</returns>
        Task<ICompany> GetPrimaryCompanyAsync();
    }
}