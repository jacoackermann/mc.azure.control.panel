﻿using System;
using System.Collections.Generic;

using Colorware.Core.Data.Models;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IReferenceRepositoryMultiplexer : IReferenceRepository {
        bool CanDeleteReferences([NotNull, ItemNotNull] IEnumerable<IReference> references);
        bool CanSaveReferences([NotNull, ItemNotNull] IEnumerable<IReference> references);
        bool CanDeleteReferencePatches([NotNull, ItemNotNull] IEnumerable<IReferencePatch> patches);
        bool CanSaveReferencePatches([NotNull, ItemNotNull] IEnumerable<IReferencePatch> patches);
    }

    public static class ReferenceRepositoryMultiplexerExtensions {
        public static bool CanDeleteReference(this IReferenceRepositoryMultiplexer referenceRepositoryMultiplexer,
            [NotNull] IReference reference) {
            if (reference == null) throw new ArgumentNullException(nameof(reference));

            return referenceRepositoryMultiplexer.CanDeleteReferences(new[] {reference});
        }

        public static bool CanSaveReference(this IReferenceRepositoryMultiplexer referenceRepositoryMultiplexer,
            [NotNull] IReference reference) {
            if (reference == null) throw new ArgumentNullException(nameof(reference));

            return referenceRepositoryMultiplexer.CanSaveReferences(new[] {reference});
        }

        public static bool CanDeleteReferencePatch(this IReferenceRepositoryMultiplexer referenceRepositoryMultiplexer,
            [NotNull] IReferencePatch patch) {
            if (patch == null) throw new ArgumentNullException(nameof(patch));

            return referenceRepositoryMultiplexer.CanDeleteReferencePatches(new[] {patch});
        }

        public static bool CanSaveReferencePatch(this IReferenceRepositoryMultiplexer referenceRepositoryMultiplexer,
            [NotNull] IReferencePatch patch) {
            if (patch == null) throw new ArgumentNullException(nameof(patch));

            return referenceRepositoryMultiplexer.CanSaveReferencePatches(new[] {patch});
        }
    }
}