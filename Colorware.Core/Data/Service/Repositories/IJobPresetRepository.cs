﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;
using Colorware.Core.Enums;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IJobPresetRepository : IServiceModelRepository<IJobPreset> {
        Task<IReadOnlyList<IJobPreset>> FindAllForMachineId(long machineId);
        Task<IReadOnlyList<IJobPreset>> FindAllForMachineIdAndJobType(long machineId, JobTypes jobType);
    }
}