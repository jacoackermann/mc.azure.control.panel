﻿using System.Collections.Immutable;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IGrayFinderRepository : IServiceModelRepository<IGrayFinderPatch> {
        [NotNull]
        Task DeleteAllForReferences(IImmutableList<long> referenceIds);
    }
}