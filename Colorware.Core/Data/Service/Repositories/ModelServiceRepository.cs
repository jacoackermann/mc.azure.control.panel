﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Colorware.Core.Data.Specification;
using Colorware.Core.Extensions;
using Colorware.Core.Functional.Option;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.Repositories {
    public class ModelServiceRepository<T> : IServiceModelRepository<T> where T : class, IBaseModel {
        private readonly IUntypedServiceModelRepository untypedRepository;

        public ModelServiceRepository([NotNull] IUntypedServiceModelRepository untypedRepository) {
            if (untypedRepository == null) throw new ArgumentNullException(nameof(untypedRepository));

            this.untypedRepository = untypedRepository;
        }

        public virtual Task DeleteAllAsync([NotNull] IReadOnlyList<T> items) {
            if (items == null) throw new ArgumentNullException(nameof(items));

            return untypedRepository.DeleteAllAsync(items);
        }

        public async Task DeleteAllNonCascadingAsync(IReadOnlyList<T> items) {
            // Delete in batches of 2000 because 2100 is the SQL upper limit
            foreach (var itemBatch in items.Batch(2000)) {
                var query = new Query(Query.QueryType.Or);
                itemBatch.ForEach(item => query.Eq("id", item.Id));
                await untypedRepository.DeleteNonCascadingAsync(typeof(T), query).ConfigureAwait(false);
            }
        }

        public virtual Task DeleteNonCascadingAsync([NotNull] Query query) {
            if (query == null) throw new ArgumentNullException(nameof(query));

            return untypedRepository.DeleteNonCascadingAsync(typeof(T), query);
        }

        /// <exception cref="TaskCanceledException" />
        [Pure]
        public virtual async Task<IReadOnlyList<T>> FindAllAsync(Query query, CancellationToken cancellationToken) {
            var results = await untypedRepository.FindAllAsync(typeof(T), query, cancellationToken)
                                                 .ConfigureAwait(false);

            return results.Cast<T>().ToList();
        }

        public virtual Task SaveAllAsync([NotNull] IReadOnlyList<T> items) {
            if (items == null) throw new ArgumentNullException(nameof(items));

            return untypedRepository.SaveAllAsync(items);
        }

        public Task SaveAllBulkAsync(IReadOnlyList<T> items) {
            if (items == null) throw new ArgumentNullException(nameof(items));

            return untypedRepository.SaveAllBulkAsync(items, Option<long>.Nothing);
        }
    }
}