﻿using System.Collections.Generic;

using Colorware.Core.Data.Models;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IProcessedReportsMeasurementsRepository
    {
        /// <summary>
        /// Return a list of measurement id's that still have to be send over to the server with the given id.
        /// </summary>
        /// <param name="serverId">The server id to get the measurements for.</param>
        /// <returns>A list of measurement ids that still have to be sent to the given server.</returns>
        IEnumerable<long> GetMeasurementsIdsToProcessForServer(long serverId);

        /// <summary>
        /// Mark a measurement as done (sent over) for a given server.
        /// </summary>
        /// <param name="serverId">The id of the server to mark the measurement for.</param>
        /// <param name="measurementId">The id of the measurement to mark.</param>
        void MarkDone(long serverId, long measurementId);

        IEnumerable<IFailedProcessedReportsMeasurement> GetFailedMeasurements(long id);
        void ResendMeasurement(long id);
        void ResendJob(long serverId, long jobId);
        void ResendAll(long serverId);
    }
}