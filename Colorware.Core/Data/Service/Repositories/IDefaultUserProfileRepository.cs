﻿using System.Collections.Generic;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IDefaultUserProfileRepositoryEntry {
        string Name { get; }
        string Path { get; }
    }

    public interface IDefaultUserProfileRepository {
        IEnumerable<IDefaultUserProfileRepositoryEntry> GetDefaultEntries();
    }
}