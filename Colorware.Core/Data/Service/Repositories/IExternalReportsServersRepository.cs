using System.Collections.Generic;
using System.Threading.Tasks;
using Colorware.Core.Data.Models;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IExternalReportsServersRepository : IServiceModelRepository<IExternalReportsServer> {
        Task<IReadOnlyList<IExternalReportsServer>> GetExternalServers();
        Task CreateExternalServer(IExternalReportsServer server);
        Task UpdateExternalServer(IExternalReportsServer server);
        Task DeleteExternalServer(IExternalReportsServer server);
        Task<IExternalReportsServer> GetExternalServer(long id);
        Task<IExternalReportsServer> GetExternalServerByUrl(string reportsUrl);
        Task<bool> ConnectionToUrlExists([NotNull] string url);
    }
}