﻿namespace Colorware.Core.Data.Service.Repositories {
    public interface IReferenceRepository : IReferenceProvider, IReferenceSaver, IReferenceDeleter {
    }
}