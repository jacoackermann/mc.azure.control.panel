﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IMeasurementCustomFieldConfigurationItemsRepository {
        Task<IEnumerable<IMeasurementCustomFieldsConfigurationItem>> SaveItemsAsync(
            IEnumerable<IMeasurementCustomFieldsConfigurationItem> items);

        Task<IEnumerable<IMeasurementCustomFieldsConfigurationItem>> GetItemsAsync();
    }
}