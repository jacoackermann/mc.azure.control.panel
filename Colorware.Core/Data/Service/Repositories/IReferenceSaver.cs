﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IReferenceSaver : IModelOrigin {
        Task SaveReferences([NotNull, ItemNotNull] IReadOnlyList<IReference> references);

        Task SaveReferencePatches([NotNull, ItemNotNull] IReadOnlyList<IReferencePatch> patches);
    }

    public static class ReferencesSaverExtensions {
        public static Task SaveReference(this IReferenceSaver referenceSaver, [NotNull] IReference reference) {
            return referenceSaver.SaveReferences(new[] {reference});
        }

        public static Task SaveReferencePatch(this IReferenceSaver referenceSaver, [NotNull] IReferencePatch patch) {
            return referenceSaver.SaveReferencePatches(new[] {patch});
        }
    }
}