﻿using System;

namespace Colorware.Core.Data.Service.Repositories {
    /// <summary>
    /// Provides a mechanism to keep track of to which source a certain model item belongs.
    /// Useful when multiple reference sources are used through a multiplexer.
    /// </summary>
    public interface IModelOrigin {
        /// <summary>
        /// Identifies a reference source (origin). Each provider should have its own unique ID.
        /// </summary>
        Guid OriginTypeGuid { get; }
    }
}