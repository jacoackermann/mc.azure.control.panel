﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Data.Models;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.Repositories {
    public interface IDefaultDotgainListRepository : IServiceModelRepository<IDefaultDotgainList> {
        [NotNull, ItemNotNull]
        Task<IReadOnlyList<IDefaultDotgainList>> GetOrderedDotgainLists();

        [NotNull, ItemNotNull]
        Task<IReadOnlyList<IDefaultDotgainEntry>> GetDotgainEntriesForDotgainListId(long dotgainListId);
    }

    public static class DefaultDotgainListRepositoryExtensions {
        [NotNull, ItemNotNull]
        public static Task<IReadOnlyList<IDefaultDotgainEntry>> GetDotgainEntriesForReference(
            this IDefaultDotgainListRepository repository,
            IReference reference) {
            return repository.GetDotgainEntriesForDotgainListId(reference.DefaultDotgainListId);
        }

        [NotNull, ItemNotNull]
        public static async Task<IReadOnlyList<IDefaultDotgainEntry>> GetDotgainEntriesForReferenceId(
            this IDefaultDotgainListRepository repository,
            long referenceId) {
            var referenceRepository = GlobalContainer.Current.Resolve<IDbReferenceRepository>();
            var reference = await referenceRepository.FindIncludingDeletedAsync(referenceId);
            if (reference == null)
                return new List<IDefaultDotgainEntry>();

            return await repository.GetDotgainEntriesForReference(reference);
        }
    }
}