﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Colorware.Core.Data.Specification;
using Colorware.Core.Functional.Option;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service {
    public interface IUntypedServiceModelRepository {
        /// <summary>
        /// Fetch all models of a given type from the datasource.
        /// </summary>
        /// <param name="type">The type of the items to find, can be interface or concrete type.</param>
        /// <param name="query">Additional query parameters to include in the search.</param>
        /// <param name="cancellationToken">Cancellation token to cancel the task.</param>
        /// <exception cref="TaskCanceledException" />
        [ItemNotNull]
        Task<IReadOnlyList<IBaseModel>> FindAllAsync([NotNull] Type type, [CanBeNull] Query query,
                                                     CancellationToken cancellationToken);

        /// <summary>
        /// Save a list of items back to the datasource, note that previously unsaved items are
        /// assigned an ID after saving and no longer have the NewInstance flag. (returning
        /// copies would be too expensive for most implementations)
        /// </summary>
        /// <param name="items">The items to save to the datasource.</param>
        [NotNull]
        Task SaveAllAsync([NotNull, ItemNotNull] IReadOnlyList<IBaseModel> items);

        /// <summary>
        /// Same as SaveAllAsync(), but MUCH faster for large collections.
        /// 
        /// Does not fully replace SaveAllAsync() yet because it needs more extensive testing
        /// before introducing it throughout the entire software.
        /// </summary>
        /// <param name="items">The items to save to the datasource.</param>
        /// <param name="remoteCompanyId"></param>
        [NotNull]
        Task SaveAllBulkAsync([NotNull, ItemNotNull] IReadOnlyList<IBaseModel> items, Option<long> remoteCompanyId);

        /// <summary>
        /// Delete a given collection of objects from the datasource.
        /// </summary>
        /// <param name="items">The items to delete from the datasource.</param>
        Task DeleteAllAsync([NotNull, ItemNotNull] IReadOnlyList<IBaseModel> items);

        /// <summary>
        /// Executes a query to delete all items of the given type using the given query parameters.
        /// </summary>
        /// <param name="type">The type of the items to find, can be interface or concrete type.</param>
        /// <param name="query">The query that specifies the items to delete.</param>
        Task DeleteNonCascadingAsync([NotNull] Type type, [NotNull] Query query);
    }

    public static class IUntypedServiceModelRepositoryExtensions {
        /// <summary>
        /// Fetch all models of a given type from the datasource.
        /// </summary>
        /// <param name="type">The type of the items to find, can be interface or concrete type.</param>
        /// <param name="query">Additional query parameters to include in the search.</param>
        [ItemNotNull]
        public static Task<IReadOnlyList<IBaseModel>> FindAllAsync(this IUntypedServiceModelRepository repository,
                                                                   [NotNull] Type type, [CanBeNull] Query query) {
            return repository.FindAllAsync(type, query, CancellationToken.None);
        }
    }
}