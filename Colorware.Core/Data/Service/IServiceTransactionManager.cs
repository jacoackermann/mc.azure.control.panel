﻿using System;

namespace Colorware.Core.Data.Service {
    /// <summary>
    /// Defines an interface for a transaction manager. Currently the client-server interface does not
    /// allow for different types of objects to be saved within a single call. Particularly, this does not allow
    /// relations to be setup and saved in a transactional matter. To make this work, the parent object currently has
    /// to be saved first after which its ID can be used on child objects. If something goes wrong during all of this,
    /// there is a risk of parent objects remaining alive in the database and the model may enter an unexpected state.
    /// 
    /// A transaction manager implementation creats <see cref="IServiceTransaction"/> instances which implement
    /// <see cref="IDisposable"/> and can be used as a safeguard in this situation. Objects can be saved at any time, but
    /// if the transaction is not commited, these objects will be destroyed when the transaction object is disposed.
    /// </summary>
    public interface IServiceTransactionManager {
        /// <summary>
        /// Create a new transaction object.
        /// </summary>
        /// <returns></returns>
        IServiceTransaction Begin();
    }
}