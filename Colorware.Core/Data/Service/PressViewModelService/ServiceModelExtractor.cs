﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Specification;
using Colorware.Core.Helpers;
using Colorware.Core.Logging;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.PressViewModelService {
    [UsedImplicitly]
    public class ServiceModelExtractor : IServiceModelExtractor {
        #region IModelExtractor Members
        public SimpleDictionaryModelData Extract<T>(T model) where T : IBaseModel {
            try {
                var data = new SimpleDictionaryModelData();
                var props = BaseModelReflectionHelper.GetServiceFieldProperties(model.GetType());

                foreach (var prop in props)
                    extractPropertyAndUpdateTimestamps(model, prop, data);

                return data;
            }
            catch (Exception e) {
                LogManager.GetLogger(GetType()).Error(
                    "Could not extract data from object of type: " + typeof(T).Name + "Error: " + e.Message, e);
                throw;
            }
        }

        public DataTable ExtractIntoDataTable<T>(DataTable dataTable, IEnumerable<T> models) where T : IBaseModel {
            if (models == null) throw new ArgumentNullException(nameof(models));

            // TODO: currently inefficient
            foreach (var model in models) {
                var row = dataTable.NewRow();
                var rawData = Extract(model);

                foreach (var pair in rawData.Items)
                    row[pair.Key] = pair.Value;

                dataTable.Rows.Add(row);
            }

            return dataTable;
        }
        #endregion

        /// <summary>
        /// Extracts the given property value from the model and updates it on the model if it's a CreatedAt/UpdatedAt.
        /// </summary>
        private void extractPropertyAndUpdateTimestamps([NotNull] IBaseModel model, [NotNull] PropertyInfo prop,
                                                        [NotNull] SimpleDictionaryModelData data) {
            var dbField = BaseModelReflectionHelper.GetCustomAttribute<ServiceField>(prop).ListingName;

            // NB: works fine for Nullable properties, value becomes null if HasValue = false (protobuf-net also supports it)
            var value = prop.GetValue(model);

            if (prop.PropertyType == typeof(bool)) {
                data.Set(dbField, (bool)value ? 1 : 0);
            }
            else if (prop.PropertyType == typeof(double) || prop.PropertyType == typeof(float)) {
                var doubleValue = Convert.ToDouble(value);

                if (double.IsNaN(doubleValue))
                    // TODO: This seems to be the default handling of invalid numbers for MySQL,
                    // but it is not optimal.
                    doubleValue = 0.0;

                data.Set(dbField, doubleValue);
            }
            else if (prop.PropertyType.IsEnum) {
                data.Set(dbField, Convert.ToInt32(value));
            }
            else if (prop.PropertyType == typeof(DateTime)) {
                var dt = (DateTime)value;
                if (prop.Name == "UpdatedAt") {
                    dt = DateTime.Now;
                    ((IUpdatedStamped)model).UpdatedAt = dt;
                }
                else if (prop.Name == "CreatedAt" && model.NewInstance) {
                    dt = DateTime.Now;
                    ((ICreatedStamped)model).CreatedAt = dt;
                }

                data.Set(dbField, dt);
            }
            else if (prop.PropertyType == typeof(string)) {
                // We never want NULL strings in the database
                data.Set(dbField, (string)value ?? string.Empty);
            }
            else {
                data.Set(dbField, value);
            }
        }
    }
}