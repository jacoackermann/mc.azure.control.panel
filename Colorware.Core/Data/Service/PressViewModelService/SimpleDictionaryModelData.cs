﻿using System;
using System.Collections.Generic;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.PressViewModelService {
    /// <summary>
    /// Class for keeping track of raw model fields.
    /// </summary>
    public class SimpleDictionaryModelData {
        private readonly Dictionary<string, object> fieldValues = new Dictionary<string, object>();

        public Dictionary<string, object> Items => fieldValues;

        public void Set([NotNull] string fieldName, object rawValue) {
            if (fieldName == null) throw new ArgumentNullException(nameof(fieldName));

            fieldValues[fieldName] = rawValue;
        }

        [CanBeNull]
        public object Get([NotNull] string fieldName) {
            if (fieldName == null) throw new ArgumentNullException(nameof(fieldName));

            object value;
            return fieldValues.TryGetValue(fieldName, out value) ? value : null;
        }
    }
}