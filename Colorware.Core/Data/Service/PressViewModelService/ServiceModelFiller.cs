﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Colorware.Core.Data.Specification;
using Colorware.Core.Helpers;
using Colorware.Core.Logging;

using JetBrains.Annotations;

namespace Colorware.Core.Data.Service.PressViewModelService {
    [UsedImplicitly]
    public class ServiceModelFiller : IServiceModelFiller {
        /// <summary>
        /// Helper class to keep together information about a property and its associated <see cref="ServiceField"/>.
        /// This is used to improve performance by only getting this information once for lists of items.
        /// </summary>
        private class ServiceFieldInfo {
            [NotNull]
            public PropertyInfo Property { get; }

            [NotNull]
            public ServiceField Field { get; }

            public ServiceFieldInfo([NotNull] PropertyInfo property) {
                if (property == null) throw new ArgumentNullException(nameof(property));

                Property = property;
                Field = BaseModelReflectionHelper.GetCustomAttribute<ServiceField>(property);
            }
        }

        #region IModelFiller Members
        public IBaseModel Fill(Type type, IBaseModel model, SimpleDictionaryModelData data) {
            if (type == null) throw new ArgumentNullException(nameof(type));
            if (model == null) throw new ArgumentNullException(nameof(model));
            if (data == null) throw new ArgumentNullException(nameof(data));

            var propertyInfos = BaseModelReflectionHelper.GetServiceFieldProperties(type)
                                                         .Select(prop => new ServiceFieldInfo(prop)).ToList();
            return fill(model, data, propertyInfos);
        }

        private T fill<T>(T model, SimpleDictionaryModelData data, IEnumerable<ServiceFieldInfo> props)
            where T : IBaseModel {
            try {
                foreach (var prop in props) {
                    var key = prop.Field.ListingName;
                    var rawValue = data.Get(key);

                    setPropertyToNativeValue(model, prop.Property, rawValue, prop.Field);
                }
                return model;
            }
            catch (Exception e) {
                LogManager.GetLogger(GetType()).Error(
                    "Could not fill data for object of type " + typeof(T).Name + " error: " + e.Message, e);
                throw;
            }
        }

        // TODO: performance can probably be greatly improved by using batching and parallelization
        // but this requires lots of async/await refactoring through the code-base
        public IReadOnlyList<IBaseModel> CreateAndFill(Type type,
            IEnumerable<SimpleDictionaryModelData> data) {
            if (type == null) throw new ArgumentNullException(nameof(type));
            if (data == null) throw new ArgumentNullException(nameof(data));

            var propertyInfos =
                BaseModelReflectionHelper.GetServiceFieldProperties(type)
                                         .Select(prop => new ServiceFieldInfo(prop))
                                         .ToArray();

            var results = new List<IBaseModel>(data.Count());
            foreach (var modelData in data) {
                var instance = ReflectionHelpers.CreateInstance<IBaseModel>(type);
                instance.NewInstance = false;
                results.Add(fill(instance, modelData, propertyInfos));
            }
            return results;
        }
        #endregion

        #region Fillers
        // NB: works fine for Nullable properties
        private static void setPropertyToNativeValue<T>(T model, PropertyInfo prop, object value, ServiceField field) {
            if (value == null) {
                if (!field.CanBeNull)
                    throw new Exception("Property cannot be null: " + field.ListingName);

                if (field.DefaultValue != null)
                    setPropertyToNativeValue(model, prop, field.DefaultValue, field);

                return;
            }

            var type = value.GetType();
            if (type == typeof(byte[]) && prop.PropertyType == typeof(string)) {
                var bytes = (byte[])value;
                var asString = System.Text.Encoding.ASCII.GetString(bytes, 0, bytes.Length);
                prop.CallSetter(model, asString);
            }
            else if (type == typeof(byte) && prop.PropertyType == typeof(bool)) {
                prop.CallSetter(model, (byte)value > 0);
            }
            else if (type == typeof(float) && prop.PropertyType == typeof(double)) {
                prop.CallSetter(model, Convert.ToDouble(value));
            }
            // Enum types of which DB column is 'byte', but C# representation is still 'enum : int'
            else if (type == typeof(byte) && prop.PropertyType.IsEnum &&
                     prop.PropertyType.GetEnumUnderlyingType() == typeof(int)) {
                prop.CallSetter(model, Convert.ToInt32(value));
            }
            else if (type == typeof(long) && prop.PropertyType == typeof(int)) {
                prop.CallSetter(model, Convert.ToInt32(value));
            }
            else if (type == typeof(int) && prop.PropertyType == typeof(long)) {
                prop.CallSetter(model, Convert.ToInt64(value));
            }
            else if (type == typeof(string) && prop.PropertyType == typeof(Guid)) {
                prop.CallSetter(model, new Guid((string)value));
            }
            else {
                prop.CallSetter(model, value);
            }
        }
        #endregion
    }
}