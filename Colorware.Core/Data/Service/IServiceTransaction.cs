﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Colorware.Core.Data.Specification;

namespace Colorware.Core.Data.Service {
    public interface IServiceTransaction : IDisposable {
        void Attach(IBaseModel instance);
        void Attach(IEnumerable<IBaseModel> instances);
        /// <summary>
        /// Do nothing, this will keep the instances in the database.
        /// </summary>
        void Commit();

        /// <summary>
        /// `Rollback', this will delete any items that are marked as being present in the database.
        /// </summary>
        Task Rollback();
    }

    public static class ServiceTransactionExtensions {
        public static T Attach<T>(this IServiceTransaction transaction, T instance) where T : IBaseModel {
            transaction.Attach(instance);
            return instance;
        }

        /// <summary>
        /// Saves the instance and adds it to the transaction. If the transaction is rolled back, the instance
        /// will be deleted again.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="transaction"></param>
        /// <param name="instance"></param>
        public static Task Save<T>(this IServiceTransaction transaction, T instance) where T : IBaseModel {
            transaction.Attach(instance);
            return instance.SaveAsync();
        }

        public static Task SaveAll<T>(this IServiceTransaction transaction, IEnumerable<T> instances)
            where T : IBaseModel {
            var instanceList = instances.Cast<IBaseModel>().ToList();
            transaction.Attach(instanceList);
            return BaseModel.SaveAllAsync(instanceList);
        }
    }
}