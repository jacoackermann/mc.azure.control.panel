﻿using System.Threading.Tasks;

using Colorware.Core.Data.Models;

namespace Colorware.Core.Data.Service
{
    public interface IPaperwhiteProvider {
        Task<ISample> GetPaperwhiteSample(ISample currentSample);
    }
}
