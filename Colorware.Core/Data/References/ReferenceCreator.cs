using System;

using Colorware.Core.Data.Models;

using JetBrains.Annotations;

namespace Colorware.Core.Data.References {
    [UsedImplicitly]
    public class ReferenceCreator : IReferenceCreator {
        private readonly IItemCreator<IReference> referenceCreator;

        public ReferenceCreator([NotNull] IItemCreator<IReference> referenceCreator) {
            if (referenceCreator == null) throw new ArgumentNullException("referenceCreator");

            this.referenceCreator = referenceCreator;
        }

        public IReference CreateItem() {
            return referenceCreator.CreateItem();
        }

        [NotNull]
        public IReference CreateSpotReference() {
            var reference = CreateItem();

            reference.IsSpotLib = true;
            reference.Name = "";

            return reference;
        }
    }
}