using Colorware.Core.Color;
using Colorware.Core.Data.Models;

using JetBrains.Annotations;

namespace Colorware.Core.Data.References {
    public interface IReferencePatchCreator {
        [NotNull]
        IReferencePatch CreateReferencePatch([NotNull] SpectralMeasurementConditions mc);
    }
}