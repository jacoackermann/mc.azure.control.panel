using Colorware.Core.Data.Models;

namespace Colorware.Core.Data.References {
    public interface IReferenceCreator : IItemCreator<IReference> {
        IReference CreateSpotReference();
    }
}