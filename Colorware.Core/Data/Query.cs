﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using Colorware.Core.Data.Specification;

using JetBrains.Annotations;

using ProtoBuf;

namespace Colorware.Core.Data {
    /// <summary>
    /// Helper class to abstract away query parameters when searching for a model.
    /// </summary>
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class Query {
        #region InnerClasses
        public enum QueryOperation {
            Contains,
            Eq,
            // Not equal
            Neq,
            Gt,
            Lt
        }

        // WHERE clause in our SQL generation implementation has two parts:
        // WHERE (... = ... AND ... = ...) AND (... = ... OR ... = ...)
        //                  1                              2
        //
        // The type specifies to which part the conditions will be added, the type is generally
        // set on Query level (in which case it applies to all QueryItems in the Query), but can
        // also be set for individual QueryItems to override the parent Query's QueryType
        public enum QueryType {
            // Add condition(s) to part 1
            And,
            // Add condition(s) to part 2
            Or,
            // Don't override parent Query's QueryType
            DontOverride
        }

        public enum OrderType {
            Ascending,
            Descending
        }

        [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
        public class QueryItem {
            public readonly QueryOperation Operation;

            public readonly string TableName;

            public readonly string FieldName;

            public readonly string Value;

            // Overrides its parent Query's QueryType
            public readonly QueryType TypeOverride;

            [ProtoIgnore]
            public bool HasTable => TableName != null;

            [ProtoIgnore]
            public string FullName {
                get {
                    if (HasTable)
                        return TableName + "." + FieldName;

                    return FieldName;
                }
            }

            // For serialization
            public QueryItem() {}

            public QueryItem(QueryOperation op, string fieldName, string tableName, string value,
                             QueryType typeOverride = QueryType.DontOverride) {
                Operation = op;
                FieldName = fieldName;
                TableName = tableName;
                Value = value;
                TypeOverride = typeOverride;
            }

            public QueryItem Copy() {
                return new QueryItem(Operation, FieldName, TableName, Value, TypeOverride);
            }

            public string ToDebugString() =>
                $"({Operation}: {FieldName} => {Value ?? "<null>"}";
        }

        [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
        public class OrderByItem {
            public readonly OrderType OrderType;
            public readonly string TableName;
            public readonly string FieldName;

            [ProtoIgnore]
            public bool HasTable => TableName != null;

            [ProtoIgnore]
            public string FullName =>
                HasTable
                    ? TableName + "." + FieldName
                    : FieldName;

            /// <summary>
            /// For serialization.
            /// </summary>
            public OrderByItem() {}


            public OrderByItem(OrderType orderType, string tableName, string fieldName) {
                OrderType = orderType;
                TableName = tableName;
                FieldName = fieldName;
            }

            public OrderByItem Copy() {
                return new OrderByItem(OrderType, TableName, FieldName);
            }
        }

        [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
        public class SelectTopItem {
            public readonly int NumberOfRows;

            /// <summary>
            /// For serialization.
            /// </summary>
            public SelectTopItem() {}

            public SelectTopItem(int numberOfRows) {
                NumberOfRows = numberOfRows;
            }

            public SelectTopItem Copy() {
                return new SelectTopItem(NumberOfRows);
            }
        }
        #endregion

        public QueryType TypeOfQuery;

        public List<QueryItem> QueryItems { get; }
        public OrderByItem OrderByInstance { get; private set; }
        public SelectTopItem TopItem { get; private set; }

        /// <summary>
        /// A hint that tells the query generation engine to also include any deleted items (<see
        /// cref="IBaseModel.IsDeleted"/>) into the result set. Use <see cref="IncludeDeleted"/> to set.
        /// </summary>
        public bool IncludeDeletedItemsInResult { get; private set; } = false;

        // For serialization
        public Query() : this(QueryType.And) {}

        [DebuggerStepThrough]
        public Query(QueryType typeOfQuery) {
            if (typeOfQuery == QueryType.DontOverride)
                throw new ArgumentException(
                    "QueryType.DontOverride is only valid for QueryItems, not for Query objects!");

            TypeOfQuery = typeOfQuery;
            QueryItems = new List<QueryItem>();
        }

        public Query Copy() {
            var q = new Query();

            foreach (var qi in QueryItems)
                // TODO: Should we use qi?.Copy() here?
                q.QueryItems.Add(new QueryItem(qi.Operation, qi.FieldName, qi.TableName, qi.Value, qi.TypeOverride));

            q.OrderByInstance = OrderByInstance?.Copy();
            q.TopItem = TopItem?.Copy();

            return q;
        }

        public void Add(QueryOperation op, string fieldName, string value,
                        QueryType typeOverride = QueryType.DontOverride) {
            QueryItems.Add(new QueryItem(op, fieldName, null, value, typeOverride));
        }

        public void Add(QueryOperation op, string fieldName, string tableName, string value,
                        QueryType typeOverride = QueryType.DontOverride) {
            QueryItems.Add(new QueryItem(op, fieldName, tableName, value, typeOverride));
        }

        public Query Contains(string fieldName, object value, QueryType typeOverride = QueryType.DontOverride) {
            if (value == null) throw new ArgumentNullException(nameof(value));
            Add(QueryOperation.Contains, fieldName, value.ToString(), typeOverride);
            return this;
        }

        public Query Contains(string fieldName, string tableName, object value,
                              QueryType typeOverride = QueryType.DontOverride) {
            if (value == null) throw new ArgumentNullException(nameof(value));
            Add(QueryOperation.Contains, fieldName, tableName, value.ToString(), typeOverride);
            return this;
        }

        public Query Eq(string fieldName, object value, QueryType typeOverride = QueryType.DontOverride) {
            if (value == null) throw new ArgumentNullException(nameof(value));
            Add(QueryOperation.Eq, fieldName, value.ToString(), typeOverride);
            return this;
        }

        public Query Eq(string fieldName, string tableName, object value,
                        QueryType typeOverride = QueryType.DontOverride) {
            if (value == null) throw new ArgumentNullException(nameof(value));
            Add(QueryOperation.Eq, fieldName, tableName, value.ToString(), typeOverride);
            return this;
        }

        public Query Neq(string fieldName, object value, QueryType typeOverride = QueryType.DontOverride) {
            if (value == null) throw new ArgumentNullException(nameof(value));
            Add(QueryOperation.Neq, fieldName, value.ToString(), typeOverride);
            return this;
        }

        public Query Neq(string fieldName, string tableName, object value,
                         QueryType typeOverride = QueryType.DontOverride) {
            if (value == null) throw new ArgumentNullException(nameof(value));
            Add(QueryOperation.Neq, fieldName, tableName, value.ToString(), typeOverride);
            return this;
        }

        /// <summary>
        /// Greater then
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        /// <param name="typeOverride"></param>
        /// <returns></returns>
        public Query Gt(string fieldName, object value, QueryType typeOverride = QueryType.DontOverride) {
            if (value == null) throw new ArgumentNullException(nameof(value));
            Add(QueryOperation.Gt, fieldName, value.ToString(), typeOverride);
            return this;
        }

        /// <summary>
        /// Greater then
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="tableName"></param>
        /// <param name="value"></param>
        /// <param name="typeOverride"></param>
        /// <returns></returns>
        public Query Gt(string fieldName, string tableName, object value,
                        QueryType typeOverride = QueryType.DontOverride) {
            if (value == null) throw new ArgumentNullException(nameof(value));
            Add(QueryOperation.Gt, fieldName, tableName, value.ToString(), typeOverride);
            return this;
        }

        public Query Lt(string fieldName, object value, QueryType typeOverride = QueryType.DontOverride) {
            if (value == null) throw new ArgumentNullException(nameof(value));
            Add(QueryOperation.Lt, fieldName, value.ToString(), typeOverride);
            return this;
        }

        public Query Lt(string fieldName, string tableName, object value,
                        QueryType typeOverride = QueryType.DontOverride) {
            if (value == null) throw new ArgumentNullException(nameof(value));
            Add(QueryOperation.Lt, fieldName, tableName, value.ToString(), typeOverride);
            return this;
        }

        public Query OrderBy([NotNull] string fieldName, [NotNull] string tableName, OrderType orderType) {
            if (fieldName == null) throw new ArgumentNullException(nameof(fieldName));
            if (tableName == null) throw new ArgumentNullException(nameof(tableName));
            OrderByInstance = new OrderByItem(orderType, fieldName, tableName);
            return this;
        }

        public Query OrderBy([NotNull] string fieldName, OrderType orderType) {
            if (fieldName == null) throw new ArgumentNullException(nameof(fieldName));
            OrderByInstance = new OrderByItem(orderType, null, fieldName);
            return this;
        }

        public Query Top(int top) {
            TopItem = new SelectTopItem(top);
            return this;
        }

        public Query IncludeDeleted() {
            IncludeDeletedItemsInResult = true;
            return this;
        }

        public string ToDebugString() {
            var sb = new StringBuilder();
            foreach (var queryItem in QueryItems) {
                sb.Append(queryItem.ToDebugString());
                sb.Append("; ");
            }
            return sb.ToString();
        }
    }
}