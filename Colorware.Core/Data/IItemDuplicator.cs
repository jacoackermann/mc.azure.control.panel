﻿using System.Threading.Tasks;

namespace Colorware.Core.Data {
    public interface IItemDuplicator<in TIn, TOut> {
        Task<TOut> DuplicateItemAsync(TIn item);
    }
}