﻿using System.Collections.Generic;

using Colorware.Core.Data.Models;

namespace Colorware.Core.Data {
    /// <summary>
    /// Represents the custom fields for a single measurement.
    /// </summary>
    public class MeasurementCustomFieldSet {
        public int Index { get; private set; }
        public IMeasurement Measurement { get; private set; }
        public IEnumerable<IMeasurementCustomField> MeasurementCustomFields { get; private set; }

        public MeasurementCustomFieldSet(int index, IMeasurement measurement,
            IEnumerable<IMeasurementCustomField> measurementCustomFields) {
            Index = index;
            Measurement = measurement;
            MeasurementCustomFields = measurementCustomFields;
        }
    }
}