﻿namespace Colorware.Core.Data {
    public interface IItemCreator<out T> {
        T CreateItem();
    }

    public interface IItemCreator<in TIn, out TOut> {
        TOut CreateItem(TIn input);
    }
}