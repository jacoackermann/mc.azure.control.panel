﻿namespace Colorware.Core.Data {
    public interface ILocalServiceModelIdGenerator {
        /// <summary>
        /// Generates a locally unique identifier to be used for newly created service model items.
        /// 
        /// This is useful when you need to specify relationships between model items (e.g. between 
        /// solid patches and dotgain patches), but haven't saved the items yet (meaning they don't
        /// have a DB unique id yet)
        /// </summary>
        /// <returns></returns>
        long GenerateId();
    }
}