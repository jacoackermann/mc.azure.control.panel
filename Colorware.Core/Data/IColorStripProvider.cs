﻿using System.Threading.Tasks;

using Colorware.Core.Data.Models;

namespace Colorware.Core.Data {
    public interface IColorStripProvider {
        Task<IColorStrip> GetColorStrip();
    }
}