﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Colorware.Core.Data.Specification;

using JetBrains.Annotations;

namespace Colorware.Core.Data {
    public interface IServiceModelRepository<T> where T : IBaseModel {
        /// <exception cref="TaskCanceledException" />
        [Pure, ItemNotNull]
        Task<IReadOnlyList<T>> FindAllAsync([CanBeNull] Query query, CancellationToken cancellationToken);

        Task SaveAllAsync([NotNull, ItemNotNull] IReadOnlyList<T> items);

        Task SaveAllBulkAsync([NotNull, ItemNotNull] IReadOnlyList<T> items);

        Task DeleteAllAsync([NotNull, ItemNotNull] IReadOnlyList<T> items);

        Task DeleteAllNonCascadingAsync([NotNull, ItemNotNull] IReadOnlyList<T> items);

        Task DeleteNonCascadingAsync([NotNull] Query query);
    }

    public static class IServiceModelRepositoryExtensions {
        [Pure, ItemNotNull]
        public static Task<IReadOnlyList<T>> FindAllAsync<T>(this IServiceModelRepository<T> repository)
            where T : IBaseModel {
            return repository.FindAllAsync(null);
        }

        [Pure, ItemNotNull]
        public static Task<IReadOnlyList<T>> FindAllAsync<T>(this IServiceModelRepository<T> repository,
                                                             [CanBeNull] Query query) where T : IBaseModel {
            // ReSharper disable once ExceptionNotDocumented
            return repository.FindAllAsync(query, CancellationToken.None);
        }

        /// <exception cref="TaskCanceledException" />
        [Pure, ItemNotNull]
        public static Task<IReadOnlyList<T>> FindAllAsync<T>(this IServiceModelRepository<T> repository,
                                                             CancellationToken cancellationToken) where T : IBaseModel {
            return repository.FindAllAsync(null, cancellationToken);
        }

        /// <exception cref="TaskCanceledException" />
        [Pure, ItemCanBeNull]
        public static async Task<T> FindIncludingDeletedAsync<T>(this IServiceModelRepository<T> repository, long id,
                                                                 CancellationToken cancellationToken)
            where T : IBaseModel {
            // Always IncludeDeleted here as we assume that when a specific id is requested the
            // client will still want to use that.
            return (await repository.FindAllAsync(new Query().Eq("id", id).IncludeDeleted(), cancellationToken))
                .FirstOrDefault();
        }

        [Pure, ItemCanBeNull]
        public static Task<T> FindIncludingDeletedAsync<T>(this IServiceModelRepository<T> repository, long id)
            where T : IBaseModel {
            return repository.FindIncludingDeletedAsync(id, CancellationToken.None);
        }

        public static Task SaveAsync<T>(this IServiceModelRepository<T> repository, [NotNull] T item)
            where T : IBaseModel {
            return repository.SaveAllAsync(new[] {item});
        }

        public static Task DeleteAsync<T>(this IServiceModelRepository<T> repository, [NotNull] T item)
            where T : IBaseModel {
            return repository.DeleteAllAsync(new[] {item});
        }
    }
}