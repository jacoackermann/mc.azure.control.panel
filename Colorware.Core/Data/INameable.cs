﻿using System;

using JetBrains.Annotations;

namespace Colorware.Core.Data {
    /// <summary>
    /// Definition of NAMEABLE
    /// 1 :  worthy of being named [memorable]
    /// 2 :  capable of being named [identifiable]
    /// </summary>
    [Obsolete]
    public interface INameableObsolete {
        [NotNull]
        string GetName();
        [CanBeNull]
        string GetDescription();
    }
}