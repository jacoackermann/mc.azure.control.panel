﻿using System.Threading.Tasks;

namespace Colorware.Core.Data {
    public interface IItemEditor<in TIn, TOut> {
        Task<TOut> EditItemAsync(TIn item);
    }
}