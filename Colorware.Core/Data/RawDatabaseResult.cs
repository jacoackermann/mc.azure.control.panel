﻿using System;

namespace Colorware.Core.Data {
    /// <summary>
    /// Helper class which wraps a database result set. The goal of the class is to provide a low memory/serialization impact so it can
    /// be easily serialized and sent over a communication connection.
    /// </summary>
    public class RawDatabaseResult {
        public string Type { get; set; }

        public string[] Columns;

        public object[][] Rows;

        public int Count {
            get { return Rows.Length; }
        }

        private bool idIndexCalculated;
        private int? idIndex;

        public int IdIndex {
            get {
                if (!idIndexCalculated)
                    idIndex = calculateIdIndex();
                idIndexCalculated = true;
                return idIndex.HasValue ? idIndex.Value : -1;
            }
        }

        private int calculateIdIndex() {
            // Meh, hardcoded string values.
            return Array.IndexOf(Columns, "id");
        }

        /// <summary>
        /// Get the index for the column with the specified name. If no such column was found, -1 is returned.
        /// </summary>
        /// <param name="columnName">The name of the column.</param>
        /// <returns>The index of the column in the results.</returns>
        public int ColumnIndex(string columnName) {
            return Array.IndexOf(Columns, columnName);
        }

        #region Implementation of IXmlSerializable
        #endregion
    }
}