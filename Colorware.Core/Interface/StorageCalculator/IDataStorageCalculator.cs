﻿using Colorware.Core.Functional.Result;

namespace Colorware.Core.Interface.StorageCalculator {
    public interface IDataStorageCalculator {
        /// <summary>
        /// Returns space in bytes
        /// </summary>
        /// <returns>Space in bytes</returns>
        Result<long> GetSpace();
    }
}