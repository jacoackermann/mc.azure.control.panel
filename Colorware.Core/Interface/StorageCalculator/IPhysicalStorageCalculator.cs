﻿using Colorware.Core.Functional.Result;

namespace Colorware.Core.Interface.StorageCalculator {
    public interface IPhysicalStorageCalculator {
        /// <summary>
        /// Returns space in bytes
        /// </summary>
        /// <returns>Space in bytes</returns>
        Result<long> GetSpace();
        
    }
}