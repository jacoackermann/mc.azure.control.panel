﻿using JetBrains.Annotations;

namespace Colorware.Core.Interface.CustomerData {
    public interface ICustomerDataProvider {
        bool IsCurrentCustomerActive();

        [NotNull]
        string GetCurrentConnectionString();
    }
}