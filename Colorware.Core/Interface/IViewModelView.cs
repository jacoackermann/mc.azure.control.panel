﻿namespace Colorware.Core.Interface {
    public interface IViewModelView<out TView, out TViewModel> {
        TView View { get; }
        TViewModel ViewModel { get; }
    }
}