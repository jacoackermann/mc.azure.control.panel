﻿namespace Colorware.Core.Interface {
    public interface IViewFactory<TView> {
        TView Create();

        void Destroy(TView instance);
    }

    // Useful when the View needs a ViewModel that needs some arguments to
    // be constructed (create the VM using a factory, then pass in into Create(VM))
    public interface IViewFactory<TView, in TViewModel> {
        TView Create(TViewModel viewModel);

        void Destroy(TView instance);
    }
}