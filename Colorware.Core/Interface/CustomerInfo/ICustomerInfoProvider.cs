﻿using JetBrains.Annotations;

namespace Colorware.Core.Interface.CustomerInfo {
    public interface ICustomerInfoProvider {
        bool IsCurrentCustomerActive();

        [NotNull]
        string GetCurrentConnectionString();
    }
}