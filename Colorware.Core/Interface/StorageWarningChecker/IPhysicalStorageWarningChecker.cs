﻿using Colorware.Core.Functional.Result;

namespace Colorware.Core.Interface.StorageWarningChecker {
    public interface IPhysicalStorageWarningChecker {
        /// <summary>
        /// Returns true if a warning should be thrown
        /// </summary>
        /// <returns></returns>
        Result<StorageWarningCheckerResult> ReachedWarningLimit();
    }
}