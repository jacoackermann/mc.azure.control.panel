﻿using System;

namespace Colorware.Core.Interface.StorageWarningChecker {
    /// <summary>
    /// Represents the result of the storage check.
    /// </summary>
    public class StorageWarningCheckerResult {
        public StorageWarningCheckerResult(long currentLimitInBytes,
                                           long actualUsedSizeInBytes,
                                           long totalAvailableSizeInBytes) {
            if (actualUsedSizeInBytes < 0) {
                throw new ArgumentOutOfRangeException(nameof(actualUsedSizeInBytes), actualUsedSizeInBytes,
                                                      "The parameter should be 0 or greater.");
            }
            if (currentLimitInBytes <= 0) {
                // Warning will be false if the limit has been configured as 0 (or less).
                LimitExceeded = false;
            }
            else {
                // Check the actual size against the limit.
                LimitExceeded = actualUsedSizeInBytes > currentLimitInBytes;
            }
            TotalAvailableSizeInBytes = totalAvailableSizeInBytes;
            CurrentLimitInBytes = currentLimitInBytes;
            ActualUsedSizeInBytes = actualUsedSizeInBytes;
        }

        /// <summary>
        /// Total size available on the device.
        /// </summary>
        public long TotalAvailableSizeInBytes { get; }

        /// <summary>
        /// Actual amount of used bytes.
        /// </summary>
        public long ActualUsedSizeInBytes { get; }

        /// <summary>
        /// The currently configured limit (will be 0 if no limit).
        /// </summary>
        public long CurrentLimitInBytes { get; }

        /// <summary>
        /// When <c>true</c>, this indicates that the limit has been reached or passed.
        /// </summary>
        public bool LimitExceeded { get; }
    }
}