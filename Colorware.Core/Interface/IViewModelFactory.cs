﻿namespace Colorware.Core.Interface {
    public interface IViewModelFactory<T> {
        T Create();
        void Release(T instance);
    }
}