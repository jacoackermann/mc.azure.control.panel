﻿namespace Colorware.Core.Interface.SignalR {
    public interface ISignalRWrapper {
        void Send(long progress);
    }
}