﻿using System;

using Castle.Core.Logging;

using JetBrains.Annotations;

namespace Colorware.Core.Notification {
    // TODO: this sucks because it eats the original call site, which should be in the log
    public class LoggingNotifierDecorator : INotifier {
        private readonly INotifier notifier;
        private readonly ILogger log;

        public LoggingNotifierDecorator([NotNull] INotifier notifier, [NotNull] ILogger logger) {
            if (notifier == null) throw new ArgumentNullException(nameof(notifier));
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            this.notifier = notifier;
            log = logger;
        }

        public void Notify(string message, string shortMessage) {
            log.Info(message);
            notifier.Notify(message, shortMessage);
        }
    }
}