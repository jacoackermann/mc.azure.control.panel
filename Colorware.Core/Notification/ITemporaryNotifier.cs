﻿using System;
using System.Threading.Tasks;

namespace Colorware.Core.Notification {
    public interface ITemporaryNotifier {
        Task NotifyTemporary(string message, TimeSpan duration);
    }

    public static class ITemporaryNotifierExtensions {
        public static Task NotifyTemporary(this ITemporaryNotifier temporaryNotifier, string message) {
            return temporaryNotifier.NotifyTemporary(message, TimeSpan.FromSeconds(3));
        }
    }
}