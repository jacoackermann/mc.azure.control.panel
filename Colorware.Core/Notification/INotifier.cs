﻿using JetBrains.Annotations;

namespace Colorware.Core.Notification {
    public interface INotifier {
        void Notify([NotNull] string message, [CanBeNull] string shortMessage);
    }

    public static class INotifierExtensions {
        public static void Notify(this INotifier notifier, [NotNull] string message) {
            notifier.Notify(message, null);
        }
    }
}