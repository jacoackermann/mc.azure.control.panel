using System;

using JetBrains.Annotations;

namespace Colorware.Gui.Desktop.ViewModels {
    public class TemporaryNotificationInput {
        public string Message { get; }
        public TimeSpan Duration { get; }

        public TemporaryNotificationInput([NotNull] string message, TimeSpan duration) {
            if (message == null) throw new ArgumentNullException(nameof(message));
            Message = message;
            Duration = duration;
        }
    }
}