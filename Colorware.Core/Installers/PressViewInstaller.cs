﻿using System;

using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using JetBrains.Annotations;

namespace Colorware.Core.Installers {
    public enum InstallerContext {
        ClientPreLogin,
        ClientPostLogin,
        Server
    }

    public abstract class PressViewInstaller : IWindsorInstaller {
        protected IWindsorContainer container;

        protected InstallerContext context { get; private set; }

        protected virtual void installPreLogin() {
        }

        protected virtual void installPostLogin() {
        }

        protected virtual void installAll() {
            installPreLogin();
            installPostLogin();
        }

        protected PressViewInstaller(InstallerContext context) {
            this.context = context;
        }

        public virtual void Install([NotNull] IWindsorContainer container, IConfigurationStore store) {
            if (container == null) throw new ArgumentNullException("container");

            this.container = container;

            switch (context) {
                case InstallerContext.ClientPreLogin:
                    installPreLogin();
                    break;
                case InstallerContext.ClientPostLogin:
                    installPostLogin();
                    break;
                default:
                    installAll();
                    break;
            }
        }
    }
}