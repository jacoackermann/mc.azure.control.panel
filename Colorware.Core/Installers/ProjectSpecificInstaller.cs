﻿using Castle.MicroKernel.Registration;

namespace Colorware.Core.Installers {
    public abstract class ProjectSpecificInstaller : PressViewInstaller {
        public ProjectSpecificInstaller(InstallerContext context) : base(context) {
        }

        protected abstract string belongingAssemblyName { get; }

        protected virtual FromAssemblyDescriptor fromBelongingAssembly {
            get { return Classes.FromAssemblyNamed(belongingAssemblyName); }
        }
    }
}