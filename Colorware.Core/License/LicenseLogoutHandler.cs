﻿using System;
using System.Threading.Tasks;

using Castle.Core.Logging;

using Colorware.Core.Authentication;
using Colorware.Core.License.Models;
using Colorware.Core.License.Service;

using JetBrains.Annotations;

namespace Colorware.Core.License {
    [UsedImplicitly]
    public class LicenseLogoutHandler : ILogoutHandler {
        private readonly LicenseHeartBeatMonitor heartBeatMonitor;
        private readonly LicenseTicketServiceClient licenseTicketServiceClient;
        private readonly LicenseTicketStore licenseTicketStore;
        private readonly ILogger log;

        public LicenseLogoutHandler([NotNull] LicenseHeartBeatMonitor heartBeatMonitor,
            [NotNull] LicenseTicketServiceClient licenseTicketServiceClient,
            [NotNull] LicenseTicketStore licenseTicketStore,
            [NotNull] ILogger log) {
            if (heartBeatMonitor == null) throw new ArgumentNullException("heartBeatMonitor");
            if (licenseTicketServiceClient == null) throw new ArgumentNullException("licenseTicketServiceClient");
            if (licenseTicketStore == null) throw new ArgumentNullException("licenseTicketStore");
            if (log == null) throw new ArgumentNullException("log");

            this.heartBeatMonitor = heartBeatMonitor;
            this.licenseTicketServiceClient = licenseTicketServiceClient;
            this.licenseTicketStore = licenseTicketStore;
            this.log = log;
        }

        public async Task OnLogout() {
            log.Info("Stopping license heartbeat monitor");
            heartBeatMonitor.StopMonitoring();

            var ticket = licenseTicketStore.GetLicenseTicket();

            try {
                log.Info("Releasing license ticket");
                await licenseTicketServiceClient.ReleaseTicketAsync(ticket);
            }
            catch (Exception e) {
                log.Error("An error occurred while releasing the license ticket!", e);
                // TODO
            }
            finally {
                licenseTicketStore.SetLicenseTicket(LicenseTicket.InvalidTicket);
            }
        }
    }
}