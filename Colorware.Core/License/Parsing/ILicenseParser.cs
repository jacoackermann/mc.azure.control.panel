﻿using JetBrains.Annotations;

namespace Colorware.Core.License.Parsing {
    public interface ILicenseParser {
        [NotNull]
        Models.License Parse([NotNull] string licenseFileContents);
    }
}