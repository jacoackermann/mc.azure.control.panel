﻿using System;
using System.Runtime.Serialization;

namespace Colorware.Core.License {
    [Serializable]
    public class LicenseFunctionNotFoundException : Exception {
        public LicenseFunctionNotFoundException() {
        }

        public LicenseFunctionNotFoundException(string message) : base(message) {
        }

        public LicenseFunctionNotFoundException(string message, Exception inner) : base(message, inner) {
        }

        protected LicenseFunctionNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context) {
        }
    }
}