﻿using System;

using Colorware.Core.License.Models;

using JetBrains.Annotations;

namespace Colorware.Core.License {
    [UsedImplicitly]
    public class LicenseTicketStore {
        [CanBeNull]
        private LicenseTicket licenseTicket;

        [NotNull]
        public LicenseTicket GetLicenseTicket() {
            return licenseTicket ?? LicenseTicket.InvalidTicket;
        }

        public void SetLicenseTicket([NotNull] LicenseTicket ticket) {
            if (ticket == null) throw new ArgumentNullException("ticket");

            licenseTicket = ticket;
        }
    }
}