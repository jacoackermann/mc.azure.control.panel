﻿using System;
using System.IO;
using System.Text;
using System.Xml;

using Colorware.Core.License.Hardware;

using JetBrains.Annotations;

namespace Colorware.Core.License.OfflineActivation {
    [UsedImplicitly]
    public class OfflineActivationFileGenerator : IOfflineActivationFileGenerator {
        private readonly IHardwareEnumerator hardwareEnumerator;

        public OfflineActivationFileGenerator([NotNull] IHardwareEnumerator hardwareEnumerator) {
            if (hardwareEnumerator == null) throw new ArgumentNullException("hardwareEnumerator");

            this.hardwareEnumerator = hardwareEnumerator;
        }

        public string Generate([NotNull] string key) {
            if (key == null) throw new ArgumentNullException("key");

            using (var stream = new MemoryStream()) {
                var settings = new XmlWriterSettings {Encoding = Encoding.UTF8};

                using (var xml = XmlWriter.Create(stream, settings))
                    writeActivatePart(xml, key);

                return Encoding.UTF8.GetString(stream.GetBuffer(), 0, (int)stream.Length);
            }
        }

        private void writeActivatePart([NotNull] XmlWriter xml, [NotNull] string key) {
            xml.WriteStartElement("Activate");
            xml.WriteAttributeString("Key", key);
            writeHardwarePart(xml);
            xml.WriteEndElement();
        }

        private void writeHardwarePart([NotNull] XmlWriter xml) {
            var hardwareItems = hardwareEnumerator.GetElements();
            xml.WriteStartElement("Hardware");
            foreach (var item in hardwareItems)
                writeHardwareElementPart(xml, item);

            xml.WriteEndElement();
        }

        private void writeHardwareElementPart([NotNull] XmlWriter xml, [NotNull] HardwareElement item) {
            xml.WriteStartElement("Item");
            xml.WriteAttributeString("Id", item.Id);
            xml.WriteAttributeString("Value", item.Value);
            xml.WriteEndElement();
        }
    }
}