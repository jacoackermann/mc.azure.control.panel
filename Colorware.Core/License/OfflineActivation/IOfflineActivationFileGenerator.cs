﻿using JetBrains.Annotations;

namespace Colorware.Core.License.OfflineActivation {
    /// <summary>
    /// Generates content for a file that can be used for offline activation on the
    /// central license server. The file is based on the specfied key.
    /// </summary>
    public interface IOfflineActivationFileGenerator {
        string Generate([NotNull] string key);
    }
}