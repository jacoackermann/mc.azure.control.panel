﻿using System;
using System.IO;

using Castle.Core.Logging;

using Colorware.Core.Config;
using Colorware.Core.License.Models;
using Colorware.Core.License.Serializing;

using JetBrains.Annotations;

namespace Colorware.Core.License {
    /// <summary>
    /// Provides a cross-runtime license lock to keep track of the status of a license.
    /// If a license is acquired this class can create a marker that will be released as the license is released.
    /// If the application crashes, the marker will remain in place allowing the client to release the license
    /// when it next starts.
    /// </summary>
    [UsedImplicitly]
    public class LicenseTicketLockManager {
        private readonly LicenseTicketLockSerializer licenseTicketLockSerializer;
        private readonly LicenseTicketLockDeserializer licenseTicketLockDeserializer;
        private readonly ILogger log;
        private const string LockFileName = "ticket.lock";

        public LicenseTicketLockManager(
            [NotNull] LicenseTicketLockSerializer licenseTicketLockSerializer,
            [NotNull] LicenseTicketLockDeserializer licenseTicketLockDeserializer,
            [NotNull] ILogger logger) {
            if (licenseTicketLockSerializer == null)
                throw new ArgumentNullException(nameof(licenseTicketLockSerializer));
            if (licenseTicketLockDeserializer == null)
                throw new ArgumentNullException(nameof(licenseTicketLockDeserializer));
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            this.licenseTicketLockSerializer = licenseTicketLockSerializer;
            this.licenseTicketLockDeserializer = licenseTicketLockDeserializer;
            log = logger;
        }

        public void Lock([CanBeNull] LicenseTicket ticket) {
            var path = getLockFilePath();
            if (ticket == null && File.Exists(path)) {
                File.Delete(path);
            }
            else {
                try {
                    licenseTicketLockSerializer.Serialize(path, new LicenseTicketLock(ticket));
                }
                catch (Exception ex) {
                    log.Error("Error writing lock: " + ex.Message);
                }
            }
        }

        [CanBeNull]
        public LicenseTicketLock GetLock() {
            var path = getLockFilePath();
            if (!File.Exists(path)) {
                return null;
            }
            else {
                try {
                    var licenseTicketLock = licenseTicketLockDeserializer.Deserialize(path);
                    return licenseTicketLock;
                }
                catch (Exception ex) {
                    log.Error("Error reading lock: " + ex.Message);
                    return null;
                }
            }
        }

        private string getLockFilePath() {
            return GlobalConfig.GetDataPath(LockFileName);
        }

        public void Unlock() {
            var path = getLockFilePath();
            try {
                if (File.Exists(path))
                    File.Delete(path);
            }
            catch (Exception ex) {
                log.Error("Error releasing lock: " + ex.Message);
            }
        }
    }
}