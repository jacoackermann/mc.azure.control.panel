﻿using System;

using JetBrains.Annotations;

namespace Colorware.Core.License {
    // TODO: get rid of this attribute and use DI instead (eventually)
    public class RequiresLicense : Attribute {
        public enum CheckMode {
            FunctionGuid,
            Property
        }

        public readonly Guid FunctionGuid;

        [CanBeNull]
        public readonly string PropertyToCheck;

        public RequiresLicense(string param, CheckMode checkMode = CheckMode.FunctionGuid) {
            switch (checkMode) {
                case CheckMode.FunctionGuid:
                    FunctionGuid = new Guid(param);
                    break;
                case CheckMode.Property:
                    PropertyToCheck = param;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("checkMode");
            }
        }
    }
}