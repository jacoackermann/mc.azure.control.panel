﻿using System;
using System.IO;
using System.Xml.Serialization;

using Colorware.Core.License.Models;
using Colorware.Cryptography.Encryption;
using Colorware.Cryptography.KeyGenerators;

using JetBrains.Annotations;

namespace Colorware.Core.License.Serializing {
    [UsedImplicitly]
    public class LicenseTicketLockSerializer {
        private readonly ISymmetricKeyGenerator symmetricKeyGenerator;
        private readonly ISymmetricEncyptor encryptor;

        public LicenseTicketLockSerializer([NotNull] ISymmetricKeyGenerator symmetricKeyGenerator,
                                           [NotNull] ISymmetricEncyptor encryptor) {
            if (symmetricKeyGenerator == null) throw new ArgumentNullException("symmetricKeyGenerator");
            if (encryptor == null) throw new ArgumentNullException("encryptor");
            this.symmetricKeyGenerator = symmetricKeyGenerator;
            this.encryptor = encryptor;
        }

        public void Serialize([NotNull] string path, [NotNull] LicenseTicketLock licenseTicketLock) {
            if (path == null) throw new ArgumentNullException("path");
            if (licenseTicketLock == null) throw new ArgumentNullException("licenseTicketLock");

            var key = symmetricKeyGenerator.Generate();
            using (var fStream = new FileStream(path, FileMode.Create, FileAccess.Write)) {
                using (var stream = encryptor.CreateEncryptingStream(key, fStream)) {
                    var serializer = new XmlSerializer(typeof(LicenseTicketLock));
                    serializer.Serialize(stream, licenseTicketLock);
                }
            }
        }
    }
}