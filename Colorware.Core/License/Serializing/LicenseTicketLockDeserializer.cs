﻿using System;
using System.IO;
using System.Xml.Serialization;

using Colorware.Core.License.Models;
using Colorware.Cryptography.Decryption;
using Colorware.Cryptography.KeyGenerators;

using JetBrains.Annotations;

namespace Colorware.Core.License.Serializing {
    [UsedImplicitly]
    public class LicenseTicketLockDeserializer {
        private readonly ISymmetricKeyGenerator symmetricKeyGenerator;
        private readonly ISymmetricDecryptor symmetricDecryptor;

        public LicenseTicketLockDeserializer([NotNull] ISymmetricKeyGenerator symmetricKeyGenerator,
            [NotNull] ISymmetricDecryptor symmetricDecryptor) {
            if (symmetricKeyGenerator == null) throw new ArgumentNullException("symmetricKeyGenerator");
            if (symmetricDecryptor == null) throw new ArgumentNullException("symmetricDecryptor");
            this.symmetricKeyGenerator = symmetricKeyGenerator;
            this.symmetricDecryptor = symmetricDecryptor;
        }

        public LicenseTicketLock Deserialize([NotNull] string path) {
            if (path == null) throw new ArgumentNullException("path");

            var key = symmetricKeyGenerator.Generate();
            using (var fStream = new FileStream(path, FileMode.Open, FileAccess.Read)) {
                using (var stream = symmetricDecryptor.CreateDecryptingStream(key, fStream)) {
                    var deserializer = new XmlSerializer(typeof(LicenseTicketLock));
                    var licenseTicketLock = (LicenseTicketLock)deserializer.Deserialize(stream);
                    return licenseTicketLock;
                }
            }
        }
    }
}