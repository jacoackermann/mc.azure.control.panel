﻿using System;

using JetBrains.Annotations;

namespace Colorware.Core.License.Hardware {
    public class HardwareElement {
        public string Id { get; private set; }
        public string Value { get; private set; }

        public HardwareElement(string id, [NotNull] string value) {
            if (value == null) throw new ArgumentNullException("value");

            Id = id;
            Value = value;
        }

        public override string ToString() {
            return string.Format("{0}: {1}", Id, Value);
        }
    }
}