﻿using System.Collections.Generic;

using JetBrains.Annotations;

namespace Colorware.Core.License.Hardware {
    public interface IHardwareEnumerator {
        [ItemNotNull]
        IEnumerable<HardwareElement> GetElements();
    }
}