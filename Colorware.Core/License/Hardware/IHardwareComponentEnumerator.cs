﻿using System.Collections.Generic;

using JetBrains.Annotations;

namespace Colorware.Core.License.Hardware {
    public interface IHardwareComponentEnumerator {
        [ItemNotNull]
        IEnumerable<HardwareElement> GetElements();
    }
}