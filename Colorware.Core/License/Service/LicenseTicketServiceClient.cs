using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

using Colorware.Core.Helpers;
using Colorware.Core.License.Models;
using Colorware.Core.License.Repositories;

using JetBrains.Annotations;

namespace Colorware.Core.License.Service {
    // Communicates with PV server LicenseController (service)
    [UsedImplicitly]
    public class LicenseTicketServiceClient {
        private readonly LicenseTicketLockManager licenseTicketLockManager;
        private readonly ILicenseServerUrlRepository licenseServerUrlRepository;
        private readonly WebRequestHelper webRequestHelper;
        private const string GetProductAvailabilitiesUrl = "Api/LicenseTicket/GetProductAvailabilities";
        private const string ClaimTicketUrl = "Api/LicenseTicket/ClaimTicket";
        private const string RenewTicketUrl = "Api/LicenseTicket/RenewTicket";
        private const string ReleaseTicketUrl = "Api/LicenseTicket/ReleaseTicket";
        private const string ReleaseStaleTicketUrl = "Api/LicenseTicket/ReleaseStaleTicket";

        public LicenseTicketServiceClient([NotNull] LicenseTicketLockManager licenseTicketLockManager,
            [NotNull] ILicenseServerUrlRepository licenseServerUrlRepository,
            [NotNull] WebRequestHelper webRequestHelper) {
            if (licenseTicketLockManager == null) throw new ArgumentNullException("licenseTicketLockManager");
            if (licenseServerUrlRepository == null) throw new ArgumentNullException("licenseServerUrlRepository");
            if (webRequestHelper == null) throw new ArgumentNullException("webRequestHelper");

            this.licenseTicketLockManager = licenseTicketLockManager;
            this.licenseServerUrlRepository = licenseServerUrlRepository;
            this.webRequestHelper = webRequestHelper;
        }

        [NotNull, ItemNotNull]
        public Task<GetProductAvailabilitiesResult> GetProductAvailabilitiesAsync() {
            return GetProductAvailabilitiesAsync(CancellationToken.None);
        }

        [ItemNotNull, NotNull]
        public Task<GetProductAvailabilitiesResult> GetProductAvailabilitiesAsync(
            CancellationToken cancellationToken) {
            var url = licenseServerUrlRepository.GetUrl();
            return GetProductAvailabilitiesAsync(cancellationToken, url);
        }

        [NotNull, ItemNotNull]
        public async Task<GetProductAvailabilitiesResult> GetProductAvailabilitiesAsync(
            CancellationToken cancellationToken, [NotNull] string licenseHostUrl) {
            try {
                var currentLock = licenseTicketLockManager.GetLock();
                if (currentLock != null)
                    await releaseCurrentLock(currentLock).ConfigureAwait(false);
            }
            catch (Exception ex) {
                Debug.WriteLine("Error getting lock: " + ex.Message);
            }

            return
                await webRequestHelper.GetAsProtoBufAsync<GetProductAvailabilitiesResult>(
                    new Uri(licenseHostUrl), GetProductAvailabilitiesUrl, cancellationToken)
                                      .ConfigureAwait(false);
        }

        /// <exception cref="AggregateException" />
        /// <exception cref="HttpRequestException" />
        /// <exception cref="Exception" />
        [ItemCanBeNull]
        public async Task<LicenseTicket> ClaimTicketAsync(Guid productGuid) {
            var result =
                await
                    webRequestHelper.PostAsProtoBufAsync<Guid, ClaimLicenseTicketResult>(
                        new Uri(licenseServerUrlRepository.GetUrl()),
                        ClaimTicketUrl, productGuid).ConfigureAwait(false);

            licenseTicketLockManager.Lock(result.LicenseTicket);

            if (result.Error)
                return null;

            result.LicenseTicket.LastUpdateClientTime = DateTime.Now;
            return result.LicenseTicket;
        }

        [ItemCanBeNull]
        public async Task<LicenseTicket> RenewTicketAsync([NotNull] LicenseTicket licenseTicket) {
            if (licenseTicket == null) throw new ArgumentNullException("licenseTicket");

            var result =
                await
                    webRequestHelper.PostAsProtoBufAsync<LicenseTicket, RenewLicenseTicketResult>(
                        new Uri(licenseServerUrlRepository.GetUrl()), RenewTicketUrl, licenseTicket)
                                    .ConfigureAwait(false);

            if (result.Error)
                return null;

            result.LicenseTicket.LastUpdateClientTime = DateTime.Now;
            return result.LicenseTicket;
        }

        public async Task ReleaseTicketAsync([NotNull] LicenseTicket licenseTicket) {
            if (licenseTicket == null) throw new ArgumentNullException("licenseTicket");

            await webRequestHelper.PostAsProtoBufAsync(new Uri(licenseServerUrlRepository.GetUrl()), ReleaseTicketUrl,
                licenseTicket).ConfigureAwait(false);

            licenseTicketLockManager.Unlock();
        }

        private async Task releaseCurrentLock([NotNull] LicenseTicketLock licenseTicketLock) {
            await
                webRequestHelper.PostAsProtoBufAsync(new Uri(licenseTicketLock.LicenseServerHost), ReleaseStaleTicketUrl,
                    licenseTicketLock).ConfigureAwait(false);

            licenseTicketLockManager.Unlock();
        }
    }
}