﻿using System;

using Colorware.Core.License.Models;
using Colorware.Core.Service;

using JetBrains.Annotations;

using ProtoBuf;

namespace Colorware.Core.License.Service {
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class ClaimLicenseTicketResult : IWebApiResult {
        private ClaimLicenseTicketResult() {
        }

        private ClaimLicenseTicketResult(bool error) {
            Error = error;
        }

        private ClaimLicenseTicketResult([NotNull] LicenseTicket ticket) {
            if (ticket == null) throw new ArgumentNullException("ticket");

            LicenseTicket = ticket;
        }

        public static ClaimLicenseTicketResult CreateError() {
            return new ClaimLicenseTicketResult(true);
        }

        public static ClaimLicenseTicketResult CreateSuccess([NotNull] LicenseTicket ticket) {
            if (ticket == null) throw new ArgumentNullException("ticket");

            return new ClaimLicenseTicketResult(ticket);
        }

        public bool Error { get; private set; }

        [CanBeNull]
        public LicenseTicket LicenseTicket { get; private set; }
    }
}