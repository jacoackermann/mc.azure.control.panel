﻿using System;
using System.Collections.Generic;

using Colorware.Core.License.Models;

using JetBrains.Annotations;

using ProtoBuf;

namespace Colorware.Core.License.Service {
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class GetProductAvailabilitiesResult {
        [CanBeNull]
        public long? LicenseId { get; private set; }

        [NotNull, ItemNotNull]
        public List<ProductAvailability> ProductAvailabilites { get; private set; }

        public GetProductAvailabilitiesResult([CanBeNull] long? licenseId,
            [NotNull, ItemNotNull] List<ProductAvailability> productAvailabilites) {
            if (productAvailabilites == null) throw new ArgumentNullException("productAvailabilites");

            LicenseId = licenseId;
            ProductAvailabilites = productAvailabilites;
        }

        private GetProductAvailabilitiesResult() {
            ProductAvailabilites = new List<ProductAvailability>();
        }
    }
}