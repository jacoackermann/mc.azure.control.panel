﻿using System;

using Colorware.Core.License.Models;
using Colorware.Core.Service;

using JetBrains.Annotations;

using ProtoBuf;

namespace Colorware.Core.License.Service {
    [ProtoContract(ImplicitFields = ImplicitFields.AllFields)]
    public class RenewLicenseTicketResult : IWebApiResult {
        private RenewLicenseTicketResult() {
        }

        private RenewLicenseTicketResult(bool error) {
            Error = error;
        }

        private RenewLicenseTicketResult([NotNull] LicenseTicket ticket) {
            if (ticket == null) throw new ArgumentNullException("ticket");

            LicenseTicket = ticket;
        }

        public static RenewLicenseTicketResult CreateError() {
            return new RenewLicenseTicketResult(true);
        }

        public static RenewLicenseTicketResult CreateSuccess([NotNull] LicenseTicket ticket) {
            if (ticket == null) throw new ArgumentNullException("ticket");

            return new RenewLicenseTicketResult(ticket);
        }

        public bool Error { get; private set; }

        [CanBeNull]
        public LicenseTicket LicenseTicket { get; private set; }
    }
}