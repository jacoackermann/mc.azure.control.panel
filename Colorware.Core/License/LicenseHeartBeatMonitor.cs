using System;
using System.Threading;
using System.Threading.Tasks;

using Colorware.Core.Application;
using Colorware.Core.ErrorReporting;
using Colorware.Core.Helpers;
using Colorware.Core.License.Models;
using Colorware.Core.License.Service;
using Colorware.Core.Logging;

using JetBrains.Annotations;

namespace Colorware.Core.License {
    [UsedImplicitly]
    public class LicenseHeartBeatMonitor : IDisposable {
        private readonly LicenseTicketStore licenseTicketStore;
        private readonly LicenseTicketServiceClient licenseTicketServiceClient;
        private readonly IErrorReporter errorReporter;
        private readonly ILogManager logManager;
        private readonly IApplicationExiter applicationExiter;

        private static readonly TimeSpan hearbeatInterval = ApplicationHelper.IsDebugBuild()
            ? TimeSpan.FromSeconds(10)
            : TimeSpan.FromMinutes(5);

        private static readonly TimeSpan maxTicketLifetime = ApplicationHelper.IsDebugBuild()
            ? TimeSpan.FromSeconds(90)
            : TimeSpan.FromMinutes(30);

        private static readonly TimeSpan oldTicketWarningAfter = ApplicationHelper.IsDebugBuild()
            ? TimeSpan.FromSeconds(31)
            : TimeSpan.FromMinutes(11);

        private Task monitorTask;
        private CancellationTokenSource monitorLoopCancellationSource;

        public LicenseHeartBeatMonitor([NotNull] LicenseTicketStore licenseTicketStore,
            [NotNull] LicenseTicketServiceClient licenseTicketServiceClient,
            [NotNull] IErrorReporter errorReporter,
            [NotNull] ILogManager logManager,
            [NotNull] IApplicationExiter applicationExiter) {
            if (licenseTicketStore == null) throw new ArgumentNullException("licenseTicketStore");
            if (licenseTicketServiceClient == null) throw new ArgumentNullException("licenseTicketServiceClient");
            if (errorReporter == null) throw new ArgumentNullException("errorReporter");
            if (logManager == null) throw new ArgumentNullException("logManager");
            if (applicationExiter == null) throw new ArgumentNullException("applicationExiter");

            this.licenseTicketStore = licenseTicketStore;
            this.licenseTicketServiceClient = licenseTicketServiceClient;
            this.errorReporter = errorReporter;
            this.logManager = logManager;
            this.applicationExiter = applicationExiter;
        }


        // Should be called at program start (but only after a ticket has been set in the LicenseTicketStore)
        public void StartMonitoring() {
            if (monitorTask != null)
                throw new InvalidOperationException("License heartbeat monitor is already running!");

            monitorLoopCancellationSource = new CancellationTokenSource();
            monitorTask = Task.Factory.StartNew(new Func<object, Task>(monitorLoop), monitorLoopCancellationSource.Token,
                TaskCreationOptions.LongRunning);
        }

        // Should be called at program end
        public void StopMonitoring() {
            if (monitorTask == null || monitorLoopCancellationSource.IsCancellationRequested)
                return;

            monitorLoopCancellationSource.Cancel();
        }

        private async Task monitorLoop(object cancellationToken) {
            try {
                var token = (CancellationToken)cancellationToken;
                bool failedCheck = false;

                while (true) {
                    try {
                        await Task.Delay(hearbeatInterval, token);
                    }
                    catch (TaskCanceledException) {
                        break;
                    }

                    // Renew ticket or reclaim if the server purged our ticket (e.g. when computer
                    // was sleeping/suspended)
                    await renewOrReclaimTicket(licenseTicketStore.GetLicenseTicket());

                    // Check ticket, allow to fail once for when computer just came out
                    // of sleep/hibernation
                    failedCheck = !checkCurrentTicketAge(!failedCheck);
                }

                monitorTask = null;
                monitorLoopCancellationSource = null;
            }
            catch (Exception e) {
                errorReporter.ReportError(
                    string.Format(
                        "An error occurred while checking the license, {0} will now shutdown.\n\nPlease contact your system administrator if this keeps happening.",
                        ApplicationHelper.ApplicationName), "License checking error", e);

                if (!ApplicationHelper.IsDebugBuild())
                    applicationExiter.Exit();
            }
        }

        private async Task renewOrReclaimTicket([NotNull] LicenseTicket ticket) {
            if (ticket == null) throw new ArgumentNullException("ticket");

            var renewedTicket = await renewTicket(ticket);

            // When computer comes out of sleep/hibernation we need to claim a new one,
            // because ticket was probably purged from server due to old age
            if (renewedTicket == null) {
                try {
                    renewedTicket =
                        await licenseTicketServiceClient.ClaimTicketAsync(ticket.Product.Guid);
                }
                catch {
                }
            }

            if (renewedTicket != null)
                licenseTicketStore.SetLicenseTicket(renewedTicket);
        }

        [ItemCanBeNull]
        private async Task<LicenseTicket> renewTicket([NotNull] LicenseTicket ticket) {
            if (ticket == null) throw new ArgumentNullException("ticket");

            LicenseTicket renewedTicket = null;

            try {
                renewedTicket = await licenseTicketServiceClient.RenewTicketAsync(ticket);

                // NB: If it keeps failing we will end up with an old ticket (LicenseTicket.ClaimedAt)
                // and we will act accordingly in checkTicketAge(), so we don't have to fail hard here
                if (renewedTicket == null)
                    logManager.Get(GetType())
                              .Warn(string.Format(
                                  "License ticket service returned no ticket at renewal! (original ticket was claimed at: {0})",
                                  ticket.LastUpdateClientTime));
            }
                // ReSharper disable once EmptyGeneralCatchClause
                // No warning here as we warn in checkTicketAge()
            catch {
            }

            return renewedTicket;
        }

        /// <summary>
        /// Checks the current ticket age and takes the appropriate action if it's too old
        /// or if the user tampered with the system clock.
        /// </summary>
        /// <param name="allowFail">Iff true, we won't take action when the license is too old.</param>
        /// <returns>True iff the ticket passed the checks.</returns>
        private bool checkCurrentTicketAge(bool allowFail) {
            var ticket = licenseTicketStore.GetLicenseTicket();

            // NB: 1st condition is to check if user rewound clock
            if ((DateTime.Now < ticket.LastUpdateClientTime ||
                 DateTime.Now - ticket.LastUpdateClientTime > maxTicketLifetime)) {
                // Don't quit when failing is allowed, but only if also the user didn't tamper with clock
                if (allowFail && DateTime.Now >= ticket.LastUpdateClientTime)
                    return false;

                errorReporter.ReportError(
                    string.Format(
                        "License ticket expired, {0} will now shutdown.\n\nPlease notify your system administrator.",
                        ApplicationHelper.ApplicationName), "License error");

                if (!ApplicationHelper.IsDebugBuild())
                    applicationExiter.Exit();
            }

            if (DateTime.Now - ticket.LastUpdateClientTime > oldTicketWarningAfter) {
                if (allowFail)
                    return false;

                errorReporter.ReportError(
                    string.Format(
                        @"Could not renew the license ticket, please finish your work as soon as possible and restart {0}.

The application will be closed in {1} if the ticket can't be renewed before then.",
                        ApplicationHelper.ApplicationName,
                        (maxTicketLifetime - (DateTime.Now - ticket.LastUpdateClientTime)).ToString(@"hh\:mm\:ss")),
                    "License error");
            }

            return true;
        }

        public void Dispose() {
            StopMonitoring();
        }
    }
}