﻿using System;
using System.Globalization;
using System.IO;
using System.Xml;

using Colorware.Core.License.Hardware;
using Colorware.Core.License.Models;
using Colorware.Core.License.Parsing;

using JetBrains.Annotations;

namespace Colorware.Core.License.Parsers {
    [UsedImplicitly]
    public class XmlLicenseParser : ILicenseParser {
        public Models.License Parse(string licenseFileContents) {
            if (licenseFileContents == null) throw new ArgumentNullException("licenseFileContents");

            using (var reader = new StringReader(licenseFileContents)) {
                using (var xml = XmlReader.Create(reader)) {
                    var license = new Models.License(LicenseState.Valid) {
                        RawContentsHash = licenseFileContents.GetHashCode()
                    };
                    parseLicense(xml, license);
                    return license;
                }
            }
        }

        private void parseLicense([NotNull] XmlReader xml, [NotNull] Models.License license) {
            while (true) {
                if (xml.IsStartElement("License")) {
                    parseLicenseHeader(xml, license);
                }
                if (xml.IsStartElement("Products")) {
                    parseProducts(xml, license);
                }
                if (xml.IsStartElement("Hardware")) {
                    parseHardware(xml, license);
                }
                else if (!next(xml))
                    return;
            }
        }

        private void parseLicenseHeader([NotNull] XmlReader xml, [NotNull] Models.License license) {
            license.Key = xml.GetAttribute("Key");
            var licenseIdStr = xml.GetAttribute("LicenseId");
            if (licenseIdStr != null) {
                long licenseId;
                if (long.TryParse(licenseIdStr, NumberStyles.Any, CultureInfo.InvariantCulture, out licenseId)) {
                    license.LicenseId = licenseId;
                }
            }
            var isDemoStr = xml.GetAttribute("IsDemo");
            license.IsDemo = isDemoStr != null && isDemoStr.ToLowerInvariant() != "false";
            var isTimeLimitedStr = xml.GetAttribute("IsTimeLimited");
            license.IsTimeLimited = isTimeLimitedStr != null && isTimeLimitedStr.ToLowerInvariant() != "false";
            if (license.IsTimeLimited) {
                var validUntilStr = xml.GetAttribute("ValidUntil");
                if (string.IsNullOrEmpty(validUntilStr)) {
                    throw new Exception("No end date set for demo");
                }
                license.ValidTill = DateTime.ParseExact(validUntilStr, "O", CultureInfo.InvariantCulture);
            }
            var supportDate = xml.GetAttribute("Support");
            if (supportDate == null) {
                license.SupportValidTill = DateTime.MaxValue; // Unlimited support.
            }
            else {
                license.SupportValidTill = DateTime.ParseExact(supportDate, "O", CultureInfo.InvariantCulture);
            }
        }

        private void parseHardware([NotNull] XmlReader xml, [NotNull] Models.License license) {
            while (true) {
                if (xml.IsStartElement("Item")) {
                    parseHardwareItem(xml, license);
                }
                else if (xml.Name == "Hardware" && xml.NodeType == XmlNodeType.EndElement)
                    return;
                else if (!next(xml))
                    return;
            }
        }

        private void parseHardwareItem([NotNull] XmlReader xml, [NotNull] Models.License license) {
            var id = xml.GetAttribute("Id");
            var value = xml.ReadElementString();
            license.AddHardware(new HardwareElement(id, value));
        }

        private void parseProducts([NotNull] XmlReader xml, [NotNull] Models.License license) {
            while (true) {
                if (xml.IsStartElement("Product")) {
                    var product = new Product();
                    parseProduct(xml, product);
                    license.AddProduct(product);
                }
                else if (xml.Name == "Products" && xml.NodeType == XmlNodeType.EndElement)
                    return;
                else if (!next(xml))
                    return;
            }
        }

        private void parseProduct([NotNull] XmlReader xml, [NotNull] Product product) {
            product.Name = xml.GetAttribute("Name");
            product.Description = xml.GetAttribute("Description");
            var guidString = xml.GetAttribute("Guid");
            if (string.IsNullOrEmpty(guidString))
                throw new Exception("Could not read Guid for product");
            product.Guid = Guid.Parse(guidString);
            var seatsStr = xml.GetAttribute("Seats");
            if (seatsStr != null)
                product.Seats = int.Parse(seatsStr);
            while (true) {
                if (xml.IsStartElement("Group")) {
                    var group = new FunctionGroup();
                    parseFunctionGroup(xml, group);
                    product.AddFunctionGroup(group);
                }
                if (xml.IsStartElement("Field")) {
                    var customField = new CustomField();
                    parseCustomField(xml, customField);
                    product.AddCustomField(customField);
                }
                else if (xml.Name == "Product" && xml.NodeType == XmlNodeType.EndElement)
                    return;
                else if (!next(xml))
                    return;
            }
        }

        private void parseFunctionGroup([NotNull] XmlReader xml, [NotNull] FunctionGroup group) {
            group.Name = xml.GetAttribute("Name");
            group.Description = xml.GetAttribute("Description");
            var guidString = xml.GetAttribute("Guid");
            if (string.IsNullOrEmpty(guidString))
                throw new Exception("Could not read Guid for group");
            group.Guid = Guid.Parse(guidString);

            while (true) {
                if (xml.IsStartElement("Function")) {
                    var function = new Function();
                    parseFunction(xml, function);
                    group.AddFunction(function);
                }
                else if (xml.Name == "Group" && xml.NodeType == XmlNodeType.EndElement)
                    return;
                else if (!next(xml))
                    return;
            }
        }

        private void parseCustomField([NotNull] XmlReader xml, [NotNull] CustomField customField) {
            var guidString = xml.GetAttribute("Guid");
            if (string.IsNullOrEmpty(guidString))
                throw new Exception("Could not read Guid for function");

            customField.Guid = Guid.Parse(guidString);
            customField.Name = xml.GetAttribute("Name");
            customField.Value = xml.GetAttribute("Value");
            next(xml);
        }

        private void parseFunction(XmlReader xml, Function function) {
            var guidString = xml.GetAttribute("Guid");
            if (string.IsNullOrEmpty(guidString))
                throw new Exception("Could not read Guid for funciton");

            function.Guid = Guid.Parse(guidString);
            function.Name = xml.GetAttribute("Name");
            function.Description = xml.GetAttribute("Description");
            next(xml);
        }

        private bool next([NotNull] XmlReader xml) {
            return xml.Read() && !xml.EOF;
        }
    }
}