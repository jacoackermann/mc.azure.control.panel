﻿using System;
using System.Globalization;
using System.Linq;

using Colorware.Core.License.Models;

using JetBrains.Annotations;

namespace Colorware.Core.License {
    [UsedImplicitly]
    public class LicenseRightsChecker {
        private readonly LicenseTicketStore licenseTicketStore;

        public LicenseRightsChecker([NotNull] LicenseTicketStore licenseTicketStore) {
            if (licenseTicketStore == null) throw new ArgumentNullException("licenseTicketStore");

            this.licenseTicketStore = licenseTicketStore;
        }

        public bool MayUse(Guid functionGuid) {
            var ticket = getTicket();
            return ticket != LicenseTicket.InvalidTicket && ticket.HasFunction(functionGuid);
        }

        public bool MayUse(string functionGuid) {
            return MayUse(new Guid(functionGuid));
        }


        public T GetCustomFieldValue<T>(Guid customFieldGuid) where T : IConvertible {
            var ticket = getTicket();

            if (ticket == null)
                throw new Exception("No license ticket available to get custom field value from!");

            var customField = ticket.Product.CustomFields.FirstOrDefault(c => c.Guid == customFieldGuid);

            if (customField == null)
                throw new LicenseFunctionNotFoundException(
                    string.Format("Custom field with GUID '{0}' not available in license ticket!",
                        customFieldGuid));

            return (T)Convert.ChangeType(customField.Value, typeof(T), CultureInfo.InvariantCulture);
        }

        public T GetCustomFieldValue<T>(string customFieldGuid) where T : IConvertible {
            return GetCustomFieldValue<T>(new Guid(customFieldGuid));
        }

        public T GetCustomFieldValue<T>(Guid customFieldGuid, T defaultValue) where T : IConvertible {
            try {
                return GetCustomFieldValue<T>(customFieldGuid);
            }
            catch (LicenseFunctionNotFoundException) {
                return defaultValue;
            }
        }

        public T GetCustomFieldValue<T>(string customFieldGuid, T defaultValue) where T : IConvertible {
            return GetCustomFieldValue(new Guid(customFieldGuid), defaultValue);
        }

        public bool LicenseExpired {
            get { return getTicket().LicenseExpired; }
        }

        public DateTime LicenseValidTill {
            get { return getTicket().LicenseValidTill; }
        }

        public bool LicenseIsDemo {
            get { return getTicket().LicenseIsDemo; }
        }

        public bool LicenseIsTimeLimited {
            get { return getTicket().LicenseIsTimeLimited; }
        }

        public string LicenseKey {
            get {
                return getTicket().LicenseKey;
            }
        }

        public string LicenseProductName {
            get {
                return getTicket().Product.Name;
            }
        }

        [NotNull]
        private LicenseTicket getTicket() {
            return licenseTicketStore.GetLicenseTicket();
        }
    }
}