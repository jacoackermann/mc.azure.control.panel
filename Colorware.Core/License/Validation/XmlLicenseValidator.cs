﻿using System;
using System.Xml;

using Colorware.Core.License.Models;
using Colorware.Cryptography.KeyGenerators;
using Colorware.Cryptography.Signing;

using JetBrains.Annotations;

namespace Colorware.Core.License.Validation {
    [UsedImplicitly]
    public class XmlLicenseValidator : ILicenseValidator {
        private readonly IXmlVerifier xmlVerifier;
        private readonly IPublicKeyProvider publicKeyProvider;

        public XmlLicenseValidator([NotNull] IXmlVerifier xmlVerifier, [NotNull] IPublicKeyProvider publicKeyProvider) {
            if (xmlVerifier == null) throw new ArgumentNullException("xmlVerifier");
            if (publicKeyProvider == null) throw new ArgumentNullException("publicKeyProvider");
            this.xmlVerifier = xmlVerifier;
            this.publicKeyProvider = publicKeyProvider;
        }

        public LicenseState Validate([NotNull] string licenseFileContents, [CanBeNull] out string reason) {
            if (licenseFileContents == null) throw new ArgumentNullException("licenseFileContents");

            var key = publicKeyProvider.ProvideKey();
            var doc = new XmlDocument();
            doc.LoadXml(licenseFileContents);
            var isValid = xmlVerifier.VerifyXml(key, doc);
            reason = isValid ? null : "License could not be verified.";
            return isValid ? LicenseState.Valid : LicenseState.Invalid;
        }
    }
}