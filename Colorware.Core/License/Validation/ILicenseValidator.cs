﻿using Colorware.Core.License.Models;

using JetBrains.Annotations;

namespace Colorware.Core.License.Validation {
    public interface ILicenseValidator {
        LicenseState Validate([NotNull] string licenseFileContents, [CanBeNull] out string reason);
    }
}