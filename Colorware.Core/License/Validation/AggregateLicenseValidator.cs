﻿using System;
using System.Collections.Generic;

using Colorware.Core.License.Models;

using JetBrains.Annotations;

namespace Colorware.Core.License.Validation {
    [UsedImplicitly]
    public class AggregateLicenseValidator : ILicenseValidator {
        private readonly IEnumerable<ILicenseValidator> innerValidators;

        public AggregateLicenseValidator([NotNull] IEnumerable<ILicenseValidator> innerValidators) {
            if (innerValidators == null) throw new ArgumentNullException("innerValidators");

            this.innerValidators = innerValidators;
        }

        public LicenseState Validate([NotNull] string licenseFileContents, [CanBeNull] out string reason) {
            if (licenseFileContents == null) throw new ArgumentNullException("licenseFileContents");

            foreach (var innerValidator in innerValidators) {
                var innerState = innerValidator.Validate(licenseFileContents, out reason);
                if(innerState != LicenseState.Valid)
                    return innerState;
            }

            reason = null;
            return LicenseState.Valid;
        }
    }
}