﻿using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.License.Hardware;
using Colorware.Core.License.Models;
using Colorware.Core.License.Validation.Parsers;

using JetBrains.Annotations;

namespace Colorware.Core.License.Validation {
    [UsedImplicitly]
    public class HardwareLicenseValidator : ILicenseValidator {
        /// <summary>
        /// Designates what percentage of categories should validate for the hardware to be considered 'valid'.
        /// </summary>
        private const double PercentageOfCategoriesThatShouldValidate = 0.70;

        private readonly IHardwareEnumerator hardwareEnumerator;
        private readonly IHardwareComponentParser hardwareComponentParser;

        public HardwareLicenseValidator([NotNull] IHardwareEnumerator hardwareEnumerator,
            [NotNull] IHardwareComponentParser hardwareComponentParser) {
            if (hardwareEnumerator == null) throw new ArgumentNullException("hardwareEnumerator");
            if (hardwareComponentParser == null) throw new ArgumentNullException("hardwareComponentParser");
            this.hardwareEnumerator = hardwareEnumerator;
            this.hardwareComponentParser = hardwareComponentParser;
        }

        public LicenseState Validate([NotNull] string licenseData, [CanBeNull] out string reason) {
            if (licenseData == null) throw new ArgumentNullException("licenseData");

            var components = hardwareEnumerator.GetElements();
            var required = hardwareComponentParser.Parse(licenseData);
            var isValid = check(required, components);
            reason = isValid ? null : "Invalid hardware configuration";
            return isValid ? LicenseState.Valid : LicenseState.HardwareMismatch;
        }

        private bool check(IEnumerable<HardwareElement> required, IEnumerable<HardwareElement> components) {
            var categories = buildCategories(required);
            var result = validateCategories(categories, components);
            return validateResult(result);
        }

        private bool validateResult(CategoryValidationResult result) {
            var numberOfCategoriesThatShouldBeValid =
                Math.Floor(PercentageOfCategoriesThatShouldValidate * result.TotalNumberOfCategories);

            return result.ValidCategories >= numberOfCategoriesThatShouldBeValid;
        }

        /// <summary>
        /// Produce a validation result considering all passed in categories.
        /// </summary>
        /// <param name="categories">The elements of hardware that are required, contained in separate categories.</param>
        /// <param name="components">The components that were found in the machine.</param>
        /// <returns>A result detailing how many categories were valid/invalid.</returns>
        private CategoryValidationResult validateCategories(IEnumerable<HardwareCategory> categories, IEnumerable<HardwareElement> components) {
            var validCategories = 0;
            var numberOfCategories = 0;
            foreach (var category in categories) {
                numberOfCategories ++;
                var valid = validateCategory(category, components);
                if (valid)
                    validCategories ++;
            }
            return new CategoryValidationResult {
                TotalNumberOfCategories = numberOfCategories,
                ValidCategories = validCategories
            };
        }

        /// <summary>
        /// Validate a given category. A category is currently valid if we can find at least one hardware
        /// element in the list of components and the number of found hardware elements exeeds the amount of missing
        /// hardware elements.
        /// </summary>
        /// <param name="category">The category with the components that are required to be present.</param>
        /// <param name="components">The components that were found on the machine.</param>
        /// <returns><c>true</c> if the category is deemed valid, <c>false</c> otherwise.</returns>
        private bool validateCategory(HardwareCategory category, IEnumerable<HardwareElement> components) {
            var numberOfMatches = 0;
            var numberOfMisses = 0;
            foreach (var element in category.Elements) {
                var found = components.FirstOrDefault(c => c.Value == element.Value);
                if (found == null) {
                    numberOfMisses ++;
                }
                else {
                    numberOfMatches ++;
                }
            }
            // Valid if we have at least one match and if the number of hits exeeds (or at least is the same as) the number of misses.
            return numberOfMatches > 0 && numberOfMatches >= numberOfMisses;
        }

        /// <summary>
        /// Build a list of categories that contain required hardware elements sorted by their ids.
        /// </summary>
        /// <param name="hardwareElements">List of required hardware elements.</param>
        /// <returns></returns>
        private List<HardwareCategory> buildCategories(IEnumerable<HardwareElement> hardwareElements) {
            var result = new Dictionary<string, HardwareCategory>();
            foreach (var element in hardwareElements) {
                HardwareCategory category;
                if (!result.TryGetValue(element.Id, out category)) {
                    category = new HardwareCategory(element.Id);
                    result.Add(element.Id, category);
                }
                category.Add(element);
            }
            return result.Values.ToList();
        }

        private class CategoryValidationResult {
            public int TotalNumberOfCategories { get; set; }
            public int ValidCategories { get; set; }
        }

        private class HardwareCategory {
            public string Id { get; set; }
            public List<HardwareElement> Elements { get; private set; }

            public HardwareCategory(string id) {
                Id = id;
                Elements = new List<HardwareElement>();
            }

            public void Add(HardwareElement element) {
                Elements.Add(element);
            }
        }
    }
}