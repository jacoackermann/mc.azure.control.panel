﻿using System.Collections.Generic;

using Colorware.Core.License.Hardware;

using JetBrains.Annotations;

namespace Colorware.Core.License.Validation.Parsers {
    /// <summary>
    /// Parses a hardware component section from string data.
    /// </summary>
    public interface IHardwareComponentParser {
        IEnumerable<HardwareElement> Parse([NotNull] string xmlToParse);
    }
}