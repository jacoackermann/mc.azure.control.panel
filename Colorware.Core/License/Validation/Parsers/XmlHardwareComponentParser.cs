﻿using System;
using System.Collections.Generic;
using System.Xml;

using Colorware.Core.License.Hardware;

using JetBrains.Annotations;

namespace Colorware.Core.License.Validation.Parsers {
    [UsedImplicitly]
    public class XmlHardwareComponentParser : IHardwareComponentParser {
        public IEnumerable<HardwareElement> Parse([NotNull] string xmlToParse) {
            if (xmlToParse == null) throw new ArgumentNullException("xmlToParse");

            var dom = new XmlDocument();
            dom.LoadXml(xmlToParse);
            var hardwareNodes = dom.GetElementsByTagName("Hardware");
            var items = new List<HardwareElement>();
            foreach (XmlElement hardwareNode in hardwareNodes) {
                var itemNodes = hardwareNode.GetElementsByTagName("Item");
                foreach (XmlElement itemNode in itemNodes) {
                    var id = itemNode.GetAttribute("Id");
                    var value = itemNode.InnerText;
                    items.Add(new HardwareElement(id, value));
                }
            }
            return items;
        }
    }
}