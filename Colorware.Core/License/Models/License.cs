﻿using System;
using System.Collections.Generic;

using Colorware.Core.License.Hardware;

using JetBrains.Annotations;

namespace Colorware.Core.License.Models {
    public sealed class License {
        /// <summary>
        /// The state of the license and whether it is valid.
        /// </summary>
        public LicenseState LicenseState { get; private set; }

        // Hash of the raw content this license was created from, used to determine
        // if two licenses differ content wise (can't use Key because it can be the same
        // for two different licenses, e.g. after changing a customer's license)
        public int RawContentsHash { get; set; }

        public string Key { get; set; }
        public long LicenseId { get; set; }
        public bool IsDemo { get; set; }
        public bool IsTimeLimited { get; set; }
        public DateTime ValidTill { get; set; }
        public DateTime SupportValidTill { get; set; }

        [NotNull, ItemNotNull]
        public List<HardwareElement> Hardware { get; private set; }

        [NotNull, ItemNotNull]
        public List<Product> Products { get; private set; }

        public License(LicenseState state) {
            LicenseState = state;
            Key = string.Empty;
            Hardware = new List<HardwareElement>();
            Products = new List<Product>();
        }

        public void AddHardware([NotNull] HardwareElement hardwareElement) {
            if (hardwareElement == null) throw new ArgumentNullException("hardwareElement");

            Hardware.Add(hardwareElement);
        }

        public void AddProduct([NotNull] Product product) {
            if (product == null) throw new ArgumentNullException("product");

            Products.Add(product);
        }

        public bool IsValid {
            get { return LicenseState == LicenseState.Valid && !IsExpired; }
        }

        public bool IsExpired {
            get { return IsTimeLimited && DateTime.Now > ValidTill; }
        }

        public bool HasUnlimitedSupport {
            get { return SupportValidTill >= DateTime.MaxValue; }
        }

        /// <summary>
        /// Invalidate a license to the passed in state. A license can only be moved to an invalid state and this method
        /// will produce an exception if the passed in state is <see cref="Models.LicenseState.Valid"/>
        /// </summary>
        /// <param name="state">The new state to set the license to.</param>
        /// <exception cref="InvalidOperationException">Thrown when an attempt is made to set the license to a valid state.</exception>
        public void Invalidate(LicenseState state) {
            if (state == LicenseState.Valid)
                throw new InvalidOperationException("Cannot make an invalid license valid!");

            LicenseState = state;
        }
    }
}