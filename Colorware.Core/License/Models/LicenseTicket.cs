﻿using System;
using System.Linq;

using JetBrains.Annotations;

using ProtoBuf;

namespace Colorware.Core.License.Models {
    [System.Reflection.ObfuscationAttribute(Feature = "all", Exclude = true)]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class LicenseTicket {
        public Guid SessionGuid { get; private set; }

        [NotNull]
        public string LicenseKey { get; private set; }

        [NotNull]
        public Product Product { get; private set; }

        public bool LicenseIsTimeLimited { get; private set; }

        public DateTime LicenseValidTill { get; private set; }

        public DateTime SupportValidTill { get; private set; }

        public bool LicenseIsDemo { get; private set; }

        /// <summary>
        /// When the ticket was last claimed/renewed at the server.
        /// </summary>
        public DateTime LastUpdateServerTime { get; set; }

        private DateTime lastUpdateClientTime;

        [ProtoIgnore]
        public bool LicenseExpired {
            get { return LicenseIsTimeLimited && DateTime.Now > LicenseValidTill; }
        }

        /// <summary>
        /// When the ticket was last received at the client after claiming/renewing it at the server.
        /// </summary>
        [ProtoIgnore]
        public DateTime LastUpdateClientTime {
            get { return lastUpdateClientTime; }
            set {
                if (lastUpdateClientTime != DateTime.MinValue)
                    throw new Exception("LastUpdateClientTime was already set! (can only be set once)");

                lastUpdateClientTime = value;
            }
        }

        public static LicenseTicket InvalidTicket {
            get {
                return new LicenseTicket(Guid.Empty, "", new Product {Name = "?", Description = ""}, true,
                    new DateTime(), new DateTime(), false, new DateTime());
            }
        }

        private LicenseTicket() {
        }

        public LicenseTicket(Guid sessionGuid, [NotNull] string licenseKey, [NotNull] Product product,
            bool licenseIsTimeLimited, DateTime licenseValidTill, DateTime supportValidTill, bool licenseIsDemo,
            DateTime lastUpdateServerTime) {
            if (licenseKey == null) throw new ArgumentNullException("licenseKey");
            if (product == null) throw new ArgumentNullException("product");

            SessionGuid = sessionGuid;
            LicenseKey = licenseKey;
            LicenseIsTimeLimited = licenseIsTimeLimited;
            Product = product;
            LicenseValidTill = licenseValidTill;
            SupportValidTill = supportValidTill;
            LicenseIsDemo = licenseIsDemo;
            LastUpdateServerTime = lastUpdateServerTime;
        }

        public bool HasFunction(Guid functionGuid) {
            return Product.Functions.Any(f => f.Guid == functionGuid);
        }

        public bool IsSupportExpired(DateTime? currentVersionDate) {
            if (!currentVersionDate.HasValue) {
                return true;
            }
            return SupportValidTill < currentVersionDate;
        }
    }
}