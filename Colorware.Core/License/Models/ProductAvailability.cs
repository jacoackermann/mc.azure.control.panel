﻿using System;
using System.ComponentModel;

using JetBrains.Annotations;

using ProtoBuf;

namespace Colorware.Core.License.Models {
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class ProductAvailability : INotifyPropertyChanged {
        public Guid ProductGuid { get; private set; }

        [NotNull]
        public string ProductName { get; private set; }

        public int SeatsAvailable { get; set; }

        public int TotalSeats { get; private set; }

        private ProductAvailability() {}

        public ProductAvailability(Guid productGuid, [NotNull] string productName, int seatsAvailable, int totalSeats) {
            if (productGuid == null) throw new ArgumentNullException("productGuid");
            if (productName == null) throw new ArgumentNullException("productName");

            ProductGuid = productGuid;
            ProductName = productName;
            SeatsAvailable = seatsAvailable;
            TotalSeats = totalSeats;
        }

        // Anti memory leak
#pragma warning disable 0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 0067
    }
}