﻿using System;

using ProtoBuf;

namespace Colorware.Core.License.Models {
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public sealed class Function {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}