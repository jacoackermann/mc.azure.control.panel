﻿namespace Colorware.Core.License.Models {
    public enum LicenseState {
        /// <summary>
        /// No license is present.
        /// </summary>
        Missing = 1,
        /// <summary>
        /// License is present and has been validated.
        /// </summary>
        Valid = 2,
        /// <summary>
        /// License is present, but the contents could not be validated or something else is amiss.
        /// </summary>
        Invalid = 3,
        /// <summary>
        /// License is present and could be validated, but the hardware for the license doesn't match that of the machine.
        /// </summary>
        HardwareMismatch = 4,
        /// <summary>
        /// Something (possibly unknown) went wrong.
        /// </summary>
        Error = 5
    }
}