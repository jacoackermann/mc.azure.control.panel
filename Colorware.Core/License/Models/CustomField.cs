﻿using System;

using ProtoBuf;

namespace Colorware.Core.License.Models {
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class CustomField {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}