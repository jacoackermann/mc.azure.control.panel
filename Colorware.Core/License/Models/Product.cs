﻿using System;
using System.Collections.Generic;
using System.Linq;

using JetBrains.Annotations;

using ProtoBuf;

namespace Colorware.Core.License.Models {
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public sealed class Product {
        public string Name { get; set; }
        public Guid Guid { get; set; }
        public string Description { get; set; }
        public int Seats { get; set; }

        
        [NotNull]
        [ProtoIgnore]
        public IEnumerable<Function> Functions {
            get { return FunctionGroups.SelectMany(fg => fg.Functions); }
        }

        [NotNull]
        public List<FunctionGroup> FunctionGroups { get; set; }
        
        [NotNull]
        public List<CustomField> CustomFields { get; set; }

        public Product() {
            FunctionGroups = new List<FunctionGroup>();
            CustomFields = new List<CustomField>();
        }

        public void AddFunctionGroup(FunctionGroup functionGroup) {
            FunctionGroups.Add(functionGroup);
        }

        public void AddCustomField(CustomField customField) {
            CustomFields.Add(customField);
        }
    }
}