﻿using System;

using Colorware.Core.Azure.Storage.TableStorage.ConfigKeys;
using Colorware.Core.Config;

using JetBrains.Annotations;

using ProtoBuf;

namespace Colorware.Core.License.Models {
    /// <summary>
    /// Used to keep a license ticket `lock' on disk. Represents a license session that is currently being used
    /// and should survive an application shutdown if the license ticket could not be released properly (i.e. if the
    /// application crashes).
    /// </summary>
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class LicenseTicketLock {
        public Guid SessionGuid { get; set; }

        public string LicenseKey { get; set; }

        public string LicenseServerHost { get; set; }

        [UsedImplicitly]
        public LicenseTicketLock() : this(null) {
        }

        public LicenseTicketLock([CanBeNull] LicenseTicket ticket) {
            if (ticket == null) {
                SessionGuid = Guid.Empty;
                LicenseKey = null;
            }
            else {
                SessionGuid = ticket.SessionGuid;
                LicenseKey = ticket.LicenseKey;
            }

            // Meh, should probably use URL locator or something.
            LicenseServerHost = AzureConfigKeys.ServerHost;
        }
    }
}