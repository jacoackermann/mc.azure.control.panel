﻿using System;
using System.Collections.Generic;

using JetBrains.Annotations;

using ProtoBuf;

namespace Colorware.Core.License.Models {
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class FunctionGroup {
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [NotNull]
        public List<Function> Functions { get; set; }

        public FunctionGroup() {
            Functions = new List<Function>();
        }

        public void AddFunction(Function function) {
            Functions.Add(function);
        }
    }
}