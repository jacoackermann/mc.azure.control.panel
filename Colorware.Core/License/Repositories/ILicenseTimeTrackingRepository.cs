﻿using System;

namespace Colorware.Core.License.Repositories {
    public interface ILicenseTimeTrackingRepository {
        DateTime GetLastStoredSystemTime();

        DateTime GetLastValidLicenseSystemTime();

        /// <returns>False iff the time couldn't be stored because the currently stored time is in the future compared
        /// to the time we tried to store. (usually means the user rewound the system clock)</returns>
        bool UpdateLastSystemTime(DateTime time);

        void UpdateLastValidLicenseTime(DateTime time);
    }
}