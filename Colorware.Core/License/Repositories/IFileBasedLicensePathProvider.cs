﻿using JetBrains.Annotations;

namespace Colorware.Core.License.Repositories {
    public interface IFileBasedLicensePathProvider {
        [NotNull]
        string GetDirectory();

        [NotNull]
        string GetPath();

        [NotNull]
        string GetPath(string directory);
    }
}