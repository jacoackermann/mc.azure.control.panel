﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using Castle.Core.Logging;

using Colorware.Core.Azure.Interfaces;
using Colorware.Core.License.Events;
using Colorware.Core.License.Models;
using Colorware.Core.License.Parsing;
using Colorware.Core.License.Validation;
using Colorware.Core.Subdomain.SubdomainProvider;

using JetBrains.Annotations;

namespace Colorware.Core.License.Repositories {
    /// <summary>
    /// Note: Possibly not thread safe. May give problems when used in server environment as-is.
    /// </summary>
    [UsedImplicitly]
    public class FileBasedLicenseRepository : ILicenseRepository {
        private readonly ILicenseValidator licenseValidator;
        private readonly ILicenseParser licenseParser;
        private readonly ILogger log;
        private readonly ILicenseFileStorageHelper licenseFileStorageHelper;

        public event LicenseUpdatedHandler LicenseUpdated;

        protected virtual void invokeLicenseUpdated() {
            LicenseUpdated?.Invoke(this, new LicenseUpdatedEventArgs());
        }

        // Key is License.RawContentsHash
        private readonly Dictionary<int, DateTime> firstIssueTimestamps = new Dictionary<int, DateTime>();

        public FileBasedLicenseRepository([NotNull] ILicenseValidator licenseValidator,
                                          [NotNull] ILicenseParser licenseParser, [NotNull] ILogger logger,
                                          [NotNull] ILicenseFileStorageHelper licenseFileStorageHelper) {
            if (licenseValidator == null) throw new ArgumentNullException(nameof(licenseValidator));
            if (licenseParser == null) throw new ArgumentNullException(nameof(licenseParser));
            if (logger == null) throw new ArgumentNullException(nameof(logger));

            this.licenseValidator = licenseValidator;
            this.licenseParser = licenseParser;
            log = logger;

            this.licenseFileStorageHelper = licenseFileStorageHelper ??
                                            throw new ArgumentNullException(nameof(licenseFileStorageHelper));
        }

        public async Task StoreAsync(string licenseFileContents) {
            if (licenseFileContents == null) throw new ArgumentNullException(nameof(licenseFileContents));

            var state = licenseValidator.Validate(licenseFileContents, out var reason);
            if (state != LicenseState.Valid) {
                log.Error("Tried to store invalid license: " + reason);
                throw new Exception("Invalid license: " + reason);
            }

            // TODO: Encrypt file?
            await licenseFileStorageHelper.SaveLicenseAsync(licenseFileContents);
            invokeLicenseUpdated();
        }

        public Models.License Retrieve() {
            return Retrieve(ServerSubdomainProvider.Subdomain);
        }

        public async Task<Models.License> RetrieveAsync() {
            return await RetrieveAsync(ServerSubdomainProvider.Subdomain);
        }

        public async Task<Models.License> RetrieveAsync(string directory) {
            string contents;
            try {
                contents = await RetrieveRawAsync(directory);
            }
            catch (FileNotFoundException) {
                return new Models.License(LicenseState.Missing);
            }

            return processLicense(contents);
        }

        public Models.License Retrieve(string directory) {
            string contents;
            try {
                contents = RetrieveRaw(directory);
            }
            catch (FileNotFoundException) {
                return new Models.License(LicenseState.Missing);
            }

            return processLicense(contents);
        }

        private Models.License processLicense(string contents) {
            string reason;
            LicenseState state;
            try {
                state = licenseValidator.Validate(contents, out reason);
            }
            catch (Exception ex) {
                log.Error("Error occured while validating license: " + ex.Message, ex);
                return new Models.License(LicenseState.Error);
            }

            if (state != LicenseState.Valid) {
                log.Error($"Invalid license file for customer {ServerSubdomainProvider.Subdomain} - reason:  {reason}");

                // Try to parse it anyway:
                try {
                    var license = licenseParser.Parse(contents);
                    license.Invalidate(state);
                    return license;
                }
                catch (Exception ex) {
                    log.Error("Could not parse invalid license: " + ex.Message, ex);
                    return new Models.License(state);
                }
            }

            try {
                var license = licenseParser.Parse(contents);

                if (!firstIssueTimestamps.ContainsKey(license.RawContentsHash))
                    firstIssueTimestamps[license.RawContentsHash] = DateTime.Now;

                return license;
            }
            catch (Exception ex) {
                log.Error("Could not parse license: " + ex.Message, ex);
                return new Models.License(LicenseState.Error);
            }
        }

        public async Task<string> RetrieveRawAsync() {
            return await RetrieveRawAsync(ServerSubdomainProvider.Subdomain);
        }

        public async Task<string> RetrieveRawAsync(string directory) {
            if (!await licenseFileStorageHelper.LicenseExistsAsync()) {
                log.Error("No license found for customer " + ServerSubdomainProvider.Subdomain);
                throw new FileNotFoundException("License not found");
            }

            return await licenseFileStorageHelper.ReadLicenseAsync(directory);
        }

        public string RetrieveRaw(string directory) {
            if (!licenseFileStorageHelper.LicenseExists()) {
                log.Error("No license found for customer " + ServerSubdomainProvider.Subdomain);
                throw new FileNotFoundException("License not found");
            }

            return licenseFileStorageHelper.ReadLicense(directory);
        }

        public DateTime GetFirstIssueTimestamp(Models.License license) {
            if (license == null) throw new ArgumentNullException(nameof(license));

            if (!firstIssueTimestamps.TryGetValue(license.RawContentsHash, out var timestamp))
                throw new ArgumentException("Supplied license was never issued by this repository", nameof(license));

            return timestamp;
        }

        public async Task ClearAsync() {
            if (!await licenseFileStorageHelper.LicenseExistsAsync()) {
                log.Error("No license found for customer " + ServerSubdomainProvider.Subdomain);
                throw new FileNotFoundException("License not found");
            }

            await licenseFileStorageHelper.DeleteLicenseAsync();
            invokeLicenseUpdated();
        }
    }
}