﻿using System;
using System.IO;

using Colorware.Core.Subdomain.SubdomainProvider;

using JetBrains.Annotations;

namespace Colorware.Core.License.Repositories.PathProviders {
    /// <summary>
    /// Returns path to file in datadirectory.
    /// </summary>
    [UsedImplicitly]
    public class DataDirectoryFileBasedLicensePathProvider : IFileBasedLicensePathProvider {
        private readonly string licenseFileName;

        public DataDirectoryFileBasedLicensePathProvider([NotNull] string licenseFileName) {
            this.licenseFileName = licenseFileName ?? throw new ArgumentNullException(nameof(licenseFileName));
        }

        public string GetDirectory() {
            var dataPath = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();
            var path = Path.Combine(dataPath, "licenses");
            return path;
        }

        public string GetPath() {
            var subdomain = ServerSubdomainProvider.Subdomain;
            return GetPath(subdomain);
        }

        public string GetPath(string directory) {
            var path = Path.Combine(GetDirectory(), directory);
            path = Path.Combine(path, licenseFileName);
            return path;
        }
    }
}