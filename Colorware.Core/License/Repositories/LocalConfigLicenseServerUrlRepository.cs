﻿using System;

using Colorware.Core.Config;

using JetBrains.Annotations;

namespace Colorware.Core.License.Repositories {
    [UsedImplicitly]
    public class LocalConfigLicenseServerUrlRepository : ILicenseServerUrlRepository {
        private readonly IConfig config;

        public LocalConfigLicenseServerUrlRepository([NotNull] IConfig config) {
            if (config == null) throw new ArgumentNullException("config");

            this.config = config;
        }

        public string GetUrl() {
            return config.Get(GlobalConfigKeys.LicenseHostConfigKey, GlobalConfigKeys.ServerHost) ?? "";
        }

        public void SetUrl([NotNull] string url) {
            if (url == null) throw new ArgumentNullException("url");

            config.Set(GlobalConfigKeys.LicenseHostConfigKey, url);
        }
    }
}