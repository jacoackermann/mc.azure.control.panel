﻿using System;
using System.IO;
using System.Threading.Tasks;

using Colorware.Core.License.Events;

using JetBrains.Annotations;

namespace Colorware.Core.License.Repositories {
    public delegate void LicenseUpdatedHandler(object sender, LicenseUpdatedEventArgs args);

    /// <summary>
    /// Note: will throw if anything goes wrong with both store and retrieve.
    /// </summary>
    public interface ILicenseRepository {
        event LicenseUpdatedHandler LicenseUpdated;

        /// <exception cref="Exception">There is no license to retrieve.</exception>
        Task StoreAsync([NotNull] string licenseFileContents);

        [NotNull]
        Models.License Retrieve();

        [NotNull]
        Models.License Retrieve(string directory);

        [NotNull]
        Task<Models.License> RetrieveAsync();

        [NotNull]
        Task<Models.License> RetrieveAsync(string directory);

        /// <exception cref="FileNotFoundException">There is no license to retrieve.</exception>
        [NotNull]
        Task<string> RetrieveRawAsync();

        [NotNull]
        Task<string> RetrieveRawAsync(string directory);

        [NotNull]
        string RetrieveRaw(string directory);

        /// <summary>
        /// Gets a timestamp that indicates when a given license was retrieved from the
        /// repository for the first time.
        /// </summary>
        /// <exception cref="ArgumentNullException">The supplied license was not issued by this repository.</exception>
        DateTime GetFirstIssueTimestamp([NotNull] Models.License license);

        /// <summary>
        /// Destroys the current license.
        /// </summary>
        /// <exception cref="FileNotFoundException">There is no license to clear.</exception>
        Task ClearAsync();
    }
}