﻿using System;
using System.Globalization;

using Colorware.Core.Azure.Interfaces;
using Colorware.Core.Extensions;
using Colorware.Cryptography.Decryption;
using Colorware.Cryptography.Encryption;
using Colorware.Cryptography.KeyGenerators;

using JetBrains.Annotations;

namespace Colorware.Core.License.Repositories {
    [UsedImplicitly]
    public class FileBasedLicenseTimeTrackingRepository : ILicenseTimeTrackingRepository {
        private class LicenseTimes {
            public DateTime LastValidLicenseSystemTime;
            public DateTime LastSystemTime;

            public LicenseTimes(DateTime lastValidLicenseSystemTime, DateTime lastSystemTime) {
                LastValidLicenseSystemTime = lastValidLicenseSystemTime;
                LastSystemTime = lastSystemTime;
            }

            public LicenseTimes(string serializedTimes) {
                var splitTimes = serializedTimes.Split("\n", StringSplitOptions.RemoveEmptyEntries);

                LastValidLicenseSystemTime = DateTime.Parse(splitTimes[0], CultureInfo.InvariantCulture);
                LastSystemTime = DateTime.Parse(splitTimes[1], CultureInfo.InvariantCulture);
            }

            public string Serialize() {
                return LastValidLicenseSystemTime.ToString(CultureInfo.InvariantCulture) + "\n" +
                       LastSystemTime.ToString(CultureInfo.InvariantCulture);
            }
        }

        //=====================================================================

        private readonly ISymmetricKeyGenerator symmetricKeyGenerator;
        private readonly ISymmetricEncyptor symmetricEncyptor;
        private readonly ISymmetricDecryptor symmetricDecryptor;
        private readonly ILicenseTimeTrackingRepositoryHelper licenseTimeTrackingRepositoryHelper;

        public FileBasedLicenseTimeTrackingRepository([NotNull] ISymmetricKeyGenerator symmetricKeyGenerator,
                                                      [NotNull] ISymmetricEncyptor symmetricEncyptor,
                                                      [NotNull] ISymmetricDecryptor symmetricDecryptor,
                                                      [NotNull]
                                                      ILicenseTimeTrackingRepositoryHelper
                                                          licenseTimeTrackingRepositoryHelper) {
            if (symmetricKeyGenerator == null) throw new ArgumentNullException("symmetricKeyGenerator");
            if (symmetricEncyptor == null) throw new ArgumentNullException("symmetricEncyptor");
            if (symmetricDecryptor == null) throw new ArgumentNullException("symmetricDecryptor");

            this.symmetricKeyGenerator = symmetricKeyGenerator;
            this.symmetricEncyptor = symmetricEncyptor;
            this.symmetricDecryptor = symmetricDecryptor;

            this.licenseTimeTrackingRepositoryHelper = licenseTimeTrackingRepositoryHelper ??
                                                       throw new ArgumentNullException(
                                                           nameof(licenseTimeTrackingRepositoryHelper));
        }

        [NotNull]
        private LicenseTimes readTimesFromFile() {
            try {
                var encryptedTimes = licenseTimeTrackingRepositoryHelper.Read();

                var key = symmetricKeyGenerator.Generate();
                var decryptedTimesString = symmetricDecryptor.Decrypt(key, Convert.FromBase64String(encryptedTimes));

                return new LicenseTimes(decryptedTimesString);
            }
            catch (Exception) {
                return new LicenseTimes(new DateTime(), DateTime.Now);
            }
        }

        private void writeTimesToFile(LicenseTimes licenseTimes) {
            var key = symmetricKeyGenerator.Generate();
            var encryptedTimes = symmetricEncyptor.Encrypt(key, licenseTimes.Serialize());

            licenseTimeTrackingRepositoryHelper.Write(Convert.ToBase64String(encryptedTimes));
        }

        public DateTime GetLastStoredSystemTime() {
            return readTimesFromFile().LastSystemTime;
        }

        public DateTime GetLastValidLicenseSystemTime() {
            return readTimesFromFile().LastValidLicenseSystemTime;
        }

        public bool UpdateLastSystemTime(DateTime time) {
            var storedTimes = readTimesFromFile();

            // User probably rewound clock
            if (time < storedTimes.LastSystemTime)
                return false;

            writeTimesToFile(new LicenseTimes(storedTimes.LastValidLicenseSystemTime, time));

            return true;
        }

        public void UpdateLastValidLicenseTime(DateTime time) {
            var storedTimes = readTimesFromFile();

            writeTimesToFile(new LicenseTimes(time, storedTimes.LastSystemTime));
        }
    }
}