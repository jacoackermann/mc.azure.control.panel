﻿using JetBrains.Annotations;

namespace Colorware.Core.License.Repositories {
    public interface ILicenseServerUrlRepository {
        [NotNull]
        string GetUrl();

        void SetUrl([NotNull] string url);
    }
}