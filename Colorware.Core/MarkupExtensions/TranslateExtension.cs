using System;
using System.Windows.Markup;

using Colorware.Core.Globalization;

namespace Colorware.Core.MarkupExtensions {
    [MarkupExtensionReturnType(typeof(String))]
    [ContentProperty("Key")]
    public class TranslateExtension : MarkupExtension {
        [ConstructorArgument("key")]
        public String Key { get; set; }

        public String Default { get; set; }

        private readonly String value;

        public TranslateExtension() {
            Key = "";
            Default = "N/A";
        }

        public TranslateExtension(String key) {
            Key = key;
            Default = key;
            value = LanguageManager._(Key);
        }

        #region Overrides of MarkupExtension
        public override object ProvideValue(IServiceProvider serviceProvider) {
            // return String.Format("Key: {0}, Default: {1}, Time: {2}", Key, Default, DateTime.Now.ToString());
            return value ?? Default;
        }
        #endregion
    }
}