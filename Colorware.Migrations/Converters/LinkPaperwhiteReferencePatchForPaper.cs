﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

using Colorware.Core.Enums;
using Colorware.Core.Extensions;

namespace Colorware.Migrations.Converters {
    public class LinkPaperwhiteReferencePatchForPaper {
        private class RawPaperwhitePatch {
            public long Id { get; set; }
            public string Name { get; set; }
        }

        private class RawPaper {
            public long Id { get; set; }

            public IList<RawPaperwhitePatch> PotentialPaperwhites { get; private set; }

            public RawPaper() {
                PotentialPaperwhites = new List<RawPaperwhitePatch>();
            }
        }

        public void ConvertAll(IDbConnection connection, IDbTransaction transaction) {
            var papers = getPapers(connection, transaction);
            updatePapers(papers, connection, transaction);
        }

        private IEnumerable<RawPaper> getPapers(IDbConnection connection, IDbTransaction transaction) {
            var paperQuery = string.Format(@"
SELECT papers.id as paper_id, reference_patches.id as reference_patch_id, reference_patches.name as reference_patch_name
FROM papers, paper_types, reference_patches
WHERE paper_types.id = papers.paper_type_id
AND paper_types.color_reference_id = reference_patches.reference_id
AND reference_patches.patch_type = {0}
", (int)PatchTypes.Paperwhite);

            var items = new Dictionary<long, RawPaper>();
            using (var cmd = connection.CreateCommand()) {
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = transaction;
                cmd.CommandText = paperQuery;

                using (var reader = cmd.ExecuteReader()) {
                    var iId = reader.GetOrdinal("paper_id");
                    var iReferencePatchId = reader.GetOrdinal("reference_patch_id");
                    var iReferencePatchName = reader.GetOrdinal("reference_patch_name");
                    while (reader.Read()) {
                        var id = reader.GetInt64(iId);
                        if (!items.ContainsKey(id)) {
                            items[id] = new RawPaper {Id = id};
                        }
                        var item = items[id];
                        item.PotentialPaperwhites.Add(new RawPaperwhitePatch {
                            Id = reader.GetInt64(iReferencePatchId),
                            Name = reader.GetString(iReferencePatchName)
                        });
                    }
                }
            }
            return items.Values;
        }

        private void updatePapers(IEnumerable<RawPaper> papers, IDbConnection connection, IDbTransaction transaction) {
            papers.ForEach(paper => updatePaper(paper, connection, transaction));
        }

        private void updatePaper(RawPaper paper, IDbConnection connection, IDbTransaction transaction) {
            using (var cmd = connection.CreateCommand()) {
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = transaction;
                cmd.CommandText = @"UPDATE papers SET paperwhite_reference_patch_id = @paperwhite_reference_patch_id WHERE id = @paper_id";

                // Prefer patch with name PW.
                var paperPatch = paper.PotentialPaperwhites
                                      .FirstOrDefault(p => p.Name.ToLowerInvariant() == "pw") ??
                                 // If not found, take first.
                                 paper.PotentialPaperwhites.FirstOrDefault();
                // If still not found bail.
                if(paperPatch == null)
                    return;

                var pRefPatchId = cmd.CreateParameter();
                pRefPatchId.ParameterName = "paperwhite_reference_patch_id";
                pRefPatchId.Value = paperPatch.Id;
                cmd.Parameters.Add(pRefPatchId);

                var pPaperId = cmd.CreateParameter();
                pPaperId.ParameterName = "paper_id";
                pPaperId.Value = paper.Id;
                cmd.Parameters.Add(pPaperId);

                cmd.ExecuteNonQuery();
            }
        }
    }
}