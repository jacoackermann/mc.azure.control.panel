using System.Collections.Generic;
using System.Data;

namespace Colorware.Migrations.Converters {
    public class UpdatedReferenceIndicationConvertor {
        private class Reference {
            public long Id { get; set; }
            public bool IsSpot { get; set; }
        }

        public void ConvertAll(IDbConnection connection, IDbTransaction transaction) {
            var references = getReferences(connection, transaction);
            updateReferences(references, connection, transaction);
        }

        private IEnumerable<Reference> getReferences(IDbConnection connection, IDbTransaction transaction) {
            var cmd = connection.CreateCommand();
            cmd.CommandText = "SELECT id, is_spot_lib FROM color_references";
            cmd.Transaction = transaction;
            var result = new List<Reference>();
            using (var reader = cmd.ExecuteReader()) {
                while (reader.Read()) {
                    var id = reader.GetInt64(0);
                    var isSpot = reader.GetBoolean(1);
                    result.Add(new Reference {Id = id, IsSpot = isSpot});
                }
            }
            return result;
        }

        private void updateReferences(IEnumerable<Reference> references, IDbConnection connection, IDbTransaction transaction) {
            foreach (var reference in references) {
                updateReference(reference, connection, transaction);
            }
        }

        private void updateReference(Reference reference, IDbConnection connection, IDbTransaction transaction) {
            var cmd = connection.CreateCommand();
            cmd.CommandText = "UPDATE reference_patches SET is_spot = @is_spot WHERE reference_id = @reference_id";
            cmd.Transaction = transaction;
            var pIsSpot = cmd.CreateParameter();
            pIsSpot.ParameterName = "is_spot";
            pIsSpot.Value = reference.IsSpot;
            cmd.Parameters.Add(pIsSpot);
            var pReferenceId = cmd.CreateParameter();
            pReferenceId.ParameterName = "reference_id";
            pReferenceId.Value = reference.Id;
            cmd.Parameters.Add(pReferenceId);

            cmd.ExecuteNonQuery();
        }
    }
}