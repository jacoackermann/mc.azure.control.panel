﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;

using Colorware.Core.Data.Models;
using Colorware.Core.Data.Models.ColorStripMapper;
using Colorware.Core.Data.Specification;
using Colorware.Core.Enums;
using Colorware.Core.Extensions;
using Colorware.Core.Functional.Option;
using Colorware.Models;
using Colorware.Models.ColorSequence;
using Colorware.Models.ServiceModels;
using Colorware.Models.ServiceModels.Serialization;

using ReactiveUI;

using INotifyPropertyChanging = ReactiveUI.INotifyPropertyChanging;
using PropertyChangingEventArgs = ReactiveUI.PropertyChangingEventArgs;
using PropertyChangingEventHandler = ReactiveUI.PropertyChangingEventHandler;

namespace Colorware.Migrations.Converters {
    /// <summary>
    /// Converts old-style-job-setup-preset data to new-style-style-job-setup-preset data.
    /// This basically combines the information from color_mapping and color_sequence into the new ink_definitions_serialized
    /// column.
    /// </summary>
    public class JobPresetToNewJobSetupConverter {
        [DebuggerDisplay("RawJobPreset(Id={Id}, PaperTypeId={PaperTypeId}, InkDefinition={InkDefinitions})")]
        private class RawJobPreset {
            public long Id { get; set; }
            public long PaperTypeId { get; set; }
            public long ColorStripId { get; set; }
            public ColorStripMappingConfig MappingConfig { get; set; }
            public PressColorSequence ColorSequence { get; set; }
            public List<ColorDensitiesConfig.DensityConfigItem> DensityConfigItems { get; set; }
            public string JobColors { get; set; }
            public string ImageName { get; set; }
            public string PantoneLivePaletteGuid { get; set; }

            /// <summary>
            /// A map from slot to default dotgain list id.
            /// </summary>
            public Dictionary<int, long> DefaultDotgainListIds { get; set; }

            /// <summary>
            /// Will be generated from above data.
            /// </summary>
            public List<JobPresetInkDefinition> InkDefinitions { get; set; }
        }

        [DebuggerDisplay("RawReferencePatch(Id={Id}, Name={Name})")]
        private class RawReferencePatch {
            public long Id { get; set; }
            public string Name { get; set; }
            public double? DefaultDensity { get; set; }
        }

        private class RawColorStripPatch : IColorStripPatch {
            #region Unused
            /// <summary>
            /// Properties in this region are not used by any code in this class but are required for the IColorStripPatch interface.
            /// </summary>
            event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged {
                add { throw new NotImplementedException(); }
                remove { throw new NotImplementedException(); }
            }

            public void RaisePropertyChanging(PropertyChangingEventArgs args) {
                throw new NotImplementedException();
            }

            public void RaisePropertyChanged(PropertyChangedEventArgs args) {
                throw new NotImplementedException();
            }

            event PropertyChangingEventHandler IReactiveObject.PropertyChanging {
                add { throw new NotImplementedException(); }
                remove { throw new NotImplementedException(); }
            }

            event PropertyChangedEventHandler IReactiveObject.PropertyChanged {
                add { throw new NotImplementedException(); }
                remove { throw new NotImplementedException(); }
            }

            event PropertyChangingEventHandler INotifyPropertyChanging.PropertyChanging {
                add { throw new NotImplementedException(); }
                remove { throw new NotImplementedException(); }
            }

            public long Id { get; set; }
            public bool NewInstance { get; set; }
            public bool IsDeleted { get; set; } = false;

            public T Clone<T>() where T : IBaseModel {
                throw new NotImplementedException();
            }

            public T CloneNew<T>() where T : IBaseModel {
                throw new NotImplementedException();
            }

            public bool ServiceFieldPropertiesAllEqual(IBaseModel other) {
                throw new NotImplementedException();
            }

            public bool ServiceFieldPropertiesAllEqual(IBaseModel other, Func<object, object, bool> compareFunc) {
                throw new NotImplementedException();
            }

            public bool ServiceFieldPropertiesAllEqualWithSqlDatePrecision(IBaseModel other) {
                throw new NotImplementedException();
            }

            public string Name { get; set; }
            public int Percentage { get; set; }
            public int InkZone { get; set; }
            public ToleranceGroups ToleranceGroup { get; set; }
            public PatchTypes PatchType { get; set; }
            public int SortOrder { get; set; }
            public long ColorStripId { get; set; }
            public bool HasSlot1 { get; private set; }
            public bool HasSlot2 { get; private set; }
            public bool HasSlot3 { get; private set; }
            public bool IsUndefined { get; private set; }
            #endregion Unused

            public int Slot1 { get; set; }
            public int Slot2 { get; set; }
            public int Slot3 { get; set; }

            public RawColorStripPatch(int slot1, int slot2, int slot3) {
                Slot1 = slot1;
                Slot2 = slot2;
                Slot3 = slot3;
            }
        }

        public void ConvertAll(IDbConnection connection, IDbTransaction transaction) {
            var presets = getPresets(connection, transaction);
            updateDefinitions(presets, connection, transaction);
            savePresets(presets, connection, transaction);
        }

        private void savePresets(IEnumerable<RawJobPreset> presets, IDbConnection connection, IDbTransaction transaction) {
            presets.ForEach(p => savePreset(p, connection, transaction));
        }

        private void savePreset(RawJobPreset preset, IDbConnection connection, IDbTransaction transaction) {
            if (preset.InkDefinitions == null)
                return;
            var newDefinition = JobPresetInkDefinitionSerializer.Serialize(preset.InkDefinitions);
            var cmd = connection.CreateCommand();
            cmd.Transaction = transaction;
            cmd.CommandText = "UPDATE job_presets SET ink_definitions_serialized = @new_definition WHERE id = @id";
            var rdParam = cmd.CreateParameter();
            rdParam.ParameterName = "new_definition";
            rdParam.Value = newDefinition;
            cmd.Parameters.Add(rdParam);
            var idParam = cmd.CreateParameter();
            idParam.ParameterName = "id";
            idParam.Value = preset.Id;
            cmd.Parameters.Add(idParam);
            cmd.ExecuteNonQuery();
        }

        private void updateDefinitions(IEnumerable<RawJobPreset> presets, IDbConnection connection,
                                       IDbTransaction transaction) {
            presets.ForEach(p => updateDefinition(p, connection, transaction));
        }

        private void updateDefinition(RawJobPreset preset, IDbConnection connection, IDbTransaction transaction) {
            preset.InkDefinitions = new List<JobPresetInkDefinition>();
            var references = getReferences(preset.PaperTypeId, connection, transaction);
            var colorStripPatches = getColorStripPatches(preset.ColorStripId, connection, transaction);
            var slots = ColorStrip.CalculateSlots(colorStripPatches);
            for (var i = 0; i < preset.MappingConfig.LoadedConfig.Count; i++) {
                var mapping = preset.MappingConfig.LoadedConfig[i];
                long referenceId = 0;
                string pantoneLiveObjectGuid = null;
                double solidDensity = 0;
                var name = "?";
                var isSpot = false;
                var defaultDotgainListId = Option<long>.Nothing;

                int slot;
                if (i >= 0 && i < slots.Length) {
                    slot = slots[i];
                }
                else {
                    slot = i; // Should not happen probably.
                }
                var densityItem = Option<ColorDensitiesConfig.DensityConfigItem>.Nothing;
                if (preset.DensityConfigItems.Any(dci => dci.Slot == slot))
                    densityItem = preset.DensityConfigItems.FirstOrDefault(dci => dci.Slot == slot).ToOption();
                if (slot < 0) {
                    continue;
                }

                // Setup default dotgain list id.
                if (preset.DefaultDotgainListIds.ContainsKey(slot)) {
                    var dotgainListIdFromPreset = preset.DefaultDotgainListIds[slot];
                    if(dotgainListIdFromPreset > 0)
                        defaultDotgainListId = dotgainListIdFromPreset.ToOption();

                }

                switch (mapping.TypeOfPatch) {
                    case ColorStripMappingConfig.PatchType.Reference:
                        var reference1 = references.FirstOrDefault(r => r.Id == mapping.ReferenceId);
                        if (reference1 == null) {
                            var index = mapping.Index;
                            if (index < 0 || index >= references.Count())
                                continue;
                            reference1 = references[index];
                        }
                        referenceId = reference1.Id;
                        name = reference1.Name;
                        solidDensity = densityItem.Select(di => di.Value)
                                                  .Or(() => reference1.DefaultDensity.ToOption())
                                                  .OrElse(0);
                        isSpot = false;
                        break;
                    case ColorStripMappingConfig.PatchType.Client:
                        // Pantone live patch.
                        if (!string.IsNullOrEmpty(mapping.PantoneLiveOid)) {
                            // Image doesn't have the conventional mappings
                            if (!string.IsNullOrEmpty(preset.ImageName)) {
                                densityItem = preset.DensityConfigItems.FirstOrDefault(
                                    di => di.Slot == -mapping.PantoneLiveOid.GetHashCode()).ToOption();
                            }

                            pantoneLiveObjectGuid = mapping.PantoneLiveOid;

                            try {
                                name =
                                    densityItem.SelectOrElse(
                                        di =>
                                        new string(
                                            preset.JobColors.Split("); ")[preset.DensityConfigItems.IndexOf(di)]
                                                .Reverse()
                                                .SkipWhile(c => c != '(')
                                                .Skip(2)
                                                .Reverse()
                                                .ToArray()),
                                        () => pantoneLiveObjectGuid);
                            }
                            catch {
                                name = pantoneLiveObjectGuid;
                            }

                            solidDensity = densityItem.SelectOrElse(di => di.Value, () => 0);
                            isSpot = true;
                        }
                        else {
                            // Regular patch.
                            var maybeRef = getReferenceById(mapping.ReferenceId, connection, transaction);
                            if (!maybeRef.HasValue) {
                                continue;
                            }
                            maybeRef.Do(reference => {
                                referenceId = reference.Id;
                                name = reference.Name;
                                solidDensity = densityItem.Select(di => di.Value)
                                                          .Or(() => reference.DefaultDensity.ToOption())
                                                          .OrElse(0);
                                isSpot = true;
                            });
                        }

                        break;
                    case ColorStripMappingConfig.PatchType.Undefined:
                        continue;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                // Subtract from count as we need to reverse the order.
                /*
                var sequenceIndex = preset.ColorSequence.Elements.Count
                                    - preset.ColorSequence.FindIndexForSlot(i).OrElseDefault()
                                    - 1; // - 1 as we use 0 based indexing.
                                    */
                var sequenceIndex = findSequenceForSlot(slot, preset.ColorSequence);
                var inkDefinition = new JobPresetInkDefinition(slot,
                                                               sequenceIndex,
                                                               referenceId,
                                                               pantoneLiveObjectGuid,
                                                               null,
                                                               preset.PantoneLivePaletteGuid,
                                                               defaultDotgainListId,
                                                               // This should lead to the old behavior of selecting the curve based on the setting in the base reference.
                                                               solidDensity,
                                                               Option<double>.Nothing,
                                                               name,
                                                               string.Empty,
                                                               isSpot);
                preset.InkDefinitions.Add(inkDefinition);
            }
        }

        private IEnumerable<RawColorStripPatch> getColorStripPatches(long colorStripId, IDbConnection connection,
                                                                     IDbTransaction transaction) {
            var cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.Transaction = transaction;
            cmd.CommandText =
                "SELECT slot1, slot2, slot3 FROM color_strip_patches WHERE color_strip_id = @color_strip_id";

            var pColorStripId = cmd.CreateParameter();
            pColorStripId.ParameterName = "color_strip_id";
            pColorStripId.Value = colorStripId;
            cmd.Parameters.Add(pColorStripId);
            using (var reader = cmd.ExecuteReader()) {
                var items = new List<RawColorStripPatch>();
                while (reader.Read()) {
                    var slot1 = reader.GetInt32(0);
                    var slot2 = reader.GetInt32(1);
                    var slot3 = reader.GetInt32(2);
                    items.Add(new RawColorStripPatch(slot1, slot2, slot3));
                }
                return items;
            }
        }

        private static int findSequenceForSlot(int slotIndex, PressColorSequence colorSequence) {
            var index = colorSequence.Elements.IndexOf(el => el.Slot == slotIndex);
            if (index >= 0)
                return colorSequence.Elements.Count() - index - 1; // Starts from other side in new situation.
            return index;
        }

        /*
        private static int[] getSlotsFromInkSequence(PressColorSequence colorSequence) {
            return colorSequence.Elements.Select(el => el.Slot)
                                .Where(slot => slot >= 0)
                                .ToArray();
        }
        */

        private Option<RawReferencePatch> getReferenceById(long referenceId, IDbConnection connection,
                                                           IDbTransaction transaction) {
            var cmd = connection.CreateCommand();
            cmd.Transaction = transaction;
            cmd.CommandText = "SELECT name FROM reference_patches WHERE id = @id";
            var param = cmd.CreateParameter();
            param.ParameterName = "id";
            param.Value = referenceId;
            cmd.Parameters.Add(param);
            using (var reader = cmd.ExecuteReader()) {
                if (reader.Read()) {
                    return new RawReferencePatch {Id = referenceId, Name = reader.GetString(0)}.ToOption();
                }
                return Option<RawReferencePatch>.Nothing;
            }
        }

        private IList<RawReferencePatch> getReferences(long paperTypeId, IDbConnection connection,
                                                       IDbTransaction transaction) {
            var refId = getColorReferenceIdForPaperTypeId(paperTypeId, connection, transaction);
            return refId.Select(referenceId => {
                var items = new List<RawReferencePatch>();
                var cmd = connection.CreateCommand();
                cmd.Transaction = transaction;
                var patchTypeSolid = (int)PatchTypes.Solid;
                cmd.CommandText =
                    "SELECT id, name, default_density FROM reference_patches WHERE reference_id = @reference_id AND patch_type = " +
                    patchTypeSolid;
                var param = cmd.CreateParameter();
                param.ParameterName = "reference_id";
                param.Value = referenceId;
                cmd.Parameters.Add(param);
                using (var reader = cmd.ExecuteReader()) {
                    while (reader.Read()) {
                        var id = reader.GetInt64(0);
                        var name = reader.GetString(1);
                        double? defaultDensity = null;
                        if (!reader.IsDBNull(2))
                            defaultDensity = reader.GetFloat(2);
                        items.Add(new RawReferencePatch {
                            Id = id,
                            Name = name,
                            DefaultDensity = defaultDensity
                        });
                    }
                }
                return items;
            }).OrElse(() => new List<RawReferencePatch>());
        }

        private Option<long> getColorReferenceIdForPaperTypeId(long paperTypeId, IDbConnection connection,
                                                               IDbTransaction transaction) {
            var cmd = connection.CreateCommand();
            cmd.Transaction = transaction;
            cmd.CommandText = "SELECT color_reference_id FROM paper_types WHERE id = @id";
            var param = cmd.CreateParameter();
            param.ParameterName = "id";
            param.Value = paperTypeId;
            cmd.Parameters.Add(param);
            using (var reader = cmd.ExecuteReader()) {
                if (reader.Read() && !reader.IsDBNull(0)) {
                    return reader.GetInt64(0).ToOption();
                }
                return Option<long>.Nothing;
            }
        }

        /// <summary>
        /// Get current presets from database and generate a list of <see cref="RawJobPreset"/> items from the entries.
        /// </summary>
        private IList<RawJobPreset> getPresets(IDbConnection connection, IDbTransaction transaction) {
            var cmd = connection.CreateCommand();
            cmd.Transaction = transaction;
            cmd.CommandText =
                "SELECT id, color_strip_id, paper_type_id, color_mapping, color_sequence, default_densities, job_colors, image_name, pantone_live_client_reference_guid, default_dotgain_lists from job_presets";
            var items = new List<RawJobPreset>();
            using (var reader = cmd.ExecuteReader()) {
                while (reader.Read()) {
                    var id = reader.GetInt64(0);
                    var colorStripId = reader.GetInt64(1);
                    var paperTypeId = reader.GetInt64(2);
                    var mappingStr = reader.GetString(3);
                    var sequenceStr = reader.GetString(4);
                    var defaultDensities = reader.GetString(5);
                    var jobColors = reader.GetString(6);
                    var imageName = reader.IsDBNull(7) ? null : reader.GetString(7);
                    var pantoneLiveClientReferenceGuid = reader.IsDBNull(8) ? null : reader.GetGuid(8).ToString();

                    Dictionary<int, long> defaultDotgainListIds;
                    try {
                        var defaultDotgainListIdsStr = reader.GetString(9);
                        defaultDotgainListIds =
                            DefaultDotgainListSelectionHelper.DeserializeDotgainListSelections(defaultDotgainListIdsStr);
                    }
                    catch (Exception) {
                        defaultDotgainListIds = new Dictionary<int, long>();
                    }

                    var mappingConfig = new ColorStripMappingConfig(colorStripId);
                    // colorStripId probably not needed here.
                    mappingConfig.Load(mappingStr, false);

                    var sequence = new PressColorSequence();
                    sequence.DeserializeFromString(sequenceStr);

                    var item = new RawJobPreset {
                        Id = id,
                        PaperTypeId = paperTypeId,
                        ColorStripId = colorStripId,
                        MappingConfig = mappingConfig,
                        ColorSequence = sequence,
                        DensityConfigItems = ColorDensitiesConfig.UnserializeDensities(defaultDensities),
                        JobColors = jobColors,
                        ImageName = imageName,
                        PantoneLivePaletteGuid = pantoneLiveClientReferenceGuid,
                        DefaultDotgainListIds = defaultDotgainListIds
                    };

                    items.Add(item);
                }
            }
            return items;
        }
    }
}