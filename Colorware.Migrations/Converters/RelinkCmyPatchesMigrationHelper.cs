﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;

using Colorware.Core.Enums;

namespace Colorware.Migrations.Converters {
    /// <summary>
    /// Currently used algorithm:
    /// * Get all overprint patches.
    /// * Filter out any patches that already have parents set.
    /// * For the remaining patches:
    /// *   Get each potential parent id for cyan, magenta and yellow, based on the ink separation of
    ///     the patch and the reference id of the patch.
    /// *   Update the patch with the new parent ids.
    /// </summary>
    public class RelinkCmyPatchesMigrationHelper {
        public void RelinkAll(IDbConnection connection, IDbTransaction transaction) {
            var patchesToRelink = getOverprintPatches(connection, transaction);
            relinkPatches(patchesToRelink, connection, transaction);
        }

        /// <summary>
        /// Get all overprint patches for processing and convert them into <see
        /// cref="OverprintPatch"/> instances.
        /// </summary>
        private IEnumerable<OverprintPatch> getOverprintPatches(IDbConnection connection, IDbTransaction transaction) {
            using (var cmd = connection.CreateCommand()) {
                cmd.Transaction = transaction;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText =
                    @"select id, reference_id, cyan, magenta, yellow, [key], parent1_id, parent2_id, parent3_id, name from reference_patches cmy
where patch_type = " + (int)PatchTypes.Overprint;

                var items = new List<OverprintPatch>();
                using (var reader = cmd.ExecuteReader()) {
                    while (reader.Read()) {
                        var id = reader.GetInt64(0);
                        var referenceId = reader.GetInt64(1);
                        var cyan = reader.GetFloat(2);
                        var magenta = reader.GetFloat(3);
                        var yellow = reader.GetFloat(4);
                        var parent1Id = reader.IsDBNull(6) ? (long?)null : reader.GetInt64(6);
                        var parent2Id = reader.IsDBNull(7) ? (long?)null : reader.GetInt64(7);
                        var parent3Id = reader.IsDBNull(8) ? (long?)null : reader.GetInt64(8);
                        var name = reader.GetString(9);
                        items.Add(
                            new OverprintPatch(id,
                                               referenceId,
                                               cyan, magenta, yellow,
                                               parent1Id, parent2Id, parent3Id,
                                               name));
                    }
                }
                return items;
            }
        }

        private void relinkPatches(IEnumerable<OverprintPatch> patches,
                                   IDbConnection connection,
                                   IDbTransaction transaction) {
            var patchesToRelink = patches.Where(x => x.HasToBeRelinked());
            foreach (var patch in patchesToRelink) {
                relinkPatch(patch, connection, transaction);
            }
        }

        /// <summary>
        /// If a patch has to be relinked ( <see cref="OverprintPatch.HasToBeRelinked"/>), we try to
        /// get new parents from the database and update the patch data with those new parent ids.
        /// </summary>
        private void relinkPatch(OverprintPatch patch, IDbConnection connection, IDbTransaction transaction) {
            var cyanParentId = patch.Cyan > 0
                                   ? getParent(patch, 100, 0, 0, 0, connection, transaction)
                                   : null;
            var magentaParentId = patch.Magenta > 0
                                      ? getParent(patch, 0, 100, 0, 0, connection, transaction)
                                      : null;
            var yellowParentId = patch.Yellow > 0
                                     ? getParent(patch, 0, 0, 100, 0, connection, transaction)
                                     : null;
            using (var cmd = connection.CreateCommand()) {
                cmd.Transaction = transaction;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText =
                    @"UPDATE reference_patches SET parent1_id=@parent1_id, parent2_id=@parent2_id, parent3_id=@parent3_id where id=@id";
                addParam(cmd, "id", patch.Id);
                addParam(cmd, "parent1_id", cyanParentId);
                addParam(cmd, "parent2_id", magentaParentId);
                addParam(cmd, "parent3_id", yellowParentId);
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Try to get a new parent id from the database, based on the given input data.
        /// </summary>
        /// <param name="patch">The patch to get a parent for.</param>
        /// <param name="cyan">Only search for patches with this amount of cyan separation.</param>
        /// <param name="magenta">Only search for patches with this amount of magenta separation.</param>
        /// <param name="yellow">Only search for patches with this amount of yellow separation.</param>
        /// <param name="key">Only search for patches with this amount of key/black separation.</param>
        /// <param name="connection">Connection to use.</param>
        /// <param name="transaction">Transaction to use.</param>
        /// <returns></returns>
        private long? getParent(OverprintPatch patch, int cyan, int magenta, int yellow, int key, IDbConnection connection,
                                IDbTransaction transaction) {
            using (var cmd = connection.CreateCommand()) {
                cmd.Transaction = transaction;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText =
                    "SELECT id from reference_patches where reference_id=@reference_id and cyan=@cyan and magenta=@magenta and yellow=@yellow and [key]=@key and patch_type=" +
                    (int)PatchTypes.Solid;
                    
                addParam(cmd, "reference_id", patch.ReferenceId);
                addParam(cmd, "cyan", cyan);
                addParam(cmd, "magenta", magenta);
                addParam(cmd, "yellow", yellow);
                addParam(cmd, "key", key);
                using (var reader = cmd.ExecuteReader()) {
                    // Return first item.
                    if (!reader.Read() || reader.IsDBNull(0)) {
                        return null;
                    }
                    var parentId = reader.GetInt64(0);
                    return parentId;
                }
            }
        }

        /// <summary>
        /// Helper method to add a parameter to a <see cref="IDbCommand"/>.
        /// </summary>
        private void addParam(IDbCommand cmd, string name, object value) {
            var p = cmd.CreateParameter();
            p.ParameterName = name;
            p.Value = value ?? DBNull.Value;
            cmd.Parameters.Add(p);
        }

        [DebuggerDisplay("{" + nameof(debuggerDisplay) + ",nq}")]
        private class OverprintPatch {
            public OverprintPatch(long id, long referenceId, double cyan, double magenta, double yellow,
                                  long? parent1Id, long? parent2Id, long? parent3Id, string name) {
                Id = id;
                ReferenceId = referenceId;
                Cyan = cyan;
                Magenta = magenta;
                Yellow = yellow;
                Parent1Id = parent1Id;
                Parent2Id = parent2Id;
                Parent3Id = parent3Id;
                Name = name;
            }

            private string debuggerDisplay =>
                $"OverprintPatch({Id} (from {ReferenceId}), {Name}, C={Cyan}, M={Magenta}, Y={Yellow})";

            public double Cyan { get; }
            public long Id { get; }
            public double Magenta { get; }
            public long? Parent1Id { get; }
            public long? Parent2Id { get; }
            public long? Parent3Id { get; }
            public string Name { get; }
            public long ReferenceId { get; }
            public double Yellow { get; }

            public bool HasToBeRelinked() {
                // Heuristic: Check if the number of linked parents is equal or larger than the
                // number of required parents.
                //
                // The first part is checked by checking how many parent links are non-null.
                //
                // The second part is checked by checking the ink separation components of each ink.
                var numParents = 0;
                if (Parent1Id.HasValue && Parent1Id.Value > 0)
                    numParents++;
                if (Parent2Id.HasValue && Parent2Id.Value > 0)
                    numParents++;
                if (Parent3Id.HasValue && Parent3Id.Value > 0)
                    numParents++;
                var requiresParents = 0;
                if (Cyan > 0)
                    requiresParents++;
                if (Magenta > 0)
                    requiresParents++;
                if (Yellow > 0)
                    requiresParents++;
                return numParents < requiresParents;
            }
        }
    }
}