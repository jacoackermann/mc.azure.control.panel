﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;

using Colorware.Core.Data.Models.ColorStripMapper;
using Colorware.Core.Enums;
using Colorware.Core.Extensions;
using Colorware.Core.Functional.Option;
using Colorware.Models.ColorSequence;
using Colorware.Models.ServiceModels;
using Colorware.Models.ServiceModels.Serialization;

using JetBrains.Annotations;

namespace Colorware.Migrations.Converters {
    /// <summary>
    /// Converts old-style-image-job-setup-preset data to new-style-image-job-setup-preset data.
    /// This basically combines the information from color_mapping and color_sequence into the new
    /// ink_definitions_serialized column.
    /// </summary>
    public class ImageJobPresetToNewJobSetupConverter {
        [DebuggerDisplay("RawJobPreset(Id={Id}, ReferenceId={ReferenceId}, InkDefinition={InkDefinitions})")]
        private class RawJobPreset {
            public long Id { get; set; }
            public long ReferenceId { get; set; }
            public ColorStripMappingConfig MappingConfig { get; set; }
            public PressColorSequence ColorSequence { get; set; }
            public List<ColorDensitiesConfig.DensityConfigItem> DensityConfigItems { get; set; }
            public string JobColors { get; set; }
            public string PantoneLivePaletteGuid { get; set; }
            public int NumberOfPanels { get; set; }
            public List<RawJobPresetTargetPosition> TargetPositions { get; set; }

            /// <summary>
            /// Will be generated from above data.
            /// </summary>
            public List<JobPresetInkDefinition> InkDefinitions { get; set; }
        }

        [DebuggerDisplay(
            "RawJobPreset(Id={Id}, InkDefinitionsSequenceIndex={InkDefinitionsSequenceIndex}, IsSubstrate={IsSubstrate})"
            )]
        protected class RawJobPresetTargetPosition {
            public long Id { get; set; }
            public int InkDefinitionsSequenceIndex { get; set; }
            public bool IsSubstrate { get; set; }
        }

        [DebuggerDisplay("RawReferencePatch(Id={Id}, Name={Name})")]
        private class RawReferencePatch {
            public long Id { get; set; }
            public string Name { get; set; }
            public double? DefaultDensity { get; set; }
            public PatchTypes PatchType { get; set; }
        }

        public void ConvertAll(IDbConnection connection, IDbTransaction transaction) {
            var presets = getPresets(connection, transaction);
            updateDefinitions(presets, connection, transaction);
            savePresets(presets, connection, transaction);
        }

        private void savePresets(IEnumerable<RawJobPreset> presets, IDbConnection connection, IDbTransaction transaction) {
            presets.ForEach(p => savePreset(p, connection, transaction));
        }

        private void savePreset(RawJobPreset preset, IDbConnection connection, IDbTransaction transaction) {
            if (preset.InkDefinitions == null)
                return;

            var newDefinition = JobPresetInkDefinitionSerializer.Serialize(preset.InkDefinitions);
            var cmd = connection.CreateCommand();
            cmd.Transaction = transaction;
            cmd.CommandText =
                "UPDATE job_presets SET ink_definitions_serialized = @new_definition, job_type = @job_type WHERE id = @id";

            var rdParam = cmd.CreateParameter();
            rdParam.ParameterName = "new_definition";
            rdParam.Value = newDefinition;
            cmd.Parameters.Add(rdParam);

            var jtParam = cmd.CreateParameter();
            jtParam.ParameterName = "job_type";
            jtParam.Value = JobTypes.Image;
            cmd.Parameters.Add(jtParam);

            var idParam = cmd.CreateParameter();
            idParam.ParameterName = "id";
            idParam.Value = preset.Id;
            cmd.Parameters.Add(idParam);

            cmd.ExecuteNonQuery();

            preset.TargetPositions.ForEach(tp => saveTargetPosition(tp, connection, transaction));
        }

        private void saveTargetPosition(RawJobPresetTargetPosition targetPosition, IDbConnection connection,
                                        IDbTransaction transaction) {
            var cmd = connection.CreateCommand();
            cmd.Transaction = transaction;
            cmd.CommandText =
                "UPDATE job_preset_target_positions SET ink_definitions_sequence_index = @sequence_index, is_substrate = @is_substrate WHERE id = @id";

            var siParam = cmd.CreateParameter();
            siParam.ParameterName = "sequence_index";
            siParam.Value = targetPosition.InkDefinitionsSequenceIndex;
            cmd.Parameters.Add(siParam);

            var suParam = cmd.CreateParameter();
            suParam.ParameterName = "is_substrate";
            suParam.Value = targetPosition.IsSubstrate;
            cmd.Parameters.Add(suParam);

            var idParam = cmd.CreateParameter();
            idParam.ParameterName = "id";
            idParam.Value = targetPosition.Id;
            cmd.Parameters.Add(idParam);

            cmd.ExecuteNonQuery();
        }

        private void updateDefinitions(IEnumerable<RawJobPreset> presets, IDbConnection connection,
                                       IDbTransaction transaction) {
            presets.ForEach(p => updateDefinition(p, connection, transaction));
        }

        private void updateDefinition(RawJobPreset preset, IDbConnection connection, IDbTransaction transaction) {
            preset.InkDefinitions = new List<JobPresetInkDefinition>();

            var refPatches = getReferencePatches(preset.ReferenceId, connection, transaction);
            var foundSequenceIndexes = new List<int>();

            for (var i = 0; i < preset.MappingConfig.LoadedConfig.Count; i++) {
                var mapping = preset.MappingConfig.LoadedConfig[i];
                var slot = 0;
                long refPatchId = 0;
                var patchType = PatchTypes.Undefined;
                Guid pantoneLiveObjectGuid = Guid.Empty;
                double solidDensity = 0;
                var name = "?";
                var isSpot = false;
                var defaultDotgainListId = Option<long>.Nothing;

                switch (mapping.TypeOfPatch) {
                    case ColorStripMappingConfig.PatchType.Reference:
                        var refPatch = refPatches.FirstOrDefault(r => r.Id == mapping.ReferenceId);
                        if (refPatch == null) {
                            var index = mapping.Index;
                            if (index < 0 || index >= refPatches.Count)
                                continue;
                            refPatch = refPatches[index];
                        }
                        refPatchId = refPatch.Id;
                        name = refPatch.Name;
                        patchType = refPatch.PatchType;

                        solidDensity = preset.DensityConfigItems.FirstOrDefault(dci => dci.Slot == refPatchId).ToOption()
                                             .Select(di => di.Value)
                                             .Or(() => refPatch.DefaultDensity.ToOption())
                                             .OrElse(0);

                        break;
                    case ColorStripMappingConfig.PatchType.Client:
                        if (!string.IsNullOrEmpty(mapping.PantoneLiveOid) &&
                            Guid.TryParse(mapping.PantoneLiveOid, out pantoneLiveObjectGuid)) {
                            // Pantone live patch.
                            var densityItem =
                                preset.DensityConfigItems.FirstOrDefault(
                                    di => di.Slot == -Math.Abs(pantoneLiveObjectGuid.ToOption().GetHashCode())).ToOption
                                    ();

                            try {
                                name =
                                    densityItem.SelectOrElse(
                                        di =>
                                        new string(
                                            preset.JobColors.Split("); ")[preset.DensityConfigItems.IndexOf(di)]
                                                .Reverse()
                                                .SkipWhile(c => c != '(')
                                                .Skip(2)
                                                .Reverse()
                                                .ToArray()),
                                        () => mapping.PantoneLiveOid);
                            }
                            catch {
                                name = mapping.PantoneLiveOid;
                            }

                            solidDensity = densityItem.SelectOrElse(di => di.Value, () => 0);
                            isSpot = true;
                        }
                        else {
                            // Regular patch.
                            var maybeRef = getReferenceById(mapping.ReferenceId, connection, transaction);
                            if (!maybeRef.HasValue) {
                                continue;
                            }
                            maybeRef.Do(reference => {
                                refPatchId = reference.Id;
                                name = reference.Name;
                                patchType = reference.PatchType;

                                solidDensity = preset.DensityConfigItems.FirstOrDefault(dci => dci.Slot == refPatchId)
                                                     .ToOption()
                                                     .Select(di => di.Value)
                                                     .Or(() => reference.DefaultDensity.ToOption())
                                                     .OrElse(0);
                                isSpot = true;
                            });
                        }

                        break;
                    case ColorStripMappingConfig.PatchType.Undefined:
                        continue;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                var sequenceIndex = findSequenceForReferencePatchId(refPatchId, preset.ColorSequence,
                                                                    foundSequenceIndexes);
                updateTargetPositions(i, sequenceIndex, sequenceIndex == -1 && patchType == PatchTypes.Paperwhite,
                                      preset.NumberOfPanels,
                                      preset.TargetPositions);

                if (sequenceIndex == -1)
                    continue;

                var inkDefinition = new JobPresetInkDefinition(slot,
                                                               sequenceIndex,
                                                               refPatchId,
                                                               pantoneLiveObjectGuid == Guid.Empty
                                                                   ? null
                                                                   : pantoneLiveObjectGuid.ToString(),
                                                               null,
                                                               preset.PantoneLivePaletteGuid,
                                                               defaultDotgainListId,
                                                               solidDensity,
                                                               Option<double>.Nothing,
                                                               name,
                                                               string.Empty,
                                                               isSpot);
                preset.InkDefinitions.Add(inkDefinition);
            }
        }

        protected int findSequenceForReferencePatchId(long refPatchId, [NotNull] PressColorSequence colorSequence,
                                                      [NotNull] List<int> foundSequenceIndexes) {
            if (colorSequence == null) throw new ArgumentNullException(nameof(colorSequence));
            if (foundSequenceIndexes == null) throw new ArgumentNullException(nameof(foundSequenceIndexes));

            // The color sequence can contain id's from reference patches.
            // We want to handle the found patches in reverse order from how they are listed in the color sequence.
            // This method will return a newly defined index for the found patch.
            //
            // A patch can be present multiple times in a color sequence.
            // If that is the case, then the index of the found patch in de color sequence will be checked
            // using the foundSequenceIndexes list.
            // If the found index is already present in the list, the search for the patch in the color sequence will continue.
            for (var i = colorSequence.Elements.Count - 1; i > 0; i--) {
                if (colorSequence.Elements[i].Slot == refPatchId && !foundSequenceIndexes.Contains(i)) {
                    foundSequenceIndexes.Add(i);
                    return colorSequence.Elements.Count - i - 1;
                }
            }
            return -1;
        }

        /// <summary>
        /// Update newly defined properties of a target position on all existing panels.
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="sequenceIndex"></param>
        /// <param name="isSubstrate"></param>
        /// <param name="numberOfPanels"></param>
        /// <param name="targetPositions"></param>
        protected void updateTargetPositions(int offset, int sequenceIndex, bool isSubstrate, int numberOfPanels,
                                             [NotNull] List<RawJobPresetTargetPosition> targetPositions) {
            if (targetPositions == null) throw new ArgumentNullException(nameof(targetPositions));

            if (!targetPositions.Any() || offset < 0 || numberOfPanels <= 0)
                return;

            var index = offset;
            var nextPanelIncrement = targetPositions.Count / numberOfPanels;

            for (var i = 0; i < numberOfPanels; i++) {
                targetPositions[index].InkDefinitionsSequenceIndex = sequenceIndex;
                targetPositions[index].IsSubstrate = isSubstrate;

                index += nextPanelIncrement;
            }
        }

        private Option<RawReferencePatch> getReferenceById(long referenceId, IDbConnection connection,
                                                           IDbTransaction transaction) {
            var cmd = connection.CreateCommand();
            cmd.Transaction = transaction;
            cmd.CommandText = "SELECT name FROM reference_patches WHERE id = @id";
            var param = cmd.CreateParameter();
            param.ParameterName = "id";
            param.Value = referenceId;
            cmd.Parameters.Add(param);
            using (var reader = cmd.ExecuteReader()) {
                if (reader.Read()) {
                    return new RawReferencePatch {Id = referenceId, Name = reader.GetString(0)}.ToOption();
                }
                return Option<RawReferencePatch>.Nothing;
            }
        }

        private IList<RawReferencePatch> getReferencePatches(long referenceId, IDbConnection connection,
                                                             IDbTransaction transaction) {
            var items = new List<RawReferencePatch>();
            var cmd = connection.CreateCommand();
            cmd.Transaction = transaction;
            cmd.CommandText =
                "SELECT id, name, default_density, patch_type FROM reference_patches WHERE reference_id = @reference_id";
            var param = cmd.CreateParameter();
            param.ParameterName = "reference_id";
            param.Value = referenceId;
            cmd.Parameters.Add(param);
            using (var reader = cmd.ExecuteReader()) {
                while (reader.Read()) {
                    var id = reader.GetInt64(0);
                    var name = reader.GetString(1);
                    double? defaultDensity = null;
                    if (!reader.IsDBNull(2))
                        defaultDensity = reader.GetFloat(2);
                    var patchType = (PatchTypes)reader.GetInt32(3);

                    items.Add(new RawReferencePatch {
                        Id = id,
                        Name = name,
                        DefaultDensity = defaultDensity,
                        PatchType = patchType
                    });
                }
            }
            return items;
        }

        /// <summary>
        /// Get current presets from database and generate a list of <see cref="RawJobPreset"/> items from the entries.
        /// </summary>
        private IList<RawJobPreset> getPresets(IDbConnection connection, IDbTransaction transaction) {
            var cmd = connection.CreateCommand();
            cmd.Transaction = transaction;
            cmd.CommandText =
                "SELECT id, reference_id, color_mapping, color_sequence, default_densities, job_colors, image_name, pantone_live_client_reference_guid, number_of_panels FROM job_presets WHERE image_name <> @empty_guid";
            var param = cmd.CreateParameter();
            param.ParameterName = "empty_guid";
            param.Value = Guid.Empty;
            cmd.Parameters.Add(param);
            var items = new List<RawJobPreset>();
            using (var reader = cmd.ExecuteReader()) {
                while (reader.Read()) {
                    var id = reader.GetInt64(0);
                    var referenceId = reader.GetInt64(1);
                    var mappingStr = reader.GetString(2);
                    var sequenceStr = reader.GetString(3);
                    var defaultDensities = reader.GetString(4);
                    var jobColors = reader.GetString(5);
                    var pantoneLiveClientReferenceGuid = reader.IsDBNull(7) ? null : reader.GetGuid(7).ToString();
                    var numberOfPanels = reader.GetInt32(8);

                    var mappingConfig = new ColorStripMappingConfig(0);
                    mappingConfig.Load(mappingStr, false);

                    var sequence = new PressColorSequence();
                    sequence.DeserializeFromString(sequenceStr);

                    var item = new RawJobPreset {
                        Id = id,
                        ReferenceId = referenceId,
                        MappingConfig = mappingConfig,
                        ColorSequence = sequence,
                        DensityConfigItems = ColorDensitiesConfig.UnserializeDensities(defaultDensities),
                        JobColors = jobColors,
                        PantoneLivePaletteGuid =
                            pantoneLiveClientReferenceGuid == Guid.Empty.ToString()
                                ? null
                                : pantoneLiveClientReferenceGuid,
                        NumberOfPanels = numberOfPanels
                    };

                    items.Add(item);
                }
            }

            items.ForEach(item => item.TargetPositions = getPresetTargetPositions(item.Id, connection, transaction));

            return items;
        }

        /// <summary>
        /// Get target positions for the specified preset from the database and generate a list of <see cref="RawJobPresetTargetPosition"/> items from the entries.
        /// </summary>
        private List<RawJobPresetTargetPosition> getPresetTargetPositions(long presetId, IDbConnection connection,
                                                                          IDbTransaction transaction) {
            var cmd = connection.CreateCommand();
            cmd.Transaction = transaction;
            cmd.CommandText =
                "SELECT id, ink_definitions_sequence_index, is_substrate FROM job_preset_target_positions WHERE job_preset_id = @preset_id";
            var param = cmd.CreateParameter();
            param.ParameterName = "preset_id";
            param.Value = presetId;
            cmd.Parameters.Add(param);

            var items = new List<RawJobPresetTargetPosition>();

            using (var reader = cmd.ExecuteReader()) {
                while (reader.Read()) {
                    var id = reader.GetInt64(0);
                    var inkDefinitionsSequenceIndex = reader.GetInt32(1);
                    var isSubstrate = reader.GetBoolean(2);

                    var item = new RawJobPresetTargetPosition {
                        Id = id,
                        InkDefinitionsSequenceIndex = inkDefinitionsSequenceIndex,
                        IsSubstrate = isSubstrate
                    };

                    items.Add(item);
                }
            }
            return items;
        }
    }
}