﻿using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

using Colorware.Core.Extensions;

namespace Colorware.Migrations.Converters {
    /// <summary>
    /// Relink the paper types (printing conditions) to the references.
    /// This is needed as the paper types have lost their reference to a reference and the reference now has a reference
    /// to a paper type.
    /// </summary>
    public class RelinkPaperTypeReferencesConverter {
        [DebuggerDisplay("PaperTypeReferenceLink(PaperTypeId={PaperTypeId}, ReferenceId={ReferenceId})")]
        private class PaperTypeReferenceLink {
            public long PaperTypeId { get; set; }
            public long ReferenceId { get; set; }
        }
        public void ConvertAll(IDbConnection connection, IDbTransaction transaction) {
            var links = getLinks(connection, transaction);
            updateLinks(links, connection, transaction);
        }

        private IEnumerable<PaperTypeReferenceLink> getLinks(IDbConnection connection, IDbTransaction transaction) {
            using (var cmd = connection.CreateCommand()) {
                cmd.Transaction = transaction;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT id, color_reference_id FROM paper_types";
                using (var reader = cmd.ExecuteReader()) {
                    var items = new List<PaperTypeReferenceLink>();
                    while (reader.Read()) {
                        if (reader.IsDBNull(0) || reader.IsDBNull(1))
                            continue;
                        var paperTypeId = reader.GetInt64(0);
                        var referenceId = reader.GetInt64(1);

                        items.Add(new PaperTypeReferenceLink {
                            ReferenceId = referenceId,
                            PaperTypeId = paperTypeId
                        });
                    }
                    return items;
                }
            }
        }

        private void updateLinks(IEnumerable<PaperTypeReferenceLink> links, IDbConnection connection, IDbTransaction transaction) {
            links.ForEach(link => udpdateLink(link, connection, transaction));
        }

        private void udpdateLink(PaperTypeReferenceLink link, IDbConnection connection, IDbTransaction transaction) {
            using (var cmd = connection.CreateCommand()) {
                cmd.Transaction = transaction;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "UPDATE color_references SET paper_type_id = @paper_type_id WHERE id = @reference_id";

                var pPaperTypeId = cmd.CreateParameter();
                pPaperTypeId.ParameterName = "paper_type_id";
                pPaperTypeId.Value = link.PaperTypeId;
                cmd.Parameters.Add(pPaperTypeId);

                var pReferenceId = cmd.CreateParameter();
                pReferenceId.ParameterName = "reference_id";
                pReferenceId.Value = link.ReferenceId;
                cmd.Parameters.Add(pReferenceId);

                cmd.ExecuteNonQuery();
            }
        }
    }
}