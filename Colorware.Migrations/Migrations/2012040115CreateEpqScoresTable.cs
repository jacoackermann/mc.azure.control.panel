using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040115)]
    public class CreateEpqScoresTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("epq_scores")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("job_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("measurement_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("production").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("received").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("recorded").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("visual_score").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("visual_defects").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("registrations").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("opacity_type").AsInt32().Nullable().WithDefaultValue("0")
                  .WithColumn("opacity_low").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("opacity_high").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("opacity_average").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("has_visual_score").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("has_registrations").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("last_export_date").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("was_exported").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("printer_name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("printer_location").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("brand").AsString(255).Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("epq_scores");
        }
        #endregion
    }
}