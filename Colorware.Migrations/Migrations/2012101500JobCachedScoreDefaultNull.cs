using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201210151200)]
    public class JobCachedScoreDefaultNull : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Execute.EmbeddedScript("JobCachedScoreDefaultNull.sql");
        }

        public override void Down() {
        }
        #endregion
    }
}