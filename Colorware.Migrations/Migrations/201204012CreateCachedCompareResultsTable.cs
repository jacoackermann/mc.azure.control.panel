using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201204012)]
    public class CreateCachedCompareResultsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("cached_compare_results")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("measurement_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("job_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("machine_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("paper_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("ink_set_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("ok_sheet_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("slot1").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("slot2").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("slot3").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("percentage").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("patch_type").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("delta_e").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("delta_h").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("dry_delta_e").AsFloat().Nullable().WithDefaultValue("")
                //.WithColumn("dotgains").AsBinary().Nullable()
                  .WithColumn("dotgains").AsString(Int32.MaxValue).Nullable()
                  .WithColumn("l").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("a").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("b").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("delta_density").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("user_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                //.WithColumn("score").AsBinary().Nullable()
                  .WithColumn("score").AsString(Int32.MaxValue).Nullable()
                  .WithColumn("printed_density").AsFloat().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("cached_compare_results");
        }
        #endregion
    }
}