﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201704181521)]
    public class AddCustomTranslationsTable : AutoReversingMigration {
        public override void Up() {
            Create.Table("custom_translations")
                  .WithColumn("Id")
                  .AsInt64()
                  .Identity()
                  .PrimaryKey()
                  .WithColumn("translation_key")
                  .AsString(1024)
                  .NotNullable()
                  .WithColumn("translation_value")
                  .AsString(int.MaxValue)
                  .NotNullable();
        }
    }
}