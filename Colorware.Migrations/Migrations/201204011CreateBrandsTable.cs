using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201204011)]
    public class CreateBrandsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("brands")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("notes").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("brands");
        }
        #endregion
    }
}