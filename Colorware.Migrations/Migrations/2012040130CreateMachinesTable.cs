using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040130)]
    public class CreateMachinesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("machines")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("company_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("machine_type_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("default_tolerance_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("number_of_colors").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("number_of_inkzones").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("zone_width").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("zone_width_units").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("user_group_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                ;
        }

        public override void Down() {
            Delete.Table("machines");
        }
        #endregion
    }
}