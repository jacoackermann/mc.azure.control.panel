﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201402271314)]
    public class AddMeasurementCustomFieldsConfigurationItemsTable : Migration {
        public override void Up() {
            Create.Table("measurement_custom_fields_configuration_items")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("is_enabled").AsBoolean().WithDefaultValue(false)
                  .WithColumn("is_multiline").AsBoolean().WithDefaultValue(false)
                  .WithColumn("name").AsString(255)
                  .WithColumn("type_of_field").AsInt32().WithDefaultValue(0)
                  .WithColumn("field_index").AsInt32().WithDefaultValue(0)
                  .WithColumn("sort_order").AsInt32().WithDefaultValue(0)
                  ;
        }

        public override void Down() {
            Delete.Table("measurement_custom_fields_configuration_items");
        }
    }
}