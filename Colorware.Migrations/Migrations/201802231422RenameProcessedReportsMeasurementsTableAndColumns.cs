﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201802231422)]
    public class RenameProcessedReportsMeasurementsTableAndColumns : AutoReversingMigration {
        public override void Up() {
            Rename.Column("measurement_id")
                  .OnTable("processed_reports_measurements")
                  .To("model_id");
            
            Create.Column("aggregate")
                  .OnTable("processed_reports_measurements")
                  .AsString()
                  .SetExistingRowsTo("Measurement");

            Rename.Table("processed_reports_measurements")
                  .To("processed_reports_models");
        }
    }
}