using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040137)]
    public class CreatePapersTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("papers")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("active").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("shared").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("company_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("paper_type_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("paper_manufacturer_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("weight").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("papers");
        }
        #endregion
    }
}