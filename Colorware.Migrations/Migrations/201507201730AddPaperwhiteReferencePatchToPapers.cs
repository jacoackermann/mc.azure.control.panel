
using System;

using Colorware.Migrations.Converters;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201507201730)]
    public class AddPaperwhiteReferencePatchToPapers : Migration {
        public override void Up() {
            Create.Column("paperwhite_reference_patch_id")
                  .OnTable("papers")
                  .AsInt64()
                  .Nullable()
                  .WithDefaultValue(null);

            Execute.WithConnection((connection, transaction) =>
                                   new LinkPaperwhiteReferencePatchForPaper().ConvertAll(connection, transaction));
        }

        public override void Down() {
            Delete.Column("paperwhite_reference_patch_id")
                  .FromTable("papers");
        }
    }
}