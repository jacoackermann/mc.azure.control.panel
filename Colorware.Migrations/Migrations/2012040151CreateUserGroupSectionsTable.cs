using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040151)]
    public class CreateUserGroupSectionsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("user_group_sections")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("symbol").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("description").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("user_group_sections");
        }
        #endregion
    }
}