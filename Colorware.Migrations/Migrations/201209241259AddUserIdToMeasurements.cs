﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201209241259)]
    public class AddUserIdToMeasurements : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Column("user_id").OnTable("measurements").AsInt64().WithDefaultValue("");
        }

        public override void Down() {
            Delete.DefaultConstraint().OnTable("measurements").OnColumn("user_id");
            Delete.Column("user_id").FromTable("measurements");
        }
        #endregion
    }
}