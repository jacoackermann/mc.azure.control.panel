﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {

    [Migration(201612051542)]
    public class CreateOpacityResultsTable : Migration {

        public override void Up() {
            Create.Table("opacity_results")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("measurement_id").AsInt64().NotNullable()
                  .WithColumn("created_at").AsDateTime().NotNullable().WithDefaultValue(SystemMethods.CurrentDateTime)
                  .WithColumn("opacity").AsDouble().NotNullable().WithDefaultValue(0.0)
                ;
        }

        public override void Down() {
            Delete.Table("opacity_results");
        }
    }
}