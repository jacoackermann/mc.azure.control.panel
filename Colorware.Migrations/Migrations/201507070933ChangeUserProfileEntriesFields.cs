
using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201507070933)]
    public class ChangeUserProfileEntriesFields : Migration {
        public override void Up() {
           Create.Column("profile_category")
                  .OnTable("user_profile_entries")
                  .AsString(512)
                  .Nullable()
                  .WithDefaultValue(null);
        }

        public override void Down() {
        }
    }
}