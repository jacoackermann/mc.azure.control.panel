using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040153)]
    public class CreateVersionsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("versions")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("product_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("document_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("reference_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("notes").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("versions");
        }
        #endregion
    }
}