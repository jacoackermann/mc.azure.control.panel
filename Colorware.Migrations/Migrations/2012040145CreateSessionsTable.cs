using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040145)]
    public class CreateSessionsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("sessions")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("session_id").AsString(255).NotNullable().WithDefaultValue("")
                  .WithColumn("data").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("sessions");
        }
        #endregion
    }
}