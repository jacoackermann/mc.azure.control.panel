﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201401151025)]
    public class AddHPIndigoExp05Curve : Migration {
        public override void Up() {
            Execute.EmbeddedScript("AddHPIndigoEXP05Curve.sql");
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}