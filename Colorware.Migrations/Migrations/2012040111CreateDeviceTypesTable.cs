using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040111)]
    public class CreateDeviceTypesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("device_types")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("manufacturer").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("caps").AsInt32().Nullable().WithDefaultValue("0")
                ;
        }

        public override void Down() {
            Delete.Table("device_types");
        }
        #endregion
    }
}