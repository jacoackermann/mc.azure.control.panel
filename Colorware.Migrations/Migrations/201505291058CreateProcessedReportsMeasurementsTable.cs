
using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201505291058)]
    public class CreateProcessedReportsMeasurementsTable : Migration {
        public override void Up() {
            Create.Table("processed_reports_measurements")
                .WithColumn("id").AsInt64().Identity().PrimaryKey()
                .WithColumn("external_reports_server_id").AsInt64().Nullable()
                .WithColumn("measurement_id").AsInt64().Nullable()
                ;
        }

        public override void Down() {
            Delete.Table("processed_reports_measurements");
        }
    }
}