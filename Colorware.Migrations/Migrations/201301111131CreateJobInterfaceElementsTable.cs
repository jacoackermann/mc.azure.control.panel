﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201301111131)]
    public class CreateJobInterfaceElementsTable : Migration {
        public override void Up() {
            Create.Table("job_interface_elements")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("job_id").AsInt64().NotNullable()
                  .WithColumn("family_id").AsInt32().Nullable()
                  .WithColumn("name").AsString(255).Nullable()
                  .WithColumn("tint").AsInt32().Nullable().WithDefaultValue(0)
                  .WithColumn("attribute_type").AsString(255).Nullable()
                  .WithColumn("description").AsString(int.MaxValue).Nullable()
                  .WithColumn("patch_type").AsInt32().Nullable().WithDefaultValue(0) // 0 => PatchTypes.Undefined
                ;
        }

        public override void Down() {
            Delete.Table("job_interface_elements");
        }
    }
}