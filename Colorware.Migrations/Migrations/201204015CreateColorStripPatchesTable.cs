using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201204015)]
    public class CreateColorStripPatchesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("color_strip_patches")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("color_strip_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("percentage").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("ink_zone").AsInt32().Nullable().WithDefaultValue("0")
                  .WithColumn("obsolete_process_identifier").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("obsolete_spot_identifier").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("patch_type").AsInt32().Nullable().WithDefaultValue("0")
                  .WithColumn("tolerance_group").AsInt32().Nullable().WithDefaultValue("1")
                  .WithColumn("slot1").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("slot2").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("slot3").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("sort_order").AsInt32().Nullable().WithDefaultValue("0")
                ;
        }

        public override void Down() {
            Delete.Table("color_strip_patches");
        }
        #endregion
    }
}