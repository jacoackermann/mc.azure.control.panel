using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040131)]
    public class CreateMeasurementConditionsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("measurement_conditions")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("is_standard").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("density_standard").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("dotgain_method").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("white_base").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("illuminant").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("observer_angle").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("use_density_pollfilter").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("use_spectral_pollfilter").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("measurement_conditions");
        }
        #endregion
    }
}