using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040114)]
    public class CreateEnteredSerialsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("entered_serials")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("key").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("product_code").AsString(255).Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("entered_serials");
        }
        #endregion
    }
}