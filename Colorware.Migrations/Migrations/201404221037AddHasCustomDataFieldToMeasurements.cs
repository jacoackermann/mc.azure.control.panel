﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201404221037)]
    public class AddHasCustomDataFieldToMeasurements : Migration {
        public override void Up() {
            Create.Column("has_custom_data").OnTable("measurements").AsBoolean().WithDefaultValue(false);
        }

        public override void Down() {
            Delete.Column("has_custom_data").FromTable("measurements");
        }
    }
}