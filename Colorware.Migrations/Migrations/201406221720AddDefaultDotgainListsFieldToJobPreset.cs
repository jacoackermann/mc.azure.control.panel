﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201406221720)]
    public class AddDefaultDotgainListsFieldToJobPreset : Migration {
        public override void Up() {
            Create.Column("default_dotgain_lists").OnTable("job_presets").AsString(255).WithDefaultValue(string.Empty);
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}