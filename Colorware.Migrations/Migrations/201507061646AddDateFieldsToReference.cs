﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201507061646)]
    public class AddDateFieldsToReference : Migration {
        public override void Up() {
            Create.Column("created_at")
                  .OnTable("color_references")
                  .AsDateTime()
                  .NotNullable()
                  .WithDefault(SystemMethods.CurrentDateTime);

            Create.Column("updated_at")
                  .OnTable("color_references")
                  .AsDateTime()
                  .NotNullable()
                  .WithDefault(SystemMethods.CurrentDateTime);
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}