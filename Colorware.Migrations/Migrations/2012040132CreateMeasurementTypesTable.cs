using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040132)]
    public class CreateMeasurementTypesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("measurement_types")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("measurement_types");
        }
        #endregion
    }
}