﻿using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201604061306)]
    public class AddedLinkBetweenClientAndExternalReportsServer : Migration {
        public override void Up() {
            Create.Column("valid_connection")
                  .OnTable("external_reports_servers")
                  .AsBoolean()
                  .NotNullable()
                  .WithDefaultValue(false);

            Create.Column("is_internal_server")
                  .OnTable("external_reports_servers")
                  .AsBoolean()
                  .NotNullable()
                  .WithDefaultValue(false);

            Create.Table("client_external_reports_servers")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("client_id").AsInt64().NotNullable().Indexed()
                  .WithColumn("external_reports_server_id").AsInt64().NotNullable().Indexed()
                  .WithColumn("active").AsBoolean().NotNullable()
                  .WithColumn("send_method").AsInt32().NotNullable();
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}