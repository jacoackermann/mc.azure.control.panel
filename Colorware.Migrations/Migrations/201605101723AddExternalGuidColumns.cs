﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201605101723)]
    public class AddExternalGuidColumns : Migration {
        private void addTo(string tableName) {
            Create.Column("external_guid")
                  .OnTable(tableName)
                  .AsGuid()
                  .NotNullable()
                  .WithDefault(SystemMethods.NewGuid);
        }
        private void removeFrom(string tableName) {
            Delete.Column("external_guid").FromTable(tableName);
        }
        public override void Up() {
            addTo("clients");
            addTo("companies");
            addTo("machines");
            addTo("machine_types");
            addTo("jobs");
            addTo("color_references");
            addTo("reference_patches");
            addTo("ink_sets");
            addTo("inks");
            addTo("job_strips");
            addTo("job_strip_patches");
            addTo("measurements");
            addTo("database_users");
            addTo("tolerance_sets");
            addTo("measurement_conditions");
            addTo("measurement_types");
            addTo("papers");
            addTo("paper_types");
        }

        public override void Down() {
            removeFrom("clients");
            removeFrom("companies");
            removeFrom("machines");
            removeFrom("machine_types");
            removeFrom("jobs");
            removeFrom("color_references");
            removeFrom("reference_patches");
            removeFrom("ink_sets");
            removeFrom("inks");
            removeFrom("job_strips");
            removeFrom("job_strip_patches");
            removeFrom("measurements");
            removeFrom("database_users");
            removeFrom("tolerance_sets");
            removeFrom("measurement_conditions");
            removeFrom("measurement_types");
            removeFrom("papers");
            removeFrom("paper_types");
        }
    }
}