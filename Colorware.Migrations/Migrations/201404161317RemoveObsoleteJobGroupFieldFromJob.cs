﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201404161317)]
    public class RemoveObsoleteJobGroupFieldFromJob : Migration {
        public override void Up() {
            // Drop obsolete columns from tables
            Delete.Index("IX_jobs_job_group_id").OnTable("jobs");
            Delete.Column("job_group_id")
                .FromTable("jobs");
        }

        public override void Down() {
        }
    }
}