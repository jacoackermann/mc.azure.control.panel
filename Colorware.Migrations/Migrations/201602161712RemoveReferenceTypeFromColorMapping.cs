﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201602161712)]
    public class RemoveReferenceTypeFromColorMapping : Migration {
        public override void Up() {
            Delete.Column("reference_type")
                  .FromTable("color_mappings");
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}