﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201401091701)]
    public class DropObsoleteTablesAndColumns : Migration {
        public override void Up() {
            // Drop complete obsolete tables
            Delete.Table("brands");
            Delete.Table("device_types");
            Delete.Table("devices");
            Delete.Table("documents");
            Delete.Table("entered_serials");
            Delete.Table("ink_sets_inks");
            Delete.Table("job_groups");
            Delete.Table("paper_weights");
            Delete.Table("printabilities");
            Delete.Table("products");
            Delete.Table("schema_migrations");
            Delete.Table("sessions");
            Delete.Table("sheet_jobs");
            Delete.Table("sheet_positions");
            Delete.Table("single_samples");
            Delete.Table("user_group_accesses");
            Delete.Table("user_group_sections");
            Delete.Table("versions");

            // Drop obsolete columns from tables
            Delete.Column("dry_delta_e")
                .FromTable("cached_compare_results");
            Delete.Column("guid")
                .FromTable("clients");
            Delete.Column("obsolete_process_identifier")
                .Column("obsolete_spot_identifier")
                .FromTable("color_strip_patches");

            Delete.Index("IX_companies_default_tolerance_set_id").OnTable("companies");
            Delete.Index("IX_companies_entered_serial_id").OnTable("companies");
            Delete.Column("default_tolerance_set_id")
                .Column("entered_serial_id")
                .FromTable("companies");

            Delete.Index("IX_database_users_language_id").OnTable("database_users");
            Delete.Column("db_database")
                .Column("db_username")
                .Column("db_adapter")
                .Column("db_password")
                .Column("db_host")
                .Column("language_id")
                .FromTable("database_users");

            Delete.Column("shared")
                .FromTable("ink_manufacturers");
            Delete.Column("obsolete_x")
                .Column("obsolete_y")
                .Column("obsolete_z")
                .Column("cloned")
                .Column("dotgain")
                .FromTable("job_strip_patches");

            Delete.Index("IX_job_strips_job_id").OnTable("job_strips");
            Delete.Index("IX_job_strips_color_strip_id").OnTable("job_strips");
            Delete.Column("job_id")
                .Column("color_strip_id")
                .FromTable("job_strips");

            Delete.Column("thumbnail_path")
                .Column("color_method_is_density")
                .FromTable("jobs");
            Delete.Column("type")
                .Column("definition")
                .FromTable("measurements");

            Delete.Index("IX_paper_types_paper_weight_id").OnTable("paper_types");
            Delete.Column("paper_weight_id")
                .FromTable("paper_types");

            Delete.Column("is_selected")
                .FromTable("printability_results");
            Delete.Column("obsolete_family")
                .FromTable("reference_patches");
            Delete.Column("obsolete_x")
                .Column("obsolete_y")
                .Column("obsolete_z")
                .Column("sort_order")
                .FromTable("samples");
        }

        public override void Down() {
        }
    }
}