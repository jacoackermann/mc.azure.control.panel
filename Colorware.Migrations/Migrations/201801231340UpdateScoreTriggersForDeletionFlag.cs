﻿using FluentMigrator;

using JetBrains.Annotations;

namespace Colorware.Migrations.Migrations {
    [Migration(201801231340), UsedImplicitly]
    public class UpdateScoreTriggersForDeletionFlag : Migration {
        public override void Up() {
            Execute.EmbeddedScript("UpdateJobCachedScoreFieldAndTriggersToIncorporateDeletionFlag.sql");
        }

        public override void Down() { }
    }
}