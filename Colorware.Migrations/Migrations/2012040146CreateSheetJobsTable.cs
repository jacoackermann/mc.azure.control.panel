using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040146)]
    public class CreateSheetJobsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("sheet_jobs")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("brand_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("product_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("version_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("number").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("description").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("sheet_jobs");
        }
        #endregion
    }
}