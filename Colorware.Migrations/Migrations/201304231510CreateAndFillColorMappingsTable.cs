﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201304231510)]
    public class CreateColorMappingsTable : Migration {
        public override void Up() {
            Create.Table("color_mappings")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("family").AsInt32()
                  .WithColumn("priority").AsInt32()
                  .WithColumn("reference_type").AsInt32()
                  .WithColumn("foreign_name").AsString(255)
                  .WithColumn("local_name").AsString(255)
                  ;

            Insert.IntoTable("color_mappings")
                  .Row(new {family = 1, priority = 1, reference_type = 1, foreign_name = "Black", local_name = "K"})
                  .Row(new { family = 1, priority = 2, reference_type = 1, foreign_name = "Cyan", local_name = "C" })
                  .Row(new { family = 1, priority = 3, reference_type = 1, foreign_name = "Magenta", local_name = "M" })
                  .Row(new { family = 1, priority = 4, reference_type = 1, foreign_name = "Yellow", local_name = "Y" })
                  .Row(new { family = 1, priority = 5, reference_type = 2, foreign_name = "Pantone *", local_name = "Pantone® *" })
                  ;
        }

        public override void Down() {
            Delete.Table("color_mappings");
        }
    }
}