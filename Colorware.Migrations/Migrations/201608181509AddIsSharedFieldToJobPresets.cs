﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {

    [Migration(201608181509)]
    public class AddIsSharedFieldToJobPresets : Migration {

        public override void Up() {
            Create.Column("is_shared")
                  .OnTable("job_presets")
                  .AsBoolean()
                  .NotNullable()
                  .WithDefaultValue(false);
        }

        public override void Down() {
            Delete.Column("is_shared")
                  .FromTable("job_presets");
        }
    }
}