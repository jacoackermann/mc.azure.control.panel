using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040133)]
    public class CreateMeasurementsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("measurements")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("measurement_type_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("type").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("content").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("definition").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("job_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("flow_change").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("ok_sheet").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("job_strip_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("reference_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("wet").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("is_production_mode").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("job_strip_start_index").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("job_strip_end_index").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("flexo_roll").AsString(255).Nullable().WithDefaultValue("0")
                  .WithColumn("flexo_panel").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("is_flexo").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("ink_zone_locks").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("is_audited").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("has_cached_score").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("cached_score").AsDouble().Nullable().WithDefaultValue("0")
                  .WithColumn("scumming_sample_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("roll_id").AsString(255).Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("measurements");
        }
        #endregion
    }
}