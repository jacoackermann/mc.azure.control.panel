using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040126)]
    public class CreateJobsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("jobs")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("number").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("description").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("last_date").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("sheet_number").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("sheet_side").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("thumbnail_path").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("tolerance_set_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("color_strip_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("reference_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("color_method_is_density").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("dotgain_method_is_density").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("paper_white_sample_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("sequence").AsString(255).Nullable().WithDefaultValue("CMYKs1s2")
                  .WithColumn("paper_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("ink_set_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("job_strip_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("ok_sheet_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("job_group_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("machine_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("measurement_type_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("user_group_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("client_reference_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("client_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("job_type").AsInt32().Nullable().WithDefaultValue("0")
                  .WithColumn("other_side_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("measurement_condition_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("is_production_mode").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("current_flow_change").AsInt32().Nullable().WithDefaultValue("0")
                  .WithColumn("job_strip_current_start_index").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("job_strip_current_end_index").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("ink_zone_locks").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("custom_field_data_1").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("custom_field_data_2").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("custom_field_data_3").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("custom_field_data_4").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("custom_field_data_5").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("custom_field_data_6").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("is_wide_flexo").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("image_name").AsString(255).Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("jobs");
        }
        #endregion
    }
}