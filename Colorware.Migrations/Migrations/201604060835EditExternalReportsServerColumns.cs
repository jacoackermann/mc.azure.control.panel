﻿using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201604060835)]
    public class EditExternalReportsServerColumns : Migration {
        public override void Up() {
            Create.Column("type_guid")
                  .OnTable("external_reports_servers")
                  .AsGuid()
                  .NotNullable()
                  .WithDefault(SystemMethods.NewGuid);

            Create.Column("verification_token")
                  .OnTable("external_reports_servers")
                  .AsString()
                  .Nullable()
                  .WithDefaultValue(null);

            Create.Column("username")
                  .OnTable("external_reports_servers")
                  .AsString()
                  .Nullable()
                  .WithDefaultValue(null);

            Create.Column("password")
                  .OnTable("external_reports_servers")
                  .AsString()
                  .Nullable()
                  .WithDefaultValue(null);

            Create.Column("extra_field_1")
                  .OnTable("external_reports_servers")
                  .AsString()
                  .Nullable()
                  .WithDefaultValue(null);

            Create.Column("extra_field_2")
                  .OnTable("external_reports_servers")
                  .AsString()
                  .Nullable()
                  .WithDefaultValue(null);

            Create.Column("extra_field_3")
                  .OnTable("external_reports_servers")
                  .AsString()
                  .Nullable()
                  .WithDefaultValue(null);

            Create.Column("extra_field_4")
                  .OnTable("external_reports_servers")
                  .AsString()
                  .Nullable()
                  .WithDefaultValue(null);
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}