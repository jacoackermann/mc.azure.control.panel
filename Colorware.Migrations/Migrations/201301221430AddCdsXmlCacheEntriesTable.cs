﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201301221430)]
    public class AddCdsXmlCacheEntriesTable : Migration {
        public override void Up() {
            Create.Table("cds_xml_cache_entries")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("job_id").AsInt64().NotNullable()
                  .WithColumn("measurement_id").AsInt64().NotNullable()
                  .WithColumn("transaction_type").AsInt32().NotNullable()
                  .WithColumn("content").AsString(int.MaxValue).NotNullable()
                ;
        }

        public override void Down() {
            Delete.Table("cds_xml_caches");
        }
    }
}