﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201309161156)]
    public class AddCustomClientFieldsToProcessControlStatistics : Migration {
        public override void Up() {
            Create.Column("custom_field_name_1").OnTable("process_control_statistics").AsString().Nullable();
            Create.Column("custom_field_data_1").OnTable("process_control_statistics").AsString().Nullable();
            Create.Column("custom_field_name_2").OnTable("process_control_statistics").AsString().Nullable();
            Create.Column("custom_field_data_2").OnTable("process_control_statistics").AsString().Nullable();
            Create.Column("custom_field_name_3").OnTable("process_control_statistics").AsString().Nullable();
            Create.Column("custom_field_data_3").OnTable("process_control_statistics").AsString().Nullable();
            Create.Column("custom_field_name_4").OnTable("process_control_statistics").AsString().Nullable();
            Create.Column("custom_field_data_4").OnTable("process_control_statistics").AsString().Nullable();
            Create.Column("custom_field_name_5").OnTable("process_control_statistics").AsString().Nullable();
            Create.Column("custom_field_data_5").OnTable("process_control_statistics").AsString().Nullable();
            Create.Column("custom_field_name_6").OnTable("process_control_statistics").AsString().Nullable();
            Create.Column("custom_field_data_6").OnTable("process_control_statistics").AsString().Nullable();
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}