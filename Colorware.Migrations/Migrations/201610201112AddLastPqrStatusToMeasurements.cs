﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {

    [Migration(201610201112)]
    public class AddLastPqrStatusToMeasurements : AutoReversingMigration {

        public override void Up() {
            Create.Column("last_pqr_status")
                  .OnTable("measurements")
                  .AsInt32()
                  .WithDefaultValue(0);
        }
    }
}