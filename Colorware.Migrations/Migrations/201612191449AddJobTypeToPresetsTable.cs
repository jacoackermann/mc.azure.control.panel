﻿using Colorware.Core.Enums;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201612191449)]
    public class AddJobTypeToPresetsTable : AutoReversingMigration {
        public override void Up() {
            Create.Column("job_type")
                  .OnTable("job_presets")
                  .AsInt32()
                  .WithDefaultValue((int)JobTypes.Offset);
        }
    }
}