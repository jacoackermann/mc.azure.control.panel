using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201204019)]
    public class CreateDefaultDotgainEntriesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("default_dotgain_entries")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("default_dotgain_list_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("percentage").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("dotgain").AsFloat().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("default_dotgain_entries");
        }
        #endregion
    }
}