
using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201602081605)]
    public class CreateJobInkInfoTable : Migration {
        public override void Up() {
            Create.Table("job_ink_infos")
                .WithColumn("id").AsInt64().Identity().PrimaryKey()
                .WithColumn("job_id").AsInt64().NotNullable()
                .WithColumn("slot").AsInt32().NotNullable()
                .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                .WithColumn("note").AsString(255).Nullable().WithDefaultValue("");
        }

        public override void Down() {
            Delete.Table("job_ink_infos");
        }
    }
}