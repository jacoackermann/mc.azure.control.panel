﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201210311200)]
    public class AddInitialData : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Execute.EmbeddedScript("InitialData.sql");
        }

        public override void Down() {
        }
        #endregion
    }
}