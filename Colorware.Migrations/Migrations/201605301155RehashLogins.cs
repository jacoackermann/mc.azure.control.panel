﻿using System.Collections.Generic;
using System.Data;
using System.Web.Helpers;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201605301155)]
    public class RehashLogins : Migration {
        public override void Up() {
            Execute.WithConnection((connection, transaction) => hashLogins(connection, transaction));
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }

        private void hashLogins(IDbConnection connection, IDbTransaction transaction) {
            var logins = getLogins(connection, transaction);
            foreach (var login in logins) {
                updateLogin(login, connection, transaction);
            }
        }

        private IEnumerable<Login> getLogins(IDbConnection connection, IDbTransaction transaction) {
            var cmd = connection.CreateCommand();
            cmd.CommandText = "SELECT id, password FROM database_users";
            cmd.CommandType = CommandType.Text;
            cmd.Transaction = transaction;
            using (var reader = cmd.ExecuteReader()) {
                var items = new List<Login>();
                while (reader.Read()) {
                    var item = new Login(reader.GetInt64(0), reader.GetString(1));
                    items.Add(item);
                }
                return items;
            }
        }

        private void updateLogin(Login login, IDbConnection connection, IDbTransaction transaction) {
            var hashedPassword = hashPassword(login.Password);

            var cmd = connection.CreateCommand();
            cmd.CommandText = "UPDATE database_users SET password = @password WHERE id = @id";
            cmd.CommandType = CommandType.Text;
            cmd.Transaction = transaction;

            var pId = cmd.CreateParameter();
            pId.ParameterName = "id";
            pId.Value = login.Id;
            cmd.Parameters.Add(pId);

            var pPwd = cmd.CreateParameter();
            pPwd.ParameterName = "password";
            pPwd.Value = hashedPassword;
            cmd.Parameters.Add(pPwd);

            cmd.ExecuteNonQuery();
        }

        private string hashPassword(string password) {
            return Crypto.HashPassword(password);
        }

        private struct Login {
            public long Id { get; private set; }
            public string Password { get; private set; }
            public Login(long id, string password) {
                Id = id;
                Password = password;
            }
        }
    }
}