
using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201506191105)]
    public class AddUserProfileEntriesTable : Migration {
        public override void Up() {
            Create.Table("user_profile_entries")
                .WithColumn("id").AsInt64().Identity().PrimaryKey()
                .WithColumn("user_group_id").AsInt64().NotNullable()
                .WithColumn("key").AsString(512).NotNullable().WithDefaultValue(string.Empty)
                .WithColumn("value").AsString(2048).Nullable().WithDefaultValue(string.Empty)
                .WithColumn("is_custom").AsBoolean().NotNullable().WithDefaultValue(false);
        }

        public override void Down() {
            Delete.Table("user_profile_entries");
        }
    }
}