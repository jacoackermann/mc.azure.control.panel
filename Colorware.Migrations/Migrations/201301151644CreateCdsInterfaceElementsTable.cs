﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201301151644)]
    public class CreateCdsInterfaceElementsTable : Migration {
        public override void Up() {
            Create.Table("cds_interface_elements")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("job_id").AsInt64().NotNullable().Indexed("job_id")
                  .WithColumn("family_id").AsInt32().NotNullable()
                  .WithColumn("name").AsString(255).NotNullable()
                  .WithColumn("tint").AsInt32().NotNullable().WithDefaultValue(100)
                  .WithColumn("description").AsString(255).Nullable().WithDefaultValue(string.Empty)
                  .WithColumn("patch_type").AsInt32().NotNullable()
                  .WithColumn("attribute_type").AsInt32().NotNullable()
                ;
        }

        public override void Down() {
            Delete.Table("cds_interface_elements");
        }
    }
}