using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201204016)]
    public class CreateColorStripsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("color_strips")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("active").AsBoolean().Nullable().WithDefaultValue("")
                  .WithColumn("shared").AsBoolean().Nullable().WithDefaultValue("")
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("description").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("rowcount").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("repeats").AsBoolean().Nullable().WithDefaultValue("")
                  .WithColumn("patchwidth").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("patches_per_zone").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("company_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("number_of_zones").AsInt32().Nullable().WithDefaultValue("0")
                  .WithColumn("default_mapping").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("patchheight").AsFloat().Nullable().WithDefaultValue("4.6")
                ;
        }

        public override void Down() {
            Delete.Table("color_strips");
        }
        #endregion
    }
}