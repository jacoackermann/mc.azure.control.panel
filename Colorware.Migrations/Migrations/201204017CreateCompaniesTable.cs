using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201204017)]
    public class CreateCompaniesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("companies")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("address1").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("address2").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("default_tolerance_set_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("zipcode1").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("zipcode2").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("city1").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("city2").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("contact_person").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("email").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("telephone").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("mobile").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("entered_serial_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                ;
        }

        public override void Down() {
            Delete.Table("companies");
        }
        #endregion
    }
}