﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201803081707)]
    public class RemoveObsoleteActiveAndSharedColumns : Migration {
        public override void Up() {
            Delete.Column("active").FromTable("color_strips");
            Delete.Column("active").FromTable("inks");
            Delete.Column("active").FromTable("ink_manufacturers");
            Delete.Column("active").FromTable("ink_sets");
            Delete.Column("shared").FromTable("ink_sets");
            Delete.Column("active").FromTable("ink_types");
            Delete.Column("active").FromTable("papers");
            Delete.Column("shared").FromTable("papers");
            Delete.Column("active").FromTable("paper_manufacturers");
            Delete.Column("shared").FromTable("paper_manufacturers");
            Delete.Column("active").FromTable("paper_types");
            Delete.Column("shared").FromTable("paper_types");
            Delete.Column("shared").FromTable("color_references");
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}