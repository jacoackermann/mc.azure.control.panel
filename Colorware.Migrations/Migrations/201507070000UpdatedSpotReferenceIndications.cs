
using Colorware.Migrations.Converters;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201507070000)]
    public class UpdatedSpotReferenceIndications : Migration {
        public override void Up() {
            Execute.WithConnection((connection, transaction) =>
                                   new UpdatedReferenceIndicationConvertor().ConvertAll(connection, transaction));
        }

        public override void Down() {
        }
    }
}