﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201611031250)]
    public class ParseJobPresetImageNameToGuid : Migration {
        public override void Up() {
            Execute.EmbeddedScript("JobPresetImageNameToGuid.sql");
            Execute.Sql(@"
                IF EXISTS (SELECT * 
                  FROM sys.default_constraints 
                   WHERE object_id = OBJECT_ID('DF_job_presets_image_name')
                   AND parent_object_id = OBJECT_ID('job_presets')
                )
				   ALTER TABLE [dbo].[job_presets] DROP CONSTRAINT [DF_job_presets_image_name]
            ");

            Alter.Table("job_presets")
                 .AlterColumn("image_name")
                 .AsGuid()
                 .NotNullable();

            Execute.Sql("ALTER TABLE [dbo].[job_presets] ADD  CONSTRAINT [DF_job_presets_image_name]  DEFAULT ('') FOR [image_name]");
        }

        public override void Down() {}
    }
}