using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012062010)]
    public class AlterPresetColorMappingField : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Delete.DefaultConstraint().OnTable("job_presets").OnColumn("color_mapping");
            Alter.Column("color_mapping")
                 .OnTable("job_presets")
                 .AsString(Int32.MaxValue)
                 .Nullable()
                 .WithDefaultValue("");
        }

        public override void Down() {
            Delete.DefaultConstraint().OnTable("job_presets").OnColumn("color_mapping");
            Alter.Column("color_mapping").OnTable("job_presets").AsString(255).Nullable().WithDefaultValue("");
        }
        #endregion
    }
}