using Colorware.Migrations.Converters;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201507080000)]
    public class MigrateJobPresetsToNewJobSetup : Migration {
        public override void Up() {
            Create.Column("ink_definitions_serialized")
                  .OnTable("job_presets")
                  .AsString(int.MaxValue)
                  .Nullable();

            Execute.WithConnection((connection, transaction) =>
                                   new JobPresetToNewJobSetupConverter().ConvertAll(connection, transaction));
        }

        public override void Down() {
            Delete.Column("ink_definitions_serialized")
                  .FromTable("job_presets");
        }
    }
}