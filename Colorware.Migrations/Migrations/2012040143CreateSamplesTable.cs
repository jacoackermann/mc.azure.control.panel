using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040143)]
    public class CreateSamplesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("samples")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("measurement_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("obsolete_x").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("obsolete_y").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("obsolete_z").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("spectrum").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("sort_order").AsInt32().Nullable().WithDefaultValue("0")
                  .WithColumn("density_c").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("density_m").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("density_y").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("density_k").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("density_s").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("s_wavelength").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("l").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("a").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("b").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("spectrum_start").AsInt32().Nullable().WithDefaultValue("400")
                  .WithColumn("spectrum_end").AsInt32().Nullable().WithDefaultValue("700")
                  .WithColumn("tag").AsInt32().Nullable().WithDefaultValue("-1")
                  .WithColumn("illuminant").AsInt32().Nullable().WithDefaultValue("0")
                  .WithColumn("observer").AsInt32().Nullable().WithDefaultValue("0")
                ;
        }

        public override void Down() {
            Delete.Table("samples");
        }
        #endregion
    }
}