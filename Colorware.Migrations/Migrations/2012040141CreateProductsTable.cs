using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040141)]
    public class CreateProductsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("products")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("brand_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("notes").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("products");
        }
        #endregion
    }
}