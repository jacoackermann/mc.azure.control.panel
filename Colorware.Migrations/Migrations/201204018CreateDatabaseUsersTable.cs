using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201204018)]
    public class CreateDatabaseUsersTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("database_users")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("active").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("user_group_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("username").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("password").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("db_adapter").AsString(255).Nullable().WithDefaultValue("mysql")
                  .WithColumn("db_database").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("db_username").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("db_password").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("db_host").AsString(255).Nullable().WithDefaultValue("localhost")
                  .WithColumn("company_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("language_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("requested_user_level").AsInt32().Nullable().WithDefaultValue("-1")
                ;
        }

        public override void Down() {
            Delete.Table("database_users");
        }
        #endregion
    }
}