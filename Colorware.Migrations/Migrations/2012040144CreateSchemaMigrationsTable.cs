using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040144)]
    public class CreateSchemaMigrationsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("schema_migrations")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("version").AsString(255).NotNullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("schema_migrations");
        }
        #endregion
    }
}