using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040120)]
    public class CreateInksTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("inks")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("active").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("user_group_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("ink_type_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("ink_manufacturer_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("ink_set_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                ;
        }

        public override void Down() {
            Delete.Table("inks");
        }
        #endregion
    }
}