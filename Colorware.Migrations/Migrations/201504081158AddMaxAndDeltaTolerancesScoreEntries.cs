﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201504081158)]
    public class AddMaxAndDeltaTolerancesScoreEntries : Migration {
        public override void Up() {
            Create.Column("score_for_max")
                  .OnTable("tolerance_sets")
                  .AsInt32()
                  .WithDefaultValue("10");
            Create.Column("score_for_average")
                  .OnTable("tolerance_sets")
                  .AsInt32()
                  .WithDefaultValue("10");

            Create.Column("score_percentage_for_max")
                  .OnTable("tolerance_sets")
                  .AsFloat()
                  .WithDefaultValue("1");
            Create.Column("score_percentage_for_average")
                  .OnTable("tolerance_sets")
                  .AsFloat()
                  .WithDefaultValue("1");
        }

        public override void Down() {
            Delete.Column("score_for_max").FromTable("tolerance_sets");
            Delete.Column("score_for_average").FromTable("tolerance_sets");

            Delete.Column("score_percentage_for_max").FromTable("tolerance_sets");
            Delete.Column("score_percentage_for_average").FromTable("tolerance_sets");
        }
    }
}