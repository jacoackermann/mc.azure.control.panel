﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {

    [Migration(201711151052)]
    public class CreateJobPresetDisabledPatchesTable : Migration {

        public override void Up() {
            Create.Table("job_preset_disabled_patches")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("job_preset_id").AsInt64().NotNullable()
                  .WithColumn("job_strip_patch_index").AsInt32().NotNullable()
                ;
        }

        public override void Down() {
            Delete.Table("job_preset_disabled_patches");
        }
    }
}