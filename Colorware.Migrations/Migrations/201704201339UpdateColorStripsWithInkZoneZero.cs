﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201704201339)]
    public class UpdateColorStripsWithInkZoneZero : Migration {
        public override void Up() {
            Execute.EmbeddedScript("UpdateColorStripsWithInkZoneZero.sql");
        }

        public override void Down() {
        }
    }
}