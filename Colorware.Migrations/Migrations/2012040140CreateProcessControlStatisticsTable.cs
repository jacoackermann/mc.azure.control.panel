using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040140)]
    public class CreateProcessControlStatisticsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("process_control_statistics")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("user_name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("job_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("job_name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("job_number").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("job_description").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("sheet_number").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("production_status").AsString(50).Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("client_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("client_name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("client_colorbook_name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("colorbar_name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("measurement_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("measurement_comment").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("machine_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("machine_name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("paper_type_name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("sort_order").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("ink_zone").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("patch_type").AsString(100).Nullable().WithDefaultValue("")
                  .WithColumn("sample_name").AsString(200).Nullable().WithDefaultValue("")
                  .WithColumn("sample_percentage").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("sample_l").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("sample_a").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("sample_b").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("reference_l").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("reference_a").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("reference_b").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("sample_density").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("reference_density").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("reference_dotgain").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("sample_dotgain").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("delta_e").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("delta_h").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("delta_density").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("sample_spectrum").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("sample_spectrum_start").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("sample_spectrum_end").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("tolerance_set_name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("tolerance_density").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("tolerance_dotgain").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("tolerance_delta_e").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("conditions_name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("conditions_status").AsString(100).Nullable().WithDefaultValue("")
                  .WithColumn("conditions_density_polfilter").AsBoolean().Nullable().WithDefaultValue("")
                  .WithColumn("conditions_spectral_polfilter").AsBoolean().Nullable().WithDefaultValue("")
                  .WithColumn("conditions_illuminant").AsString(100).Nullable().WithDefaultValue("")
                  .WithColumn("conditions_observer_angle").AsString(50).Nullable().WithDefaultValue("")
                  .WithColumn("is_flexo").AsBoolean().Nullable().WithDefaultValue("")
                  .WithColumn("flexo_roll").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("flexo_panel").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("sample_guid").AsString(80).Nullable().WithDefaultValue("")
                  .WithColumn("measurement_guid").AsString(80).Nullable().WithDefaultValue("")
                  .WithColumn("has_scumming").AsBoolean().Nullable().WithDefaultValue("")
                  .WithColumn("scumming_delta_e").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("scumming_raw_board_l").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("scumming_raw_board_a").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("scumming_raw_board_b").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("roll_id").AsString(255).Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("process_control_statistics");
        }
        #endregion
    }
}