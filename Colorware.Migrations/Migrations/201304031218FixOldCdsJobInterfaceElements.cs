﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201304031218)]
    public class FixOldCdsJobInterfaceElements : Migration {
        public override void Up() {
            // The Density CdsAttributeTypes enumeration item was added out of order. This caused old job interface elements to
            // have the wrong attribute type set (visual becomes density, registry becomes visual).
            // The queries below aim to correct the situation for old jobs.
            const int oldRegistrationId = 2;
            const int oldVisualId = 3;
            const int undefinedPatchTypeId = 0; // PatchTypes.Undefined.

            const int newRegistrationId = 3;
            const int newVisualId = 4;

            var sqlToUpdateRegistration = string.Format(
                "UPDATE cds_interface_elements SET attribute_type = {0} WHERE attribute_type = {1} AND patch_type = {2} AND name = '{3}'", 
                    newRegistrationId, oldRegistrationId, undefinedPatchTypeId, "REG" );
            var sqlToUpdateVisual = string.Format(
                "UPDATE cds_interface_elements SET attribute_type = {0} WHERE attribute_type = {1} AND patch_type = {2} AND name = '{3}'", 
                    newVisualId, oldVisualId, undefinedPatchTypeId, "VISUAL" );

            Execute.Sql(sqlToUpdateRegistration);
            Execute.Sql(sqlToUpdateVisual);
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}