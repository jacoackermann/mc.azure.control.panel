﻿using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201711211130)]
    public class AddPieceOfArtToJobs : Migration {
        public override void Up() {
            Create.Column("piece_of_art")
                  .OnTable("jobs")
                  .AsString(Int32.MaxValue)
                  .Nullable();
        }

        public override void Down() {
        }
    }
}