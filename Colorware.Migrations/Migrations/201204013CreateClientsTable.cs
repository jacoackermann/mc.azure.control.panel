using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201204013)]
    public class CreateClientsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("clients")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("guid").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("notes").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("user_group_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("custom_field_name_1").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("custom_field_name_2").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("custom_field_name_3").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("custom_field_name_4").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("custom_field_name_5").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("custom_field_name_6").AsString(255).Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("clients");
        }
        #endregion
    }
}