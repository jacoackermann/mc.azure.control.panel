﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201711091447)]
    public class DeleteUseDensitometricDotgainFromToleranceSet : Migration {
        public override void Up() {
            Delete.Column("use_densitometric_dotgain")
                  .FromTable("tolerance_sets");
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}