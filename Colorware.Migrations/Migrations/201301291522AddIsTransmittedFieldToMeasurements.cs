﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201301291522)]
    public class AddIsTransmittedFieldToMeasurements : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Column("is_transmitted").OnTable("measurements").AsBoolean().NotNullable().WithDefaultValue(false);
        }

        public override void Down() {
            Delete.Column("is_transmitted").FromTable("measurements");
        }
        #endregion
    }
}