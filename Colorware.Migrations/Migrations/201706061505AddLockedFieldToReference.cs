﻿using Colorware.Migrations.Converters;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201706061505)]
    public class AddLockedFieldToReference : Migration {
        public override void Up() {
            Create.Column("locked")
                  .OnTable("color_references")
                  .AsBoolean()
                  .NotNullable()
                  .SetExistingRowsTo(false);
        }

        public override void Down() {
            Delete.Column("locked").FromTable("color_references");
        }
    }
}