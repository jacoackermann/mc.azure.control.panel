﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201301181615)]
    public class AddCdsFieldsToJob : Migration {
        public override void Up() {
            Create.Column("cds_scoring_set_id").OnTable("jobs").AsInt64().Nullable();
            Create.Column("cds_scoring_set_name").OnTable("jobs").AsString().Nullable();
        }

        public override void Down() {
            Delete.Column("cds_scoring_set_id").FromTable("jobs");
            Delete.Column("cds_scoring_set_name").FromTable("jobs");
        }
    }
}