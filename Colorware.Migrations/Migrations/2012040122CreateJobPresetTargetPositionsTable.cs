using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040122)]
    public class CreateJobPresetTargetPositionsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("job_preset_target_positions")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("job_preset_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("x").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("y").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("job_preset_target_positions");
        }
        #endregion
    }
}