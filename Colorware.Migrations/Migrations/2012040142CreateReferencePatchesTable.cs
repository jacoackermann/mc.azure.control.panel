using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040142)]
    public class CreateReferencePatchesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("reference_patches")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("reference_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("obsolete_family").AsInt32().Nullable().WithDefaultValue("-1")
                  .WithColumn("percentage").AsInt32().Nullable().WithDefaultValue("100")
                  .WithColumn("is_spot").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("x").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("y").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("z").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("l").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("a").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("b").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("cyan").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("magenta").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("yellow").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("key").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("default_density").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("default_dotgain").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("parent1_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("parent2_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("parent3_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("patch_type").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("spectral_density").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("wavelength_of_spectral_density").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("spectrum").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("spectrum_start").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("spectrum_end").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("illuminant").AsInt32().Nullable().WithDefaultValue("0")
                  .WithColumn("observer").AsInt32().Nullable().WithDefaultValue("0")
                  .WithColumn("gray_c").AsDouble().Nullable().WithDefaultValue("0")
                  .WithColumn("gray_m").AsDouble().Nullable().WithDefaultValue("0")
                  .WithColumn("gray_y").AsDouble().Nullable().WithDefaultValue("0")
                ;
        }

        public override void Down() {
            Delete.Table("reference_patches");
        }
        #endregion
    }
}