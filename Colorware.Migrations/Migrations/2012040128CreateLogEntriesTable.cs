using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040128)]
    public class CreateLogEntriesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("log_entries")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("database_user_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("target_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("target").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("action").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("description").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("log_entries");
        }
        #endregion
    }
}