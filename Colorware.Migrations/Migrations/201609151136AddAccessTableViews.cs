﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201609151136)]
    public class AddAccessTableViews : Migration {
        public override void Up() {
            Execute.EmbeddedScript("AddAccessTables.sql");
        }

        public override void Down() {}
    }
}