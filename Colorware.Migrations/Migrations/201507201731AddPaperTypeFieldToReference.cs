﻿using Colorware.Migrations.Converters;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201507201731)]
    public class AddPaperTypeFieldToReference : Migration {
        public override void Up() {
            Create.Column("paper_type_id")
                  .OnTable("color_references")
                  .AsInt64()
                  .Nullable()
                  .Indexed();
            Execute.WithConnection((connection, transaction) =>
                                   new RelinkPaperTypeReferencesConverter().ConvertAll(connection, transaction));
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}