using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201707051044)]
    public class FixCustomTranslationIdFieldCasing : Migration {
        public override void Up() {
            // BaseModel ID field must be lowercase, as specified on the model using the
            // ServiceField attribute
            Rename.Column("Id")
                  .OnTable("custom_translations")
                  .To("id");
        }

        public override void Down() { }
    }
}