﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201311261513)]
    public class ChangeJobTriggersToCalculateScoresWithoutOkSheet : Migration {
        public override void Up() {
            Execute.EmbeddedScript("UpdateJobCachedScoreFieldAndTriggersToNotIncorporateOkSheet.sql");
        }

        public override void Down() {
        }
    }
}