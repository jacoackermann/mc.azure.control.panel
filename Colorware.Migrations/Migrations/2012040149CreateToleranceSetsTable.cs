using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040149)]
    public class CreateToleranceSetsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("tolerance_sets")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("delta_e_method").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("delta_H_calculated").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("delta_H_tolerence").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("primaries_calculated").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("primaries_tolerance").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("primaries_variation").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("secondaries_calculated").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("secondaries_tolerance").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("secondaries_variation").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("paperwhite_calculated").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("paperwhite_tolerance").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("graybalance_calculated").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("graybalance_tolerance").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("graybalance_variation").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("spotcolor_calculated").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("spotcolor_tolerance").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("spotcolor_variation").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("max_calculated").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("max_tolerance").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("average_calculated").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("average_tolerance").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("density_calculated").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("density_tolerance").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("density_variation").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("dotgain_calculated").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("dotgain_tolerance").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("dotgain_variation").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("dotgain_spread").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("delta_h_primaries_calculated").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("delta_h_primaries_tolerance").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("score_for_primaries").AsInt32().Nullable().WithDefaultValue("5")
                  .WithColumn("score_for_dotgains").AsInt32().Nullable().WithDefaultValue("5")
                  .WithColumn("score_for_secondaries").AsInt32().Nullable().WithDefaultValue("5")
                  .WithColumn("score_for_paperwhite").AsInt32().Nullable().WithDefaultValue("5")
                  .WithColumn("score_for_graybalance").AsInt32().Nullable().WithDefaultValue("5")
                  .WithColumn("score_for_other").AsInt32().Nullable().WithDefaultValue("5")
                  .WithColumn("default_measurement_condition_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("use_densitometric_dotgain").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("delta_cmc_l").AsFloat().Nullable().WithDefaultValue("2")
                  .WithColumn("delta_cmc_c").AsFloat().Nullable().WithDefaultValue("1")
                  .WithColumn("dotgain_delta_e_calculated").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("dotgain_delta_e_tolerance").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("dotgain_delta_e_variation").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("score_percentage_for_primaries").AsFloat().Nullable().WithDefaultValue("1")
                  .WithColumn("score_percentage_for_dotgains").AsFloat().Nullable().WithDefaultValue("1")
                  .WithColumn("score_percentage_for_secondaries").AsFloat().Nullable().WithDefaultValue("1")
                  .WithColumn("score_percentage_for_paperwhite").AsFloat().Nullable().WithDefaultValue("1")
                  .WithColumn("score_percentage_for_graybalance").AsFloat().Nullable().WithDefaultValue("1")
                  .WithColumn("score_percentage_for_other").AsFloat().Nullable().WithDefaultValue("1")
                  .WithColumn("scumming_calculated").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("scumming_tolerance").AsFloat().Nullable().WithDefaultValue("1")
                ;
        }

        public override void Down() {
            Delete.Table("tolerance_sets");
        }
        #endregion
    }
}