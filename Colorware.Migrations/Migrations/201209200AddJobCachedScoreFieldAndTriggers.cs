using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201209201200)]
    public class AddJobCachedScoreFieldAndTriggers : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Execute.EmbeddedScript("AddJobCachedScoreFieldAndTriggers.sql");
        }

        public override void Down() {
        }
        #endregion
    }
}