﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201405141735)]
    public class ChangeUserRequestedLicenseField : Migration {
        public override void Up() {
            Delete.Column("requested_user_level").FromTable("database_users");
            Create.Column("requested_license_product")
                  .OnTable("database_users")
                  .AsGuid()
                  .Nullable();
        }

        public override void Down() {
        }
    }
}