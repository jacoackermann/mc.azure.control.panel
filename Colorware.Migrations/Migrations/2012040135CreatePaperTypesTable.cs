using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040135)]
    public class CreatePaperTypesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("paper_types")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("active").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("paper_weight_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("shared").AsBoolean().Nullable().WithDefaultValue("")
                  .WithColumn("number").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("description").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("color_reference_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                ;
        }

        public override void Down() {
            Delete.Table("paper_types");
        }
        #endregion
    }
}