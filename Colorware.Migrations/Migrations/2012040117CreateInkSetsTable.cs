using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040117)]
    public class CreateInkSetsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("ink_sets")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("active").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("shared").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("ink_sets");
        }
        #endregion
    }
}