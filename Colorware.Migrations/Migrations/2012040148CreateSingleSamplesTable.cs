using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040148)]
    public class CreateSingleSamplesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("single_samples")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("job_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("reference_patch_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("x").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("y").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("z").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("raw").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("density_c").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("density_m").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("density_y").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("density_k").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("single_samples");
        }
        #endregion
    }
}