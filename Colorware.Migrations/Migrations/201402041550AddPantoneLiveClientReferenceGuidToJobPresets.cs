﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201402041550)]
    public class AddPantoneLiveClientReferenceGuidToJobPresets : Migration {
        public override void Up() {
            Create.Column("pantone_live_client_reference_guid").OnTable("job_presets").AsString(255).Nullable();
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}