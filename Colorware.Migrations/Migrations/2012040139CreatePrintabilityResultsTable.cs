using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040139)]
    public class CreatePrintabilityResultsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("printability_results")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("reference_patch_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("ink_set_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("job_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("measurement_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("job_name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("job_number").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("measurement_date").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("chosen_density").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("delta_e").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("is_wet").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("is_selected").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("printability_results");
        }
        #endregion
    }
}