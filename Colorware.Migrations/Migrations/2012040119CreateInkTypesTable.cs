using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040119)]
    public class CreateInkTypesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("ink_types")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("active").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("ink_types");
        }
        #endregion
    }
}