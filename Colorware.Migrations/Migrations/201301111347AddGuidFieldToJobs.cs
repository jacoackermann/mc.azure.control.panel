﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201301111347)]
    public class AddGuidFieldToJobs : Migration {
        public override void Up() {
            Create.Column("guid").OnTable("jobs").AsString(255).Nullable();
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}