﻿using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201612011416)]
    public class AddICCFileGuid : AutoReversingMigration {
        public override void Up() {
            Alter.Table("color_references")
                 .AddColumn("icc_file_name")
                 .AsGuid()
                 .NotNullable()
                 .WithDefaultValue(Guid.Empty);
        }
    }
}