﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201612201108)]
    public class AddOpacityEntriesToToleranceSets : Migration {
        public override void Up() {
            Create.Column("opacity_measured")
                  .OnTable("tolerance_sets")
                  .AsBoolean()
                  .WithDefaultValue("false");
            Create.Column("opacity_target")
                  .OnTable("tolerance_sets")
                  .AsFloat()
                  .WithDefaultValue("50");
            Create.Column("score_for_opacity")
                  .OnTable("tolerance_sets")
                  .AsFloat()
                  .WithDefaultValue("20");
        }

        public override void Down() {
            Delete.Column("opacity_measured").FromTable("tolerance_sets");
            Delete.Column("opacity_target").FromTable("tolerance_sets");
            Delete.Column("score_for_opacity").FromTable("tolerance_sets");
        }
    }
}