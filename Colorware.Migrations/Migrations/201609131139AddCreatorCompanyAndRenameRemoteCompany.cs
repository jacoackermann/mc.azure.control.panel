﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201609131139)]
    public class AddCreatorCompanyAndRenameRemoteCompany : Migration {
        public override void Up() {
            Rename.Column("remote_company_id")
                  .OnTable("jobs")
                  .To("print_location_company_id");

            Create.Column("creator_company_id")
                  .OnTable("jobs")
                  .AsInt64()
                  .NotNullable()
                  .WithDefaultValue(0);

            // 1. Set the CreatorCompany to the company associated with the job's user (the user that created the job),
            //    this is usually already set, but I'm not sure it was done properly in older MC versions.
            // 2. Set all not yet set PrintLocationCompany fields equal to the CreatorCompany (the not-yet-set check
            // technically isn't necessary because this migration is part of the initial migrations of 16.2, but we 
            // have some test installs already that might have already filled the RemoteCompany column)
            Execute.Sql(@"
UPDATE
    jobs
SET
	creator_company_id = companies.id
FROM
    jobs
INNER JOIN database_users ON database_users.id = jobs.user_id
INNER JOIN companies ON companies.id = database_users.company_id

UPDATE
    jobs
SET
	print_location_company_id = creator_company_id
WHERE
	jobs.print_location_company_id = 0");
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}