﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201605091036)]
    public class AddStatusFieldsToProcessedReportsMeasurementsTable : Migration {
        public override void Up() {
            Create.Column("status").OnTable("processed_reports_measurements")
                  .AsInt16()
                  .WithDefaultValue(1);
            Create.Column("message").OnTable("processed_reports_measurements")
                  .AsString(int.MaxValue)
                  .Nullable()
                  .WithDefaultValue(null);

            Create.Index().OnTable("processed_reports_measurements").OnColumn("measurement_id");
            Create.Index().OnTable("processed_reports_measurements").OnColumn("external_reports_server_id");
        }

        public override void Down() {
            Delete.Column("message").FromTable("processed_reports_measurements");
            Delete.Column("status").FromTable("processed_reports_measurements");
        }
    }
}