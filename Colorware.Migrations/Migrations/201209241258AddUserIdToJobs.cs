﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201209241258)]
    public class AddUserIdToJobs : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Column("user_id").OnTable("jobs").AsInt64().WithDefaultValue("");
        }

        public override void Down() {
            Delete.DefaultConstraint().OnTable("jobs").OnColumn("user_id");
            Delete.Column("user_id").FromTable("jobs");
        }
        #endregion
    }
}