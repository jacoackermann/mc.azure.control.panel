using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040110)]
    public class CreateDefaultDotgainListsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("default_dotgain_lists")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("default_dotgain_lists");
        }
        #endregion
    }
}