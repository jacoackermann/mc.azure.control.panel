using Colorware.Migrations.Converters;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201708041347)]
    public class MigrateImageJobPresetsToNewJobSetup : Migration {
        public override void Up() {
            Create.Column("ink_definitions_sequence_index")
                  .OnTable("job_preset_target_positions")
                  .AsInt32()
                  .NotNullable()
                  .WithDefaultValue(-1);
            Create.Column("is_substrate")
                  .OnTable("job_preset_target_positions")
                  .AsBoolean()
                  .NotNullable()
                  .WithDefaultValue(false);

            Execute.WithConnection((connection, transaction) =>
                                   new ImageJobPresetToNewJobSetupConverter().ConvertAll(connection, transaction));
        }

        public override void Down() {
            Delete.Column("ink_definitions_sequence_index")
                  .FromTable("job_preset_target_positions");
            Delete.Column("is_substrate")
                  .FromTable("job_preset_target_positions");
        }
    }
}