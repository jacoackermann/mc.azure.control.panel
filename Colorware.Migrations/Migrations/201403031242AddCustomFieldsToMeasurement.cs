﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201403031242)]
    public class AddCustomFieldsToMeasurement : Migration {
        public override void Up() {
            Create.Column("custom_string_field_1").OnTable("measurements").AsString(255).WithDefaultValue(string.Empty);
            Create.Column("custom_string_field_2").OnTable("measurements").AsString(255).WithDefaultValue(string.Empty);
            Create.Column("custom_string_field_3").OnTable("measurements").AsString(255).WithDefaultValue(string.Empty);
            Create.Column("custom_string_field_4").OnTable("measurements").AsString(255).WithDefaultValue(string.Empty);
            Create.Column("custom_string_field_5").OnTable("measurements").AsString(255).WithDefaultValue(string.Empty);

            Create.Column("custom_float_field_1").OnTable("measurements").AsDouble().WithDefaultValue(0f);
            Create.Column("custom_float_field_2").OnTable("measurements").AsDouble().WithDefaultValue(0f);
            Create.Column("custom_float_field_3").OnTable("measurements").AsDouble().WithDefaultValue(0f);
            Create.Column("custom_float_field_4").OnTable("measurements").AsDouble().WithDefaultValue(0f);
            Create.Column("custom_float_field_5").OnTable("measurements").AsDouble().WithDefaultValue(0f);
        }

        public override void Down() {
            Delete.Column("custom_string_field_1").FromTable("measurements");
            Delete.Column("custom_string_field_2").FromTable("measurements");
            Delete.Column("custom_string_field_3").FromTable("measurements");
            Delete.Column("custom_string_field_4").FromTable("measurements");
            Delete.Column("custom_string_field_5").FromTable("measurements");

            Delete.Column("custom_float_field_1").FromTable("measurements");
            Delete.Column("custom_float_field_2").FromTable("measurements");
            Delete.Column("custom_float_field_3").FromTable("measurements");
            Delete.Column("custom_float_field_4").FromTable("measurements");
            Delete.Column("custom_float_field_5").FromTable("measurements");
        }
    }
}