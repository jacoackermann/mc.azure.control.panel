using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040136)]
    public class CreatePaperWeightsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("paper_weights")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("active").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("shared").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("weight").AsInt32().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("paper_weights");
        }
        #endregion
    }
}