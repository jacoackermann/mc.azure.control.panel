using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040124)]
    public class CreateJobStripPatchesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("job_strip_patches")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("job_strip_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("reference_patch_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("ink_zone").AsInt32().Nullable().WithDefaultValue("0")
                  .WithColumn("patch_type").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("obsolete_x").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("obsolete_y").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("obsolete_z").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("cyan").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("magenta").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("yellow").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("key").AsFloat().Nullable().WithDefaultValue("0")
                  .WithColumn("cloned").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("tolerance_group").AsInt32().Nullable().WithDefaultValue("1")
                  .WithColumn("dotgain").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("default_density").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("default_dotgain").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("percentage").AsInt32().Nullable().WithDefaultValue("0")
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("UNDEFINED")
                  .WithColumn("is_customized").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("l").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("a").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("b").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("slot1").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("slot2").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("slot3").AsInt32().Nullable().WithDefaultValue("")
                  .WithColumn("spectrum").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("spectrum_start").AsInt32().Nullable().WithDefaultValue("400")
                  .WithColumn("spectrum_end").AsInt32().Nullable().WithDefaultValue("700")
                  .WithColumn("illuminant").AsInt32().Nullable().WithDefaultValue("0")
                  .WithColumn("observer").AsInt32().Nullable().WithDefaultValue("0")
                  .WithColumn("target_x").AsInt32().Nullable().WithDefaultValue("0")
                  .WithColumn("target_y").AsInt32().Nullable().WithDefaultValue("0")
                ;

            // Create.Index("reference__patch_id").OnTable("job_strip_patches").WithOptions().Clustered().OnColumn("reference_patch_id");
            // Create.Index().OnTable("job_strip_patches").OnColumn("reference_patch_id");
        }

        public override void Down() {
            Delete.Table("job_strip_patches");
        }
        #endregion
    }
}