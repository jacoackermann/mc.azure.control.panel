using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040113)]
    public class CreateDocumentsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("documents")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("filename").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("mime").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("description").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("documents");
        }
        #endregion
    }
}