using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040123)]
    public class CreateJobPresetsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("job_presets")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("user_group_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("machine_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("paper_type_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("tolerance_set_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("measurement_condition_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("client_reference_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("paper_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("ink_set_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("color_strip_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("description").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("reference_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("color_mapping").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("color_sequence").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("default_densities").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("job_colors").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("image_name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("number_of_panels").AsInt32().Nullable().WithDefaultValue("1")
                ;
        }

        public override void Down() {
            Delete.Table("job_presets");
        }
        #endregion
    }
}