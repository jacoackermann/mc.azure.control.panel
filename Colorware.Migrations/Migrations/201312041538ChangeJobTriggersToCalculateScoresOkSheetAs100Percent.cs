﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201312041538)]
    public class ChangeJobTriggersToCalculateScoresOkSheetAs100Percent : Migration {
        public override void Up() {
            Execute.EmbeddedScript("UpdateJobCachedScoreFieldAndTriggersToIncorporateOkSheetAs100Percent.sql");
        }

        public override void Down() {
        }
    }
}