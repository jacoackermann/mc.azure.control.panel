﻿using FluentMigrator;

using JetBrains.Annotations;

namespace Colorware.Migrations.Migrations {
    [Migration(201801191516), UsedImplicitly]
    public class AddMachineTypeAccessList : Migration {
        public override void Up() {
            Execute.EmbeddedScript("AddAccessTables_MachineType.sql");
        }

        public override void Down() {
        }
    }
}