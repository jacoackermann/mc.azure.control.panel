﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201612121218)]
    public class AddG7ToToleranceSet : AutoReversingMigration {
        public override void Up() {
            Create.Column("g7_calculated").OnTable("tolerance_sets")
                  .AsBoolean().Nullable().WithDefaultValue("0");
            Create.Column("g7_weighted_delta_l_average").OnTable("tolerance_sets")
                  .AsFloat().Nullable().WithDefaultValue("2.5");
            Create.Column("g7_weighted_delta_l_peak").OnTable("tolerance_sets")
                  .AsFloat().Nullable().WithDefaultValue("5");
            Create.Column("g7_weighted_delta_ch_average").OnTable("tolerance_sets")
                  .AsFloat().Nullable().WithDefaultValue("3");
            Create.Column("g7_weighted_delta_ch_peak").OnTable("tolerance_sets")
                  .AsFloat().Nullable().WithDefaultValue("7");
            Create.Column("score_for_g7").OnTable("tolerance_sets")
                  .AsInt32().Nullable().WithDefaultValue("5");
            Create.Column("score_percentage_for_g7").OnTable("tolerance_sets")
                  .AsFloat().Nullable().WithDefaultValue("1");
        }
    }
}