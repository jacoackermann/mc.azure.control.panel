﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201504131509)]
    public class AddSpotScoreEntriesToToleranceSet : Migration {
        public override void Up() {
            Create.Column("score_for_spots")
                  .OnTable("tolerance_sets")
                  .AsInt32()
                  .WithDefaultValue("5");
            Create.Column("score_percentage_for_spots")
                  .OnTable("tolerance_sets")
                  .AsFloat()
                  .WithDefaultValue("1");
        }

        public override void Down() {
            Delete.Column("score_for_spots").FromTable("tolerance_sets");
            Delete.Column("score_percentage_for_spots").FromTable("tolerance_sets");
        }
    }
}