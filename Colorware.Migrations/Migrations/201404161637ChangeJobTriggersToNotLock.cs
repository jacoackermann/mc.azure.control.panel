﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201404161637)]
    public class ChangeJobTriggersToNotLock : Migration {
        public override void Up() {
            Execute.EmbeddedScript("UpdateJobCachedScoreTriggersToNotLock.sql");
        }

        public override void Down() {
        }
    }
}