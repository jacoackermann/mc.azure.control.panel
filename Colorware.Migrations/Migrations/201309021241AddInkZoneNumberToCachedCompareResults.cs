﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201309021241)]
    public class AddInkZoneNumberToCachedCompareResults : Migration {
        public override void Up() {
            Create.Column("ink_zone").OnTable("cached_compare_results").AsInt32().WithDefaultValue(-1).NotNullable();
        }

        public override void Down() {
            Delete.Column("ink_zone").FromTable("cached_compare_results");
        }
    }
}