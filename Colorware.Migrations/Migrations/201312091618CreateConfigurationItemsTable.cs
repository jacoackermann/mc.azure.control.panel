﻿using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201312091618)]
    public class CreateConfigurationItemsTable : Migration {
        public override void Up() {
            Create.Table("configuration_items")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("key").AsString(255).NotNullable()
                  .WithColumn("value").AsString(Int32.MaxValue).Nullable()
                ;
        }

        public override void Down() {
            Delete.Table("configuration_items");
        }
    }
}