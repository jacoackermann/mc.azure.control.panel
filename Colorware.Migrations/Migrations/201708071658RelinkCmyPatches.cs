﻿using Colorware.Migrations.Converters;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201708071658)]
    public class RelinkCmyPatches : Migration {
        public override void Up() {
            Execute.WithConnection((connection, transaction) =>
                                   new RelinkCmyPatchesMigrationHelper().RelinkAll(connection, transaction));
        }

        public override void Down() {

        }
    }
}