using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040147)]
    public class CreateSheetPositionsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("sheet_positions")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("version_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("reference_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("reference_patch_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("x").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("y").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("sheet_positions");
        }
        #endregion
    }
}