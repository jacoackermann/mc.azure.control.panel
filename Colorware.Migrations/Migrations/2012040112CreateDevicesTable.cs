using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040112)]
    public class CreateDevicesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("devices")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("device_type_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("active").AsBoolean().Nullable().WithDefaultValue("1")
                  .WithColumn("serialnr").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("whitecal_ref").AsBinary().Nullable()
                  .WithColumn("whitecal").AsBinary().Nullable()
                  .WithColumn("whitecal_lastdate").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("unical_ref").AsBinary().Nullable()
                  .WithColumn("unical_lastdate").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("slopcal_content").AsBinary().Nullable()
                  .WithColumn("slopecal_lastdate").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("company_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("key").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("devices");
        }
        #endregion
    }
}