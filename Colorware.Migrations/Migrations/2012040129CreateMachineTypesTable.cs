using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040129)]
    public class CreateMachineTypesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("machine_types")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("type_name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("follow_from_ok_sheet").AsBoolean().Nullable().WithDefaultValue("1")
                ;
        }

        public override void Down() {
            Delete.Table("machine_types");
        }
        #endregion
    }
}