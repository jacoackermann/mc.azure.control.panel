using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040138)]
    public class CreatePrintabilitiesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("printabilities")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("job_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("paper_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("ink_set_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("reference_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("factor_c_lch_l").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("factor_c_lch_c").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("factor_c_lch_h").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("factor_m_lch_l").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("factor_m_lch_c").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("factor_m_lch_h").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("factor_y_lch_l").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("factor_y_lch_c").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("factor_y_lch_h").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("factor_k_lch_l").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("factor_k_lch_c").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("factor_k_lch_h").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("factor_c_density").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("factor_m_density").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("factor_y_density").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("factor_k_density").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("c_best_dry_delta_e").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("m_best_dry_delta_e").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("y_best_dry_delta_e").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("k_best_dry_delta_e").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("c_best_wet_delta_e").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("m_best_wet_delta_e").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("y_best_wet_delta_e").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("k_best_wet_delta_e").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("c_best_wet_l").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("c_best_wet_a").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("c_best_wet_b").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("c_best_dry_l").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("c_best_dry_a").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("c_best_dry_b").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("m_best_wet_l").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("m_best_wet_a").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("m_best_wet_b").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("m_best_dry_l").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("m_best_dry_a").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("m_best_dry_b").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("y_best_wet_l").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("y_best_wet_a").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("y_best_wet_b").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("y_best_dry_l").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("y_best_dry_a").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("y_best_dry_b").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("k_best_wet_l").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("k_best_wet_a").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("k_best_wet_b").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("k_best_dry_l").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("k_best_dry_a").AsFloat().Nullable().WithDefaultValue("")
                  .WithColumn("k_best_dry_b").AsFloat().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("printabilities");
        }
        #endregion
    }
}