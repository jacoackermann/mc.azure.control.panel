﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201605261224)]
    public class AddTimestampFieldsToProcessedReportsMeasurementsTable : Migration {
        public override void Up() {
            Create.Column("created_at").OnTable("processed_reports_measurements")
                  .AsDateTime()
                  .WithDefault(SystemMethods.CurrentDateTime)
                  .NotNullable();
            Create.Column("updated_at").OnTable("processed_reports_measurements")
                  .AsDateTime()
                  .WithDefault(SystemMethods.CurrentDateTime)
                  .NotNullable();
        }

        public override void Down() {
            Delete.Column("updated_at").FromTable("processed_reports_measurements");
            Delete.Column("created_at").FromTable("processed_reports_measurements");
        }
    }
}