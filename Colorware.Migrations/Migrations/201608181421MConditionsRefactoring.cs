﻿using Colorware.Core.Enums;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201608181421)]
    public class MConditionsRefactoring : Migration {
        public override void Up() {
            // An 'Undefined' M-condition would be more correct, but we just assume users
            // used to work in M0 (makes it easier for them and us).
            var defaultMCondition = (byte)MCondition.M0;

            Create.Column("spectral_m_condition")
                  .OnTable("job_strip_patches")
                  .AsByte()
                  .NotNullable()
                  .SetExistingRowsTo(defaultMCondition);


            Create.Column("density_m_condition")
                  .OnTable("measurement_conditions")
                  .AsByte()
                  .NotNullable()
                  .SetExistingRowsTo(defaultMCondition);

            Create.Column("spectral_m_condition")
                  .OnTable("measurement_conditions")
                  .AsByte()
                  .NotNullable()
                  .SetExistingRowsTo(defaultMCondition);


            Create.Column("spectral_m_condition")
                  .OnTable("reference_patches")
                  .AsByte()
                  .NotNullable()
                  .SetExistingRowsTo(defaultMCondition);


            Create.Column("spectral_m_condition")
                  .OnTable("samples")
                  .AsByte()
                  .NotNullable()
                  .SetExistingRowsTo(defaultMCondition);


            Create.Column("illuminant")
                  .OnTable("cached_compare_results")
                  .AsByte()
                  .NotNullable()
                // This used to be the implicitly assumed value for Lab in this table
                // (we update it to the correct illuminant with an SQL statement below, if possible)
                  .SetExistingRowsTo((byte)Illuminant.D50);

            Create.Column("observer")
                  .OnTable("cached_compare_results")
                  .AsByte()
                  .NotNullable()
                // This used to be the implicitly assumed value for Lab in this table
                // (we update it to the correct observer with an SQL statement below, if possible)
                  .SetExistingRowsTo((byte)Observer.TwoDeg);

            Create.Column("spectral_m_condition")
                  .OnTable("cached_compare_results")
                  .AsByte()
                  .NotNullable()
                  .SetExistingRowsTo(defaultMCondition);

            Create.Column("conditions_density_m_condition")
                  .OnTable("process_control_statistics")
                  .AsByte()
                  .NotNullable()
                  .SetExistingRowsTo(defaultMCondition);

            Create.Column("conditions_spectral_m_condition")
                  .OnTable("process_control_statistics")
                  .AsByte()
                  .NotNullable()
                  .SetExistingRowsTo(defaultMCondition);

            // Remove pol filter fields from measurement conditions, they're now stored in the
            // m-condition fields instead (M0 = no pol, M3 = pol). (Note that technically no pol
            // could be M0, M1 or M2, but we just assume M0)

            // Update a MeasurementConditionsConfig's M-conditions to reflect the obsolete pol filter settings
            Execute.Sql(@"
UPDATE
   measurement_conditions
SET
   density_m_condition = CASE WHEN use_density_pollfilter = 1 THEN 3 ELSE 0 END,
   spectral_m_condition = CASE WHEN use_spectral_pollfilter = 1 THEN 3 ELSE 0 END");

            // Now remove pol filter fields
            Delete.Column("use_density_pollfilter")
                  .Column("use_spectral_pollfilter")
                  .FromTable("measurement_conditions");

            Delete.Column("conditions_density_polfilter")
                  .Column("conditions_spectral_polfilter")
                  .FromTable("process_control_statistics");

            // Update CachedCompareResult's illuminant/observer/M-condition to the value known in its job's MeasurementCondition
            Execute.Sql(@"
UPDATE
   cached_compare_results
SET
   illuminant = measurement_conditions.illuminant,
   observer = measurement_conditions.observer_angle,
   spectral_m_condition = measurement_conditions.spectral_m_condition
FROM
   cached_compare_results
INNER JOIN jobs ON jobs.id = cached_compare_results.job_id
INNER JOIN measurement_conditions ON measurement_conditions.id = jobs.measurement_condition_id");
        }

        public override void Down() {}
    }
}