﻿using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201507211146)]
    public class AddExtraInfoFieldsToReferencePatch : Migration {
        public override void Up() {
            Create.Column("extra_info_1")
                  .OnTable("reference_patches")
                  .AsString(Int32.MaxValue)
                  .Nullable();

            Create.Column("extra_info_2")
                  .OnTable("reference_patches")
                  .AsString(Int32.MaxValue)
                  .Nullable();
        }

        public override void Down() {
        }
    }
}