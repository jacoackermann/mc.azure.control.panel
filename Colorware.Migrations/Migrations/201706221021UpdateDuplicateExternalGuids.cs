﻿using FluentMigrator;
using FluentMigrator.Expressions;

namespace Colorware.Migrations.Migrations {
    [Migration(201706221021)]
    public class UpdateDuplicateExternalGuids : Migration {
        public override void Up() {
            Execute.EmbeddedScript("UpdateDuplicateExternalGuids.sql");
        }

        public override void Down() {
        }
    }
}