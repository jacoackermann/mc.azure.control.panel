﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201802121602)]
    public class RenameIsInternalServerColumnToSendAllDataUnfiltered : AutoReversingMigration {
        public override void Up() {
            Rename.Column("is_internal_server")
                  .OnTable("external_reports_servers")
                  .To("send_all_data_unfiltered");
        }
    }
}