﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201406261133)]
    public class ChangePantoneLiveClientReferenceGuidFieldType : Migration {
        public override void Up() {
            Delete.Column("pantone_live_client_reference_guid").FromTable("job_presets");
            Create.Column("pantone_live_client_reference_guid").OnTable("job_presets").AsGuid().Nullable();
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}