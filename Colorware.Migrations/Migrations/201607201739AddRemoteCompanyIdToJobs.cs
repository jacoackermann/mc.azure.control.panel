﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201607201739)]
    public class AddRemoteCompanyIdToJobs : Migration {
        public override void Up() {
            Create.Column("remote_company_id")
                  .OnTable("jobs")
                  .AsInt64()
                  .NotNullable()
                  .WithDefaultValue(0);
        }

        public override void Down() {
            Delete.Column("remote_company_id")
                  .FromTable("jobs");
        }
    }
}