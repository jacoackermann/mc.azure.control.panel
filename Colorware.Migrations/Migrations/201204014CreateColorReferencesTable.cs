using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201204014)]
    public class CreateColorReferencesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("color_references")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("is_spot_lib").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("company_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("shared").AsBoolean().Nullable().WithDefaultValue("")
                  .WithColumn("client_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("default_dotgain_list_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("illuminant").AsInt32().Nullable().WithDefaultValue("0")
                  .WithColumn("observer").AsInt32().Nullable().WithDefaultValue("0")
                ;
        }

        public override void Down() {
            Delete.Table("color_references");
        }
        #endregion
    }
}