﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201611151118)]
    public class CreateTableGrayFinder : Migration {
        public override void Up() {
            Create.Table("grayfinder_patches")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("reference_patch_id").AsInt64().NotNullable()
                  .WithColumn("data").AsBinary().NotNullable();
            
            Execute.EmbeddedScript("AddAccessTables_GrayFinder.sql");
        }

        public override void Down() {
            Delete.Table("grayfinder_patches");
        }
    }
}