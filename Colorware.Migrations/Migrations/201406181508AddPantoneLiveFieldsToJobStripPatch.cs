﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201406181508)]
    public class AddPantoneLiveFieldsToJobStripPatch : Migration {
        public override void Up() {
            Create.Column("pantone_live_dependent_standard_guid").OnTable("job_strip_patches").AsGuid().Nullable();
            Create.Column("pantone_live_palette_guid").OnTable("job_strip_patches").AsGuid().Nullable();
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}