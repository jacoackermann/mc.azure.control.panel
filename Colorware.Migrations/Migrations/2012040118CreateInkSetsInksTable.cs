using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040118)]
    public class CreateInkSetsInksTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("ink_sets_inks")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("ink_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("ink_set_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                ;
        }

        public override void Down() {
            Delete.Table("ink_sets_inks");
        }
        #endregion
    }
}