﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201212181628)]
    public class DeleteLanguageTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Delete.Table("languages");
        }

        public override void Down() {
        }
        #endregion
    }
}