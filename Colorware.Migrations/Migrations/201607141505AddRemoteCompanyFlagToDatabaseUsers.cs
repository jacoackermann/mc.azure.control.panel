﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201607141505)]
    public class AddRemoteCompanyFlagToCompanies : Migration {
        public override void Up() {
            Create.Column("is_remote_company")
                  .OnTable("companies")
                  .AsBoolean()
                  .WithDefaultValue(false)
                  .NotNullable();
        }

        public override void Down() {
            Delete.Column("is_remote_company")
                  .FromTable("companies");
        }
    }
}