﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201405071042)]
    public class AddCountryToCompany : Migration {
        public override void Up() {
            Create.Column("country").OnTable("companies").AsString(150).WithDefaultValue(string.Empty);
        }

        public override void Down() {
            Delete.Column("country").FromTable("companies");
        }
    }
}