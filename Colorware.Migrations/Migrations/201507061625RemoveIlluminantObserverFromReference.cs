﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201507061625)]
    public class RemoveIlluminantObserverFromReference : Migration {
        public override void Up() {
            // Remove Illuminant and Observer from Reference, they weren't used anyway.
            // We're going to store Ill/Obs per reference patch
            Delete.Column("illuminant")
                  .FromTable("color_references");

            Delete.Column("observer")
                  .FromTable("color_references");
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}