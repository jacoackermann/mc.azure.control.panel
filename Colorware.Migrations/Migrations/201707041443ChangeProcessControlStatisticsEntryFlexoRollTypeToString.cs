using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201707041443)]
    public class ChangeProcessControlStatisticsEntryFlexoRollTypeToString : Migration {
        public override void Up() {
            Delete.DefaultConstraint().OnTable("process_control_statistics").OnColumn("flexo_roll");

            // Used to be 'int', make it 'varchar(255)' to match Measurement.FlexoRoll type
            Alter.Table("process_control_statistics")
                 .AlterColumn("flexo_roll")
                 .AsString(255)
                 .Nullable()
                 .WithDefaultValue("");
        }

        public override void Down() { }
    }
}