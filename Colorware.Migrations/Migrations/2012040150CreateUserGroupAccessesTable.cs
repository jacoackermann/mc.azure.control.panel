using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040150)]
    public class CreateUserGroupAccessesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("user_group_accesses")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("user_group_section_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("user_group_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("read").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("read_owned_only").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("write").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("write_owned_only").AsBoolean().Nullable().WithDefaultValue("0")
                ;
        }

        public override void Down() {
            Delete.Table("user_group_accesses");
        }
        #endregion
    }
}