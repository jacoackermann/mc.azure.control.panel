using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040152)]
    public class CreateUserGroupsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("user_groups")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("description").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("admin").AsBoolean().Nullable().WithDefaultValue("0")
                ;
        }

        public override void Down() {
            Delete.Table("user_groups");
        }
        #endregion
    }
}