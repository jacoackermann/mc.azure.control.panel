using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040125)]
    public class CreateJobStripsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("job_strips")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("job_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("color_strip_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("is_customized").AsBoolean().Nullable().WithDefaultValue("0")
                  .WithColumn("is_reset").AsBoolean().Nullable().WithDefaultValue("0")
                ;
        }

        public override void Down() {
            Delete.Table("job_strips");
        }
        #endregion
    }
}