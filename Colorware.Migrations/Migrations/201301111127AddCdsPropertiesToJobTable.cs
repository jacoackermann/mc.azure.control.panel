﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201301111127)]
    public class AddCdsPropertiesToJobTable : Migration {
        public override void Up() {
            Create.Column("brand").OnTable("jobs").AsString(255).Nullable().WithDefaultValue(string.Empty);
            Create.Column("printer_location").OnTable("jobs").AsString(255).Nullable().WithDefaultValue(string.Empty);
            Create.Column("printer_name").OnTable("jobs").AsString(255).Nullable().WithDefaultValue(string.Empty);
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}