using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040127)]
    public class CreateLanguagesTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("languages")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("file").AsString(255).Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("languages");
        }
        #endregion
    }
}