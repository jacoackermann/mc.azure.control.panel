﻿using FluentMigrator;

using JetBrains.Annotations;

namespace Colorware.Migrations.Migrations {
    [Migration(201801191511), UsedImplicitly]
    public class UpdateGrayPatchAccessList : Migration {
        public override void Up() {
            Execute.EmbeddedScript("AddAccessTables_GrayFinder.sql");
        }

        public override void Down() {
        }
    }
}