﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    // Remove Reference from PaperType: relation is being changed to
    // Reference   *---1   PaperType
    // (for new job setup and reference editor)
    [Migration(201507201735)]
    public class RemoveReferenceFromPaperType : Migration {
        public override void Up() {
            Delete.Index("IX_paper_types_color_reference_id").OnTable("paper_types");

            Delete.Column("color_reference_id")
                  .FromTable("paper_types");
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}