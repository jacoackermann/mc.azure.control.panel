﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201612191450)]
    public class AddAutomaticDensityAdjustment : AutoReversingMigration {
        public override void Up() {
            Create.Column("use_automatic_density_adjustment").OnTable("jobs")
                  .AsBoolean().NotNullable().WithDefaultValue("1");
        }
    }
}