using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201702131952)]
    public class AddOpacityAccessList : Migration {
        public override void Up() {
            Execute.EmbeddedScript("AddAccessTableForOpacity.sql");
        }

        public override void Down() {}
    }
}