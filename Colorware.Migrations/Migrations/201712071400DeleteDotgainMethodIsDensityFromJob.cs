﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    /// <summary>
    /// This was supposed to be done when the respective property was removed for PV-2167.
    /// </summary>
    [Migration(201712071400)]
    public class DeleteDotgainMethodIsDensityFromJob : Migration {
        public override void Up() {
            Delete.Column("dotgain_method_is_density")
                  .FromTable("jobs");
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}