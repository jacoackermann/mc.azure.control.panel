﻿using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201710171704)]
    public class AddTolerancePerColorToReferencePatchAndJobStripPatch : AutoReversingMigration {
        public override void Up() {
            Create.Column("use_tolerance_delta_e_override")
                  .OnTable("reference_patches")
                  .AsBoolean()
                  .NotNullable()
                  .WithDefaultValue(false)
                  .WithColumnDescription("Determines if a reference patch has a delta-e tolerance override.");
            Create.Column("tolerance_delta_e_override")
                  .OnTable("reference_patches")
                  .AsDouble()
                  .NotNullable()
                  .WithDefaultValue(0.0);

            Create.Column("use_tolerance_delta_e_override")
                  .OnTable("job_strip_patches")
                  .AsBoolean()
                  .NotNullable()
                  .WithDefaultValue(false)
                  .WithColumnDescription("Determines if a reference patch has a delta-e tolerance override.");
            Create.Column("tolerance_delta_e_override")
                  .OnTable("job_strip_patches")
                  .AsDouble()
                  .NotNullable()
                  .WithDefaultValue(0.0);
        }
    }
}