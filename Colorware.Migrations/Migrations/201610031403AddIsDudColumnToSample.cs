﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201510031403)]
    public class AddIsDudColumnToSample : Migration {
        public override void Up() {
            Create.Column("is_dud")
                  .OnTable("samples")
                  .AsBoolean()
                  .NotNullable()
                  .WithDefaultValue(false);
        }

        public override void Down() {
            throw new System.NotImplementedException();
        }
    }
}