using System;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(2012040121)]
    public class CreateJobGroupsTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Table("job_groups")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("client_id").AsInt64().Nullable().WithDefaultValue("").Indexed()
                  .WithColumn("job_nr").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("name").AsString(255).Nullable().WithDefaultValue("")
                  .WithColumn("description").AsString(Int32.MaxValue).Nullable().WithDefaultValue("")
                  .WithColumn("created_at").AsDateTime().Nullable().WithDefaultValue("")
                  .WithColumn("updated_at").AsDateTime().Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("job_groups");
        }
        #endregion
    }
}