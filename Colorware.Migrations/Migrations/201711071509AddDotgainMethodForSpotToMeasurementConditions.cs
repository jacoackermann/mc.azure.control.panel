﻿using Colorware.Core.Enums;

using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201711071509)]
    public class AddDotgainMethodForSpotToMeasurementConditions : Migration {
        public override void Up() {
            Rename.Column("dotgain_method")
                  .OnTable("measurement_conditions")
                  .To("dotgain_method_process");

            Create.Column("dotgain_method_spot")
                  .OnTable("measurement_conditions")
                  .AsInt32()
                  .WithDefaultValue((int)DotgainMethod.Density);

            // Initialize the settings for dotgain_method_spot to correspond to the current settings
            // for dotgain_method_process:
            Execute.Sql("UPDATE measurement_conditions SET dotgain_method_spot=dotgain_method_process");
        }

        public override void Down() {
            Delete.Column("dotgain_method_spot")
                  .FromTable("measurement_conditions");
            Rename.Column("dotgain_method_process")
                  .OnTable("measurement_conditions")
                  .To("dotgain_method");
        }
    }
}