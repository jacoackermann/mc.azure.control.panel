﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201611031104)]
    public class ParseJobImageNameToGuid : Migration {
        public override void Up() {
            Execute.EmbeddedScript("ImageNameToGuid.sql");
            Execute.Sql(@"
                IF EXISTS (SELECT * 
                  FROM sys.default_constraints 
                   WHERE object_id = OBJECT_ID('DF_jobs_image_name')
                   AND parent_object_id = OBJECT_ID('jobs')
                )
				   ALTER TABLE [dbo].[jobs] DROP CONSTRAINT [DF_jobs_image_name]
            ");

            Alter.Table("jobs")
                 .AlterColumn("image_name")
                 .AsGuid()
                 .NotNullable();

            Execute.Sql("ALTER TABLE [dbo].[jobs] ADD  CONSTRAINT [DF_jobs_image_name]  DEFAULT ('') FOR [image_name]");
        }

        public override void Down() {}
    }
}