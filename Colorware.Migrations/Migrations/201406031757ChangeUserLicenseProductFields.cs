﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201406031757)]
    public class ChangeUserLicenseProductFields : Migration {
        public override void Up() {
            Create.Column("forced_license_product")
                  .OnTable("database_users")
                  .AsGuid()
                  .Nullable();
        }

        public override void Down() {
        }
    }
}