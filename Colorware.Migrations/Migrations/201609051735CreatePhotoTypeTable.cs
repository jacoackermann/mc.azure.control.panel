﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {

    [Migration(201609051735)]
    public class CreatePhotoTypeTable : AutoReversingMigration {

        public override void Up() {
            Create.Table("phototype_entries")
                  .WithColumn("id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("job_id").AsInt64().NotNullable()
                  .WithColumn("measurement_id").AsInt64().NotNullable()
                  .WithColumn("date_received").AsDateTime().NotNullable()
                  .WithColumn("date_scheduled").AsDateTime().NotNullable()
                  .WithColumn("pressrun_date").AsDateTime().NotNullable()
                  .WithColumn("print_lot_number").AsString().NotNullable()
                  .WithColumn("print_plant_location").AsString(256).NotNullable()
                  .WithColumn("pressrun_completed").AsBoolean().NotNullable().WithDefaultValue(false)
                  .WithColumn("piece_of_art_name").AsString(256).NotNullable().WithDefaultValue(string.Empty)
                  .WithColumn("comments").AsString(int.MaxValue).Nullable().WithDefaultValue(string.Empty)
                  .WithColumn("completed_date").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime)
                  .WithColumn("press_sheet_identifier").AsString(256).NotNullable()
                  .WithColumn("printed_date").AsDateTime().NotNullable()
                  .WithColumn("printer_roll_number").AsString().Nullable().WithDefaultValue(string.Empty)
                  .WithColumn("is_production").AsBoolean().NotNullable().WithDefaultValue(false)
                  .WithColumn("upc").AsInt32().NotNullable()
                  .WithColumn("vertical_registration").AsDouble().NotNullable().WithDefaultValue(0.0)
                  .WithColumn("horizontal_registration").AsDouble().NotNullable().WithDefaultValue(0.0)
                  .WithColumn("visual_match").AsInt32().NotNullable()
                  .WithColumn("defect_class_1").AsInt32().Nullable()
                  .WithColumn("defect_class_2").AsInt32().Nullable()
                  .WithColumn("defect_class_3").AsInt32().Nullable()
                  .WithColumn("defect_class_4").AsInt32().Nullable()
                ;
        }
    }
}