﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201301291102)]
    public class AddTransmitDateColumnsToEpqTable : Migration {
        #region Overrides of MigrationBase
        public override void Up() {
            Create.Column("last_transmit_date").OnTable("epq_scores").AsDateTime().Nullable();
            Create.Column("was_transmitted").OnTable("epq_scores").AsBoolean().NotNullable().WithDefaultValue(false);
        }

        public override void Down() {
            Delete.Column("last_transmit_date").FromTable("epq_scores");
            Delete.Column("was_transmitted").FromTable("epq_scores");
        }
        #endregion
    }
}