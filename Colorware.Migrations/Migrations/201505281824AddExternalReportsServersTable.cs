using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201505281824)]
    public class AddExternalReportsServersTable : Migration {
        public override void Up() {
            Create.Table("external_reports_servers")
                  .WithColumn("id")          .AsInt64()     .Identity().PrimaryKey()
                  .WithColumn("name")        .AsString(255) .Nullable().WithDefaultValue("")
                  .WithColumn("url")         .AsString(1024).Nullable().WithDefaultValue("")
                  .WithColumn("send_enabled").AsBoolean()   .Nullable().WithDefaultValue("")
                ;
        }

        public override void Down() {
            Delete.Table("external_reports_servers");
        }
    }
}
