﻿using FluentMigrator;

namespace Colorware.Migrations.Migrations {
    [Migration(201711081626)]
    public class AddIsSpotToJobStripPatch : AutoReversingMigration {
        public override void Up() {
            Create.Column("is_spot")
                  .OnTable("job_strip_patches")
                  .AsBoolean()
                  .WithDefaultValue(false); // For existing jobs, we threat all items as process.
        }
    }
}