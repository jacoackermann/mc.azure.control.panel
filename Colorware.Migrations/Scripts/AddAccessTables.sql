﻿-- NOTE: The company.id of the current user will be inserted via the CONTEXT_INFO().
--
-- DECLARE @userID varbinary(128) = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO()))
-- SET CONTEXT_INFO @userId
-- GO

IF OBJECT_ID('companies_access', 'V') IS NOT NULL
    DROP VIEW companies_access
GO
CREATE VIEW companies_access AS
    SELECT DISTINCT(companies.id) AS allowed_id FROM companies
    WHERE companies.id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO()))
GO

IF OBJECT_ID('database_users_access', 'V') IS NOT NULL
    DROP VIEW database_users_access
GO
CREATE VIEW database_users_access AS
    SELECT DISTINCT(database_users.id) as allowed_id FROM database_users
    WHERE database_users.company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO()))
GO

IF OBJECT_ID('clients_access', 'V') IS NOT NULL
    DROP VIEW clients_access
GO
CREATE VIEW clients_access AS
    SELECT DISTINCT(clients.id) AS allowed_id FROM clients
    INNER JOIN jobs ON jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.client_id = clients.id
GO

IF OBJECT_ID('client_external_reports_servers_access', 'V') IS NOT NULL
    DROP VIEW client_external_reports_servers_access
GO
CREATE VIEW client_external_reports_servers_access AS
    SELECT DISTINCT(client_external_reports_servers.id) AS allowed_id FROM client_external_reports_servers
    INNER JOIN jobs ON jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.client_id = client_external_reports_servers.client_id
GO

IF OBJECT_ID('machines_access', 'V') IS NOT NULL
    DROP VIEW machines_access
GO
CREATE VIEW machines_access AS
    SELECT DISTINCT(machines.id) AS allowed_id FROM machines
    INNER JOIN jobs ON jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.machine_id = machines.id
GO

IF OBJECT_ID('jobs_access', 'V') IS NOT NULL
    DROP VIEW jobs_access
GO
CREATE VIEW jobs_access AS
    SELECT DISTINCT(id) AS allowed_id FROM jobs
    WHERE jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO()))
GO

IF OBJECT_ID('job_ink_infos_access', 'V') IS NOT NULL
    DROP VIEW job_ink_infos_access
GO
CREATE VIEW job_ink_infos_access AS
    SELECT DISTINCT(job_ink_infos.id) AS allowed_id FROM job_ink_infos
    INNER JOIN jobs ON jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.id = job_ink_infos.job_id
GO

IF OBJECT_ID('measurements_access', 'V') IS NOT NULL
    DROP VIEW measurements_access
GO
CREATE VIEW measurements_access AS
    SELECT DISTINCT(measurements.id) AS allowed_id FROM measurements
    INNER JOIN jobs ON jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.id = measurements.job_id
GO

IF OBJECT_ID('samples_access', 'V') IS NOT NULL
    DROP VIEW samples_access
GO
CREATE VIEW samples_access AS
    SELECT DISTINCT(samples.id) AS allowed_id FROM samples
    INNER JOIN measurements ON measurements.id = samples.measurement_id
    INNER JOIN jobs ON jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.id = measurements.job_id
GO

IF OBJECT_ID('measurement_custom_fields_configuration_items_access', 'V') IS NOT NULL
    DROP VIEW measurement_custom_fields_configuration_items_access
GO
CREATE VIEW measurement_custom_fields_configuration_items_access AS
    SELECT id AS allowed_id FROM measurement_custom_fields_configuration_items
GO

IF OBJECT_ID('papers_access', 'V') IS NOT NULL
    DROP VIEW papers_access
GO
CREATE VIEW papers_access AS
    SELECT DISTINCT(papers.id) as allowed_id FROM papers
    INNER JOIN jobs ON jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.paper_id = papers.id
GO

IF OBJECT_ID('paper_types_access', 'V') IS NOT NULL
    DROP VIEW paper_types_access
GO
CREATE VIEW paper_types_access AS
    SELECT DISTINCT(paper_types.id) as allowed_id from paper_types
    INNER JOIN papers on papers.paper_type_id = paper_types.id
    INNER JOIN jobs on jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.paper_id = papers.id
GO

IF OBJECT_ID('inks_access', 'V') IS NOT NULL
    DROP VIEW inks_access
GO
CREATE VIEW inks_access AS
    SELECT DISTINCT(inks.id) as allowed_id FROM inks
    INNER JOIN ink_sets ON ink_sets.id = inks.ink_set_id
    INNER JOIN jobs ON jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.ink_set_id = ink_sets.id
GO

IF OBJECT_ID('ink_sets_access', 'V') IS NOT NULL
    DROP VIEW ink_sets_access
GO
CREATE VIEW ink_sets_access AS
    SELECT DISTINCT(ink_sets.id) as allowed_id FROM ink_sets
    INNER JOIN jobs ON jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.ink_set_id = ink_sets.id
GO

IF OBJECT_ID('cached_compare_results_access', 'V') IS NOT NULL
    DROP VIEW cached_compare_results_access
GO
CREATE VIEW cached_compare_results_access AS
    SELECT DISTINCT(cached_compare_results.id) as allowed_id FROM cached_compare_results
    INNER JOIN jobs ON jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.id = cached_compare_results.job_id
GO

IF OBJECT_ID('job_strips_access', 'V') IS NOT NULL
    DROP VIEW job_strips_access
GO
CREATE VIEW job_strips_access AS
    SELECT DISTINCT(job_strips.id) AS allowed_id FROM job_strips
    INNER JOIN jobs ON jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.job_strip_id = job_strips.id
GO

IF OBJECT_ID('job_strip_patches_access', 'V') IS NOT NULL
    DROP VIEW job_strip_patches_access
GO
CREATE VIEW job_strip_patches_access AS
    SELECT DISTINCT(job_strip_patches.id) AS allowed_id FROM job_strip_patches
    INNER JOIN job_strips on job_strips.id = job_strip_patches.job_strip_id
    INNER JOIN jobs ON jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.job_strip_id = job_strips.id
GO

IF OBJECT_ID('tolerance_sets_access', 'V') IS NOT NULL
    DROP VIEW tolerance_sets_access
GO
CREATE VIEW tolerance_sets_access AS
    SELECT DISTINCT(tolerance_sets.id) AS allowed_id FROM tolerance_sets
    INNER JOIN jobs ON jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.tolerance_set_id = tolerance_sets.id
GO

IF OBJECT_ID('measurement_conditions_access', 'V') IS NOT NULL
    DROP VIEW measurement_conditions_access
GO
CREATE VIEW measurement_conditions_access AS
    SELECT DISTINCT(measurement_conditions.id) AS allowed_id FROM measurement_conditions
    INNER JOIN jobs ON jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.measurement_condition_id = measurement_conditions.id
GO

IF OBJECT_ID('color_strips_access', 'V') IS NOT NULL
    DROP VIEW color_strips_access
GO
CREATE VIEW color_strips_access AS
    SELECT DISTINCT(color_strips.id) AS allowed_id FROM color_strips
    INNER JOIN jobs ON jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.color_strip_id = color_strips.id
GO

IF OBJECT_ID('color_strip_patches_access', 'V') IS NOT NULL
    DROP VIEW color_strip_patches_access
GO
CREATE VIEW color_strip_patches_access AS
    SELECT DISTINCT(color_strip_patches.id) AS allowed_id FROM color_strip_patches
    INNER JOIN color_strips ON color_strips.id = color_strip_patches.color_strip_id
    INNER JOIN jobs ON jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.color_strip_id = color_strips.id
GO

IF OBJECT_ID('color_references_access', 'V') IS NOT NULL
    DROP VIEW color_references_access
GO
CREATE VIEW color_references_access AS
    SELECT DISTINCT(color_references.id) AS allowed_id FROM color_references
    INNER JOIN reference_patches ON reference_patches.reference_id = color_references.id
    INNER JOIN job_strip_patches ON job_strip_patches.reference_patch_id = reference_patches.id
    INNER JOIN job_strips ON job_strips.id = job_strip_patches.job_strip_id
    INNER JOIN jobs ON jobs.job_strip_id = job_strips.id AND jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO()))
GO

IF OBJECT_ID('reference_patches_access', 'V') IS NOT NULL
    DROP VIEW reference_patches_access
GO
CREATE VIEW reference_patches_access AS
    SELECT DISTINCT(reference_patches.id) AS allowed_id FROM reference_patches
    INNER JOIN job_strip_patches ON job_strip_patches.reference_patch_id = reference_patches.id
    INNER JOIN job_strips ON job_strips.id = job_strip_patches.job_strip_id
    INNER JOIN jobs ON jobs.job_strip_id = job_strips.id AND jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO()))
GO

IF OBJECT_ID('color_mappings_access', 'V') IS NOT NULL
    DROP VIEW color_mappings_access
GO
CREATE VIEW color_mappings_access AS
    SELECT color_mappings.id AS allowed_id FROM color_mappings
GO

IF OBJECT_ID('default_dotgain_lists_access', 'V') IS NOT NULL
    DROP VIEW default_dotgain_lists_access
GO
CREATE VIEW default_dotgain_lists_access AS
    SELECT default_dotgain_lists.id as allowed_id FROM default_dotgain_lists
GO

IF OBJECT_ID('default_dotgain_entries_access', 'V') IS NOT NULL
    DROP VIEW default_dotgain_entries_access
GO
CREATE VIEW default_dotgain_entries_access AS
    SELECT default_dotgain_entries.id FROM default_dotgain_entries
GO
