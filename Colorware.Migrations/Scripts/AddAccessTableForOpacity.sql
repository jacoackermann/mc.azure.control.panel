﻿
IF OBJECT_ID('opacity_results_access', 'V') IS NOT NULL
    DROP VIEW opacity_results_access
GO
CREATE VIEW opacity_results_access AS
    SELECT DISTINCT(opacity_results.id) as allowed_id FROM opacity_results
    INNER JOIN measurements ON measurements.id = opacity_results.measurement_id
    INNER JOIN jobs ON jobs.print_location_company_id = CONVERT(bigint, CONVERT(varbinary(8), CONTEXT_INFO())) AND jobs.id = measurements.job_id
GO