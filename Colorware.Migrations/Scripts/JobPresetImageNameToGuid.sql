﻿UPDATE job_presets SET image_name = CASE WHEN job_presets.[image_name] IS NULL OR job_presets.[image_name] = ''   
	THEN CAST(CAST(0 as binary) as UNIQUEIDENTIFIER)  
	ELSE CONVERT(UNIQUEIDENTIFIER, job_presets.[image_name])
END