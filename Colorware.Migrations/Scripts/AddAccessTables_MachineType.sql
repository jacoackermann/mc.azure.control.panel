﻿IF OBJECT_ID('machine_types_access', 'V') IS NOT NULL
    DROP VIEW machine_types_access
GO
CREATE VIEW machine_types_access AS
    SELECT DISTINCT(machine_types.id) AS allowed_id FROM machine_types
GO