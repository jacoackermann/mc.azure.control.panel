﻿IF OBJECT_ID('grayfinder_patches_access', 'V') IS NOT NULL
    DROP VIEW grayfinder_patches_access
GO
CREATE VIEW grayfinder_patches_access AS
    SELECT DISTINCT(grayfinder_patches.id) AS allowed_id FROM grayfinder_patches
GO