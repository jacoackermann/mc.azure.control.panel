﻿
-- Jobs
WITH CTE AS (
	SELECT ROW_NUMBER() OVER (PARTITION BY external_guid ORDER BY id) as rn, id, external_guid FROM jobs
)
UPDATE CTE SET external_guid = NEWID() WHERE rn > 1
GO

-- Measurements
WITH CTE AS (
	SELECT ROW_NUMBER() OVER (PARTITION BY external_guid ORDER BY id) as rn, id, external_guid FROM measurements
)
UPDATE CTE SET external_guid = NEWID() WHERE rn > 1
GO

-- References
WITH CTE AS (
	SELECT ROW_NUMBER() OVER (PARTITION BY external_guid ORDER BY id) as rn, id, external_guid FROM color_references
)
UPDATE CTE SET external_guid = NEWID() WHERE rn > 1
GO

-- Reference patches
WITH CTE AS (
	SELECT ROW_NUMBER() OVER (PARTITION BY external_guid ORDER BY id) as rn, id, external_guid FROM reference_patches
)
UPDATE CTE SET external_guid = NEWID() WHERE rn > 1
GO
