﻿ALTER TRIGGER updateJobsCachedScoreInsertUpdate
ON measurements
AFTER INSERT, UPDATE
AS
BEGIN
    UPDATE jobs
    SET jobs.cached_score = (SELECT AVG(CASE WHEN ok_sheet > 0 THEN 100 ELSE cached_score END)
                             FROM measurements
							 WITH (NOLOCK)
                             WHERE job_id IN (SELECT job_id FROM inserted) AND (is_production_mode = '1' OR ok_sheet = '1'))
    WHERE id IN (SELECT job_id FROM inserted)
END
GO

ALTER TRIGGER updateJobsCachedScoreDelete
ON measurements
AFTER DELETE
AS
BEGIN
    UPDATE jobs
    SET jobs.cached_score = (SELECT AVG(CASE WHEN ok_sheet > 0 THEN 100 ELSE cached_score END)
                             FROM measurements
							 WITH (NOLOCK)
                             WHERE job_id IN (SELECT job_id FROM deleted) AND (is_production_mode = '1' OR ok_sheet = '1'))
    WHERE id IN (SELECT job_id FROM deleted)
END
GO