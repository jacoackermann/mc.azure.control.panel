﻿UPDATE color_strip_patches
SET ink_zone = ink_zone + 1
WHERE color_strip_patches.color_strip_id in
	(SELECT color_strips.id from color_strip_patches
		LEFT JOIN color_strips ON color_strips.id = color_strip_patches.color_strip_id
		WHERE color_strip_patches.ink_zone = 0
		GROUP BY color_strips.id)