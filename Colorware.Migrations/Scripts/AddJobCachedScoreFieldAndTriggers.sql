﻿ALTER TABLE jobs ADD cached_score FLOAT
GO

CREATE TRIGGER updateJobsCachedScoreInsertUpdate
ON measurements
AFTER INSERT, UPDATE
AS
BEGIN
    UPDATE jobs
    SET jobs.cached_score = (SELECT AVG(cached_score)
                             FROM measurements
                             WHERE job_id IN (SELECT job_id FROM inserted) AND is_production_mode = '1')
    WHERE id IN (SELECT job_id FROM inserted)
END
GO

CREATE TRIGGER updateJobsCachedScoreDelete
ON measurements
AFTER DELETE
AS
BEGIN
    UPDATE jobs
    SET jobs.cached_score = (SELECT AVG(cached_score)
                             FROM measurements
                             WHERE job_id IN (SELECT job_id FROM deleted) AND is_production_mode = '1')
    WHERE id IN (SELECT job_id FROM deleted)
END
GO

-- Initial score calculations
UPDATE jobs
SET jobs.cached_score = (SELECT AVG(cached_score)
                         FROM measurements
                         WHERE job_id = jobs.id AND is_production_mode = '1');
GO