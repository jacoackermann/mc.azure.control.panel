﻿BEGIN TRANSACTION AddHPIndigoCurve;
BEGIN TRY
    -- Insert parent entry.
    INSERT INTO default_dotgain_lists
     (name, updated_at)
    VALUES
     ('HPIndigo EXP05 curve', GETDATE());

    -- Get ID of inserted parent item.	
    DECLARE @parentId BigInt;
    SET @parentId = (SELECT SCOPE_IDENTITY());
    
    -- Insert child entries.
    INSERT INTO default_dotgain_entries
        (default_dotgain_list_id, percentage, dotgain)
    VALUES
        (@parentId, 0, 0),
        (@parentId, 5, 3.2), 
        (@parentId, 10, 5.9), 
        (@parentId, 15, 8.2), 
        (@parentId, 20, 10.1), 
        (@parentId, 25, 11.6), 
        (@parentId, 30, 12.7), 
        (@parentId, 35, 13.5), 
        (@parentId, 40, 14), 
        (@parentId, 45, 14.1), 
        (@parentId, 50, 14), 
        (@parentId, 55, 13.6), 
        (@parentId, 60, 13), 
        (@parentId, 65, 12.1), 
        (@parentId, 70, 10.9), 
        (@parentId, 75, 9.6), 
        (@parentId, 80, 8), 
        (@parentId, 85, 6.3), 
        (@parentId, 90, 4.4), 
        (@parentId, 95, 2.3), 
        (@parentId, 100, 0);

    -- Commit transaction.
    PRINT('Committing transaction AddHPIndigoCurve');
    COMMIT TRANSACTION AddHPIndigoCurve;
END TRY
BEGIN CATCH
    -- Something went wrong, rollback transaction.
    PRINT('Rolling back transaction AddHPIndigoCurve');
    ROLLBACK TRANSACTION AddHPIndigoCurve;
    -- Rethrow the error. In SQL Server 2012 this can probably be replaced with a single THROW keyword.
    declare @ErrorMessage nvarchar(max), @ErrorSeverity int, @ErrorState int;
    select @ErrorMessage = ERROR_MESSAGE() + ' Line ' + cast(ERROR_LINE() as nvarchar(5)), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();
    raiserror (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
GO
