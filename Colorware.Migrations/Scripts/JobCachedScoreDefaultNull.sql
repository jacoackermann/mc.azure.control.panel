﻿-- This forces new jobs to have NULL as a score instead of 0,
-- which allows us to display '--' in the client for unscored jobs.
--
-- A better solution would be to support nullable data types in
-- the data exchange system, but this works for now.
CREATE TRIGGER updateNewJobSetCachedScoreNull
ON jobs
AFTER INSERT
AS
BEGIN
    UPDATE jobs
    SET cached_score = NULL
	WHERE id IN (SELECT id FROM inserted)
END
GO