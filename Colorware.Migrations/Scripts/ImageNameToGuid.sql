﻿UPDATE jobs SET image_name = CASE WHEN jobs.[image_name] IS NULL OR jobs.[image_name] = ''   
	THEN CAST(CAST(0 as binary) as UNIQUEIDENTIFIER)  
	ELSE CONVERT(UNIQUEIDENTIFIER, jobs.[image_name])
END