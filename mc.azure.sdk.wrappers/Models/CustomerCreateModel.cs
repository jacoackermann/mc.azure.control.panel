﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

using mc.azure.utilities.Azure;

namespace mc.azure.sdk.wrappers.Models {
    public class BasicCustomerModel {
        [DisplayName("Customer Name")]
        [Required(ErrorMessage = "Customer Name cannot be blank.")]
        [StringLength(60, MinimumLength = 1, ErrorMessage = "Customer name is too long")]
        public string Name { get; set; }

        [DisplayName("Customer Location (Optional)")]
        [StringLength(60, MinimumLength = 0, ErrorMessage = "Customer location is too long")]
        public string Location { get; set; }
    }

    //=========================================================================

    public class CustomerCreateModel {
        [DisplayName("New Customer?")]
        public bool IsNewCustomer { get; set; }

        public BasicCustomerModel CustomerNameLocation { get; set; }

        [EmailAddress]
        [DisplayName("Email Recipients")]
        public string EmailRecipients { get; set; }

        [Required]
        [EmailAddress]
        [DisplayName("Support Email Recipients")]
        public string SupportEmailRecipients { get; set; }

        public bool DbBackupAvailable { get; set; }

        [DisplayName("Recover DB From Backup")]
        public bool ShouldRecoverDbFromBackup { get; set; }

        // not for display
        public string SanitizedCustomerName => NameSanitizer.GetSanitizedName(CustomerNameLocation.Name);

        public string SanitizedCustomerLocation =>
            NameSanitizer.GetSanitizedName(CustomerNameLocation.Location);

        public string CustomerFullName =>
            $"{CustomerNameLocation.Name?.ToLower()}{(string.IsNullOrWhiteSpace(CustomerNameLocation.Location) ? "" : "-")}{CustomerNameLocation.Location?.ToLower()}";

        //---------------------------------------------------------------------

        public CustomerCreateModel() {
            CustomerNameLocation = new BasicCustomerModel();
        }

        public CustomerCreateModel(BasicCustomerModel basicCustomerModel) {
            CustomerNameLocation = basicCustomerModel;
        }
    }
}