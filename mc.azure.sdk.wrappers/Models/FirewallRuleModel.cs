﻿using System.Collections.Generic;
using System.ComponentModel;

using mc.azure.utilities.Azure;

using Microsoft.Azure.Management.ResourceManager.Fluent.Core;

namespace mc.azure.sdk.wrappers.Models {
    public class FirewallRuleCollectionModel {
        public List<FirewallRuleModel> ExistingFirewallRules { get; set; }

        [DisplayName("New Firewall Rule Name")]
        public string NewRuleName { get; set; }

        [DisplayName("New Firewall Rule IP")]
        public string NewRuleIp { get; set; }

        public string CurrentClientIp { get; set; }

        public string Feedback { get; set; }

        public string NewRuleNameSanitized => NameSanitizer.GetSanitizedName(NewRuleName);
    }

    public class FirewallRuleModel {
        public string SqlServerName { get; set; }
        public string Kind { get; set; }
        public string StartIpAddress { get; set; }
        public string EndIpAddress { get; set; }
        public Region Region { get; set; }
    }
}