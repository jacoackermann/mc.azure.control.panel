﻿using System.Collections.Generic;

namespace mc.azure.sdk.wrappers.Models {
    public class CustomerViewModel {
        public List<CustomerInfoModel> CustomerInfos { get; set; }
    }
}