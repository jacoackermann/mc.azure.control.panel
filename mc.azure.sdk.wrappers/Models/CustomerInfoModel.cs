﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

using Colorware.Core.Azure.Config;
using Colorware.Core.Azure.Enums;
using Colorware.Core.Azure.Storage.TableStorage.CustomerInfo;

using mc.azure.utilities.Azure;

namespace mc.azure.sdk.wrappers.Models {
    public class BaseCustomerInfoModel {
        private string customerName;
        private string customerLocation;
        private readonly string server;
        private readonly string domain;
        private string databaseName;

        // contained database user: user name
        private readonly string containedUserName;

        // contained database user: password
        private readonly string containedUserPassword;

        //---------------------------------------------------------------------

        [DisplayName("Customer Name")]
        [Required(ErrorMessage = "Customer Name cannot be blank")]
        public string CustomerName {
            get => customerName;
            set {
                customerName = NameSanitizer.GetSanitizedName(value);
                updateDatabaseName();
            }
        }

        [DisplayName("Customer Location")]
        public string CustomerLocation {
            get => customerLocation;
            set {
                customerLocation = NameSanitizer.GetSanitizedName(value);
                updateDatabaseName();
            }
        }

        public int CustomerId => CustomerFullName.GetHashCode();

        public string CustomerFullName =>
            $"{CustomerName?.ToLower()}{(string.IsNullOrWhiteSpace(CustomerLocation) ? "" : "-")}{CustomerLocation?.ToLower()}";

        public CustomerInfoState ActiveState { get; set; }
        public FailureReason ActiveStateFailureReason { get; set; }

        [DisplayName("DB Server Name")]
        public string Server => server;

        public string Domain => domain;

        // ideal:  Colorware-coke-atlanta or just Colorware-coke
        // TODO - may have to clean up first, not all characters are acceptable (currently restricted in UI to alphanumeric only)
        [DisplayName("DB Name")]
        public string DatabaseName => databaseName;

        [DisplayName("DB User")]
        public string ContainedUserName => containedUserName;

        [DisplayName("DB Password")]
        public string ContainedUserPassword => containedUserPassword;

        //---------------------------------------------------------------------

        protected BaseCustomerInfoModel() {
            server = ConfigValueReader.ServerName;
            domain = ConfigValueReader.McServerDomain;
            containedUserName = ConfigValueReader.ContainedDbUser;
            containedUserPassword = Guid.NewGuid().ToString();
        }

        //---------------------------------------------------------------------

        protected BaseCustomerInfoModel(string customerName, string customerLocation, string server, string domain,
                                        string databaseName,
                                        string userName, string password,
                                        CustomerInfoState activeState) {
            this.customerName = customerName ?? throw new ArgumentNullException(nameof(customerName));
            this.customerLocation = customerLocation;
            this.server = server;

            this.domain = domain;
            this.databaseName = databaseName;
            containedUserPassword = password;
            containedUserName = userName;

            ActiveState = activeState;
        }

        //---------------------------------------------------------------------

        public static string GetDbNameForCustomer(string customerName, string customerLocation) {
            customerName = NameSanitizer.GetSanitizedName(customerName);
            customerLocation = NameSanitizer.GetSanitizedName(customerLocation ?? "");
            return
                $"Colorware-{customerName?.ToLower()}{(string.IsNullOrWhiteSpace(customerLocation) ? "" : "-")}{customerLocation?.ToLower()}";
        }

        //---------------------------------------------------------------------

        private void updateDatabaseName() {
            databaseName = GetDbNameForCustomer(CustomerName, CustomerLocation);
        }

        //---------------------------------------------------------------------
    }

    //=========================================================================

    public class CustomerInfoModel : BaseCustomerInfoModel {
        [DisplayName("CNAME Record Value")]
        public string CNameRecordValue => CustomerFullName;

        [DisplayName("MeasureColor Server Url")]
        public string CustomerUrl => $"{CustomerFullName}.{Domain}";

        public string AzureUrl => $"{CustomerFullName}.azurewebsites.net";

        [DisplayName("Server Name")]
        public string SqlServerManagementStudioServerName => $"tcp:{Server}.database.windows.net,1433";

        [DisplayName("DB Connection String")]
        public string ConnectionString {
            get {
                if (ConfigValueReader.HasDevConnectionString)
                    return ConfigValueReader.GetDevConnectionString(DatabaseName);

                return getConnectionString(ContainedUserName, ContainedUserPassword);
            }
        }

        public string AdminConnectionString {
            get {
                if (ConfigValueReader.HasDevConnectionString)
                    return ConfigValueReader.GetDevConnectionString(DatabaseName);

                return getConnectionString(ConfigValueReader.ServerAdmin, ConfigValueReader.ServerAdminPassword);
            }
        }

        [DisplayName("License Blob Path")]
        public string LicenseBlobPath => $"<app blob storage>\\licenses\\{CNameRecordValue}";

        [DisplayName("Server Version")]
        public string ServerVersion => "17.2";

        public string Feedback { get; set; }

        //---------------------------------------------------------------------

        public CustomerInfoModel() {
            ActiveState = CustomerInfoState.None;
            ActiveStateFailureReason = FailureReason.None;
        }

        //---------------------------------------------------------------------

        public CustomerInfoModel(string customerName,
                                 string customerLocation,
                                 string server,
                                 string databaseName,
                                 string userName,
                                 string password,
                                 string domain,
                                 CustomerInfoState activeState)
            : base(customerName, customerLocation, server, domain, databaseName, userName, password, activeState) { }

        //---------------------------------------------------------------------

        public CustomerInfoModel(CustomerInfoEntity customerInfoEntity)
            : this(customerName: customerInfoEntity.PartitionKey, customerLocation: customerInfoEntity.RowKey,
                   server: customerInfoEntity.Server, databaseName: customerInfoEntity.DatabaseName,
                   userName: customerInfoEntity.UserName, password: customerInfoEntity.Password,
                   domain: customerInfoEntity.Domain, activeState: customerInfoEntity.ActiveStateEnum) { }

        //---------------------------------------------------------------------

        public override string ToString() {
            return CustomerFullName;
        }

        //---------------------------------------------------------------------

        private string getConnectionString(string userName, string password) {
            return
                $"Server=tcp:{Server}.database.windows.net,1433;Initial Catalog={DatabaseName};Persist Security Info=False;User ID={userName};Password={password};MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False";
        }

        //---------------------------------------------------------------------
    }
}