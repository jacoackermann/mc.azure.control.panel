﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace mc.azure.sdk.wrappers.Models {
    public class CustomerDeactivateModel {
        public List<CustomerInfoModel> CustomerInfos { get; set; }

        public string SelectedCustomerName { get; set; }

        [Required]
        [EmailAddress]
        [DisplayName("Support Email Recipients")]
        public string SupportEmail { get; set; }
    }
}