﻿using System;

using Colorware.Core.Azure.Config;
using Colorware.Core.Azure.Storage.BlobStorage;

namespace mc.azure.sdk.wrappers.Storage.BlobStorage {
    public static class DbBackupBlobHelper {
        // customer DB backups - secondary storage only!
        private static readonly BlobWrapper blobWrapper = new BlobWrapper(BlobWrapper.StorageType.Secondary);

        //---------------------------------------------------------------------

        public static string GetDbBackupFolderUri(string dbName) {
            var backupContainer = blobWrapper.GetContainerRef(ConfigValueReader.CustomerSqlBackupBlobContainer);
            var uri = $"{backupContainer.Uri.AbsoluteUri}/{dbName}/";
            return uri;
        }

        //---------------------------------------------------------------------

        public static bool DbHasBackupFiles(string dbName) {
            var backupFiles =
                blobWrapper.GetBlobFiles(ConfigValueReader.CustomerSqlBackupBlobContainer, dbName);
            if (backupFiles.Count > 0) {
                return true;
            }

            return false;
        }

        //---------------------------------------------------------------------

        public static string LatestBackupFileUri(string dbName) {
            var backupFiles =
                blobWrapper.GetBlobFiles(ConfigValueReader.CustomerSqlBackupBlobContainer, dbName);

            // TODO
            if (backupFiles.Count == 0) return "";

            // reverse sort: y before x
            backupFiles.Sort((x, y) => String.Compare(y.Uri.AbsoluteUri, x.Uri.AbsoluteUri, StringComparison.Ordinal));

            var lastFileUri = backupFiles[0].Uri.AbsoluteUri;
            return lastFileUri;
        }

        //---------------------------------------------------------------------
    }
}