﻿using System;
using System.Collections.Generic;
using System.Linq;

using Colorware.Core.Azure.Enums;
using Colorware.Core.Azure.Storage.Accounts;
using Colorware.Core.Azure.Storage.TableStorage.CustomerInfo;

using JetBrains.Annotations;

using mc.azure.sdk.wrappers.Interfaces;
using mc.azure.sdk.wrappers.Models;

using Microsoft.WindowsAzure.Storage.Table;

namespace mc.azure.sdk.wrappers.Storage.TableStorage {
    public class CustomerInfoManager : ICustomerInfoManager {
        private readonly string emptyCustomerLocationToken = "<blank>";

        private readonly IStorageAccountManager storageAccountManager;

        //---------------------------------------------------------------------

        public CustomerInfoManager([NotNull] IStorageAccountManager storageAccountManager) {
            this.storageAccountManager =
                storageAccountManager ?? throw new ArgumentNullException(nameof(storageAccountManager));
        }

        //---------------------------------------------------------------------

        public List<CustomerInfoModel> GetCustomerInfos(CustomerInfoSelectOption option) {
            var customerInfoTable = getCustomerInfoTable();

            TableQuery<CustomerInfoEntity> query;
            switch (option) {
                case CustomerInfoSelectOption.Active:
                    query = new TableQuery<CustomerInfoEntity>().Where(TableQuery.GenerateFilterCondition(
                                                                           nameof(CustomerInfoEntity.ActiveState),
                                                                           QueryComparisons.Equal,
                                                                           CustomerInfoState.Active.ToString()));
                    break;

                case CustomerInfoSelectOption.Deactivated:
                    query = new TableQuery<CustomerInfoEntity>().Where(TableQuery.GenerateFilterCondition(
                                                                           nameof(CustomerInfoEntity.ActiveState),
                                                                           QueryComparisons.Equal,
                                                                           CustomerInfoState.Deactivated.ToString()));
                    break;

                default:
                    query = new TableQuery<CustomerInfoEntity>();
                    break;
            }

            var customerEntities = customerInfoTable.ExecuteQuery(query).ToList();
            customerEntities.ForEach(x => x.RowKey = decodeCustomerLocation(x.RowKey));

            List<CustomerInfoModel> customers = new List<CustomerInfoModel>();
            customerEntities.ForEach(x => customers.Add(new CustomerInfoModel(x)));

            return customers;
        }

        //---------------------------------------------------------------------

        public CustomerInfoModel GetCustomerInfo(string customerName, string customerLocation) {
            CustomerInfoEntity customerEntity = getCustomerEntity(customerName,
                                                                  customerLocation);
            if (customerEntity != null) {
                var customerInfoModel = new CustomerInfoModel(customerEntity);
                return customerInfoModel;
            }

            return new CustomerInfoModel {
                CustomerName = customerName,
                CustomerLocation = customerLocation
            };
        }

        //---------------------------------------------------------------------

        public void CreateOrUpdateCustomer(CustomerInfoModel customerInfo,
                                           CustomerInfoState activeStateOption,
                                           FailureReason failureReason) {
            var customerInfoTable = getCustomerInfoTable();

            var tableEntity =
                new CustomerInfoEntity(customerInfo.CustomerName, encodeCustomerLocation(customerInfo.CustomerLocation),
                                       customerInfo.DatabaseName,
                                       customerInfo.ContainedUserName, customerInfo.ContainedUserPassword,
                                       customerInfo.Server,
                                       customerInfo.Domain, customerInfo.ConnectionString, customerInfo.ServerVersion,
                                       activeStateOption, failureReason);
            TableOperation insertOperation = TableOperation.InsertOrReplace(tableEntity);
            customerInfoTable.Execute(insertOperation);
        }

        //---------------------------------------------------------------------

        public CustomerInfoState GetCustomerActiveState(string customerName, string customerLocation) {
            var customerEntity = getCustomerEntity(customerName, customerLocation);
            if (customerEntity == null) return CustomerInfoState.None;

            return customerEntity.ActiveStateEnum;
        }

        //---------------------------------------------------------------------

        private CloudTable getCustomerInfoTable() {
            CloudTableClient tableClient = storageAccountManager.PrimaryStorageAccountOld.CreateCloudTableClient();
            CloudTable customerInfoTable = tableClient.GetTableReference("CustomerInfo");
            customerInfoTable.CreateIfNotExists();
            return customerInfoTable;
        }

        //---------------------------------------------------------------------

        private CustomerInfoEntity getCustomerEntity(string customerName, string customerLocation) {
            TableQuery<CustomerInfoEntity> query = new TableQuery<CustomerInfoEntity>().Where(
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition(nameof(CustomerInfoEntity.PartitionKey),
                                                       QueryComparisons.Equal, customerName),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition(nameof(CustomerInfoEntity.RowKey),
                                                       QueryComparisons.Equal,
                                                       encodeCustomerLocation(customerLocation))));

            var customerInfoTable = getCustomerInfoTable();
            var results = customerInfoTable.ExecuteQuery(query).ToList();

            if (results.Count > 0) {
                var customer = results[0];
                customer.RowKey = decodeCustomerLocation(customer.RowKey);

                return customer;
            }

            return null;
        }

        //---------------------------------------------------------------------

        private string encodeCustomerLocation(string customerLocation) {
            var encodedCustomerLocation = string.IsNullOrWhiteSpace(customerLocation)
                                              ? emptyCustomerLocationToken
                                              : customerLocation.ToLower();
            return encodedCustomerLocation;
        }

        //---------------------------------------------------------------------

        private string decodeCustomerLocation(string customerLocation) {
            var decodedCustomerLocation = customerLocation == emptyCustomerLocationToken ? "" : customerLocation;
            return decodedCustomerLocation;
        }

        //---------------------------------------------------------------------
    }
}