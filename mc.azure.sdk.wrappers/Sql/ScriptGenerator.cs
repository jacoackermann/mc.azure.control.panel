﻿using System.Text;

namespace mc.azure.sdk.wrappers.Sql {
    internal static class ScriptTemplateGenerator {
        internal static string GetCreateContainedDbUserScript() {
            // https://blogs.msdn.microsoft.com/azuresqldbsupport/2016/10/05/create-sql-login-and-sql-user-on-your-azure-sql-db/
            // https://docs.microsoft.com/en-us/azure/sql-database/sql-database-manage-logins

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("create user [XXX_UserName_XXX] with password = 'XXX_Password_XXX'")
              .AppendLine("alter role db_datareader add member [XXX_UserName_XXX]")
              .AppendLine("alter role db_datawriter add member [XXX_UserName_XXX]");

            return sb.ToString();
        }

        //---------------------------------------------------------------------

        //internal static string GetDbBackupScript()
        //{
        //    //https://msdn.microsoft.com/en-us/library/jj720552(v=sql.120).aspx

        //    StringBuilder sb = new StringBuilder();

        //    sb.AppendLine("BACKUP DATABASE[XXX_DatabaseName_XXX]")
        //      .AppendLine(
        //          "TO URL = 'XXX_BlobStorageEndPoint_XXXXXX_SqlBackupContainer_XXX/XXX_SqlBackupPath_XXX/XXX_BackupFileName_XXX.bak'");
        //    /* URL includes the endpoint for the BLOB service, followed by the container name, and the name of the backup file*/
        //    //              .AppendLine("WITH CREDENTIAL = 'mycredential'");

        //    return sb.ToString();
        //}
    }
}