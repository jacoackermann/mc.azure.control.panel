﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Colorware.Core.Azure.Enums;
using Colorware.Core.Azure.Storage.Accounts;

using JetBrains.Annotations;

using mc.azure.email.Enums;
using mc.azure.email.Interfaces;
using mc.azure.migrations.Interfaces;
using mc.azure.sdk.wrappers.Interfaces;
using mc.azure.sdk.wrappers.Models;
using mc.azure.sdk.wrappers.Services.AzureResources.Components;

// https://github.com/Azure/azure-sdk-for-net/tree/Fluent
// https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-create-service-principal-portal

namespace mc.azure.sdk.wrappers.Services.AzureResources {
    public class AzureResourceService : IAzureResourceService {
        private readonly IStorageAccountManager storageAccountManager;
        private readonly ICustomerInfoManager customerInfoManager;
        private readonly IDataMigrationService dataMigrationService;
        private readonly IEmailService emailService;

        private readonly StringBuilder messages = new StringBuilder();
        public string Feedback => messages.ToString();

        //---------------------------------------------------------------------

        public AzureResourceService([NotNull] IStorageAccountManager storageAccountManager,
                                    [NotNull] ICustomerInfoManager customerInfoManager,
                                    [NotNull] IDataMigrationService dataMigrationService,
                                    [NotNull] IEmailService emailService) {
            this.storageAccountManager =
                storageAccountManager ?? throw new ArgumentNullException(nameof(storageAccountManager));
            this.customerInfoManager =
                customerInfoManager ?? throw new ArgumentNullException(nameof(customerInfoManager));
            this.dataMigrationService =
                dataMigrationService ?? throw new ArgumentNullException(nameof(dataMigrationService));
            this.emailService = emailService ?? throw new ArgumentNullException(nameof(emailService));
        }

        //---------------------------------------------------------------------

        public async Task CreateCustomerAsync(CustomerCreateModel model, string sitePath, string requestIp) {
            AzureResourceCreator resourceCreator =
                new AzureResourceCreator(storageAccountManager, customerInfoManager, dataMigrationService, emailService,
                                         model);
            await resourceCreator.CreateCustomerInfrastructureAsync(sitePath);
        }

        //---------------------------------------------------------------------

        public async Task DeactivateCustomerAsync(CustomerInfoModel model, string supportEmail) {
            AzureResourceDeactivator resourceDeactivator =
                new AzureResourceDeactivator(storageAccountManager, customerInfoManager, emailService, model,
                                             supportEmail);
            await resourceDeactivator.DeactivateCustomerAsync();
        }

        //---------------------------------------------------------------------

        public bool CanCreateCustomerInfrastructure(CustomerCreateModel model, out string message) {
            AzureResourceCreator resourceCreator =
                new AzureResourceCreator(storageAccountManager, customerInfoManager, dataMigrationService, emailService,
                                         model);
            return resourceCreator.CanCreateCustomerInfrastructure(out message);
        }

        //---------------------------------------------------------------------

        public bool IsNewCustomer(CustomerCreateModel model) {
            var customerState =
                customerInfoManager.GetCustomerActiveState(model.SanitizedCustomerName,
                                                           model.SanitizedCustomerLocation);
            return customerState == CustomerInfoState.None;
        }

        //---------------------------------------------------------------------

        public bool IsCustomerDbBackupAvailable(CustomerCreateModel model) {
            AzureResourcesDatabaseManager databaseManager =
                new AzureResourcesDatabaseManager(storageAccountManager);
            return databaseManager.BackupExists(
                BaseCustomerInfoModel.GetDbNameForCustomer(model.SanitizedCustomerName,
                                                           model.SanitizedCustomerLocation));
        }

        //---------------------------------------------------------------------

        public List<CustomerInfoModel> GetCustomerInfos(CustomerInfoSelectOption option) {
            List<CustomerInfoModel> returnValue = customerInfoManager.GetCustomerInfos(option);
            return returnValue;
        }

        //---------------------------------------------------------------------

        public Task MigrateDatabases(List<CustomerInfoModel> customersToMigrate, string sitePath, string supportEmail) {
            StringBuilder sb = new StringBuilder();

            foreach (var customer in customersToMigrate) {
                try {
                    dataMigrationService.InitialiseCustomerDatabase(customer.AdminConnectionString, sitePath);

                    sb.AppendLine($"Successfully migrated customer database '{customer.DatabaseName}'");
                }
                catch (Exception e) {
                    sb.AppendLine($"Could not migrate customer database '{customer.DatabaseName}'")
                      .AppendLine($"    Reason:  {e.Message}");
                }

                sb.AppendLine()
                  .AppendLine("-------------------------")
                  .AppendLine();
            }

            emailService.Send(MessageType.DbMigration, "", "", "", supportEmail, "",
                              sb.ToString());

            return null;
        }

        //---------------------------------------------------------------------

        public async Task<List<FirewallRuleModel>> GetFirewallRulesAsync() {
            AzureResourcesFirewallRuleManager firewallRuleManager = new AzureResourcesFirewallRuleManager();
            return await firewallRuleManager.GetFirewallRulesAsync();
        }

        //---------------------------------------------------------------------

        public async Task AddFirewallRuleForIpAsync(FirewallRuleCollectionModel model) {
            AzureResourcesFirewallRuleManager firewallRuleManager = new AzureResourcesFirewallRuleManager();
            await firewallRuleManager.AddFirewallRuleForIpAsync(model.NewRuleNameSanitized, model.NewRuleIp);

            messages.AppendLine(firewallRuleManager.Feedback);
        }

        //---------------------------------------------------------------------
    }
}