﻿using System;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Colorware.Core.Azure.Config;
using Colorware.Core.Azure.Storage.Accounts;

using JetBrains.Annotations;

using mc.azure.sdk.wrappers.Services.AzureResources.Components;
using mc.azure.sdk.wrappers.Sql;
using mc.azure.sdk.wrappers.Storage.BlobStorage;
using mc.azure.utilities.Exceptions;

using Microsoft.Azure;
using Microsoft.Azure.Management.Sql;
using Microsoft.Azure.Management.Sql.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace mc.azure.sdk.wrappers.Services.AzureResources.DatabaseManagers {
    public class AzureDatabaseManager : BaseDatabaseManager {
        private static readonly string _templateArgDbUserName = "XXX_UserName_XXX";
        private static readonly string _templateArgDbUserPassword = "XXX_Password_XXX";

        private readonly StringBuilder messages;

        private readonly IStorageAccountManager storageAccountManager;
        private readonly AzureResourcesSqlServerManager serverManager = new AzureResourcesSqlServerManager();

        //---------------------------------------------------------------------

        public AzureDatabaseManager([NotNull] IStorageAccountManager storageAccountManager,
                                    StringBuilder messages)
            : base(messages) {
            this.storageAccountManager =
                storageAccountManager ?? throw new ArgumentNullException(nameof(storageAccountManager));

            this.messages = messages ?? throw new ArgumentNullException(nameof(messages));
        }

        //---------------------------------------------------------------------

        public override bool BackupExists(string dbName) {
            var hasBackupFiles = DbBackupBlobHelper.DbHasBackupFiles(dbName);
            return hasBackupFiles;
        }

        //---------------------------------------------------------------------

        public override async Task<bool> DatabaseExistsAsync(string dbName) {
            var sqlServer = await serverManager.GetSqlServerAsync();
            if (sqlServer == null) {
                messages.Append(serverManager.Feedback);
                return false;
            }

            var dbInstance = sqlServer.Databases.Get(dbName);
            return dbInstance != null;
        }

        //---------------------------------------------------------------------

        public override async Task<bool> CreateDatabaseAsync(string dbName,
                                                             string connectionString,
                                                             string userName,
                                                             string password) {
            var sqlServer = await serverManager.GetSqlServerAsync();
            if (sqlServer == null) {
                messages.Append(serverManager.Feedback);
                return false;
            }

            try {
                await sqlServer.Databases.Define(dbName)
                               .WithEdition(ConfigValueReader.DatabaseEdition)
                               .WithServiceObjective(ConfigValueReader.DatabaseServiceObjective)
                               .CreateAsync();
            }
            catch (Exception e) {
                messages.AppendLine(ExceptionUtils.GetExceptionMsg(e));
            }

            try {
                await createContainedDbUser(connectionString, userName, password);
            }
            catch (Exception e) {
                messages.AppendLine(ExceptionUtils.GetExceptionMsg(e));
                return false;
            }

            return true;
        }

        //---------------------------------------------------------------------

        public override async Task<bool> DeleteDatabaseAsync(string dbName) {
            var sqlServer = await serverManager.GetSqlServerAsync();
            if (sqlServer == null) {
                messages.Append(serverManager.Feedback);
                return false;
            }

            try {
                await sqlServer.Databases.DeleteAsync(dbName);
            }
            catch (Exception e) {
                messages.AppendLine(ExceptionUtils.GetExceptionMsg(e));
                return false;
            }

            return true;
        }

        //---------------------------------------------------------------------

        private async Task createContainedDbUser(string connectionString,
                                                 string containedUserName,
                                                 string containedPassword) {
            using (SqlConnection connection = new SqlConnection(connectionString)) {
                connection.Open();

                string script = ScriptTemplateGenerator.GetCreateContainedDbUserScript();
                script = script.Replace(_templateArgDbUserName, containedUserName);
                script = script.Replace(_templateArgDbUserPassword, containedPassword);

                using (SqlCommand command = new SqlCommand(script, connection)) {
                    await command.ExecuteNonQueryAsync();
                }
            }
        }

        //---------------------------------------------------------------------


        // TODO - tons of duplicate code!! Clean up!!


        #region Backup (export) customer DB
        protected override async Task<bool> exportCustomerDb(string dbName) {
            // delete previous backups?
            // or delete when we restore?
            // no, rather delete when you create a new backup...???
            // or don't bother to delete initially, but then restore must look for the latest file

            var storageAccCredentials = storageAccountManager.SecondaryStorageAccountOld.Credentials;
            var sqlManagementClient = await getSqlManagementClient();

            var resourceGroup = ConfigValueReader.ResourceGroupName;
            var serverName = ConfigValueReader.ServerName;
            var adminLogin = ConfigValueReader.ServerAdmin;
            var adminPassword = ConfigValueReader.ServerAdminPassword;
            var storageKey = storageAccCredentials.ExportBase64EncodedKey();

            var storageUri = DbBackupBlobHelper.GetDbBackupFolderUri(dbName);
            var backFilename = dbName + "-" + $"{DateTime.UtcNow:yyyyMMddHHmm}" + ".bacpac";
            var backupUrl = storageUri + backFilename;

            ExportRequestParameters exportRequestParameters = new ExportRequestParameters {
                AdministratorLogin = adminLogin,
                AdministratorLoginPassword = adminPassword,
                StorageKey = storageKey,
                StorageKeyType = "StorageAccessKey",
                StorageUri = new Uri(backupUrl)
            };

            var export = await sqlManagementClient.ImportExport.ExportAsync(resourceGroup, serverName, dbName,
                                                                            exportRequestParameters);

            ImportExportOperationStatusResponse exportStatus = new ImportExportOperationStatusResponse();
            while (exportStatus.Status == OperationStatus.InProgress) {
                Thread.Sleep(20 * 1000);
                exportStatus =
                    sqlManagementClient.ImportExport.GetImportExportOperationStatus(export.OperationStatusLink);
            }

            return true;
        }
        #endregion

        //---------------------------------------------------------------------

        #region Import customer DB
        protected override async Task<bool> restoreCustomerDbAsync(string dbName) {
            var storageAccCredentials = storageAccountManager.SecondaryStorageAccountOld.Credentials;
            var sqlManagementClient = await getSqlManagementClient();

            var adminLogin = ConfigValueReader.ServerAdmin;
            var adminPassword = ConfigValueReader.ServerAdminPassword;
            var storageKey = storageAccCredentials.ExportBase64EncodedKey();
            var resourceGroup = ConfigValueReader.ResourceGroupName;
            var serverName = ConfigValueReader.ServerName;

            // TODO make sure correct file is selected!!
            var backupUrl = DbBackupBlobHelper.LatestBackupFileUri(dbName);

            var importRequestParameters = new ImportRequestParameters {
                AdministratorLogin = adminLogin,
                AdministratorLoginPassword = adminPassword,
                StorageKey = storageKey,
                StorageKeyType = "StorageAccessKey",
                StorageUri = new Uri(backupUrl),

                DatabaseName = dbName,

                //AuthenticationType = "",
                //DatabaseMaxSize = 0,
                Edition = ConfigValueReader.DatabaseEdition,
                ServiceObjectiveName = ConfigValueReader.DatabaseServiceObjective
            };

            var import =
                await sqlManagementClient.ImportExport.ImportAsync(resourceGroup, serverName, importRequestParameters);

            ImportExportOperationStatusResponse exportStatus = new ImportExportOperationStatusResponse();
            while (exportStatus.Status == OperationStatus.InProgress) {
                Thread.Sleep(10 * 1000);
                exportStatus =
                    sqlManagementClient.ImportExport.GetImportExportOperationStatus(import.OperationStatusLink);
            }

            return true;
        }
        #endregion

        //---------------------------------------------------------------------

        private async Task<SqlManagementClient> getSqlManagementClient() {
            var subscriptionId = ConfigValueReader.AzureSubscriptionId;
            var applicationId = ConfigValueReader.ApplicationId;
            var tenantId = ConfigValueReader.TenantId;
            var secretKey = ConfigValueReader.ApplicationSecret;
            var accessToken = await getAccessToken(tenantId, applicationId, secretKey);
            var tokenCloudCredentials = new TokenCloudCredentials(subscriptionId, accessToken);
            return new SqlManagementClient(tokenCloudCredentials);
        }

        //---------------------------------------------------------------------

        private async Task<string> getAccessToken(string tenantId, string clientId, string secretKey) {
            var authenticationContext = new AuthenticationContext($"https://login.windows.net/{tenantId}");
            var credential = new ClientCredential(clientId, secretKey);
            var result = await authenticationContext.AcquireTokenAsync("https://management.core.windows.net/",
                                                                       credential);

            if (result == null)
                throw new InvalidOperationException("Failed to obtain the JWT token");

            var token = result.AccessToken;
            return token;
        }

        //---------------------------------------------------------------------
    }
}