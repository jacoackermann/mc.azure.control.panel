﻿using System.Threading.Tasks;

namespace mc.azure.sdk.wrappers.Services.AzureResources.DatabaseManagers {
    public interface IDatabaseManager {
        Task<bool> DatabaseExistsAsync(string dbName);

        Task<bool> CreateDatabaseAsync(string dbName,
                                       string connectionString,
                                       string userName,
                                       string password);

        bool BackupExists(string dbName);
        Task<bool> RestoreDatabaseAsync(string dbName);
        Task<bool> BackupDatabaseAsync(string dbName);
        Task<bool> DeleteDatabaseAsync(string dbName);
    }
}