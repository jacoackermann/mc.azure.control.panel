﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Colorware.Core.Azure.Config;
using Colorware.Core.Azure.Storage.Accounts;
using Colorware.Core.Azure.Storage.BlobStorage;

using JetBrains.Annotations;

using mc.azure.utilities.Exceptions;
using mc.azure.utilities.IO;

using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;


// TODO - store backups in dev storage, not local file!!!


namespace mc.azure.sdk.wrappers.Services.AzureResources.DatabaseManagers {
    public class LocalDatabaseManager : BaseDatabaseManager {
        private readonly IStorageAccountManager storageAccountManager;

        private readonly StringBuilder messages;

        private readonly string setSingleUserQuery =
            "USE master; ALTER DATABASE [{0}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE";

        private readonly string setMultiUserQuery = "USE master; ALTER DATABASE [{0}] SET MULTI_USER";
        private readonly string changeUsersLoginQuery = "USE master; EXEC sp_change_users_login 'AUTO_FIX', '{0}'";

        private readonly string restoreQuery =
            "USE master; RESTORE DATABASE [{0}] FROM DISK = N'{1}' WITH FILE = 1, NOUNLOAD, REPLACE, STATS = 5";

        //---------------------------------------------------------------------

        public LocalDatabaseManager([NotNull] IStorageAccountManager storageAccountManager,
                                    StringBuilder messages)
            : base(messages) {
            this.storageAccountManager =
                storageAccountManager ?? throw new ArgumentNullException(nameof(storageAccountManager));

            this.messages = messages ?? throw new ArgumentNullException(nameof(messages));
        }

        //---------------------------------------------------------------------

        public override bool BackupExists(string dbName) {
            var backupDir = GetLocalBackupDir(dbName);
            if (!Directory.Exists(backupDir)) {
                return false;
            }

            return Directory.GetFiles(backupDir, "*.bak", SearchOption.TopDirectoryOnly).Length > 0;
        }

        //---------------------------------------------------------------------

        public override async Task<bool> CreateDatabaseAsync(string dbName, string connectionString, string userName,
                                                             string password) {
            try {
                var server = getServer(dbName);

                Database db = new Database(server, dbName);
                db.Create();

                return true;
            }
            catch (Exception e) {
                messages.AppendLine(ExceptionUtils.GetExceptionMsg(e));
            }

            return false;
        }

        //---------------------------------------------------------------------

        public override async Task<bool> DatabaseExistsAsync(string dbName) {
            try {
                var server = getServer(dbName);
                foreach (Database database in server.Databases) {
                    if (database.Name == dbName) return true;
                }
            }
            catch (Exception e) {
                messages.AppendLine(ExceptionUtils.GetExceptionMsg(e));
            }

            return false;
        }

        //---------------------------------------------------------------------

        public override async Task<bool> DeleteDatabaseAsync(string dbName) {
            try {
                var dbToDrop = getDatabase(dbName);

                setSingleUserMode(dbToDrop);

                dbToDrop.Drop();

                return true;
            }
            catch (Exception e) {
                messages.AppendLine(ExceptionUtils.GetExceptionMsg(e));
            }

            return false;
        }

        //---------------------------------------------------------------------

        protected override async Task<bool> exportCustomerDb(string dbName) {
            var connStringBuilder =
                new SqlConnectionStringBuilder(ConfigValueReader.GetDevConnectionString(dbName));
            Server server = getServer(dbName);

            Backup bk = new Backup() {
                Action = BackupActionType.Database,
                BackupSetDescription = $"Full backup of {connStringBuilder.InitialCatalog}",
                BackupSetName = $"{connStringBuilder.InitialCatalog} Backup",
                Database = connStringBuilder.InitialCatalog,
                Incremental = false,
                ExpirationDate = DateTime.MaxValue,
                LogTruncation = BackupTruncateLogType.Truncate
            };


            #region try and backup to development storage
            //// turn these 3 lines into a method?
            //// repeated in AzureDatabaseManager.cs...
            //var storageUri = DbBackupBlobHelper.GetDbBackupFolderUri(dbName);
            //var backFilename = dbName + "-" + $"{DateTime.UtcNow:yyyyMMddHHmm}" + ".bak";
            //var backupUrl = storageUri + backFilename;


            //Credential cred = new Credential(server, "devStoreCredential");
            //try {
            //    cred.Create("devStoreCredential");
            //}
            //catch(FailedOperationException e) {
            //    // ignore
            //}

            //Credential test = server.GetSmoObject(cred.Urn) as Credential;
            //test.Drop();
            //cred.Create();


            //var bdi = new BackupDeviceItem(backupUrl, DeviceType.Url, cred.Identity);
            //bdi.CredentialName = cred.Identity;

            //bk.Devices.Add(bdi);
            //bk.CredentialName = cred.Identity;
            //bk.SqlBackup(server);
            #endregion


            #region for time being, backup to file and copy to blob storage
            var backupFilename = dbName + "-" + $"{DateTime.UtcNow:yyyyMMddHHmm}" + ".bak";
            var localBackupDir = GetLocalBackupDir(dbName);
            var backupPath = Path.Combine(localBackupDir, backupFilename);
            DirectoryUtils.CreatePath(localBackupDir);
            var bdi = new BackupDeviceItem(backupPath, DeviceType.File);
            bk.Devices.Add(bdi);
            bk.SqlBackup(server);

            // now copy to blob storage
            var blobWrapper = new BlobWrapper(BlobWrapper.StorageType.Secondary);
            await blobWrapper.WriteToBlobAsync(ConfigValueReader.CustomerSqlBackupBlobContainer,
                                               Path.Combine(dbName, backupFilename),
                                               File.ReadAllText(backupPath));
            #endregion


            return true;
        }

        //---------------------------------------------------------------------

        protected override async Task<bool> restoreCustomerDbAsync(string dbName) {
            var dbToRestore = getDatabase(dbName);

            try {
                // set single-user mode
                setSingleUserMode(dbToRestore);

                // restore
                restoreDatabase(dbToRestore);

                // set multi user mode
                setMultiuserMode(dbToRestore);

                // fix logins
                fixUserLogins(dbToRestore);

                return true;
            }
            catch (Exception e) {
                // TODO - fix!
                // set multi user mode
                setMultiuserMode(dbToRestore);
            }

            return true;
        }

        //---------------------------------------------------------------------

        private string GetLocalBackupDir(string dbName) {
            return $"c:/temp/colorware/sqlbackups/{dbName}";
        }

        //---------------------------------------------------------------------

        private Server getServer(string dbName) {
            var connStringBuilder =
                new SqlConnectionStringBuilder(ConfigValueReader.GetDevConnectionString(dbName));
            ServerConnection connection = new ServerConnection(connStringBuilder.DataSource);
            Server server = new Server(connection);
            return server;
        }

        //---------------------------------------------------------------------

        private Database getDatabase(string dbName) {
            var server = getServer(dbName);

            // why is this step necessary??
            Database db = new Database(server, dbName);
            return server.GetSmoObject(db.Urn) as Database;
        }

        //---------------------------------------------------------------------

        private void setSingleUserMode(Database database) {
            var query = string.Format(setSingleUserQuery, database.Name);
            database.ExecuteNonQuery(query);
        }

        //---------------------------------------------------------------------

        private void restoreDatabase(Database database) {
            string backupFilePath = getLatestBackupFilename(database.Name);
            var query = string.Format(restoreQuery, database.Name, backupFilePath);
            database.ExecuteNonQuery(query);
        }

        //---------------------------------------------------------------------

        private void setMultiuserMode(Database database) {
            var query = string.Format(setMultiUserQuery, database.Name);
            database.ExecuteNonQuery(query);
        }

        //---------------------------------------------------------------------

        private void fixUserLogins(Database database) {
            var query = string.Format(changeUsersLoginQuery, database.Name);
            database.ExecuteNonQuery(query);
        }

        //---------------------------------------------------------------------

        private string getLatestBackupFilename(string dbName) {
            var backupDir = GetLocalBackupDir(dbName);
            if (!Directory.Exists(backupDir)) {
                return "";
            }

            var files = Directory.GetFiles(backupDir, "*.bak", SearchOption.TopDirectoryOnly).ToList();
            files.Sort();
            files.Reverse();

            return files[0];
        }

        //---------------------------------------------------------------------
    }
}