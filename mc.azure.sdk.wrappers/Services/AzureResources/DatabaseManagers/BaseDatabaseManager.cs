﻿using System;
using System.Text;
using System.Threading.Tasks;

using mc.azure.utilities.Exceptions;

namespace mc.azure.sdk.wrappers.Services.AzureResources.DatabaseManagers {
    public abstract class BaseDatabaseManager : IDatabaseManager {
        private readonly StringBuilder messages;

        public BaseDatabaseManager(StringBuilder messages) {
            this.messages = messages ?? throw new ArgumentNullException(nameof(messages));
        }

        //---------------------------------------------------------------------

        public async Task<bool> RestoreDatabaseAsync(string dbName) {
            try {
                await restoreCustomerDbAsync(dbName);
            }
            catch (Exception e) {
                messages.AppendLine(ExceptionUtils.GetExceptionMsg(e));
                return false;
            }

            return true;
        }

        //---------------------------------------------------------------------

        public async Task<bool> BackupDatabaseAsync(string dbName) {
            try {
                await exportCustomerDb(dbName);
            }
            catch (Exception e) {
                messages.AppendLine(ExceptionUtils.GetExceptionMsg(e));
                return false;
            }

            return true;
        }

        //---------------------------------------------------------------------

        public abstract bool BackupExists(string dbName);
        public abstract Task<bool> DatabaseExistsAsync(string dbName);

        public abstract Task<bool> CreateDatabaseAsync(string dbName, string userName,
                                                       string connectionString,
                                                       string password);

        public abstract Task<bool> DeleteDatabaseAsync(string dbName);
        protected abstract Task<bool> exportCustomerDb(string dbName);
        protected abstract Task<bool> restoreCustomerDbAsync(string dbName);

        //---------------------------------------------------------------------
    }
}