﻿using System;
using System.Text;
using System.Threading.Tasks;

using Colorware.Core.Azure.Enums;
using Colorware.Core.Azure.Storage.Accounts;

using mc.azure.email.Enums;
using mc.azure.email.Interfaces;
using mc.azure.migrations.Interfaces;
using mc.azure.sdk.wrappers.Interfaces;
using mc.azure.sdk.wrappers.Models;

namespace mc.azure.sdk.wrappers.Services.AzureResources.Components {
    public class AzureResourceCreator {
        private readonly IStorageAccountManager storageAccountManager;
        private readonly ICustomerInfoManager customerInfoManager;
        private readonly IDataMigrationService dataMigrationService;
        private readonly IEmailService emailService;

        private readonly CustomerCreateModel customerCreateInfo;

        private readonly CustomerInfoModel customerInfo;

        private readonly StringBuilder messages = new StringBuilder();
        public string Feedback => messages.ToString();

        //---------------------------------------------------------------------

        public AzureResourceCreator(IStorageAccountManager storageAccountManager,
                                    ICustomerInfoManager customerInfoManager,
                                    IDataMigrationService dataMigrationService,
                                    IEmailService emailService,
                                    CustomerCreateModel customerCreateInfo) {
            this.storageAccountManager =
                storageAccountManager ?? throw new ArgumentNullException(nameof(storageAccountManager));
            this.customerInfoManager =
                customerInfoManager ?? throw new ArgumentNullException(nameof(customerInfoManager));
            this.dataMigrationService =
                dataMigrationService ?? throw new ArgumentNullException(nameof(dataMigrationService));
            this.emailService = emailService ?? throw new ArgumentNullException(nameof(emailService));

            this.customerCreateInfo = customerCreateInfo ?? throw new ArgumentNullException(nameof(customerCreateInfo));

            customerInfo =
                customerInfoManager.GetCustomerInfo(customerCreateInfo.SanitizedCustomerName,
                                                    customerCreateInfo.SanitizedCustomerLocation);
        }

        //---------------------------------------------------------------------

        public bool CanCreateCustomerInfrastructure(out string errorMsg) {
            errorMsg = "";

            switch (customerInfo.ActiveState) {
                case CustomerInfoState.Active:
                    errorMsg =
                        "Cannot create customer infrastructure, because the current customer is already active.";
                    break;
                case CustomerInfoState.Creating:
                    errorMsg =
                        "Cannot create customer infrastructure, because another create request is already running for this customer.";
                    break;
                case CustomerInfoState.Deactivating:
                    errorMsg =
                        "Cannot create customer infrastructure.  Wait for deactivation to finish and try again.";
                    break;

                // TODO - may have to think about this case a bit more...
                //      - may have to provide a simple way to "reset" customer state back to e.g. none in table storage
                //case CustomerInfoState.CreateFailed:
                //    messages.AppendLine(
                //        "Previous attempt to create customer infrastructure has failed.  Please investigate via the Azure portal and try again.");
                //    return null;
            }

            if (!string.IsNullOrWhiteSpace(errorMsg)) {
                return false;
            }

            return true;
        }

        //---------------------------------------------------------------------

        public async Task CreateCustomerInfrastructureAsync(string sitePath) {
            if (!CanCreateCustomerInfrastructure(out var errorMsg)) {
                emailService.Send(MessageType.CreateCustomerFailure, customerInfo.CustomerName,
                                  customerInfo.CustomerLocation, customerCreateInfo.EmailRecipients,
                                  customerCreateInfo.SupportEmailRecipients,
                                  customerInfo.CustomerUrl,
                                  errorMsg);
                return;
            }

            // set customer status to 'Creating'
            customerInfoManager.CreateOrUpdateCustomer(customerInfo, CustomerInfoState.Creating);

            // send welcome email
            // TODO - include customer name and location!!
            emailService.Send(MessageType.CreateCustomerWelcome, customerInfo.CustomerName,
                              customerInfo.CustomerLocation, customerCreateInfo.EmailRecipients,
                              customerCreateInfo.SupportEmailRecipients,
                              customerInfo.CustomerUrl);

            // check if customer DB already exists
            AzureResourcesDatabaseManager dbManager =
                new AzureResourcesDatabaseManager(storageAccountManager);
            if (!await dbManager.DatabaseExistsAsync(customerInfo.DatabaseName)) {
                // create database
                if (!await dbManager.CreateDatabase(customerInfo.DatabaseName,
                                                    customerInfo.AdminConnectionString,
                                                    customerInfo.ContainedUserName,
                                                    customerInfo.ContainedUserPassword)) {
                    emailService.Send(MessageType.CreateCustomerFailure, customerInfo.CustomerName,
                                      customerInfo.CustomerLocation, customerCreateInfo.EmailRecipients,
                                      customerCreateInfo.SupportEmailRecipients,
                                      customerInfo.CustomerUrl,
                                      $"Failed to create customer database:{Environment.NewLine}{dbManager.Feedback}");

                    // set customer status to 'CreateFailed'
                    customerInfoManager.CreateOrUpdateCustomer(customerInfo, CustomerInfoState.CreateFailed,
                                                               FailureReason.DbCreateFailed);
                    return;
                }

                // restore if option was selected and DB backup file exists
                // TODO
                if (customerCreateInfo.ShouldRecoverDbFromBackup) {
                    // restore DB from backup
                    if (!await dbManager.RestoreDatabase(customerInfo.DatabaseName)) {
                        emailService.Send(MessageType.CreateCustomerFailure, customerInfo.CustomerName,
                                          customerInfo.CustomerLocation, customerCreateInfo.EmailRecipients,
                                          customerCreateInfo.SupportEmailRecipients,
                                          customerInfo.CustomerUrl,
                                          $"Failed to restore customer database:{Environment.NewLine}{dbManager.Feedback}");

                        // set customer status to 'CreateFailed'
                        customerInfoManager.CreateOrUpdateCustomer(customerInfo, CustomerInfoState.CreateFailed,
                                                                   FailureReason.DbRestoreFailed);
                        return;
                    }
                }
            }

            // run migrations
            // TODO - should migrations run after an import?
            if (!dataMigrationService.InitialiseCustomerDatabase(customerInfo.AdminConnectionString, sitePath)) {
                emailService.Send(MessageType.CreateCustomerFailure, customerInfo.CustomerName,
                                  customerInfo.CustomerLocation, customerCreateInfo.EmailRecipients,
                                  customerCreateInfo.SupportEmailRecipients,
                                  customerInfo.CustomerUrl,
                                  $"Failed to run migration scripts for customer database:{Environment.NewLine}{dataMigrationService.Feedback}");

                // set customer status to 'CreateFailed'
                customerInfoManager.CreateOrUpdateCustomer(customerInfo, CustomerInfoState.CreateFailed,
                                                           FailureReason.MigrationsFailed);

                return;
            }

            // send success message via email to customer and support
            emailService.Send(MessageType.CreateCustomerSuccess, customerInfo.CustomerName,
                              customerInfo.CustomerLocation, customerCreateInfo.EmailRecipients,
                              customerCreateInfo.SupportEmailRecipients,
                              customerInfo.CustomerUrl);

            // set customer status to 'Active'
            customerInfoManager.CreateOrUpdateCustomer(customerInfo, CustomerInfoState.Active);
        }

        //---------------------------------------------------------------------
    }
}