﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Colorware.Core.Azure.Config;

using mc.azure.sdk.wrappers.Authentication;

using Microsoft.Azure.Management.Fluent;
using Microsoft.Azure.Management.ResourceManager.Fluent.Core;
using Microsoft.Azure.Management.Sql.Fluent;

namespace mc.azure.sdk.wrappers.Services.AzureResources.Components {
    public class AzureResourcesSqlServerManager {
        private ISqlServer sqlServer;

        private readonly StringBuilder messages = new StringBuilder();
        public string Feedback => messages.ToString();

        //---------------------------------------------------------------------

        public async Task<ISqlServer> GetSqlServerAsync() {
            if (sqlServer != null) return sqlServer;

            IAzure azure = AzureManagementProvider.Instance.GetInterface();
            if (azure == null) {
                messages.AppendLine("Azure Management Instance is null!");
                return null;
            }

            sqlServer = (await azure.SqlServers.ListByResourceGroupAsync(ConfigValueReader.ResourceGroupName))
                        .FirstOrDefault()
                        ??
                        await azure.SqlServers.Define(ConfigValueReader.ServerName)
                                   .WithRegion(Region.Create(ConfigValueReader.ResourceGroupLocation))
                                   .WithNewResourceGroup(ConfigValueReader.ResourceGroupName)
                                   .WithAdministratorLogin(ConfigValueReader.ServerAdmin)
                                   .WithAdministratorPassword(ConfigValueReader.ServerAdminPassword)
                                   .CreateAsync();

            if (sqlServer == null) {
                messages.AppendLine("Could not create ISqlServer instance!");
            }

            return sqlServer;
        }

        //---------------------------------------------------------------------
    }
}