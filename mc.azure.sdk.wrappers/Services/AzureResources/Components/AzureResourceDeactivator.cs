﻿using System;
using System.Threading.Tasks;

using Colorware.Core.Azure.Enums;
using Colorware.Core.Azure.Storage.Accounts;

using mc.azure.email.Enums;
using mc.azure.email.Interfaces;
using mc.azure.sdk.wrappers.Interfaces;
using mc.azure.sdk.wrappers.Models;

namespace mc.azure.sdk.wrappers.Services.AzureResources.Components {
    public class AzureResourceDeactivator {
        private readonly ICustomerInfoManager customerInfoManager;
        private readonly IEmailService emailService;

        private readonly CustomerInfoModel customerToDeactivate;

        private readonly AzureResourcesDatabaseManager databaseManager;
        private readonly string supportEmail;

        //---------------------------------------------------------------------

        public AzureResourceDeactivator(IStorageAccountManager storageAccountManager,
                                        ICustomerInfoManager customerInfoManager,
                                        IEmailService emailService,
                                        CustomerInfoModel customerToDeactivate,
                                        string supportEmail) {
            this.customerInfoManager =
                customerInfoManager ?? throw new ArgumentNullException(nameof(customerInfoManager));
            this.emailService = emailService ?? throw new ArgumentNullException(nameof(emailService));
            this.customerToDeactivate = customerToDeactivate ??
                                        throw new ArgumentNullException(nameof(customerToDeactivate));

            databaseManager = new AzureResourcesDatabaseManager(storageAccountManager);

            this.supportEmail = supportEmail ?? throw new ArgumentNullException(nameof(supportEmail));
        }

        //---------------------------------------------------------------------

        public async Task DeactivateCustomerAsync() {
            // create customer info
            CustomerInfoModel customerInfo =
                customerInfoManager.GetCustomerInfo(customerToDeactivate.CustomerName,
                                                    customerToDeactivate.CustomerLocation);

            if (customerInfo.ActiveState != CustomerInfoState.Active) {
                emailService.Send(MessageType.DeactivateCustomerFailure, customerInfo.CustomerName,
                                  customerInfo.CustomerLocation, "", supportEmail, customerInfo.CustomerUrl,
                                  "Customer is not in an active state and cannot be deactivated!");
                return;
            }

            // check if customer DB exists
            if (!await databaseManager.DatabaseExistsAsync(customerInfo.DatabaseName)) {
                // nothing to do, simply update customer state
                customerInfoManager.CreateOrUpdateCustomer(customerInfo, CustomerInfoState.Deactivated);
                return;
            }

            // set customer status to 'Deactivating'
            customerInfoManager.CreateOrUpdateCustomer(customerInfo, CustomerInfoState.Deactivating);

            // create a backup of the customer DB and delete
            if (!await databaseManager.BackupDatabase(customerToDeactivate.DatabaseName)
                ||
                !await databaseManager.DeleteDatabase(customerToDeactivate.DatabaseName)) {
                emailService.Send(MessageType.DeactivateCustomerFailure, customerInfo.CustomerName,
                                  customerInfo.CustomerLocation, "", supportEmail, customerInfo.CustomerUrl,
                                  databaseManager.Feedback);

                // we failed to create a backup or to delete the DB, so leave everything as it was and report the error
                customerInfoManager.CreateOrUpdateCustomer(customerInfo, CustomerInfoState.Active);
                return;
            }

            emailService.Send(MessageType.DeactivateCustomerSuccess, customerInfo.CustomerName,
                              customerInfo.CustomerLocation, "", supportEmail);

            // update table storage record for this customer
            customerInfoManager.CreateOrUpdateCustomer(customerInfo, CustomerInfoState.Deactivated);
        }

        //---------------------------------------------------------------------
    }
}