﻿using System.Text;
using System.Threading.Tasks;

using Colorware.Core.Azure.Config;
using Colorware.Core.Azure.Storage.Accounts;

using JetBrains.Annotations;

using mc.azure.sdk.wrappers.Services.AzureResources.DatabaseManagers;

namespace mc.azure.sdk.wrappers.Services.AzureResources.Components {
    public class AzureResourcesDatabaseManager {
        private readonly StringBuilder messages = new StringBuilder();
        internal string Feedback => messages.ToString();

        private readonly IDatabaseManager databaseManager;

        //---------------------------------------------------------------------

        public AzureResourcesDatabaseManager([NotNull] IStorageAccountManager storageAccountManager) {
            if (ConfigValueReader.HasDevConnectionString) {
                databaseManager = new LocalDatabaseManager(storageAccountManager, messages);
            }
            else {
                databaseManager = new AzureDatabaseManager(storageAccountManager, messages);
            }
        }

        //---------------------------------------------------------------------

        internal async Task<bool> DatabaseExistsAsync(string dbName) {
            return await databaseManager.DatabaseExistsAsync(dbName);
        }

        //---------------------------------------------------------------------

        internal async Task<bool> CreateDatabase(string dbName,
                                                 string connectionString,
                                                 string userName,
                                                 string password) {
            return await databaseManager.CreateDatabaseAsync(dbName, connectionString,
                                                             userName, password);
        }

        //---------------------------------------------------------------------

        internal bool BackupExists(string dbName) {
            return databaseManager.BackupExists(dbName);
        }

        //---------------------------------------------------------------------

        internal async Task<bool> RestoreDatabase(string dbName) {
            return await databaseManager.RestoreDatabaseAsync(dbName);
        }

        //---------------------------------------------------------------------

        internal async Task<bool> BackupDatabase(string dbName) {
            return await databaseManager.BackupDatabaseAsync(dbName);
        }

        //---------------------------------------------------------------------

        internal async Task<bool> DeleteDatabase(string dbName) {
            return await databaseManager.DeleteDatabaseAsync(dbName);
        }

        //---------------------------------------------------------------------
    }
}