﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using mc.azure.sdk.wrappers.Models;
using mc.azure.utilities.Exceptions;

namespace mc.azure.sdk.wrappers.Services.AzureResources.Components {
    public class AzureResourcesFirewallRuleManager {
        private readonly StringBuilder messages = new StringBuilder();
        public string Feedback => messages.ToString();

        private readonly AzureResourcesSqlServerManager serverManager = new AzureResourcesSqlServerManager();

        //---------------------------------------------------------------------

        internal async Task<bool> CreateFirewallRuleIfNotExistAsync(string firewallRuleName, string requestIp) {
            if (!await firewallRuleExistsForIpAsync(requestIp)) {
                try {
                    await AddFirewallRuleForIpAsync(firewallRuleName, requestIp);
                }
                catch (Exception e) {
                    messages.AppendLine($"Cannot add firewall rule for IP {requestIp}.")
                            .AppendLine()
                            .AppendLine($"Reason:  {ExceptionUtils.GetExceptionMsg(e)}");
                    return false;
                }
            }

            return true;
        }

        //---------------------------------------------------------------------

        public async Task<List<FirewallRuleModel>> GetFirewallRulesAsync() {
            var sqlServer = await serverManager.GetSqlServerAsync();

            if (sqlServer == null) {
                messages.Append(serverManager.Feedback);
                return null;
            }

            // create the list of existing firewall rules
            List<FirewallRuleModel> firewallRules = new List<FirewallRuleModel>();

            foreach (var firewallRule in sqlServer.FirewallRules.List()) {
                firewallRules.Add(new FirewallRuleModel {
                    SqlServerName = firewallRule.SqlServerName,
                    Kind = firewallRule.Kind,
                    StartIpAddress = firewallRule.StartIPAddress,
                    EndIpAddress = firewallRule.EndIPAddress,
                    Region = firewallRule.Region
                });
            }

            return firewallRules;
        }

        //---------------------------------------------------------------------

        public async Task AddFirewallRuleForIpAsync(string ruleName, string ipToAllow) {
            var currentFirewallRules = await GetFirewallRulesAsync();
            foreach (var firewallRule in currentFirewallRules) {
                if (firewallRule.StartIpAddress == ipToAllow) {
                    return;
                }
            }

            var sqlServer = await serverManager.GetSqlServerAsync();

            try {
                await sqlServer.FirewallRules.Define(ruleName)
                               .WithIPAddress(ipToAllow)
                               .CreateAsync();
            }
            catch (Exception e) {
                messages.AppendLine($"Cannot add server firewall rule:  '{e.Message}'");
            }
        }

        //---------------------------------------------------------------------

        private async Task<bool> firewallRuleExistsForIpAsync(string ip) {
            var firewallRules = await GetFirewallRulesAsync();
            var match = firewallRules.Find(x => x.StartIpAddress == ip);
            if (match != null) return true;

            return false;
        }

        //---------------------------------------------------------------------
    }
}