﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Colorware.Core.Azure.Enums;

using mc.azure.sdk.wrappers.Models;

namespace mc.azure.sdk.wrappers.Interfaces {
    public interface IAzureResourceService {
        string Feedback { get; }

        Task CreateCustomerAsync(CustomerCreateModel customerCreateInfo, string sitePath, string requestIp);
        Task DeactivateCustomerAsync(CustomerInfoModel customerToDeactivate, string supportEmail);

        bool CanCreateCustomerInfrastructure(CustomerCreateModel customerCreateModel, out string message);
        bool IsNewCustomer(CustomerCreateModel customerCreateInfo);
        bool IsCustomerDbBackupAvailable(CustomerCreateModel customerCreateInfo);
        List<CustomerInfoModel> GetCustomerInfos(CustomerInfoSelectOption option);

        Task MigrateDatabases(List<CustomerInfoModel> customersToMigrate, string sitePath, string supportEmail);

        Task<List<FirewallRuleModel>> GetFirewallRulesAsync();
        Task AddFirewallRuleForIpAsync(FirewallRuleCollectionModel model);
    }
}