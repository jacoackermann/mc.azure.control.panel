﻿using System.Collections.Generic;

using Colorware.Core.Azure.Enums;

using mc.azure.sdk.wrappers.Models;

namespace mc.azure.sdk.wrappers.Interfaces {
    public interface ICustomerInfoManager {
        CustomerInfoModel GetCustomerInfo(string customerName, string customerLocation);

        List<CustomerInfoModel> GetCustomerInfos(CustomerInfoSelectOption option = CustomerInfoSelectOption.All);

        void CreateOrUpdateCustomer(CustomerInfoModel customerInfo, CustomerInfoState activeOption,
                                    FailureReason failureReason = FailureReason.None);

        CustomerInfoState GetCustomerActiveState(string customerName, string customerLocation);
    }
}