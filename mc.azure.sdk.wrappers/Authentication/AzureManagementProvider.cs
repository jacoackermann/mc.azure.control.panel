﻿using Colorware.Core.Azure.Config;

using Microsoft.Azure.Management.Fluent;
using Microsoft.Azure.Management.ResourceManager.Fluent;
using Microsoft.Azure.Management.ResourceManager.Fluent.Authentication;

namespace mc.azure.sdk.wrappers.Authentication {
    public class AzureManagementProvider {
        public static AzureManagementProvider Instance { get; }

        static AzureManagementProvider() {
            Instance = new AzureManagementProvider();
        }

        private AzureManagementProvider() { }

        public IAzure GetInterface() {
            AzureCredentials ac = new AzureCredentials(
                new ServicePrincipalLoginInformation {
                    ClientId = ConfigValueReader.ApplicationId,
                    ClientSecret = ConfigValueReader.ApplicationSecret
                },
                ConfigValueReader.TenantId, AzureEnvironment.AzureGlobalCloud);
            return Azure.Configure().Authenticate(ac)
                        .WithSubscription(ConfigValueReader.AzureSubscriptionId);
        }
    }
}