﻿namespace mc.azure.migrations.Interfaces {
    public interface IDataMigrationService {
        string Feedback { get; }

        bool InitialiseCustomerDatabase(string connectionString, string sitePath);
    }
}