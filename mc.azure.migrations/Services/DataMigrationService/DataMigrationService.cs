﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

using Colorware.Core.Azure.Config;

using FluentMigrator.Runner.Announcers;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;

using mc.azure.migrations.Interfaces;
using mc.azure.utilities.Exceptions;

namespace mc.azure.migrations.Services.DataMigrationService {
    public class DataMigrationService : IDataMigrationService {
        private const int RunnerContextTimeout = 3600; // 1hour = 60 seconds * 60 minutes = 3600 seconds.

        private readonly StringBuilder messages = new StringBuilder();
        public string Feedback => messages.ToString();

        //---------------------------------------------------------------------

        public bool InitialiseCustomerDatabase(string connectionString, string sitePath) {
            messages.Clear();

            try {
                using (TextWriter writer = new StringWriter()) {
                    var processor = new MigrationProcessorFactoryProvider().GetFactory("SqlServer2008");

                    var announcer = new TextWriterAnnouncer(writer) {
                        ShowElapsedTime = true,
                        ShowSql = true
                    };

                    var target = sitePath;
                    if (ConfigValueReader.IsProductionMode) {
                        target += "/site/wwwroot/bin/Colorware.Migrations.dll";
                    }
                    else {
                        target += "/bin/Colorware.Migrations.dll";
                    }

                    var runnerContext = new RunnerContext(announcer) {
                        Database = processor.Name,
                        Connection = connectionString,
                        Targets = new[] {target},
                        Timeout = RunnerContextTimeout,
                        PreviewOnly = false
                    };

                    new TaskExecutor(runnerContext).Execute();

                    return true;
                }
            }
            catch (Exception e) {
                messages.AppendLine("Initialisation failed!  Exception message:")
                        .AppendLine(ExceptionUtils.GetExceptionMsg(e));
            }

            return false;
        }

        //---------------------------------------------------------------------

        private Task<bool> executeAsync(RunnerContext runnerContext, TextWriter writer) {
            return Task.Run(() => {
                try {
                    new TaskExecutor(runnerContext).Execute();

                    return true;
                }
                catch (Exception ex) {
                    messages.AppendLine(ExceptionUtils.GetExceptionMsg(ex));
                    return false;
                }
                finally {
                    writer.Close();
                }
            });
        }

        //---------------------------------------------------------------------
    }
}